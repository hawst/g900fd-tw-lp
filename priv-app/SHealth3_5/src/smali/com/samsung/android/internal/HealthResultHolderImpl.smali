.class public Lcom/samsung/android/internal/HealthResultHolderImpl;
.super Ljava/lang/Object;
.source "HealthResultHolderImpl.java"

# interfaces
.implements Lcom/samsung/android/health/HealthResultHolder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/samsung/android/health/HealthResultHolder$BaseResult;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/samsung/android/health/HealthResultHolder",
        "<TT;>;"
    }
.end annotation


# static fields
.field protected static final RESULT_CANCELED:I = 0x1

.field protected static final RESULT_INTERRUPTED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Health.HealthResultHolderImpl"


# instance fields
.field private mCallback:Lcom/samsung/android/health/HealthResultHolder$ResultListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/android/health/HealthResultHolder$ResultListener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mCanceled:Z

.field private volatile mConsumed:Z

.field private mFinished:Z

.field private final mHandler:Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mLatch:Ljava/util/concurrent/CountDownLatch;

.field private final mLock:Ljava/lang/Object;

.field private volatile mResult:Lcom/samsung/android/health/HealthResultHolder$BaseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 48
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLatch:Ljava/util/concurrent/CountDownLatch;

    .line 49
    new-instance v0, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;

    invoke-direct {v0}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mHandler:Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/health/HealthResultHolder$BaseResult;Landroid/os/Looper;)V
    .locals 2
    .param p2, "looper"    # Landroid/os/Looper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/os/Looper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    .local p1, "paramR":Lcom/samsung/android/health/HealthResultHolder$BaseResult;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLatch:Ljava/util/concurrent/CountDownLatch;

    .line 53
    new-instance v0, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;

    invoke-direct {v0, p2}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mHandler:Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;

    .line 54
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/HealthResultHolderImpl;->setResult(Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    .local p1, "handler":Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;, "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLatch:Ljava/util/concurrent/CountDownLatch;

    .line 58
    iput-object p1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mHandler:Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;

    .line 59
    return-void
.end method

.method private acquireResult()Lcom/samsung/android/health/HealthResultHolder$BaseResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 70
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    iget-object v2, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 71
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->confirmResultReady()V

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->confirmResultNotConsumed()V

    .line 74
    iget-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mResult:Lcom/samsung/android/health/HealthResultHolder$BaseResult;

    .line 75
    .local v0, "result":Lcom/samsung/android/health/HealthResultHolder$BaseResult;, "TT;"
    invoke-virtual {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->clearStatus()V

    .line 76
    monitor-exit v2

    return-object v0

    .line 77
    .end local v0    # "result":Lcom/samsung/android/health/HealthResultHolder$BaseResult;, "TT;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private confirmResultNotConsumed()V
    .locals 2

    .prologue
    .line 179
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    iget-boolean v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mConsumed:Z

    if-eqz v0, :cond_0

    .line 180
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Result has already been processed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_0
    return-void
.end method

.method private confirmResultReady()V
    .locals 2

    .prologue
    .line 185
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->isReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Result is not ready"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    return-void
.end method

.method private processInterrupt()V
    .locals 2

    .prologue
    .line 145
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    iget-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 146
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->isReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mFinished:Z

    .line 149
    :cond_0
    monitor-exit v1

    .line 150
    return-void

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private processResult(Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    .local p1, "paramR":Lcom/samsung/android/health/HealthResultHolder$BaseResult;, "TT;"
    iput-object p1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mResult:Lcom/samsung/android/health/HealthResultHolder$BaseResult;

    .line 170
    iget-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 171
    iget-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCallback:Lcom/samsung/android/health/HealthResultHolder$ResultListener;

    if-eqz v0, :cond_0

    .line 172
    iget-boolean v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCanceled:Z

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mHandler:Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;

    iget-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCallback:Lcom/samsung/android/health/HealthResultHolder$ResultListener;

    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->acquireResult()Lcom/samsung/android/health/HealthResultHolder$BaseResult;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;->send(Lcom/samsung/android/health/HealthResultHolder$ResultListener;Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V

    .line 176
    :cond_0
    return-void
.end method


# virtual methods
.method public final await()Lcom/samsung/android/health/HealthResultHolder$BaseResult;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 83
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "await() must not be called on the main thread"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->confirmResultNotConsumed()V

    .line 89
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->confirmResultReady()V

    .line 96
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->acquireResult()Lcom/samsung/android/health/HealthResultHolder$BaseResult;

    move-result-object v1

    return-object v1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "localInterruptedException":Ljava/lang/InterruptedException;
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->processInterrupt()V

    goto :goto_0
.end method

.method public final cancel()V
    .locals 4

    .prologue
    .line 122
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    iget-object v2, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 123
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCanceled:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mConsumed:Z

    if-eqz v1, :cond_1

    .line 124
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :goto_0
    return-void

    .line 128
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->cancelResult()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 133
    :goto_1
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCallback:Lcom/samsung/android/health/HealthResultHolder$ResultListener;

    .line 134
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCanceled:Z

    .line 135
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 129
    :catch_0
    move-exception v0

    .line 130
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v1, "Health.HealthResultHolderImpl"

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method protected cancelResult()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    return-void
.end method

.method protected clearStatus()V
    .locals 2

    .prologue
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    const/4 v1, 0x0

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mConsumed:Z

    .line 140
    iput-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mResult:Lcom/samsung/android/health/HealthResultHolder$BaseResult;

    .line 141
    iput-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCallback:Lcom/samsung/android/health/HealthResultHolder$ResultListener;

    .line 142
    return-void
.end method

.method public final isCanceled()Z
    .locals 2

    .prologue
    .line 115
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    iget-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCanceled:Z

    monitor-exit v1

    return v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final isReady()Z
    .locals 4

    .prologue
    .line 66
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    iget-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setResult(Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 153
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    .local p1, "paramR":Lcom/samsung/android/health/HealthResultHolder$BaseResult;, "TT;"
    iget-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mFinished:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCanceled:Z

    if-eqz v0, :cond_1

    .line 155
    :cond_0
    monitor-exit v1

    .line 166
    :goto_0
    return-void

    .line 158
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->isReady()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Result have been set already"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 162
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->confirmResultNotConsumed()V

    .line 164
    invoke-direct {p0, p1}, Lcom/samsung/android/internal/HealthResultHolderImpl;->processResult(Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V

    .line 165
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final setResultListener(Lcom/samsung/android/health/HealthResultHolder$ResultListener;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/health/HealthResultHolder$ResultListener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl;, "Lcom/samsung/android/internal/HealthResultHolderImpl<TT;>;"
    .local p1, "callback":Lcom/samsung/android/health/HealthResultHolder$ResultListener;, "Lcom/samsung/android/health/HealthResultHolder$ResultListener<TT;>;"
    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->confirmResultNotConsumed()V

    .line 103
    iget-object v1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    monitor-exit v1

    .line 112
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mHandler:Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;

    invoke-direct {p0}, Lcom/samsung/android/internal/HealthResultHolderImpl;->acquireResult()Lcom/samsung/android/health/HealthResultHolder$BaseResult;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;->send(Lcom/samsung/android/health/HealthResultHolder$ResultListener;Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V

    .line 111
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 109
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/samsung/android/internal/HealthResultHolderImpl;->mCallback:Lcom/samsung/android/health/HealthResultHolder$ResultListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
