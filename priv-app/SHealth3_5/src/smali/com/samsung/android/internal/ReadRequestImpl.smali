.class public final Lcom/samsung/android/internal/ReadRequestImpl;
.super Ljava/lang/Object;
.source "ReadRequestImpl.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/samsung/android/health/HealthDataResolver$ReadRequest;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/internal/ReadRequestImpl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBegin:J

.field private final mDataType:Ljava/lang/String;

.field private mEnd:J

.field private mPackageName:Ljava/lang/String;

.field private mQuery:Ljava/lang/String;

.field private mSortOrder:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/samsung/android/internal/ReadRequestImpl$1;

    invoke-direct {v0}, Lcom/samsung/android/internal/ReadRequestImpl$1;-><init>()V

    sput-object v0, Lcom/samsung/android/internal/ReadRequestImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mDataType:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mQuery:Ljava/lang/String;

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mSortOrder:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mPackageName:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mBegin:J

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mEnd:J

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "dataTypeName"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mDataType:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 0
    .param p1, "dataTypeName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "query"    # Ljava/lang/String;
    .param p4, "sortOrder"    # Ljava/lang/String;
    .param p5, "begin"    # J
    .param p7, "end"    # J

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mDataType:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mPackageName:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mQuery:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mSortOrder:Ljava/lang/String;

    .line 49
    iput-wide p5, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mBegin:J

    .line 50
    iput-wide p7, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mEnd:J

    .line 51
    return-void
.end method

.method public static creatReadRequestForLastItem(Ljava/lang/String;)Lcom/samsung/android/internal/ReadRequestImpl;
    .locals 2
    .param p0, "dataTypeName"    # Ljava/lang/String;

    .prologue
    .line 54
    new-instance v0, Lcom/samsung/android/internal/ReadRequestImpl;

    invoke-direct {v0, p0}, Lcom/samsung/android/internal/ReadRequestImpl;-><init>(Ljava/lang/String;)V

    .line 55
    .local v0, "inst":Lcom/samsung/android/internal/ReadRequestImpl;
    const-string/jumbo v1, "update_time DESC LIMIT 1"

    iput-object v1, v0, Lcom/samsung/android/internal/ReadRequestImpl;->mSortOrder:Ljava/lang/String;

    .line 56
    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mDataType:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public getSortOrder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mSortOrder:Ljava/lang/String;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mSortOrder:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mDataType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mSortOrder:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-wide v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mBegin:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 80
    iget-wide v0, p0, Lcom/samsung/android/internal/ReadRequestImpl;->mEnd:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 81
    return-void
.end method
