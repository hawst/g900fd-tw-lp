.class Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;
.super Landroid/os/Handler;
.source "HealthResultHolderImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/internal/HealthResultHolderImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResultHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/samsung/android/health/HealthResultHolder$BaseResult;",
        ">",
        "Landroid/os/Handler;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 193
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;, "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler<TT;>;"
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;-><init>(Landroid/os/Looper;)V

    .line 194
    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1, "looper"    # Landroid/os/Looper;

    .prologue
    .line 197
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;, "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler<TT;>;"
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 198
    return-void
.end method

.method private invokeCallback(Lcom/samsung/android/health/HealthResultHolder$ResultListener;Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/health/HealthResultHolder$ResultListener",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;, "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler<TT;>;"
    .local p1, "callback":Lcom/samsung/android/health/HealthResultHolder$ResultListener;, "Lcom/samsung/android/health/HealthResultHolder$ResultListener<TT;>;"
    .local p2, "result":Lcom/samsung/android/health/HealthResultHolder$BaseResult;, "TT;"
    invoke-interface {p1, p2}, Lcom/samsung/android/health/HealthResultHolder$ResultListener;->onResult(Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V

    .line 219
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 207
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;, "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler<TT;>;"
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 213
    const-string v1, "Health.HealthResultHolderImpl"

    const-string v2, "No default handler"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :goto_0
    return-void

    .line 209
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 210
    .local v0, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/samsung/android/health/HealthResultHolder$ResultListener<TT;>;TT;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/health/HealthResultHolder$ResultListener;

    iget-object v2, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/android/health/HealthResultHolder$BaseResult;

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;->invokeCallback(Lcom/samsung/android/health/HealthResultHolder$ResultListener;Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V

    goto :goto_0

    .line 207
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public send(Lcom/samsung/android/health/HealthResultHolder$ResultListener;Lcom/samsung/android/health/HealthResultHolder$BaseResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/health/HealthResultHolder$ResultListener",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 201
    .local p0, "this":Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;, "Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler<TT;>;"
    .local p1, "callback":Lcom/samsung/android/health/HealthResultHolder$ResultListener;, "Lcom/samsung/android/health/HealthResultHolder$ResultListener<TT;>;"
    .local p2, "result":Lcom/samsung/android/health/HealthResultHolder$BaseResult;, "TT;"
    const/4 v0, 0x1

    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/internal/HealthResultHolderImpl$ResultHandler;->sendMessage(Landroid/os/Message;)Z

    .line 202
    return-void
.end method
