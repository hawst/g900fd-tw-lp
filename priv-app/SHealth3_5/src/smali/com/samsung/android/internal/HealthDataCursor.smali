.class public Lcom/samsung/android/internal/HealthDataCursor;
.super Landroid/database/AbstractWindowedCursor;
.source "HealthDataCursor.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final BUNDLE_KEY:Ljava/lang/String; = "ColumnNames"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/internal/HealthDataCursor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBundle:Landroid/os/Bundle;

.field private mColumnNames:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/samsung/android/internal/HealthDataCursor$1;

    invoke-direct {v0}, Lcom/samsung/android/internal/HealthDataCursor$1;-><init>()V

    sput-object v0, Lcom/samsung/android/internal/HealthDataCursor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/database/CursorWindow;)V
    .locals 1
    .param p1, "window"    # Landroid/database/CursorWindow;

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/database/AbstractWindowedCursor;-><init>()V

    .line 34
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthDataCursor;->mBundle:Landroid/os/Bundle;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/internal/HealthDataCursor;->mColumnNames:[Ljava/lang/String;

    .line 39
    invoke-super {p0, p1}, Landroid/database/AbstractWindowedCursor;->setWindow(Landroid/database/CursorWindow;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/database/AbstractWindowedCursor;-><init>()V

    .line 34
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/internal/HealthDataCursor;->mBundle:Landroid/os/Bundle;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/internal/HealthDataCursor;->mColumnNames:[Ljava/lang/String;

    .line 43
    invoke-virtual {p0, p1}, Lcom/samsung/android/internal/HealthDataCursor;->readFromParcel(Landroid/os/Parcel;)V

    .line 44
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/internal/HealthDataCursor;->mColumnNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Landroid/database/AbstractWindowedCursor;->getWindow()Landroid/database/CursorWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/CursorWindow;->getNumRows()I

    move-result v0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 60
    const-class v1, Landroid/database/CursorWindow;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/database/CursorWindow;

    .line 61
    .local v0, "window":Landroid/database/CursorWindow;
    invoke-super {p0, v0}, Landroid/database/AbstractWindowedCursor;->setWindow(Landroid/database/CursorWindow;)V

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/internal/HealthDataCursor;->mBundle:Landroid/os/Bundle;

    .line 63
    iget-object v1, p0, Lcom/samsung/android/internal/HealthDataCursor;->mBundle:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, p0, Lcom/samsung/android/internal/HealthDataCursor;->mBundle:Landroid/os/Bundle;

    const-string v2, "ColumnNames"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/internal/HealthDataCursor;->mColumnNames:[Ljava/lang/String;

    .line 66
    :cond_0
    return-void
.end method

.method public setColumnNames([Ljava/lang/String;)V
    .locals 0
    .param p1, "columnNames"    # [Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/internal/HealthDataCursor;->mColumnNames:[Ljava/lang/String;

    .line 84
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 53
    invoke-super {p0}, Landroid/database/AbstractWindowedCursor;->getWindow()Landroid/database/CursorWindow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 55
    iget-object v0, p0, Lcom/samsung/android/internal/HealthDataCursor;->mBundle:Landroid/os/Bundle;

    const-string v1, "ColumnNames"

    iget-object v2, p0, Lcom/samsung/android/internal/HealthDataCursor;->mColumnNames:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/samsung/android/internal/HealthDataCursor;->mBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 57
    return-void
.end method
