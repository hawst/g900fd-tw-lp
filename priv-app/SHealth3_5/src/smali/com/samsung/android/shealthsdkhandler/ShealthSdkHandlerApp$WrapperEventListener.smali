.class public Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;
.super Ljava/lang/Object;
.source "ShealthSdkHandlerApp.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "WrapperEventListener"
.end annotation


# instance fields
.field mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

.field final synthetic this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;


# direct methods
.method constructor <init>(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0
    .param p2, "device"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    iput-object p2, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 273
    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 5
    .param p1, "error"    # I

    .prologue
    .line 278
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    iget-object v2, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->onDeviceJoined(Ljava/lang/String;I)V

    .line 279
    if-nez p1, :cond_0

    .line 281
    :try_start_0
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "onJoined"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    invoke-virtual {v1}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "deviceObj.onJoined called ..."

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 284
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$100(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    new-instance v2, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;

    iget-object v3, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    iget-object v4, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;-><init>(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 287
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "startReceivingData called..."

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    .line 310
    :goto_0
    return-void

    .line 288
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 291
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 294
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 297
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 299
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 300
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 302
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 303
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 305
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 308
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :cond_0
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error sent by Sensor : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onLeft(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 314
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device with ID :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Left the show, error code :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v0, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->onDeviceLeft(Ljava/lang/String;I)V

    .line 317
    iget-object v0, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$100(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$100(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;->mDevice:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    :cond_0
    return-void
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 0
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    .line 331
    return-void
.end method

.method public onStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 326
    return-void
.end method
