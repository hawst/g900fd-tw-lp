.class public abstract Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
.super Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;
.source "ShealthSdkHandlerApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;,
        Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;,
        Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;
    }
.end annotation


# static fields
.field private static LOG_TAG:Ljava/lang/String; = null

.field protected static final RECEIVED_CONNECTED_DEVICE:I = 0x7b

.field protected static final REGISTER_LISTENER:I = 0x141


# instance fields
.field private mDeviceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mEventHandler:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    invoke-static {v0}, Lcom/sec/android/app/shealth/common/utils/Logging;->getLogTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;-><init>()V

    .line 266
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    return-object v0
.end method

.method private initHandler()V
    .locals 3

    .prologue
    .line 102
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SensorEventWorker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 103
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 104
    new-instance v1, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;-><init>(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mEventHandler:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;

    .line 105
    return-void
.end method


# virtual methods
.method public abstract getCurrentMeasureType()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public leaveConnectedDeviceIfAny()Z
    .locals 6

    .prologue
    .line 110
    sget-object v4, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    const-string v5, "leaveConnectedDeviceIfAny"

    invoke-static {v4, v5}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v3, 0x1

    .line 112
    .local v3, "status":Z
    iget-object v4, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 113
    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 115
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 116
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 120
    :try_start_0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 121
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->stopReceivingData()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    .line 141
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 142
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_6

    goto :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 123
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 125
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 127
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 129
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 131
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 133
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 134
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v0

    .line 136
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_1

    .line 148
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_5
    move-exception v0

    .line 150
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 152
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_6
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 158
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 160
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->teardownFinder()Z

    .line 161
    return v3
.end method

.method public leaveDevice(Ljava/lang/String;)Z
    .locals 3
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 394
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 396
    :cond_0
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    const-string v2, "illegal argument passed for device Id"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    const/4 v1, 0x0

    .line 416
    :goto_0
    return v1

    .line 400
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->leave()V

    .line 402
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 404
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2

    .line 416
    :cond_2
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 406
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 409
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 411
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_1

    .line 412
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_2
    move-exception v0

    .line 414
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Lcom/sec/android/app/shealth/framework/ui/base/BasePluginApplication;->onCreate()V

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    .line 98
    invoke-direct {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->initHandler()V

    .line 99
    return-void
.end method

.method public abstract onDataStopped(Ljava/lang/String;II)V
.end method

.method public abstract onDeviceJoined(Ljava/lang/String;I)V
.end method

.method public abstract onDeviceLeft(Ljava/lang/String;I)V
.end method

.method public abstract onReceived(Ljava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
.end method

.method public resumeDataReceiving(Ljava/lang/String;)Z
    .locals 4
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 428
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 430
    :cond_0
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    const-string v2, "illegal argument passed for device Id"

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const/4 v1, 0x0

    .line 437
    :goto_0
    return v1

    .line 433
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mEventHandler:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;

    const/16 v2, 0x141

    invoke-virtual {v1, v2}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 434
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mDeviceMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 435
    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->mEventHandler:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 437
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setConnectedDevice(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Z
    .locals 5
    .param p1, "uContext"    # Landroid/content/Context;
    .param p2, "uDevice"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    const/4 v4, 0x0

    .line 349
    if-nez p2, :cond_0

    .line 351
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "setConnectedDevice :Device is null !!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :goto_0
    return v4

    .line 355
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->getCurrentMeasureType()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->getCurrentMeasureType()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 357
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "setConnectedDevice : Measure type is not supported !!"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 368
    :cond_1
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;

    const-string v3, "UpdateDeviceConnected"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    move-object v1, p2

    .line 372
    .local v1, "mDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_start_0
    new-instance v2, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;

    invoke-direct {v2, p0, v1}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$WrapperEventListener;-><init>(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->join(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 374
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 377
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 379
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 380
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 382
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 383
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 385
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 386
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v0

    .line 388
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0
.end method
