.class final Lcom/samsung/android/shealthsdkhandler/SensorScanFactory$1;
.super Ljava/lang/Object;
.source "SensorScanFactory.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 56
    # getter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$000()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x1

    # setter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->bsSensorServiceConnected:Z
    invoke-static {v0}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$102(Z)Z

    .line 60
    # getter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$000()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v1

    monitor-enter v1

    .line 62
    :try_start_0
    # getter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "onServiceConnected:notifying"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    # getter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$000()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 64
    monitor-exit v1

    .line 72
    :goto_0
    return-void

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 68
    :cond_0
    # getter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "onServiceConnected:sOnlyInstance is NILL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 77
    # getter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$002(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 79
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->bsSensorServiceConnected:Z
    invoke-static {v0}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->access$102(Z)Z

    .line 80
    return-void
.end method
