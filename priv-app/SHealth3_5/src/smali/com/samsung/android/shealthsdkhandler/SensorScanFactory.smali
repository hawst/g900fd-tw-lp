.class public Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;
.super Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
.source "SensorScanFactory.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static bsSensorServiceConnected:Z

.field private static sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private static sSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "SensorScanFactory"

    sput-object v0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory$1;

    invoke-direct {v0}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory$1;-><init>()V

    sput-object v0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    .line 15
    return-void
.end method

.method static synthetic access$000()Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 8
    sput-object p0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object p0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 8
    sput-boolean p0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->bsSensorServiceConnected:Z

    return p0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static declared-synchronized getDeviceFinder(Landroid/content/Context;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    const-class v2, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v1, :cond_0

    .line 21
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    sget-object v3, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sSensorConnectionListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    invoke-direct {v1, p0, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    sput-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 24
    :try_start_1
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;

    const-string v3, "getDeviceFinder: About start waiting"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    sget-object v3, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    monitor-enter v3
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 27
    const/4 v1, 0x0

    :try_start_2
    sput-boolean v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->bsSensorServiceConnected:Z

    .line 28
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 29
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;

    const-string v4, "getDeviceFinder: Lost out on time"

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 36
    :goto_0
    :try_start_3
    sget-boolean v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->bsSensorServiceConnected:Z

    if-nez v1, :cond_0

    .line 38
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;

    const-string v3, "getDeviceFinder: Couldn\'t bind , setting sOnlyInstance to NULL"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 43
    :cond_0
    sget-object v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v2

    return-object v1

    .line 30
    :catchall_0
    move-exception v1

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v1
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 31
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 19
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static isFinderConnected()Z
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static teardownFinder()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    sget-boolean v2, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->bsSensorServiceConnected:Z

    if-ne v2, v0, :cond_0

    .line 90
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 92
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->sOnlyInstance:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 93
    sput-boolean v1, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->bsSensorServiceConnected:Z

    .line 97
    :goto_0
    return v0

    .line 96
    :cond_0
    sget-object v0, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->TAG:Ljava/lang/String;

    const-string v2, "No Binder connection"

    invoke-static {v0, v2}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 97
    goto :goto_0
.end method
