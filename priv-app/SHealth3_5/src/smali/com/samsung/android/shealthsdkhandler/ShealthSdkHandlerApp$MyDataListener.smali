.class Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;
.super Ljava/lang/Object;
.source "ShealthSdkHandlerApp.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDataListener"
.end annotation


# instance fields
.field mSensorDeviceId:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;


# direct methods
.method public constructor <init>(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;Ljava/lang/String;)V
    .locals 0
    .param p2, "DeviceID"    # Ljava/lang/String;

    .prologue
    .line 235
    iput-object p1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    iput-object p2, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;->mSensorDeviceId:Ljava/lang/String;

    .line 237
    return-void
.end method


# virtual methods
.method public onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "data"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 258
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DataListener -> onReceived()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v0, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;->mSensorDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->onReceived(Ljava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V

    .line 261
    return-void
.end method

.method public onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "data"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p3, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 253
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DataListener -> onReceived([])"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    return-void
.end method

.method public onStarted(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 247
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DataListener -> onStarted()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStarted dataType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    return-void
.end method

.method public onStopped(II)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 240
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DataListener -> onStopped()"

    invoke-static {v0, v1}, Lcom/sec/android/app/shealth/common/utils/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "onStopped dataType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " error = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v0, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    iget-object v1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;->mSensorDeviceId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->onDataStopped(Ljava/lang/String;II)V

    .line 243
    return-void
.end method
