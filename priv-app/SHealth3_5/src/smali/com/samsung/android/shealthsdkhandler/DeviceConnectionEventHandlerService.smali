.class public Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;
.super Landroid/app/IntentService;
.source "DeviceConnectionEventHandlerService.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mHealthSensorFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mhealthDeviceId:Ljava/lang/String;

.field private mhealthDeviceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "DeviceFoundHandlerService"

    sput-object v0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->TAG:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mHealthSensorFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 29
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 34
    if-nez p1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "onHandleIntent"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 44
    const-string v2, "DEVICE_ID"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 45
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "DEVICE_ID"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mhealthDeviceId:Ljava/lang/String;

    .line 46
    :cond_2
    const-string v2, "DEVICE_TYPE"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 47
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "DEVICE_TYPE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mhealthDeviceType:I

    .line 49
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    .line 51
    .local v1, "uThisApp":Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
    invoke-virtual {v1}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->getCurrentMeasureType()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->getCurrentMeasureType()Ljava/util/ArrayList;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mhealthDeviceType:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 53
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "setConnectedDevice : Measure type is not supported !!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 56
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/shealthsdkhandler/SensorScanFactory;->getDeviceFinder(Landroid/content/Context;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mHealthSensorFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 57
    iget-object v2, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mHealthSensorFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v2, :cond_5

    .line 59
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->TAG:Ljava/lang/String;

    const-string v3, "Applicaiton not able to bind to Health Service ... so stopping IntentService"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 64
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->registerDataInApplication()V

    .line 65
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stopping Intent Service"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->TAG:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public registerDataInApplication()V
    .locals 9

    .prologue
    .line 76
    const/4 v2, 0x0

    .line 79
    .local v2, "listPairedBTDevices":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mHealthSensorFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    const/4 v6, 0x0

    iget v7, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mhealthDeviceType:I

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v2

    .line 93
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    .line 94
    .local v4, "uThisApp":Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
    if-eqz v2, :cond_1

    .line 96
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 98
    .local v3, "temp":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->mhealthDeviceId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionEventHandlerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->setConnectedDevice(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Z

    .line 105
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "temp":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :cond_1
    return-void

    .line 80
    .end local v4    # "uThisApp":Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 86
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 88
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 89
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_3
    move-exception v0

    .line 91
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method
