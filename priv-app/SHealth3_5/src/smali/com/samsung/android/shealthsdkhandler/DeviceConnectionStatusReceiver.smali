.class public Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DeviceConnectionStatusReceiver.java"


# static fields
.field public static ACTION_DEVICE_CONNECTED:Ljava/lang/String;

.field private static TAG:Ljava/lang/String;


# instance fields
.field private DEVICE_ID:Ljava/lang/String;

.field private DEVICE_TYPE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "DeviceConnectionStatusReceiver"

    sput-object v0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->TAG:Ljava/lang/String;

    .line 10
    const-string v0, "ACTION_DEVICE_CONNECTED"

    sput-object v0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 11
    const-string v0, "DEVICE_TYPE"

    iput-object v0, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->DEVICE_TYPE:Ljava/lang/String;

    .line 12
    const-string v0, "DEVICE_ID"

    iput-object v0, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->DEVICE_ID:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 18
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->TAG:Ljava/lang/String;

    const-string v3, "BloodPressesureDeviceStatus : onReceived"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_2

    sget-object v2, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->ACTION_DEVICE_CONNECTED:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 24
    new-instance v1, Landroid/content/Intent;

    const-string v2, "START_SCANNING_DEVICES"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 26
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "DEVICE_ID"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 27
    iget-object v2, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->DEVICE_ID:Ljava/lang/String;

    const-string v3, "DEVICE_ID"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 28
    :cond_0
    const-string v2, "DEVICE_TYPE"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 29
    iget-object v2, p0, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->DEVICE_TYPE:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "DEVICE_TYPE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 31
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 37
    .end local v1    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 34
    :cond_2
    sget-object v2, Lcom/samsung/android/shealthsdkhandler/DeviceConnectionStatusReceiver;->TAG:Ljava/lang/String;

    const-string v3, "ACTION_DEVICE_CONNECTED: Received for different device Type "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
