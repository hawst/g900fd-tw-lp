.class public Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;
.super Landroid/os/Handler;
.source "ShealthSdkHandlerApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SensorEventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;


# direct methods
.method public constructor <init>(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    .line 188
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 189
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 193
    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 225
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported message - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 195
    :sswitch_0
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "RECEIVED_CONNECTED_DEVICE"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 199
    :sswitch_1
    # getter for: Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "REGISTER_LISTENER"

    invoke-static {v2, v3}, Lcom/sec/android/app/shealth/common/utils/Logging;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .line 201
    .local v1, "uDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    if-eqz v1, :cond_0

    .line 203
    :try_start_0
    new-instance v2, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;

    iget-object v3, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$MyDataListener;-><init>(Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V

    .line 204
    iget-object v2, p0, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp$SensorEventHandler;->this$0:Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;

    invoke-virtual {v2}, Lcom/samsung/android/shealthsdkhandler/ShealthSdkHandlerApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "deviceObj.startReceivingData called..."

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/shealth/common/commonui/ToastView;->makeCustomView(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 207
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 208
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 209
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_2
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 212
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 215
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v0

    .line 217
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 218
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v0

    .line 220
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 193
    :sswitch_data_0
    .sparse-switch
        0x7b -> :sswitch_0
        0x141 -> :sswitch_1
    .end sparse-switch
.end method
