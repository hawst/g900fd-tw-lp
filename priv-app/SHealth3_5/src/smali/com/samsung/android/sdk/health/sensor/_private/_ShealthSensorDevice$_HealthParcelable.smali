.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;
.super Ljava/lang/Object;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_HealthParcelable"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 916
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 888
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 889
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->readFromParcel(Landroid/os/Parcel;)V

    .line 890
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)V
    .locals 0
    .param p1, "data"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .prologue
    .line 883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 884
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 885
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 931
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 932
    .local v0, "dataType":I
    packed-switch v0, :pswitch_data_0

    .line 1019
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_DefaultHealth;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_DefaultHealth;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 1022
    :goto_0
    return-void

    .line 935
    :pswitch_0
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 939
    :pswitch_1
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 943
    :pswitch_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 947
    :pswitch_3
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 951
    :pswitch_4
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 955
    :pswitch_5
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 959
    :pswitch_6
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 963
    :pswitch_7
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 967
    :pswitch_8
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 971
    :pswitch_9
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 975
    :pswitch_a
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 979
    :pswitch_b
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Temperature;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Temperature;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 983
    :pswitch_c
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 987
    :pswitch_d
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 991
    :pswitch_e
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 995
    :pswitch_f
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 999
    :pswitch_10
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto/16 :goto_0

    .line 1003
    :pswitch_11
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Profile;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Profile;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto/16 :goto_0

    .line 1007
    :pswitch_12
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_StepLevel;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_StepLevel;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto/16 :goto_0

    .line 1011
    :pswitch_13
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_InactiveTimer;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_InactiveTimer;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto/16 :goto_0

    .line 1015
    :pswitch_14
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;-><init>(Landroid/os/Parcel;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto/16 :goto_0

    .line 932
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_e
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_14
        :pswitch_13
    .end packed-switch
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 900
    const/4 v0, 0x0

    return v0
.end method

.method public getData()Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    .locals 1

    .prologue
    .line 894
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 906
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    if-eqz v0, :cond_0

    .line 908
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->getDataType()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 909
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;->_data:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;->writeToParcel(Landroid/os/Parcel;I)V

    .line 911
    :cond_0
    return-void
.end method
