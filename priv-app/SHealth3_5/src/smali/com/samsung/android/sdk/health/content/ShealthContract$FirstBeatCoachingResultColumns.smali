.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingResultColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FirstBeatCoachingResultColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final MAXIMAL_MET:Ljava/lang/String; = "maximal_met"

.field public static final RESOURCE_RECOVERY:Ljava/lang/String; = "resource_recovery"

.field public static final TRAINING_LOAD_PEAK:Ljava/lang/String; = "training_load_peak"

.field public static final USER_DEVICE__ID:Ljava/lang/String; = "user_device__id"
