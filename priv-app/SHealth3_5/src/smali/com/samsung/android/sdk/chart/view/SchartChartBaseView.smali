.class public Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;
.super Landroid/view/TextureView;
.source "SchartChartBaseView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;,
        Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;,
        Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;,
        Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;
    }
.end annotation


# static fields
.field public static final CATEGORY_DATE_TYPE:I = 0x2

.field public static final CATEGORY_STRING_TYPE:I = 0x1

.field public static final CHART_AREACURVE_TYPE:I = 0xb

.field public static final CHART_AREALINE_TYPE:I = 0xa

.field public static final CHART_BAR_TYPE:I = 0x3

.field public static final CHART_CANDLE_TYPE:I = 0x6

.field public static final CHART_CURVE_TYPE:I = 0x2

.field public static final CHART_DONUT_TYPE:I = 0x8

.field public static final CHART_LAYOUT_TYPE:I = 0x9

.field public static final CHART_LINE_TYPE:I = 0x1

.field public static final CHART_MULTY_TYPE:I = 0x5

.field public static final CHART_PIE_TYPE:I = 0x7

.field public static final CHART_STACKED_BAR_TYPE:I = 0x4

.field static final DRAG:I = 0x1

.field static final NONE:I = 0x0

.field static final TOUCH_DOWN:I = 0x4

.field static final TOUCH_UP:I = 0x3

.field private static final VELOCITY_THRESHOLD:I = 0x14

.field static final ZOOM:I = 0x2


# instance fields
.field protected final ATTRIBUTE_AXIS_TYPE:I

.field protected final ATTRIBUTE_CHART_TYPE:I

.field protected final ATTRIBUTE_EVENTICONS_TYPE:I

.field protected final ATTRIBUTE_GOALEVENTICONS_TYPE:I

.field protected final ATTRIBUTE_GOALLINE_TYPE:I

.field protected final ATTRIBUTE_GRAPHVALUES_TYPE:I

.field protected final ATTRIBUTE_GRAPH_TYPE:I

.field protected final ATTRIBUTE_GRIDLINE_TYPE:I

.field protected final ATTRIBUTE_HANDLER_TYPE:I

.field protected final ATTRIBUTE_INFOPOINT_TYPE:I

.field protected final ATTRIBUTE_INTERACTION_TYPE:I

.field protected final ATTRIBUTE_LEGEND_TYPE:I

.field protected final ATTRIBUTE_NORMALRANGE_TYPE:I

.field protected final ATTRIBUTE_SERIES_TYPE:I

.field protected final ATTRIBUTE_VALUEMARKING_TYPE:I

.field protected final DRAWING_AXISTEXT_TYPE:I

.field protected final DRAWING_AXIS_TYPE:I

.field protected final DRAWING_FRAME_TYPE:I

.field protected final DRAWING_GOALLINE_TYPE:I

.field protected final DRAWING_GRAPHBACKGROUND_TYPE:I

.field protected final DRAWING_GRAPH_TYPE:I

.field protected final DRAWING_GRID_TYPE:I

.field protected final DRAWING_HANDLERITEM_TYPE:I

.field protected final DRAWING_HANDLERLINE_TYPE:I

.field protected final DRAWING_HANDLERPOINT_TYPE:I

.field protected final DRAWING_INSTANCESPOT_TYPE:I

.field protected final DRAWING_LEGEND_TYPE:I

.field protected final DRAWING_NORMALRANGE_TYPE:I

.field final LOG_TAG:Ljava/lang/String;

.field private customPopup:Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;

.field final handlerInfo:Landroid/os/Handler;

.field isDetachedFromWindow:Z

.field isDoubleTap:Z

.field isDown:Z

.field isDrag:Z

.field private isFirstDraw:Z

.field isZoom:Z

.field private mAccMoveDistanceX:F

.field mAnimating:Z

.field mBitmap:Landroid/graphics/Bitmap;

.field protected mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

.field private mChartBitmapListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;

.field protected mChartHandle:J

.field protected mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

.field protected mChartType:I

.field protected mContext:Landroid/content/Context;

.field private mCustomHandler:Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;

.field protected mDataType:I

.field mDirtyMode:Z

.field private mDownX:F

.field private mDownY:F

.field protected mEventIconVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/EventIconProperty;",
            ">;"
        }
    .end annotation
.end field

.field mGLInit:Z

.field private mGes:Landroid/view/GestureDetector;

.field protected mGoalEventIconVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;",
            ">;"
        }
    .end annotation
.end field

.field protected mGoalLineVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/GoalLineProperty;",
            ">;"
        }
    .end annotation
.end field

.field mGraphAniDurationVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mGraphAniGraphIdxVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mGraphValueVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/GraphValueProperty;",
            ">;"
        }
    .end annotation
.end field

.field protected mGraphVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/GraphProperty;",
            ">;"
        }
    .end annotation
.end field

.field protected mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

.field protected mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

.field protected mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

.field mInit:Z

.field protected mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

.field protected mIsChartInitFinish:Z

.field private mIsDown:Z

.field private mIsFirstDataPrepareZoom:Z

.field mIsFirstFrame:Z

.field private mIsFlick:Z

.field protected mLeftYAxis_fixedMaxY:F

.field protected mLeftYAxis_fixedMinY:F

.field protected mLeftYAxis_usefixedMax:Z

.field protected mLeftYAxis_usefixedMin:Z

.field protected mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

.field private mMoveDistance:F

.field private mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected mNormalRangeVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/NormalRangeProperty;",
            ">;"
        }
    .end annotation
.end field

.field private mReadyToShowListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;

.field private mRefView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

.field mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

.field protected mRightYAxis_fixedMaxY:F

.field protected mRightYAxis_fixedMinY:F

.field protected mRightYAxis_usefixedMax:Z

.field protected mRightYAxis_usefixedMin:Z

.field private final mScrollThreshold:F

.field protected mSeriesCount:I

.field protected mSeriesNum:I

.field protected mSeriesVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/SeriesProperty;",
            ">;"
        }
    .end annotation
.end field

.field mSurfaceDestroyd:Z

.field protected mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTask:Ljava/util/TimerTask;

.field private mTimer:Ljava/util/Timer;

.field mTrace:Z

.field mTraceVelocity:F

.field private mTracker:Landroid/view/VelocityTracker;

.field private mVScroll:F

.field private mVScrollVelocity:F

.field protected mValueMarkingVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;",
            ">;"
        }
    .end annotation
.end field

.field private mVelocity:F

.field private mVertFlick:Z

.field protected mXAxisVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/AxisProperty;",
            ">;"
        }
    .end annotation
.end field

.field protected mYAxisVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/AxisProperty;",
            ">;"
        }
    .end annotation
.end field

.field private m_ZoomX:F

.field m_firstPoint:Landroid/graphics/PointF;

.field m_realFirstPoint:Landroid/graphics/PointF;

.field m_secondPoint:Landroid/graphics/PointF;

.field mid:Landroid/graphics/PointF;

.field mode:I

.field oldDist:F

.field pickType:I

.field final popupHandler:Landroid/os/Handler;

.field protected processProperty:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/sic/android/property/BaseProperty;",
            ">;"
        }
    .end annotation
.end field

.field private xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

.field zoomIdx:I

.field private zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 343
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 97
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGLInit:Z

    .line 98
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    .line 102
    iput v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    .line 132
    const-string v0, "ChartView RenderThread"

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->LOG_TAG:Ljava/lang/String;

    .line 150
    iput v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_CHART_TYPE:I

    .line 151
    iput v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GRAPH_TYPE:I

    .line 152
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GRIDLINE_TYPE:I

    .line 153
    iput v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_AXIS_TYPE:I

    .line 154
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_SERIES_TYPE:I

    .line 155
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_INFOPOINT_TYPE:I

    .line 156
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_HANDLER_TYPE:I

    .line 157
    const/4 v0, 0x7

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_NORMALRANGE_TYPE:I

    .line 158
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_VALUEMARKING_TYPE:I

    .line 159
    const/16 v0, 0x9

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GOALLINE_TYPE:I

    .line 160
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_LEGEND_TYPE:I

    .line 161
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_INTERACTION_TYPE:I

    .line 162
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GRAPHVALUES_TYPE:I

    .line 163
    const/16 v0, 0xd

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_EVENTICONS_TYPE:I

    .line 164
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GOALEVENTICONS_TYPE:I

    .line 166
    iput v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_AXIS_TYPE:I

    .line 167
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GRID_TYPE:I

    .line 168
    iput v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GRAPH_TYPE:I

    .line 169
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_AXISTEXT_TYPE:I

    .line 170
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_LEGEND_TYPE:I

    .line 171
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_FRAME_TYPE:I

    .line 172
    const/4 v0, 0x7

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_HANDLERLINE_TYPE:I

    .line 173
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_HANDLERPOINT_TYPE:I

    .line 174
    const/16 v0, 0x9

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_HANDLERITEM_TYPE:I

    .line 175
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_INSTANCESPOT_TYPE:I

    .line 176
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_NORMALRANGE_TYPE:I

    .line 177
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GRAPHBACKGROUND_TYPE:I

    .line 178
    const/16 v0, 0xd

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GOALLINE_TYPE:I

    .line 180
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mContext:Landroid/content/Context;

    .line 183
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstFrame:Z

    .line 185
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAnimating:Z

    .line 188
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    .line 189
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    .line 192
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    .line 193
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartBitmapListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;

    .line 194
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mReadyToShowListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;

    .line 200
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isFirstDraw:Z

    .line 201
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    .line 204
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 206
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDetachedFromWindow:Z

    .line 226
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    .line 228
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    .line 241
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    .line 252
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    .line 253
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    .line 254
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    .line 255
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    .line 256
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    .line 260
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 261
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    .line 262
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mScrollThreshold:F

    .line 263
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownX:F

    .line 264
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    .line 266
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsDown:Z

    .line 267
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    .line 268
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstDataPrepareZoom:Z

    .line 270
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    .line 271
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    .line 272
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownY:F

    .line 273
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    .line 279
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDown:Z

    .line 280
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDrag:Z

    .line 282
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isZoom:Z

    .line 283
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_ZoomX:F

    .line 287
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    .line 288
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomIdx:I

    .line 295
    iput v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 297
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mid:Landroid/graphics/PointF;

    .line 298
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->oldDist:F

    .line 307
    iput v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    .line 309
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMin:Z

    .line 310
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMax:Z

    .line 311
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_fixedMinY:F

    .line 312
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_fixedMaxY:F

    .line 314
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMin:Z

    .line 315
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMax:Z

    .line 316
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_fixedMinY:F

    .line 317
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_fixedMaxY:F

    .line 320
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDoubleTap:Z

    .line 323
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInit:Z

    .line 324
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    .line 325
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTraceVelocity:F

    .line 328
    iput-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mBitmap:Landroid/graphics/Bitmap;

    .line 2037
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$1;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->handlerInfo:Landroid/os/Handler;

    .line 2064
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$2;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->popupHandler:Landroid/os/Handler;

    .line 344
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mContext:Landroid/content/Context;

    .line 345
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->init()V

    .line 346
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setOpaque(Z)V

    .line 347
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 359
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 371
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 374
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGLInit:Z

    .line 98
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    .line 102
    iput v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    .line 132
    const-string v4, "ChartView RenderThread"

    iput-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->LOG_TAG:Ljava/lang/String;

    .line 150
    iput v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_CHART_TYPE:I

    .line 151
    iput v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GRAPH_TYPE:I

    .line 152
    const/4 v4, 0x2

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GRIDLINE_TYPE:I

    .line 153
    iput v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_AXIS_TYPE:I

    .line 154
    const/4 v4, 0x4

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_SERIES_TYPE:I

    .line 155
    const/4 v4, 0x5

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_INFOPOINT_TYPE:I

    .line 156
    const/4 v4, 0x6

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_HANDLER_TYPE:I

    .line 157
    const/4 v4, 0x7

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_NORMALRANGE_TYPE:I

    .line 158
    const/16 v4, 0x8

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_VALUEMARKING_TYPE:I

    .line 159
    const/16 v4, 0x9

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GOALLINE_TYPE:I

    .line 160
    const/16 v4, 0xa

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_LEGEND_TYPE:I

    .line 161
    const/16 v4, 0xb

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_INTERACTION_TYPE:I

    .line 162
    const/16 v4, 0xc

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GRAPHVALUES_TYPE:I

    .line 163
    const/16 v4, 0xd

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_EVENTICONS_TYPE:I

    .line 164
    const/16 v4, 0xe

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ATTRIBUTE_GOALEVENTICONS_TYPE:I

    .line 166
    iput v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_AXIS_TYPE:I

    .line 167
    const/4 v4, 0x2

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GRID_TYPE:I

    .line 168
    iput v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GRAPH_TYPE:I

    .line 169
    const/4 v4, 0x4

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_AXISTEXT_TYPE:I

    .line 170
    const/4 v4, 0x5

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_LEGEND_TYPE:I

    .line 171
    const/4 v4, 0x6

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_FRAME_TYPE:I

    .line 172
    const/4 v4, 0x7

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_HANDLERLINE_TYPE:I

    .line 173
    const/16 v4, 0x8

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_HANDLERPOINT_TYPE:I

    .line 174
    const/16 v4, 0x9

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_HANDLERITEM_TYPE:I

    .line 175
    const/16 v4, 0xa

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_INSTANCESPOT_TYPE:I

    .line 176
    const/16 v4, 0xb

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_NORMALRANGE_TYPE:I

    .line 177
    const/16 v4, 0xc

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GRAPHBACKGROUND_TYPE:I

    .line 178
    const/16 v4, 0xd

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->DRAWING_GOALLINE_TYPE:I

    .line 180
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mContext:Landroid/content/Context;

    .line 183
    iput-boolean v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstFrame:Z

    .line 185
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAnimating:Z

    .line 188
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    .line 189
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    .line 192
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    .line 193
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartBitmapListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;

    .line 194
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mReadyToShowListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;

    .line 200
    iput-boolean v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isFirstDraw:Z

    .line 201
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    .line 204
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 206
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDetachedFromWindow:Z

    .line 226
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    .line 228
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    .line 241
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    .line 252
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    .line 253
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    .line 254
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    .line 255
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    .line 256
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    .line 260
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 261
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    .line 262
    const/high16 v4, 0x41a00000    # 20.0f

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mScrollThreshold:F

    .line 263
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownX:F

    .line 264
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    .line 266
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsDown:Z

    .line 267
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    .line 268
    iput-boolean v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstDataPrepareZoom:Z

    .line 270
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    .line 271
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    .line 272
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownY:F

    .line 273
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    .line 279
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDown:Z

    .line 280
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDrag:Z

    .line 282
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isZoom:Z

    .line 283
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_ZoomX:F

    .line 287
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    .line 288
    const/4 v4, -0x1

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomIdx:I

    .line 295
    iput v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 297
    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mid:Landroid/graphics/PointF;

    .line 298
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->oldDist:F

    .line 307
    iput v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    .line 309
    iput-boolean v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMin:Z

    .line 310
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMax:Z

    .line 311
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_fixedMinY:F

    .line 312
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_fixedMaxY:F

    .line 314
    iput-boolean v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMin:Z

    .line 315
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMax:Z

    .line 316
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_fixedMinY:F

    .line 317
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_fixedMaxY:F

    .line 320
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDoubleTap:Z

    .line 323
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInit:Z

    .line 324
    iput-boolean v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    .line 325
    iput v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTraceVelocity:F

    .line 328
    iput-object v7, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mBitmap:Landroid/graphics/Bitmap;

    .line 2037
    new-instance v4, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$1;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->handlerInfo:Landroid/os/Handler;

    .line 2064
    new-instance v4, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$2;

    invoke-direct {v4, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$2;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->popupHandler:Landroid/os/Handler;

    .line 375
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mContext:Landroid/content/Context;

    .line 376
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->init()V

    .line 378
    const/4 v3, 0x0

    .line 379
    .local v3, "value":Ljava/lang/String;
    invoke-interface {p2}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v1

    .line 380
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-lt v2, v1, :cond_0

    .line 388
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 392
    return-void

    .line 381
    :cond_0
    invoke-interface {p2, v2}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, "attrName":Ljava/lang/String;
    invoke-interface {p2, v2}, Landroid/util/AttributeSet;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v3

    .line 380
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private native CheckSpotInfo(JF)V
.end method

.method private native GetPickingObjectType(JFF)I
.end method

.method private ProcessProperty()V
    .locals 7

    .prologue
    .line 3098
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 3099
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 3162
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 3164
    .end local v1    # "i":I
    :cond_0
    return-void

    .line 3101
    .restart local v1    # "i":I
    :cond_1
    const/4 v0, -0x1

    .line 3103
    .local v0, "attributeType":I
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/AxisProperty;

    if-ne v2, v3, :cond_4

    .line 3105
    const/4 v0, 0x3

    .line 3148
    :cond_2
    :goto_1
    const/4 v2, -0x1

    if-eq v0, v2, :cond_3

    .line 3149
    iget-wide v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    int-to-long v5, v0

    .line 3150
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    .line 3149
    invoke-static {v3, v4, v5, v6, v2}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetAttribute(JJLcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3099
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3106
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/ChartProperty;

    if-ne v2, v3, :cond_5

    .line 3108
    const/4 v0, 0x0

    .line 3109
    goto :goto_1

    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/GraphProperty;

    if-ne v2, v3, :cond_6

    .line 3111
    const/4 v0, 0x1

    .line 3112
    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/GridLineProperty;

    if-ne v2, v3, :cond_7

    .line 3114
    const/4 v0, 0x2

    .line 3115
    goto :goto_1

    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    if-ne v2, v3, :cond_8

    .line 3117
    const/4 v0, 0x4

    .line 3118
    goto :goto_1

    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    if-ne v2, v3, :cond_9

    .line 3120
    const/4 v0, 0x5

    .line 3121
    goto :goto_1

    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/HandlerProperty;

    if-ne v2, v3, :cond_a

    .line 3123
    const/4 v0, 0x6

    .line 3124
    goto :goto_1

    :cond_a
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    if-ne v2, v3, :cond_b

    .line 3126
    const/4 v0, 0x7

    .line 3127
    goto/16 :goto_1

    :cond_b
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    if-ne v2, v3, :cond_c

    .line 3129
    const/16 v0, 0x8

    .line 3130
    goto/16 :goto_1

    :cond_c
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    if-ne v2, v3, :cond_d

    .line 3132
    const/16 v0, 0x9

    .line 3133
    goto/16 :goto_1

    :cond_d
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/LegendProperty;

    if-ne v2, v3, :cond_e

    .line 3135
    const/16 v0, 0xa

    .line 3136
    goto/16 :goto_1

    :cond_e
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/InteractionProperty;

    if-ne v2, v3, :cond_f

    .line 3138
    const/16 v0, 0xb

    .line 3139
    goto/16 :goto_1

    :cond_f
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    if-ne v2, v3, :cond_10

    .line 3140
    const/16 v0, 0xc

    .line 3141
    goto/16 :goto_1

    :cond_10
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    if-ne v2, v3, :cond_11

    .line 3142
    const/16 v0, 0xd

    .line 3143
    goto/16 :goto_1

    :cond_11
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v3, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    if-ne v2, v3, :cond_2

    .line 3144
    const/16 v0, 0xe

    goto/16 :goto_1
.end method

.method private native SetChartScroll(JFI)V
.end method

.method private native SetDataZoom(JFFF)V
.end method

.method private native SetVM(J)V
.end method

.method private native SetZoom(JFFF)V
.end method

.method private native SettingHandlerMagneticPosition(JF)V
.end method

.method private native SettingHandlerRecentlyPosition(J)V
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;J)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetVM(J)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private convertStringToBooleanArray([Ljava/lang/String;)[Z
    .locals 5
    .param p1, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 1764
    array-length v2, p1

    .line 1765
    .local v2, "strArrayLen":I
    new-array v0, v2, [Z

    .line 1767
    .local v0, "booleanArray":[Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 1775
    return-object v0

    .line 1768
    :cond_0
    aget-object v3, p1, v1

    const-string v4, "false"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1769
    const/4 v3, 0x0

    aput-boolean v3, v0, v1

    .line 1767
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1770
    :cond_2
    aget-object v3, p1, v1

    const-string/jumbo v4, "true"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1771
    const/4 v3, 0x1

    aput-boolean v3, v0, v1

    goto :goto_1
.end method

.method private createHandlerTimer()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2132
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getTimeOutDelay()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    .line 2182
    :cond_0
    :goto_0
    return-void

    .line 2137
    :cond_1
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    .line 2138
    new-instance v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$5;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    .line 2160
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getTimeOutDelay()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2161
    :catch_0
    move-exception v0

    .line 2164
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    if-eqz v1, :cond_2

    .line 2167
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    invoke-virtual {v1}, Ljava/util/TimerTask;->cancel()Z

    .line 2168
    iput-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    .line 2171
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_3

    .line 2174
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 2175
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->purge()I

    .line 2176
    iput-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    .line 2178
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->createHandlerTimer()V

    goto :goto_0
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 395
    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRefView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .line 396
    iput-boolean v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDirtyMode:Z

    .line 398
    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    .line 399
    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartBitmapListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;

    .line 403
    new-instance v2, Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/ChartProperty;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    .line 405
    new-instance v2, Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    .line 407
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mXAxisVector:Ljava/util/Vector;

    .line 408
    new-instance v0, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;-><init>()V

    .line 409
    .local v0, "temp1":Lcom/sec/dmc/sic/android/property/AxisProperty;
    invoke-virtual {v0, v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 410
    invoke-virtual {v0, v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeXY(I)V

    .line 411
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeDirection(I)V

    .line 412
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 414
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    .line 415
    new-instance v1, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;-><init>()V

    .line 416
    .local v1, "temp2":Lcom/sec/dmc/sic/android/property/AxisProperty;
    invoke-virtual {v1, v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 417
    invoke-virtual {v1, v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeXY(I)V

    .line 418
    invoke-virtual {v1, v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeDirection(I)V

    .line 419
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 421
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphVector:Ljava/util/Vector;

    .line 423
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesVector:Ljava/util/Vector;

    .line 425
    new-instance v2, Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/GridLineProperty;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    .line 427
    new-instance v2, Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    .line 429
    new-instance v2, Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    .line 431
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNormalRangeVector:Ljava/util/Vector;

    .line 432
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    .line 433
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalLineVector:Ljava/util/Vector;

    .line 434
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphValueVector:Ljava/util/Vector;

    .line 435
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mEventIconVector:Ljava/util/Vector;

    .line 436
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalEventIconVector:Ljava/util/Vector;

    .line 440
    invoke-virtual {p0, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 441
    invoke-virtual {p0, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 443
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    .line 446
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    .line 447
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_realFirstPoint:Landroid/graphics/PointF;

    .line 448
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_secondPoint:Landroid/graphics/PointF;

    .line 450
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniGraphIdxVector:Ljava/util/Vector;

    .line 451
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniDurationVector:Ljava/util/Vector;

    .line 461
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v2, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 462
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v2, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 463
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 464
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 465
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 467
    new-instance v2, Landroid/view/GestureDetector;

    invoke-direct {v2, p0}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGes:Landroid/view/GestureDetector;

    .line 469
    new-instance v2, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 470
    return-void
.end method

.method private native isHandlerOnScreen(J)Z
.end method

.method private midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 7
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    .line 2805
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float v0, v2, v3

    .line 2806
    .local v0, "x":F
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    add-float v1, v2, v3

    .line 2807
    .local v1, "y":F
    div-float v2, v0, v4

    div-float v3, v1, v4

    invoke-virtual {p1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    .line 2808
    return-void
.end method

.method private native setInteractionCallback(Ljava/lang/String;)V
.end method

.method private spacing(Landroid/view/MotionEvent;)F
    .locals 7
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 2786
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    if-le v3, v5, :cond_2

    .line 2787
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    sub-float v0, v3, v4

    .line 2788
    .local v0, "x":F
    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    sub-float v1, v3, v4

    .line 2790
    .local v1, "y":F
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->getDataZoomEnable()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2792
    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    .line 2793
    const/high16 v2, -0x40800000    # -1.0f

    mul-float/2addr v0, v2

    .line 2801
    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_0
    :goto_0
    return v0

    .line 2799
    .restart local v0    # "x":F
    .restart local v1    # "y":F
    :cond_1
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v0, v2

    goto :goto_0

    .end local v0    # "x":F
    .end local v1    # "y":F
    :cond_2
    move v0, v2

    .line 2801
    goto :goto_0
.end method

.method private native step(J)Z
.end method


# virtual methods
.method public DeleteForHeartRate(I)J
    .locals 2
    .param p1, "offset"    # I

    .prologue
    .line 4514
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1, p1}, Lcom/sec/dmc/sic/jni/ChartJNI;->DeleteForHeartRate(JI)J

    move-result-wide v0

    return-wide v0
.end method

.method native SetHandlerTranslate(JIF)V
.end method

.method native SetZoomPanning(JFF)V
.end method

.method protected addAxis(II)V
    .locals 2
    .param p1, "xAxisCnt"    # I
    .param p2, "yAxisCnt"    # I

    .prologue
    .line 3741
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_1

    .line 3744
    const/4 v1, 0x4

    if-le p2, v1, :cond_0

    .line 3745
    const/4 p2, 0x4

    .line 3747
    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-lt v0, p2, :cond_2

    .line 3751
    return-void

    .line 3742
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->addXAxis()Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 3741
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3748
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->addYAxis()Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 3747
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected addAxises(II)V
    .locals 1
    .param p1, "xAxisCnt"    # I
    .param p2, "yAxisCnt"    # I

    .prologue
    const/4 v0, 0x2

    .line 3817
    if-lt p1, v0, :cond_0

    .line 3818
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->addXAxis()Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 3820
    :cond_0
    if-lt p2, v0, :cond_1

    .line 3821
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->addYAxis()Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 3823
    :cond_1
    return-void
.end method

.method protected addSecondYAxis()Lcom/sec/dmc/sic/android/property/AxisProperty;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 3401
    iget-wide v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v3, v4}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddYAxis(J)I

    move-result v0

    .line 3403
    .local v0, "idx":I
    new-instance v2, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;-><init>()V

    .line 3404
    .local v2, "temp":Lcom/sec/dmc/sic/android/property/AxisProperty;
    invoke-virtual {v2, v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 3406
    new-array v1, v5, [I

    const/4 v3, 0x0

    aput v0, v1, v3

    .line 3407
    .local v1, "seriesIDs":[I
    invoke-virtual {v2, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V

    .line 3408
    invoke-virtual {v2, v5}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 3409
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeXY(I)V

    .line 3411
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v3, v0, v2}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3413
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/dmc/sic/android/property/AxisProperty;

    return-object v3
.end method

.method protected addXAxis()Lcom/sec/dmc/sic/android/property/AxisProperty;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3363
    iget-wide v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v3, v4}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddXAxis(J)I

    move-result v0

    .line 3365
    .local v0, "idx":I
    new-instance v2, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;-><init>()V

    .line 3366
    .local v2, "temp":Lcom/sec/dmc/sic/android/property/AxisProperty;
    invoke-virtual {v2, v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 3368
    new-array v1, v6, [I

    aput v0, v1, v5

    .line 3369
    .local v1, "seriesIDs":[I
    invoke-virtual {v2, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V

    .line 3370
    invoke-virtual {v2, v6}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 3371
    invoke-virtual {v2, v5}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeXY(I)V

    .line 3373
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v3, v0, v2}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3375
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/dmc/sic/android/property/AxisProperty;

    return-object v3
.end method

.method protected addYAxis()Lcom/sec/dmc/sic/android/property/AxisProperty;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 3385
    iget-wide v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v3, v4}, Lcom/sec/dmc/sic/jni/ChartJNI;->AddYAxis(J)I

    move-result v0

    .line 3387
    .local v0, "idx":I
    new-instance v2, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;-><init>()V

    .line 3388
    .local v2, "temp":Lcom/sec/dmc/sic/android/property/AxisProperty;
    invoke-virtual {v2, v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 3390
    new-array v1, v5, [I

    const/4 v3, 0x0

    aput v0, v1, v3

    .line 3391
    .local v1, "seriesIDs":[I
    invoke-virtual {v2, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V

    .line 3392
    invoke-virtual {v2, v5}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 3393
    invoke-virtual {v2, v5}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeXY(I)V

    .line 3395
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v3, v0, v2}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3397
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/dmc/sic/android/property/AxisProperty;

    return-object v3
.end method

.method protected convertStringToBitmapArray([Ljava/lang/String;)[Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 1814
    array-length v2, p1

    .line 1815
    .local v2, "strArrayLen":I
    new-array v0, v2, [Landroid/graphics/Bitmap;

    .line 1817
    .local v0, "bitmapArray":[Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 1821
    return-object v0

    .line 1818
    :cond_0
    aget-object v3, p1, v1

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->extractImageFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getDrawableFromResString(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v0, v1

    .line 1817
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected convertStringToBitmapArrayList([Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "strArray"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1826
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1828
    .local v0, "bitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-lt v1, v2, :cond_0

    .line 1832
    return-object v0

    .line 1829
    :cond_0
    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->extractImageFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getDrawableFromResString(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1828
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected convertStringToColorArray([Ljava/lang/String;)[I
    .locals 4
    .param p1, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 1790
    array-length v2, p1

    .line 1791
    .local v2, "strArrayLen":I
    new-array v1, v2, [I

    .line 1793
    .local v1, "intArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 1797
    return-object v1

    .line 1794
    :cond_0
    aget-object v3, p1, v0

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v0

    .line 1793
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected convertStringToFloatArray([Ljava/lang/String;)[F
    .locals 4
    .param p1, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 1779
    array-length v2, p1

    .line 1780
    .local v2, "strArrayLen":I
    new-array v0, v2, [F

    .line 1782
    .local v0, "floatArray":[F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 1786
    return-object v0

    .line 1783
    :cond_0
    aget-object v3, p1, v1

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    aput v3, v0, v1

    .line 1782
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected convertStringToIntegerArray([Ljava/lang/String;)[I
    .locals 4
    .param p1, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 1802
    array-length v2, p1

    .line 1803
    .local v2, "strArrayLen":I
    new-array v1, v2, [I

    .line 1805
    .local v1, "intArray":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 1809
    return-object v1

    .line 1806
    :cond_0
    aget-object v3, p1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v0

    .line 1805
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected createChart()V
    .locals 4

    .prologue
    .line 2964
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDataType:I

    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartType:I

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->create(II)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    .line 2965
    const-string v0, "ChartView RenderThread"

    .line 2966
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "~~~~~~~~~~~~~~~~~~~~~~public void init() -> mChartHandle: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2967
    iget-wide v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2966
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2965
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2968
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    .line 2969
    return-void
.end method

.method public createSeries(I)V
    .locals 11
    .param p1, "seriesType"    # I

    .prologue
    .line 3250
    iget-wide v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v9, v10, p1}, Lcom/sec/dmc/sic/jni/ChartJNI;->CreateSeriesSingle(JI)V

    .line 3252
    new-instance v6, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-direct {v6}, Lcom/sec/dmc/sic/android/property/SeriesProperty;-><init>()V

    .line 3253
    .local v6, "sTemp":Lcom/sec/dmc/sic/android/property/SeriesProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v6, v9}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setSeriesID(I)V

    .line 3254
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v6}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3255
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3257
    new-instance v7, Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-direct {v7}, Lcom/sec/dmc/sic/android/property/GraphProperty;-><init>()V

    .line 3258
    .local v7, "temp":Lcom/sec/dmc/sic/android/property/GraphProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v7, v9}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setGraphID(I)V

    .line 3259
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v7}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3260
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3262
    new-instance v8, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-direct {v8}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;-><init>()V

    .line 3263
    .local v8, "vMarkingPro":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v8, v9}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setValueMarkingID(I)V

    .line 3264
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v8}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3265
    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3267
    new-instance v5, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-direct {v5}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;-><init>()V

    .line 3268
    .local v5, "nRangePro":Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v5, v9}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setNormalRangeID(I)V

    .line 3269
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNormalRangeVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v5}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3270
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3272
    new-instance v2, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;-><init>()V

    .line 3273
    .local v2, "goalLinePro":Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v2, v9}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalLineID(I)V

    .line 3274
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalLineVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v2}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3275
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3277
    new-instance v3, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-direct {v3}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;-><init>()V

    .line 3278
    .local v3, "graphValuesPro":Lcom/sec/dmc/sic/android/property/GraphValueProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v3, v9}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setSeriesID(I)V

    .line 3279
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphValueVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v3}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3280
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3282
    new-instance v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;-><init>()V

    .line 3283
    .local v0, "eventIconsPro":Lcom/sec/dmc/sic/android/property/EventIconProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v0, v9}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setSeriesID(I)V

    .line 3284
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mEventIconVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v0}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3285
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3287
    new-instance v1, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;-><init>()V

    .line 3288
    .local v1, "goalEventIconsPro":Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v1, v9}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setSeriesID(I)V

    .line 3289
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalEventIconVector:Ljava/util/Vector;

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    invoke-virtual {v9, v10, v1}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3290
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3292
    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    if-nez v9, :cond_0

    .line 3293
    new-instance v4, Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-direct {v4}, Lcom/sec/dmc/sic/android/property/LegendProperty;-><init>()V

    .line 3294
    .local v4, "legend":Lcom/sec/dmc/sic/android/property/LegendProperty;
    iput-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    .line 3295
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3298
    .end local v4    # "legend":Lcom/sec/dmc/sic/android/property/LegendProperty;
    :cond_0
    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    .line 3300
    return-void
.end method

.method public createSeries(II)V
    .locals 12
    .param p1, "seriesType"    # I
    .param p2, "seriesCount"    # I

    .prologue
    .line 3176
    iget-wide v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v10, v11, p1, p2}, Lcom/sec/dmc/sic/jni/ChartJNI;->CreateSeries(JII)V

    .line 3178
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-lt v4, p2, :cond_1

    .line 3233
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    if-nez v10, :cond_0

    .line 3234
    new-instance v5, Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-direct {v5}, Lcom/sec/dmc/sic/android/property/LegendProperty;-><init>()V

    .line 3241
    .local v5, "legend":Lcom/sec/dmc/sic/android/property/LegendProperty;
    iput-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    .line 3242
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3245
    .end local v5    # "legend":Lcom/sec/dmc/sic/android/property/LegendProperty;
    :cond_0
    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    add-int/2addr v10, p2

    iput v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesCount:I

    .line 3246
    return-void

    .line 3179
    :cond_1
    new-instance v7, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-direct {v7}, Lcom/sec/dmc/sic/android/property/SeriesProperty;-><init>()V

    .line 3180
    .local v7, "sTemp":Lcom/sec/dmc/sic/android/property/SeriesProperty;
    invoke-virtual {v7, v4}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setSeriesID(I)V

    .line 3181
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v7}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3183
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3185
    new-instance v8, Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-direct {v8}, Lcom/sec/dmc/sic/android/property/GraphProperty;-><init>()V

    .line 3186
    .local v8, "temp":Lcom/sec/dmc/sic/android/property/GraphProperty;
    invoke-virtual {v8, v4}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setGraphID(I)V

    .line 3187
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v8}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3189
    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3191
    new-instance v9, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-direct {v9}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;-><init>()V

    .line 3192
    .local v9, "vMarkingPro":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    invoke-virtual {v9, v4}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setValueMarkingID(I)V

    .line 3193
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v9}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3194
    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3196
    new-instance v6, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-direct {v6}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;-><init>()V

    .line 3197
    .local v6, "nRangePro":Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
    invoke-virtual {v6, v4}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setNormalRangeID(I)V

    .line 3198
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNormalRangeVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v6}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3199
    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3201
    new-instance v2, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;-><init>()V

    .line 3202
    .local v2, "goalLinePro":Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    invoke-virtual {v2, v4}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalLineID(I)V

    .line 3203
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalLineVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v2}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3204
    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3206
    new-instance v3, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-direct {v3}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;-><init>()V

    .line 3207
    .local v3, "graphValuePro":Lcom/sec/dmc/sic/android/property/GraphValueProperty;
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setVisible(Z)V

    .line 3208
    invoke-virtual {v3, v4}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setSeriesID(I)V

    .line 3211
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setOffsetX(F)V

    .line 3212
    const/high16 v10, 0x41a00000    # 20.0f

    invoke-virtual {v3, v10}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setOffsetY(F)V

    .line 3213
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphValueVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v3}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3214
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3216
    new-instance v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;-><init>()V

    .line 3217
    .local v0, "eventIconPro":Lcom/sec/dmc/sic/android/property/EventIconProperty;
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setVisible(Z)V

    .line 3218
    invoke-virtual {v0, v4}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setSeriesID(I)V

    .line 3219
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setOffsetX(F)V

    .line 3220
    const/high16 v10, 0x42200000    # 40.0f

    invoke-virtual {v0, v10}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setOffsetY(F)V

    .line 3221
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v0}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3222
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3224
    new-instance v1, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;-><init>()V

    .line 3225
    .local v1, "goalEventIconsPro":Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setVisible(Z)V

    .line 3226
    invoke-virtual {v1, v4}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setSeriesID(I)V

    .line 3227
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setOffsetX(F)V

    .line 3228
    const/high16 v10, 0x42200000    # 40.0f

    invoke-virtual {v1, v10}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setOffsetY(F)V

    .line 3229
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v4, v1}, Ljava/util/Vector;->add(ILjava/lang/Object;)V

    .line 3230
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3178
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0
.end method

.method public deleteData(IID)V
    .locals 6
    .param p1, "dataType"    # I
    .param p2, "seriesID"    # I
    .param p3, "epochTimeTarget"    # D

    .prologue
    .line 3330
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->DeleteData(JIID)V

    .line 3331
    return-void
.end method

.method public deleteDatas(IIDD)V
    .locals 8
    .param p1, "dataType"    # I
    .param p2, "seriesID"    # I
    .param p3, "epochTimeFrom"    # D
    .param p5, "epochTimeTo"    # D

    .prologue
    .line 3336
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-wide v6, p5

    invoke-static/range {v0 .. v7}, Lcom/sec/dmc/sic/jni/ChartJNI;->DeleteDatas(JIIDD)V

    .line 3338
    return-void
.end method

.method protected extractImageFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "resPath"    # Ljava/lang/String;

    .prologue
    .line 1848
    const-string v3, "/"

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 1849
    .local v1, "file":I
    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 1851
    .local v0, "ext":I
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1853
    .local v2, "filename":Ljava/lang/String;
    return-object v2
.end method

.method public getChartProperty()Lcom/sec/dmc/sic/android/property/ChartProperty;
    .locals 1

    .prologue
    .line 3712
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    return-object v0
.end method

.method protected getChartType()I
    .locals 1

    .prologue
    .line 3519
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartType:I

    return v0
.end method

.method public getCustomHandler()Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;
    .locals 1

    .prologue
    .line 3782
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mCustomHandler:Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;

    return-object v0
.end method

.method public getCustomPopup()Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;
    .locals 1

    .prologue
    .line 3761
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->customPopup:Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;

    return-object v0
.end method

.method protected getDataType()I
    .locals 1

    .prologue
    .line 3511
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDataType:I

    return v0
.end method

.method protected getDrawableFromResString(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "drawableName"    # Ljava/lang/String;

    .prologue
    .line 1836
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1837
    const-string v2, "drawable"

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 1836
    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 1838
    .local v0, "resourceId":I
    if-nez v0, :cond_0

    .line 1839
    const/4 v1, 0x0

    .line 1841
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public getGoalLineProperty(I)Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 3671
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalLineVector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 3672
    const/4 v0, 0x0

    .line 3674
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGoalLineVector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    goto :goto_0
.end method

.method public getGraphProperty(I)Lcom/sec/dmc/sic/android/property/GraphProperty;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 3701
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphVector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 3702
    const/4 v0, 0x0

    .line 3704
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphVector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty;

    goto :goto_0
.end method

.method public getGridLineProperty()Lcom/sec/dmc/sic/android/property/GridLineProperty;
    .locals 1

    .prologue
    .line 3708
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    return-object v0
.end method

.method public getHandlerProperty()Lcom/sec/dmc/sic/android/property/HandlerProperty;
    .locals 1

    .prologue
    .line 3732
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    return-object v0
.end method

.method public getInfoProperty()Lcom/sec/dmc/sic/android/property/InfoPointProperty;
    .locals 1

    .prologue
    .line 3728
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    return-object v0
.end method

.method public getLegendProperty()Lcom/sec/dmc/sic/android/property/LegendProperty;
    .locals 1

    .prologue
    .line 3736
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    return-object v0
.end method

.method public getMaxY()F
    .locals 2

    .prologue
    .line 4504
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 4506
    const/4 v0, 0x0

    .line 4508
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetMaxY(J)F

    move-result v0

    goto :goto_0
.end method

.method public getMinY()F
    .locals 2

    .prologue
    .line 4496
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 4498
    const/4 v0, 0x0

    .line 4500
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetMinY(J)F

    move-result v0

    goto :goto_0
.end method

.method public getNormalRangeProperty(I)Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 3664
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNormalRangeVector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 3665
    const/4 v0, 0x0

    .line 3667
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNormalRangeVector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    goto :goto_0
.end method

.method public getReferenceView()Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRefView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    return-object v0
.end method

.method public getSeriesProperty(I)Lcom/sec/dmc/sic/android/property/SeriesProperty;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 3716
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesVector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 3717
    const/4 v0, 0x0

    .line 3719
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesVector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    goto :goto_0
.end method

.method public getTraceMode()Z
    .locals 1

    .prologue
    .line 3487
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    return v0
.end method

.method public getValueMarkingProperty(I)Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    .locals 4
    .param p1, "idx"    # I

    .prologue
    const/4 v2, 0x0

    .line 3678
    if-gez p1, :cond_1

    .line 3697
    :cond_0
    :goto_0
    return-object v2

    .line 3681
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesVector:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-gt p1, v3, :cond_0

    .line 3685
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ne p1, v2, :cond_3

    .line 3686
    new-instance v1, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;-><init>()V

    .line 3687
    .local v1, "temp1":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    invoke-virtual {v1, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setValueMarkingID(I)V

    .line 3688
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3697
    .end local v1    # "temp1":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v2, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    goto :goto_0

    .line 3689
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-le p1, v2, :cond_2

    .line 3690
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v0

    .local v0, "i":I
    :goto_1
    if-ge v0, p1, :cond_2

    .line 3691
    new-instance v1, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;-><init>()V

    .line 3692
    .restart local v1    # "temp1":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    invoke-virtual {v1, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setValueMarkingID(I)V

    .line 3693
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3690
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getXAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 3528
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 3529
    const/4 v0, 0x0

    .line 3531
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/AxisProperty;

    goto :goto_0
.end method

.method public getYAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 3536
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 3537
    const/4 v0, 0x0

    .line 3539
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/sic/android/property/AxisProperty;

    goto :goto_0
.end method

.method protected initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 181
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 474
    .line 475
    sget-object v177, Lcom/sec/dmc/sic/R$styleable;->SicChartView:[I

    .line 474
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v171

    .line 477
    .local v171, "typedArray":Landroid/content/res/TypedArray;
    if-nez v171, :cond_0

    .line 1187
    :goto_0
    return-void

    .line 487
    :cond_0
    const/16 v177, 0x12

    const/16 v178, 0x1

    .line 486
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v177

    move/from16 v0, v177

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    .line 491
    const/16 v177, 0x10

    const/16 v178, 0x1

    .line 490
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v141

    .line 493
    .local v141, "numOfXAxis":I
    const/16 v177, 0x11

    const/16 v178, 0x1

    .line 492
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v142

    .line 495
    .local v142, "numOfYAxis":I
    new-instance v65, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-object/from16 v0, v65

    move/from16 v1, v141

    move/from16 v2, v142

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;-><init>(II)V

    .line 500
    .local v65, "chartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    const/16 v177, 0x0

    const/16 v178, 0x64

    .line 499
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v172, v0

    .line 502
    .local v172, "width":F
    const/16 v177, 0x1

    const/16 v178, 0x64

    .line 501
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v108, v0

    .line 503
    .local v108, "height":F
    move-object/from16 v0, v65

    move/from16 v1, v172

    move/from16 v2, v108

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setWidthHeight(FF)V

    .line 506
    const/16 v177, 0x2

    const/16 v178, 0xa

    .line 505
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v166, v0

    .line 508
    .local v166, "posX":F
    const/16 v177, 0x3

    const/16 v178, 0xa

    .line 507
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v167, v0

    .line 509
    .local v167, "posY":F
    move-object/from16 v0, v65

    move/from16 v1, v166

    move/from16 v2, v167

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPosition(FF)V

    .line 512
    const/16 v177, 0x4

    const/16 v178, 0xa

    .line 511
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v144, v0

    .line 514
    .local v144, "paddingLeft":F
    const/16 v177, 0x5

    const/16 v178, 0xa

    .line 513
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v145, v0

    .line 516
    .local v145, "paddingRight":F
    const/16 v177, 0x6

    const/16 v178, 0xa

    .line 515
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v146, v0

    .line 518
    .local v146, "paddingTop":F
    const/16 v177, 0x7

    const/16 v178, 0xa

    .line 517
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v143, v0

    .line 519
    .local v143, "paddingBottom":F
    move-object/from16 v0, v65

    move/from16 v1, v144

    move/from16 v2, v145

    move/from16 v3, v146

    move/from16 v4, v143

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPadding(FFFF)V

    .line 523
    const/16 v177, 0xf

    const/16 v178, 0x0

    .line 522
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v140

    .line 524
    .local v140, "markingLineBase":I
    move-object/from16 v0, v65

    move/from16 v1, v140

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setMarkingLineBase(I)V

    .line 527
    const/16 v177, 0x9

    const/16 v178, -0x1

    .line 526
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v64

    .line 528
    .local v64, "chartBackgroundColor":I
    move-object/from16 v0, v65

    move/from16 v1, v64

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setChartBackgroundColor(I)V

    .line 531
    const/16 v177, 0x8

    const/16 v178, -0x1

    .line 530
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v67

    .line 532
    .local v67, "graphBackgroundColor":I
    move-object/from16 v0, v65

    move/from16 v1, v67

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundColor(I)V

    .line 535
    const/16 v177, 0xa

    const/16 v178, 0x0

    .line 534
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v68

    .line 536
    .local v68, "graphBackgroundImageResID":I
    if-eqz v68, :cond_1

    .line 538
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 539
    move-object/from16 v0, v177

    move/from16 v1, v68

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    .line 538
    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    .line 540
    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v177

    .line 538
    move-object/from16 v0, v65

    move-object/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 544
    :cond_1
    const/16 v177, 0xb

    const/16 v178, 0x1

    .line 543
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v71, v0

    .line 545
    .local v71, "graphSeparatorWidth":F
    move-object/from16 v0, v65

    move/from16 v1, v71

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphSeparatorWidth(F)V

    .line 548
    const/16 v177, 0xc

    const v178, 0x1010101

    .line 547
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v69

    .line 549
    .local v69, "graphSeparatorColor":I
    move-object/from16 v0, v65

    move/from16 v1, v69

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphSeparatorColor(I)V

    .line 552
    const/16 v177, 0xd

    const/16 v178, 0x0

    .line 551
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v72

    .line 553
    .local v72, "graphTitleVisible":Z
    move-object/from16 v0, v65

    move/from16 v1, v72

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphTitleVisible(Z)V

    .line 556
    const/16 v177, 0xe

    const/16 v178, 0x0

    .line 555
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v70

    .line 557
    .local v70, "graphSeparatorVisible":Z
    move-object/from16 v0, v65

    move/from16 v1, v70

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGraphSeparatorVisible(Z)V

    .line 585
    const/16 v177, 0x8b

    const/16 v178, 0x1

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v29

    .line 586
    .local v29, "XAxisLineVislble":Z
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v29

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisLineVisible(ZI)V

    .line 588
    const/16 v177, 0x8c

    const/16 v178, 0x1

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v31

    .line 589
    .local v31, "XAxisTextVislble":Z
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v31

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextVisible(ZI)V

    .line 591
    const/16 v177, 0x8d

    const/16 v178, 0x1

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v30

    .line 592
    .local v30, "XAxisMarkingVislble":Z
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v30

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisMarkingVisible(ZI)V

    .line 594
    const/16 v177, 0x8e

    const/high16 v178, -0x1000000

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v26

    .line 595
    .local v26, "XAxisColor":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v26

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisColor(II)V

    .line 597
    const/16 v177, 0x8f

    const/16 v178, 0x4

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v32, v0

    .line 598
    .local v32, "XAxisThickness":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v32

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisWidth(FI)V

    .line 600
    const/16 v177, 0x90

    move-object/from16 v0, v171

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v33

    .line 601
    .local v33, "XAxisTitle":Ljava/lang/String;
    if-eqz v33, :cond_2

    .line 602
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move-object/from16 v1, v33

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTitle(Ljava/lang/String;I)V

    .line 605
    :cond_2
    const/16 v177, 0x91

    const/16 v178, 0x1

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v34

    .line 606
    .local v34, "XAxisTitleAlign":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v34

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTitleAlign(II)V

    .line 608
    const/16 v177, 0x92

    const/16 v178, 0x3

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v27

    .line 609
    .local v27, "XAxisDirection":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v27

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisDirection(II)V

    .line 611
    const/16 v177, 0x93

    const/16 v178, 0x3

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v28

    .line 612
    .local v28, "XAxisLabelAlign":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v28

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisLabelAlign(II)V

    .line 615
    const/16 v177, 0x94

    const/16 v178, 0x14

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v36, v0

    .line 616
    .local v36, "XAxisTitleSpacingAxis":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v36

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTitleSpacingAxis(FI)V

    .line 618
    const/16 v177, 0x95

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v35, v0

    .line 619
    .local v35, "XAxisTitleSpacingAlign":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v35

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTitleSpacingAlign(FI)V

    .line 621
    const/16 v177, 0x9d

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v37

    .line 624
    .local v37, "XAxisUseManualMarking":Z
    const/16 v177, 0x97

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v43

    .line 625
    .local v43, "XAxis_visibleIndexCount":I
    const/16 v177, 0x98

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v168

    .line 626
    .local v168, "resID":I
    const/16 v169, 0x0

    .line 627
    .local v169, "strArray":[Ljava/lang/String;
    const/16 v44, 0x0

    .line 628
    .local v44, "XAxis_visibleIndex_array":[I
    const/high16 v42, -0x31000000

    .line 629
    .local v42, "XAxis_visibleIndex":F
    if-eqz v168, :cond_13

    .line 630
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    move-object/from16 v0, v177

    move/from16 v1, v168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v169

    .line 631
    move-object/from16 v0, p0

    move-object/from16 v1, v169

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToIntegerArray([Ljava/lang/String;)[I

    move-result-object v44

    .line 638
    :goto_1
    const/16 v177, 0x99

    const/16 v178, 0x1e

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v40

    .line 639
    .local v40, "XAxis_separatorSpacingTop":I
    move-object/from16 v0, v65

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingTop(I)V

    .line 642
    const/16 v177, 0x9a

    const/16 v178, 0x1e

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v39

    .line 643
    .local v39, "XAxis_separatorSpacingLine":I
    move-object/from16 v0, v65

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorTextSpacingLine(I)V

    .line 646
    const/16 v177, 0x9b

    const/high16 v178, 0x3f800000    # 1.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v38

    .line 647
    .local v38, "XAxis_separatorLineThickness":F
    move-object/from16 v0, v65

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setSeparatorLineThickness(F)V

    .line 650
    const/16 v177, 0x9c

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v41

    .line 651
    .local v41, "XAxis_textSpace":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v41

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextSpace(FI)V

    .line 660
    const/16 v177, 0x9e

    const/16 v178, 0x1

    .line 659
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v48

    .line 661
    .local v48, "YAxisLineVislble":Z
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v48

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLineVisible(ZI)V

    .line 664
    const/16 v177, 0x9f

    const/16 v178, 0x1

    .line 663
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v50

    .line 665
    .local v50, "YAxisTextVislble":Z
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v50

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextVisible(ZI)V

    .line 668
    const/16 v177, 0xa0

    const/16 v178, 0x1

    .line 667
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v49

    .line 669
    .local v49, "YAxisMarkingVislble":Z
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v49

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisMarkingVisible(ZI)V

    .line 672
    const/16 v177, 0xa1

    const/high16 v178, -0x1000000

    .line 671
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v45

    .line 673
    .local v45, "YAxisColor":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v45

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisColor(II)V

    .line 677
    const/16 v177, 0xa2

    const/16 v178, 0x4

    .line 676
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v51, v0

    .line 678
    .local v51, "YAxisThickness":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v51

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisWidth(FI)V

    .line 681
    const/16 v177, 0xa3

    move-object/from16 v0, v171

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v52

    .line 682
    .local v52, "YAxisTitle":Ljava/lang/String;
    if-eqz v52, :cond_3

    .line 683
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move-object/from16 v1, v52

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTitle(Ljava/lang/String;I)V

    .line 687
    :cond_3
    const/16 v177, 0xa4

    const/16 v178, 0x2

    .line 686
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v53

    .line 688
    .local v53, "YAxisTitleAlign":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v53

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTitleAlign(II)V

    .line 690
    const/16 v177, 0xa5

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v46

    .line 691
    .local v46, "YAxisDirection":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v46

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisDirection(II)V

    .line 693
    const/16 v177, 0xa6

    const/16 v178, 0x3

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v47

    .line 694
    .local v47, "YAxisLabelAlign":I
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v47

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisLabelAlign(II)V

    .line 698
    const/16 v177, 0xa7

    .line 699
    const/16 v178, 0x14

    .line 697
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v55, v0

    .line 700
    .local v55, "YAxisTitleSpacingAxis":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v55

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTitleSpacingAxis(FI)V

    .line 704
    const/16 v177, 0xa8

    .line 705
    const/16 v178, 0x0

    .line 703
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v54, v0

    .line 706
    .local v54, "YAxisTitleSpacingAlign":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v54

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTitleSpacingAlign(FI)V

    .line 708
    const/16 v177, 0xb2

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v56

    .line 709
    .local v56, "YAxisUseManualMarking":Z
    const/16 v177, 0xaa

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v62

    .line 711
    .local v62, "YAxis_visibleIndexCount":I
    const/16 v177, 0xab

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v168

    .line 712
    const/16 v169, 0x0

    .line 713
    const/16 v63, 0x0

    .line 714
    .local v63, "YAxis_visibleIndex_array":[I
    const/high16 v61, -0x80000000

    .line 715
    .local v61, "YAxis_visibleIndex":I
    if-eqz v168, :cond_14

    .line 716
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    move-object/from16 v0, v177

    move/from16 v1, v168

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v169

    .line 717
    move-object/from16 v0, p0

    move-object/from16 v1, v169

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToIntegerArray([Ljava/lang/String;)[I

    move-result-object v63

    .line 723
    :goto_2
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v56

    move/from16 v2, v62

    move-object/from16 v3, v63

    move/from16 v4, v177

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisUseManualMarking(ZI[II)V

    .line 726
    const/16 v177, 0xac

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v59

    .line 728
    .local v59, "YAxis_separatorSpacingTop":I
    const/16 v177, 0xad

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v58

    .line 730
    .local v58, "YAxis_separatorSpacingLine":I
    const/16 v177, 0xae

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v57

    .line 733
    .local v57, "YAxis_separatorLineThickness":F
    const/16 v177, 0xb1

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v60

    .line 734
    .local v60, "YAxis_textSpace":F
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move/from16 v1, v60

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextSpace(FI)V

    .line 736
    const/16 v177, 0xb0

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v8

    .line 738
    .local v8, "CustomYLabelStrsCount":I
    const/16 v177, 0xaf

    const/16 v178, 0x0

    .line 737
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 739
    .local v7, "CustomYLabelStringsID":I
    const/4 v6, 0x0

    .line 740
    .local v6, "CustomYLabelStrings":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 741
    .local v5, "CustomYLabel":Ljava/lang/String;
    if-eqz v7, :cond_15

    .line 742
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    move-object/from16 v0, v177

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    .line 744
    move-object/from16 v0, v65

    invoke-virtual {v0, v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisCustomLabel([Ljava/lang/String;I)V

    .line 768
    :cond_4
    :goto_3
    const/16 v177, 0x84

    const/16 v178, 0x1

    .line 767
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v84

    .line 769
    .local v84, "gridLineVisible":Z
    move-object/from16 v0, v65

    move/from16 v1, v84

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGridVisible(Z)V

    .line 772
    const/16 v177, 0x85

    const/16 v178, 0x2

    .line 771
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v82, v0

    .line 773
    .local v82, "gridLineThickness":F
    move-object/from16 v0, v65

    move/from16 v1, v82

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGridWidth(F)V

    .line 776
    const/16 v177, 0x86

    const/high16 v178, -0x1000000

    .line 775
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v78

    .line 777
    .local v78, "gridLineColor":I
    move-object/from16 v0, v65

    move/from16 v1, v78

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGridColor(I)V

    .line 780
    const/16 v177, 0x87

    const/16 v178, 0x0

    .line 779
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v83

    .line 782
    .local v83, "gridLineUseManualMarking":Z
    const/16 v177, 0x88

    const/16 v178, 0x0

    .line 781
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v79

    .line 785
    .local v79, "gridLineManualMarkingCnt":I
    const/16 v177, 0x89

    .line 786
    const/16 v178, 0x0

    .line 784
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v80

    .line 787
    .local v80, "gridLineManualMarkingIndexArrayResID":I
    const/16 v81, 0x0

    .line 788
    .local v81, "gridLineManualMarkingVisibleIndices":[I
    if-eqz v80, :cond_5

    .line 789
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 790
    move-object/from16 v0, v177

    move/from16 v1, v80

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v81

    .line 792
    :cond_5
    if-eqz v83, :cond_6

    const/16 v177, 0x1

    move/from16 v0, v79

    move/from16 v1, v177

    if-le v0, v1, :cond_6

    .line 793
    move-object/from16 v0, v65

    move/from16 v1, v83

    move/from16 v2, v79

    move-object/from16 v3, v81

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setGridUseManualMarking(ZI[I)V

    .line 817
    :cond_6
    const/16 v177, 0x49

    const/16 v178, 0x1

    .line 816
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v107

    .line 820
    .local v107, "handler_visible":Z
    const/16 v177, 0x4a

    const/16 v178, 0x0

    .line 819
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v90

    .line 821
    .local v90, "handlerItemShapeType":I
    move-object/from16 v0, v65

    move/from16 v1, v90

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemShapeType(I)V

    .line 824
    const/16 v177, 0x4b

    const/16 v178, 0x1

    .line 823
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v86

    .line 826
    .local v86, "handlerItemDirection":I
    const/16 v177, 0x4c

    const/16 v178, -0x1

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v105, v0

    .line 827
    .local v105, "handler_item_width":F
    move-object/from16 v0, v65

    move/from16 v1, v105

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemWidth(F)V

    .line 829
    const/16 v177, 0x4d

    const/16 v178, -0x1

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v103, v0

    .line 830
    .local v103, "handler_item_height":F
    move-object/from16 v0, v65

    move/from16 v1, v103

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemHeight(F)V

    .line 832
    const/16 v177, 0x4e

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v104, v0

    .line 833
    .local v104, "handler_item_offset":F
    move-object/from16 v0, v65

    move/from16 v1, v104

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemOffset(F)V

    .line 836
    const/16 v177, 0x4f

    const/16 v178, -0x1

    .line 835
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v85

    .line 837
    .local v85, "handlerItemColor":I
    move-object/from16 v0, v65

    move/from16 v1, v85

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemColor(I)V

    .line 840
    const/16 v177, 0x50

    const/16 v178, 0x0

    .line 839
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v87

    .line 841
    .local v87, "handlerItemImageResID":I
    if-eqz v87, :cond_7

    .line 843
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 844
    move-object/from16 v0, v177

    move/from16 v1, v87

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    .line 843
    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    .line 845
    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v177

    .line 843
    move-object/from16 v0, v65

    move-object/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemImage(Landroid/graphics/Bitmap;)V

    .line 849
    :cond_7
    const/16 v177, 0x51

    const/16 v178, 0x2

    .line 848
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v91, v0

    .line 850
    .local v91, "handlerItemStrokeWidth":F
    move-object/from16 v0, v65

    move/from16 v1, v91

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemStrokeWidth(F)V

    .line 853
    const/16 v177, 0x52

    const/high16 v178, -0x1000000

    .line 852
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v88

    .line 856
    .local v88, "handlerItemPressedColor":I
    const/16 v177, 0x53

    const/16 v178, 0x0

    .line 855
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v89

    .line 857
    .local v89, "handlerItemPressedImageResID":I
    if-eqz v89, :cond_8

    .line 859
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 860
    move-object/from16 v0, v177

    move/from16 v1, v89

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    .line 859
    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    .line 861
    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v177

    .line 859
    move-object/from16 v0, v65

    move-object/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemPressedColor(Landroid/graphics/Bitmap;)V

    .line 866
    :cond_8
    const/16 v177, 0x54

    const/16 v178, 0x0

    .line 865
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v97

    .line 867
    .local v97, "handlerLineShapeType":I
    move-object/from16 v0, v65

    move/from16 v1, v97

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineType(I)V

    .line 870
    const/16 v177, 0x55

    const/16 v178, 0x2

    .line 869
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v98, v0

    .line 871
    .local v98, "handlerLineThickness":F
    move-object/from16 v0, v65

    move/from16 v1, v98

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineWidth(F)V

    .line 874
    const/16 v177, 0x56

    const/high16 v178, -0x1000000

    .line 873
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v93

    .line 875
    .local v93, "handlerLineColor":I
    move-object/from16 v0, v65

    move/from16 v1, v93

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineColor(I)V

    .line 878
    const/16 v177, 0x57

    const/16 v178, 0x0

    .line 877
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v94

    .line 879
    .local v94, "handlerLineImageResID":I
    if-eqz v94, :cond_9

    .line 881
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 882
    move-object/from16 v0, v177

    move/from16 v1, v94

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    .line 881
    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    .line 883
    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v177

    .line 881
    move-object/from16 v0, v65

    move-object/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLineImage(Landroid/graphics/Bitmap;)V

    .line 887
    :cond_9
    const/16 v177, 0x58

    const/high16 v178, -0x1000000

    .line 886
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v95

    .line 891
    .local v95, "handlerLinePressedColor":I
    const/16 v177, 0x59

    const/16 v178, 0x0

    .line 890
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v96

    .line 892
    .local v96, "handlerLinePressedImageResID":I
    if-eqz v96, :cond_a

    .line 894
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 895
    move-object/from16 v0, v177

    move/from16 v1, v96

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    .line 894
    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    .line 896
    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v177

    .line 894
    move-object/from16 v0, v65

    move-object/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerLinePressedColor(Landroid/graphics/Bitmap;)V

    .line 901
    :cond_a
    const/16 v177, 0x5a

    const/16 v178, 0x1

    .line 900
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v106

    .line 902
    .local v106, "handler_tooltip_visible":Z
    move-object/from16 v0, v65

    move/from16 v1, v106

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipEnable(Z)V

    .line 905
    const/16 v177, 0x5b

    .line 906
    const/16 v178, -0x1

    .line 905
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v99

    .line 907
    .local v99, "handlerTooltipColor":I
    move-object/from16 v0, v65

    move/from16 v1, v99

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipColor(I)V

    .line 910
    const/16 v177, 0x5c

    const/16 v178, 0x0

    .line 909
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v100

    .line 911
    .local v100, "handlerTooltipImageResID":I
    if-eqz v100, :cond_b

    .line 912
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 913
    move-object/from16 v0, v177

    move/from16 v1, v100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v177

    .line 912
    move-object/from16 v0, v65

    move-object/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipImage(Landroid/graphics/Bitmap;)V

    .line 917
    :cond_b
    const/16 v177, 0x5d

    .line 918
    const/high16 v178, -0x1000000

    .line 916
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v101

    .line 919
    .local v101, "handlerTooltipStrokeColor":I
    move-object/from16 v0, v65

    move/from16 v1, v101

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipStrokeColor(I)V

    .line 922
    const/16 v177, 0x5e

    const/16 v178, 0x2

    .line 921
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v102, v0

    .line 923
    .local v102, "handlerTooltipStrokeWidth":F
    move-object/from16 v0, v65

    move/from16 v1, v102

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipStrokeWidth(F)V

    .line 928
    const/16 v177, 0x60

    const/16 v178, 0x1

    .line 927
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v164

    .line 930
    .local v164, "popupVisible":Z
    const/16 v177, 0x5f

    const/16 v178, 0x0

    .line 929
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v161

    .line 932
    .local v161, "popupShapeType":I
    const/16 v177, 0x61

    const/16 v178, 0x1e

    .line 931
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v165, v0

    .line 934
    .local v165, "popupWidth":F
    const/16 v177, 0x62

    const/16 v178, 0x1e

    .line 933
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v158, v0

    .line 936
    .local v158, "popupHeight":F
    const/16 v177, 0x63

    const/16 v178, 0xf

    .line 935
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v160, v0

    .line 938
    .local v160, "popupRadius":F
    const/16 v177, 0x64

    const/16 v178, -0x1

    .line 937
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v157

    .line 940
    .local v157, "popupColor":I
    const/16 v177, 0x68

    const/16 v178, 0x0

    .line 939
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v159

    .line 942
    .local v159, "popupImageResID":I
    const/16 v177, 0x66

    const/high16 v178, -0x1000000

    .line 941
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v162

    .line 944
    .local v162, "popupStrokeColor":I
    const/16 v177, 0x65

    const/16 v178, 0x2

    .line 943
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v163, v0

    .line 946
    .local v163, "popupStrokeWidth":F
    move-object/from16 v0, v65

    move/from16 v1, v164

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupVisible(Z)V

    .line 947
    move-object/from16 v0, v65

    move/from16 v1, v161

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupShapeType(I)V

    .line 948
    move-object/from16 v0, v65

    move/from16 v1, v165

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupStrokeWidth(F)V

    .line 949
    move-object/from16 v0, v65

    move/from16 v1, v165

    move/from16 v2, v158

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupSize(FF)V

    .line 950
    move-object/from16 v0, v65

    move/from16 v1, v160

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupSize(F)V

    .line 951
    move-object/from16 v0, v65

    move/from16 v1, v157

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupColor(I)V

    .line 952
    if-eqz v159, :cond_c

    .line 953
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 954
    move-object/from16 v0, v177

    move/from16 v1, v159

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v177

    .line 953
    move-object/from16 v0, v65

    move-object/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupImage(Landroid/graphics/Bitmap;)V

    .line 957
    :cond_c
    move-object/from16 v0, v65

    move/from16 v1, v162

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupStrokeColor(I)V

    .line 958
    move-object/from16 v0, v65

    move/from16 v1, v163

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupStrokeWidth(F)V

    .line 962
    const/16 v177, 0x70

    const/16 v178, 0x1

    .line 961
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v121

    .line 963
    .local v121, "legendItemVisible":Z
    move-object/from16 v0, v65

    move/from16 v1, v121

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendVisible(Z)V

    .line 966
    const/16 v177, 0x71

    const/16 v178, 0x3

    .line 965
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v111

    .line 967
    .local v111, "legendItemDirection":I
    move-object/from16 v0, v65

    move/from16 v1, v111

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendDirection(I)V

    .line 970
    const/16 v177, 0x72

    const/16 v178, 0x64

    .line 969
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v126, v0

    .line 971
    .local v126, "legnedItemWidth":F
    move-object/from16 v0, v65

    move/from16 v1, v126

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendItemWidth(F)V

    .line 974
    const/16 v177, 0x73

    const/16 v178, 0x14

    .line 973
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v123, v0

    .line 975
    .local v123, "legnedItemHeight":F
    move-object/from16 v0, v65

    move/from16 v1, v123

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendItemHeight(F)V

    .line 978
    const/16 v177, 0x74

    const/16 v178, 0x32

    .line 977
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v125, v0

    .line 979
    .local v125, "legnedItemWSpace":F
    move-object/from16 v0, v65

    move/from16 v1, v125

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendItemWidthSpace(F)V

    .line 982
    const/16 v177, 0x75

    const/16 v178, 0x14

    .line 981
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v122, v0

    .line 983
    .local v122, "legnedItemHSpace":F
    move-object/from16 v0, v65

    move/from16 v1, v122

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendItemHeightSpace(F)V

    .line 986
    const/16 v177, 0x76

    const/16 v178, 0x5

    .line 985
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v124, v0

    .line 987
    .local v124, "legnedItemInSpace":F
    move-object/from16 v0, v65

    move/from16 v1, v124

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendItemInnerSpace(F)V

    .line 990
    const/16 v177, 0x77

    .line 991
    const v178, -0x333334

    .line 989
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v112

    .line 992
    .local v112, "legendItemDisableColor":I
    move-object/from16 v0, v65

    move/from16 v1, v112

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendDisableColor(I)V

    .line 995
    const/16 v177, 0x78

    const/16 v178, 0x0

    .line 994
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v119

    .line 996
    .local v119, "legendItemImageArrayResID":I
    const/16 v120, 0x0

    .line 997
    .local v120, "legendItemImageStringArray":[Ljava/lang/String;
    const/16 v118, 0x0

    .line 998
    .local v118, "legendItemImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/16 v117, 0x0

    .line 999
    .local v117, "legendItemImage":Landroid/graphics/Bitmap;
    if-eqz v119, :cond_d

    .line 1001
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    move-object/from16 v0, v177

    move/from16 v1, v119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v120

    .line 1003
    move-object/from16 v0, p0

    move-object/from16 v1, v120

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBitmapArrayList([Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v118

    .line 1018
    :cond_d
    if-eqz v118, :cond_e

    .line 1019
    move-object/from16 v0, v65

    move-object/from16 v1, v118

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendItemBitmapArray(Ljava/util/ArrayList;)V

    .line 1023
    :cond_e
    const/16 v177, 0x79

    const/16 v178, 0x0

    .line 1022
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v115

    .line 1024
    .local v115, "legendItemDisableImageArrayResID":I
    const/16 v116, 0x0

    .line 1025
    .local v116, "legendItemDisableImageStringArray":[Ljava/lang/String;
    const/16 v114, 0x0

    .line 1026
    .local v114, "legendItemDisableImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    const/16 v113, 0x0

    .line 1027
    .local v113, "legendItemDisableImage":Landroid/graphics/Bitmap;
    if-eqz v115, :cond_f

    .line 1029
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 1030
    move-object/from16 v0, v177

    move/from16 v1, v115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v116

    .line 1031
    move-object/from16 v0, p0

    move-object/from16 v1, v116

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBitmapArrayList([Ljava/lang/String;)Ljava/util/ArrayList;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v114

    .line 1043
    :cond_f
    if-eqz v114, :cond_10

    .line 1045
    move-object/from16 v0, v65

    move-object/from16 v1, v114

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setLegendDisableItemBitmapArray(Ljava/util/ArrayList;)V

    .line 1049
    :cond_10
    new-instance v110, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    invoke-direct/range {v110 .. v110}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;-><init>()V

    .line 1050
    .local v110, "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    const/16 v177, 0x69

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v23

    .line 1051
    .local v23, "Interaction_mScaleZoomEnable":Z
    move-object/from16 v0, v110

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 1052
    const/16 v177, 0x6a

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v19

    .line 1053
    .local v19, "Interaction_mDataZoomEnable":Z
    move-object/from16 v0, v110

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInteractionEnabled(Z)V

    .line 1054
    const/16 v177, 0x6b

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v22

    .line 1055
    .local v22, "Interaction_mPartialZoomEnable":Z
    move-object/from16 v0, v110

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 1056
    const/16 v177, 0x6c

    const/high16 v178, 0x3f800000    # 1.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v25

    .line 1058
    .local v25, "Interaction_mScaleZoomOutRate":F
    const/16 v177, 0x6d

    const/high16 v178, 0x40000000    # 2.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v24

    .line 1059
    .local v24, "Interaction_mScaleZoomInRate":F
    move-object/from16 v0, v110

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInRate(F)V

    .line 1060
    const/16 v177, 0x6e

    const/high16 v178, 0x40000000    # 2.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v21

    .line 1061
    .local v21, "Interaction_mDataZoomOutRate":F
    move-object/from16 v0, v110

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomOutRate(F)V

    .line 1062
    const/16 v177, 0x6f

    const/high16 v178, 0x40000000    # 2.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v20

    .line 1063
    .local v20, "Interaction_mDataZoomInRate":F
    move-object/from16 v0, v110

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setDataZoomInRate(F)V

    .line 1065
    move-object/from16 v0, p0

    move-object/from16 v1, v110

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 1070
    const/16 v177, 0x29

    const v178, 0x3e99999a    # 0.3f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v147

    .line 1071
    .local v147, "pieHoleRadiusRatio":F
    const/16 v177, 0x2a

    const v178, 0x3f4ccccd    # 0.8f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v155

    .line 1072
    .local v155, "pieValueCenterRadiusRatio":F
    const/16 v177, 0x2b

    const v178, 0x3f333333    # 0.7f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v154

    .line 1073
    .local v154, "pieRadiusRatio":F
    const/16 v177, 0x2c

    const v178, 0x3f4ccccd    # 0.8f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v150

    .line 1074
    .local v150, "pieLabelCenterRadiusRatio":F
    const/16 v177, 0x2d

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v151

    .line 1075
    .local v151, "pieLabelOffsetX":F
    const/16 v177, 0x2e

    const/high16 v178, -0x3e380000    # -25.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v152

    .line 1076
    .local v152, "pieLabelOffsetY":F
    const/16 v177, 0x2f

    const/16 v178, 0x1

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v153

    .line 1077
    .local v153, "pieLabelVisible":Z
    const/16 v177, 0x30

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v148

    .line 1078
    .local v148, "pieItemColorIndicesID":I
    const/16 v177, 0x31

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v149

    .line 1082
    .local v149, "pieItemColorValuesID":I
    const/16 v177, 0x32

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v138

    .line 1083
    .local v138, "mStackedBarItemNameIndicesID":I
    const/16 v177, 0x33

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v139

    .line 1086
    .local v139, "mStackedBarItemNamesID":I
    const/16 v177, 0x34

    const/high16 v178, 0x41a00000    # 20.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v137

    .line 1087
    .local v137, "mCandleShadowWidth":F
    const/16 v177, 0x35

    const/high16 v178, 0x41a00000    # 20.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v136

    .line 1088
    .local v136, "mCandleShadowThickness":F
    const/16 v177, 0x36

    const/high16 v178, -0x1000000

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v135

    .line 1089
    .local v135, "mCandleShadowColor":I
    const/16 v177, 0x37

    const/high16 v178, 0x41f00000    # 30.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v129

    .line 1090
    .local v129, "mCandleBodyWidth":F
    const/16 v177, 0x38

    const/high16 v178, -0x10000

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v128

    .line 1091
    .local v128, "mCandleBodyIncreaseColor":I
    const/16 v177, 0x39

    const v178, -0xffff01

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v127

    .line 1092
    .local v127, "mCandleBodyDecreaseColor":I
    const/16 v177, 0x3a

    const/high16 v178, 0x40a00000    # 5.0f

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v134

    .line 1093
    .local v134, "mCandleLineThickness":F
    const/16 v177, 0x3b

    const/high16 v178, -0x1000000

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v131

    .line 1094
    .local v131, "mCandleLineHighColor":I
    const/16 v177, 0x3c

    const/high16 v178, -0x1000000

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v132

    .line 1095
    .local v132, "mCandleLineLowColor":I
    const/16 v177, 0x3d

    const/high16 v178, -0x1000000

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v133

    .line 1096
    .local v133, "mCandleLineOpenColor":I
    const/16 v177, 0x3e

    const/high16 v178, -0x1000000

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v130

    .line 1099
    .local v130, "mCandleLineCloseColor":I
    const/16 v177, 0x3f

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v12

    .line 1100
    .local v12, "EventIcon_Visible":Z
    const/16 v177, 0x40

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v11

    .line 1101
    .local v11, "EventIcon_OffsetX":F
    const/16 v177, 0x41

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v13

    .line 1102
    .local v13, "EventIcon_mOffsetY":F
    const/16 v177, 0x42

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    .line 1103
    .local v9, "EventIcon_BitmapListID":I
    const/16 v177, 0x43

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v10

    .line 1106
    .local v10, "EventIcon_NumBitmaps":I
    const/16 v177, 0x44

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v18

    .line 1107
    .local v18, "GoalEventIcon_Visible":Z
    const/16 v177, 0x45

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v15

    .line 1108
    .local v15, "GoalEventIcon_OffsetX":F
    const/16 v177, 0x46

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v16

    .line 1109
    .local v16, "GoalEventIcon_OffsetY":F
    const/16 v177, 0x47

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v17

    .line 1110
    .local v17, "GoalEventIcon_Value":F
    const/16 v177, 0x48

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v14

    .line 1116
    .local v14, "GoalEventIcon_Bitmap":I
    const/16 v177, 0x7f

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v77

    .line 1117
    .local v77, "graphValue_Visible":Z
    const/16 v177, 0x80

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v73

    .line 1118
    .local v73, "graphValue_OffsetX":F
    const/16 v177, 0x81

    const/16 v178, 0x0

    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v74

    .line 1119
    .local v74, "graphValue_OffsetY":F
    const/16 v177, 0x82

    move-object/from16 v0, v171

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v76

    .line 1120
    .local v76, "graphValue_Prefix":Ljava/lang/String;
    const/16 v177, 0x83

    move-object/from16 v0, v171

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v75

    .line 1123
    .local v75, "graphValue_Postfix":Ljava/lang/String;
    invoke-virtual/range {v171 .. v171}, Landroid/content/res/TypedArray;->recycle()V

    .line 1126
    new-instance v174, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v174 .. v174}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1127
    .local v174, "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v177, 0x41c80000    # 25.0f

    move-object/from16 v0, v174

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1128
    const/16 v177, 0x1

    move-object/from16 v0, v174

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1129
    const/16 v177, 0xff

    const/16 v178, 0x4c

    const/16 v179, 0x4c

    const/16 v180, 0x4c

    move-object/from16 v0, v174

    move/from16 v1, v177

    move/from16 v2, v178

    move/from16 v3, v179

    move/from16 v4, v180

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1130
    const/16 v177, 0x0

    move-object/from16 v0, v174

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1131
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move-object/from16 v1, v174

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1132
    const/16 v177, 0x2

    move/from16 v0, v141

    move/from16 v1, v177

    if-ne v0, v1, :cond_11

    .line 1134
    new-instance v173, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v173 .. v173}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1135
    .local v173, "xRightTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v177, 0x41c80000    # 25.0f

    move-object/from16 v0, v173

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1136
    const/16 v177, 0x1

    move-object/from16 v0, v173

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1137
    const/16 v177, 0xff

    const/16 v178, 0x4c

    const/16 v179, 0x4c

    const/16 v180, 0x4c

    move-object/from16 v0, v173

    move/from16 v1, v177

    move/from16 v2, v178

    move/from16 v3, v179

    move/from16 v4, v180

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1138
    const/16 v177, 0x0

    move-object/from16 v0, v173

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1139
    const/16 v177, 0x1

    move-object/from16 v0, v65

    move-object/from16 v1, v173

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1143
    .end local v173    # "xRightTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_11
    new-instance v175, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v175 .. v175}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1145
    .local v175, "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v177, 0x420c0000    # 35.0f

    move-object/from16 v0, v175

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1146
    const/16 v177, 0x1

    move-object/from16 v0, v175

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1147
    const/16 v177, 0xff

    const/16 v178, 0x4c

    const/16 v179, 0x4c

    const/16 v180, 0x4c

    move-object/from16 v0, v175

    move/from16 v1, v177

    move/from16 v2, v178

    move/from16 v3, v179

    move/from16 v4, v180

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1148
    const/16 v177, 0x0

    move-object/from16 v0, v175

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1149
    const/16 v177, 0x0

    move-object/from16 v0, v65

    move-object/from16 v1, v175

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1150
    const/16 v177, 0x2

    move/from16 v0, v142

    move/from16 v1, v177

    if-ne v0, v1, :cond_12

    .line 1151
    new-instance v176, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v176 .. v176}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1153
    .local v176, "yTopTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v177, 0x420c0000    # 35.0f

    invoke-virtual/range {v176 .. v177}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1154
    const/16 v177, 0x1

    invoke-virtual/range {v176 .. v177}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1155
    const/16 v177, 0xff

    const/16 v178, 0x4c

    const/16 v179, 0x4c

    const/16 v180, 0x4c

    invoke-virtual/range {v176 .. v180}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1156
    const/16 v177, 0x0

    invoke-virtual/range {v176 .. v177}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1157
    const/16 v177, 0x1

    move-object/from16 v0, v65

    move-object/from16 v1, v176

    move/from16 v2, v177

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V

    .line 1160
    .end local v176    # "yTopTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_12
    new-instance v156, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v156 .. v156}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1162
    .local v156, "popUpTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v177, 0x420c0000    # 35.0f

    move-object/from16 v0, v156

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1163
    const/16 v177, 0x1

    move-object/from16 v0, v156

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1164
    const/16 v177, 0xff

    const/16 v178, 0xff

    const/16 v179, 0x0

    const/16 v180, 0xff

    move-object/from16 v0, v156

    move/from16 v1, v177

    move/from16 v2, v178

    move/from16 v3, v179

    move/from16 v4, v180

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1165
    const/16 v177, 0x0

    move-object/from16 v0, v156

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1166
    move-object/from16 v0, v65

    move-object/from16 v1, v156

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setPopupTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1168
    new-instance v170, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v170 .. v170}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1169
    .local v170, "tooltipTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v177, 0x41c80000    # 25.0f

    move-object/from16 v0, v170

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1170
    const/16 v177, 0x1

    move-object/from16 v0, v170

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1171
    const/16 v177, 0xff

    const/16 v178, 0x4c

    const/16 v179, 0x4c

    const/16 v180, 0x4c

    move-object/from16 v0, v170

    move/from16 v1, v177

    move/from16 v2, v178

    move/from16 v3, v179

    move/from16 v4, v180

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1172
    const/16 v177, 0x0

    move-object/from16 v0, v170

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1173
    move-object/from16 v0, v65

    move-object/from16 v1, v170

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerTooltipTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1175
    new-instance v92, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct/range {v92 .. v92}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1176
    .local v92, "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v177, 0x41c80000    # 25.0f

    move-object/from16 v0, v92

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1177
    const/16 v177, 0x1

    move-object/from16 v0, v92

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1178
    const/16 v177, 0xff

    const/16 v178, 0x0

    const/16 v179, 0x0

    const/16 v180, 0x0

    move-object/from16 v0, v92

    move/from16 v1, v177

    move/from16 v2, v178

    move/from16 v3, v179

    move/from16 v4, v180

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1179
    const/16 v177, 0x0

    move-object/from16 v0, v92

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1180
    move-object/from16 v0, v65

    move-object/from16 v1, v92

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1182
    move-object/from16 v0, v65

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    .line 1185
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->initSeriesAttribute(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto/16 :goto_0

    .line 634
    .end local v5    # "CustomYLabel":Ljava/lang/String;
    .end local v6    # "CustomYLabelStrings":[Ljava/lang/String;
    .end local v7    # "CustomYLabelStringsID":I
    .end local v8    # "CustomYLabelStrsCount":I
    .end local v9    # "EventIcon_BitmapListID":I
    .end local v10    # "EventIcon_NumBitmaps":I
    .end local v11    # "EventIcon_OffsetX":F
    .end local v12    # "EventIcon_Visible":Z
    .end local v13    # "EventIcon_mOffsetY":F
    .end local v14    # "GoalEventIcon_Bitmap":I
    .end local v15    # "GoalEventIcon_OffsetX":F
    .end local v16    # "GoalEventIcon_OffsetY":F
    .end local v17    # "GoalEventIcon_Value":F
    .end local v18    # "GoalEventIcon_Visible":Z
    .end local v19    # "Interaction_mDataZoomEnable":Z
    .end local v20    # "Interaction_mDataZoomInRate":F
    .end local v21    # "Interaction_mDataZoomOutRate":F
    .end local v22    # "Interaction_mPartialZoomEnable":Z
    .end local v23    # "Interaction_mScaleZoomEnable":Z
    .end local v24    # "Interaction_mScaleZoomInRate":F
    .end local v25    # "Interaction_mScaleZoomOutRate":F
    .end local v38    # "XAxis_separatorLineThickness":F
    .end local v39    # "XAxis_separatorSpacingLine":I
    .end local v40    # "XAxis_separatorSpacingTop":I
    .end local v41    # "XAxis_textSpace":F
    .end local v45    # "YAxisColor":I
    .end local v46    # "YAxisDirection":I
    .end local v47    # "YAxisLabelAlign":I
    .end local v48    # "YAxisLineVislble":Z
    .end local v49    # "YAxisMarkingVislble":Z
    .end local v50    # "YAxisTextVislble":Z
    .end local v51    # "YAxisThickness":F
    .end local v52    # "YAxisTitle":Ljava/lang/String;
    .end local v53    # "YAxisTitleAlign":I
    .end local v54    # "YAxisTitleSpacingAlign":F
    .end local v55    # "YAxisTitleSpacingAxis":F
    .end local v56    # "YAxisUseManualMarking":Z
    .end local v57    # "YAxis_separatorLineThickness":F
    .end local v58    # "YAxis_separatorSpacingLine":I
    .end local v59    # "YAxis_separatorSpacingTop":I
    .end local v60    # "YAxis_textSpace":F
    .end local v61    # "YAxis_visibleIndex":I
    .end local v62    # "YAxis_visibleIndexCount":I
    .end local v63    # "YAxis_visibleIndex_array":[I
    .end local v73    # "graphValue_OffsetX":F
    .end local v74    # "graphValue_OffsetY":F
    .end local v75    # "graphValue_Postfix":Ljava/lang/String;
    .end local v76    # "graphValue_Prefix":Ljava/lang/String;
    .end local v77    # "graphValue_Visible":Z
    .end local v78    # "gridLineColor":I
    .end local v79    # "gridLineManualMarkingCnt":I
    .end local v80    # "gridLineManualMarkingIndexArrayResID":I
    .end local v81    # "gridLineManualMarkingVisibleIndices":[I
    .end local v82    # "gridLineThickness":F
    .end local v83    # "gridLineUseManualMarking":Z
    .end local v84    # "gridLineVisible":Z
    .end local v85    # "handlerItemColor":I
    .end local v86    # "handlerItemDirection":I
    .end local v87    # "handlerItemImageResID":I
    .end local v88    # "handlerItemPressedColor":I
    .end local v89    # "handlerItemPressedImageResID":I
    .end local v90    # "handlerItemShapeType":I
    .end local v91    # "handlerItemStrokeWidth":F
    .end local v92    # "handlerItemTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .end local v93    # "handlerLineColor":I
    .end local v94    # "handlerLineImageResID":I
    .end local v95    # "handlerLinePressedColor":I
    .end local v96    # "handlerLinePressedImageResID":I
    .end local v97    # "handlerLineShapeType":I
    .end local v98    # "handlerLineThickness":F
    .end local v99    # "handlerTooltipColor":I
    .end local v100    # "handlerTooltipImageResID":I
    .end local v101    # "handlerTooltipStrokeColor":I
    .end local v102    # "handlerTooltipStrokeWidth":F
    .end local v103    # "handler_item_height":F
    .end local v104    # "handler_item_offset":F
    .end local v105    # "handler_item_width":F
    .end local v106    # "handler_tooltip_visible":Z
    .end local v107    # "handler_visible":Z
    .end local v110    # "interaction":Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
    .end local v111    # "legendItemDirection":I
    .end local v112    # "legendItemDisableColor":I
    .end local v113    # "legendItemDisableImage":Landroid/graphics/Bitmap;
    .end local v114    # "legendItemDisableImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .end local v115    # "legendItemDisableImageArrayResID":I
    .end local v116    # "legendItemDisableImageStringArray":[Ljava/lang/String;
    .end local v117    # "legendItemImage":Landroid/graphics/Bitmap;
    .end local v118    # "legendItemImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .end local v119    # "legendItemImageArrayResID":I
    .end local v120    # "legendItemImageStringArray":[Ljava/lang/String;
    .end local v121    # "legendItemVisible":Z
    .end local v122    # "legnedItemHSpace":F
    .end local v123    # "legnedItemHeight":F
    .end local v124    # "legnedItemInSpace":F
    .end local v125    # "legnedItemWSpace":F
    .end local v126    # "legnedItemWidth":F
    .end local v127    # "mCandleBodyDecreaseColor":I
    .end local v128    # "mCandleBodyIncreaseColor":I
    .end local v129    # "mCandleBodyWidth":F
    .end local v130    # "mCandleLineCloseColor":I
    .end local v131    # "mCandleLineHighColor":I
    .end local v132    # "mCandleLineLowColor":I
    .end local v133    # "mCandleLineOpenColor":I
    .end local v134    # "mCandleLineThickness":F
    .end local v135    # "mCandleShadowColor":I
    .end local v136    # "mCandleShadowThickness":F
    .end local v137    # "mCandleShadowWidth":F
    .end local v138    # "mStackedBarItemNameIndicesID":I
    .end local v139    # "mStackedBarItemNamesID":I
    .end local v147    # "pieHoleRadiusRatio":F
    .end local v148    # "pieItemColorIndicesID":I
    .end local v149    # "pieItemColorValuesID":I
    .end local v150    # "pieLabelCenterRadiusRatio":F
    .end local v151    # "pieLabelOffsetX":F
    .end local v152    # "pieLabelOffsetY":F
    .end local v153    # "pieLabelVisible":Z
    .end local v154    # "pieRadiusRatio":F
    .end local v155    # "pieValueCenterRadiusRatio":F
    .end local v156    # "popUpTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .end local v157    # "popupColor":I
    .end local v158    # "popupHeight":F
    .end local v159    # "popupImageResID":I
    .end local v160    # "popupRadius":F
    .end local v161    # "popupShapeType":I
    .end local v162    # "popupStrokeColor":I
    .end local v163    # "popupStrokeWidth":F
    .end local v164    # "popupVisible":Z
    .end local v165    # "popupWidth":F
    .end local v170    # "tooltipTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .end local v174    # "xTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .end local v175    # "yTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_13
    const/16 v177, 0x98

    .line 635
    const/high16 v178, -0x80000000

    .line 633
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v177

    move/from16 v0, v177

    int-to-float v0, v0

    move/from16 v42, v0

    goto/16 :goto_1

    .line 720
    .restart local v38    # "XAxis_separatorLineThickness":F
    .restart local v39    # "XAxis_separatorSpacingLine":I
    .restart local v40    # "XAxis_separatorSpacingTop":I
    .restart local v41    # "XAxis_textSpace":F
    .restart local v45    # "YAxisColor":I
    .restart local v46    # "YAxisDirection":I
    .restart local v47    # "YAxisLabelAlign":I
    .restart local v48    # "YAxisLineVislble":Z
    .restart local v49    # "YAxisMarkingVislble":Z
    .restart local v50    # "YAxisTextVislble":Z
    .restart local v51    # "YAxisThickness":F
    .restart local v52    # "YAxisTitle":Ljava/lang/String;
    .restart local v53    # "YAxisTitleAlign":I
    .restart local v54    # "YAxisTitleSpacingAlign":F
    .restart local v55    # "YAxisTitleSpacingAxis":F
    .restart local v56    # "YAxisUseManualMarking":Z
    .restart local v61    # "YAxis_visibleIndex":I
    .restart local v62    # "YAxis_visibleIndexCount":I
    .restart local v63    # "YAxis_visibleIndex_array":[I
    :cond_14
    const/16 v177, 0xab

    .line 721
    const/high16 v178, -0x80000000

    .line 719
    move-object/from16 v0, v171

    move/from16 v1, v177

    move/from16 v2, v178

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v61

    goto/16 :goto_2

    .line 747
    .restart local v5    # "CustomYLabel":Ljava/lang/String;
    .restart local v6    # "CustomYLabelStrings":[Ljava/lang/String;
    .restart local v7    # "CustomYLabelStringsID":I
    .restart local v8    # "CustomYLabelStrsCount":I
    .restart local v57    # "YAxis_separatorLineThickness":F
    .restart local v58    # "YAxis_separatorSpacingLine":I
    .restart local v59    # "YAxis_separatorSpacingTop":I
    .restart local v60    # "YAxis_textSpace":F
    :cond_15
    const/16 v177, 0xaf

    move-object/from16 v0, v171

    move/from16 v1, v177

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 748
    if-eqz v5, :cond_4

    .line 750
    const/16 v177, 0x1

    move/from16 v0, v177

    new-array v6, v0, [Ljava/lang/String;

    .line 751
    const/16 v177, 0x0

    aput-object v5, v6, v177

    .line 752
    move-object/from16 v0, v65

    invoke-virtual {v0, v6, v8}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->setYAxisCustomLabel([Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 1004
    .restart local v78    # "gridLineColor":I
    .restart local v79    # "gridLineManualMarkingCnt":I
    .restart local v80    # "gridLineManualMarkingIndexArrayResID":I
    .restart local v81    # "gridLineManualMarkingVisibleIndices":[I
    .restart local v82    # "gridLineThickness":F
    .restart local v83    # "gridLineUseManualMarking":Z
    .restart local v84    # "gridLineVisible":Z
    .restart local v85    # "handlerItemColor":I
    .restart local v86    # "handlerItemDirection":I
    .restart local v87    # "handlerItemImageResID":I
    .restart local v88    # "handlerItemPressedColor":I
    .restart local v89    # "handlerItemPressedImageResID":I
    .restart local v90    # "handlerItemShapeType":I
    .restart local v91    # "handlerItemStrokeWidth":F
    .restart local v93    # "handlerLineColor":I
    .restart local v94    # "handlerLineImageResID":I
    .restart local v95    # "handlerLinePressedColor":I
    .restart local v96    # "handlerLinePressedImageResID":I
    .restart local v97    # "handlerLineShapeType":I
    .restart local v98    # "handlerLineThickness":F
    .restart local v99    # "handlerTooltipColor":I
    .restart local v100    # "handlerTooltipImageResID":I
    .restart local v101    # "handlerTooltipStrokeColor":I
    .restart local v102    # "handlerTooltipStrokeWidth":F
    .restart local v103    # "handler_item_height":F
    .restart local v104    # "handler_item_offset":F
    .restart local v105    # "handler_item_width":F
    .restart local v106    # "handler_tooltip_visible":Z
    .restart local v107    # "handler_visible":Z
    .restart local v111    # "legendItemDirection":I
    .restart local v112    # "legendItemDisableColor":I
    .restart local v117    # "legendItemImage":Landroid/graphics/Bitmap;
    .restart local v118    # "legendItemImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .restart local v119    # "legendItemImageArrayResID":I
    .restart local v120    # "legendItemImageStringArray":[Ljava/lang/String;
    .restart local v121    # "legendItemVisible":Z
    .restart local v122    # "legnedItemHSpace":F
    .restart local v123    # "legnedItemHeight":F
    .restart local v124    # "legnedItemInSpace":F
    .restart local v125    # "legnedItemWSpace":F
    .restart local v126    # "legnedItemWidth":F
    .restart local v157    # "popupColor":I
    .restart local v158    # "popupHeight":F
    .restart local v159    # "popupImageResID":I
    .restart local v160    # "popupRadius":F
    .restart local v161    # "popupShapeType":I
    .restart local v162    # "popupStrokeColor":I
    .restart local v163    # "popupStrokeWidth":F
    .restart local v164    # "popupVisible":Z
    .restart local v165    # "popupWidth":F
    :catch_0
    move-exception v66

    .line 1006
    .local v66, "e":Ljava/lang/Exception;
    new-instance v118, Ljava/util/ArrayList;

    .end local v118    # "legendItemImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-direct/range {v118 .. v118}, Ljava/util/ArrayList;-><init>()V

    .line 1008
    .restart local v118    # "legendItemImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 1009
    move-object/from16 v0, v177

    move/from16 v1, v119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    .line 1008
    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    .line 1010
    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v117

    .line 1012
    const/16 v109, 0x0

    .local v109, "i":I
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    move/from16 v177, v0

    move/from16 v0, v109

    move/from16 v1, v177

    if-ge v0, v1, :cond_d

    .line 1013
    move-object/from16 v0, v118

    move-object/from16 v1, v117

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1012
    add-int/lit8 v109, v109, 0x1

    goto :goto_4

    .line 1032
    .end local v66    # "e":Ljava/lang/Exception;
    .end local v109    # "i":I
    .restart local v113    # "legendItemDisableImage":Landroid/graphics/Bitmap;
    .restart local v114    # "legendItemDisableImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    .restart local v115    # "legendItemDisableImageArrayResID":I
    .restart local v116    # "legendItemDisableImageStringArray":[Ljava/lang/String;
    :catch_1
    move-exception v66

    .line 1034
    .restart local v66    # "e":Ljava/lang/Exception;
    new-instance v114, Ljava/util/ArrayList;

    .end local v114    # "legendItemDisableImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-direct/range {v114 .. v114}, Ljava/util/ArrayList;-><init>()V

    .line 1035
    .restart local v114    # "legendItemDisableImageArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v177

    .line 1036
    move-object/from16 v0, v177

    move/from16 v1, v115

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v177

    .line 1035
    check-cast v177, Landroid/graphics/drawable/BitmapDrawable;

    .line 1037
    invoke-virtual/range {v177 .. v177}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v113

    .line 1038
    const/16 v109, 0x0

    .restart local v109    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    move/from16 v177, v0

    move/from16 v0, v109

    move/from16 v1, v177

    if-ge v0, v1, :cond_f

    .line 1039
    move-object/from16 v0, v114

    move-object/from16 v1, v113

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1038
    add-int/lit8 v109, v109, 0x1

    goto :goto_5
.end method

.method protected initSeriesAttribute(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 93
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 1191
    .line 1192
    sget-object v88, Lcom/sec/dmc/sic/R$styleable;->SicChartView:[I

    .line 1191
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v88

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v67

    .line 1194
    .local v67, "typedArray":Landroid/content/res/TypedArray;
    if-eqz v67, :cond_4

    .line 1196
    const/16 v88, 0x12

    const/16 v89, 0x1

    .line 1195
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v88

    move/from16 v0, v88

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    .line 1198
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    move/from16 v88, v0

    move/from16 v0, v88

    new-array v0, v0, [Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;

    move-object/from16 v46, v0

    .line 1199
    .local v46, "seriesStyles":[Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    move/from16 v88, v0

    move/from16 v0, v88

    if-lt v14, v0, :cond_5

    .line 1204
    const/16 v88, 0x13

    const/16 v89, 0x0

    .line 1203
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v35

    .line 1205
    .local v35, "seriesNameArrayResID":I
    const/16 v34, 0x0

    .line 1206
    .local v34, "seriesNameArray":[Ljava/lang/String;
    const/16 v33, 0x0

    .line 1207
    .local v33, "seriesName":Ljava/lang/String;
    if-eqz v35, :cond_6

    .line 1208
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v35

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v34

    .line 1216
    :goto_1
    const/16 v88, 0x14

    .line 1217
    const/16 v89, 0x0

    .line 1215
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v41

    .line 1218
    .local v41, "seriesNormalRangeVisibleArrayResID":I
    const/16 v40, 0x0

    .line 1219
    .local v40, "seriesNormalRangeVisibleArray":[Ljava/lang/String;
    const/16 v24, 0x0

    .line 1220
    .local v24, "normalRangeVisibleArray":[Z
    const/16 v23, 0x0

    .line 1221
    .local v23, "normalRangeVisible":Z
    if-eqz v41, :cond_7

    .line 1222
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v40

    .line 1224
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBooleanArray([Ljava/lang/String;)[Z

    move-result-object v24

    .line 1233
    :goto_2
    const/16 v88, 0x15

    .line 1234
    const/16 v89, 0x0

    .line 1232
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v39

    .line 1235
    .local v39, "seriesNormalRangeMinValueArrayResID":I
    const/16 v38, 0x0

    .line 1236
    .local v38, "seriesNormalRangeMinValueArray":[Ljava/lang/String;
    const/16 v22, 0x0

    .line 1237
    .local v22, "normalRangeMinValueArray":[F
    const/16 v21, 0x1

    .line 1238
    .local v21, "normalRangeMinValue":F
    if-eqz v39, :cond_8

    .line 1239
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v38

    .line 1241
    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToFloatArray([Ljava/lang/String;)[F

    move-result-object v22

    .line 1250
    :goto_3
    const/16 v88, 0x16

    .line 1251
    const/16 v89, 0x0

    .line 1249
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v37

    .line 1252
    .local v37, "seriesNormalRangeMaxValueArrayResID":I
    const/16 v36, 0x0

    .line 1253
    .local v36, "seriesNormalRangeMaxValueArray":[Ljava/lang/String;
    const/16 v20, 0x0

    .line 1254
    .local v20, "normalRangeMaxValueArray":[F
    const/16 v19, 0x1

    .line 1255
    .local v19, "normalRangeMaxValue":F
    if-eqz v37, :cond_9

    .line 1256
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v36

    .line 1258
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToFloatArray([Ljava/lang/String;)[F

    move-result-object v20

    .line 1268
    :goto_4
    const/16 v88, 0x17

    .line 1269
    const/16 v89, 0x0

    .line 1267
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v45

    .line 1270
    .local v45, "seriesNormalRangecolorInRangeArrayResID":I
    const/16 v44, 0x0

    .line 1271
    .local v44, "seriesNormalRangecolorInRangeArray":[Ljava/lang/String;
    const/16 v18, 0x0

    .line 1272
    .local v18, "normalRangeColorInRangeArray":[I
    const/high16 v17, -0x10000

    .line 1273
    .local v17, "normalRangeColorInRange":I
    if-eqz v45, :cond_a

    .line 1274
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1275
    move-object/from16 v0, v88

    move/from16 v1, v45

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v44

    .line 1276
    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToColorArray([Ljava/lang/String;)[I

    move-result-object v18

    .line 1285
    :goto_5
    const/16 v88, 0x18

    const/16 v89, 0x0

    .line 1284
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v43

    .line 1286
    .local v43, "seriesNormalRangecolorArrayResID":I
    const/16 v42, 0x0

    .line 1287
    .local v42, "seriesNormalRangecolorArray":[Ljava/lang/String;
    const/16 v16, 0x0

    .line 1288
    .local v16, "normalRangeColorArray":[I
    const v15, -0x1b0628

    .line 1289
    .local v15, "normalRangeColor":I
    if-eqz v43, :cond_b

    .line 1290
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1291
    move-object/from16 v0, v88

    move/from16 v1, v43

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v44

    .line 1292
    move-object/from16 v0, p0

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToColorArray([Ljava/lang/String;)[I

    move-result-object v16

    .line 1301
    :goto_6
    const/16 v88, 0x19

    .line 1302
    const/16 v89, 0x0

    .line 1300
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v66

    .line 1303
    .local v66, "seriesValueMarkingVisibleArrayResID":I
    const/16 v65, 0x0

    .line 1304
    .local v65, "seriesValueMarkingVisibleArray":[Ljava/lang/String;
    const/16 v87, 0x0

    .line 1305
    .local v87, "valueMarkingVisibleArray":[Z
    const/16 v86, 0x0

    .line 1306
    .local v86, "valueMarkingVisible":Z
    if-eqz v66, :cond_c

    .line 1307
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v65

    .line 1309
    move-object/from16 v0, p0

    move-object/from16 v1, v65

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBooleanArray([Ljava/lang/String;)[Z

    move-result-object v87

    .line 1318
    :goto_7
    const/16 v88, 0x1a

    const/16 v89, 0x0

    .line 1317
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v64

    .line 1319
    .local v64, "seriesValueMarkingSizeArrayResID":I
    const/16 v63, 0x0

    .line 1320
    .local v63, "seriesValueMarkingSizeArray":[Ljava/lang/String;
    const/16 v85, 0x0

    .line 1321
    .local v85, "valueMarkingSizeArray":[F
    const/high16 v84, 0x41200000    # 10.0f

    .line 1322
    .local v84, "valueMarkingSize":F
    if-eqz v64, :cond_d

    .line 1323
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v64

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v63

    .line 1325
    move-object/from16 v0, p0

    move-object/from16 v1, v63

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToFloatArray([Ljava/lang/String;)[F

    move-result-object v85

    .line 1335
    :goto_8
    const/16 v88, 0x1b

    .line 1336
    const/16 v89, 0x0

    .line 1334
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v56

    .line 1337
    .local v56, "seriesValueMarkingNormalColorArrayResID":I
    const/16 v55, 0x0

    .line 1338
    .local v55, "seriesValueMarkingNormalColorArray":[Ljava/lang/String;
    const/16 v77, 0x0

    .line 1339
    .local v77, "valueMarkingNormalColorArray":[I
    const v76, -0xffff01

    .line 1340
    .local v76, "valueMarkingNormalColor":I
    if-eqz v56, :cond_e

    .line 1341
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1342
    move-object/from16 v0, v88

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v55

    .line 1343
    move-object/from16 v0, p0

    move-object/from16 v1, v55

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToColorArray([Ljava/lang/String;)[I

    move-result-object v77

    .line 1354
    :goto_9
    const/16 v88, 0x1c

    .line 1355
    const/16 v89, 0x0

    .line 1353
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v58

    .line 1356
    .local v58, "seriesValueMarkingNormalImageArrayResID":I
    const/16 v57, 0x0

    .line 1357
    .local v57, "seriesValueMarkingNormalImageArray":[Ljava/lang/String;
    const/16 v79, 0x0

    .line 1358
    .local v79, "valueMarkingNormalImageArray":[Landroid/graphics/Bitmap;
    const/16 v78, 0x0

    .line 1359
    .local v78, "valueMarkingNormalImage":Landroid/graphics/Bitmap;
    if-eqz v58, :cond_0

    .line 1361
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1362
    move-object/from16 v0, v88

    move/from16 v1, v58

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v57

    .line 1364
    move-object/from16 v0, p0

    move-object/from16 v1, v57

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBitmapArray([Ljava/lang/String;)[Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v79

    .line 1375
    :cond_0
    :goto_a
    const/16 v88, 0x1d

    .line 1376
    const/16 v89, 0x0

    .line 1374
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v52

    .line 1377
    .local v52, "seriesValueMarkingInRangeColorArrayResID":I
    const/16 v51, 0x0

    .line 1378
    .local v51, "seriesValueMarkingInRangeColorArray":[Ljava/lang/String;
    const/16 v73, 0x0

    .line 1379
    .local v73, "valueMarkingInRangeColorArray":[I
    const/high16 v72, -0x10000

    .line 1380
    .local v72, "valueMarkingInRangeColor":I
    if-eqz v52, :cond_f

    .line 1381
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1382
    move-object/from16 v0, v88

    move/from16 v1, v52

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v51

    .line 1384
    move-object/from16 v0, p0

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToColorArray([Ljava/lang/String;)[I

    move-result-object v73

    .line 1395
    :goto_b
    const/16 v88, 0x1e

    .line 1396
    const/16 v89, 0x0

    .line 1394
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v54

    .line 1397
    .local v54, "seriesValueMarkingInRangeImageArrayResID":I
    const/16 v53, 0x0

    .line 1398
    .local v53, "seriesValueMarkingInRangeImageArray":[Ljava/lang/String;
    const/16 v75, 0x0

    .line 1399
    .local v75, "valueMarkingInRangeImageArray":[Landroid/graphics/Bitmap;
    const/16 v74, 0x0

    .line 1400
    .local v74, "valueMarkingInRangeImage":Landroid/graphics/Bitmap;
    if-eqz v54, :cond_1

    .line 1402
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1403
    move-object/from16 v0, v88

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v53

    .line 1405
    move-object/from16 v0, p0

    move-object/from16 v1, v53

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBitmapArray([Ljava/lang/String;)[Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v75

    .line 1416
    :cond_1
    :goto_c
    const/16 v88, 0x1f

    .line 1417
    const/16 v89, 0x0

    .line 1415
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v48

    .line 1418
    .local v48, "seriesValueMarkingHandlerOverColorArrayResID":I
    const/16 v47, 0x0

    .line 1419
    .local v47, "seriesValueMarkingHandlerOverColorArray":[Ljava/lang/String;
    const/16 v69, 0x0

    .line 1420
    .local v69, "valueMarkingHandlerOverColorArray":[I
    const v68, -0xff0100

    .line 1421
    .local v68, "valueMarkingHandlerOverColor":I
    if-eqz v48, :cond_10

    .line 1422
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1423
    move-object/from16 v0, v88

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v47

    .line 1425
    move-object/from16 v0, p0

    move-object/from16 v1, v47

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToColorArray([Ljava/lang/String;)[I

    move-result-object v69

    .line 1436
    :goto_d
    const/16 v88, 0x20

    .line 1437
    const/16 v89, 0x0

    .line 1435
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v50

    .line 1438
    .local v50, "seriesValueMarkingHandlerOverInRangeImageArrayResID":I
    const/16 v49, 0x0

    .line 1439
    .local v49, "seriesValueMarkingHandlerOverInRangeImageArray":[Ljava/lang/String;
    const/16 v71, 0x0

    .line 1440
    .local v71, "valueMarkingHandlerOverInRangeImageArray":[Landroid/graphics/Bitmap;
    const/16 v70, 0x0

    .line 1441
    .local v70, "valueMarkingHandlerOverInRangeImage":Landroid/graphics/Bitmap;
    if-eqz v50, :cond_2

    .line 1443
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1444
    move-object/from16 v0, v88

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v49

    .line 1446
    move-object/from16 v0, p0

    move-object/from16 v1, v49

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBitmapArray([Ljava/lang/String;)[Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v71

    .line 1481
    :cond_2
    :goto_e
    const/16 v88, 0x21

    .line 1482
    const/16 v89, 0x0

    .line 1480
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v60

    .line 1483
    .local v60, "seriesValueMarkingSelectedColorArrayResID":I
    const/16 v59, 0x0

    .line 1484
    .local v59, "seriesValueMarkingSelectedColorArray":[Ljava/lang/String;
    const/16 v81, 0x0

    .line 1485
    .local v81, "valueMarkingSelectedColorArray":[I
    const/16 v80, -0x100

    .line 1486
    .local v80, "valueMarkingSelectedColor":I
    if-eqz v60, :cond_11

    .line 1487
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1488
    move-object/from16 v0, v88

    move/from16 v1, v60

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v59

    .line 1490
    move-object/from16 v0, p0

    move-object/from16 v1, v59

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToColorArray([Ljava/lang/String;)[I

    move-result-object v81

    .line 1501
    :goto_f
    const/16 v88, 0x22

    .line 1502
    const/16 v89, 0x0

    .line 1500
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v62

    .line 1503
    .local v62, "seriesValueMarkingSelectedImageArrayResID":I
    const/16 v61, 0x0

    .line 1504
    .local v61, "seriesValueMarkingSelectedImageArray":[Ljava/lang/String;
    const/16 v83, 0x0

    .line 1505
    .local v83, "valueMarkingSelectedImageArray":[Landroid/graphics/Bitmap;
    const/16 v82, 0x0

    .line 1506
    .local v82, "valueMarkingSelectedImage":Landroid/graphics/Bitmap;
    if-eqz v62, :cond_3

    .line 1508
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1509
    move-object/from16 v0, v88

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v61

    .line 1511
    move-object/from16 v0, p0

    move-object/from16 v1, v61

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBitmapArray([Ljava/lang/String;)[Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v83

    .line 1521
    :cond_3
    :goto_10
    const/16 v88, 0x23

    const/16 v89, 0x0

    .line 1520
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v30

    .line 1522
    .local v30, "seriesGoalLineVisibleArrayResID":I
    const/16 v29, 0x0

    .line 1523
    .local v29, "seriesGoalLineVisibleArray":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 1524
    .local v11, "goalLineVisibleArray":[Z
    const/4 v10, 0x0

    .line 1525
    .local v10, "goalLineVisible":Z
    if-eqz v30, :cond_12

    .line 1526
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v29

    .line 1528
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToBooleanArray([Ljava/lang/String;)[Z

    move-result-object v11

    .line 1536
    :goto_11
    const/16 v88, 0x24

    const/16 v89, 0x0

    .line 1535
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v28

    .line 1537
    .local v28, "seriesGoalLineValueArrayResID":I
    const/16 v27, 0x0

    .line 1538
    .local v27, "seriesGoalLineValueArray":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 1539
    .local v9, "goalLineValueArray":[F
    const/4 v8, 0x1

    .line 1540
    .local v8, "goalLineValue":F
    if-eqz v28, :cond_13

    .line 1541
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v27

    .line 1543
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToFloatArray([Ljava/lang/String;)[F

    move-result-object v9

    .line 1551
    :goto_12
    const/16 v88, 0x25

    const/16 v89, 0x0

    .line 1550
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v32

    .line 1552
    .local v32, "seriesGoalLineWidthArrayResID":I
    const/16 v31, 0x0

    .line 1553
    .local v31, "seriesGoalLineWidthArray":[Ljava/lang/String;
    const/4 v13, 0x0

    .line 1554
    .local v13, "goalLineWidthArray":[F
    const/4 v12, 0x1

    .line 1555
    .local v12, "goalLineWidth":F
    if-eqz v32, :cond_14

    .line 1556
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v31

    .line 1558
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToFloatArray([Ljava/lang/String;)[F

    move-result-object v13

    .line 1567
    :goto_13
    const/16 v88, 0x26

    const/16 v89, 0x0

    .line 1566
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v26

    .line 1568
    .local v26, "seriesGoalLineColorArrayResID":I
    const/16 v25, 0x0

    .line 1569
    .local v25, "seriesGoalLineColorArray":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1570
    .local v6, "goalLineColorArray":[I
    const v5, -0xff0100

    .line 1571
    .local v5, "goalLineColor":I
    if-eqz v26, :cond_15

    .line 1572
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    move-object/from16 v0, v88

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v25

    .line 1574
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->convertStringToColorArray([Ljava/lang/String;)[I

    move-result-object v6

    .line 1581
    :goto_14
    invoke-virtual/range {v67 .. v67}, Landroid/content/res/TypedArray;->recycle()V

    .line 1583
    const/4 v14, 0x0

    :goto_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    move/from16 v88, v0

    move/from16 v0, v88

    if-lt v14, v0, :cond_16

    .line 1761
    .end local v5    # "goalLineColor":I
    .end local v6    # "goalLineColorArray":[I
    .end local v8    # "goalLineValue":F
    .end local v9    # "goalLineValueArray":[F
    .end local v10    # "goalLineVisible":Z
    .end local v11    # "goalLineVisibleArray":[Z
    .end local v12    # "goalLineWidth":F
    .end local v13    # "goalLineWidthArray":[F
    .end local v14    # "i":I
    .end local v15    # "normalRangeColor":I
    .end local v16    # "normalRangeColorArray":[I
    .end local v17    # "normalRangeColorInRange":I
    .end local v18    # "normalRangeColorInRangeArray":[I
    .end local v19    # "normalRangeMaxValue":F
    .end local v20    # "normalRangeMaxValueArray":[F
    .end local v21    # "normalRangeMinValue":F
    .end local v22    # "normalRangeMinValueArray":[F
    .end local v23    # "normalRangeVisible":Z
    .end local v24    # "normalRangeVisibleArray":[Z
    .end local v25    # "seriesGoalLineColorArray":[Ljava/lang/String;
    .end local v26    # "seriesGoalLineColorArrayResID":I
    .end local v27    # "seriesGoalLineValueArray":[Ljava/lang/String;
    .end local v28    # "seriesGoalLineValueArrayResID":I
    .end local v29    # "seriesGoalLineVisibleArray":[Ljava/lang/String;
    .end local v30    # "seriesGoalLineVisibleArrayResID":I
    .end local v31    # "seriesGoalLineWidthArray":[Ljava/lang/String;
    .end local v32    # "seriesGoalLineWidthArrayResID":I
    .end local v33    # "seriesName":Ljava/lang/String;
    .end local v34    # "seriesNameArray":[Ljava/lang/String;
    .end local v35    # "seriesNameArrayResID":I
    .end local v36    # "seriesNormalRangeMaxValueArray":[Ljava/lang/String;
    .end local v37    # "seriesNormalRangeMaxValueArrayResID":I
    .end local v38    # "seriesNormalRangeMinValueArray":[Ljava/lang/String;
    .end local v39    # "seriesNormalRangeMinValueArrayResID":I
    .end local v40    # "seriesNormalRangeVisibleArray":[Ljava/lang/String;
    .end local v41    # "seriesNormalRangeVisibleArrayResID":I
    .end local v42    # "seriesNormalRangecolorArray":[Ljava/lang/String;
    .end local v43    # "seriesNormalRangecolorArrayResID":I
    .end local v44    # "seriesNormalRangecolorInRangeArray":[Ljava/lang/String;
    .end local v45    # "seriesNormalRangecolorInRangeArrayResID":I
    .end local v46    # "seriesStyles":[Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    .end local v47    # "seriesValueMarkingHandlerOverColorArray":[Ljava/lang/String;
    .end local v48    # "seriesValueMarkingHandlerOverColorArrayResID":I
    .end local v49    # "seriesValueMarkingHandlerOverInRangeImageArray":[Ljava/lang/String;
    .end local v50    # "seriesValueMarkingHandlerOverInRangeImageArrayResID":I
    .end local v51    # "seriesValueMarkingInRangeColorArray":[Ljava/lang/String;
    .end local v52    # "seriesValueMarkingInRangeColorArrayResID":I
    .end local v53    # "seriesValueMarkingInRangeImageArray":[Ljava/lang/String;
    .end local v54    # "seriesValueMarkingInRangeImageArrayResID":I
    .end local v55    # "seriesValueMarkingNormalColorArray":[Ljava/lang/String;
    .end local v56    # "seriesValueMarkingNormalColorArrayResID":I
    .end local v57    # "seriesValueMarkingNormalImageArray":[Ljava/lang/String;
    .end local v58    # "seriesValueMarkingNormalImageArrayResID":I
    .end local v59    # "seriesValueMarkingSelectedColorArray":[Ljava/lang/String;
    .end local v60    # "seriesValueMarkingSelectedColorArrayResID":I
    .end local v61    # "seriesValueMarkingSelectedImageArray":[Ljava/lang/String;
    .end local v62    # "seriesValueMarkingSelectedImageArrayResID":I
    .end local v63    # "seriesValueMarkingSizeArray":[Ljava/lang/String;
    .end local v64    # "seriesValueMarkingSizeArrayResID":I
    .end local v65    # "seriesValueMarkingVisibleArray":[Ljava/lang/String;
    .end local v66    # "seriesValueMarkingVisibleArrayResID":I
    .end local v68    # "valueMarkingHandlerOverColor":I
    .end local v69    # "valueMarkingHandlerOverColorArray":[I
    .end local v70    # "valueMarkingHandlerOverInRangeImage":Landroid/graphics/Bitmap;
    .end local v71    # "valueMarkingHandlerOverInRangeImageArray":[Landroid/graphics/Bitmap;
    .end local v72    # "valueMarkingInRangeColor":I
    .end local v73    # "valueMarkingInRangeColorArray":[I
    .end local v74    # "valueMarkingInRangeImage":Landroid/graphics/Bitmap;
    .end local v75    # "valueMarkingInRangeImageArray":[Landroid/graphics/Bitmap;
    .end local v76    # "valueMarkingNormalColor":I
    .end local v77    # "valueMarkingNormalColorArray":[I
    .end local v78    # "valueMarkingNormalImage":Landroid/graphics/Bitmap;
    .end local v79    # "valueMarkingNormalImageArray":[Landroid/graphics/Bitmap;
    .end local v80    # "valueMarkingSelectedColor":I
    .end local v81    # "valueMarkingSelectedColorArray":[I
    .end local v82    # "valueMarkingSelectedImage":Landroid/graphics/Bitmap;
    .end local v83    # "valueMarkingSelectedImageArray":[Landroid/graphics/Bitmap;
    .end local v84    # "valueMarkingSize":F
    .end local v85    # "valueMarkingSizeArray":[F
    .end local v86    # "valueMarkingVisible":Z
    .end local v87    # "valueMarkingVisibleArray":[Z
    :cond_4
    return-void

    .line 1200
    .restart local v14    # "i":I
    .restart local v46    # "seriesStyles":[Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    :cond_5
    new-instance v88, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;

    invoke-direct/range {v88 .. v88}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;-><init>()V

    aput-object v88, v46, v14

    .line 1199
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 1212
    .restart local v33    # "seriesName":Ljava/lang/String;
    .restart local v34    # "seriesNameArray":[Ljava/lang/String;
    .restart local v35    # "seriesNameArrayResID":I
    :cond_6
    const/16 v88, 0x13

    move-object/from16 v0, v67

    move/from16 v1, v88

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v33

    .line 1211
    goto/16 :goto_1

    .line 1228
    .restart local v23    # "normalRangeVisible":Z
    .restart local v24    # "normalRangeVisibleArray":[Z
    .restart local v40    # "seriesNormalRangeVisibleArray":[Ljava/lang/String;
    .restart local v41    # "seriesNormalRangeVisibleArrayResID":I
    :cond_7
    const/16 v88, 0x14

    .line 1229
    const/16 v89, 0x0

    .line 1227
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v23

    .line 1226
    goto/16 :goto_2

    .line 1245
    .restart local v21    # "normalRangeMinValue":F
    .restart local v22    # "normalRangeMinValueArray":[F
    .restart local v38    # "seriesNormalRangeMinValueArray":[Ljava/lang/String;
    .restart local v39    # "seriesNormalRangeMinValueArrayResID":I
    :cond_8
    const/16 v88, 0x15

    .line 1246
    const/16 v89, 0x1

    .line 1244
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v21

    .line 1243
    goto/16 :goto_3

    .line 1262
    .restart local v19    # "normalRangeMaxValue":F
    .restart local v20    # "normalRangeMaxValueArray":[F
    .restart local v36    # "seriesNormalRangeMaxValueArray":[Ljava/lang/String;
    .restart local v37    # "seriesNormalRangeMaxValueArrayResID":I
    :cond_9
    const/16 v88, 0x16

    .line 1263
    const/16 v89, 0x1

    .line 1261
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v19

    .line 1260
    goto/16 :goto_4

    .line 1280
    .restart local v17    # "normalRangeColorInRange":I
    .restart local v18    # "normalRangeColorInRangeArray":[I
    .restart local v44    # "seriesNormalRangecolorInRangeArray":[Ljava/lang/String;
    .restart local v45    # "seriesNormalRangecolorInRangeArrayResID":I
    :cond_a
    const/16 v88, 0x17

    .line 1281
    const/high16 v89, -0x10000

    .line 1279
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v17

    .line 1278
    goto/16 :goto_5

    .line 1296
    .restart local v15    # "normalRangeColor":I
    .restart local v16    # "normalRangeColorArray":[I
    .restart local v42    # "seriesNormalRangecolorArray":[Ljava/lang/String;
    .restart local v43    # "seriesNormalRangecolorArrayResID":I
    :cond_b
    const/16 v88, 0x18

    .line 1297
    const v89, -0x1b0628

    .line 1295
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v15

    .line 1294
    goto/16 :goto_6

    .line 1313
    .restart local v65    # "seriesValueMarkingVisibleArray":[Ljava/lang/String;
    .restart local v66    # "seriesValueMarkingVisibleArrayResID":I
    .restart local v86    # "valueMarkingVisible":Z
    .restart local v87    # "valueMarkingVisibleArray":[Z
    :cond_c
    const/16 v88, 0x19

    .line 1314
    const/16 v89, 0x0

    .line 1312
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v86

    .line 1311
    goto/16 :goto_7

    .line 1329
    .restart local v63    # "seriesValueMarkingSizeArray":[Ljava/lang/String;
    .restart local v64    # "seriesValueMarkingSizeArrayResID":I
    .restart local v84    # "valueMarkingSize":F
    .restart local v85    # "valueMarkingSizeArray":[F
    :cond_d
    const/16 v88, 0x1a

    .line 1330
    const/16 v89, 0xa

    .line 1328
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v88

    move/from16 v0, v88

    int-to-float v0, v0

    move/from16 v84, v0

    .line 1327
    goto/16 :goto_8

    .line 1347
    .restart local v55    # "seriesValueMarkingNormalColorArray":[Ljava/lang/String;
    .restart local v56    # "seriesValueMarkingNormalColorArrayResID":I
    .restart local v76    # "valueMarkingNormalColor":I
    .restart local v77    # "valueMarkingNormalColorArray":[I
    :cond_e
    const/16 v88, 0x1b

    .line 1348
    const v89, -0xffff01

    .line 1346
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v76

    .line 1345
    goto/16 :goto_9

    .line 1365
    .restart local v57    # "seriesValueMarkingNormalImageArray":[Ljava/lang/String;
    .restart local v58    # "seriesValueMarkingNormalImageArrayResID":I
    .restart local v78    # "valueMarkingNormalImage":Landroid/graphics/Bitmap;
    .restart local v79    # "valueMarkingNormalImageArray":[Landroid/graphics/Bitmap;
    :catch_0
    move-exception v4

    .line 1367
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1368
    move-object/from16 v0, v88

    move/from16 v1, v58

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v88

    .line 1367
    check-cast v88, Landroid/graphics/drawable/BitmapDrawable;

    .line 1369
    invoke-virtual/range {v88 .. v88}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v78

    .line 1367
    goto/16 :goto_a

    .line 1388
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v51    # "seriesValueMarkingInRangeColorArray":[Ljava/lang/String;
    .restart local v52    # "seriesValueMarkingInRangeColorArrayResID":I
    .restart local v72    # "valueMarkingInRangeColor":I
    .restart local v73    # "valueMarkingInRangeColorArray":[I
    :cond_f
    const/16 v88, 0x1d

    .line 1389
    const/high16 v89, -0x10000

    .line 1387
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v72

    .line 1386
    goto/16 :goto_b

    .line 1406
    .restart local v53    # "seriesValueMarkingInRangeImageArray":[Ljava/lang/String;
    .restart local v54    # "seriesValueMarkingInRangeImageArrayResID":I
    .restart local v74    # "valueMarkingInRangeImage":Landroid/graphics/Bitmap;
    .restart local v75    # "valueMarkingInRangeImageArray":[Landroid/graphics/Bitmap;
    :catch_1
    move-exception v4

    .line 1408
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1409
    move-object/from16 v0, v88

    move/from16 v1, v54

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v88

    .line 1408
    check-cast v88, Landroid/graphics/drawable/BitmapDrawable;

    .line 1410
    invoke-virtual/range {v88 .. v88}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v74

    .line 1408
    goto/16 :goto_c

    .line 1429
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v47    # "seriesValueMarkingHandlerOverColorArray":[Ljava/lang/String;
    .restart local v48    # "seriesValueMarkingHandlerOverColorArrayResID":I
    .restart local v68    # "valueMarkingHandlerOverColor":I
    .restart local v69    # "valueMarkingHandlerOverColorArray":[I
    :cond_10
    const/16 v88, 0x1f

    .line 1430
    const v89, -0xff0100

    .line 1428
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v68

    .line 1427
    goto/16 :goto_d

    .line 1447
    .restart local v49    # "seriesValueMarkingHandlerOverInRangeImageArray":[Ljava/lang/String;
    .restart local v50    # "seriesValueMarkingHandlerOverInRangeImageArrayResID":I
    .restart local v70    # "valueMarkingHandlerOverInRangeImage":Landroid/graphics/Bitmap;
    .restart local v71    # "valueMarkingHandlerOverInRangeImageArray":[Landroid/graphics/Bitmap;
    :catch_2
    move-exception v4

    .line 1449
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1450
    move-object/from16 v0, v88

    move/from16 v1, v50

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v88

    .line 1449
    check-cast v88, Landroid/graphics/drawable/BitmapDrawable;

    .line 1451
    invoke-virtual/range {v88 .. v88}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v70

    .line 1449
    goto/16 :goto_e

    .line 1494
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v59    # "seriesValueMarkingSelectedColorArray":[Ljava/lang/String;
    .restart local v60    # "seriesValueMarkingSelectedColorArrayResID":I
    .restart local v80    # "valueMarkingSelectedColor":I
    .restart local v81    # "valueMarkingSelectedColorArray":[I
    :cond_11
    const/16 v88, 0x21

    .line 1495
    const/16 v89, -0x100

    .line 1493
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v80

    .line 1492
    goto/16 :goto_f

    .line 1512
    .restart local v61    # "seriesValueMarkingSelectedImageArray":[Ljava/lang/String;
    .restart local v62    # "seriesValueMarkingSelectedImageArrayResID":I
    .restart local v82    # "valueMarkingSelectedImage":Landroid/graphics/Bitmap;
    .restart local v83    # "valueMarkingSelectedImageArray":[Landroid/graphics/Bitmap;
    :catch_3
    move-exception v4

    .line 1514
    .restart local v4    # "e":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getResources()Landroid/content/res/Resources;

    move-result-object v88

    .line 1515
    move-object/from16 v0, v88

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v88

    .line 1514
    check-cast v88, Landroid/graphics/drawable/BitmapDrawable;

    .line 1516
    invoke-virtual/range {v88 .. v88}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v82

    .line 1514
    goto/16 :goto_10

    .line 1531
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v10    # "goalLineVisible":Z
    .restart local v11    # "goalLineVisibleArray":[Z
    .restart local v29    # "seriesGoalLineVisibleArray":[Ljava/lang/String;
    .restart local v30    # "seriesGoalLineVisibleArrayResID":I
    :cond_12
    const/16 v88, 0x23

    .line 1532
    const/16 v89, 0x0

    .line 1530
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v10

    goto/16 :goto_11

    .line 1546
    .restart local v8    # "goalLineValue":F
    .restart local v9    # "goalLineValueArray":[F
    .restart local v27    # "seriesGoalLineValueArray":[Ljava/lang/String;
    .restart local v28    # "seriesGoalLineValueArrayResID":I
    :cond_13
    const/16 v88, 0x24

    .line 1547
    const/16 v89, 0x1

    .line 1545
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v8

    goto/16 :goto_12

    .line 1562
    .restart local v12    # "goalLineWidth":F
    .restart local v13    # "goalLineWidthArray":[F
    .restart local v31    # "seriesGoalLineWidthArray":[Ljava/lang/String;
    .restart local v32    # "seriesGoalLineWidthArrayResID":I
    :cond_14
    const/16 v88, 0x25

    .line 1563
    const/16 v89, 0x0

    .line 1561
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v88

    move/from16 v0, v88

    int-to-float v12, v0

    .line 1560
    goto/16 :goto_13

    .line 1577
    .restart local v5    # "goalLineColor":I
    .restart local v6    # "goalLineColorArray":[I
    .restart local v25    # "seriesGoalLineColorArray":[Ljava/lang/String;
    .restart local v26    # "seriesGoalLineColorArrayResID":I
    :cond_15
    const/16 v88, 0x26

    .line 1578
    const v89, -0xff0100

    .line 1576
    move-object/from16 v0, v67

    move/from16 v1, v88

    move/from16 v2, v89

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    goto/16 :goto_14

    .line 1584
    :cond_16
    if-eqz v34, :cond_1c

    .line 1585
    aget-object v88, v46, v14

    aget-object v89, v34, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    .line 1592
    :cond_17
    :goto_16
    if-eqz v24, :cond_1d

    .line 1593
    aget-object v88, v46, v14

    .line 1594
    aget-boolean v89, v24, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeVisible(Z)V

    .line 1599
    :goto_17
    if-eqz v22, :cond_1e

    .line 1600
    if-eqz v20, :cond_1e

    .line 1601
    aget-object v88, v46, v14

    .line 1602
    aget v89, v22, v14

    .line 1603
    aget v90, v20, v14

    .line 1601
    invoke-virtual/range {v88 .. v90}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeMinMaxValue(FF)V

    .line 1609
    :goto_18
    if-eqz v16, :cond_1f

    .line 1610
    aget-object v88, v46, v14

    .line 1611
    aget v89, v16, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeFillColor(I)V

    .line 1616
    :goto_19
    if-eqz v18, :cond_20

    .line 1617
    aget-object v88, v46, v14

    .line 1618
    aget v89, v18, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeColorInRange(I)V

    .line 1624
    :goto_1a
    if-eqz v87, :cond_21

    .line 1625
    aget-object v88, v46, v14

    .line 1626
    aget-boolean v89, v87, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingVisible(Z)V

    .line 1631
    :goto_1b
    if-eqz v85, :cond_22

    .line 1632
    aget-object v88, v46, v14

    .line 1633
    aget v89, v85, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingSize(F)V

    .line 1638
    :goto_1c
    if-eqz v77, :cond_23

    .line 1639
    aget-object v88, v46, v14

    .line 1640
    aget v89, v77, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingNormalColor(I)V

    .line 1646
    :goto_1d
    if-eqz v79, :cond_24

    .line 1647
    aget-object v88, v46, v14

    .line 1648
    aget-object v89, v79, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    .line 1654
    :goto_1e
    if-eqz v73, :cond_25

    .line 1655
    aget-object v88, v46, v14

    .line 1656
    aget v89, v73, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingInRangeColor(I)V

    .line 1662
    :goto_1f
    if-eqz v75, :cond_26

    .line 1663
    aget-object v88, v46, v14

    .line 1664
    aget-object v89, v75, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingInRangeImage(Landroid/graphics/Bitmap;)V

    .line 1670
    :goto_20
    if-eqz v69, :cond_27

    .line 1671
    aget-object v88, v46, v14

    .line 1672
    aget v89, v69, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingHandlerOverColor(I)V

    .line 1678
    :goto_21
    if-eqz v71, :cond_28

    .line 1679
    aget-object v88, v46, v14

    .line 1680
    aget-object v89, v71, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingHandlerOverInRangeImage(Landroid/graphics/Bitmap;)V

    .line 1695
    :goto_22
    if-eqz v81, :cond_29

    .line 1696
    aget-object v88, v46, v14

    .line 1697
    aget v89, v81, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingSelectedColor(I)V

    .line 1703
    :goto_23
    if-eqz v83, :cond_2a

    .line 1704
    aget-object v88, v46, v14

    .line 1705
    aget-object v89, v83, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingSelectedImage(Landroid/graphics/Bitmap;)V

    .line 1711
    :goto_24
    if-eqz v11, :cond_2b

    .line 1712
    aget-object v88, v46, v14

    aget-boolean v89, v11, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineVisible(Z)V

    .line 1717
    :goto_25
    if-eqz v9, :cond_2c

    .line 1718
    aget-object v88, v46, v14

    aget v89, v9, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineValue(F)V

    .line 1723
    :goto_26
    if-eqz v13, :cond_2d

    .line 1724
    aget-object v88, v46, v14

    .line 1725
    aget v89, v13, v14

    move/from16 v0, v89

    float-to-int v0, v0

    move/from16 v89, v0

    move/from16 v0, v89

    int-to-float v0, v0

    move/from16 v89, v0

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineThickness(F)V

    .line 1730
    :goto_27
    if-eqz v6, :cond_2e

    .line 1731
    aget-object v88, v46, v14

    aget v89, v6, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineColor(I)V

    .line 1736
    :goto_28
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMin:Z

    move/from16 v88, v0

    if-nez v88, :cond_18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMax:Z

    move/from16 v88, v0

    if-eqz v88, :cond_19

    .line 1737
    :cond_18
    aget-object v88, v46, v14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMin:Z

    move/from16 v89, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_usefixedMax:Z

    move/from16 v90, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_fixedMinY:F

    move/from16 v91, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mLeftYAxis_fixedMaxY:F

    move/from16 v92, v0

    invoke-virtual/range {v88 .. v92}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1740
    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSeriesNum:I

    move/from16 v88, v0

    const/16 v89, 0x2

    move/from16 v0, v88

    move/from16 v1, v89

    if-ne v0, v1, :cond_1b

    .line 1741
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMin:Z

    move/from16 v88, v0

    if-nez v88, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMax:Z

    move/from16 v88, v0

    if-eqz v88, :cond_1b

    .line 1742
    :cond_1a
    aget-object v88, v46, v14

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMin:Z

    move/from16 v89, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_usefixedMax:Z

    move/from16 v90, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_fixedMinY:F

    move/from16 v91, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRightYAxis_fixedMaxY:F

    move/from16 v92, v0

    invoke-virtual/range {v88 .. v92}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setFixedMinMaxY(ZZFF)V

    .line 1747
    :cond_1b
    new-instance v7, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v7}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    .line 1748
    .local v7, "goalLineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    const/high16 v88, 0x41a00000    # 20.0f

    move/from16 v0, v88

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 1749
    const/16 v88, 0x1

    move/from16 v0, v88

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 1750
    const/16 v88, 0xff

    const/16 v89, 0x0

    const/16 v90, 0x0

    const/16 v91, 0x0

    move/from16 v0, v88

    move/from16 v1, v89

    move/from16 v2, v90

    move/from16 v3, v91

    invoke-virtual {v7, v0, v1, v2, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 1751
    const/16 v88, 0x0

    move/from16 v0, v88

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 1753
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    move-object/from16 v88, v0

    aget-object v89, v46, v14

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V

    .line 1583
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_15

    .line 1587
    .end local v7    # "goalLineTextStyle":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :cond_1c
    if-eqz v33, :cond_17

    .line 1588
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setSeriesName(Ljava/lang/String;)V

    goto/16 :goto_16

    .line 1596
    :cond_1d
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeVisible(Z)V

    goto/16 :goto_17

    .line 1605
    :cond_1e
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    move/from16 v1, v21

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeMinMaxValue(FF)V

    goto/16 :goto_18

    .line 1613
    :cond_1f
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    invoke-virtual {v0, v15}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeFillColor(I)V

    goto/16 :goto_19

    .line 1620
    :cond_20
    aget-object v88, v46, v14

    .line 1621
    move-object/from16 v0, v88

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setNormalRangeColorInRange(I)V

    goto/16 :goto_1a

    .line 1628
    :cond_21
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    move/from16 v1, v86

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingVisible(Z)V

    goto/16 :goto_1b

    .line 1635
    :cond_22
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    move/from16 v1, v84

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingSize(F)V

    goto/16 :goto_1c

    .line 1642
    :cond_23
    aget-object v88, v46, v14

    .line 1643
    move-object/from16 v0, v88

    move/from16 v1, v76

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingNormalColor(I)V

    goto/16 :goto_1d

    .line 1650
    :cond_24
    aget-object v88, v46, v14

    .line 1651
    move-object/from16 v0, v88

    move-object/from16 v1, v78

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1e

    .line 1658
    :cond_25
    aget-object v88, v46, v14

    .line 1659
    move-object/from16 v0, v88

    move/from16 v1, v72

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingInRangeColor(I)V

    goto/16 :goto_1f

    .line 1666
    :cond_26
    aget-object v88, v46, v14

    .line 1667
    move-object/from16 v0, v88

    move-object/from16 v1, v74

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingInRangeImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_20

    .line 1674
    :cond_27
    aget-object v88, v46, v14

    .line 1675
    move-object/from16 v0, v88

    move/from16 v1, v68

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingHandlerOverColor(I)V

    goto/16 :goto_21

    .line 1682
    :cond_28
    aget-object v88, v46, v14

    .line 1683
    move-object/from16 v0, v88

    move-object/from16 v1, v70

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingHandlerOverInRangeImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_22

    .line 1699
    :cond_29
    aget-object v88, v46, v14

    .line 1700
    move-object/from16 v0, v88

    move/from16 v1, v80

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingSelectedColor(I)V

    goto/16 :goto_23

    .line 1707
    :cond_2a
    aget-object v88, v46, v14

    .line 1708
    move-object/from16 v0, v88

    move-object/from16 v1, v82

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setValueMarkingSelectedImage(Landroid/graphics/Bitmap;)V

    goto/16 :goto_24

    .line 1714
    :cond_2b
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineVisible(Z)V

    goto/16 :goto_25

    .line 1720
    :cond_2c
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineValue(F)V

    goto/16 :goto_26

    .line 1727
    :cond_2d
    aget-object v88, v46, v14

    float-to-int v0, v12

    move/from16 v89, v0

    move/from16 v0, v89

    int-to-float v0, v0

    move/from16 v89, v0

    invoke-virtual/range {v88 .. v89}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineThickness(F)V

    goto/16 :goto_27

    .line 1733
    :cond_2e
    aget-object v88, v46, v14

    move-object/from16 v0, v88

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->setGoalLineColor(I)V

    goto/16 :goto_28
.end method

.method public insertData(IILcom/samsung/android/sdk/chart/series/SchartTimeData;)V
    .locals 3
    .param p1, "dataType"    # I
    .param p2, "seriesID"    # I
    .param p3, "insertData"    # Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    .prologue
    .line 3317
    const/16 v1, 0x10

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 3318
    .local v0, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 3320
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 3321
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v1

    long-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 3323
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v1, v2, p1, p2, v0}, Lcom/sec/dmc/sic/jni/ChartJNI;->InsertData(JIILjava/nio/ByteBuffer;)V

    .line 3325
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 3326
    return-void
.end method

.method protected internalNotifyRenderThread()V
    .locals 2

    .prologue
    .line 2779
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2780
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    monitor-enter v1

    .line 2781
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 2780
    monitor-exit v1

    .line 2783
    :cond_0
    return-void

    .line 2780
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected mPopupInformation(Lcom/samsung/android/sdk/chart/view/SchartToolTipData;I)V
    .locals 2
    .param p1, "data"    # Lcom/samsung/android/sdk/chart/view/SchartToolTipData;
    .param p2, "visible"    # I

    .prologue
    .line 2094
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->customPopup:Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;

    if-eqz v0, :cond_0

    .line 2095
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->customPopup:Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;

    if-nez p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->popupInformation(Lcom/samsung/android/sdk/chart/view/SchartToolTipData;Z)V

    .line 2096
    :cond_0
    return-void

    .line 2095
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected notifyRenderThread()V
    .locals 2

    .prologue
    .line 2772
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    if-nez v0, :cond_0

    .line 2773
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    monitor-enter v1

    .line 2774
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 2773
    monitor-exit v1

    .line 2776
    :cond_0
    return-void

    .line 2773
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 4468
    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    .line 4470
    const-string v0, "ChartView RenderThread"

    const-string/jumbo v1, "onAttachedToWindow "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4471
    return-void
.end method

.method protected onClickTitle(I)V
    .locals 0
    .param p1, "seriesIndex"    # I

    .prologue
    .line 2090
    return-void
.end method

.method protected onCreateHandlerTimer()V
    .locals 0

    .prologue
    .line 2111
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->createHandlerTimer()V

    .line 2112
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 4406
    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    .line 4407
    const-string v0, "ChartView RenderThread"

    const-string/jumbo v1, "onDetachedFromWindow1 "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4409
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDetachedFromWindow:Z

    .line 4438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    .line 4440
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    if-eqz v0, :cond_0

    .line 4441
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->finish()V

    .line 4442
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->internalNotifyRenderThread()V

    .line 4445
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4453
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 4454
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 4455
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 4456
    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    .line 4459
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 4460
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->destroy(J)V

    .line 4461
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    .line 4463
    :cond_2
    return-void

    .line 4447
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 2710
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDoubleTap:Z

    .line 2712
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->getDataZoomEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2721
    :goto_0
    return v1

    .line 2716
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->resetZoom()V

    .line 2720
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2727
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2672
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 2679
    const/4 v0, 0x0

    return v0
.end method

.method protected onHandlerInfo(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartHandlerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2051
    .local p1, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartHandlerData;>;"
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$3;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$3;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;Ljava/util/ArrayList;)V

    .line 2059
    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$3;->start()V

    .line 2060
    return-void
.end method

.method protected onHandlerVisible()V
    .locals 1

    .prologue
    .line 2116
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getListener()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2117
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getListener()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;->OnVisible()V

    .line 2119
    :cond_0
    return-void
.end method

.method protected onLeftScrollDataChange(J)V
    .locals 1
    .param p1, "dataInfo"    # J

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    if-eqz v0, :cond_0

    .line 2008
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;->onLeftScrollDataChange(J)V

    .line 2010
    :cond_0
    return-void
.end method

.method protected onLeftScrollDataChangeComming(J)V
    .locals 1
    .param p1, "dataInfo"    # J

    .prologue
    .line 2023
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    if-eqz v0, :cond_0

    .line 2024
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;->onLeftScrollDataChangeComming(J)V

    .line 2026
    :cond_0
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2686
    return-void
.end method

.method protected onPopupInfo(Lcom/samsung/android/sdk/chart/view/SchartToolTipData;)V
    .locals 1
    .param p1, "data"    # Lcom/samsung/android/sdk/chart/view/SchartToolTipData;

    .prologue
    .line 2078
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$4;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$4;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;Lcom/samsung/android/sdk/chart/view/SchartToolTipData;)V

    .line 2084
    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$4;->start()V

    .line 2085
    return-void
.end method

.method protected onRightScrollDataChange(J)V
    .locals 1
    .param p1, "dataInfo"    # J

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    if-eqz v0, :cond_0

    .line 2016
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;->onRightScrollDataChange(J)V

    .line 2018
    :cond_0
    return-void
.end method

.method protected onRightScrollDataChangeComming(J)V
    .locals 1
    .param p1, "dataInfo"    # J

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    if-eqz v0, :cond_0

    .line 2032
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;->onRightScrollDataChangeComming(J)V

    .line 2034
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    .line 2692
    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2699
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2733
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 2704
    const/4 v0, 0x0

    return v0
.end method

.method public onStep()V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    const/high16 v3, 0x41a00000    # 20.0f

    const v7, 0x3dcccccd    # 0.1f

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2974
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    if-nez v1, :cond_1

    .line 3095
    :cond_0
    :goto_0
    return-void

    .line 2977
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->ProcessProperty()V

    .line 2979
    const/4 v0, 0x0

    .line 2980
    .local v0, "isTraceUsed":Z
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    if-eqz v1, :cond_8

    .line 2981
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    const v2, 0x3f75c28f    # 0.96f

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    .line 2982
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 2996
    :cond_2
    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    if-eqz v1, :cond_3

    .line 2998
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    .line 2999
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    .line 3003
    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_9

    .line 3005
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_4

    .line 3007
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    iget v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    invoke-static {v1, v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetVScroll(JF)V

    .line 3021
    :cond_4
    :goto_2
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    .line 3022
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 3025
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    if-eqz v1, :cond_5

    .line 3026
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v7

    if-gtz v1, :cond_5

    .line 3027
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    .line 3028
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    .line 3029
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDirtyMode:Z

    .line 3031
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v1, v2, v4, v4}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnTouchUp(JFF)V

    .line 3032
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v1, v2}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyScrollEnd(J)V

    .line 3033
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v1, v2}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyFlickEnd(J)V

    .line 3035
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isHandlerOnScreen(J)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3036
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->createHandlerTimer()V

    .line 3041
    :cond_5
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    if-eqz v1, :cond_6

    .line 3043
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v7

    if-gtz v1, :cond_6

    .line 3045
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    .line 3046
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    .line 3051
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iget-wide v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->step(J)Z

    move-result v2

    iput-boolean v2, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    .line 3053
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isFirstDraw:Z

    if-eqz v1, :cond_7

    .line 3062
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iput-boolean v6, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    .line 3063
    iput-boolean v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isFirstDraw:Z

    .line 3071
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAnimating:Z

    .line 3076
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    if-nez v1, :cond_0

    .line 3078
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_d

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDown:Z

    if-nez v1, :cond_d

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    if-nez v1, :cond_d

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    if-nez v1, :cond_d

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    if-nez v1, :cond_d

    .line 3080
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 3081
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iput-boolean v6, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    goto/16 :goto_0

    .line 2985
    :cond_8
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    cmpl-float v1, v1, v4

    if-nez v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 2987
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTraceVelocity:F

    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 2988
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 3010
    :cond_9
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v3

    if-gez v1, :cond_a

    if-eqz v0, :cond_a

    .line 3011
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    iget v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    invoke-direct {p0, v1, v2, v3, v6}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetChartScroll(JFI)V

    goto/16 :goto_2

    .line 3012
    :cond_a
    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v4

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_4

    .line 3014
    if-eqz v0, :cond_b

    .line 3015
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    iget v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    invoke-direct {p0, v1, v2, v3, v6}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetChartScroll(JFI)V

    goto/16 :goto_2

    .line 3017
    :cond_b
    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    iget v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    invoke-direct {p0, v1, v2, v3, v5}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetChartScroll(JFI)V

    goto/16 :goto_2

    .line 3084
    :cond_c
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iput-boolean v5, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    goto/16 :goto_0

    .line 3089
    :cond_d
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iput-boolean v6, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    goto/16 :goto_0
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2815
    const-string v0, "ChartView RenderThread"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onSurfaceTextureAvailable : this, surface "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "        "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2818
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInit:Z

    if-nez v0, :cond_2

    .line 2820
    new-instance v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-direct {v0, p0, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;-><init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    .line 2822
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 2824
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    if-eqz v0, :cond_0

    .line 2825
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    monitor-enter v1

    .line 2826
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    .line 2825
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2831
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->start()V

    .line 2885
    :cond_1
    :goto_0
    return-void

    .line 2825
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2859
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    if-eqz v0, :cond_1

    .line 2860
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    monitor-enter v1

    .line 2861
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    .line 2864
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_3

    .line 2865
    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->release()V

    .line 2867
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    .line 2860
    :cond_3
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2873
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mLastTraceStatus:Z

    if-eqz v0, :cond_4

    .line 2874
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mLastTraceStatus:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    .line 2876
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto :goto_0

    .line 2860
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 4
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 2889
    const-string v1, "ChartView RenderThread"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onSurfaceTextureDestroyed : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "        "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2905
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    if-eqz v1, :cond_0

    .line 2906
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    monitor-enter v2

    .line 2909
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getTraceMode()Z

    move-result v3

    iput-boolean v3, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mLastTraceStatus:Z

    .line 2910
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    .line 2911
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    .line 2906
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2915
    :cond_0
    const/4 v0, 0x0

    .line 2917
    .local v0, "ret":Z
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDetachedFromWindow:Z

    if-eqz v1, :cond_1

    .line 2918
    const/4 v0, 0x1

    .line 2921
    :cond_1
    return v0

    .line 2906
    .end local v0    # "ret":Z
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 2
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2926
    const-string v0, "ChartView RenderThread"

    const-string/jumbo v1, "onSurfaceTextureSizeChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2929
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    if-eqz v0, :cond_0

    .line 2930
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->setWH(II)V

    .line 2931
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->onSurfaceChanged(II)V

    .line 2933
    :cond_0
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 1
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 2951
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mReadyToShowListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstFrame:Z

    if-eqz v0, :cond_0

    .line 2952
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mReadyToShowListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;->onChartReadyToShow(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    .line 2953
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstFrame:Z

    .line 2955
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 17
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2204
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isFirstDraw:Z

    if-eqz v2, :cond_0

    .line 2205
    const/4 v2, 0x0

    .line 2666
    :goto_0
    return v2

    .line 2208
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->getInteractionEnable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2210
    const/4 v2, 0x0

    goto :goto_0

    .line 2213
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    if-eqz v2, :cond_2

    .line 2214
    const/4 v2, 0x0

    goto :goto_0

    .line 2217
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAnimating:Z

    if-eqz v2, :cond_3

    .line 2221
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    const/4 v3, 0x1

    if-eq v2, v3, :cond_3

    .line 2222
    const/4 v2, 0x0

    goto :goto_0

    .line 2225
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    if-eqz v2, :cond_4

    .line 2229
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    invoke-virtual {v2}, Ljava/util/TimerTask;->cancel()Z

    .line 2230
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 2237
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    if-eqz v2, :cond_5

    .line 2241
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    .line 2242
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->purge()I

    .line 2243
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2251
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRefView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    if-eqz v2, :cond_6

    .line 2253
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRefView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRefView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2256
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGes:Landroid/view/GestureDetector;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2259
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2261
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 2665
    :cond_7
    :goto_3
    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    .line 2666
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2264
    :pswitch_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDirtyMode:Z

    .line 2266
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    .line 2268
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 2269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_realFirstPoint:Landroid/graphics/PointF;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 2271
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 2272
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 2271
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->GetPickingObjectType(JFF)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    .line 2274
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_9

    .line 2277
    const-string v2, "SIC"

    const-string v3, "---picked DRAWING_LEGEND_TYPE---"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2309
    :cond_8
    :goto_4
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 2317
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTracker:Landroid/view/VelocityTracker;

    .line 2318
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownX:F

    .line 2319
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownY:F

    .line 2320
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    .line 2321
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyFlickEnd(J)V

    .line 2322
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 2323
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    .line 2324
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    .line 2325
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsDown:Z

    .line 2327
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyScrollStart(J)V

    goto/16 :goto_3

    .line 2294
    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_8

    .line 2295
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 2296
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 2295
    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->IsGraphRegion(JFF)Z

    move-result v2

    .line 2296
    if-nez v2, :cond_8

    .line 2298
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 2303
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto :goto_4

    .line 2336
    :pswitch_2
    const/4 v8, 0x0

    .line 2337
    .local v8, "checkTimerCreate":Z
    const/4 v11, 0x0

    .line 2339
    .local v11, "isCloseTouch":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_realFirstPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    .line 2340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_realFirstPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41a00000    # 20.0f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_a

    .line 2341
    const/4 v11, 0x1

    .line 2344
    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_e

    .line 2345
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const/high16 v4, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SettingHandlerMagneticPosition(JF)V

    .line 2346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    .line 2347
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    .line 2383
    :goto_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_c

    .line 2386
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnTouchUp(JFF)V

    .line 2389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    .line 2390
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    .line 2392
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->recycle()V

    .line 2393
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsDown:Z

    .line 2394
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVelocity:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41a00000    # 20.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_11

    .line 2395
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDirtyMode:Z

    .line 2396
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    .line 2397
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyFlickStart(J)V

    .line 2410
    :goto_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartType:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_12

    .line 2412
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScrollVelocity:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x41a00000    # 20.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_b

    .line 2413
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    .line 2422
    :cond_b
    :goto_7
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 2426
    :cond_c
    if-eqz v8, :cond_d

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    if-nez v2, :cond_d

    .line 2428
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isHandlerOnScreen(J)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2431
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->createHandlerTimer()V

    .line 2460
    :cond_d
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDoubleTap:Z

    if-eqz v2, :cond_7

    .line 2461
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isDoubleTap:Z

    .line 2462
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isZoom:Z

    goto/16 :goto_3

    .line 2351
    :cond_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_f

    .line 2359
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->CheckSpotInfo(JF)V

    .line 2361
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SettingHandlerMagneticPosition(JF)V

    .line 2362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    .line 2363
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto/16 :goto_5

    .line 2369
    :cond_f
    if-eqz v11, :cond_10

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_10

    .line 2373
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->CheckSpotInfo(JF)V

    .line 2375
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SettingHandlerMagneticPosition(JF)V

    .line 2376
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    .line 2377
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto/16 :goto_5

    .line 2379
    :cond_10
    const/4 v8, 0x1

    goto/16 :goto_5

    .line 2401
    :cond_11
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyScrollEnd(J)V

    .line 2402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2406
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto/16 :goto_6

    .line 2418
    :cond_12
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    .line 2419
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    goto/16 :goto_7

    .line 2436
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    if-eqz v2, :cond_14

    .line 2439
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    invoke-virtual {v2}, Ljava/util/TimerTask;->cancel()Z

    .line 2440
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTask:Ljava/util/TimerTask;

    .line 2443
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    if-eqz v2, :cond_d

    .line 2446
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    .line 2447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2}, Ljava/util/Timer;->purge()I

    .line 2448
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTimer:Ljava/util/Timer;

    goto/16 :goto_8

    .line 2468
    .end local v8    # "checkTimerCreate":Z
    .end local v11    # "isCloseTouch":Z
    :pswitch_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_15

    .line 2469
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstDataPrepareZoom:Z

    .line 2471
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetReleaseZoom(J)V

    .line 2473
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnTouchUp(JFF)V

    .line 2474
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 2476
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyScrollEnd(J)V

    .line 2477
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2481
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto/16 :goto_3

    .line 2485
    :cond_15
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnTouchUp(JFF)V

    .line 2486
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 2488
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2492
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto/16 :goto_3

    .line 2500
    :pswitch_4
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTracker:Landroid/view/VelocityTracker;

    .line 2501
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownX:F

    .line 2502
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFlick:Z

    .line 2503
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->OnNotifyFlickEnd(J)V

    .line 2506
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 2507
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    .line 2508
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsDown:Z

    .line 2511
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_ZoomX:F

    .line 2512
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->spacing(Landroid/view/MotionEvent;)F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->oldDist:F

    .line 2513
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mid:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 2516
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 2517
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 2516
    invoke-static {v2, v3, v4, v5}, Lcom/sec/dmc/sic/jni/ChartJNI;->IsGraphRegion(JFF)Z

    move-result v2

    .line 2517
    if-nez v2, :cond_16

    .line 2518
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 2522
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    .line 2523
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2526
    :cond_16
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstDataPrepareZoom:Z

    .line 2527
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    .line 2528
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->isZoom:Z

    .line 2530
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownY:F

    .line 2531
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVertFlick:Z

    .line 2532
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    goto/16 :goto_3

    .line 2540
    :pswitch_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_17

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_22

    .line 2554
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_realFirstPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mAccMoveDistanceX:F

    .line 2555
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_secondPoint:Landroid/graphics/PointF;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 2556
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_secondPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float v15, v2, v3

    .line 2557
    .local v15, "offsetX":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_secondPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float v16, v2, v3

    .line 2559
    .local v16, "offsetY":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_1a

    .line 2561
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4, v15}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetHandlerTranslate(JIF)V

    .line 2607
    :cond_18
    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_secondPoint:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 2609
    const/4 v2, 0x0

    cmpl-float v2, v15, v2

    if-nez v2, :cond_19

    const/4 v2, 0x0

    cmpl-float v2, v16, v2

    if-eqz v2, :cond_7

    .line 2610
    :cond_19
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    goto/16 :goto_3

    .line 2563
    :cond_1a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->pickType:I

    const/16 v3, 0xd

    if-ne v2, v3, :cond_1b

    .line 2564
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const/4 v4, 0x0

    move/from16 v0, v16

    invoke-static {v2, v3, v4, v0}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetGoalLineTranslate(JIF)V

    goto :goto_9

    .line 2567
    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->getDataZoomEnable()Z

    move-result v2

    if-nez v2, :cond_1e

    .line 2569
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetCanZoomPanning(J)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 2570
    const/4 v2, 0x0

    cmpl-float v2, v15, v2

    if-nez v2, :cond_1c

    const/4 v2, 0x0

    cmpl-float v2, v16, v2

    if-eqz v2, :cond_1d

    .line 2571
    :cond_1c
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v3, v15, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetZoomPanning(JFF)V

    .line 2572
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_firstPoint:Landroid/graphics/PointF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->m_secondPoint:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 2576
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    .line 2578
    :cond_1d
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2582
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->getScrollEnable()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2586
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsDown:Z

    if-eqz v2, :cond_20

    .line 2587
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTracker:Landroid/view/VelocityTracker;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 2589
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    .line 2590
    .local v9, "fNowx":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1f

    .line 2591
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownX:F

    sub-float v2, v9, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mMoveDistance:F

    .line 2593
    :cond_1f
    move-object/from16 v0, p0

    iput v9, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownX:F

    .line 2597
    .end local v9    # "fNowx":F
    :cond_20
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsDown:Z

    if-eqz v2, :cond_18

    .line 2598
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    .line 2599
    .local v10, "fNowy":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_21

    .line 2600
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownY:F

    sub-float v2, v10, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mVScroll:F

    .line 2602
    :cond_21
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDownY:F

    goto/16 :goto_9

    .line 2613
    .end local v10    # "fNowy":F
    .end local v15    # "offsetX":F
    .end local v16    # "offsetY":F
    :cond_22
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    .line 2614
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->spacing(Landroid/view/MotionEvent;)F

    move-result v13

    .line 2615
    .local v13, "newDist":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->oldDist:F

    sub-float v14, v13, v2

    .line 2617
    .local v14, "offset":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->getDataZoomEnable()Z

    move-result v2

    if-eqz v2, :cond_25

    .line 2618
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstDataPrepareZoom:Z

    if-eqz v2, :cond_23

    const/4 v2, 0x0

    cmpl-float v2, v14, v2

    if-eqz v2, :cond_23

    .line 2622
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsFirstDataPrepareZoom:Z

    .line 2625
    :cond_23
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mid:Landroid/graphics/PointF;

    iget v5, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mid:Landroid/graphics/PointF;

    iget v6, v2, Landroid/graphics/PointF;->y:F

    const/high16 v2, 0x40200000    # 2.5f

    mul-float v7, v14, v2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetDataZoom(JFFF)V

    .line 2657
    :cond_24
    :goto_a
    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->oldDist:F

    goto/16 :goto_3

    .line 2628
    :cond_25
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetCanZoomPanning(J)Z

    move-result v2

    if-nez v2, :cond_26

    const/4 v2, 0x0

    cmpl-float v2, v14, v2

    if-lez v2, :cond_26

    .line 2630
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->SetPrepareZoom(J)V

    .line 2632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    if-eqz v2, :cond_26

    .line 2633
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getWidth()I

    move-result v2

    .line 2634
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getHeight()I

    move-result v3

    .line 2633
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 2636
    .local v12, "mapBitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_26

    .line 2640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    invoke-virtual {v2, v12}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setMapBitmap(Landroid/graphics/Bitmap;)V

    .line 2645
    .end local v12    # "mapBitmap":Landroid/graphics/Bitmap;
    :cond_26
    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mid:Landroid/graphics/PointF;

    iget v5, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mid:Landroid/graphics/PointF;

    iget v6, v2, Landroid/graphics/PointF;->y:F

    const/high16 v2, 0x40200000    # 2.5f

    mul-float v7, v14, v2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetZoom(JFFF)V

    .line 2648
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v2, v3}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetCanZoomPanning(J)Z

    move-result v2

    if-nez v2, :cond_24

    .line 2649
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    if-eqz v2, :cond_24

    .line 2650
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setVisibility(I)V

    goto :goto_a

    .line 2245
    .end local v13    # "newDist":F
    .end local v14    # "offset":F
    :catch_0
    move-exception v2

    goto/16 :goto_2

    .line 2232
    :catch_1
    move-exception v2

    goto/16 :goto_1

    .line 2261
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method protected onValueMarkingInfo(Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;)V
    .locals 2
    .param p1, "data"    # Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;

    .prologue
    .line 2123
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 2128
    return-void

    .line 2125
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getListener()Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2126
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getListener()Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;->OnGraphData(Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;)V

    .line 2123
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onZoomMap(FFFFFFFF)V
    .locals 9
    .param p1, "leftRate"    # F
    .param p2, "topRate"    # F
    .param p3, "widthRate"    # F
    .param p4, "HeightRate"    # F
    .param p5, "currentRegionWidth"    # F
    .param p6, "currentRegionHeight"    # F
    .param p7, "originalRegionWidth"    # F
    .param p8, "originalRegionHeight"    # F

    .prologue
    .line 2102
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    if-eqz v0, :cond_0

    .line 2103
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->updateZoomRegionRect(FFFFFFFF)V

    .line 2106
    :cond_0
    return-void
.end method

.method public resetTraceMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3496
    iput-boolean v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    .line 3497
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTraceVelocity:F

    .line 3498
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDataType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 3499
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setTraceMode(JZ)V

    .line 3501
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 3502
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    .line 3503
    return-void
.end method

.method protected resetZoom()V
    .locals 3

    .prologue
    .line 2738
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetCanZoomPanning(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2740
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    if-eqz v0, :cond_0

    .line 2741
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setVisibility(I)V

    .line 2743
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const/16 v2, 0x320

    invoke-static {v0, v1, v2}, Lcom/sec/dmc/sic/jni/ChartJNI;->StartRendererZoomOutAni(JI)Z

    .line 2745
    :cond_1
    return-void
.end method

.method public resetZoomWithAnimation()V
    .locals 0

    .prologue
    .line 2768
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->resetZoom()V

    .line 2769
    return-void
.end method

.method public resetZoomWithoutAnimation()V
    .locals 2

    .prologue
    .line 2753
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->GetIsZoom(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2754
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    if-eqz v0, :cond_0

    .line 2755
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setVisibility(I)V

    .line 2757
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->ResetZoom(J)V

    .line 2760
    :cond_1
    return-void
.end method

.method public setChartBitmapListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;)V
    .locals 0
    .param p1, "chartBitmapListener"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;

    .prologue
    .line 3797
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartBitmapListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartChartBitmapListener;

    .line 3798
    return-void
.end method

.method public setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V
    .locals 3
    .param p1, "interaction"    # Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    .prologue
    const/4 v2, 0x0

    .line 1989
    invoke-virtual {p1}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->getInteractionProperty()Lcom/sec/dmc/sic/android/property/InteractionProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    .line 1991
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartType:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartType:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 1992
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScrollEnable(Z)V

    .line 1994
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    if-eqz v0, :cond_2

    .line 1996
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomEnable(Z)V

    .line 1997
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScaleZoomEnable(Z)V

    .line 1998
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setPartialZoomEnable(Z)V

    .line 2001
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 2002
    return-void
.end method

.method protected setChartType(I)V
    .locals 0
    .param p1, "chartType"    # I

    .prologue
    .line 3523
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartType:I

    .line 3524
    return-void
.end method

.method public setCustomHandler(Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;)V
    .locals 0
    .param p1, "customHandler"    # Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;

    .prologue
    .line 3793
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mCustomHandler:Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;

    .line 3794
    return-void
.end method

.method public setCustomPopup(Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;)V
    .locals 0
    .param p1, "customPopup"    # Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;

    .prologue
    .line 3772
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->customPopup:Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;

    .line 3773
    return-void
.end method

.method protected setDataType(I)V
    .locals 0
    .param p1, "DataType"    # I

    .prologue
    .line 3515
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDataType:I

    .line 3516
    return-void
.end method

.method public setMap(Lcom/samsung/android/sdk/chart/view/SchartZoomMap;)V
    .locals 1
    .param p1, "map"    # Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setRefChartView(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V

    .line 219
    return-void
.end method

.method public setPopupShow(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2193
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual {p0, v0, v1, p1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setPopupShow(JI)V

    .line 2194
    return-void
.end method

.method native setPopupShow(JI)V
.end method

.method public setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V
    .locals 6
    .param p1, "property"    # Lcom/sec/dmc/sic/android/property/BaseProperty;

    .prologue
    .line 3544
    const/4 v0, 0x1

    .line 3547
    .local v0, "b_add":Z
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 3549
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lt v3, v4, :cond_2

    .line 3643
    .end local v3    # "i":I
    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    .line 3647
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/dmc/sic/android/property/BaseProperty;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/dmc/sic/android/property/BaseProperty;

    .line 3648
    .local v1, "clonedProperty":Lcom/sec/dmc/sic/android/property/BaseProperty;
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3660
    .end local v1    # "clonedProperty":Lcom/sec/dmc/sic/android/property/BaseProperty;
    :cond_1
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    .line 3661
    return-void

    .line 3551
    .restart local v3    # "i":I
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/GraphProperty;

    if-ne v4, v5, :cond_4

    .line 3553
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getGraphID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getGraphID()I

    move-result v4

    if-ne v5, v4, :cond_d

    .line 3555
    const/4 v0, 0x0

    .line 3636
    :cond_3
    :goto_3
    if-nez v0, :cond_d

    .line 3637
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3, p1}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 3558
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3559
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_5

    .line 3560
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/AxisProperty;

    if-ne v4, v5, :cond_5

    .line 3561
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAxisID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 3562
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAxisID()I

    move-result v4

    .line 3561
    if-ne v5, v4, :cond_d

    .line 3563
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 3564
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTypeXY()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 3565
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTypeXY()I

    move-result v4

    .line 3563
    if-ne v5, v4, :cond_d

    .line 3566
    const/4 v0, 0x0

    .line 3569
    goto :goto_3

    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3570
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_6

    .line 3571
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    if-ne v4, v5, :cond_6

    .line 3572
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->getSeriesID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    .line 3573
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->getSeriesID()I

    move-result v4

    .line 3572
    if-ne v5, v4, :cond_d

    .line 3574
    const/4 v0, 0x0

    .line 3577
    goto/16 :goto_3

    :cond_6
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3578
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_7

    .line 3579
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    if-ne v4, v5, :cond_7

    .line 3580
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    .line 3581
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getValueMarkingID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    .line 3582
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getValueMarkingID()I

    move-result v4

    .line 3580
    if-ne v5, v4, :cond_d

    .line 3583
    const/4 v0, 0x0

    .line 3586
    goto/16 :goto_3

    :cond_7
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3587
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_8

    .line 3588
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    if-ne v4, v5, :cond_8

    .line 3589
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    .line 3590
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getNormalRangeID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    .line 3591
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getNormalRangeID()I

    move-result v4

    .line 3589
    if-ne v5, v4, :cond_d

    .line 3592
    const/4 v0, 0x0

    .line 3595
    goto/16 :goto_3

    :cond_8
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3596
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_9

    .line 3597
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    if-ne v4, v5, :cond_9

    .line 3598
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    .line 3599
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    .line 3600
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineID()I

    move-result v4

    .line 3598
    if-ne v5, v4, :cond_d

    .line 3601
    const/4 v0, 0x0

    .line 3604
    goto/16 :goto_3

    :cond_9
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3605
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_a

    .line 3606
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    if-ne v4, v5, :cond_a

    .line 3607
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    .line 3608
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getSeriesID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    .line 3609
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getSeriesID()I

    move-result v4

    .line 3607
    if-ne v5, v4, :cond_d

    .line 3610
    const/4 v0, 0x0

    .line 3613
    goto/16 :goto_3

    :cond_a
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3614
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_b

    .line 3615
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    if-ne v4, v5, :cond_b

    .line 3616
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    .line 3617
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getSeriesID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    .line 3618
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getSeriesID()I

    move-result v4

    .line 3616
    if-ne v5, v4, :cond_d

    .line 3619
    const/4 v0, 0x0

    .line 3622
    goto/16 :goto_3

    :cond_b
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3623
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_c

    .line 3624
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    if-ne v4, v5, :cond_c

    .line 3625
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    .line 3626
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->getSeriesID()I

    move-result v5

    move-object v4, p1

    check-cast v4, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    .line 3627
    invoke-virtual {v4}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->getSeriesID()I

    move-result v4

    .line 3625
    if-ne v5, v4, :cond_d

    .line 3628
    const/4 v0, 0x0

    .line 3631
    goto/16 :goto_3

    :cond_c
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->processProperty:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 3632
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_3

    .line 3633
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 3549
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 3651
    .end local v3    # "i":I
    :catch_0
    move-exception v2

    .line 3653
    .local v2, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v2}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public setReadyToShowListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;)V
    .locals 0
    .param p1, "readyToShowListener"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;

    .prologue
    .line 3801
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mReadyToShowListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartReadyToShowListener;

    .line 3802
    return-void
.end method

.method public setReferenceView(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V
    .locals 0
    .param p1, "mRefView"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRefView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .line 238
    return-void
.end method

.method public setTraceMode(F)V
    .locals 4
    .param p1, "velocity"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3458
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    if-nez v0, :cond_1

    .line 3460
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false : You should set a chart "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3479
    :cond_0
    :goto_0
    return-void

    .line 3463
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mDataType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 3465
    iput-boolean v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    .line 3466
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTraceVelocity:F

    .line 3467
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-virtual {p0, v0, v1, v3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setTraceMode(JZ)V

    .line 3469
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mTrace:Z

    if-eqz v0, :cond_2

    .line 3471
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomEnable(Z)V

    .line 3472
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScaleZoomEnable(Z)V

    .line 3473
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setPartialZoomEnable(Z)V

    .line 3475
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 3477
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto :goto_0
.end method

.method native setTraceMode(JZ)V
.end method

.method public setXYChartScrollDataChangeListener(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;)V
    .locals 0
    .param p1, "xyChartScrollDataChangeListener"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    .prologue
    .line 3813
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->xyChartScrollDataChangeListener:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;

    .line 3814
    return-void
.end method

.method protected startGraphRevealAnimation()V
    .locals 5

    .prologue
    .line 3436
    iget-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    if-nez v1, :cond_0

    .line 3449
    :goto_0
    return-void

    .line 3439
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniGraphIdxVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 3445
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniGraphIdxVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 3446
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniDurationVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    goto :goto_0

    .line 3440
    :cond_1
    iget-wide v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    .line 3441
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniGraphIdxVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 3442
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniDurationVector:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 3440
    invoke-static {v2, v3, v4, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->StartGraphRevealAni(JII)Z

    .line 3439
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public startGraphRevealAnimation(II)Z
    .locals 3
    .param p1, "graphIdx"    # I
    .param p2, "duration"    # I

    .prologue
    .line 3426
    const/4 v0, 0x1

    .line 3429
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniGraphIdxVector:Ljava/util/Vector;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3430
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGraphAniDurationVector:Ljava/util/Vector;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 3432
    return v0
.end method
