.class public Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;
.source "SchartTimeStackedBarChartView.java"


# instance fields
.field private timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x2

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->setDataType(I)V

    .line 36
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->createChart()V

    .line 38
    new-instance v0, Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setChartHandle(J)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    const/4 v0, 0x2

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->setDataType(I)V

    .line 51
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->createChart()V

    .line 53
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mSeriesNum:I

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->createSeries(II)V

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 57
    new-instance v0, Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setChartHandle(J)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;-><init>(Landroid/content/Context;)V

    .line 71
    const/4 v1, 0x1

    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->createSeries(II)V

    .line 73
    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfXAxis()I

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfYAxis()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->addAxis(II)V

    .line 75
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 78
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 81
    return-void

    .line 79
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->addData(ILjava/util/List;)V

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addData(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 171
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 173
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->addData(IILjava/util/List;)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 205
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;->clearData()V

    goto :goto_0
.end method

.method public prepareChangeData(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 189
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 191
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->prepareChangeData(IILjava/util/List;)V

    goto :goto_0
.end method

.method public setHandlerStartDate(D)V
    .locals 2
    .param p1, "handlerStartDate"    # D

    .prologue
    .line 219
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 221
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setHandlerStartDate(D)V

    goto :goto_0
.end method

.method public setScrollRangeDepthLevel(I)V
    .locals 1
    .param p1, "timeLevel"    # I

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setScrollRangeDepthLevel(I)V

    .line 241
    return-void
.end method

.method public setScrollRangeType(I)V
    .locals 1
    .param p1, "rangeType"    # I

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setScrollRangeType(I)V

    .line 255
    return-void
.end method

.method public setStartDate(D)V
    .locals 2
    .param p1, "startDate"    # D

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 136
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartDate(D)V

    .line 140
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartDepthLevel(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 119
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartDepthLevel(I)V

    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartMainMarking(II)V
    .locals 2
    .param p1, "scaleIntervalMainMarking"    # I
    .param p2, "nMainMarkingCount"    # I

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 154
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartMainMarking(II)V

    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartVisual(IDII)V
    .locals 6
    .param p1, "level"    # I
    .param p2, "startDate"    # D
    .param p4, "scaleIntervalMainMarking"    # I
    .param p5, "nMainMarkingCount"    # I

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 100
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartVisual(IDII)V

    .line 104
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setUserScrollRange(JJ)V
    .locals 1
    .param p1, "userLeftStart"    # J
    .param p3, "userRightEnd"    # J

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeStackedBarChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/dmc/sic/android/view/TimeChart;->setUserScrollRange(JJ)V

    .line 268
    return-void
.end method
