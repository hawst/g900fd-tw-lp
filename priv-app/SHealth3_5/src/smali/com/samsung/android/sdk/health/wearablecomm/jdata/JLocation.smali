.class public Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;
.super Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHealth;
.source "JLocation.java"


# instance fields
.field public accuracy:F

.field public altitude:D

.field public bearing:F

.field public extra:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExtra;

.field public extraLocation:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocationBundle;

.field public latitude:D

.field public longitude:D

.field public speed:F

.field public time:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHealth;-><init>()V

    return-void
.end method
