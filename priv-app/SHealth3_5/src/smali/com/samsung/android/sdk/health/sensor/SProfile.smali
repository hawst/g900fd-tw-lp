.class public Lcom/samsung/android/sdk/health/sensor/SProfile;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/SProfile$ActivityType;,
        Lcom/samsung/android/sdk/health/sensor/SProfile$DistanceUnit;,
        Lcom/samsung/android/sdk/health/sensor/SProfile$Gender;,
        Lcom/samsung/android/sdk/health/sensor/SProfile$HeightUnit;,
        Lcom/samsung/android/sdk/health/sensor/SProfile$WeightUnit;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public activityClass:I

.field public age:I

.field public birthday:J

.field public distanceUnit:I

.field public gender:I

.field public height:F

.field public heightUnit:I

.field public time:J

.field public weight:F

.field public weightUnit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SProfile$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SProfile$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 134
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const v0, 0x7fffffff

    .line 68
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 47
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    .line 50
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->height:F

    .line 52
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    .line 54
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->gender:I

    .line 56
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->age:I

    .line 58
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    .line 60
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weightUnit:I

    .line 62
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->heightUnit:I

    .line 64
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    .line 66
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->birthday:J

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    const v0, 0x7fffffff

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 47
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    .line 50
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->height:F

    .line 52
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    .line 54
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->gender:I

    .line 56
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->age:I

    .line 58
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    .line 60
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weightUnit:I

    .line 62
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->heightUnit:I

    .line 64
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    .line 66
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->birthday:J

    .line 74
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SProfile;->readFromParcel(Landroid/os/Parcel;)V

    .line 75
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->height:F

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->gender:I

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->age:I

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weightUnit:I

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->heightUnit:I

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->birthday:J

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->extra:Landroid/os/Bundle;

    .line 120
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[PROFILE] time="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->height:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 101
    const-string v1, ", weight = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gender ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->gender:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 102
    const-string v1, ", age = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->age:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", distanceUnit ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 103
    const-string v1, ", weightUnit = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weightUnit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", heightUnit ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 104
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->heightUnit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", activityClass = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", birthday ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->birthday:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 85
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->height:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 86
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weight:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 87
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->gender:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->age:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->distanceUnit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->weightUnit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->heightUnit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->activityClass:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->birthday:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SProfile;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 95
    return-void
.end method
