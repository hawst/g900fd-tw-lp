.class public Lcom/samsung/android/sdk/chart/view/SchartCandleChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartXYChartView;
.source "SchartCandleChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;)V

    .line 37
    const/4 v0, 0x6

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartCandleChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    const/4 v0, 0x6

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 50
    return-void
.end method
