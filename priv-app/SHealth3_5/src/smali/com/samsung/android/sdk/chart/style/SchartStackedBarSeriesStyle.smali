.class public Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;
.super Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
.source "SchartStackedBarSeriesStyle.java"


# static fields
.field public static final STACKED_BAR_ALIGN_BETWEEN_LEFT:I = 0x4

.field public static final STACKED_BAR_ALIGN_BETWEEN_RIGHT:I = 0x3

.field public static final STACKED_BAR_ALIGN_CENTER:I = 0x1

.field public static final STACKED_BAR_ALIGN_LEFT:I = 0x0

.field public static final STACKED_BAR_ALIGN_RIGHT:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;-><init>()V

    .line 8
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mSeriesType:I

    .line 9
    return-void
.end method


# virtual methods
.method public addItemColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->addStackedBarItemColor(I)V

    .line 53
    return-void
.end method

.method public addItemName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->addStackedBarItemName(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public getItemColor(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieItemColor(I)I

    move-result v0

    return v0
.end method

.method public getItemName(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getStackedBarItemName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setStackedBarAlign(I)V
    .locals 1
    .param p1, "align"    # I

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setAlign(I)V

    .line 30
    return-void
.end method

.method public setStackedBarWidth(F)V
    .locals 1
    .param p1, "stackedBarWidth"    # F

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setStrokeWidth(F)V

    .line 49
    return-void
.end method

.method public setStackedBarWidth(J)V
    .locals 1
    .param p1, "stackedBarWidthInTime"    # J

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartStackedBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setBarWidthInTime(J)V

    .line 40
    return-void
.end method
