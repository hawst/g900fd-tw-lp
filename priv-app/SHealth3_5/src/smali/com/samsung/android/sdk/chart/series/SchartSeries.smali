.class public abstract Lcom/samsung/android/sdk/chart/series/SchartSeries;
.super Ljava/lang/Object;
.source "SchartSeries.java"


# static fields
.field public static final CANDLE_TYPE:I = 0x2

.field public static final INVALID_VALUE:D = -3.3999999521443642E38

.field public static final XY_TYPE:I = 0x1


# instance fields
.field protected mSeriesDataType:I

.field protected mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/chart/series/SchartSeries;->mSeriesDataType:I

    .line 22
    return-void
.end method


# virtual methods
.method public getSeriesDataType()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/samsung/android/sdk/chart/series/SchartSeries;->mSeriesDataType:I

    return v0
.end method

.method public abstract getSize()I
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartSeries;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/series/SchartSeries;->mTitle:Ljava/lang/String;

    .line 45
    return-void
.end method
