.class public Lcom/samsung/android/sdk/health/sensor/utils/SharedPrefernceUtil;
.super Ljava/lang/Object;
.source "SharedPrefernceUtil.java"


# static fields
.field public static final UNIQUE_ID:Ljava/lang/String; = "hashed_device_id"

.field private static prefs:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/utils/SharedPrefernceUtil;->prefs:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDeviceUniqueId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/utils/SharedPrefernceUtil;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "hashed_device_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/utils/SharedPrefernceUtil;->prefs:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 17
    const-string/jumbo v0, "sync_preference"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/utils/SharedPrefernceUtil;->prefs:Landroid/content/SharedPreferences;

    .line 18
    :cond_0
    return-void
.end method

.method public static saveDeviceUniqueId(Ljava/lang/String;)V
    .locals 2
    .param p0, "uniqueId"    # Ljava/lang/String;

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/utils/SharedPrefernceUtil;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "hashed_device_id"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 23
    return-void
.end method
