.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$TemperatureType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TemperatureType"
.end annotation


# static fields
.field public static final BABY:I = 0x11173

.field public static final BASAL_BODY_TEMPERATURE:I = 0x11172

.field public static final BODY_TEMPERATURE:I = 0x11171

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field
