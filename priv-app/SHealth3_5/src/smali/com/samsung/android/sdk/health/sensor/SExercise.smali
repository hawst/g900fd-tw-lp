.class public Lcom/samsung/android/sdk/health/sensor/SExercise;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SExercise.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/SExercise$ExerciseInfoType;,
        Lcom/samsung/android/sdk/health/sensor/SExercise$FitnessLevel;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SExercise;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public calorie:D

.field public coachingResult:Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

.field public distance:D

.field public duration:J

.field public fitnessLevel:I

.field public heartRate:D

.field public location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

.field public time:J

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SExercise$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SExercise$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SExercise;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 144
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const/4 v3, 0x0

    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 49
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 25
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->time:J

    .line 27
    const/16 v0, 0x2712

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    .line 29
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->duration:J

    .line 31
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->calorie:D

    .line 33
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->heartRate:D

    .line 35
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->distance:D

    .line 43
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->fitnessLevel:I

    .line 45
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    .line 47
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const/4 v3, 0x0

    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 53
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 25
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->time:J

    .line 27
    const/16 v0, 0x2712

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    .line 29
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->duration:J

    .line 31
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->calorie:D

    .line 33
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->heartRate:D

    .line 35
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->distance:D

    .line 43
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->fitnessLevel:I

    .line 45
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    .line 47
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SExercise;->readFromParcel(Landroid/os/Parcel;)V

    .line 56
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->time:J

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->duration:J

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->calorie:D

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->heartRate:D

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->distance:D

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->fitnessLevel:I

    .line 120
    const-class v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    .line 122
    const-class v3, Lcom/samsung/android/sdk/health/sensor/SLocation;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 123
    .local v1, "temp":[Landroid/os/Parcelable;
    if-eqz v1, :cond_1

    .line 124
    array-length v3, v1

    new-array v2, v3, [Lcom/samsung/android/sdk/health/sensor/SLocation;

    .line 125
    .local v2, "tmp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-lt v0, v3, :cond_0

    .line 127
    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    .line 130
    .end local v0    # "i":I
    .end local v2    # "tmp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->extra:Landroid/os/Bundle;

    .line 131
    return-void

    .line 126
    .restart local v0    # "i":I
    .restart local v2    # "tmp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    :cond_0
    aget-object v3, v1, v0

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/SLocation;

    aput-object v3, v2, v0

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    .end local v0    # "i":I
    .end local v2    # "tmp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 88
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    .local v3, "loctionRes":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "coachingStr":Ljava/lang/String;
    const/4 v2, 0x0

    .line 91
    .local v2, "locationStr":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    if-eqz v4, :cond_0

    .line 92
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    array-length v4, v4

    if-lt v1, v4, :cond_3

    .line 98
    .end local v1    # "i":I
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    if-eqz v4, :cond_1

    .line 99
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    :cond_1
    if-eqz v3, :cond_2

    .line 102
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 104
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[EXERCISE] type="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " time="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->time:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->time:J

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 105
    const-string v5, ", duration = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->duration:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", calorie ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->calorie:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 106
    const-string v5, ", heartRate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->heartRate:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", distance ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->distance:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 107
    const-string v5, ", fitnessLevel = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->fitnessLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", coachingResult ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 108
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", GPS = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 104
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    .line 93
    .restart local v1    # "i":I
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v4, v4, v1

    if-eqz v4, :cond_4

    .line 94
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lcom/samsung/android/sdk/health/sensor/SLocation;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 67
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->time:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 68
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->type:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 69
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->duration:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 70
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->calorie:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 71
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->heartRate:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 72
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->distance:D

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeDouble(D)V

    .line 73
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->fitnessLevel:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/SCoachingResult;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 75
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    if-eqz v2, :cond_1

    .line 76
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    array-length v2, v2

    new-array v1, v2, [Lcom/samsung/android/sdk/health/sensor/SLocation;

    .line 77
    .local v1, "temp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 79
    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 83
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 84
    return-void

    .line 78
    .restart local v0    # "i":I
    .restart local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SExercise;->location:[Lcom/samsung/android/sdk/health/sensor/SLocation;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/SLocation;
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    goto :goto_1
.end method
