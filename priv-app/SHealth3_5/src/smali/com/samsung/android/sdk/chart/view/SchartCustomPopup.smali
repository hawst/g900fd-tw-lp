.class public abstract Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;
.super Landroid/view/View;
.source "SchartCustomPopup.java"


# instance fields
.field private defaultAction:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->setVisibility(I)V

    .line 22
    return-void
.end method


# virtual methods
.method public abstract onInVisiblePopup()V
.end method

.method public abstract onPopupInformation(Lcom/samsung/android/sdk/chart/view/SchartToolTipData;)V
.end method

.method public abstract onVisiblePopup()V
.end method

.method protected popupInformation(Lcom/samsung/android/sdk/chart/view/SchartToolTipData;Z)V
    .locals 1
    .param p1, "dataList"    # Lcom/samsung/android/sdk/chart/view/SchartToolTipData;
    .param p2, "visible"    # Z

    .prologue
    .line 26
    if-eqz p2, :cond_2

    .line 28
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->onPopupInformation(Lcom/samsung/android/sdk/chart/view/SchartToolTipData;)V

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->defaultAction:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->setVisibility(I)V

    .line 33
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->onVisiblePopup()V

    .line 43
    :cond_1
    :goto_0
    return-void

    .line 37
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->onInVisiblePopup()V

    .line 39
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->defaultAction:Z

    if-eqz v0, :cond_1

    .line 40
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->setVisibility(I)V

    goto :goto_0
.end method

.method public setDefaultActionEnabled(Z)V
    .locals 0
    .param p1, "defaultAction"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomPopup;->defaultAction:Z

    .line 77
    return-void
.end method
