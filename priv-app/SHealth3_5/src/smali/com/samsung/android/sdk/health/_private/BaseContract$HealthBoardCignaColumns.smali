.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$HealthBoardCignaColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "HealthBoardCignaColumns"
.end annotation


# static fields
.field public static final ARTICLE:Ljava/lang/String; = "article"

.field public static final BOARDCONTENTID:Ljava/lang/String; = "boardcontentid"

.field public static final HB_DATA_ONE:Ljava/lang/String; = "hb_data_one"

.field public static final HB_DATA_TWO:Ljava/lang/String; = "hb_data_two"

.field public static final META_DATA:Ljava/lang/String; = "meta_data"

.field public static final RULE_ID:Ljava/lang/String; = "rule_id"
