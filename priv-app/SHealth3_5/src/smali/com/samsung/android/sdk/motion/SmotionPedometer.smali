.class public Lcom/samsung/android/sdk/motion/SmotionPedometer;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;,
        Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;,
        Lcom/samsung/android/sdk/motion/SmotionPedometer$a;,
        Lcom/samsung/android/sdk/motion/SmotionPedometer$b;
    }
.end annotation


# static fields
.field private static f:Z

.field private static l:Lcom/samsung/android/sdk/motion/Smotion;


# instance fields
.field private a:Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

.field private b:Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

.field private c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

.field private d:Landroid/hardware/scontext/SContextListener;

.field private e:Landroid/os/PowerManager;

.field private g:Z

.field private h:Ljava/util/Timer;

.field private i:Z

.field private j:Z

.field private k:J

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->l:Lcom/samsung/android/sdk/motion/Smotion;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Lcom/samsung/android/sdk/motion/Smotion;)V
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    iput-boolean v2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->g:Z

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->h:Ljava/util/Timer;

    iput-boolean v2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->i:Z

    iput-boolean v2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->j:Z

    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->k:J

    iput-boolean v2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->m:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionPedometer : Looper is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionPedometer : Smotion is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionPedometer : Smotion.initialize() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer : Smotion.initialize() is not successful. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/motion/SmotionPedometer$a;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

    sput-object p2, Lcom/samsung/android/sdk/motion/SmotionPedometer;->l:Lcom/samsung/android/sdk/motion/Smotion;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->f:Z

    iget-object v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->e:Landroid/os/PowerManager;

    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->f:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method static synthetic a()Lcom/samsung/android/sdk/motion/Smotion;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->l:Lcom/samsung/android/sdk/motion/Smotion;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionPedometer;Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionPedometer;Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->g:Z

    return-void
.end method

.method static synthetic b(Lcom/samsung/android/sdk/motion/SmotionPedometer;Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->j:Z

    return-void
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->f:Z

    return v0
.end method

.method private c()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->g:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->h:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->h:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->h:Ljava/util/Timer;

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/samsung/android/sdk/motion/SmotionPedometer;Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->i:Z

    return-void
.end method


# virtual methods
.method public getInfo()Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer : start() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->e:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_2

    iput-boolean v4, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->i:Z

    invoke-virtual {p0}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->updateInfo()V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->h:Ljava/util/Timer;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->h:Ljava/util/Timer;

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->h:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/motion/SmotionPedometer$b;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/motion/SmotionPedometer$b;-><init>(Lcom/samsung/android/sdk/motion/SmotionPedometer;B)V

    iget-wide v2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->k:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->i:Z

    if-eqz v0, :cond_3

    :goto_0
    iput-boolean v4, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->i:Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c()V

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->j:Z

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->g:Z

    if-eqz v0, :cond_1

    const-string v0, "SmotionPedometer"

    const-string v1, "SmotionPedometer : getInfo() Time out !!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    goto :goto_1
.end method

.method public start(Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;)V
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionPedometer : Listener is null. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->f:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    if-nez v0, :cond_2

    iput-object p1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    invoke-direct {v0}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    if-nez p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionPedometer$a;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->m:Z

    invoke-virtual {p0}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->updateInfo()V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer : ChangeListener is already registered. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/samsung/android/sdk/motion/d;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/motion/d;-><init>(Lcom/samsung/android/sdk/motion/SmotionPedometer;Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer : start() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionPedometer$a;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c()V

    iput-object v3, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    iput-object v3, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    return-void
.end method

.method public updateInfo()V
    .locals 3

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer : start() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->e:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionPedometer$a;->requestToUpdate(Landroid/hardware/scontext/SContextListener;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->m:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;->onChanged(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer;->d:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionPedometer$a;->requestToUpdate(Landroid/hardware/scontext/SContextListener;I)V

    goto :goto_0
.end method
