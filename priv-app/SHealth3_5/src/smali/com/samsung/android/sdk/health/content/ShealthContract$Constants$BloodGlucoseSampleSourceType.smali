.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$BloodGlucoseSampleSourceType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BloodGlucoseSampleSourceType"
.end annotation


# static fields
.field public static final CAPILLARY:I = 0x426a

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final VENOUS:I = 0x4269
