.class public Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
.super Lcom/samsung/android/sdk/chart/series/SchartTimeData;
.source "SchartTimeCandleData.java"


# instance fields
.field protected mValue1:D

.field protected mValueMax:D

.field protected mValueMin:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(JDDDD)V
    .locals 0
    .param p1, "time"    # J
    .param p3, "high"    # D
    .param p5, "low"    # D
    .param p7, "open"    # D
    .param p9, "close"    # D

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>()V

    .line 16
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mTime:J

    .line 17
    iput-wide p3, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValueMax:D

    .line 18
    iput-wide p5, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValueMin:D

    .line 19
    iput-wide p7, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValue:D

    .line 20
    iput-wide p9, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValue1:D

    .line 21
    return-void
.end method


# virtual methods
.method public getValueClose()D
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValue1:D

    return-wide v0
.end method

.method public getValueHigh()D
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValueMax:D

    return-wide v0
.end method

.method public getValueLow()D
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValueMin:D

    return-wide v0
.end method

.method public getValueOpen()D
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValue:D

    return-wide v0
.end method

.method public setValueClose(D)V
    .locals 0
    .param p1, "close"    # D

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValue1:D

    .line 53
    return-void
.end method

.method public setValueHigh(D)V
    .locals 0
    .param p1, "high"    # D

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValueMax:D

    .line 29
    return-void
.end method

.method public setValueLow(D)V
    .locals 0
    .param p1, "low"    # D

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValueMin:D

    .line 37
    return-void
.end method

.method public setValueOpen(D)V
    .locals 0
    .param p1, "open"    # D

    .prologue
    .line 44
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;->mValue:D

    .line 45
    return-void
.end method
