.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HeartRateSNRUnit;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PulseOximeter"
.end annotation


# instance fields
.field public SNR:F

.field public SNRUnit:I

.field public eventTime:J

.field public heartRate:I

.field public interval:J

.field public pulseOximetry:F


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7fffffff

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 2512
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2514
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->heartRate:I

    .line 2516
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->eventTime:J

    .line 2518
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->interval:J

    .line 2520
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->SNR:F

    .line 2522
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->SNRUnit:I

    .line 2524
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$PulseOximeter;->pulseOximetry:F

    return-void
.end method
