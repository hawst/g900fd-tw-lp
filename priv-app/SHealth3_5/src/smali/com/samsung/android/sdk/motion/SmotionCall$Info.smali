.class public Lcom/samsung/android/sdk/motion/SmotionCall$Info;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/motion/SmotionCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# instance fields
.field private a:I

.field private b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionCall;->a()Lcom/samsung/android/sdk/motion/Smotion;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionCall.Info : SmotionCall is not created. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionCall;->b()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionCall.Info : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionCall$Info;J)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/android/sdk/motion/SmotionCall$Info;->b:J

    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/motion/SmotionCall$Info;->a:I

    return-void
.end method

.method public getCallPosition()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall$Info;->a:I

    return v0
.end method

.method public getTimeStamp()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall$Info;->b:J

    return-wide v0
.end method
