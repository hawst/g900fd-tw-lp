.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SensorSRDListener"
.end annotation


# instance fields
.field command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

.field exerciseId:J

.field record:Z

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;ZJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    .locals 2
    .param p2, "record"    # Z
    .param p3, "exerciseId"    # J
    .param p5, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .prologue
    .line 2961
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrapperSensorListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 2956
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->record:Z

    .line 2957
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->exerciseId:J

    .line 2958
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .line 2962
    iput-boolean p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->record:Z

    .line 2963
    iput-wide p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->exerciseId:J

    .line 2964
    iput-object p5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .line 2965
    return-void
.end method


# virtual methods
.method public onJoined(I)V
    .locals 7
    .param p1, "error"    # I

    .prologue
    .line 2972
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->record:Z

    iget-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->exerciseId:J

    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->startReceivingData(Lcom/samsung/android/sdk/health/sensor/SensorListener;ZJLcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_6

    .line 2991
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2992
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$SensorSRDListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->getSensorListener()Lcom/samsung/android/sdk/health/sensor/SensorListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->access$900(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Lcom/samsung/android/sdk/health/sensor/SensorListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/SensorListener;->onConnected(I)V

    .line 2993
    :cond_0
    return-void

    .line 2973
    :catch_0
    move-exception v6

    .line 2974
    .local v6, "e":Landroid/os/RemoteException;
    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 2975
    .end local v6    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v6

    .line 2976
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2977
    .end local v6    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v6

    .line 2978
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2979
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v6

    .line 2981
    .local v6, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;->printStackTrace()V

    goto :goto_0

    .line 2982
    .end local v6    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
    :catch_4
    move-exception v6

    .line 2983
    .local v6, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;->printStackTrace()V

    goto :goto_0

    .line 2984
    .end local v6    # "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorDeviceInUseException;
    :catch_5
    move-exception v6

    .line 2985
    .local v6, "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;->printStackTrace()V

    goto :goto_0

    .line 2986
    .end local v6    # "e":Lcom/samsung/android/sdk/health/sensor/exception/SHealthSensorInternalErrorException;
    :catch_6
    move-exception v6

    .line 2987
    .local v6, "e":Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;->printStackTrace()V

    goto :goto_0
.end method
