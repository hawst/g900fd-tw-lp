.class Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;
.super Ljava/lang/Thread;
.source "SchartChartBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RenderThread"
.end annotation


# static fields
.field private static final EGL_CONTEXT_CLIENT_VERSION:I = 0x3098

.field private static final EGL_OPENGL_ES2_BIT:I = 0x4

.field static final LOG_TAG:Ljava/lang/String; = "ChartView RenderThread"


# instance fields
.field mChangeSurface:Z

.field private mEgl:Ljavax/microedition/khronos/egl/EGL10;

.field private mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field private mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field private mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field private volatile mFinished:Z

.field private mGL:Ljavax/microedition/khronos/opengles/GL;

.field mHeight:I

.field public mLastTraceStatus:Z

.field private mView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

.field mWidth:I

.field public mbAnimating:Z

.field final synthetic this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V
    .locals 2
    .param p2, "view"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .prologue
    const/4 v1, 0x0

    .line 3874
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 3836
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 3837
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 3845
    iput-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mFinished:Z

    .line 3847
    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mWidth:I

    .line 3848
    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mHeight:I

    .line 3849
    iput-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mChangeSurface:Z

    .line 3870
    iput-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    .line 3871
    iput-boolean v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mLastTraceStatus:Z

    .line 3876
    iput-object p2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .line 3878
    return-void
.end method

.method private checkCurrent()V
    .locals 5

    .prologue
    .line 4129
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4130
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    .line 4131
    const/16 v2, 0x3059

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentSurface(I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v1

    .line 4130
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 4131
    if-nez v0, :cond_1

    .line 4132
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 4133
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 4132
    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    .line 4133
    if-nez v0, :cond_1

    .line 4134
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "eglMakeCurrent failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4135
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v2

    invoke-static {v2}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4134
    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4138
    :cond_1
    return-void
.end method

.method private checkEglError()V
    .locals 4

    .prologue
    .line 4106
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 4107
    .local v0, "error":I
    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 4108
    const-string v1, "ChartView RenderThread"

    .line 4109
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EGL error = 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 4108
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4111
    :cond_0
    return-void
.end method

.method private chooseEglConfig()Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 4193
    new-array v5, v4, [I

    .line 4194
    .local v5, "configsCount":[I
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 4195
    .local v3, "configs":[Ljavax/microedition/khronos/egl/EGLConfig;
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->getConfig()[I

    move-result-object v2

    .line 4196
    .local v2, "configSpec":[I
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    .line 4197
    if-nez v0, :cond_0

    .line 4198
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "eglChooseConfig failed "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4199
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4198
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4200
    :cond_0
    aget v0, v5, v6

    if-lez v0, :cond_1

    .line 4201
    aget-object v0, v3, v6

    .line 4203
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private finishGL()V
    .locals 5

    .prologue
    .line 4118
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 4119
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 4120
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_0

    .line 4121
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 4123
    :cond_0
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 4124
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 4126
    return-void
.end method

.method private getConfig()[I
    .locals 1

    .prologue
    .line 4207
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3026
        0x8
        0x3038
    .end array-data
.end method

.method private initGL()V
    .locals 7

    .prologue
    .line 4141
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v2

    check-cast v2, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    .line 4143
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 4144
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v2, v3, :cond_0

    .line 4145
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "eglGetDisplay failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4146
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4145
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4149
    :cond_0
    const/4 v2, 0x2

    new-array v1, v2, [I

    .line 4150
    .local v1, "version":[I
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v2, v3, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4151
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "eglInitialize failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4152
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4151
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4155
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->chooseEglConfig()Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 4156
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v2, :cond_2

    .line 4157
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "eglConfig not initialized"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4160
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 4162
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 4163
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v5, v5, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v6, 0x0

    .line 4162
    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 4165
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v2, v3, :cond_5

    .line 4166
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 4167
    .local v0, "error":I
    const/16 v2, 0x300b

    if-ne v0, v2, :cond_4

    .line 4168
    const-string v2, "ChartView RenderThread"

    .line 4169
    const-string v3, "createWindowSurface returned EGL_BAD_NATIVE_WINDOW."

    .line 4168
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4183
    .end local v0    # "error":I
    :goto_0
    return-void

    .line 4172
    .restart local v0    # "error":I
    :cond_4
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "createWindowSurface failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4173
    invoke-static {v0}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4172
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4176
    .end local v0    # "error":I
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 4177
    iget-object v6, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 4176
    invoke-interface {v2, v3, v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v2

    .line 4177
    if-nez v2, :cond_6

    .line 4178
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "eglMakeCurrent failed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 4179
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4178
    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 4182
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v2}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mGL:Ljavax/microedition/khronos/opengles/GL;

    goto :goto_0
.end method


# virtual methods
.method public changeSurface()V
    .locals 6

    .prologue
    .line 3881
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v1, v2, :cond_0

    .line 3882
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 3883
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 3886
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 3887
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v4, v4, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v5, 0x0

    .line 3886
    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 3889
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v1, v2, :cond_2

    .line 3890
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 3891
    .local v0, "error":I
    const/16 v1, 0x300b

    if-ne v0, v1, :cond_3

    .line 3892
    const-string v1, "ChartView RenderThread"

    .line 3893
    const-string v2, "createWindowSurface returned EGL_BAD_NATIVE_WINDOW."

    .line 3892
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3900
    .end local v0    # "error":I
    :cond_2
    return-void

    .line 3896
    .restart local v0    # "error":I
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "createWindowSurface failed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3897
    invoke-static {v0}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3896
    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;
    .locals 2
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2, "eglDisplay"    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3, "eglConfig"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 4187
    const/4 v1, 0x3

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 4189
    .local v0, "attrib_list":[I
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    .line 4188
    invoke-interface {p1, p2, p3, v1, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    return-object v1

    .line 4187
    nop

    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method public destroy()V
    .locals 4

    .prologue
    .line 4098
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 4099
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1}, Lcom/sec/dmc/sic/jni/ChartJNI;->destroy(J)V

    .line 4100
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    .line 4102
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    .line 4103
    return-void
.end method

.method public draw()V
    .locals 1

    .prologue
    .line 4046
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->onStep()V

    .line 4047
    return-void
.end method

.method finish()V
    .locals 1

    .prologue
    .line 4219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mFinished:Z

    .line 4220
    return-void
.end method

.method public onSurfaceChanged(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v3, 0x1

    .line 4055
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInit:Z

    if-nez v0, :cond_1

    .line 4063
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1, p1, p2}, Lcom/sec/dmc/sic/jni/ChartJNI;->init(JII)V

    .line 4064
    const-string v0, "SIC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[ schartFramework.jar ] VersionInfo - revision : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/sdk/chart/SchartRevision;->GetRevision()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", date : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/sdk/chart/SchartRevision;->GetBuildDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4065
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget v2, v2, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartType:I

    invoke-static {v0, v1, v2}, Lcom/sec/dmc/sic/jni/ChartJNI;->setChartType(JI)V

    .line 4068
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v1, v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    # invokes: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetVM(J)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$0(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;J)V

    .line 4070
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    # getter for: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
    invoke-static {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$1(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4071
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    # getter for: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
    invoke-static {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$1(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setOriWidth(F)V

    .line 4072
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    # getter for: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
    invoke-static {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$1(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    move-result-object v0

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setOriHeight(F)V

    .line 4076
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mInit:Z

    .line 4095
    :goto_0
    return-void

    .line 4083
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v0, v1, p1, p2}, Lcom/sec/dmc/sic/jni/ChartJNI;->changeSize(JII)V

    .line 4085
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    # getter for: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
    invoke-static {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$1(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 4086
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    # getter for: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
    invoke-static {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$1(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setOriWidth(F)V

    .line 4087
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    # getter for: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->zoomMap:Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
    invoke-static {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$1(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Lcom/samsung/android/sdk/chart/view/SchartZoomMap;

    move-result-object v0

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setOriHeight(F)V

    .line 4090
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    # getter for: Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mNeedAnimation:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->access$2(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    .line 4091
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public onSurfaceCreated()V
    .locals 2

    .prologue
    .line 4050
    const-string v0, "ChartView RenderThread"

    const-string/jumbo v1, "onSurfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4051
    return-void
.end method

.method public run()V
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 3909
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-boolean v8, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGLInit:Z

    if-nez v8, :cond_0

    .line 3911
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->initGL()V

    .line 3912
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iput-boolean v11, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGLInit:Z

    .line 3917
    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getWidth()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    invoke-virtual {v9}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->getHeight()I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->onSurfaceChanged(II)V

    .line 3922
    const/16 v0, 0x3c

    .line 3923
    .local v0, "FPS":I
    const/16 v8, 0x3e8

    div-int v6, v8, v0

    .line 3927
    .local v6, "frameTime":I
    :cond_1
    :goto_0
    iget-boolean v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mFinished:Z

    if-eqz v8, :cond_3

    .line 4026
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v8, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_2

    .line 4027
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v8, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    invoke-static {v8, v9}, Lcom/sec/dmc/sic/jni/ChartJNI;->destroy(J)V

    .line 4028
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    const-wide/16 v9, -0x1

    iput-wide v9, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    .line 4031
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iput-boolean v12, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mIsChartInitFinish:Z

    .line 4036
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->finishGL()V

    .line 4042
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iput-boolean v12, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mGLInit:Z

    .line 4043
    return-void

    .line 3953
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 3955
    .local v1, "begin":J
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->startGraphRevealAnimation()V

    .line 3963
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->draw()V

    .line 3965
    monitor-enter p0

    .line 3966
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-boolean v8, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    if-nez v8, :cond_4

    .line 3968
    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v9, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v8, v9, v10}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 3969
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->checkEglError()V

    .line 3970
    const-string v8, "ChartView RenderThread"

    const-string v9, "cannot swap buffers!"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3965
    :cond_4
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 3994
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v3, v8, v1

    .line 3995
    .local v3, "diff":J
    int-to-long v8, v6

    sub-long/2addr v8, v3

    long-to-int v7, v8

    .line 3998
    .local v7, "sleep":I
    if-lez v7, :cond_5

    .line 3999
    int-to-long v8, v7

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 4003
    :cond_5
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->checkEglError()V

    .line 4006
    iget-boolean v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->this$0:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-boolean v8, v8, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mSurfaceDestroyd:Z

    if-eqz v8, :cond_1

    .line 4009
    :cond_6
    :try_start_2
    monitor-enter p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 4010
    :try_start_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->isInterrupted()Z

    move-result v8

    if-nez v8, :cond_7

    .line 4011
    iget-boolean v8, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mFinished:Z

    if-nez v8, :cond_7

    .line 4012
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 4009
    :cond_7
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v8

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v8
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    .line 4017
    :catch_0
    move-exception v5

    .line 4018
    .local v5, "e":Ljava/lang/InterruptedException;
    iput-boolean v11, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mbAnimating:Z

    goto/16 :goto_0

    .line 3965
    .end local v3    # "diff":J
    .end local v5    # "e":Ljava/lang/InterruptedException;
    .end local v7    # "sleep":I
    :catchall_1
    move-exception v8

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v8

    .line 4000
    .restart local v3    # "diff":J
    .restart local v7    # "sleep":I
    :catch_1
    move-exception v8

    goto :goto_1
.end method

.method setChangeSurface(Z)V
    .locals 0
    .param p1, "changed"    # Z

    .prologue
    .line 3852
    iput-boolean p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mChangeSurface:Z

    .line 3853
    return-void
.end method

.method setWH(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 3864
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mWidth:I

    .line 3865
    iput p2, p0, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;->mHeight:I

    .line 3866
    return-void
.end method
