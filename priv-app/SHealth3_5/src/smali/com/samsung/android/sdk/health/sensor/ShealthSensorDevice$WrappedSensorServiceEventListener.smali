.class final Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_SensorServiceEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "WrappedSensorServiceEventListener"
.end annotation


# instance fields
.field private mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .prologue
    .line 398
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    .line 400
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    return-object v0
.end method


# virtual methods
.method public onConnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 431
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 433
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 435
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;I)V

    .line 436
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 439
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;
    :cond_0
    monitor-exit v2

    .line 440
    return-void

    .line 439
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onDisconnected(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 479
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 481
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 483
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1DisconnectedCallBack;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1DisconnectedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;I)V

    .line 484
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1DisconnectedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 487
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1DisconnectedCallBack;
    :cond_0
    monitor-exit v2

    .line 488
    return-void

    .line 487
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
    .locals 5
    .param p1, "command"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
    .param p2, "response"    # Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;

    .prologue
    .line 522
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 524
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 526
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;

    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    const/4 v3, 0x0

    invoke-direct {v1, p1, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;)V

    new-instance v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    const/4 v4, 0x0

    invoke-direct {v3, p2, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$1;)V

    invoke-direct {v0, p0, v1, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V

    .line 527
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 531
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;
    :cond_0
    monitor-exit v2

    .line 532
    return-void

    .line 531
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 557
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 559
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 561
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1StateChangedCallBack;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1StateChangedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;I)V

    .line 562
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1StateChangedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 565
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1StateChangedCallBack;
    :cond_0
    monitor-exit v2

    .line 566
    return-void

    .line 565
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
