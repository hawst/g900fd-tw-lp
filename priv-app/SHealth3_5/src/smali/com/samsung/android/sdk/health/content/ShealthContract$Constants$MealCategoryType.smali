.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealCategoryType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MealCategoryType"
.end annotation


# static fields
.field public static final EATEN:I = 0x2

.field public static final MY_FOOD:I = 0x0

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final PLANNED:I = 0x1

.field public static final PLANNED_CANCELED:I = 0x4

.field public static final PLANNED_EATEN:I = 0x3
