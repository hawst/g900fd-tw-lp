.class public Lcom/samsung/android/sdk/motion/SmotionCall;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;,
        Lcom/samsung/android/sdk/motion/SmotionCall$Info;,
        Lcom/samsung/android/sdk/motion/SmotionCall$a;
    }
.end annotation


# static fields
.field public static final POSITION_LEFT:I = 0x0

.field public static final POSITION_RIGHT:I = 0x1

.field private static d:Z

.field private static e:Lcom/samsung/android/sdk/motion/Smotion;


# instance fields
.field private a:Lcom/samsung/android/sdk/motion/SmotionCall$a;

.field private b:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

.field private c:Landroid/hardware/motion/MRListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/motion/SmotionCall;->e:Lcom/samsung/android/sdk/motion/Smotion;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Lcom/samsung/android/sdk/motion/Smotion;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->b:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->c:Landroid/hardware/motion/MRListener;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionCall : Looper is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionCall : Smotion is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionCall : Smotion.initialize() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionCall : Smotion.initialize() is not successful. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionCall$a;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/motion/SmotionCall$a;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->a:Lcom/samsung/android/sdk/motion/SmotionCall$a;

    sput-object p2, Lcom/samsung/android/sdk/motion/SmotionCall;->e:Lcom/samsung/android/sdk/motion/Smotion;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/sdk/motion/SmotionCall;->d:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionCall : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method static synthetic a()Lcom/samsung/android/sdk/motion/Smotion;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/motion/SmotionCall;->e:Lcom/samsung/android/sdk/motion/Smotion;

    return-object v0
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionCall;->d:Z

    return v0
.end method


# virtual methods
.method public start(Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;)V
    .locals 5

    const/4 v1, 0x0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionCall : ChangeListener is null. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionCall;->d:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionCall : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->b:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    if-nez v0, :cond_2

    iput-object p1, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->b:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->b:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    if-nez v2, :cond_3

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->c:Landroid/hardware/motion/MRListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->a:Lcom/samsung/android/sdk/motion/SmotionCall$a;

    iget-object v2, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->c:Landroid/hardware/motion/MRListener;

    const/high16 v3, 0x40000000    # 2.0f

    const/16 v4, 0x400

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/samsung/android/sdk/motion/SmotionCall$a;->registerListenerEvent(Landroid/hardware/motion/MRListener;IILandroid/os/Handler;)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionCall : ChangeListener is already registered."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/samsung/android/sdk/motion/c;

    invoke-direct {v0, p0, v2}, Lcom/samsung/android/sdk/motion/c;-><init>(Lcom/samsung/android/sdk/motion/SmotionCall;Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->b:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionCall : start() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->a:Lcom/samsung/android/sdk/motion/SmotionCall$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->a:Lcom/samsung/android/sdk/motion/SmotionCall$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->c:Landroid/hardware/motion/MRListener;

    const/16 v2, 0x400

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionCall$a;->unregisterListener(Landroid/hardware/motion/MRListener;I)V

    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->c:Landroid/hardware/motion/MRListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/motion/SmotionCall;->b:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    return-void
.end method
