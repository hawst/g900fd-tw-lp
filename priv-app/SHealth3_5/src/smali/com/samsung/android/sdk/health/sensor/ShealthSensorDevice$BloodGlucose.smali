.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$BloodGlucoseSampleSourceType;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$BloodGlucoseSampleType;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$BloodGlucosetUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealTimeType;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BloodGlucose"
.end annotation


# instance fields
.field public glucose:F

.field public glucoseErrorDetail:I

.field public glucoseSensorState:I

.field public glucoseUnit:I

.field public mealTime:J

.field public mealType:I

.field public mean:F

.field public sampleSource:I

.field public sampleType:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    const v2, 0x7fffffff

    .line 2177
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2181
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->sampleSource:I

    .line 2183
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->sampleType:I

    .line 2185
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->glucose:F

    .line 2187
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->glucoseUnit:I

    .line 2189
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->mealTime:J

    .line 2191
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->mealType:I

    .line 2193
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->mean:F

    .line 2195
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->glucoseSensorState:I

    .line 2197
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$BloodGlucose;->glucoseErrorDetail:I

    return-void
.end method
