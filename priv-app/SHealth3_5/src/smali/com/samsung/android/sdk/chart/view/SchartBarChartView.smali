.class public Lcom/samsung/android/sdk/chart/view/SchartBarChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartXYChartView;
.source "SchartBarChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, 0x3

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartBarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    const/4 v0, 0x3

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 40
    return-void
.end method
