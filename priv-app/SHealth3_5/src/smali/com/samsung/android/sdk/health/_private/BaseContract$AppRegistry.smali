.class public final Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistryColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppRegistry"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 684
    sget-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "app_registry"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
