.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$WeightColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WeightColumns"
.end annotation


# static fields
.field public static final ACTIVITY_METABOLIC_RATE:Ljava/lang/String; = "activity_metabolic_rate"

.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final BASAL_METABOLIC_RATE:Ljava/lang/String; = "basal_metabolic_rate"

.field public static final BODY_AGE:Ljava/lang/String; = "body_age"

.field public static final BODY_FAT:Ljava/lang/String; = "body_fat"

.field public static final BODY_MASS_INDEX:Ljava/lang/String; = "body_mass_index"

.field public static final BODY_WATER:Ljava/lang/String; = "body_water"

.field public static final BONE_MASS:Ljava/lang/String; = "bone_mass"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final MUSCLE_MASS:Ljava/lang/String; = "muscle_mass"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final SKELETAL_MUSCLE:Ljava/lang/String; = "skeletal_muscle"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"

.field public static final VISCERAL_FAT:Ljava/lang/String; = "visceral_fat"

.field public static final WEIGHT:Ljava/lang/String; = "weight"
