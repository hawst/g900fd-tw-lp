.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$TrainingLoadPeakColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TrainingLoadPeakColumns"
.end annotation


# static fields
.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final FITNESS_CLASS:Ljava/lang/String; = "fitness_class"

.field public static final MAXIMAL_MET:Ljava/lang/String; = "maximal_met"

.field public static final RESOURCE_RECOVERY:Ljava/lang/String; = "resource_recovery"

.field public static final TIME:Ljava/lang/String; = "time"

.field public static final TRAINING_LOAD_PEAK:Ljava/lang/String; = "training_load_peak"
