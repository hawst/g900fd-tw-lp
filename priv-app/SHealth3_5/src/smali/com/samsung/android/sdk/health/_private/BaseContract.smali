.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract;
.super Ljava/lang/Object;
.source "BaseContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/_private/BaseContract$PrivateConstants;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$ServiceType;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$LongtermCoach;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$LongtermCoachColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$FitnessChart;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$FitnessChartColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$TakenDosage;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$TakenDosageColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$AlarmTime;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$AlarmTimeColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$DosageAlarm;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$DosageAlarmColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$WaterTareType;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$WaterTareTypeColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$WaterTareItem;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$WaterTareItemColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$WaterIntakeLocal;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$WaterIntakeLocalColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$TrainingLoadPeak;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$TrainingLoadPeakColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$DeviceInfo;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$DeviceInfoColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$SharedConfigColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$HealthBoardCigna;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$HealthBoardCignaColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$ChallengesMember;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$ChallengesMemberColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$ChallengesInfo;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$ChallengesInfoColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$BuddyInfo;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$BuddyInfoColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistry;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistryColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;,
        Lcom/samsung/android/sdk/health/_private/BaseContract$Constants;
    }
.end annotation


# static fields
.field public static final APP_REG_REPLACE_URI:Landroid/net/Uri;

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.shealth.framework.repository.BaseContentProvider"

.field public static final AUTHORITY_URI:Landroid/net/Uri;

.field public static final CONTENT_NORMAL_DB_URI:Landroid/net/Uri;

.field public static final CONTENT_RAWQUERY_URI:Landroid/net/Uri;

.field public static final CONTENT_SECURE_DB_URI:Landroid/net/Uri;

.field public static final CONTENT_SHARED_PREF_URI:Landroid/net/Uri;

.field public static final MIGRATION_RAWQUERY_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-string v0, "content://com.sec.android.app.shealth.framework.repository.BaseContentProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    .line 18
    sget-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "Shealthdb_read"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->CONTENT_NORMAL_DB_URI:Landroid/net/Uri;

    .line 23
    sget-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "Shealth_securedb_read"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->CONTENT_SECURE_DB_URI:Landroid/net/Uri;

    .line 28
    sget-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "Shared_prefs_read"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->CONTENT_SHARED_PREF_URI:Landroid/net/Uri;

    .line 34
    sget-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string/jumbo v1, "rawquery"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->CONTENT_RAWQUERY_URI:Landroid/net/Uri;

    .line 40
    sget-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string/jumbo v1, "migration"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->MIGRATION_RAWQUERY_URI:Landroid/net/Uri;

    .line 42
    sget-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "app_registry_replace"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/_private/BaseContract;->APP_REG_REPLACE_URI:Landroid/net/Uri;

    return-void
.end method
