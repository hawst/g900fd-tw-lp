.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$BloodGlucoseMeasurementContextColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BloodGlucoseMeasurementContextColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final BLOOD_GLUCOSE__ID:Ljava/lang/String; = "blood_glucose__id"

.field public static final CONTEXT_TYPE:Ljava/lang/String; = "context_type"
