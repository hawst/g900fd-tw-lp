.class Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;
.super Landroid/content/BroadcastReceiver;
.source "ShealthContentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x0

    const/4 v10, -0x1

    .line 344
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onReceive(): number of times "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.private.syncadapter.receiver"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 347
    const-string v7, "DATA_SYNC_TYPE"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 349
    .local v1, "dataType":I
    const-string v7, "Status_bTrueFinished"

    invoke-virtual {p2, v7, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 350
    .local v0, "bTrueFinished":Z
    const-string v7, "TimeStamp"

    const-wide/16 v8, 0x0

    invoke-virtual {p2, v7, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    .line 351
    .local v3, "key":J
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v7

    if-eqz v7, :cond_6

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v7

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 352
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onReceive(): mListenerColl has key: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v7

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    .line 354
    .local v6, "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    if-eqz v6, :cond_5

    .line 356
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "onReceive(): syncCallBack is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    const-string v7, "PROGRESS_STATUS"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 359
    .local v5, "progressStatus":I
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onReceive(): progress: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    if-ltz v5, :cond_1

    const/16 v7, 0x64

    if-gt v5, v7, :cond_1

    .line 362
    invoke-interface {v6, v1, v5}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onProgress(II)V

    .line 401
    .end local v0    # "bTrueFinished":Z
    .end local v1    # "dataType":I
    .end local v3    # "key":J
    .end local v5    # "progressStatus":I
    .end local v6    # "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    :cond_0
    :goto_0
    return-void

    .line 366
    .restart local v0    # "bTrueFinished":Z
    .restart local v1    # "dataType":I
    .restart local v3    # "key":J
    .restart local v5    # "progressStatus":I
    .restart local v6    # "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    :cond_1
    if-nez v0, :cond_3

    .line 368
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onReceive(): calling on started for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    const-string v7, "ERROR_TYPE"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 370
    .local v2, "error_code":I
    const/16 v7, 0x12

    if-ne v2, v7, :cond_2

    .line 371
    const-string v7, "ERROR_TYPE"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const-string v8, "ERROR_MESSAGE"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v1, v7, v8}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(IILjava/lang/String;)V

    goto :goto_0

    .line 373
    :cond_2
    const-string v7, "ERROR_TYPE"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const/4 v8, 0x0

    invoke-interface {v6, v1, v7, v8}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(IILjava/lang/String;)V

    goto :goto_0

    .line 377
    .end local v2    # "error_code":I
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "onReceive(): calling on onFinished for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const-string v7, "is_exercise_sync_error"

    invoke-virtual {p2, v7, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 380
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "onReceive(): Error occurred during exercise sync"

    invoke-static {v7, v8}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    const/16 v7, 0x11

    invoke-interface {v6, v1, v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onFinished(II)V

    .line 385
    :goto_1
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "size of mListenerColl before removing"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "removing list with key"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v7

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "size of mListenerColl after removing"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "is empty "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->isEmpty()Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;
    invoke-static {}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$100()Ljava/util/Map;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 391
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # invokes: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$200(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V

    goto/16 :goto_0

    .line 384
    :cond_4
    const-string v7, "ERROR_TYPE"

    invoke-virtual {p2, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    invoke-interface {v6, v1, v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onFinished(II)V

    goto/16 :goto_1

    .line 395
    .end local v5    # "progressStatus":I
    :cond_5
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "no call reg with key ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 398
    .end local v6    # "syncCallBack":Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    :cond_6
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;->this$0:Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    # getter for: Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "no call reg with key ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
