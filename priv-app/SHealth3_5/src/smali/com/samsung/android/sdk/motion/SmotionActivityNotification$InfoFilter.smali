.class public Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/motion/SmotionActivityNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InfoFilter"
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a:Ljava/util/ArrayList;

    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->a()Lcom/samsung/android/sdk/motion/Smotion;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification.InfoFilter : SmotionActivityNotification is not created. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->b()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification.InfoFilter : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public addActivity(I)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification.InfoFilter : InfoFilter is not created."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x4

    if-gt p1, v0, :cond_1

    if-gez p1, :cond_2

    :cond_1
    const-string v0, "SmotionActivityNotification"

    const-string v1, "This activity type is not supported."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivityNotification.InfoFilter : This activity type is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_4

    const-string v0, "SmotionActivityNotification"

    const-string v1, "This activity type is duplicated."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
