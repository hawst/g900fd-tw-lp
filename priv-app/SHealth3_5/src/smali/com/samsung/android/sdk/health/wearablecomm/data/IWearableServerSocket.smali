.class public interface abstract Lcom/samsung/android/sdk/health/wearablecomm/data/IWearableServerSocket;
.super Ljava/lang/Object;
.source "IWearableServerSocket.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract finishSending()V
.end method

.method public abstract initialize()V
.end method

.method public abstract listenToConnect()V
.end method

.method public abstract receiveData()V
.end method

.method public abstract sendData([B)I
.end method

.method public abstract setListener(Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)V
.end method
