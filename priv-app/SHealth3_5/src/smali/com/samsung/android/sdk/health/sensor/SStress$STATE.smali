.class public interface abstract Lcom/samsung/android/sdk/health/sensor/SStress$STATE;
.super Ljava/lang/Object;
.source "SStress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/SStress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "STATE"
.end annotation


# static fields
.field public static final FAIL:I = 0x0

.field public static final HIGH:I = 0x6

.field public static final INVALID:I = 0x1

.field public static final LOW:I = 0x2

.field public static final NORMAL:I = 0x4

.field public static final QUITE_HIGH:I = 0x5

.field public static final QUITE_LOW:I = 0x3
