.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$StrengthFitnessColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StrengthFitnessColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final DURATION_MILLISECOND:Ljava/lang/String; = "duration_millisecond"

.field public static final DURATION_MIN:Ljava/lang/String; = "duration_min"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final GRIP:Ljava/lang/String; = "grip"

.field public static final LATERALITY:Ljava/lang/String; = "laterality"

.field public static final MOVEMENT:Ljava/lang/String; = "movement"

.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final REPETITION_COUNT:Ljava/lang/String; = "repetition_count"

.field public static final RESISTANCE:Ljava/lang/String; = "resistance"
