.class public Lcom/samsung/android/sdk/health/sensor/SLocation;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SLocation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public accuracy:F

.field public altitude:D

.field public bearing:F

.field public extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

.field public extraBundle:Landroid/os/Bundle;

.field public latitude:D

.field public longitude:D

.field public speed:F

.field public time:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SLocation$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SLocation$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 70
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 27
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 9
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->time:J

    .line 11
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->latitude:D

    .line 13
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->longitude:D

    .line 15
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->altitude:D

    .line 17
    iput v4, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->accuracy:F

    .line 19
    iput v4, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->speed:F

    .line 21
    iput v4, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->bearing:F

    .line 23
    iput-object v5, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    .line 25
    iput-object v5, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extraBundle:Landroid/os/Bundle;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 31
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 9
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->time:J

    .line 11
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->latitude:D

    .line 13
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->longitude:D

    .line 15
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->altitude:D

    .line 17
    iput v4, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->accuracy:F

    .line 19
    iput v4, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->speed:F

    .line 21
    iput v4, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->bearing:F

    .line 23
    iput-object v5, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    .line 25
    iput-object v5, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extraBundle:Landroid/os/Bundle;

    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SLocation;->readFromParcel(Landroid/os/Parcel;)V

    .line 34
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->time:J

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->latitude:D

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->longitude:D

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->altitude:D

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->accuracy:F

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->speed:F

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->bearing:F

    .line 81
    const-class v0, Lcom/samsung/android/sdk/health/sensor/SExtra;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/SExtra;

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extraBundle:Landroid/os/Bundle;

    .line 83
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 89
    .local v0, "extraStr":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/SExtra;->toString()Ljava/lang/String;

    move-result-object v0

    .line 92
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[LOCATION] (time : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->time:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->time:J

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", latitude: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->latitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 93
    const-string v2, ", longitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->longitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", altitude:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->altitude:D

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 94
    const-string v2, ", accuracy:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->accuracy:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", speed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->speed:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", speed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 95
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->speed:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", extra : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 92
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 46
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->latitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 47
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->longitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 48
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->altitude:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 49
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->accuracy:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 50
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->speed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 51
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->bearing:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extra:Lcom/samsung/android/sdk/health/sensor/SExtra;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SLocation;->extraBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 54
    return-void
.end method
