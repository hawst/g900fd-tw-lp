.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$AppRegistryColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AppRegistryColumns"
.end annotation


# static fields
.field public static final ACTIONS:Ljava/lang/String; = "actions"

.field public static final APP_NAME:Ljava/lang/String; = "app_name"

.field public static final APP_TYPE:Ljava/lang/String; = "app_type"

.field public static final EXTRA_INFO:Ljava/lang/String; = "extra_info"

.field public static final HAS_ACCESSORIES:Ljava/lang/String; = "has_accessories"

.field public static final HAS_RESETTABLE_DATA:Ljava/lang/String; = "has_resettable_data"

.field public static final HEALTH_MEASURE_TYPE:Ljava/lang/String; = "health_measure_type"

.field public static final IS_ENABLED:Ljava/lang/String; = "is_enabled"

.field public static final IS_FAVORITE:Ljava/lang/String; = "is_favorite"

.field public static final IS_PUSH_ENABLED:Ljava/lang/String; = "is_push_enabled"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field public static final PLUGIN_DISPLAY_ICONS:Ljava/lang/String; = "plugin_display_icons"

.field public static final PLUGIN_ID:Ljava/lang/String; = "plugin_id"

.field public static final SUMMARY_INFO:Ljava/lang/String; = "summary_info"

.field public static final VERSION:Ljava/lang/String; = "version"
