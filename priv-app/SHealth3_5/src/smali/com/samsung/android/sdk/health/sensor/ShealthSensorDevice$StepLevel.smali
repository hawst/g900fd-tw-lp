.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StepLevel;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "StepLevel"
.end annotation


# static fields
.field public static final STEP_LEVEL_HEALTHY:I = 0x3

.field public static final STEP_LEVEL_NORMAL:I = 0x2

.field public static final STEP_LEVEL_SEDENTARY:I = 0x1

.field public static final STEP_LEVEL_STATIONARY:I


# instance fields
.field public calorie:D

.field public count:I

.field public distance:D

.field public duration:I

.field public level:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide v1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const v0, 0x7fffffff

    .line 2816
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2828
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StepLevel;->level:I

    .line 2830
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StepLevel;->count:I

    .line 2832
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StepLevel;->distance:D

    .line 2834
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StepLevel;->calorie:D

    .line 2836
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StepLevel;->duration:I

    return-void
.end method
