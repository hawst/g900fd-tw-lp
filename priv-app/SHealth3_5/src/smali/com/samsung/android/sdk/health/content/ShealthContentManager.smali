.class public Lcom/samsung/android/sdk/health/content/ShealthContentManager;
.super Ljava/lang/Object;
.source "ShealthContentManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    }
.end annotation


# static fields
.field private static final ACCOUNT:Ljava/lang/String; = "Account"

.field private static final ACCOUNT_FOUND:Ljava/lang/String; = "ACCOUNT_FOUND"

.field public static final ACCOUNT_TYPE:Ljava/lang/String; = "AccountType"

.field public static final ACCOUNT_TYPE_HEALTH:Ljava/lang/String; = "HEALTH_ACCOUNT"

.field public static final ACCOUNT_TYPE_SAM:Ljava/lang/String; = "SAMSUNG_ACCOUNT"

.field private static ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String; = null

.field public static final ACTION_BACKUP_COMPLETED:Ljava/lang/String; = "action_backup_completed"

.field public static final ACTION_BACKUP_STARTED:Ljava/lang/String; = "action_backup_started"

.field public static final ACTION_DELETION_COMPLETED:Ljava/lang/String; = "action_deletion_completed"

.field public static final ACTION_DELETION_STARTED:Ljava/lang/String; = "action_deletion_started"

.field public static final ACTION_RESTORATION_COMPLETED:Ljava/lang/String; = "action_restoration_completed"

.field public static final ACTION_RESTORATION_ONGOING_DB_REPLACE:Ljava/lang/String; = "action_restoration_ongoing_db_replace"

.field public static final ACTION_RESTORATION_STARTED:Ljava/lang/String; = "action_restoration_started"

.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.android.app.shealth.cp.HealthContentProvider"

.field public static final BACKUP_TYPE_ALL:I = 0x1

.field public static final BACKUP_TYPE_BIO_BLOODGLUCOSE:I = 0x66

.field public static final BACKUP_TYPE_BIO_BLOODPRESURE:I = 0x67

.field public static final BACKUP_TYPE_BIO_BODYTEMPERATURE:I = 0x69

.field public static final BACKUP_TYPE_BIO_ECG:I = 0x6d

.field public static final BACKUP_TYPE_BIO_HRM:I = 0x6c

.field public static final BACKUP_TYPE_BIO_PO:I = 0x6e

.field public static final BACKUP_TYPE_BIO_SKIN:I = 0x6f

.field public static final BACKUP_TYPE_BIO_WEIGHT:I = 0x68

.field public static final BACKUP_TYPE_EXERCISE:I = 0xc8

.field public static final BACKUP_TYPE_FOOD:I = 0x320

.field public static final BACKUP_TYPE_PROFILE:I = 0x190

.field public static final BACKUP_TYPE_RANKING:I = 0xc9

.field public static final BACKUP_TYPE_SLEEP:I = 0x1f5

.field public static final BACKUP_TYPE_STRESS:I = 0x1f6

.field public static final BACKUP_TYPE_TEMPERATURE_HUMIDITY:I = 0x25b

.field public static final BACKUP_TYPE_UV:I = 0x259

.field public static final BACKUP_TYPE_UV_PROTECTION:I = 0x25a

.field public static final BACKUP_TYPE_WATER_INTAKE:I = 0x12c

.field public static final DATA_TYPE:Ljava/lang/String; = "data_type"

.field public static final GET_SYNC_STATUS:Ljava/lang/String; = "GET_SYNC_STATUS"

.field public static final IS_RESTORE_COMPLETED:Ljava/lang/String; = "Is_Restore_Completed"

.field public static final PROGRESS_STATUS:Ljava/lang/String; = "PROGRESS_STATUS"

.field public static final RECEIVER_ACTION:Ljava/lang/String; = "com.private.syncadapter.receiver"

.field public static final REQUEST_TYPE:Ljava/lang/String; = "request_type"

.field public static final REQUEST_TYPE_BACKUP:I = 0x1869f

.field public static final REQUEST_TYPE_DELETE_BACKUP:I = 0x1869c

.field public static final REQUEST_TYPE_RESTORE:I = 0x1869e

.field public static final REQUEST_TYPE_SYNC:I = 0x1869d

.field public static final RESTORE_TYPE_ALL:I = 0xb

.field public static final SET_AUTOBACKUP_INTERVAL:Ljava/lang/String; = "autobackup_interval"

.field public static final SHEALTH_2_5_EXISTS:Ljava/lang/String; = "SHEALTH_2_5_EXISTS"

.field public static final SHEALTH_2_5_EXISTS_KIES:Ljava/lang/String; = "SHEALTH_2_5_EXISTS_KIES"

.field private static final STATUS:Ljava/lang/String; = "Status"

.field public static final WIFI_STATUS_KEY:Ljava/lang/String; = "SET_WIFI_STATUS"

.field private static mListenerColl:Ljava/util/Map;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private errorCode:I

.field private isRegistered:Z

.field mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    .line 132
    const-string v0, "com.osp.app.signin"

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const-class v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    .line 239
    iput v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->errorCode:I

    .line 241
    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    .line 338
    new-instance v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$1;-><init>(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 247
    if-nez p1, :cond_0

    .line 248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    .line 254
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/content/ShealthContentManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V

    return-void
.end method

.method private checkNetworkAndAccount()Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 433
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "checkNetworkAndAccount()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 437
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "key"

    const-string v4, "GET_SAMSUNG_ACCOUNT"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const-string/jumbo v3, "value"

    iget-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    invoke-virtual {v3, v4, v5, v2, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 440
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 442
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "result is not null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    .end local v1    # "result":Landroid/os/Bundle;
    :goto_0
    return-object v1

    .line 447
    .restart local v1    # "result":Landroid/os/Bundle;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "result is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    const/4 v3, 0x3

    iput v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->errorCode:I

    move-object v1, v2

    .line 451
    goto :goto_0
.end method

.method private connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 15
    .param p1, "requestType"    # I
    .param p2, "dataType"    # I
    .param p3, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;

    .prologue
    .line 566
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "connectToSyncAdapter(): requestType: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ". dataType: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    new-instance v7, Landroid/content/IntentFilter;

    const-string v12, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-direct {v7, v12}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 570
    .local v7, "intentFilter":Landroid/content/IntentFilter;
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v7}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v6

    .line 571
    .local v6, "intent":Landroid/content/Intent;
    if-eqz v6, :cond_1

    .line 573
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): Device storage is low, Sync cannot happen"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    if-eqz p3, :cond_0

    .line 575
    const/16 v12, 0x10

    const/4 v13, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v12, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(IILjava/lang/String;)V

    .line 659
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isBackUpRestoreInProgress()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 582
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): Cancel the previous sync opepration"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    const-string v13, "account"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/accounts/AccountManager;

    .line 584
    .local v4, "accountManager":Landroid/accounts/AccountManager;
    sget-object v12, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->ACCOUNT_TYPE_SAM_ACC:Ljava/lang/String;

    invoke-virtual {v4, v12}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 585
    .local v3, "account":[Landroid/accounts/Account;
    if-eqz v3, :cond_2

    array-length v12, v3

    if-gtz v12, :cond_4

    .line 587
    :cond_2
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): No account found."

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    if-eqz p3, :cond_3

    .line 590
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): previous sync is in progress, listener is not null, send onstated callbackup with errorcode: 2"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v12, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(IILjava/lang/String;)V

    goto :goto_0

    .line 594
    :cond_3
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): previous sync is in progress, listener is null"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 599
    :cond_4
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): Account found, cancel the previous sync"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    const/4 v12, 0x0

    aget-object v12, v3, v12

    const-string v13, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v12, v13}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 603
    .end local v3    # "account":[Landroid/accounts/Account;
    .end local v4    # "accountManager":Landroid/accounts/AccountManager;
    :cond_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v8

    .line 604
    .local v8, "result":Landroid/os/Bundle;
    if-eqz v8, :cond_a

    .line 606
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter() result is not null"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const-string v12, "Status"

    invoke-virtual {v8, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 608
    .local v9, "status":Ljava/lang/String;
    const-string v12, "AccountType"

    invoke-virtual {v8, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 609
    .local v2, "accType":Ljava/lang/String;
    if-eqz v2, :cond_6

    const-string v12, "NONE"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 610
    :cond_6
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): No account found so returning.."

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    if-eqz p3, :cond_0

    .line 612
    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v12, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 615
    :cond_7
    const-string v12, "SAMSUNG_ACCOUNT"

    invoke-virtual {v2, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 616
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): Samsung account is present"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    if-eqz v9, :cond_0

    const-string v12, "ACCOUNT_FOUND"

    invoke-virtual {v9, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 618
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): account found"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 620
    .local v5, "extraData":Landroid/os/Bundle;
    const-string/jumbo v12, "request_type"

    move/from16 v0, p1

    invoke-virtual {v5, v12, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 621
    const-string v12, "data_type"

    move/from16 v0, p2

    invoke-virtual {v5, v12, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 622
    const-string v12, "force"

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 623
    const-string v12, "expedited"

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 624
    const-string/jumbo v12, "packageName"

    iget-object v13, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    new-instance v12, Ljava/util/Date;

    invoke-direct {v12}, Ljava/util/Date;-><init>()V

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    .line 626
    .local v10, "timeStamp":J
    const-string v12, "TimeStamp"

    invoke-virtual {v5, v12, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 627
    const-string v12, "Shealth_SDK_Is_Caller"

    const/4 v13, 0x1

    invoke-virtual {v5, v12, v13}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 628
    const-string v12, "Account"

    invoke-virtual {v8, v12}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    .line 629
    .local v3, "account":Landroid/accounts/Account;
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "adding time stamp to mListenerColl  "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    sget-object v12, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-interface {v12, v13, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 631
    const-string v12, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v3, v12, v5}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 632
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->registerSyncAdapterBroadCastReceiver()V

    goto/16 :goto_0

    .line 638
    .end local v3    # "account":Landroid/accounts/Account;
    .end local v5    # "extraData":Landroid/os/Bundle;
    .end local v10    # "timeStamp":J
    :cond_8
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): Samsung account is not present"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    if-eqz p3, :cond_9

    .line 641
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): Samsung account is not present, listener is not null, send onstated callbackup with errorcode: 2"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    const/4 v12, 0x2

    const/4 v13, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v12, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 645
    :cond_9
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter(): Samsung account is not present, listener is null"

    invoke-static {v12, v13}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 650
    .end local v2    # "accType":Ljava/lang/String;
    .end local v9    # "status":Ljava/lang/String;
    :cond_a
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter() result is null"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    if-eqz p3, :cond_b

    .line 653
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter() result is null, listener is not null"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    iget v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->errorCode:I

    const/4 v13, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-interface {v0, v1, v12, v13}, Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;->onStarted(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 657
    :cond_b
    iget-object v12, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v13, "connectToSyncAdapter() result is null, listener is null"

    invoke-static {v12, v13}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private registerSyncAdapterBroadCastReceiver()V
    .locals 4

    .prologue
    .line 410
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "registerSyncAdapterBroadCastReceiver(): REGISTERED THE RECEIVER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.private.syncadapter.receiver"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 412
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    .line 414
    return-void
.end method

.method private unRegisterSyncAdapterBroadCastReceiver()V
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unRegisterSyncAdapterBroadCastReceiver()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "unRegisterSyncAdapterBroadCastReceiver(): UN REGISTERING THE RECEIVER"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 426
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->isRegistered:Z

    .line 429
    :cond_0
    return-void
.end method


# virtual methods
.method public deleteBackUp(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 560
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v1, "deleteBackUp()"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    const v0, 0x1869c

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V

    .line 562
    return-void
.end method

.method public isBackUpRestoreInProgress()Z
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 461
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress()"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v3

    .line 463
    .local v3, "result":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 465
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): result is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    const-string v7, "Status"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 467
    .local v5, "status":Ljava/lang/String;
    const-string v7, "AccountType"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 468
    .local v0, "accType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v7, "NONE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 470
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isBackUpRestoreInProgress(): accType: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    const-string v7, "SAMSUNG_ACCOUNT"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 473
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): accType is samsung account"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    if-eqz v5, :cond_0

    const-string v7, "ACCOUNT_FOUND"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 476
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): account found"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    const-string v7, "Account"

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 478
    .local v1, "account":Landroid/accounts/Account;
    if-eqz v1, :cond_0

    .line 480
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): account is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 486
    .local v2, "extras":Landroid/os/Bundle;
    const-string v7, "key"

    const-string v8, "GET_SYNC_STATUS"

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    const-string/jumbo v7, "value"

    iget-object v8, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    const-string v7, "Account"

    invoke-virtual {v2, v7, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 489
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v9, "CONFIG_OPTION_GET"

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10, v2}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v4

    .line 490
    .local v4, "retVal":Landroid/os/Bundle;
    if-eqz v4, :cond_1

    .line 492
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): retVal is not null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    const-string v7, "GET_SYNC_STATUS"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 506
    .end local v0    # "accType":Ljava/lang/String;
    .end local v1    # "account":Landroid/accounts/Account;
    .end local v2    # "extras":Landroid/os/Bundle;
    .end local v4    # "retVal":Landroid/os/Bundle;
    .end local v5    # "status":Ljava/lang/String;
    :cond_0
    :goto_0
    return v6

    .line 497
    .restart local v0    # "accType":Ljava/lang/String;
    .restart local v1    # "account":Landroid/accounts/Account;
    .restart local v2    # "extras":Landroid/os/Bundle;
    .restart local v4    # "retVal":Landroid/os/Bundle;
    .restart local v5    # "status":Ljava/lang/String;
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v8, "isBackUpRestoreInProgress(): retVal is null"

    invoke-static {v7, v8}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isWifiChosenForBackup()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 539
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "isWifiChosenForBackup()"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 541
    .local v0, "extras":Landroid/os/Bundle;
    const-string v3, "key"

    const-string v4, "SET_WIFI_STATUS"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v5, "CONFIG_OPTION_GET"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 544
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 546
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isWifiChosenForBackup(): value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "value"

    invoke-virtual {v1, v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const-string/jumbo v3, "value"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 550
    :goto_0
    return v2

    .line 549
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string v4, "isWifiChosenForBackup(): wifi is not chosen for backup"

    invoke-static {v3, v4}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startBackup(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startBackup()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    const-string/jumbo v0, "startBackup()"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 263
    const v0, 0x1869f

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V

    .line 264
    return-void
.end method

.method public startRestore(ILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V
    .locals 2
    .param p1, "dataType"    # I
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v1, "startRestore()"

    invoke-static {v0, v1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    const-string/jumbo v0, "startRestore()"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 301
    const v0, 0x1869e

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->connectToSyncAdapter(IILcom/samsung/android/sdk/health/content/ShealthContentManager$ActivityListener;)V

    .line 302
    return-void
.end method

.method public stopBackup()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 268
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopBackup()"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const-string/jumbo v3, "stopBackup()"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 270
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v1

    .line 271
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    .line 272
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopBackup() result is not null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    const-string v3, "Status"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 274
    .local v2, "status":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v3, "ACCOUNT_FOUND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 275
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopBackup() account found"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    const-string v3, "Account"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 279
    .local v0, "account":Landroid/accounts/Account;
    const-string v3, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 280
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 281
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopBackup() clear mListenerColl"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 284
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopBackup() unregister braodcast receiver"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V

    .line 295
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "status":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 291
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopBackup() result is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopRestore()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 306
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopRestore()"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    const-string/jumbo v3, "stopRestore()"

    invoke-static {v3}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 308
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->checkNetworkAndAccount()Landroid/os/Bundle;

    move-result-object v1

    .line 309
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_2

    .line 310
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopRestore() result is not null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const-string v3, "Status"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 312
    .local v2, "status":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v3, "ACCOUNT_FOUND"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 313
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopRestore() account found"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const-string v3, "Account"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 317
    .local v0, "account":Landroid/accounts/Account;
    const-string v3, "com.sec.android.app.shealth.cp.HealthContentProvider"

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 318
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    if-eqz v3, :cond_0

    .line 319
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopRestore() clear mListenerColl"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->mListenerColl:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 322
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopRestore() unregister braodcast receiver"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->unRegisterSyncAdapterBroadCastReceiver()V

    .line 333
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "status":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 329
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v4, "stopRestore() result is null"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public useWifiForBackup(Z)Z
    .locals 6
    .param p1, "useWifi"    # Z

    .prologue
    .line 516
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "useWifiForBackup()"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 518
    .local v0, "extras":Landroid/os/Bundle;
    const-string v2, "key"

    const-string v3, "SET_WIFI_STATUS"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    const-string/jumbo v2, "value"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 520
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v4, "CONFIG_OPTION_PUT"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 521
    .local v1, "result":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 523
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "useWifiForBackup(): result is not null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    const/4 v2, 0x1

    .line 529
    :goto_0
    return v2

    .line 528
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/content/ShealthContentManager;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "useWifiForBackup(): result is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    const/4 v2, 0x0

    goto :goto_0
.end method
