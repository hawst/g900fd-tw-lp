.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$WaterIntakeLocalColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WaterIntakeLocalColumns"
.end annotation


# static fields
.field public static final COUNT_TARE:Ljava/lang/String; = "count_tare"

.field public static final GOAL_ID:Ljava/lang/String; = "goal__id"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final SET_TIME_OFFSET:Ljava/lang/String; = "set_time_offset"

.field public static final TOTAL_VOLUME:Ljava/lang/String; = "total_volume"

.field public static final WATER_INTAKE_ID:Ljava/lang/String; = "water_intake_id"

.field public static final WATER_TARE_TYPE_ID:Ljava/lang/String; = "water_tare_type__id"
