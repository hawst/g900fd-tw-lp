.class public interface abstract Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$SchartXYChartScrollDataChangeListener;
.super Ljava/lang/Object;
.source "SchartChartBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SchartXYChartScrollDataChangeListener"
.end annotation


# virtual methods
.method public abstract onLeftScrollDataChange(J)V
.end method

.method public abstract onLeftScrollDataChangeComming(J)V
.end method

.method public abstract onRightScrollDataChange(J)V
.end method

.method public abstract onRightScrollDataChangeComming(J)V
.end method
