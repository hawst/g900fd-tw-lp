.class public Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartPieChartView;
.source "SchartStringPieChartView.java"


# instance fields
.field private nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartPieChartView;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartPieChartView;->setDataType(I)V

    .line 25
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartPieChartView;->createChart()V

    .line 27
    new-instance v0, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;-><init>(Landroid/content/Context;)V

    .line 55
    const/4 v1, 0x1

    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->createSeries(II)V

    .line 57
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 59
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 62
    return-void

    .line 60
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->addDataBack(ILjava/util/List;)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addDataBack(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 79
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addData(IILjava/util/List;)V

    .line 83
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public addDataFront(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 90
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addDataFront(IILjava/util/List;)V

    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 102
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->clearData()V

    goto :goto_0
.end method

.method public setStartVisual(II)V
    .locals 2
    .param p1, "startIndex"    # I
    .param p2, "validCount"    # I

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 68
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartVisual(II)V

    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringPieChartView;->notifyRenderThread()V

    goto :goto_0
.end method
