.class public Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SHeartRateMonitor.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public SNR:F

.field public SNRUnit:I

.field public eventTime:J

.field public heartRate:I

.field public heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

.field public interval:J

.field public time:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    const-wide v0, 0x7fffffffffffffffL

    .line 22
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 8
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->time:J

    .line 10
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRate:I

    .line 12
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->eventTime:J

    .line 14
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->interval:J

    .line 16
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNR:F

    .line 18
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNRUnit:I

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const v2, 0x7fffffff

    const-wide v0, 0x7fffffffffffffffL

    .line 25
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 8
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->time:J

    .line 10
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRate:I

    .line 12
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->eventTime:J

    .line 14
    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->interval:J

    .line 16
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNR:F

    .line 18
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNRUnit:I

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    .line 26
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->readFromParcel(Landroid/os/Parcel;)V

    .line 27
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->time:J

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRate:I

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->eventTime:J

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->interval:J

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNR:F

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNRUnit:I

    .line 78
    const-class v3, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    .line 79
    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 78
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 80
    .local v1, "temp":[Landroid/os/Parcelable;
    if-eqz v1, :cond_1

    .line 81
    array-length v3, v1

    new-array v2, v3, [Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    .line 82
    .local v2, "tmp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-lt v0, v3, :cond_0

    .line 84
    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    .line 87
    .end local v0    # "i":I
    .end local v2    # "tmp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->extra:Landroid/os/Bundle;

    .line 88
    return-void

    .line 83
    .restart local v0    # "i":I
    .restart local v2    # "tmp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    :cond_0
    aget-object v3, v1, v0

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    aput-object v3, v2, v0

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    .end local v0    # "i":I
    .end local v2    # "tmp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[HeartRateMonitor] time ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 93
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->time:J

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", heartRate ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 94
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eventTime ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->eventTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", interval ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 95
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->interval:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SNR ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNR:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SNRUnit ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNRUnit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 36
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->time:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 37
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRate:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 38
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->eventTime:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 39
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->interval:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 40
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNR:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 41
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->SNRUnit:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    if-eqz v2, :cond_1

    .line 44
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    array-length v2, v2

    new-array v1, v2, [Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    .line 45
    .local v1, "temp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 51
    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 55
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 56
    return-void

    .line 47
    .restart local v0    # "i":I
    .restart local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;-><init>()V

    aput-object v2, v1, v0

    .line 48
    aget-object v2, v1, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    aget-object v3, v3, v0

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;->samplingTime:J

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;->samplingTime:J

    .line 49
    aget-object v2, v1, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SHeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;->heartRate:I

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;->heartRate:I

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/SHeartRateRawData;
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    goto :goto_1
.end method
