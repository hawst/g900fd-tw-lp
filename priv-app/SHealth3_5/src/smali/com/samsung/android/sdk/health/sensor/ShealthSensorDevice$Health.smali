.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Health"
.end annotation


# static fields
.field public static final NOT_ASSIGNED_CHAR:C = '\uffff'

.field public static final NOT_ASSIGNED_DOUBLE:D = 1.7976931348623157E308

.field public static final NOT_ASSIGNED_FLOAT:F = 3.4028235E38f

.field public static final NOT_ASSIGNED_INT:I = 0x7fffffff

.field public static final NOT_ASSIGNED_LONG:J = 0x7fffffffffffffffL


# instance fields
.field private mExtraDeviceParams:Landroid/os/Bundle;

.field public time:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2053
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->time:J

    .line 2059
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected readBundleFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 2086
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    .line 2087
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 2088
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->readExtraParamsFromBundle(Landroid/os/Bundle;)V

    .line 2090
    :cond_0
    return-void
.end method

.method protected readExtraParamsFromBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 2111
    return-void
.end method

.method protected writeBundleInParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;

    .prologue
    .line 2070
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 2071
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    .line 2074
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->writeExtraParamsToBundle(Landroid/os/Bundle;)V

    .line 2075
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;->mExtraDeviceParams:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 2076
    return-void
.end method

.method protected writeExtraParamsToBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 2100
    return-void
.end method
