.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$Constants$Configkeys;
.super Ljava/lang/Object;
.source "BaseContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Configkeys"
.end annotation


# static fields
.field public static final INSERT_DUMMY_DATA:Ljava/lang/String; = "insert_data"

.field public static final LOCK_TIME_ZONE_ENABLED:Ljava/lang/String; = "lock_time_zone_enabled"

.field public static final NOTIFICATION_ENABLED:Ljava/lang/String; = "notification_enabled"

.field public static final NOTI_ACHIEVEMENT_ENABLED:Ljava/lang/String; = "noti_achievement_enabled"

.field public static final NOTI_CAREGIVER_ENABLED:Ljava/lang/String; = "noti_caregiver_enabled"

.field public static final NOTI_DOSAGE_ALARM_ENABLED:Ljava/lang/String; = "noti_dosage_alarm_enabled"

.field public static final NOTI_WATER_ENABLED:Ljava/lang/String; = "noti_water_enabled"

.field public static final REQUEST_PASSWORD:Ljava/lang/String; = "request_password"

.field public static final RESTORED_FROM_KIES:Ljava/lang/String; = "restored_data_from_kies"

.field public static final SECURITY_LOCK_PASSWORD:Ljava/lang/String; = "security_lock_password"

.field public static final SECURITY_PIN_ENABLED:Ljava/lang/String; = "security_pin_enabled"

.field public static final TEMPERATURE_UNIT:Ljava/lang/String; = "temperature_unit"

.field public static final USER_ACTIVITY_TYPE:Ljava/lang/String; = "activity_type"

.field public static final USER_BIRTH_DAY:Ljava/lang/String; = "birth_day"

.field public static final USER_BIRTH_MONTH:Ljava/lang/String; = "birth_month"

.field public static final USER_BIRTH_YEAR:Ljava/lang/String; = "birth_year"

.field public static final USER_DISTANCE_UNIT:Ljava/lang/String; = "distance_unit"

.field public static final USER_GENDER:Ljava/lang/String; = "gender"

.field public static final USER_GLUCOSE_UNIT:Ljava/lang/String; = "glucose_unit"

.field public static final USER_HEIGHT:Ljava/lang/String; = "height"

.field public static final USER_HEIGHT_UNIT:Ljava/lang/String; = "height_unit"

.field public static final USER_NAME:Ljava/lang/String; = "name"

.field public static final USER_WATER_UNIT:Ljava/lang/String; = "water_unit"

.field public static final USER_WEIGHT:Ljava/lang/String; = "weight"

.field public static final USER_WEIGHT_UNIT:Ljava/lang/String; = "weight_unit"

.field public static final WEEK_FORMAT:Ljava/lang/String; = "week_format"
