.class public Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;
.super Lcom/samsung/android/sdk/chart/series/SchartSeries;
.source "SchartXYStringSeries.java"


# instance fields
.field protected mDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartSeries;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mSeriesDataType:I

    .line 22
    return-void
.end method


# virtual methods
.method public add(Lcom/samsung/android/sdk/chart/series/SchartStringData;)V
    .locals 1
    .param p1, "stringData"    # Lcom/samsung/android/sdk/chart/series/SchartStringData;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public add(Ljava/lang/String;D)V
    .locals 2
    .param p1, "xValue"    # Ljava/lang/String;
    .param p2, "yValue"    # D

    .prologue
    .line 32
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;

    invoke-direct {v0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/series/SchartStringData;-><init>(Ljava/lang/String;D)V

    .line 33
    .local v0, "data":Lcom/samsung/android/sdk/chart/series/SchartStringData;
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method public getAllData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getX(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 55
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 57
    :cond_0
    const/4 v0, 0x0

    .line 59
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;->mString:Ljava/lang/String;

    goto :goto_0
.end method

.method public getY(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 70
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 72
    :cond_0
    const-wide/16 v0, 0x1

    .line 74
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;->mValue:D

    goto :goto_0
.end method

.method public setAllData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->mDatas:Ljava/util/List;

    .line 103
    return-void
.end method
