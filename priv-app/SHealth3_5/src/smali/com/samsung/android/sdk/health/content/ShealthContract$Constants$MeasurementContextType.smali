.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MeasurementContextType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MeasurementContextType"
.end annotation


# static fields
.field public static final AFTER_BEDTIME:I = 0x13889

.field public static final AFTER_BREAKFAST:I = 0x13884

.field public static final AFTER_DINNER:I = 0x13888

.field public static final AFTER_LUNCH:I = 0x13886

.field public static final AFTER_MEAL:I = 0x13882

.field public static final AFTER_SNACK:I = 0x1388a

.field public static final BEFORE_BREAKFAST:I = 0x13883

.field public static final BEFORE_DINNER:I = 0x13887

.field public static final BEFORE_LUNCH:I = 0x13885

.field public static final FASTING:I = 0x13881

.field public static final MEDICATION_CHANGE:I = 0x1388c

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final SICK:I = 0x1388b
