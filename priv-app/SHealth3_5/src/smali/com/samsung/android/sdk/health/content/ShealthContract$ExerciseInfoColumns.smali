.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfoColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExerciseInfoColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CUSTOM_FLAG:Ljava/lang/String; = "custom_flag"

.field public static final EXERCISE_INFO__ID:Ljava/lang/String; = "exercise_info__id"

.field public static final FAVORITE:Ljava/lang/String; = "favorite"

.field public static final MET:Ljava/lang/String; = "met"

.field public static final MET_HIGH:Ljava/lang/String; = "met_high"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PART_OF_BODY:Ljava/lang/String; = "part_of_body"

.field public static final REPETITION_COUNT:Ljava/lang/String; = "repetition_count"

.field public static final SET_COUNT:Ljava/lang/String; = "set_count"

.field public static final SORTING1:Ljava/lang/String; = "sorting1"

.field public static final SORTING2:Ljava/lang/String; = "sorting2"

.field public static final SOURCE_TYPE:Ljava/lang/String; = "source_type"
