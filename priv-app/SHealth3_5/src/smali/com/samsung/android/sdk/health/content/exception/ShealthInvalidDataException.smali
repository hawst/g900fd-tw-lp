.class public Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;
.super Ljava/lang/IllegalArgumentException;
.source "ShealthInvalidDataException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private column:Ljava/lang/String;

.field private table:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "column"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0, p3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->table:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->column:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getColumn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->column:Ljava/lang/String;

    return-object v0
.end method

.method public getTable()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/exception/ShealthInvalidDataException;->table:Ljava/lang/String;

    return-object v0
.end method
