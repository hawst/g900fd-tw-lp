.class public Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;
.source "SchartStringStackedBarChartView.java"


# instance fields
.field private nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->setDataType(I)V

    .line 36
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->createChart()V

    .line 38
    new-instance v0, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x1

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    invoke-super {p0, v1}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->setDataType(I)V

    .line 55
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartStackedBarChartView;->createChart()V

    .line 57
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mSeriesNum:I

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->createSeries(II)V

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 61
    new-instance v0, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;-><init>(Landroid/content/Context;)V

    .line 75
    const/4 v1, 0x1

    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->createSeries(II)V

    .line 77
    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfXAxis()I

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfYAxis()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->addAxis(II)V

    .line 79
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 81
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 85
    return-void

    .line 82
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->addDataBack(ILjava/util/List;)V

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addDataBack(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 147
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :goto_0
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addData(IILjava/util/List;)V

    .line 151
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public addDataFront(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 165
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addDataFront(IILjava/util/List;)V

    .line 169
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 197
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->clearData()V

    goto :goto_0
.end method

.method public prepareChangeData(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 183
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->prepareChangeData(IILjava/util/List;)V

    goto :goto_0
.end method

.method public setStartIndex(I)V
    .locals 2
    .param p1, "startIndex"    # I

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 113
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartIndex(I)V

    .line 117
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartValidCount(I)V
    .locals 2
    .param p1, "validCount"    # I

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 129
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartIndex(I)V

    .line 133
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartVisual(II)V
    .locals 2
    .param p1, "startIndex"    # I
    .param p2, "validCount"    # I

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 97
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartVisual(II)V

    .line 101
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringStackedBarChartView;->notifyRenderThread()V

    goto :goto_0
.end method
