.class public Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;
.super Ljava/lang/Object;
.source "WearableCommClientSocket.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/wearablecomm/data/IWearableClientSocket;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$1;,
        Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

.field private localClientSocketOutputStream:Ljava/io/BufferedOutputStream;

.field private mLocalClientSocket:Landroid/net/LocalSocket;

.field private mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;

    .line 26
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    .line 29
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;

    .line 33
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .line 34
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;
    .param p1, "x1"    # Landroid/net/LocalSocket;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Ljava/io/BufferedOutputStream;)Ljava/io/BufferedOutputStream;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;
    .param p1, "x1"    # Ljava/io/BufferedOutputStream;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    return-object p1
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 113
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 114
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fd

    iput v1, v0, Landroid/os/Message;->what:I

    .line 115
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 117
    :cond_0
    return-void
.end method

.method public finishSending()V
    .locals 2

    .prologue
    .line 95
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 96
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fc

    iput v1, v0, Landroid/os/Message;->what:I

    .line 97
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 98
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 99
    :cond_0
    return-void
.end method

.method public initialize()V
    .locals 9

    .prologue
    .line 39
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 40
    .local v2, "msg":Landroid/os/Message;
    const/16 v5, 0x43f8

    iput v5, v2, Landroid/os/Message;->what:I

    .line 42
    new-instance v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;-><init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$1;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    .line 43
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->start()V

    .line 45
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 46
    .local v3, "startTime":J
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v5

    if-nez v5, :cond_2

    .line 48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    const-wide/16 v7, 0x3e8

    div-long v0, v5, v7

    .line 49
    .local v0, "elapsedTime":J
    const-wide/16 v5, 0x1

    cmp-long v5, v0, v5

    if-lez v5, :cond_0

    .line 51
    sget-object v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;

    const-string v6, "Problem initializing the client socket!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    if-eqz v5, :cond_1

    .line 54
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 60
    .end local v0    # "elapsedTime":J
    :cond_1
    :goto_0
    return-void

    .line 59
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public receiveData()V
    .locals 2

    .prologue
    .line 104
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 105
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 106
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 108
    :cond_0
    return-void
.end method

.method public sendCertificationData(J)V
    .locals 2
    .param p1, "nonceNum"    # J

    .prologue
    .line 71
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 72
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fe

    iput v1, v0, Landroid/os/Message;->what:I

    .line 73
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 74
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 76
    :cond_0
    return-void
.end method

.method public sendData([B)I
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 81
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 82
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 83
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 84
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 89
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 87
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .line 66
    return-void
.end method
