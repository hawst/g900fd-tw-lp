.class public Lcom/samsung/android/sdk/health/content/ShealthProfile;
.super Ljava/lang/Object;
.source "ShealthProfile.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private activity:I

.field private activityType:I

.field private birthDate:Ljava/lang/String;

.field private transient context:Landroid/content/Context;

.field private country:Ljava/lang/String;

.field private distanceUnit:I

.field private gender:I

.field private height:F

.field private heightUnit:I

.field private isProfileChanged:Z

.field private name:Ljava/lang/String;

.field private profileCreateTime:J

.field private profileDiscloseYN:Ljava/lang/String;

.field private temperatureUnit:I

.field private updateTime:J

.field private weight:F

.field private weightUnit:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const-class v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->load()V

    .line 95
    return-void
.end method

.method private checkValidity()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const v4, 0x461c4000    # 10000.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 507
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0x32

    if-gt v1, v2, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    const v2, 0x2e635

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    const v2, 0x2e636

    if-ne v1, v2, :cond_3

    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    const v2, 0x249f1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    const v2, 0x249f2

    if-ne v1, v2, :cond_3

    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    cmpg-float v1, v1, v3

    if-ltz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    const v2, 0x1fbd1

    if-eq v1, v2, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    const v2, 0x1fbd2

    if-ne v1, v2, :cond_3

    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf24

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf22

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf21

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf23

    if-eq v1, v2, :cond_4

    iget v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    const v2, 0x2bf25

    if-eq v1, v2, :cond_4

    .line 523
    :cond_3
    const/4 v0, 0x0

    .line 525
    :cond_4
    return v0
.end method

.method private convertBooleanToString(Z)Ljava/lang/String;
    .locals 1
    .param p1, "isProfileDisclose"    # Z

    .prologue
    .line 563
    if-eqz p1, :cond_0

    .line 564
    const-string v0, "Y"

    .line 566
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "N"

    goto :goto_0
.end method

.method private convertStringToBoolean(Ljava/lang/String;)Z
    .locals 2
    .param p1, "profileDiscloseValue"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 551
    if-eqz p1, :cond_0

    .line 553
    const-string v1, "Y"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 554
    const/4 v0, 0x1

    .line 558
    :cond_0
    return v0
.end method

.method private getSystemTimeMillis(Ljava/lang/String;)J
    .locals 7
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    const-wide/16 v3, 0x0

    .line 573
    const/4 v2, 0x0

    .line 574
    .local v2, "tempDate":Ljava/util/Date;
    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyyyMMdd"

    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v5, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 575
    .local v1, "formatter":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 576
    if-nez v2, :cond_0

    .line 593
    .end local v1    # "formatter":Ljava/text/SimpleDateFormat;
    :goto_0
    return-wide v3

    .line 579
    .restart local v1    # "formatter":Ljava/text/SimpleDateFormat;
    :cond_0
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-wide v3

    goto :goto_0

    .line 581
    .end local v1    # "formatter":Ljava/text/SimpleDateFormat;
    :catch_0
    move-exception v0

    .line 583
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0

    .line 585
    .end local v0    # "e":Ljava/text/ParseException;
    :catch_1
    move-exception v0

    .line 587
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 589
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 591
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getActivity()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    return v0
.end method

.method public getActivityType()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    return v0
.end method

.method public getBirthDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    return-object v0
.end method

.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    return-object v0
.end method

.method public getDistanceUnit()I
    .locals 1

    .prologue
    .line 347
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    return v0
.end method

.method public getGender()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    return v0
.end method

.method public getHeightUnit()I
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    return v0
.end method

.method public getLastProfileUpdateTime()J
    .locals 2

    .prologue
    .line 389
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->updateTime:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getProfileCreateTime()J
    .locals 2

    .prologue
    .line 418
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    return-wide v0
.end method

.method public getProfileDisclose()Z
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->convertStringToBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getTemperatureUnit()I
    .locals 1

    .prologue
    .line 374
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    return v0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    return v0
.end method

.method public getWeightUnit()I
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    return v0
.end method

.method public load()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    const/4 v8, -0x1

    .line 449
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 450
    .local v1, "extras":Landroid/os/Bundle;
    const-string/jumbo v4, "value"

    invoke-virtual {v1, v4, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 451
    iget-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v6, "CONFIG_OPTION_GET"

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v1}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 454
    .local v2, "result":Landroid/os/Bundle;
    const-string/jumbo v4, "value"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;

    .line 455
    .local v3, "resultProfile":Lcom/samsung/android/sdk/health/content/ShealthProfile;
    if-eqz v3, :cond_8

    .line 456
    iget-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 458
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    :cond_0
    :goto_0
    iget-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 466
    const-string v4, ""

    iput-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 468
    :cond_1
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    if-nez v4, :cond_2

    .line 469
    iput v8, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    .line 471
    :cond_2
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    if-nez v4, :cond_3

    .line 472
    iput v8, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    .line 474
    :cond_3
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    if-nez v4, :cond_4

    .line 475
    iput v8, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    .line 477
    :cond_4
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    if-nez v4, :cond_5

    .line 478
    iput v8, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    .line 480
    :cond_5
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    if-nez v4, :cond_6

    .line 481
    iput v8, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    .line 483
    :cond_6
    iget-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    if-nez v4, :cond_7

    .line 484
    const-string v4, "N"

    iput-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    .line 486
    :cond_7
    iget-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    iput-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    .line 487
    iget-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    iput-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 488
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    .line 489
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    .line 490
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    .line 491
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    .line 492
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    .line 493
    iget-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    iput-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    .line 494
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    .line 495
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    .line 496
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    .line 497
    const-string/jumbo v4, "update_time"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->updateTime:J

    .line 498
    iget-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    iput-object v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    .line 499
    const-string/jumbo v4, "profile_create_time"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    .line 500
    iget v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    iput v4, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    .line 504
    :goto_1
    return-void

    .line 459
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "XXX"

    iput-object v4, v3, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    goto :goto_0

    .line 503
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_8
    sget-object v4, Lcom/samsung/android/sdk/health/content/ShealthProfile;->TAG:Ljava/lang/String;

    const-string/jumbo v5, "resultProfile is null."

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public save()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 535
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->checkValidity()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 536
    iget-boolean v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    if-eqz v1, :cond_1

    .line 537
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->updateTime:J

    .line 538
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    .line 539
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileCreateTime:J

    .line 540
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 541
    .local v0, "extras":Landroid/os/Bundle;
    const-string/jumbo v1, "value"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 542
    iget-object v1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/health/content/ShealthContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v3, "CONFIG_OPTION_PUT"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 547
    .end local v0    # "extras":Landroid/os/Bundle;
    :cond_1
    return-void

    .line 546
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Some of user information is not valid."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setActivity(I)V
    .locals 0
    .param p1, "activity"    # I

    .prologue
    .line 289
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activity:I

    .line 290
    return-void
.end method

.method public setActivityType(I)V
    .locals 2
    .param p1, "activityType"    # I

    .prologue
    .line 330
    const v0, 0x2bf24

    if-eq p1, v0, :cond_0

    const v0, 0x2bf22

    if-eq p1, v0, :cond_0

    const v0, 0x2bf21

    if-eq p1, v0, :cond_0

    const v0, 0x2bf23

    if-eq p1, v0, :cond_0

    const v0, 0x2bf25

    if-eq p1, v0, :cond_0

    .line 334
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity type is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    if-eq p1, v0, :cond_1

    .line 336
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 338
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->activityType:I

    .line 339
    return-void
.end method

.method public setBirthDate(Ljava/lang/String;)V
    .locals 4
    .param p1, "birthDate"    # Ljava/lang/String;

    .prologue
    .line 300
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->getSystemTimeMillis(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Birthdate is in future."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 305
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->birthDate:Ljava/lang/String;

    .line 306
    return-void
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 2
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 113
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    .line 114
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Country should be composed with 3 characters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 118
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->country:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setDistanceUnit(I)V
    .locals 2
    .param p1, "distanceUnit"    # I

    .prologue
    .line 361
    const v0, 0x29811

    if-eq p1, v0, :cond_0

    const v0, 0x29813

    if-eq p1, v0, :cond_0

    const v0, 0x29812

    if-eq p1, v0, :cond_0

    .line 364
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Distance unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 365
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    if-eq p1, v0, :cond_1

    .line 366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 368
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->distanceUnit:I

    .line 369
    return-void
.end method

.method public setGender(I)V
    .locals 2
    .param p1, "gender"    # I

    .prologue
    .line 163
    const v0, 0x2e635

    if-eq p1, v0, :cond_0

    const v0, 0x2e636

    if-eq p1, v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Gender is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    if-eq p1, v0, :cond_1

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 168
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->gender:I

    .line 169
    return-void
.end method

.method public setHeight(F)V
    .locals 2
    .param p1, "height"    # F

    .prologue
    .line 189
    const/high16 v0, 0x41a00000    # 20.0f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x43960000    # 300.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 190
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Height range should be 20~300"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_2

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 194
    :cond_2
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->height:F

    .line 195
    return-void
.end method

.method public setHeightUnit(I)V
    .locals 2
    .param p1, "heightUnit"    # I

    .prologue
    .line 214
    const v0, 0x249f1

    if-eq p1, v0, :cond_0

    const v0, 0x249f2

    if-eq p1, v0, :cond_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Height unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    if-eq p1, v0, :cond_1

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 219
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->heightUnit:I

    .line 220
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 138
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_1

    .line 139
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Name should be composed with 1~50 characters"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 141
    iput-boolean v2, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 143
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->name:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public setProfileDisclose(Z)V
    .locals 1
    .param p1, "isProfileDiscloseYN"    # Z

    .prologue
    .line 398
    iget-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->convertStringToBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 400
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/content/ShealthProfile;->convertBooleanToString(Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->profileDiscloseYN:Ljava/lang/String;

    .line 401
    return-void
.end method

.method public setTemperatureUnit(I)V
    .locals 2
    .param p1, "temperatureUnit"    # I

    .prologue
    .line 379
    const v0, 0x27101

    if-eq p1, v0, :cond_0

    const v0, 0x27102

    if-eq p1, v0, :cond_0

    .line 381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Temperature unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    if-eq p1, v0, :cond_1

    .line 383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 385
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->temperatureUnit:I

    .line 386
    return-void
.end method

.method public setWeight(F)V
    .locals 2
    .param p1, "weight"    # F

    .prologue
    .line 240
    const/high16 v0, 0x40000000    # 2.0f

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x43fa0000    # 500.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 241
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Weight range should be 2~500"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_2

    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 245
    :cond_2
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weight:F

    .line 246
    return-void
.end method

.method public setWeightUnit(I)V
    .locals 2
    .param p1, "weightUnit"    # I

    .prologue
    .line 266
    const v0, 0x1fbd1

    if-eq p1, v0, :cond_0

    const v0, 0x1fbd2

    if-eq p1, v0, :cond_0

    .line 267
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Weight unit is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    if-eq p1, v0, :cond_1

    .line 269
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->isProfileChanged:Z

    .line 271
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/health/content/ShealthProfile;->weightUnit:I

    .line 272
    return-void
.end method
