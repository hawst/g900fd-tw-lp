.class public Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;
.super Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
.source "SchartBarSeriesStyle.java"


# static fields
.field public static final BAR_ALIGN_BETWEEN_LEFT:I = 0x4

.field public static final BAR_ALIGN_BETWEEN_RIGHT:I = 0x3

.field public static final BAR_ALIGN_CENTER:I = 0x1

.field public static final BAR_ALIGN_LEFT:I = 0x0

.field public static final BAR_ALIGN_RIGHT:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;-><init>()V

    .line 10
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mSeriesType:I

    .line 11
    return-void
.end method


# virtual methods
.method public getBarAlign()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getBarAlign()I

    move-result v0

    return v0
.end method

.method public getBarColor()I
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 115
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 116
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 117
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 114
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getBarStrokeWidth()F
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public getBarWidthInTime()J
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getBarWidthInTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public setBarAlign(I)V
    .locals 1
    .param p1, "align"    # I

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setAlign(I)V

    .line 29
    return-void
.end method

.method public setBarColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setFillColor(IIII)V

    .line 58
    return-void
.end method

.method public setBarGradientColors([I[F)V
    .locals 4
    .param p1, "colors"    # [I
    .param p2, "values"    # [F

    .prologue
    .line 67
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 69
    array-length v0, p1

    .line 70
    .local v0, "colorsSize":I
    array-length v1, p2

    .line 72
    .local v1, "valuesSize":I
    if-eq v0, v1, :cond_0

    .line 74
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "gradient colors set error"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 78
    .end local v0    # "colorsSize":I
    .end local v1    # "valuesSize":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v2, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setFillGradientColors([I)V

    .line 79
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v2, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setFillGradientValues([F)V

    .line 80
    return-void
.end method

.method public setBarWidth(F)V
    .locals 1
    .param p1, "strokeWidth"    # F

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setStrokeWidth(F)V

    .line 48
    return-void
.end method

.method public setBarWidth(J)V
    .locals 1
    .param p1, "barWidthInTime"    # J

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBarSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setBarWidthInTime(J)V

    .line 39
    return-void
.end method
