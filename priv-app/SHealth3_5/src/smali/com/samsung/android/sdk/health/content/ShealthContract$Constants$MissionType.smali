.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MissionType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MissionType"
.end annotation


# static fields
.field public static final CALOIRE:I = 0x3

.field public static final DISTANCE:I = 0x1

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final NO_GOAL:I = 0x0

.field public static final TIME:I = 0x2

.field public static final TRAINING_EFFECT_INTENSITY:I = 0x4

.field public static final TRAINING_EFFECT_TIME:I = 0x5
