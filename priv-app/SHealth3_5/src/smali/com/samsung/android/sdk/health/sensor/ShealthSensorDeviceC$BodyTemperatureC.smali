.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$BodyTemperatureC;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BodyTemperatureC"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toBulkBodyTemperature([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;
    .locals 6
    .param p0, "data"    # [Landroid/os/Bundle;

    .prologue
    .line 994
    if-eqz p0, :cond_1

    .line 996
    array-length v3, p0

    new-array v0, v3, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    .line 997
    .local v0, "bodyTemperature":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_2

    .line 999
    aget-object v1, p0, v2

    .line 1000
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 1002
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;-><init>()V

    aput-object v3, v0, v2

    .line 1003
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string/jumbo v5, "temperature"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperature:F

    .line 1004
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string/jumbo v5, "time_stamp"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->time:J

    .line 1005
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string/jumbo v5, "temperature_unit"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureUnit:I

    .line 1006
    aget-object v3, v0, v2

    aget-object v4, p0, v2

    const-string/jumbo v5, "temperature_type"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;->temperatureType:I

    .line 997
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1011
    .end local v0    # "bodyTemperature":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "index":I
    :cond_1
    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method
