.class public interface abstract Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;
.super Ljava/lang/Object;
.source "PrivilegeSensorDataListener.java"


# static fields
.field public static final ERROR_FAILURE:I = 0x1

.field public static final ERROR_NONE:I


# virtual methods
.method public abstract onReceived(ILandroid/os/Bundle;)V
.end method

.method public abstract onReceived(I[Landroid/os/Bundle;)V
.end method

.method public abstract onStarted(II)V
.end method

.method public abstract onStopped(II)V
.end method
