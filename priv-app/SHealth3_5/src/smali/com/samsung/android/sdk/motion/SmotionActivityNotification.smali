.class public Lcom/samsung/android/sdk/motion/SmotionActivityNotification;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;,
        Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;,
        Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;,
        Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;
    }
.end annotation


# static fields
.field private static d:Z

.field private static e:Lcom/samsung/android/sdk/motion/Smotion;


# instance fields
.field private a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

.field private b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;

.field private c:Landroid/hardware/scontext/SContextListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->e:Lcom/samsung/android/sdk/motion/Smotion;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Lcom/samsung/android/sdk/motion/Smotion;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->c:Landroid/hardware/scontext/SContextListener;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionActivityNotification : Looper is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionActivityNotification : Smotion is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivityNotification : Smotion.initialize() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification : Smotion.initialize() is not successful."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;

    sput-object p2, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->e:Lcom/samsung/android/sdk/motion/Smotion;

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->d:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method static synthetic a()Lcom/samsung/android/sdk/motion/Smotion;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->e:Lcom/samsung/android/sdk/motion/Smotion;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification;JII)Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;

    invoke-direct {v0}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;-><init>()V

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;J)V

    packed-switch p3, :pswitch_data_0

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    :goto_0
    packed-switch p4, :pswitch_data_1

    :goto_1
    return-object v0

    :pswitch_0
    invoke-static {v0, v3}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    goto :goto_0

    :pswitch_1
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    goto :goto_0

    :pswitch_4
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->b(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    goto :goto_1

    :pswitch_5
    invoke-static {v0, v3}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->b(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    goto :goto_1

    :pswitch_6
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;->b(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->d:Z

    return v0
.end method


# virtual methods
.method public start(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;)V
    .locals 4

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivityNotification : InfoFilter is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivityNotification : InfoFilter is empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivityNotification : ChangeListener is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {p1}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [I

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

    if-nez v0, :cond_3

    iput-object p2, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->d:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification : This device is not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification : ChangeListener is already registered."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-static {p1}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_5

    if-nez p2, :cond_6

    const/4 v0, 0x0

    :goto_1
    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->c:Landroid/hardware/scontext/SContextListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->c:Landroid/hardware/scontext/SContextListener;

    const/16 v3, 0x1b

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;->registerListener(Landroid/hardware/scontext/SContextListener;I[I)Z

    return-void

    :cond_5
    invoke-static {p1}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$InfoFilter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_6
    new-instance v0, Lcom/samsung/android/sdk/motion/b;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/motion/b;-><init>(Lcom/samsung/android/sdk/motion/SmotionActivityNotification;Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;)V

    goto :goto_1
.end method

.method public stop()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivityNotification : start() is not called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->c:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x1b

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$a;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->c:Landroid/hardware/scontext/SContextListener;

    return-void
.end method
