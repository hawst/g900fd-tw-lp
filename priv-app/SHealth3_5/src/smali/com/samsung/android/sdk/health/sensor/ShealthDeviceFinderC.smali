.class public Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
.super Ljava/lang/Object;
.source "ShealthDeviceFinderC.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLock:Ljava/lang/Object;

.field private mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

.field private mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

.field private mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

.field private mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .prologue
    .line 272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mLock:Ljava/lang/Object;

    .line 273
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinderC() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 276
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context and Listener can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 280
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mContext:Landroid/content/Context;

    .line 285
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    .line 286
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 288
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    .line 289
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    invoke-direct {v0, p1, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .line 291
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 301
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinderC cleanup finished, this instance is now unusable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->close()V

    .line 307
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 308
    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .line 310
    return-void
.end method

.method public getAPIVersion()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->getAPIVersion()I

    move-result v0

    return v0
.end method

.method public getConnectedDeviceList(III)Ljava/util/List;
    .locals 9
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 396
    sget-object v5, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ShealthDeviceFinderC getConnectedDeviceList - connectivityType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " deviceType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " dataType : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v5, :cond_0

    .line 399
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v6, "Local Service not bound"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 404
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v5, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;

    move-result-object v3

    .line 405
    .local v3, "normalDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-nez v3, :cond_1

    .line 406
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "normalDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 407
    .restart local v3    # "normalDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    :cond_1
    const/4 v4, 0x0

    .line 409
    .local v4, "privilegeDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;>;"
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    if-nez v5, :cond_3

    .line 410
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v6, "Remote Service not bound"

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 414
    :catch_0
    move-exception v1

    .line 416
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;->printStackTrace()V

    .line 422
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :goto_0
    if-eqz v4, :cond_4

    .line 427
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 429
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;

    .line 430
    .local v0, "device":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    if-eqz v0, :cond_2

    .line 432
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mContext:Landroid/content/Context;

    invoke-static {v0, v5}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toSensorDevice(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;Landroid/content/Context;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 412
    .end local v0    # "device":Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    .end local v2    # "index":I
    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeConnectivityType(I)I

    move-result v6

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDeviceType(I)I

    move-result v7

    invoke-static {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDataType(I)I

    move-result v8

    invoke-virtual {v5, v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->getConnectedDeviceList(III)Ljava/util/List;
    :try_end_1
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    goto :goto_0

    .line 417
    :catch_1
    move-exception v1

    .line 419
    .local v1, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;->printStackTrace()V

    goto :goto_0

    .line 436
    .end local v1    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorNotSupportedException;
    :cond_4
    return-object v3
.end method

.method public isAvailable(IIILjava/lang/String;)Z
    .locals 5
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .param p4, "protocol"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 455
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v2, "ShealthDeviceFinderC isAvailable() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v1, :cond_0

    .line 458
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Local Service not bound"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 460
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    if-nez v1, :cond_1

    .line 461
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Remote Service not bound"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 464
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 465
    const/4 v1, 0x1

    .line 475
    :goto_0
    return v1

    .line 467
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeConnectivityType(I)I

    move-result v2

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDeviceType(I)I

    move-result v3

    invoke-static {p3}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toPrivilegeDataType(I)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;->isAvailable(III)Z
    :try_end_0
    .catch Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    goto :goto_0

    .line 468
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;->printStackTrace()V

    .line 475
    .end local v0    # "e":Lcom/samsung/android/sdk/health/sensor/exception/PrivilegeSensorServiceNotBoundException;
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 471
    :catch_1
    move-exception v0

    .line 472
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v2, "ShealthDeviceFinderC isAvailable() not supported device"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public startScan(IIIILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V
    .locals 6
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .param p4, "durationSeconds"    # I
    .param p5, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 333
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinderC startScan() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_0

    .line 335
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "Local Service not bound"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_0
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;

    invoke-direct {v5, p0, p5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V

    .line 338
    .local v5, "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->startScan(IIIILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    .line 339
    return-void
.end method

.method public startScan(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V
    .locals 3
    .param p1, "connectivityType"    # I
    .param p2, "deviceId"    # Ljava/lang/String;
    .param p3, "durationSeconds"    # I
    .param p4, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    .line 357
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v2, "ShealthDeviceFinderC startScan() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v1, :cond_0

    .line 359
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "Local Service not bound"

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 361
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;

    invoke-direct {v0, p0, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ScanListener;)V

    .line 362
    .local v0, "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$WrapperScanListener;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->startScan(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    .line 363
    return-void
.end method

.method public stopScan()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 372
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinderC - stopScan"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    if-nez v0, :cond_0

    .line 374
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "Local Service not bound"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->stopScan()V

    .line 377
    return-void
.end method
