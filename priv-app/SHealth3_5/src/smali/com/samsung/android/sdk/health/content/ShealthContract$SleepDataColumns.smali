.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$SleepDataColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SleepDataColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final SLEEP_STATUS:Ljava/lang/String; = "sleep_status"

.field public static final SLEEP__ID:Ljava/lang/String; = "sleep__id"
