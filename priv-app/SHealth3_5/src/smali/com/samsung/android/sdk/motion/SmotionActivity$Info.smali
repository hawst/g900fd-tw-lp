.class public Lcom/samsung/android/sdk/motion/SmotionActivity$Info;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/motion/SmotionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# static fields
.field public static final ACCURACY_HIGH:I = 0x2

.field public static final ACCURACY_LOW:I = 0x0

.field public static final ACCURACY_MID:I = 0x1

.field public static final MODE_ALL:I = 0x0

.field public static final MODE_BATCH:I = 0x2

.field public static final MODE_REALTIME:I = 0x1

.field public static final STATUS_RUN:I = 0x3

.field public static final STATUS_STATIONARY:I = 0x1

.field public static final STATUS_UNKNOWN:I = 0x0

.field public static final STATUS_VEHICLE:I = 0x4

.field public static final STATUS_WALK:I = 0x2


# instance fields
.field private a:I

.field private b:I

.field private c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionActivity;->a()Lcom/samsung/android/sdk/motion/Smotion;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity.Info : SmotionActivity is not created. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionActivity;->b()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity.Info : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a:I

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;J)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->c:J

    return-void
.end method

.method static synthetic b(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->b:I

    return-void
.end method


# virtual methods
.method public getAccuracy()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->b:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a:I

    return v0
.end method

.method public getTimeStamp()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->c:J

    return-wide v0
.end method
