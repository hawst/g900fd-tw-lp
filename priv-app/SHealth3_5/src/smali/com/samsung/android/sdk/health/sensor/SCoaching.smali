.class public Lcom/samsung/android/sdk/health/sensor/SCoaching;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SCoaching.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SCoaching;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public ac:C

.field public lastTrainingLevelUpdate:J

.field public latestExerciseTime:J

.field public latestFeedbackPhraseNumber:I

.field public maxHeartRate:C

.field public maxMET:J

.field public previousToPreviousTrainingLevel:I

.field public previousTrainingLevel:I

.field public recourceRecovery:I

.field public startDate:J

.field public trainingLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SCoaching$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SCoaching$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 118
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0xffff

    const-wide v1, 0x7fffffffffffffffL

    const v0, 0x7fffffff

    .line 40
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 9
    iput-char v3, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    .line 11
    iput-char v3, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    .line 16
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    .line 18
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    .line 20
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    .line 25
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    .line 27
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    .line 29
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    .line 31
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    .line 36
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    .line 38
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const v3, 0xffff

    const-wide v1, 0x7fffffffffffffffL

    const v0, 0x7fffffff

    .line 44
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 9
    iput-char v3, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    .line 11
    iput-char v3, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    .line 16
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    .line 18
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    .line 20
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    .line 25
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    .line 27
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    .line 29
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    .line 31
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    .line 36
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    .line 38
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J

    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SCoaching;->readFromParcel(Landroid/os/Parcel;)V

    .line 47
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 91
    const/4 v1, 0x2

    new-array v0, v1, [C

    .line 92
    .local v0, "temp":[C
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readCharArray([C)V

    .line 93
    const/4 v1, 0x0

    aget-char v1, v0, v1

    iput-char v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    .line 94
    const/4 v1, 0x1

    aget-char v1, v0, v1

    iput-char v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->extra:Landroid/os/Bundle;

    .line 105
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[COACHING VAR]\n ac ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-char v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxHeartRate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", maxMET ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 78
    const-string v1, ", recourceRecovery = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 79
    const-string v1, ", startDate ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", trainingLevel = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startDate ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 80
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 81
    const-string v1, ", lastTrainingLevelUpdate = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 82
    const-string v1, ", previousTrainingLevel ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 83
    const-string v1, ", previousToPreviousTrainingLevel = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 84
    const-string v1, ", latestFeedbackPhraseNumber ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 85
    const-string v1, ", latestExerciseTime ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 86
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 58
    const/4 v1, 0x2

    new-array v0, v1, [C

    const/4 v1, 0x0

    .line 59
    iget-char v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->ac:C

    aput-char v2, v0, v1

    const/4 v1, 0x1

    iget-char v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxHeartRate:C

    aput-char v2, v0, v1

    .line 61
    .local v0, "temp":[C
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    .line 62
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->maxMET:J

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 63
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->recourceRecovery:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->startDate:J

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 65
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->trainingLevel:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->lastTrainingLevelUpdate:J

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 67
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousTrainingLevel:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 68
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->previousToPreviousTrainingLevel:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 69
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestFeedbackPhraseNumber:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->latestExerciseTime:J

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 71
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoaching;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 72
    return-void
.end method
