.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$BloodPressureColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BloodPressureColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final DIASTOLIC:Ljava/lang/String; = "diastolic"

.field public static final INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final MEAN:Ljava/lang/String; = "mean"

.field public static final PULSE:Ljava/lang/String; = "pulse"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final SYSTOLIC:Ljava/lang/String; = "systolic"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"
