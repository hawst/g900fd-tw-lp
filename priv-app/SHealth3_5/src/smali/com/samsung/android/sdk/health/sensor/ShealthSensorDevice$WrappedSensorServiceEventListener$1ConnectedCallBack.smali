.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->onConnected(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConnectedCallBack"
.end annotation


# instance fields
.field error:I

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;I)V
    .locals 0
    .param p2, "error"    # I

    .prologue
    .line 414
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 415
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->error:I

    .line 416
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 420
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->error:I

    if-nez v0, :cond_1

    .line 421
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 426
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->error:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;->onJoined(I)V

    .line 428
    :cond_0
    return-void

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ConnectedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_JOINED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    goto :goto_0
.end method
