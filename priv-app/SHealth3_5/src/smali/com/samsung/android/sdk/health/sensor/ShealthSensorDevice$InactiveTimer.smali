.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$InactiveTimer;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InactiveTimer"
.end annotation


# static fields
.field public static final INACTIVE_ACHIEVE:I = 0x2

.field public static final INACTIVE_BREAK:I = 0x1

.field public static final INACTIVE_START:I


# instance fields
.field public duration:I

.field public isTimeOut:Z

.field public status:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const v0, 0x7fffffff

    .line 2842
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2851
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$InactiveTimer;->duration:I

    .line 2852
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$InactiveTimer;->status:I

    .line 2853
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$InactiveTimer;->isTimeOut:Z

    return-void
.end method
