.class public Lcom/samsung/android/sdk/chart/view/SchartHandlerData;
.super Ljava/lang/Object;
.source "SchartHandlerData.java"


# instance fields
.field private posX:F

.field private posYList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private seriesId:I

.field private seriesXValue:Ljava/lang/String;

.field private seriesYValueList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->posX:F

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesId:I

    .line 16
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->posYList:Ljava/util/Vector;

    .line 17
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesYValueList:Ljava/util/Vector;

    .line 18
    return-void
.end method


# virtual methods
.method public getPosX()F
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->posX:F

    return v0
.end method

.method public getPosYList()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->posYList:Ljava/util/Vector;

    return-object v0
.end method

.method public getSeriesId()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesId:I

    return v0
.end method

.method public getSeriesXValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesXValue:Ljava/lang/String;

    return-object v0
.end method

.method public getSeriesYValueList()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesYValueList:Ljava/util/Vector;

    return-object v0
.end method

.method public setPosX(F)V
    .locals 0
    .param p1, "posX"    # F

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->posX:F

    .line 26
    return-void
.end method

.method public setPosYList(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "posYList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Float;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->posYList:Ljava/util/Vector;

    .line 50
    return-void
.end method

.method public setSeriesId(I)V
    .locals 0
    .param p1, "seriesId"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesId:I

    .line 34
    return-void
.end method

.method public setSeriesXValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "seriesXValue"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesXValue:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setSeriesYValueList(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p1, "seriesYValueList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartHandlerData;->seriesYValueList:Ljava/util/Vector;

    .line 58
    return-void
.end method
