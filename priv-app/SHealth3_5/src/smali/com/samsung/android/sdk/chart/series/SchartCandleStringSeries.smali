.class public Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;
.super Lcom/samsung/android/sdk/chart/series/SchartSeries;
.source "SchartCandleStringSeries.java"


# instance fields
.field protected mDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartSeries;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mSeriesDataType:I

    .line 21
    return-void
.end method


# virtual methods
.method public add(Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;)V
    .locals 1
    .param p1, "stringData"    # Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method public add(Ljava/lang/String;DDDD)V
    .locals 10
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "yValueHigh"    # D
    .param p4, "yValueLow"    # D
    .param p6, "yValueOpen"    # D
    .param p8, "yValueClose"    # D

    .prologue
    .line 34
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;-><init>(Ljava/lang/String;DDDD)V

    .line 36
    .local v0, "stringData":Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method

.method public getAllData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getX(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 56
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 58
    :cond_0
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;->mString:Ljava/lang/String;

    goto :goto_0
.end method

.method public getY(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 69
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 71
    :cond_0
    const-wide/16 v0, 0x1

    .line 73
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/series/SchartStringData;->mValue:D

    goto :goto_0
.end method

.method public setAllData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->mDatas:Ljava/util/List;

    .line 98
    return-void
.end method
