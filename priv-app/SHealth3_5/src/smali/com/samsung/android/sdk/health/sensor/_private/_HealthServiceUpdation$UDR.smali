.class public Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$UDR;
.super Ljava/lang/Object;
.source "_HealthServiceUpdation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UDR"
.end annotation


# static fields
.field public static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field public static final PACKAGE_SAMSUNGAPPS:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final PATH_LOCAL_TEST_UPDATE:Ljava/lang/String; = "mnt/sdcard/healthservice_update.xml"

.field public static final PATH_PD:Ljava/lang/String; = "mnt/sdcard/go_to_andromeda.test"

.field public static final PKG_NAME:Ljava/lang/String; = "com.sec.android.service.health"

.field public static final PTAG_APPID:Ljava/lang/String; = "appId"

.field public static final PTAG_APPINFO:Ljava/lang/String; = "appInfo"

.field public static final PTAG_RESULTCODE:Ljava/lang/String; = "resultCode"

.field public static final PTAG_RESULT_MSG:Ljava/lang/String; = "resultMsg"

.field public static final PTAG_VERSION:Ljava/lang/String; = "version"

.field public static final PTAG_VERSION_CODE:Ljava/lang/String; = "versionCode"

.field public static final SAMSUNGAPPS_MAIN:Ljava/lang/String; = "com.sec.android.app.samsungapps.Main"

.field public static final SERVER_URL:Ljava/lang/String; = "http://hub.samsungapps.com/product/appCheck.as"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
