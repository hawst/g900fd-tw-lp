.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Coaching"
.end annotation


# instance fields
.field public ac:C

.field public lastTrainingLevelUpdate:J

.field public latestExerciseTime:J

.field public latestFeedbackPhraseNumber:I

.field public maxHeartRate:C

.field public maxMET:J

.field public previousToPreviousTrainingLevel:I

.field public previousTrainingLevel:I

.field public recourceRecovery:I

.field public startDate:J

.field public trainingLevel:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0xffff

    const-wide v1, 0x7fffffffffffffffL

    const v0, 0x7fffffff

    .line 2616
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2618
    iput-char v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->ac:C

    .line 2620
    iput-char v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->maxHeartRate:C

    .line 2625
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->maxMET:J

    .line 2627
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->recourceRecovery:I

    .line 2629
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->startDate:J

    .line 2634
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->trainingLevel:I

    .line 2636
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->lastTrainingLevelUpdate:J

    .line 2638
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->previousTrainingLevel:I

    .line 2640
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->previousToPreviousTrainingLevel:I

    .line 2645
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->latestFeedbackPhraseNumber:I

    .line 2647
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Coaching;->latestExerciseTime:J

    return-void
.end method
