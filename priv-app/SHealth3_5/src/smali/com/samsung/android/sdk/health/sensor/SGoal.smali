.class public Lcom/samsung/android/sdk/health/sensor/SGoal;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SGoal.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/SGoal$GoalType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SGoal;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public goal:I

.field public isAcheived:I

.field public time:J

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SGoal$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SGoal$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SGoal;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 79
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    .line 27
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 19
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->type:I

    .line 21
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    .line 23
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    .line 25
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->isAcheived:I

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const v2, 0x7fffffff

    .line 31
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 19
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->type:I

    .line 21
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    .line 23
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    .line 25
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->isAcheived:I

    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SGoal;->readFromParcel(Landroid/os/Parcel;)V

    .line 34
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->type:I

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->isAcheived:I

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->extra:Landroid/os/Bundle;

    .line 66
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SGoal]\n type ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", time = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", goal ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 56
    const-string v1, ", isAcheived = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->isAcheived:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->type:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 46
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 47
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->goal:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->isAcheived:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SGoal;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method
