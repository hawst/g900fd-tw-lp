.class public Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;
.super Ljava/lang/Object;
.source "WearableCommUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final byte2ToInt([BI)I
    .locals 2
    .param p0, "src"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 35
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static final byteToInt([B)I
    .locals 1
    .param p0, "src"    # [B

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;->byteToInt([BI)I

    move-result v0

    return v0
.end method

.method public static final byteToInt([BI)I
    .locals 2
    .param p0, "src"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 30
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v1, p1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x2

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    add-int/lit8 v1, p1, 0x3

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public static final byteToInt([BII)I
    .locals 4
    .param p0, "src"    # [B
    .param p1, "offset"    # I
    .param p2, "len"    # I

    .prologue
    .line 39
    const/4 v1, 0x0

    .line 40
    .local v1, "intValue":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 41
    add-int v2, p1, v0

    aget-byte v2, p0, v2

    and-int/lit16 v2, v2, 0xff

    rsub-int/lit8 v3, v0, 0x3

    mul-int/lit8 v3, v3, 0x8

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_0
    return v1
.end method

.method public static final byteToLong([B)J
    .locals 2
    .param p0, "src"    # [B

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;->byteToLong([BI)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final byteToLong([BI)J
    .locals 6
    .param p0, "src"    # [B
    .param p1, "offset"    # I

    .prologue
    .line 21
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;->byteToInt([BI)I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    add-int/lit8 v2, p1, 0x4

    invoke-static {p0, v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;->byteToInt([BI)I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public static final longToByte(J)[B
    .locals 2
    .param p0, "longVar"    # J

    .prologue
    .line 16
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 17
    .local v0, "byteArray":[B
    return-object v0
.end method
