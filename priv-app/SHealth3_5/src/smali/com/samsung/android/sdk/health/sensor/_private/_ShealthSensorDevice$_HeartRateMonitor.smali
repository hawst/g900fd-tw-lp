.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_HeartRateMonitor"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1315
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1269
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;-><init>()V

    .line 1270
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1273
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;-><init>()V

    .line 1274
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->readFromParcel(Landroid/os/Parcel;)V

    .line 1275
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1330
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    .line 1331
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    .line 1332
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    .line 1333
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->interval:J

    .line 1334
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNR:F

    .line 1335
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNRUnit:I

    .line 1336
    const-class v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 1337
    .local v1, "temp":[Landroid/os/Parcelable;
    if-eqz v1, :cond_1

    .line 1338
    array-length v3, v1

    new-array v2, v3, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    .line 1339
    .local v2, "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 1340
    aget-object v3, v1, v0

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    aput-object v3, v2, v0

    .line 1339
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1341
    :cond_0
    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    .line 1346
    .end local v0    # "i":I
    .end local v2    # "tmp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagging:Ljava/lang/String;

    .line 1347
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagIndex:I

    .line 1348
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->devicePkId:Ljava/lang/String;

    .line 1349
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->crud:I

    .line 1350
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagIcon:I

    .line 1351
    return-void

    .line 1343
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1280
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 1356
    const/4 v0, 0x4

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1361
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HeartRateMonitor] time ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", heartRate ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eventTime ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", interval ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->interval:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SNR ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNR:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", SNRUnit ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNRUnit:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tagIndex ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tagging ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagging:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", crud ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->crud:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tagIcon ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagIcon:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", devicePkId ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->devicePkId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1286
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->time:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 1287
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRate:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1288
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->eventTime:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 1289
    iget-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->interval:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 1290
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNR:F

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeFloat(F)V

    .line 1291
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->SNRUnit:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1292
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    if-eqz v2, :cond_1

    .line 1293
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    array-length v2, v2

    new-array v1, v2, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    .line 1294
    .local v1, "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 1296
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;

    invoke-direct {v2}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;-><init>()V

    aput-object v2, v1, v0

    .line 1297
    aget-object v2, v1, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v3, v3, v0

    iget-wide v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->samplingTime:J

    iput-wide v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;->samplingTime:J

    .line 1298
    aget-object v2, v1, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->heartRate:I

    iput v3, v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;->heartRate:I

    .line 1294
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1300
    :cond_0
    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 1302
    .end local v0    # "i":I
    .end local v1    # "temp":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 1305
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagging:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1306
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagIndex:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1307
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->devicePkId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1308
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->crud:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1309
    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;->tagIcon:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1310
    return-void
.end method
