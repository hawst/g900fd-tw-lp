.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->onBulkDataReceived([Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;[Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BulkDataReceivedCallBack"
.end annotation


# instance fields
.field dataArray:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

.field dataType:I

.field extra:[Landroid/os/Bundle;

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
    .locals 0
    .param p2, "dataType"    # I
    .param p3, "dataArray"    # [Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
    .param p4, "extra"    # [Landroid/os/Bundle;

    .prologue
    .line 695
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 696
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->dataType:I

    .line 697
    iput-object p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->dataArray:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    .line 698
    iput-object p4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->extra:[Landroid/os/Bundle;

    .line 699
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 702
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->dataType:I

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->dataArray:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1BulkDataReceivedCallBack;->extra:[Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V

    .line 704
    return-void
.end method
