.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->onDataStarted(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataStartedCallBack"
.end annotation


# instance fields
.field dataType:I

.field error:I

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;II)V
    .locals 0
    .param p2, "dataType"    # I
    .param p3, "error"    # I

    .prologue
    .line 593
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 594
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->dataType:I

    .line 595
    iput p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->error:I

    .line 596
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 600
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->error:I

    if-nez v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 605
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->dataType:I

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->error:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStarted(II)V

    .line 606
    return-void

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener$1DataStartedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STARTED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    goto :goto_0
.end method
