.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$WaterTareItemColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WaterTareItemColumns"
.end annotation


# static fields
.field public static final POSITION:Ljava/lang/String; = "position"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final VOLUME:Ljava/lang/String; = "volume"

.field public static final WATER_INTAKE_LOCAL_ID:Ljava/lang/String; = "water_intake_local__id"
