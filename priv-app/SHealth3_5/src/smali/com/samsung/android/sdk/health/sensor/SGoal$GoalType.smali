.class public interface abstract Lcom/samsung/android/sdk/health/sensor/SGoal$GoalType;
.super Ljava/lang/Object;
.source "SGoal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/SGoal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GoalType"
.end annotation


# static fields
.field public static final EXERCISE_GOAL:I = 0x9c42

.field public static final FOOD_GOAL:I = 0x9c45

.field public static final MEAL_GOAL:I = 0x9c43

.field public static final NOT_DEFINED:I = -0x1

.field public static final WALKING_GOAL:I = 0x9c41

.field public static final WATER_GOAL:I = 0x9c46

.field public static final WEIGHT_GOAL:I = 0x9c44
