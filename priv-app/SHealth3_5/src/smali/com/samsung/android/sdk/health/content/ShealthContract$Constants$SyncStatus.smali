.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$SyncStatus;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SyncStatus"
.end annotation


# static fields
.field public static final ADDED:I = 0x29812

.field public static final DELETED:I = 0x29814

.field public static final IN_SYNC:I = 0x29811
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final UPDATED:I = 0x29813
