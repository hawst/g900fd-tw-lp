.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ExerciseColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CADENCE:Ljava/lang/String; = "cadence"

.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final DURATION_MILLISECOND:Ljava/lang/String; = "duration_millisecond"

.field public static final DURATION_MIN:Ljava/lang/String; = "duration_min"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final EXERCISE_INFO_ID:Ljava/lang/String; = "exercise_info__id"

.field public static final EXERCISE_TYPE:Ljava/lang/String; = "exercise_type"

.field public static final FAT_BURN_TIME:Ljava/lang/String; = "fat_burn_time"

.field public static final HEART_RATE:Ljava/lang/String; = "heart_rate"

.field public static final INPUT_SOURCE_TYPE:Ljava/lang/String; = "input_source_type"

.field public static final LEVEL:Ljava/lang/String; = "level"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final TOTAL_CALORIE:Ljava/lang/String; = "total_calorie"
