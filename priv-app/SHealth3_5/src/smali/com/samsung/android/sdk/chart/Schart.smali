.class public Lcom/samsung/android/sdk/chart/Schart;
.super Ljava/lang/Object;
.source "Schart.java"

# interfaces
.implements Lcom/samsung/android/sdk/SsdkInterface;


# static fields
.field public static final SCHART_NATIVE_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.schart"

.field private static final VERSION:Ljava/lang/String; = "1.0.0"

.field private static final VERSION_LEVEL:I = 0x1

.field private static mIsInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/chart/Schart;->mIsInitialized:Z

    .line 17
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getVersionCode()I
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "1.0.0"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    .prologue
    .line 35
    const-string v0, "JNIChartLib"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/Schart;->loadLibrary(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 39
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/chart/Schart;->mIsInitialized:Z

    .line 40
    return-void
.end method

.method public isFeatureEnabled(I)Z
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method loadLibrary(Ljava/lang/String;)Z
    .locals 5
    .param p1, "libraryName"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    .prologue
    .line 51
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "/data/data/com.samsung.android.sdk.schart/lib/lib"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".so"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "latestLib":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 53
    .local v2, "libFilePath":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 55
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    const/4 v3, 0x1

    .line 62
    :goto_0
    return v3

    .line 57
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/Throwable;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 62
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
