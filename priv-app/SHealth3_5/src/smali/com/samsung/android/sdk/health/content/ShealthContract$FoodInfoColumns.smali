.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfoColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FoodInfoColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final FAVORITE:Ljava/lang/String; = "favorite"

.field public static final KCAL:Ljava/lang/String; = "kcal"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final SERVER_FOOD_ID:Ljava/lang/String; = "server_food_id"

.field public static final SERVER_LOCALE:Ljava/lang/String; = "server_locale"

.field public static final SERVER_ROOT_CATEGORY:Ljava/lang/String; = "server_root_category"

.field public static final SERVER_ROOT_CATEGORY_ID:Ljava/lang/String; = "server_root_category_id"

.field public static final SERVER_SOURCE_TYPE:Ljava/lang/String; = "server_source_type"

.field public static final SERVER_SUB_CATEGORY:Ljava/lang/String; = "server_sub_category"

.field public static final SERVER_SUB_CATEGORY_ID:Ljava/lang/String; = "server_sub_category_id"

.field public static final SORTING1:Ljava/lang/String; = "sorting1"

.field public static final SORTING2:Ljava/lang/String; = "sorting2"
