.class public interface abstract Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableConstants$Gender;
.super Ljava/lang/Object;
.source "JWearableConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Gender"
.end annotation


# static fields
.field public static final FEMALE:I = 0x2e636

.field public static final MALE:I = 0x2e635

.field public static final NOT_DEFINED:I = -0x1
