.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WrapperPrivilegeSensorDataListener"
.end annotation


# instance fields
.field listener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;)V
    .locals 0
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->listener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    .line 67
    return-void
.end method


# virtual methods
.method public onReceived(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "dataType"    # I
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 109
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$302(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 110
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDeviceC onReceived() is called dataTpe "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    packed-switch p1, :pswitch_data_0

    .line 141
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 145
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toDataType(I)I

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$300(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, p0, v1, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;ILcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;Landroid/os/Bundle;)V

    .line 146
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 150
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataReceivedCallBack;
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    return-void

    .line 114
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$HeartRateMonitorC;->toHeartRateMonitor(Landroid/os/Bundle;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;

    move-result-object v2

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$302(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 117
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$ElectroCardiogramC;->toElectroCardiogram(Landroid/os/Bundle;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;

    move-result-object v2

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->healthData:Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$302(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onReceived(I[Landroid/os/Bundle;)V
    .locals 5
    .param p1, "dataType"    # I
    .param p2, "data"    # [Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 156
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$502(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    .line 157
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDeviceC onReceived() is called dataTpe "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    packed-switch p1, :pswitch_data_0

    .line 193
    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 195
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 197
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$2DataReceivedCallBack;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toDataType(I)I

    move-result v1

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$500(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, p0, v1, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$2DataReceivedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;I[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;[Landroid/os/Bundle;)V

    .line 198
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$2DataReceivedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 202
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$2DataReceivedCallBack;
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    return-void

    .line 161
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$PulseOximeterC;->toBulkPulseOximeter([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;

    move-result-object v2

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$502(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 164
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$BloodGlucoseC;->toBulkBloodGlucose([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;

    move-result-object v2

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$502(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 167
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$ElectroCardiogramC;->toBulkElectroCardiogram([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;

    move-result-object v2

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$502(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 170
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$BodyTemperatureC;->toBulkBodyTemperature([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;

    move-result-object v2

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->bulkHealthData:[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$502(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;

    goto :goto_0

    .line 202
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 158
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onStarted(II)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 71
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDeviceC onStarted() is called dataTpe "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 99
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toDataType(I)I

    move-result v1

    invoke-direct {v0, p0, v1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;II)V

    .line 100
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 103
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStartedCallBack;
    :cond_0
    monitor-exit v2

    .line 104
    return-void

    .line 103
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onStopped(II)V
    .locals 4
    .param p1, "dataType"    # I
    .param p2, "error"    # I

    .prologue
    .line 209
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthSensorDeviceC onStopped() is called dataTpe "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 236
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 238
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;

    invoke-static {p1}, Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;->toDataType(I)I

    move-result v1

    invoke-direct {v0, p0, v1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;II)V

    .line 239
    .local v0, "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 242
    .end local v0    # "callBackRunnable":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;
    :cond_0
    monitor-exit v2

    .line 244
    return-void

    .line 242
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
