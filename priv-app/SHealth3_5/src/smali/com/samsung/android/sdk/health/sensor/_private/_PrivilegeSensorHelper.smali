.class public Lcom/samsung/android/sdk/health/sensor/_private/_PrivilegeSensorHelper;
.super Ljava/lang/Object;
.source "_PrivilegeSensorHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toConnectivityType(I)I
    .locals 1
    .param p0, "privConnectivityType"    # I

    .prologue
    .line 76
    packed-switch p0, :pswitch_data_0

    .line 81
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 79
    :pswitch_0
    const/4 v0, 0x4

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static toDataType(I)I
    .locals 1
    .param p0, "privDataType"    # I

    .prologue
    .line 108
    packed-switch p0, :pswitch_data_0

    .line 121
    :pswitch_0
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 111
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 113
    :pswitch_2
    const/16 v0, 0x11

    goto :goto_0

    .line 115
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 117
    :pswitch_4
    const/16 v0, 0x10

    goto :goto_0

    .line 119
    :pswitch_5
    const/4 v0, 0x7

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public static toDeviceType(I)I
    .locals 1
    .param p0, "privDeviceType"    # I

    .prologue
    .line 88
    sparse-switch p0, :sswitch_data_0

    .line 101
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 91
    :sswitch_0
    const/16 v0, 0x2718

    goto :goto_0

    .line 93
    :sswitch_1
    const/16 v0, 0x272a

    goto :goto_0

    .line 95
    :sswitch_2
    const/16 v0, 0x2714

    goto :goto_0

    .line 97
    :sswitch_3
    const/16 v0, 0x2729

    goto :goto_0

    .line 99
    :sswitch_4
    const/16 v0, 0x2716

    goto :goto_0

    .line 88
    :sswitch_data_0
    .sparse-switch
        0x2714 -> :sswitch_2
        0x2716 -> :sswitch_4
        0x2718 -> :sswitch_0
        0x2729 -> :sswitch_3
        0x272a -> :sswitch_1
    .end sparse-switch
.end method

.method public static toPrivilegeConnectivityType(I)I
    .locals 1
    .param p0, "connectivityType"    # I

    .prologue
    .line 16
    sparse-switch p0, :sswitch_data_0

    .line 23
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 19
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 21
    :sswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 16
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x4 -> :sswitch_0
    .end sparse-switch
.end method

.method public static toPrivilegeDataType(I)I
    .locals 1
    .param p0, "dataType"    # I

    .prologue
    .line 52
    sparse-switch p0, :sswitch_data_0

    .line 67
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 55
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 59
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 61
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 63
    :sswitch_4
    const/4 v0, 0x7

    goto :goto_0

    .line 65
    :sswitch_5
    const/4 v0, 0x0

    goto :goto_0

    .line 52
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_5
        0x3 -> :sswitch_2
        0x4 -> :sswitch_0
        0x7 -> :sswitch_4
        0x10 -> :sswitch_3
        0x11 -> :sswitch_1
    .end sparse-switch
.end method

.method public static toPrivilegeDeviceType(I)I
    .locals 1
    .param p0, "deviceType"    # I

    .prologue
    .line 30
    sparse-switch p0, :sswitch_data_0

    .line 45
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 33
    :sswitch_0
    const/16 v0, 0x2718

    goto :goto_0

    .line 35
    :sswitch_1
    const/16 v0, 0x272a

    goto :goto_0

    .line 37
    :sswitch_2
    const/16 v0, 0x2714

    goto :goto_0

    .line 39
    :sswitch_3
    const/16 v0, 0x2729

    goto :goto_0

    .line 41
    :sswitch_4
    const/16 v0, 0x2716

    goto :goto_0

    .line 43
    :sswitch_5
    const/4 v0, 0x0

    goto :goto_0

    .line 30
    :sswitch_data_0
    .sparse-switch
        0x2711 -> :sswitch_5
        0x2714 -> :sswitch_2
        0x2716 -> :sswitch_4
        0x2718 -> :sswitch_0
        0x2729 -> :sswitch_3
        0x272a -> :sswitch_1
    .end sparse-switch
.end method

.method public static toSensorDevice(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;Landroid/content/Context;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .locals 1
    .param p0, "device"    # Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;-><init>(Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorDevice;Landroid/content/Context;)V

    return-object v0
.end method
