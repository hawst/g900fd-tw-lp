.class Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
.super Ljava/lang/Thread;
.source "WearableCommClientSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClientSocketThread"
.end annotation


# instance fields
.field private volatile mClientSocketLooper:Landroid/os/Looper;

.field private mSocketHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)V
    .locals 1

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->mClientSocketLooper:Landroid/os/Looper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;
    .param p2, "x1"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$1;

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;-><init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)V

    return-void
.end method


# virtual methods
.method public destroyLooper()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->mClientSocketLooper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->mClientSocketLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->mClientSocketLooper:Landroid/os/Looper;

    .line 354
    :cond_0
    return-void
.end method

.method public getClientSocketMsgHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->mSocketHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 126
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 128
    new-instance v0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;-><init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->mSocketHandler:Landroid/os/Handler;

    .line 338
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->mClientSocketLooper:Landroid/os/Looper;

    .line 339
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 340
    return-void
.end method
