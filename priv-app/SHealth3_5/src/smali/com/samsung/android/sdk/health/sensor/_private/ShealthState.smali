.class public Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;
.super Ljava/lang/Object;
.source "ShealthState.java"


# static fields
.field private static sInstance:Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;


# instance fields
.field private mInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->sInstance:Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->sInstance:Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    return-object v0
.end method


# virtual methods
.method public isInitialized()Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->mInitialized:Z

    return v0
.end method

.method public setInitialized(Z)V
    .locals 0
    .param p1, "initialized"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->mInitialized:Z

    .line 30
    return-void
.end method
