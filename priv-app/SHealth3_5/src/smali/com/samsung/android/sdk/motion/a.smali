.class final Lcom/samsung/android/sdk/motion/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# instance fields
.field private synthetic a:Lcom/samsung/android/sdk/motion/SmotionActivity;

.field private final synthetic b:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/motion/SmotionActivity;Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    iput-object p2, p0, Lcom/samsung/android/sdk/motion/a;->b:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 12

    const/4 v7, 0x1

    const/4 v1, 0x0

    iget-object v0, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v2

    const/16 v3, 0x19

    if-ne v2, v3, :cond_2

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityTrackerContext()Landroid/hardware/scontext/SContextActivityTracker;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityTracker;->getStatus()I

    move-result v2

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityTracker;->getAccuracy()I

    move-result v3

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityTracker;->getTimeStamp()J

    move-result-wide v4

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    iget-object v6, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    invoke-static {v6, v4, v5, v2, v3}, Lcom/samsung/android/sdk/motion/SmotionActivity;->a(Lcom/samsung/android/sdk/motion/SmotionActivity;JII)Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionActivity;->a(Lcom/samsung/android/sdk/motion/SmotionActivity;Lcom/samsung/android/sdk/motion/SmotionActivity$Info;)V

    new-array v0, v7, [Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    iget-object v2, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    invoke-static {v2}, Lcom/samsung/android/sdk/motion/SmotionActivity;->a(Lcom/samsung/android/sdk/motion/SmotionActivity;)Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/a;->b:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    invoke-interface {v1, v7, v0}, Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;->onChanged(I[Lcom/samsung/android/sdk/motion/SmotionActivity$Info;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    invoke-static {v0}, Lcom/samsung/android/sdk/motion/SmotionActivity;->b(Lcom/samsung/android/sdk/motion/SmotionActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    invoke-static {v0, v7}, Lcom/samsung/android/sdk/motion/SmotionActivity;->b(Lcom/samsung/android/sdk/motion/SmotionActivity;Z)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    invoke-static {v0, v7}, Lcom/samsung/android/sdk/motion/SmotionActivity;->c(Lcom/samsung/android/sdk/motion/SmotionActivity;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v0

    const/16 v2, 0x1a

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityBatchContext()Landroid/hardware/scontext/SContextActivityBatch;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityBatch;->getStatus()[I

    move-result-object v2

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityBatch;->getAccuracy()[I

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityBatch;->getTimeStamp()[J

    move-result-object v4

    array-length v5, v2

    move v0, v1

    :goto_1
    if-lt v0, v5, :cond_4

    move v0, v1

    :cond_3
    array-length v5, v2

    sub-int/2addr v5, v0

    new-array v6, v5, [Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    :goto_2
    if-lt v1, v5, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/a;->b:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1, v6}, Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;->onChanged(I[Lcom/samsung/android/sdk/motion/SmotionActivity$Info;)V

    goto :goto_0

    :cond_4
    aget-wide v6, v4, v0

    iget-object v8, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    invoke-static {v8}, Lcom/samsung/android/sdk/motion/SmotionActivity;->c(Lcom/samsung/android/sdk/motion/SmotionActivity;)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gtz v6, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/samsung/android/sdk/motion/a;->a:Lcom/samsung/android/sdk/motion/SmotionActivity;

    add-int v8, v1, v0

    aget-wide v8, v4, v8

    add-int v10, v1, v0

    aget v10, v2, v10

    add-int v11, v1, v0

    aget v11, v3, v11

    invoke-static {v7, v8, v9, v10, v11}, Lcom/samsung/android/sdk/motion/SmotionActivity;->a(Lcom/samsung/android/sdk/motion/SmotionActivity;JII)Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    move-result-object v7

    aput-object v7, v6, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
