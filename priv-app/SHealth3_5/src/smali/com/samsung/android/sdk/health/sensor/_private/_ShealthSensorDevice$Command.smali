.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
.super Ljava/lang/Object;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Command"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private commandId:Ljava/lang/String;

.field private params:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 538
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 557
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->readFromParcel(Landroid/os/Parcel;)V

    .line 558
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "commandId"    # Ljava/lang/String;
    .param p2, "params"    # Landroid/os/Bundle;

    .prologue
    .line 502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 504
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->commandId:Ljava/lang/String;

    .line 505
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->params:Landroid/os/Bundle;

    .line 506
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 563
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->commandId:Ljava/lang/String;

    .line 564
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->params:Landroid/os/Bundle;

    .line 565
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 518
    const/4 v0, 0x0

    return v0
.end method

.method public getCommandId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->commandId:Ljava/lang/String;

    return-object v0
.end method

.method public getParameters()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->params:Landroid/os/Bundle;

    return-object v0
.end method

.method public setCommandId(Ljava/lang/String;)V
    .locals 0
    .param p1, "commandId"    # Ljava/lang/String;

    .prologue
    .line 605
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->commandId:Ljava/lang/String;

    .line 606
    return-void
.end method

.method public setParameters(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "params"    # Landroid/os/Bundle;

    .prologue
    .line 623
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->params:Landroid/os/Bundle;

    .line 624
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 531
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->commandId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;->params:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 533
    return-void
.end method
