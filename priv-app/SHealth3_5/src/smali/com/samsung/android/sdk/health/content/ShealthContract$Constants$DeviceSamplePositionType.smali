.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceSamplePositionType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceSamplePositionType"
.end annotation


# static fields
.field public static final ANKLE:I = 0x38273

.field public static final ARM:I = 0x38274

.field public static final NOT_DEFINED:I = 0x0
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final UNKNOWN:I = 0x38271

.field public static final WRIST:I = 0x38272
