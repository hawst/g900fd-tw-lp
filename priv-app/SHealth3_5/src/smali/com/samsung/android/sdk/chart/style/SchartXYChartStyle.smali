.class public Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
.super Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;
.source "SchartXYChartStyle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;
    }
.end annotation


# static fields
.field public static final LABLE_TITLE_ALIGN_BOTTOM:I = 0x1

.field public static final LABLE_TITLE_ALIGN_CENTER:I = 0x2

.field public static final LABLE_TITLE_ALIGN_TOP:I = 0x0

.field public static final MAJOR_LINE_BASE:I = 0x0

.field public static final MINOR_LINE_BASE:I = 0x1

.field public static final TITLE_ALIGN_BOTTOM:I = 0x3

.field public static final TITLE_ALIGN_CENTER:I = 0x4

.field public static final TITLE_ALIGN_LEFT:I = 0x0

.field public static final TITLE_ALIGN_RIGHT:I = 0x1

.field public static final TITLE_ALIGN_TOP:I = 0x2

.field public static final WEEK_FRIDAY:I = 0x5

.field public static final WEEK_MONDAY:I = 0x1

.field public static final WEEK_SATURDAY:I = 0x6

.field public static final WEEK_SUNDAY:I = 0x0

.field public static final WEEK_THURSDAY:I = 0x4

.field public static final WEEK_TUESDAY:I = 0x2

.field public static final WEEK_WEDNESDAY:I = 0x3

.field public static final XAXIS_DIRECTION_BOTTOM:I = 0x3

.field public static final XAXIS_DIRECTION_TOP:I = 0x2

.field public static final XAXIS_LABEL_ALIGN_DOWN:I = 0x3

.field public static final XAXIS_LABEL_ALIGN_UP:I = 0x2

.field public static final YAXIS_DIRECTION_LEFT:I = 0x0

.field public static final YAXIS_DIRECTION_RIGHT:I = 0x1

.field public static final YAXIS_LABEL_ALIGN_LEFT:I = 0x0

.field public static final YAXIS_LABEL_ALIGN_RIGHT:I = 0x1

.field public static final YAXIS_ROUNDING_ROUND:I = 0x0

.field public static final YAXIS_ROUNDING_ROUNDDOWN:I = 0x2

.field public static final YAXIS_ROUNDING_ROUNDUP:I = 0x1


# instance fields
.field protected mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

.field protected mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

.field protected mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

.field protected mNumOfXAxis:I

.field protected mNumOfYAxis:I

.field protected mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

.field protected mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;


# direct methods
.method public constructor <init>(II)V
    .locals 5
    .param p1, "numOfXAxis"    # I
    .param p2, "numOfYAxis"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 109
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;-><init>()V

    .line 95
    iput v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfXAxis:I

    .line 96
    iput v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfYAxis:I

    .line 111
    new-array v1, p1, [Lcom/sec/dmc/sic/android/property/AxisProperty;

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 112
    new-array v1, p2, [Lcom/sec/dmc/sic/android/property/AxisProperty;

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    .line 113
    new-instance v1, Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    .line 114
    new-instance v1, Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    .line 115
    new-instance v1, Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-direct {v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    .line 117
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, p1, :cond_0

    .line 135
    const/4 v0, 0x0

    :goto_1
    if-lt v0, p2, :cond_1

    .line 159
    iput p1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfXAxis:I

    .line 160
    iput p2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfYAxis:I

    .line 161
    return-void

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    new-instance v2, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;-><init>()V

    aput-object v2, v1, v0

    .line 119
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v1, v1, v0

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeDirection(I)V

    .line 120
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeXY(I)V

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    new-instance v2, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-direct {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;-><init>()V

    aput-object v2, v1, v0

    .line 137
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeDirection(I)V

    .line 138
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeXY(I)V

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public getGridColor()I
    .locals 4

    .prologue
    .line 2736
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 2737
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 2738
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 2739
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 2736
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getGridLineProperty()Lcom/sec/dmc/sic/android/property/GridLineProperty;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    return-object v0
.end method

.method public getGridUseManualMarking()Z
    .locals 1

    .prologue
    .line 2750
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getUseManualMarking()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2756
    :goto_0
    return v0

    .line 2753
    :catch_0
    move-exception v0

    .line 2756
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGridVisible()Z
    .locals 1

    .prologue
    .line 2700
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->isVisible()Z

    move-result v0

    return v0
.end method

.method public getGridWidth()F
    .locals 1

    .prologue
    .line 2727
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public getGridXLineVisible()Z
    .locals 1

    .prologue
    .line 2709
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getXLineVisible()Z

    move-result v0

    return v0
.end method

.method public getGridYLineVisible()Z
    .locals 1

    .prologue
    .line 2718
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->getYLineVisible()Z

    move-result v0

    return v0
.end method

.method public getHandlerAnimation()Z
    .locals 1

    .prologue
    .line 3027
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->isAnimationUse()Z

    move-result v0

    return v0
.end method

.method public getHandlerAnimationTime()J
    .locals 2

    .prologue
    .line 3037
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getAnimationTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public getHandlerEnable()Z
    .locals 1

    .prologue
    .line 2823
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->isEnable()Z

    move-result v0

    return v0
.end method

.method public getHandlerItemColor()I
    .locals 4

    .prologue
    .line 2933
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 2934
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 2935
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 2936
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 2933
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getHandlerItemDateFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2947
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemDateFormat()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHandlerItemHeight()F
    .locals 1

    .prologue
    .line 2896
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemHeight()F

    move-result v0

    return v0
.end method

.method public getHandlerItemOffset()F
    .locals 1

    .prologue
    .line 2905
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemOffset()F

    move-result v0

    return v0
.end method

.method public getHandlerItemShapeType()I
    .locals 1

    .prologue
    .line 2878
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemShape()I

    move-result v0

    return v0
.end method

.method public getHandlerItemStrokeWidth()F
    .locals 1

    .prologue
    .line 2957
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemStrokeWidth()F

    move-result v0

    return v0
.end method

.method public getHandlerItemTextOffset()F
    .locals 1

    .prologue
    .line 2914
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemTextOffset()F

    move-result v0

    return v0
.end method

.method public getHandlerItemTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 1697
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getHandlerItemTextVisible()Z
    .locals 1

    .prologue
    .line 2924
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->isItemTextVisible()Z

    move-result v0

    return v0
.end method

.method public getHandlerItemVisible()Z
    .locals 1

    .prologue
    .line 2841
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->isItemVisible()Z

    move-result v0

    return v0
.end method

.method public getHandlerItemWidth()F
    .locals 1

    .prologue
    .line 2887
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getItemWidth()F

    move-result v0

    return v0
.end method

.method public getHandlerLineColor()I
    .locals 4

    .prologue
    .line 2995
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getLineAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 2996
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getLineColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 2997
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getLineColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 2998
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getLineColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 2995
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getHandlerLineType()I
    .locals 1

    .prologue
    .line 2977
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getLineType()I

    move-result v0

    return v0
.end method

.method public getHandlerLineVisible()Z
    .locals 1

    .prologue
    .line 2850
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->isLineVisible()Z

    move-result v0

    return v0
.end method

.method public getHandlerLineWidth()F
    .locals 1

    .prologue
    .line 2986
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getLineWidth()F

    move-result v0

    return v0
.end method

.method public getHandlerProperty()Lcom/sec/dmc/sic/android/property/HandlerProperty;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    return-object v0
.end method

.method public getHandlerTimeOutDelay()J
    .locals 2

    .prologue
    .line 3017
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getTimeOutDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public getHandlerTooltipColor(I)I
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 3049
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 3050
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 3051
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 3052
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 3049
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getHandlerTooltipEnable()Z
    .locals 1

    .prologue
    .line 2869
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->isInfoEnable()Z

    move-result v0

    return v0
.end method

.method public getHandlerTooltipShape()I
    .locals 1

    .prologue
    .line 3086
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getShape()I

    move-result v0

    return v0
.end method

.method public getHandlerTooltipStrokeColor()I
    .locals 4

    .prologue
    .line 3062
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoStrokeAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 3063
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoStrokeColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 3064
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoStrokeColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 3065
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoStrokeColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 3062
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getHandlerTooltipStrokeWidth()F
    .locals 1

    .prologue
    .line 3075
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoStrokeWidth()F

    move-result v0

    return v0
.end method

.method public getHandlerTooltipTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 1840
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->getInfoTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getInfoProperty()Lcom/sec/dmc/sic/android/property/InfoPointProperty;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    return-object v0
.end method

.method public getMaxInclueGoalExceedDataCount()I
    .locals 1

    .prologue
    .line 3207
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getMaxInclueGoalExceedDataCount()I

    move-result v0

    return v0
.end method

.method public getNumberOfXAxis()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfXAxis:I

    return v0
.end method

.method public getNumberOfYAxis()I
    .locals 1

    .prologue
    .line 234
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfYAxis:I

    return v0
.end method

.method public getPopupColor()I
    .locals 4

    .prologue
    .line 3181
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 3182
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 3183
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 3184
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 3181
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getPopupEnable()Z
    .locals 1

    .prologue
    .line 3095
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->isEnable()Z

    move-result v0

    return v0
.end method

.method public getPopupShapeType()I
    .locals 1

    .prologue
    .line 3123
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getShape()I

    move-result v0

    return v0
.end method

.method public getPopupSizeHeight()F
    .locals 1

    .prologue
    .line 3158
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getHeight()F

    move-result v0

    return v0
.end method

.method public getPopupSizeRadius()F
    .locals 1

    .prologue
    .line 3171
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getRadius()F

    move-result v0

    return v0
.end method

.method public getPopupSizeWidth()F
    .locals 1

    .prologue
    .line 3144
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getWidth()F

    move-result v0

    return v0
.end method

.method public getPopupStrokeColor()I
    .locals 4

    .prologue
    .line 3194
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getStrokeAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 3195
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getStrokeColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 3196
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getStrokeColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 3197
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getStrokeColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 3194
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getPopupStrokeWidth()F
    .locals 1

    .prologue
    .line 3133
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method public getPopupTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 1973
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->getTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getPopupVisible()Z
    .locals 1

    .prologue
    .line 3106
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->isVisible()Z

    move-result v0

    return v0
.end method

.method public getSeparatorDateFormat()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2779
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSeparatorDateFormat()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSeparatorLineThickness()F
    .locals 2

    .prologue
    .line 2812
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSeparatorLineThickness()F

    move-result v0

    return v0
.end method

.method public getSeparatorTextSpacingLine()I
    .locals 2

    .prologue
    .line 2801
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSeparatorSpacingLine()I

    move-result v0

    return v0
.end method

.method public getSeparatorTextSpacingTop()I
    .locals 2

    .prologue
    .line 2790
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSeparatorSpacingTop()I

    move-result v0

    return v0
.end method

.method public getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 2

    .prologue
    .line 2767
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSeparatorTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getXAxisColor(I)I
    .locals 4
    .param p1, "idx"    # I

    .prologue
    .line 2245
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 2246
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 2247
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 2248
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 2245
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2253
    :goto_0
    return v0

    .line 2250
    :catch_0
    move-exception v0

    .line 2253
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisDateFormat(I)Ljava/lang/String;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2280
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getDateFormat()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2285
    :goto_0
    return-object v0

    .line 2282
    :catch_0
    move-exception v0

    .line 2285
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisDirection(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2398
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTypeDirection()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2403
    :goto_0
    return v0

    .line 2400
    :catch_0
    move-exception v0

    .line 2403
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisFirstDayOfWeekend(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 422
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFirstDayOfWeekend()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 428
    :goto_0
    return v0

    .line 425
    :catch_0
    move-exception v0

    .line 428
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getXAxisLabelAlign(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2354
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getXAxisLabelAlign()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2358
    :goto_0
    return v0

    .line 2355
    :catch_0
    move-exception v0

    .line 2358
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisLabelTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2368
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getLabelTitleText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2372
    :goto_0
    return-object v0

    .line 2369
    :catch_0
    move-exception v0

    .line 2372
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisLabelTitleAlign(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2382
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getLabelTitleAlign()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2387
    :goto_0
    return v0

    .line 2384
    :catch_0
    move-exception v0

    .line 2387
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisLabelTitleTextStyle(I)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 710
    const/4 v1, 0x0

    .line 712
    .local v1, "ret":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getLabelTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 717
    :goto_0
    return-object v1

    .line 714
    :catch_0
    move-exception v0

    .line 715
    .local v0, "indexOutofException":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getXAxisLineVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2197
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getVisibleAxisLine()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2203
    :goto_0
    return v0

    .line 2200
    :catch_0
    move-exception v0

    .line 2203
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisMarkingVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2230
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getVisibleMarking()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2235
    :goto_0
    return v0

    .line 2232
    :catch_0
    move-exception v0

    .line 2235
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;
    .locals 2
    .param p1, "idx"    # I

    .prologue
    const/4 v0, 0x0

    .line 174
    if-gez p1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-object v0

    .line 176
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfXAxis:I

    add-int/lit8 v1, v1, -0x1

    if-gt p1, v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getXAxisSecondDayOfWeekend(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 439
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFirstDayOfWeekend()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 445
    :goto_0
    return v0

    .line 442
    :catch_0
    move-exception v0

    .line 445
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getXAxisTextSpace(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2669
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTextSpace()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2673
    :goto_0
    return v0

    .line 2670
    :catch_0
    move-exception v0

    .line 2673
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisTextStyle(I)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 523
    const/4 v1, 0x0

    .line 525
    .local v1, "ret":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getMainTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 530
    :goto_0
    return-object v1

    .line 527
    :catch_0
    move-exception v0

    .line 528
    .local v0, "indexOutofException":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getXAxisTextStyleFirstDayOfWeekend(I)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 455
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTextStyleForWeekend1()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 461
    :goto_0
    return-object v0

    .line 458
    :catch_0
    move-exception v0

    .line 461
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisTextStyleSecondDayOfWeekend(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 471
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFirstDayOfWeekend()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 477
    :goto_0
    return v0

    .line 474
    :catch_0
    move-exception v0

    .line 477
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getXAxisTextVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2214
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getVisibleAxisText()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2219
    :goto_0
    return v0

    .line 2216
    :catch_0
    move-exception v0

    .line 2219
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2296
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTitleText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2300
    :goto_0
    return-object v0

    .line 2297
    :catch_0
    move-exception v0

    .line 2300
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisTitleAlign(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2312
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getXAxisTitleAlign()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2314
    :goto_0
    return v0

    .line 2313
    :catch_0
    move-exception v0

    .line 2314
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisTitleSpacingAlign(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2339
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSpacingAlign()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2343
    :goto_0
    return v0

    .line 2340
    :catch_0
    move-exception v0

    .line 2343
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisTitleSpacingAxis(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2324
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSpacingAxisLine()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2328
    :goto_0
    return v0

    .line 2325
    :catch_0
    move-exception v0

    .line 2328
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisTitleTextStyle(I)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 625
    const/4 v1, 0x0

    .line 627
    .local v1, "ret":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 631
    :goto_0
    return-object v1

    .line 628
    :catch_0
    move-exception v0

    .line 629
    .local v0, "indexOutofException":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getXAxisUseTextStyleForWeekend(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 404
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getUseColorForWeekend()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 410
    :goto_0
    return v0

    .line 407
    :catch_0
    move-exception v0

    .line 410
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2181
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->isVisible()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2187
    :goto_0
    return v0

    .line 2184
    :catch_0
    move-exception v0

    .line 2187
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getXAxisWidth(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2263
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getStrokeWidth()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2268
    :goto_0
    return v0

    .line 2265
    :catch_0
    move-exception v0

    .line 2268
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisColor(I)I
    .locals 4
    .param p1, "idx"    # I

    .prologue
    .line 2529
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 2530
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 2531
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 2532
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 2529
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2538
    :goto_0
    return v0

    .line 2535
    :catch_0
    move-exception v0

    .line 2538
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisCustomLabel(I)[Ljava/lang/String;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2513
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getCustomYLabelStrs()[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2518
    :goto_0
    return-object v0

    .line 2515
    :catch_0
    move-exception v0

    .line 2518
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisDirection(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2479
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTypeDirection()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2484
    :goto_0
    return v0

    .line 2481
    :catch_0
    move-exception v0

    .line 2484
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisLabelAlign(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2624
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getYAxisLabelAlign()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2628
    :goto_0
    return v0

    .line 2625
    :catch_0
    move-exception v0

    .line 2628
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisLabelTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2638
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getLabelTitleText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2642
    :goto_0
    return-object v0

    .line 2639
    :catch_0
    move-exception v0

    .line 2642
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisLabelTitleAlign(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2653
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getLabelTitleAlign()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2658
    :goto_0
    return v0

    .line 2655
    :catch_0
    move-exception v0

    .line 2658
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisLabelTitleTextStyle(I)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 1161
    const/4 v1, 0x0

    .line 1163
    .local v1, "ret":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getLabelTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1168
    :goto_0
    return-object v1

    .line 1165
    :catch_0
    move-exception v0

    .line 1166
    .local v0, "indexOutofException":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getYAxisLineVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2430
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getVisibleAxisLine()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2435
    :goto_0
    return v0

    .line 2432
    :catch_0
    move-exception v0

    .line 2435
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisMarkingVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2463
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getVisibleMarking()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2468
    :goto_0
    return v0

    .line 2465
    :catch_0
    move-exception v0

    .line 2468
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;
    .locals 2
    .param p1, "idx"    # I

    .prologue
    const/4 v0, 0x0

    .line 187
    if-gez p1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 189
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mNumOfYAxis:I

    add-int/lit8 v1, v1, -0x1

    if-gt p1, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public getYAxisReversedLabel(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 1374
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getYAxisReversed()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1378
    :goto_0
    return v0

    .line 1375
    :catch_0
    move-exception v0

    .line 1378
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisTextSpace(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2684
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTextSpace()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2688
    :goto_0
    return v0

    .line 2685
    :catch_0
    move-exception v0

    .line 2688
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisTextStyle(I)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 948
    const/4 v1, 0x0

    .line 950
    .local v1, "ret":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getMainTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 955
    :goto_0
    return-object v1

    .line 952
    :catch_0
    move-exception v0

    .line 953
    .local v0, "indexOutofException":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getYAxisTextVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2446
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getVisibleAxisText()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2451
    :goto_0
    return v0

    .line 2448
    :catch_0
    move-exception v0

    .line 2451
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2566
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTitleText()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2570
    :goto_0
    return-object v0

    .line 2567
    :catch_0
    move-exception v0

    .line 2570
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisTitleAlign(I)I
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2581
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getYAxisTitleAlign()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2585
    :goto_0
    return v0

    .line 2582
    :catch_0
    move-exception v0

    .line 2585
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisTitleSpacingAlign(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2610
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSpacingAlign()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2614
    :goto_0
    return v0

    .line 2611
    :catch_0
    move-exception v0

    .line 2614
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisTitleSpacingAxis(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2596
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getSpacingAxisLine()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2600
    :goto_0
    return v0

    .line 2597
    :catch_0
    move-exception v0

    .line 2600
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisTitleTextStyle(I)Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 3
    .param p1, "idx"    # I

    .prologue
    .line 1051
    const/4 v1, 0x0

    .line 1053
    .local v1, "ret":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1059
    :goto_0
    return-object v1

    .line 1054
    :catch_0
    move-exception v0

    .line 1055
    .local v0, "indexOutofException":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getYAxisUseManualMarking(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2496
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getUseManualMarking()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2501
    :goto_0
    return v0

    .line 2498
    :catch_0
    move-exception v0

    .line 2501
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisVisible(I)Z
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2413
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->isVisible()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2419
    :goto_0
    return v0

    .line 2416
    :catch_0
    move-exception v0

    .line 2419
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getYAxisWidth(I)F
    .locals 1
    .param p1, "idx"    # I

    .prologue
    .line 2549
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getStrokeWidth()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2554
    :goto_0
    return v0

    .line 2551
    :catch_0
    move-exception v0

    .line 2554
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handlerFadeIn()V
    .locals 2

    .prologue
    .line 2015
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setFadeIn(Z)V

    .line 2016
    return-void
.end method

.method public handlerFadeOut()V
    .locals 2

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setFadeOut(Z)V

    .line 2020
    return-void
.end method

.method public handlerItemFadeIn()V
    .locals 2

    .prologue
    .line 2023
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemFadeIn(Z)V

    .line 2024
    return-void
.end method

.method public handlerItemFadeOut()V
    .locals 2

    .prologue
    .line 2027
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemFadeOut(Z)V

    .line 2028
    return-void
.end method

.method public handlerLineFadeIn()V
    .locals 2

    .prologue
    .line 2031
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineFadeIn(Z)V

    .line 2032
    return-void
.end method

.method public handlerLineFadeOut()V
    .locals 2

    .prologue
    .line 2035
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineFadeOut(Z)V

    .line 2036
    return-void
.end method

.method public handlerToolTipFadeIn()V
    .locals 2

    .prologue
    .line 2039
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoFadeIn(Z)V

    .line 2040
    return-void
.end method

.method public handlerToolTipFadeOut()V
    .locals 2

    .prologue
    .line 2043
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoFadeOut(Z)V

    .line 2044
    return-void
.end method

.method public setGridColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 1425
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setFillColor(IIII)V

    .line 1426
    return-void
.end method

.method public setGridUseManualMarking(ZI[I)V
    .locals 2
    .param p1, "enable"    # Z
    .param p2, "manualMarkingCnt"    # I
    .param p3, "visibleIndex"    # [I

    .prologue
    .line 1438
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setUseManualMarking(Z)V

    .line 1439
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setManualMarkingValue(I)V

    .line 1440
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0, p3}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setVisibleIndex([I)V

    .line 1441
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    array-length v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setVisibleIndexCount(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1447
    :goto_0
    return-void

    .line 1444
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setGridVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1389
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setVisible(Z)V

    .line 1390
    return-void
.end method

.method public setGridWidth(F)V
    .locals 1
    .param p1, "strokeWidth"    # F

    .prologue
    .line 1416
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setStrokeWidth(F)V

    .line 1417
    return-void
.end method

.method public setGridXLineVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1398
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setXLineVisible(Z)V

    .line 1399
    return-void
.end method

.method public setGridYLineVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mGridLineProperty:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->setYLineVisible(Z)V

    .line 1408
    return-void
.end method

.method public setHandlerAnimation(Z)V
    .locals 1
    .param p1, "use"    # Z

    .prologue
    .line 1772
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setAnimationUse(Z)V

    .line 1773
    return-void
.end method

.method public setHandlerAnimationTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setAnimationTime(J)V

    .line 1783
    return-void
.end method

.method public setHandlerEnable(Z)V
    .locals 1
    .param p1, "use"    # Z

    .prologue
    .line 1521
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemEnable(Z)V

    .line 1522
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineEnable(Z)V

    .line 1523
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoEnable(Z)V

    .line 1525
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setEnable(Z)V

    .line 1526
    return-void
.end method

.method public setHandlerItemColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 1635
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemFillColor(IIII)V

    .line 1636
    return-void
.end method

.method public setHandlerItemDateFormat(Ljava/lang/String;)V
    .locals 1
    .param p1, "dateFormat"    # Ljava/lang/String;

    .prologue
    .line 1645
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemDateFormat(Ljava/lang/String;)V

    .line 1646
    return-void
.end method

.method public setHandlerItemHeight(F)V
    .locals 1
    .param p1, "height"    # F

    .prologue
    .line 1598
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemHeight(F)V

    .line 1599
    return-void
.end method

.method public setHandlerItemImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1653
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemImage(Landroid/graphics/Bitmap;)V

    .line 1654
    return-void
.end method

.method public setHandlerItemOffset(F)V
    .locals 1
    .param p1, "offset"    # F

    .prologue
    .line 1607
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemOffset(F)V

    .line 1608
    return-void
.end method

.method public setHandlerItemPressedColor(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemFocusImage(Landroid/graphics/Bitmap;)V

    .line 1681
    return-void
.end method

.method public setHandlerItemShapeType(I)V
    .locals 1
    .param p1, "shape"    # I

    .prologue
    .line 1580
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemShape(I)V

    .line 1581
    return-void
.end method

.method public setHandlerItemStrokeWidth(F)V
    .locals 1
    .param p1, "strokeWidth"    # F

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemStrokeWidth(F)V

    .line 1663
    return-void
.end method

.method public setHandlerItemTextOffset(F)V
    .locals 1
    .param p1, "offset"    # F

    .prologue
    .line 1616
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemTextOffset(F)V

    .line 1617
    return-void
.end method

.method public setHandlerItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 1689
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1690
    return-void
.end method

.method public setHandlerItemTextVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1626
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemTextVisible(Z)V

    .line 1627
    return-void
.end method

.method public setHandlerItemVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemVisible(Z)V

    .line 1544
    return-void
.end method

.method public setHandlerItemWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 1589
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setItemWidth(F)V

    .line 1590
    return-void
.end method

.method public setHandlerLineColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 1725
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineColor(IIII)V

    .line 1726
    return-void
.end method

.method public setHandlerLineImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1734
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineImage(Landroid/graphics/Bitmap;)V

    .line 1735
    return-void
.end method

.method public setHandlerLinePressedColor(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineFocusImage(Landroid/graphics/Bitmap;)V

    .line 1748
    return-void
.end method

.method public setHandlerLineType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 1707
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineType(I)V

    .line 1708
    return-void
.end method

.method public setHandlerLineVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1552
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineVisible(Z)V

    .line 1553
    return-void
.end method

.method public setHandlerLineWidth(F)V
    .locals 1
    .param p1, "strokeWidth"    # F

    .prologue
    .line 1716
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setLineWidth(F)V

    .line 1717
    return-void
.end method

.method public setHandlerListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;

    .prologue
    .line 1988
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setListener(Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle$HandlerListener;)V

    .line 1989
    return-void
.end method

.method public setHandlerTimeOutDelay(J)V
    .locals 1
    .param p1, "handlerTimeOutDelay"    # J

    .prologue
    .line 1762
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setTimeOutDelay(J)V

    .line 1763
    return-void
.end method

.method public setHandlerTooltipColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 1793
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoFillColor(IIII)V

    .line 1794
    return-void
.end method

.method public setHandlerTooltipEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1571
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoEnable(Z)V

    .line 1572
    return-void
.end method

.method public setHandlerTooltipImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1823
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoImage(Landroid/graphics/Bitmap;)V

    .line 1824
    return-void
.end method

.method public setHandlerTooltipShape(I)V
    .locals 0
    .param p1, "shape"    # I

    .prologue
    .line 1852
    return-void
.end method

.method public setHandlerTooltipStrokeColor(I)V
    .locals 5
    .param p1, "strokeColor"    # I

    .prologue
    .line 1803
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoStrokeColor(IIII)V

    .line 1804
    return-void
.end method

.method public setHandlerTooltipStrokeWidth(F)V
    .locals 1
    .param p1, "strokeWidth"    # F

    .prologue
    .line 1813
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoStrokeWidth(F)V

    .line 1814
    return-void
.end method

.method public setHandlerTooltipTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 1832
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1833
    return-void
.end method

.method public setHandlerTooltipXYCoordinateUpdatable(Z)V
    .locals 1
    .param p1, "updatable"    # Z

    .prologue
    .line 1998
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mHandlerProperty:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setInfoXYCoordinateUpdatable(Z)V

    .line 1999
    return-void
.end method

.method public setMaxInclueGoalExceedDataCount(I)V
    .locals 1
    .param p1, "dataCount"    # I

    .prologue
    .line 2010
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setMaxInclueGoalExceedDataCount(I)V

    .line 2011
    return-void
.end method

.method public setPopupColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 1935
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setFillColor(IIII)V

    .line 1936
    return-void
.end method

.method public setPopupEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1860
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setEnable(Z)V

    .line 1862
    return-void
.end method

.method public setPopupImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setImage(Landroid/graphics/Bitmap;)V

    .line 1946
    return-void
.end method

.method public setPopupShapeType(I)V
    .locals 1
    .param p1, "shape"    # I

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setShape(I)V

    .line 1889
    return-void
.end method

.method public setPopupSize(F)V
    .locals 1
    .param p1, "radius"    # F

    .prologue
    .line 1925
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setRadius(F)V

    .line 1926
    return-void
.end method

.method public setPopupSize(FF)V
    .locals 1
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 1911
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setWidth(F)V

    .line 1912
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setHeight(F)V

    .line 1914
    return-void
.end method

.method public setPopupStrokeColor(I)V
    .locals 5
    .param p1, "strokeColor"    # I

    .prologue
    .line 1955
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setStrokeColor(IIII)V

    .line 1956
    return-void
.end method

.method public setPopupStrokeWidth(F)V
    .locals 1
    .param p1, "strokeWidth"    # F

    .prologue
    .line 1898
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setStrokeWidth(F)V

    .line 1899
    return-void
.end method

.method public setPopupTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 1965
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1966
    return-void
.end method

.method public setPopupVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1871
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mInfoPointProperty:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->setVisible(Z)V

    .line 1873
    return-void
.end method

.method public setSeparatorDateFormat(Ljava/lang/String;)V
    .locals 2
    .param p1, "dateFormat"    # Ljava/lang/String;

    .prologue
    .line 1478
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSeparatorDateFormat(Ljava/lang/String;)V

    .line 1479
    return-void
.end method

.method public setSeparatorLineThickness(F)V
    .locals 2
    .param p1, "thickness"    # F

    .prologue
    .line 1511
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSeparatorLineThickness(F)V

    .line 1512
    return-void
.end method

.method public setSeparatorTextSpacingLine(I)V
    .locals 2
    .param p1, "spacing"    # I

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSeparatorSpacingLine(I)V

    .line 1501
    return-void
.end method

.method public setSeparatorTextSpacingTop(I)V
    .locals 2
    .param p1, "spacing"    # I

    .prologue
    .line 1489
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSeparatorSpacingTop(I)V

    .line 1490
    return-void
.end method

.method public setSeparatorTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 2
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 1466
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSeparatorTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 1467
    return-void
.end method

.method public setSeparatorVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 1455
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSeparatorVisible(Z)V

    .line 1456
    return-void
.end method

.method public setXAxisColor(II)V
    .locals 5
    .param p1, "color"    # I
    .param p2, "idx"    # I

    .prologue
    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setFillColor(IIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    :goto_0
    return-void

    .line 327
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisDateFormat(Ljava/lang/String;I)V
    .locals 1
    .param p1, "dateFormat"    # Ljava/lang/String;
    .param p2, "idx"    # I

    .prologue
    .line 508
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setDateFormat(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    :goto_0
    return-void

    .line 510
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisDirection(II)V
    .locals 1
    .param p1, "typeDirection"    # I
    .param p2, "idx"    # I

    .prologue
    .line 729
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 730
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeDirection(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 735
    :cond_1
    :goto_0
    return-void

    .line 732
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisLabelAlign(II)V
    .locals 1
    .param p1, "align"    # I
    .param p2, "idx"    # I

    .prologue
    .line 645
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setXAxisLabelAlign(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 649
    :goto_0
    return-void

    .line 646
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisLabelTitle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "labelTitle"    # Ljava/lang/String;
    .param p2, "idx"    # I

    .prologue
    .line 661
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setLabelTitleText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 665
    :goto_0
    return-void

    .line 662
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisLabelTitleAlign(II)V
    .locals 1
    .param p1, "align"    # I
    .param p2, "idx"    # I

    .prologue
    .line 677
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setLabelTitleAlign(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 682
    :goto_0
    return-void

    .line 679
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p2, "idx"    # I

    .prologue
    .line 695
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 700
    :goto_0
    return-void

    .line 697
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisLineVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleAxisLine(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    :goto_0
    return-void

    .line 273
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisMarkingVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 308
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleMarking(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_0
    return-void

    .line 310
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisSubTextSpace(FI)V
    .locals 1
    .param p1, "space"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1217
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSubTextSpace(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1221
    :goto_0
    return-void

    .line 1218
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisSubTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p2, "idx"    # I

    .prologue
    .line 492
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSubTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    :goto_0
    return-void

    .line 494
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisSubTextUse(ZI)V
    .locals 1
    .param p1, "use"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 1235
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSubTextUse(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1239
    :goto_0
    return-void

    .line 1236
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTextSpace(FI)V
    .locals 1
    .param p1, "space"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1199
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTextSpace(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1203
    :goto_0
    return-void

    .line 1200
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p2, "idx"    # I

    .prologue
    .line 361
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setMainTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 366
    :goto_0
    return-void

    .line 363
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTextStyleForWeekend(ZIILcom/samsung/android/sdk/chart/style/SchartTextStyle;Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "enable"    # Z
    .param p2, "firstDayOfWeekend"    # I
    .param p3, "secondDayOfWeekend"    # I
    .param p4, "style1"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p5, "style2"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p6, "idx"    # I

    .prologue
    .line 385
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p6

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setUseColorForWeekend(Z)V

    .line 386
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p6

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setFirstDayOfWeekend(I)V

    .line 387
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p6

    invoke-virtual {v0, p3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSecondDayOfWeekend(I)V

    .line 388
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p6

    invoke-virtual {v0, p4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTextStyleForWeekend1(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 389
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p6

    invoke-virtual {v0, p5}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTextStyleForWeekend2(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    :goto_0
    return-void

    .line 391
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTextVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleAxisText(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :goto_0
    return-void

    .line 292
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTitle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "idx"    # I

    .prologue
    .line 544
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTitleText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 548
    :goto_0
    return-void

    .line 545
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTitleAlign(II)V
    .locals 1
    .param p1, "align"    # I
    .param p2, "idx"    # I

    .prologue
    .line 562
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setXAxisTitleAlign(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    :goto_0
    return-void

    .line 563
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTitleSpacingAlign(FI)V
    .locals 1
    .param p1, "space"    # F
    .param p2, "idx"    # I

    .prologue
    .line 595
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSpacingAlign(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 599
    :goto_0
    return-void

    .line 596
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTitleSpacingAxis(FI)V
    .locals 1
    .param p1, "space"    # F
    .param p2, "idx"    # I

    .prologue
    .line 578
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSpacingAxisLine(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 582
    :goto_0
    return-void

    .line 579
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p2, "idx"    # I

    .prologue
    .line 611
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 615
    :goto_0
    return-void

    .line 612
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    :goto_0
    return-void

    .line 253
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setXAxisWidth(FI)V
    .locals 1
    .param p1, "strokeWidth"    # F
    .param p2, "idx"    # I

    .prologue
    .line 342
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mXAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setStrokeWidth(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :goto_0
    return-void

    .line 344
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisAssociatedSeriesID(I[II)V
    .locals 1
    .param p1, "associatedSeriesIDsCnt"    # I
    .param p2, "associatedSeriesIDs"    # [I
    .param p3, "idx"    # I

    .prologue
    .line 842
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p3

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 843
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p3

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 848
    :goto_0
    return-void

    .line 845
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisColor(II)V
    .locals 5
    .param p1, "color"    # I
    .param p2, "idx"    # I

    .prologue
    .line 898
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setFillColor(IIII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 903
    :goto_0
    return-void

    .line 900
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisCustomLabel([Ljava/lang/String;I)V
    .locals 1
    .param p1, "labels"    # [Ljava/lang/String;
    .param p2, "idx"    # I

    .prologue
    .line 882
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setCustomYLabelStrs([Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 888
    :goto_0
    return-void

    .line 884
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisDirection(II)V
    .locals 1
    .param p1, "typeDirection"    # I
    .param p2, "idx"    # I

    .prologue
    .line 822
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 823
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTypeDirection(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    :cond_1
    :goto_0
    return-void

    .line 825
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisLabelAlign(II)V
    .locals 1
    .param p1, "align"    # I
    .param p2, "idx"    # I

    .prologue
    .line 1072
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisLabelAlign(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1076
    :goto_0
    return-void

    .line 1073
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisLabelOffsets([FI)V
    .locals 1
    .param p1, "labelOffsets"    # [F
    .param p2, "idx"    # I

    .prologue
    .line 1128
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYLabelOffsets([F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1132
    :goto_0
    return-void

    .line 1129
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisLabelTitle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "labelTitle"    # Ljava/lang/String;
    .param p2, "idx"    # I

    .prologue
    .line 1088
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setLabelTitleText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1092
    :goto_0
    return-void

    .line 1089
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisLabelTitleAlign(II)V
    .locals 1
    .param p1, "align"    # I
    .param p2, "idx"    # I

    .prologue
    .line 1181
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setLabelTitleAlign(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1186
    :goto_0
    return-void

    .line 1183
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisLabelTitleOffset(FI)V
    .locals 1
    .param p1, "offset"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1108
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setLabelTitleOffset(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1112
    :goto_0
    return-void

    .line 1109
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p2, "idx"    # I

    .prologue
    .line 1146
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setLabelTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1151
    :goto_0
    return-void

    .line 1148
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisLineVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 769
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleAxisLine(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 774
    :goto_0
    return-void

    .line 771
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisMarkingVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 805
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleMarking(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 810
    :goto_0
    return-void

    .line 807
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisMaxRoundDigit(II)V
    .locals 1
    .param p1, "digit"    # I
    .param p2, "idx"    # I

    .prologue
    .line 1282
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisMaxRoundDigit(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1286
    :goto_0
    return-void

    .line 1283
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisMaxRoundMagnification(FI)V
    .locals 1
    .param p1, "magnification"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1297
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisMaxRoundMagnification(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1301
    :goto_0
    return-void

    .line 1298
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisMaxRounding(II)V
    .locals 1
    .param p1, "rounding"    # I
    .param p2, "idx"    # I

    .prologue
    .line 1267
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisMaxRounding(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1271
    :goto_0
    return-void

    .line 1268
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisMinRoundDigit(II)V
    .locals 1
    .param p1, "digit"    # I
    .param p2, "idx"    # I

    .prologue
    .line 1327
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisMinRoundDigit(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1331
    :goto_0
    return-void

    .line 1328
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisMinRoundMagnification(FI)V
    .locals 1
    .param p1, "magnification"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1342
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisMinRoundMagnification(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1346
    :goto_0
    return-void

    .line 1343
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisMinRounding(II)V
    .locals 1
    .param p1, "rounding"    # I
    .param p2, "idx"    # I

    .prologue
    .line 1312
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisMinRounding(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1316
    :goto_0
    return-void

    .line 1313
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisReversedLabel(ZI)V
    .locals 1
    .param p1, "reversed"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 1360
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisReversed(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1364
    :goto_0
    return-void

    .line 1361
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTextSpace(FI)V
    .locals 1
    .param p1, "space"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1252
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTextSpace(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1256
    :goto_0
    return-void

    .line 1253
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p2, "idx"    # I

    .prologue
    .line 932
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setMainTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 937
    :goto_0
    return-void

    .line 934
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTextVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 786
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleAxisText(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 792
    :goto_0
    return-void

    .line 788
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTitle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "idx"    # I

    .prologue
    .line 968
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTitleText(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 972
    :goto_0
    return-void

    .line 969
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTitleAlign(II)V
    .locals 1
    .param p1, "align"    # I
    .param p2, "idx"    # I

    .prologue
    .line 986
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setYAxisTitleAlign(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 991
    :goto_0
    return-void

    .line 987
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTitleSpacingAlign(FI)V
    .locals 1
    .param p1, "space"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1020
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSpacingAlign(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1024
    :goto_0
    return-void

    .line 1021
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTitleSpacingAxis(FI)V
    .locals 1
    .param p1, "space"    # F
    .param p2, "idx"    # I

    .prologue
    .line 1003
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setSpacingAxisLine(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1007
    :goto_0
    return-void

    .line 1004
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;I)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .param p2, "idx"    # I

    .prologue
    .line 1036
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1040
    :goto_0
    return-void

    .line 1037
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisUseManualMarking(ZI[II)V
    .locals 2
    .param p1, "enable"    # Z
    .param p2, "manualMarkingCnt"    # I
    .param p3, "visibleIndex"    # [I
    .param p4, "idx"    # I

    .prologue
    .line 862
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p4

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setUseManualMarking(Z)V

    .line 863
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p4

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setManualMarkingValue(I)V

    .line 864
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p4

    invoke-virtual {v0, p3}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleIndex([I)V

    .line 865
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p4

    array-length v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisibleIndexCount(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 870
    :goto_0
    return-void

    .line 867
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisVisible(ZI)V
    .locals 1
    .param p1, "visible"    # Z
    .param p2, "idx"    # I

    .prologue
    .line 750
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisible(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 756
    :goto_0
    return-void

    .line 753
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setYAxisWidth(FI)V
    .locals 1
    .param p1, "strokeWidth"    # F
    .param p2, "idx"    # I

    .prologue
    .line 915
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->mYAxisPropertys:[Lcom/sec/dmc/sic/android/property/AxisProperty;

    aget-object v0, v0, p2

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setStrokeWidth(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 920
    :goto_0
    return-void

    .line 917
    :catch_0
    move-exception v0

    goto :goto_0
.end method
