.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$ElectroCardiographyAnalysisColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ElectroCardiographyAnalysisColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final ELECTRO_CARDIOGRAPHY__ID:Ljava/lang/String; = "electro_cardiography__id"

.field public static final RESULT_ID:Ljava/lang/String; = "result_id"

.field public static final RESULT_TYPE:Ljava/lang/String; = "result_type"

.field public static final SERVER_TYPE:Ljava/lang/String; = "server_type"
