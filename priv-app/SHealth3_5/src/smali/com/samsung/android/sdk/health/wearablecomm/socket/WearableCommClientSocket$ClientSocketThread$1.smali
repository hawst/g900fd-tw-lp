.class Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;
.super Landroid/os/Handler;
.source "WearableCommClientSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 132
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "client handleMessage(Message msg)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 335
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 139
    :pswitch_1
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    new-instance v3, Landroid/net/LocalSocket;

    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$202(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;

    .line 140
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v2

    new-instance v3, Landroid/net/LocalSocketAddress;

    const-string v4, "HEALTH_SERVICE_SOCKET"

    invoke-direct {v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 141
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v3}, Landroid/net/LocalSocket;->setSendBufferSize(I)V

    .line 142
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 144
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionSuccess()V

    .line 146
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    new-instance v3, Ljava/io/BufferedOutputStream;

    iget-object v4, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v4, v4, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v4}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$402(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Ljava/io/BufferedOutputStream;)Ljava/io/BufferedOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v1

    .line 151
    .local v1, "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception ocurred while creating client socket!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 154
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    invoke-interface {v2, v5}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 156
    :cond_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 163
    .end local v1    # "ex":Ljava/io/IOException;
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 167
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 169
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;->longToByte(J)[B

    move-result-object v0

    .line 170
    .local v0, "byteArray":[B
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 171
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    .line 172
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Certification DATA SENT FROM CLIENT SOCKET = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-direct {v4, v0, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 175
    .end local v0    # "byteArray":[B
    :catch_1
    move-exception v1

    .line 177
    .restart local v1    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception ocurred while writing certification data to socket!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 180
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 182
    :cond_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 190
    .end local v1    # "ex":Ljava/io/IOException;
    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 196
    :try_start_2
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 198
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v3

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-virtual {v3, v2}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 200
    :cond_4
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DATA SENT FROM CLIENT SOCKET = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 202
    :catch_2
    move-exception v1

    .line 204
    .restart local v1    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception ocurred while writing data to socket!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 207
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    invoke-interface {v2, v5}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 209
    :cond_5
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 269
    .end local v1    # "ex":Ljava/io/IOException;
    :pswitch_4
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 273
    :try_start_3
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    const-string v3, "END_OF_DATA"

    const-string v4, "UTF-8"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 275
    :catch_3
    move-exception v1

    .line 277
    .restart local v1    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception ocurred while writing data to socket!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 280
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    invoke-interface {v2, v5}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 282
    :cond_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 290
    .end local v1    # "ex":Ljava/io/IOException;
    :pswitch_5
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 292
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 294
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x43fa

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 295
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->getClientSocketMsgHandler()Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x43fb

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 299
    :cond_7
    :try_start_4
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 300
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->localClientSocketOutputStream:Ljava/io/BufferedOutputStream;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Ljava/io/BufferedOutputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 315
    :cond_8
    :goto_1
    :try_start_5
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 316
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Landroid/net/LocalSocket;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/LocalSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 327
    :cond_9
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 328
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->destroyLooper()V

    .line 329
    :cond_a
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->clientSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;
    invoke-static {v2, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$502(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    .line 330
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$302(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    goto/16 :goto_0

    .line 303
    :catch_4
    move-exception v1

    .line 305
    .restart local v1    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception ocurred while closing outputstream!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 308
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    invoke-interface {v2, v5}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 310
    :cond_b
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 318
    .end local v1    # "ex":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 320
    .restart local v1    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Exception ocurred while closing client socket!!"

    invoke-static {v2, v3}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 323
    iget-object v2, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket$ClientSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v2}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;->access$300(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommClientSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v2

    invoke-interface {v2, v5}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 325
    :cond_c
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 133
    :pswitch_data_0
    .packed-switch 0x43f8
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_2
    .end packed-switch
.end method
