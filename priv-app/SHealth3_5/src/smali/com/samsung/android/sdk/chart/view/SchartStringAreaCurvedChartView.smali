.class public Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartAreaCurvedChartView;
.source "SchartStringAreaCurvedChartView.java"


# instance fields
.field private nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartAreaCurvedChartView;-><init>(Landroid/content/Context;)V

    .line 36
    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartAreaCurvedChartView;->setDataType(I)V

    .line 37
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartAreaCurvedChartView;->createChart()V

    .line 39
    new-instance v0, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v1, 0x1

    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartAreaCurvedChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    invoke-super {p0, v1}, Lcom/samsung/android/sdk/chart/view/SchartAreaCurvedChartView;->setDataType(I)V

    .line 52
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartAreaCurvedChartView;->createChart()V

    .line 54
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mSeriesNum:I

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->createSeries(II)V

    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 58
    new-instance v0, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;-><init>(Landroid/content/Context;)V

    .line 72
    const/4 v1, 0x1

    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->createSeries(II)V

    .line 74
    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfXAxis()I

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfYAxis()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->addAxis(II)V

    .line 76
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 78
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 81
    return-void

    .line 79
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->addDataBack(ILjava/util/List;)V

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addDataBack(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 143
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addData(IILjava/util/List;)V

    .line 147
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public addDataFront(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 161
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addDataFront(IILjava/util/List;)V

    .line 165
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 194
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->clearData()V

    goto :goto_0
.end method

.method public prepareChangeData(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 177
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 179
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->prepareChangeData(IILjava/util/List;)V

    goto :goto_0
.end method

.method public setHandlerStartIndex(I)V
    .locals 2
    .param p1, "handlerStartIndex"    # I

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 211
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setHandlerStartIndex(I)V

    goto :goto_0
.end method

.method public setStartIndex(I)V
    .locals 2
    .param p1, "startIndex"    # I

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 109
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartIndex(I)V

    .line 113
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartValidCount(I)V
    .locals 2
    .param p1, "validCount"    # I

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 125
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartIndex(I)V

    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartVisual(II)V
    .locals 2
    .param p1, "startIndex"    # I
    .param p2, "validCount"    # I

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 93
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartVisual(II)V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringAreaCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method
