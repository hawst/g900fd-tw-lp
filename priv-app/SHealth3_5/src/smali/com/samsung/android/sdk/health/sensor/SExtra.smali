.class public Lcom/samsung/android/sdk/health/sensor/SExtra;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SExtra.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SExtra;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public averageSpeed:F

.field public consumedCalorie:F

.field public declineDistance:F

.field public declineTime:J

.field public extra:Landroid/os/Bundle;

.field public flatDistance:F

.field public flatTime:J

.field public inclineDistance:F

.field public inclineTime:J

.field public stepCount:I

.field public totalDistance:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SExtra$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SExtra$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SExtra;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 74
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide v1, 0x7fffffffffffffffL

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 32
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 10
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->totalDistance:F

    .line 12
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineDistance:F

    .line 14
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineDistance:F

    .line 16
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatDistance:F

    .line 18
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineTime:J

    .line 20
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineTime:J

    .line 22
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatTime:J

    .line 24
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->averageSpeed:F

    .line 26
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->consumedCalorie:F

    .line 28
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->stepCount:I

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->extra:Landroid/os/Bundle;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide v1, 0x7fffffffffffffffL

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 36
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 10
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->totalDistance:F

    .line 12
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineDistance:F

    .line 14
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineDistance:F

    .line 16
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatDistance:F

    .line 18
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineTime:J

    .line 20
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineTime:J

    .line 22
    iput-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatTime:J

    .line 24
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->averageSpeed:F

    .line 26
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->consumedCalorie:F

    .line 28
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->stepCount:I

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->extra:Landroid/os/Bundle;

    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SExtra;->readFromParcel(Landroid/os/Parcel;)V

    .line 39
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->totalDistance:F

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineDistance:F

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineDistance:F

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatDistance:F

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->averageSpeed:F

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineTime:J

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineTime:J

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatTime:J

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->consumedCalorie:F

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->stepCount:I

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->extra:Landroid/os/Bundle;

    .line 89
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[Location Extra] totalDistance : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->totalDistance:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inclineDistance: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineDistance:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 94
    const-string v1, ", declineDistance:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineDistance:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flatDistance:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatDistance:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 95
    const-string v1, ", averageSpeed:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->averageSpeed:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", inclineTime"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 96
    const-string v1, ", declineTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flatTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", consumedCalorie:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->consumedCalorie:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 97
    const-string v1, " , stepCount = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->stepCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->totalDistance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 48
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineDistance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 49
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineDistance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 50
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatDistance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 51
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->averageSpeed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 52
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->inclineTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->declineTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 54
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->flatTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 55
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->consumedCalorie:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 56
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->stepCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SExtra;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 58
    return-void
.end method
