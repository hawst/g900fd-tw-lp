.class public Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;
.super Ljava/lang/Object;
.source "SChartSpotShowInfo.java"


# instance fields
.field private imageIndex:I

.field private seriesId:I

.field private seriesXValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->seriesId:I

    .line 12
    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->imageIndex:I

    .line 13
    return-void
.end method


# virtual methods
.method public getImageIndex()I
    .locals 1

    .prologue
    .line 22
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->imageIndex:I

    return v0
.end method

.method public getSeriesId()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->seriesId:I

    return v0
.end method

.method public getSeriesXValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->seriesXValue:Ljava/lang/String;

    return-object v0
.end method

.method public setImageIndex(I)V
    .locals 0
    .param p1, "imageIndex"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->imageIndex:I

    .line 26
    return-void
.end method

.method public setSeriesId(I)V
    .locals 0
    .param p1, "seriesId"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->seriesId:I

    .line 32
    return-void
.end method

.method public setSeriesXValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "seriesXValue"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;->seriesXValue:Ljava/lang/String;

    .line 20
    return-void
.end method
