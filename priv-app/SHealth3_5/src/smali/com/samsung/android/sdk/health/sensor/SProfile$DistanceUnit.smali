.class public interface abstract Lcom/samsung/android/sdk/health/sensor/SProfile$DistanceUnit;
.super Ljava/lang/Object;
.source "SProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/SProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DistanceUnit"
.end annotation


# static fields
.field public static final KILLOMETERS:I = 0x29811

.field public static final MILES:I = 0x29813

.field public static final NOT_DEFINED:I = -0x1

.field public static final YARDS:I = 0x29812
