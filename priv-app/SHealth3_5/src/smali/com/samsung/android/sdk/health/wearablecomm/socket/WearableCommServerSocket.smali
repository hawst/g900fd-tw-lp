.class public Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
.super Ljava/lang/Object;
.source "WearableCommServerSocket.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/wearablecomm/data/IWearableServerSocket;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$1;,
        Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private acceptWaiting:Z

.field private isClosedCalled:Z

.field private isDummyClient:Z

.field private localClientSocketInputStream:Ljava/io/BufferedInputStream;

.field private mLocalClientSocket:Landroid/net/LocalSocket;

.field private mLocalServerSocket:Landroid/net/LocalServerSocket;

.field private mReceivedDataBuffer:[B

.field private mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

.field private serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[HealthSensor]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .line 28
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    .line 29
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B

    .line 34
    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->acceptWaiting:Z

    .line 35
    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z

    .line 36
    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isClosedCalled:Z

    .line 40
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .line 41
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalServerSocket;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Landroid/net/LocalServerSocket;)Landroid/net/LocalServerSocket;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # Landroid/net/LocalServerSocket;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;

    return-object p1
.end method

.method static synthetic access$302(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->acceptWaiting:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalSocket;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # Landroid/net/LocalSocket;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Ljava/io/BufferedInputStream;)Ljava/io/BufferedInputStream;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # Ljava/io/BufferedInputStream;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p1, "x1"    # [B

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isClosedCalled:Z

    return v0
.end method


# virtual methods
.method public close()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 121
    iput-boolean v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isClosedCalled:Z

    .line 122
    iget-object v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;

    if-eqz v3, :cond_1

    .line 126
    :try_start_0
    iget-boolean v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->acceptWaiting:Z

    if-eqz v3, :cond_0

    .line 129
    new-instance v1, Landroid/net/LocalSocket;

    invoke-direct {v1}, Landroid/net/LocalSocket;-><init>()V

    .line 130
    .local v1, "mClientSocket":Landroid/net/LocalSocket;
    new-instance v3, Landroid/net/LocalSocketAddress;

    const-string v4, "HEALTH_SERVICE_SOCKET"

    invoke-direct {v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 131
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z

    .line 132
    invoke-virtual {v1}, Landroid/net/LocalSocket;->close()V

    .line 134
    .end local v1    # "mClientSocket":Landroid/net/LocalSocket;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;

    invoke-virtual {v3}, Landroid/net/LocalServerSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 141
    :cond_1
    :goto_0
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 142
    .local v2, "msg":Landroid/os/Message;
    const/16 v3, 0x43fd

    iput v3, v2, Landroid/os/Message;->what:I

    .line 143
    iget-object v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 145
    iget-object v3, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 147
    :cond_2
    return-void

    .line 136
    .end local v2    # "msg":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public finishSending()V
    .locals 2

    .prologue
    .line 103
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 104
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fc

    iput v1, v0, Landroid/os/Message;->what:I

    .line 105
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 106
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 107
    :cond_0
    return-void
.end method

.method public initialize()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 46
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 47
    .local v2, "msg":Landroid/os/Message;
    const/16 v5, 0x43f8

    iput v5, v2, Landroid/os/Message;->what:I

    .line 49
    iput-boolean v6, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isClosedCalled:Z

    .line 50
    iput-boolean v6, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z

    .line 51
    new-instance v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;-><init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$1;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    .line 52
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->start()V

    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    .line 54
    .local v3, "startTime":J
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v5

    if-nez v5, :cond_2

    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    const-wide/16 v7, 0x3e8

    div-long v0, v5, v7

    .line 57
    .local v0, "elapsedTime":J
    const-wide/16 v5, 0x1

    cmp-long v5, v0, v5

    if-lez v5, :cond_0

    .line 59
    sget-object v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;

    const-string v6, "Problem initializing the server socket!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    if-eqz v5, :cond_1

    .line 62
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 68
    .end local v0    # "elapsedTime":J
    :cond_1
    :goto_0
    return-void

    .line 67
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public listenToConnect()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 74
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43f9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 75
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 77
    :cond_0
    return-void
.end method

.method public receiveData()V
    .locals 2

    .prologue
    .line 112
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 113
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fb

    iput v1, v0, Landroid/os/Message;->what:I

    .line 114
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 116
    :cond_0
    return-void
.end method

.method public sendData([B)I
    .locals 2
    .param p1, "buffer"    # [B

    .prologue
    .line 89
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 90
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x43fa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 91
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 92
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 97
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 95
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    .line 84
    return-void
.end method
