.class public Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;
.super Ljava/lang/Object;
.source "_HealthServiceUpdation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$1;,
        Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$UDR;,
        Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;,
        Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;
    }
.end annotation


# static fields
.field private static final RESULT_FAIL:I = 0x0

.field private static final RESULT_SUCCESS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "HealthServiceUpdation"


# instance fields
.field private appId:Ljava/lang/String;

.field private mAppContext:Landroid/content/Context;

.field private mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

.field private mUiListener:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

.field private mUpdateNeeded:Z

.field private resultCode:Ljava/lang/String;

.field private resultMsg:Ljava/lang/String;

.field private version:Ljava/lang/String;

.field private versionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    .line 41
    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mAppContext:Landroid/content/Context;

    .line 43
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUpdateNeeded:Z

    .line 49
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->appId:Ljava/lang/String;

    .line 51
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->resultCode:Ljava/lang/String;

    .line 53
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->resultMsg:Ljava/lang/String;

    .line 55
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->version:Ljava/lang/String;

    .line 57
    const-string v1, ""

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->versionCode:Ljava/lang/String;

    .line 89
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mAppContext:Landroid/content/Context;

    .line 91
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    invoke-direct {v1, p0, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$1;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    .line 93
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 95
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->readModelCMCC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUiListener:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->version:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mAppContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->isLocalTestMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Ljava/io/InputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;
    .param p1, "x1"    # Ljava/io/InputStream;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->checkUpdate(Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getMCC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getMNC()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->isPD()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUpdateNeeded:Z

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUpdateNeeded:Z

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;)Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    return-object p1
.end method

.method private checkUpdate(Ljava/io/InputStream;)Z
    .locals 12
    .param p1, "inStream"    # Ljava/io/InputStream;

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x4

    .line 278
    const/4 v4, 0x0

    .line 286
    .local v4, "rtn":Z
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    .line 288
    .local v1, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 290
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v8, 0x0

    invoke-interface {v2, p1, v8}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 292
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 296
    .local v3, "parserEvent":I
    :goto_0
    const/4 v8, 0x1

    if-eq v3, v8, :cond_6

    .line 298
    const/4 v8, 0x2

    if-ne v3, v8, :cond_0

    .line 300
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 301
    .local v5, "tag":Ljava/lang/String;
    const-string v8, "HealthServiceUpdation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "XML parsed "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const-string v8, "appId"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 305
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .line 307
    .local v6, "type":I
    if-ne v6, v11, :cond_0

    .line 309
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->appId:Ljava/lang/String;

    .line 311
    const-string v8, "HealthServiceUpdation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Update RSP] appId : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->appId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "type":I
    :cond_0
    :goto_1
    const/4 v8, 0x3

    if-ne v3, v8, :cond_1

    .line 369
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v5

    .line 371
    .restart local v5    # "tag":Ljava/lang/String;
    const-string v8, "appInfo"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 379
    .end local v5    # "tag":Ljava/lang/String;
    :cond_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 315
    .restart local v5    # "tag":Ljava/lang/String;
    :cond_2
    const-string/jumbo v8, "resultCode"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 317
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .line 319
    .restart local v6    # "type":I
    if-ne v6, v11, :cond_0

    .line 321
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->resultCode:Ljava/lang/String;

    .line 323
    const-string v8, "HealthServiceUpdation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Update RSP] resultCode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->resultCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 385
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v3    # "parserEvent":I
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "type":I
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v8, "HealthServiceUpdation"

    const-string/jumbo v9, "xml parsing error"

    invoke-static {v8, v9, v0}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 405
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_2
    return v7

    .line 327
    .restart local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v3    # "parserEvent":I
    .restart local v5    # "tag":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string/jumbo v8, "resultMsg"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 329
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .line 331
    .restart local v6    # "type":I
    if-ne v6, v11, :cond_0

    .line 333
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->resultMsg:Ljava/lang/String;

    .line 335
    const-string v8, "HealthServiceUpdation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Update RSP] resultMsg : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->resultMsg:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 391
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v3    # "parserEvent":I
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "type":I
    :catch_1
    move-exception v0

    .line 393
    .local v0, "e":Ljava/net/SocketException;
    const-string v8, "HealthServiceUpdation"

    const-string v9, "Update check, network is unavailable"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 339
    .end local v0    # "e":Ljava/net/SocketException;
    .restart local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v3    # "parserEvent":I
    .restart local v5    # "tag":Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string/jumbo v8, "version"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 341
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .line 343
    .restart local v6    # "type":I
    if-ne v6, v11, :cond_0

    .line 345
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->version:Ljava/lang/String;

    .line 347
    const-string v8, "HealthServiceUpdation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Update RSP] version : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->version:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 397
    .end local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v3    # "parserEvent":I
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "type":I
    :catch_2
    move-exception v0

    .line 399
    .local v0, "e":Ljava/io/IOException;
    const-string v8, "HealthServiceUpdation"

    const-string v9, "Update check, but network fail"

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 351
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v2    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v3    # "parserEvent":I
    .restart local v5    # "tag":Ljava/lang/String;
    :cond_5
    :try_start_3
    const-string/jumbo v8, "versionCode"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 353
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    .line 355
    .restart local v6    # "type":I
    if-ne v6, v11, :cond_0

    .line 357
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->versionCode:Ljava/lang/String;

    .line 359
    const-string v8, "HealthServiceUpdation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[Update RSP] versionCode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->versionCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 383
    .end local v5    # "tag":Ljava/lang/String;
    .end local v6    # "type":I
    :cond_6
    iget-object v8, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->appId:Ljava/lang/String;

    iget-object v9, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->resultCode:Ljava/lang/String;

    invoke-direct {p0, v8, v9}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-result v4

    move v7, v4

    .line 405
    goto/16 :goto_2
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 535
    const/4 v5, 0x0

    .line 537
    .local v5, "s":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string v7, "/system/csc/sales_code.dat"

    invoke-direct {v4, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 539
    .local v4, "mFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 541
    const/16 v7, 0x14

    new-array v0, v7, [B

    .line 543
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 547
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 549
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    if-eqz v7, :cond_1

    .line 551
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v5    # "s":Ljava/lang/String;
    .local v6, "s":Ljava/lang/String;
    move-object v5, v6

    .line 565
    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 571
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_1
    return-object v5

    .line 555
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_1
    :try_start_2
    new-instance v6, Ljava/lang/String;

    const-string v7, "FAIL"

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .end local v5    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    move-object v5, v6

    .end local v6    # "s":Ljava/lang/String;
    .restart local v5    # "s":Ljava/lang/String;
    goto :goto_0

    .line 559
    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 561
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 565
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v7

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 559
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public static getIntentForHealthServiceUpdate()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 780
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 781
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.samsungapps"

    const-string v2, "com.sec.android.app.samsungapps.Main"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 782
    const-string v1, "directcall"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 783
    const-string v1, "CallerType"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 784
    const-string v1, "GUID"

    const-string v2, "com.sec.android.service.health"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 785
    const v1, 0x14000020

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 786
    const-string v1, "HealthServiceUpdation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "app store intent contents "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->describeContents()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " intent extras "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    return-object v0
.end method

.method private getMCC()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 411
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mAppContext:Landroid/content/Context;

    const-string/jumbo v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 413
    .local v3, "telMgr":Landroid/telephony/TelephonyManager;
    const-string v0, ""

    .line 415
    .local v0, "mcc":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v1, v0

    .line 457
    .end local v0    # "mcc":Ljava/lang/String;
    .local v1, "mcc":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 421
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 423
    .local v2, "networkOperator":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 441
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 443
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 445
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 457
    .end local v0    # "mcc":Ljava/lang/String;
    .restart local v1    # "mcc":Ljava/lang/String;
    goto :goto_0

    .line 427
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :pswitch_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 429
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 431
    goto :goto_1

    .line 435
    :cond_1
    const-string v0, ""

    .line 437
    goto :goto_1

    .line 449
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 423
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private getMNC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 465
    const-string v0, ""

    .line 467
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mAppContext:Landroid/content/Context;

    const-string/jumbo v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 469
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_1

    .line 483
    .end local v0    # "mnc":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 475
    .restart local v0    # "mnc":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 477
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 479
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getResultUpdateCheck(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 607
    const-string v1, "com.sec.android.service.health"

    .line 609
    .local v1, "stubPackageName":Ljava/lang/String;
    const-string v3, "HealthServiceUpdation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getResultUpdateCheck - packageName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", code : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    const-string v3, "HealthServiceUpdation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getResultUpdateCheck - stubPackageName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    const-string v3, "0"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 615
    const-string v3, "HealthServiceUpdation"

    const-string v4, "There is no application of the Application ID."

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    :cond_0
    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "2"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 629
    const-string v3, "HealthServiceUpdation"

    const-string v4, "Found Press Reader Update"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mAppContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.sec.android.app.samsungapps"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 643
    const-string v3, "HealthServiceUpdation"

    const-string/jumbo v4, "samsungapps installed"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 645
    const/4 v2, 0x1

    .line 681
    :goto_1
    return v2

    .line 617
    :cond_1
    const-string v3, "1"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 619
    const-string v3, "HealthServiceUpdation"

    const-string v4, "There is the application but it is not updatable."

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 621
    :cond_2
    const-string v3, "2"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 623
    const-string v3, "HealthServiceUpdation"

    const-string v4, "There is updates of the application. You can update it!"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 649
    :catch_0
    move-exception v0

    .line 653
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 655
    const-string v3, "HealthServiceUpdation"

    const-string v4, "com.sec.android.app.samsungapps is not present in device"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 677
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    const-string v3, "HealthServiceUpdation"

    const-string v4, "Not found Press Reader Update"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private isCSCExistFile()Z
    .locals 6

    .prologue
    .line 579
    const/4 v2, 0x0

    .line 581
    .local v2, "result":Z
    new-instance v1, Ljava/io/File;

    const-string v3, "/system/csc/sales_code.dat"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 585
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    .line 587
    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 589
    const-string v3, "HealthServiceUpdation"

    const-string v4, "CSC is not exist"

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 599
    :cond_0
    :goto_0
    return v2

    .line 593
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "HealthServiceUpdation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isCSCExistFile::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isLocalTestMode()Z
    .locals 2

    .prologue
    .line 688
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "mnt/sdcard/healthservice_update.xml"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 690
    .local v0, "localTestFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 692
    const/4 v1, 0x0

    .line 696
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isPD()Z
    .locals 2

    .prologue
    .line 702
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "mnt/sdcard/go_to_andromeda.test"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 704
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 706
    const/4 v1, 0x0

    .line 710
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static readModelCMCC()Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 718
    const-string v7, ""

    .line 720
    .local v7, "name":Ljava/lang/String;
    const-string v6, "/system/version"

    .line 722
    .local v6, "modelFile":Ljava/lang/String;
    const/4 v5, -0x1

    .line 724
    .local v5, "len":I
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 726
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    .line 728
    const/16 v9, 0x80

    new-array v0, v9, [B

    .line 730
    .local v0, "buffer":[B
    const/4 v3, 0x0

    .line 734
    .local v3, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v3    # "in":Ljava/io/InputStream;
    .local v4, "in":Ljava/io/InputStream;
    move-object v3, v4

    .line 744
    .end local v4    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :goto_0
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 746
    if-lez v5, :cond_0

    .line 748
    new-instance v8, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-direct {v8, v0, v9, v5}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v7    # "name":Ljava/lang/String;
    .local v8, "name":Ljava/lang/String;
    move-object v7, v8

    .line 758
    .end local v8    # "name":Ljava/lang/String;
    .restart local v7    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 764
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    :cond_1
    :goto_1
    return-object v7

    .line 736
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v1

    .line 738
    .local v1, "e":Ljava/io/FileNotFoundException;
    const-string v9, "HealthServiceUpdation"

    const-string v10, "Util::readModelCMCC::File not found"

    invoke-static {v9, v10}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 752
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 754
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    const-string v9, "HealthServiceUpdation"

    const-string v10, "Util::readModelCMCC::"

    invoke-static {v9, v10, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 758
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_1

    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v9
.end method


# virtual methods
.method public cancelCheckTask()V
    .locals 2

    .prologue
    .line 770
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    if-eqz v0, :cond_0

    .line 772
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mCheckTask:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$CheckTask;->cancel(Z)Z

    .line 776
    :cond_0
    return-void
.end method

.method public getCSC()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 493
    const-string v0, ""

    .line 495
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 499
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->isCSCExistFile()Z

    move-result v2

    if-eq v2, v3, :cond_0

    .line 527
    :goto_0
    return-object v0

    .line 505
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 507
    if-nez v1, :cond_1

    .line 509
    const-string v2, "HealthServiceUpdation"

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 515
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v3, :cond_2

    .line 517
    const-string v2, "HealthServiceUpdation"

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 523
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public registerAppUpdateListener(Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

    .prologue
    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUiListener:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

    .line 65
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUiListener:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

    .line 67
    return-void
.end method

.method public unregisterAppUpdateListener()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation;->mUiListener:Lcom/samsung/android/sdk/health/sensor/_private/_HealthServiceUpdation$OnAppUpdateListener;

    .line 75
    return-void
.end method
