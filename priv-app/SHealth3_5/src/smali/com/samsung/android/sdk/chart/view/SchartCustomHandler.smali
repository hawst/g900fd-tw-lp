.class public Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;
.super Landroid/widget/LinearLayout;
.source "SchartCustomHandler.java"


# instance fields
.field mXValue:D

.field mXValueString:Ljava/lang/String;

.field m_refView:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->initialize()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->initialize()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->initialize()V

    .line 47
    return-void
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->mXValue:D

    .line 52
    const-string v0, "00:00"

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->mXValueString:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->setBackgroundColor(I)V

    .line 55
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->m_refView:Ljava/util/Vector;

    .line 57
    return-void
.end method


# virtual methods
.method public addReferenceView(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V
    .locals 1
    .param p1, "refView"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->m_refView:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v4, 0x41200000    # 10.0f

    .line 97
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 99
    const/high16 v0, 0x41f00000    # 30.0f

    .line 101
    .local v0, "fontSize":F
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 102
    .local v1, "p":Landroid/graphics/Paint;
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 103
    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 106
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->mXValueString:Ljava/lang/String;

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v0, v3

    add-float/2addr v3, v4

    add-float/2addr v3, v0

    invoke-virtual {p1, v2, v4, v3, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 107
    return-void
.end method

.method public translateHandler(F)V
    .locals 5
    .param p1, "offset"    # F

    .prologue
    .line 80
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->m_refView:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 84
    return-void

    .line 82
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->m_refView:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->m_refView:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v2, v2, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, p1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetHandlerTranslate(JIF)V

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updateXValue(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2
    .param p1, "xValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SchartToolTipData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SchartToolTipData;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "time"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->mXValueString:Ljava/lang/String;

    .line 90
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartCustomHandler;->invalidate()V

    .line 91
    return-void
.end method
