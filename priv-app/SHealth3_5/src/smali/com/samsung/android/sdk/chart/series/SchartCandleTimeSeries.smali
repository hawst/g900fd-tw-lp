.class public Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;
.super Lcom/samsung/android/sdk/chart/series/SchartSeries;
.source "SchartCandleTimeSeries.java"


# instance fields
.field protected mDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartSeries;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    .line 19
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mSeriesDataType:I

    .line 20
    return-void
.end method


# virtual methods
.method public add(JDDDD)V
    .locals 11
    .param p1, "xValue"    # J
    .param p3, "yValueHigh"    # D
    .param p5, "yValueLow"    # D
    .param p7, "yValueOpen"    # D
    .param p9, "yValueClose"    # D

    .prologue
    .line 33
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;

    move-wide v1, p1

    move-wide v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;-><init>(JDDDD)V

    .line 35
    .local v0, "timeData":Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    return-void
.end method

.method public add(Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;)V
    .locals 1
    .param p1, "timeData"    # Lcom/samsung/android/sdk/chart/series/SchartTimeCandleData;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    return-void
.end method

.method public getAllData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getX(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 53
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 55
    :cond_0
    const-wide/16 v0, -0x1

    .line 57
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getY(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 66
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 68
    :cond_0
    const-wide/16 v0, 0x1

    .line 70
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    goto :goto_0
.end method

.method public setAllData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->mDatas:Ljava/util/List;

    .line 95
    return-void
.end method
