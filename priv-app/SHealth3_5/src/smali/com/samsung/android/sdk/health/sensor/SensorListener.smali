.class public interface abstract Lcom/samsung/android/sdk/health/sensor/SensorListener;
.super Ljava/lang/Object;
.source "SensorListener.java"


# virtual methods
.method public abstract onConnected(I)V
.end method

.method public abstract onDisconnected(I)V
.end method

.method public abstract onReceived(ILcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;Landroid/os/Bundle;)V
.end method

.method public abstract onReceived(I[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;[Landroid/os/Bundle;)V
.end method

.method public abstract onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
.end method

.method public abstract onStarted(II)V
.end method

.method public abstract onStateChanged(I)V
.end method

.method public abstract onStopped(II)V
.end method
