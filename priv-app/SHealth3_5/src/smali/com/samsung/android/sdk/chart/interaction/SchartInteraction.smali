.class public Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;
.super Ljava/lang/Object;
.source "SchartInteraction.java"


# instance fields
.field protected mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/InteractionProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    .line 21
    return-void
.end method


# virtual methods
.method public getInteractionProperty()Lcom/sec/dmc/sic/android/property/InteractionProperty;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    return-object v0
.end method

.method public setDataZoomInRate(F)V
    .locals 1
    .param p1, "rate"    # F

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomInRate(F)V

    .line 86
    return-void
.end method

.method public setDataZoomInteractionEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomEnable(Z)V

    .line 47
    return-void
.end method

.method public setDataZoomIntervalLimit(II)V
    .locals 1
    .param p1, "zoomInLimit"    # I
    .param p2, "zoomOutLimit"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomInLimitInterval(I)V

    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomOutLimitInterval(I)V

    .line 125
    return-void
.end method

.method public setDataZoomIntervalStep(II)V
    .locals 1
    .param p1, "zoomInStep"    # I
    .param p2, "zoomOutStep"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomInIntervalStep(I)V

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomOutIntervalStep(I)V

    .line 114
    return-void
.end method

.method public setDataZoomLimitDataCount(II)V
    .locals 1
    .param p1, "minCount"    # I
    .param p2, "maxCount"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setMaxCount(I)V

    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setMinCount(I)V

    .line 101
    return-void
.end method

.method public setDataZoomOutRate(F)V
    .locals 1
    .param p1, "rate"    # F

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setDataZoomOutRate(F)V

    .line 82
    return-void
.end method

.method public setInteractionEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setInteractionEnable(Z)V

    .line 77
    return-void
.end method

.method public setPatialZoomInteractionEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setPartialZoomEnable(Z)V

    .line 57
    return-void
.end method

.method public setScaleZoomInRate(F)V
    .locals 1
    .param p1, "rate"    # F

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScaleZoomInRate(F)V

    .line 95
    return-void
.end method

.method public setScaleZoomInteractionEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScaleZoomEnable(Z)V

    .line 38
    return-void
.end method

.method public setScrollInteractionEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->mInteractionProperty:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScrollEnable(Z)V

    .line 67
    return-void
.end method
