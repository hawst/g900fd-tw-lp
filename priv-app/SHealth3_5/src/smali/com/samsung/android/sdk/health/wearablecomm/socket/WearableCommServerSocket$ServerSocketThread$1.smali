.class Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;
.super Landroid/os/Handler;
.source "WearableCommServerSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 162
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "server handleMessage(Message msg)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 376
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 169
    :pswitch_1
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    new-instance v6, Landroid/net/LocalServerSocket;

    const-string v7, "HEALTH_SERVICE_SOCKET"

    invoke-direct {v6, v7}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$202(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Landroid/net/LocalServerSocket;)Landroid/net/LocalServerSocket;

    .line 170
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    const/4 v6, 0x1

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->acceptWaiting:Z
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$302(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Z)Z

    .line 171
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    iget-object v6, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;
    invoke-static {v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalServerSocket;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/LocalServerSocket;->accept()Landroid/net/LocalSocket;

    move-result-object v6

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$402(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Landroid/net/LocalSocket;)Landroid/net/LocalSocket;

    .line 172
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalSocket;

    move-result-object v5

    const/high16 v6, 0x10000

    invoke-virtual {v5, v6}, Landroid/net/LocalSocket;->setReceiveBufferSize(I)V

    .line 175
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 177
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    invoke-interface {v5}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionSuccess()V

    .line 178
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    new-instance v6, Ljava/io/BufferedInputStream;

    iget-object v7, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v7, v7, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalSocket;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$702(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Ljava/io/BufferedInputStream;)Ljava/io/BufferedInputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->acceptWaiting:Z
    invoke-static {v5, v9}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$302(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Z)Z

    goto :goto_0

    .line 182
    :catch_0
    move-exception v0

    .line 184
    .local v0, "ex":Ljava/lang/Exception;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Exception ocurred while creating socket!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 187
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    invoke-interface {v5, v8}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 189
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 196
    .end local v0    # "ex":Ljava/lang/Exception;
    :pswitch_2
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "mLocalClientSocket : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v7, v7, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalSocket;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "localClientSocketInputStream : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v7, v7, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v7}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalSocket;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 201
    :try_start_1
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "[RECEIVE_CERTIFICATION_DATA] data received"

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    const/16 v6, 0x8

    new-array v6, v6, [B

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$802(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;[B)[B

    .line 203
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v3

    .line 205
    .local v3, "readSize":I
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[RECEIVE_CERTIFICATION_DATA] data received = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/lang/String;

    iget-object v8, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v8, v8, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v8}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v8

    const-string v9, "UTF-8"

    invoke-direct {v7, v8, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[RECEIVE_CERTIFICATION_DATA] data received readSize = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    if-ne v3, v10, :cond_4

    .line 209
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 211
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;->byteToLong([B)J

    move-result-wide v1

    .line 212
    .local v1, "longTemp":J
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v1, v2, v6}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onCertificationDataReceived(JZ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 225
    .end local v1    # "longTemp":J
    .end local v3    # "readSize":I
    :catch_1
    move-exception v0

    .line 227
    .local v0, "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Exception ocurred while writing certification data to socket!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 230
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 232
    :cond_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 218
    .end local v0    # "ex":Ljava/io/IOException;
    .restart local v3    # "readSize":I
    :cond_4
    :try_start_2
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 220
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommUtil;->byteToLong([B)J

    move-result-wide v1

    .line 221
    .restart local v1    # "longTemp":J
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v1, v2, v6}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onCertificationDataReceived(JZ)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 265
    .end local v1    # "longTemp":J
    .end local v3    # "readSize":I
    :pswitch_3
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalClientSocket:Landroid/net/LocalSocket;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$400(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalSocket;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 269
    :cond_5
    :goto_2
    :try_start_3
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isClosedCalled:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$900(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 271
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    const/high16 v6, 0x10000

    new-array v6, v6, [B

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v5, v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$802(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;[B)[B

    .line 272
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v3

    .line 273
    .restart local v3    # "readSize":I
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[RECEIVE_DATA] data received readSize = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-direct {v4, v5, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 275
    .local v4, "tempStr":Ljava/lang/String;
    const-string v5, "END_OF_DATA"

    invoke-virtual {v5, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v5, -0x1

    if-ne v3, v5, :cond_8

    .line 277
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 279
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v5, v6, v3, v7}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onDataReceived([BIZ)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 292
    .end local v3    # "readSize":I
    .end local v4    # "tempStr":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 294
    .restart local v0    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Exception ocurred while writing data to socket!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 297
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    invoke-interface {v5, v8}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 299
    :cond_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 285
    .end local v0    # "ex":Ljava/io/IOException;
    .restart local v3    # "readSize":I
    .restart local v4    # "tempStr":Ljava/lang/String;
    :cond_8
    :try_start_4
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$600(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 287
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v6, v6, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mReceivedDataBuffer:[B
    invoke-static {v6}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$800(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v5, v6, v3, v7}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onDataReceived([BIZ)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_2

    .line 330
    .end local v3    # "readSize":I
    .end local v4    # "tempStr":Ljava/lang/String;
    :pswitch_4
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$1000(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 332
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$1000(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 334
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$1000(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x43fa

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 335
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$1000(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->getServerSocketMsgHandler()Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x43fb

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 340
    :cond_9
    :try_start_5
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 341
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->localClientSocketInputStream:Ljava/io/BufferedInputStream;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$700(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Ljava/io/BufferedInputStream;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 355
    :cond_a
    :goto_3
    :try_start_6
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalServerSocket;

    move-result-object v5

    if-eqz v5, :cond_b

    .line 356
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mLocalServerSocket:Landroid/net/LocalServerSocket;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$200(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Landroid/net/LocalServerSocket;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/LocalServerSocket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 368
    :cond_b
    :goto_4
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$1000(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->destroyLooper()V

    .line 369
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    invoke-static {v5, v11}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$1002(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    .line 370
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->isDummyClient:Z
    invoke-static {v5, v9}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$602(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Z)Z

    .line 371
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # setter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5, v11}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$502(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    goto/16 :goto_0

    .line 343
    :catch_3
    move-exception v0

    .line 345
    .restart local v0    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Exception ocurred while closing inputstream!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_c

    .line 348
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    invoke-interface {v5, v8}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 350
    :cond_c
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 358
    .end local v0    # "ex":Ljava/io/IOException;
    :catch_4
    move-exception v0

    .line 360
    .restart local v0    # "ex":Ljava/io/IOException;
    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$100()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Exception ocurred while closing server socket!!"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    if-eqz v5, :cond_d

    .line 363
    iget-object v5, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;->this$1:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    iget-object v5, v5, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->mWearableDataListener:Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;
    invoke-static {v5}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$500(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;

    move-result-object v5

    invoke-interface {v5, v8}, Lcom/samsung/android/sdk/health/wearablecomm/data/WearableDataListener;->onConnectionFailed(I)V

    .line 365
    :cond_d
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 163
    :pswitch_data_0
    .packed-switch 0x43f8
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
