.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$ChallengesInfoColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ChallengesInfoColumns"
.end annotation


# static fields
.field public static final CHALLENGES_TYPE:Ljava/lang/String; = "challenges_type"

.field public static final CHALLENGE_STATE:Ljava/lang/String; = "challenge_state"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final GOAL:Ljava/lang/String; = "goal"

.field public static final GOAL_TYPE:Ljava/lang/String; = "goal_type"

.field public static final INITIAL_VALUE:Ljava/lang/String; = "initial_value"

.field public static final SCHEDULE_DAYOFWEEK:Ljava/lang/String; = "schedule_dayofweek"

.field public static final SCHEDULE_ID:Ljava/lang/String; = "schedule_id"

.field public static final SCHEDULE_TIME:Ljava/lang/String; = "schedule_time"

.field public static final SERVER_KEY:Ljava/lang/String; = "server_key"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TOTAL:Ljava/lang/String; = "total"
