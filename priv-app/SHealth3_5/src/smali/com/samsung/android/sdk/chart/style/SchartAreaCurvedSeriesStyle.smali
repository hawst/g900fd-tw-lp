.class public Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;
.super Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;
.source "SchartAreaCurvedSeriesStyle.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/style/SchartCurvedSeriesStyle;-><init>()V

    .line 10
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mSeriesType:I

    .line 12
    return-void
.end method


# virtual methods
.method public getAreaColor(I)I
    .locals 4
    .param p1, "color"    # I

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getAreaFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 33
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getAreaFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 34
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getAreaFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 35
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getAreaFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 32
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public setAreaColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setAreaFillColor(IIII)V

    .line 22
    return-void
.end method

.method public setAreaGradientColors([I[F)V
    .locals 4
    .param p1, "colors"    # [I
    .param p2, "values"    # [F

    .prologue
    .line 45
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 47
    array-length v0, p1

    .line 48
    .local v0, "colorsSize":I
    array-length v1, p2

    .line 50
    .local v1, "valuesSize":I
    if-eq v0, v1, :cond_0

    .line 52
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "gradient colors set error"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 56
    .end local v0    # "colorsSize":I
    .end local v1    # "valuesSize":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v2, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setAreaGradientdColors([I)V

    .line 57
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartAreaCurvedSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v2, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setAreaGradientValues([F)V

    .line 58
    return-void
.end method
