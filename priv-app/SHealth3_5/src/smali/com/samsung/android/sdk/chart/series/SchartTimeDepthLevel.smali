.class public Lcom/samsung/android/sdk/chart/series/SchartTimeDepthLevel;
.super Ljava/lang/Object;
.source "SchartTimeDepthLevel.java"


# static fields
.field public static final DAY:I = 0x2

.field public static final DAYOFWEEK:I = 0x3

.field public static final DEPTHLEVELCOUNT:I = 0x7

.field public static final HOUR:I = 0x1

.field public static final MINUTE:I = 0x0

.field public static final MONTH:I = 0x5

.field public static final WEEK:I = 0x4

.field public static final YEAR:I = 0x6


# instance fields
.field private mCurrentLevel:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeDepthLevel;->mCurrentLevel:I

    .line 26
    return-void
.end method


# virtual methods
.method public getCurrentLevel()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeDepthLevel;->mCurrentLevel:I

    return v0
.end method

.method public setCurrentLevel(I)V
    .locals 0
    .param p1, "timeLevel"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeDepthLevel;->mCurrentLevel:I

    .line 40
    return-void
.end method
