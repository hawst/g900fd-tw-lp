.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ResponseReceivedCallBack"
.end annotation


# instance fields
.field command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

.field response:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
    .locals 1
    .param p2, "command"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;
    .param p3, "response"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .prologue
    const/4 v0, 0x0

    .line 509
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 506
    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .line 507
    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->response:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .line 510
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    .line 511
    iput-object p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->response:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    .line 512
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 516
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_RESPONSE_RECEIVED_CALLED:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 517
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener;)Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->command:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$WrappedSensorServiceEventListener$1ResponseReceivedCallBack;->response:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;->onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V

    .line 519
    return-void
.end method
