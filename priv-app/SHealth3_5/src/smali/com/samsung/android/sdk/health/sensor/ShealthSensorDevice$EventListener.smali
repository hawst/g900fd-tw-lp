.class public interface abstract Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$EventListener;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EventListener"
.end annotation


# static fields
.field public static final ERROR_FAILURE:I = 0x1

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_WRONG_REQUEST:I = 0x2

.field public static final STATE_BUSY:I = 0x7d2

.field public static final STATE_DEVICE_INFORMATION_UPDATE:I = 0x7d8

.field public static final STATE_DISCONNECTED:I = 0x7d7

.field public static final STATE_FREE:I = 0x7d3

.field public static final STATE_IN_BOUNDARY:I = 0x7d6

.field public static final STATE_OFF:I = 0x7d0

.field public static final STATE_OUT_OF_BOUNDARY:I = 0x7d5

.field public static final STATE_PAUSED:I = 0x7d4

.field public static final STATE_READY:I = 0x7d1


# virtual methods
.method public abstract onJoined(I)V
.end method

.method public abstract onLeft(I)V
.end method

.method public abstract onResponseReceived(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Command;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Response;)V
.end method

.method public abstract onStateChanged(I)V
.end method
