.class public Lcom/samsung/android/sdk/chart/view/SchartDonutChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;
.source "SchartDonutChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;-><init>(Landroid/content/Context;)V

    .line 19
    const/16 v0, 0x8

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setChartType(I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    const/16 v0, 0x8

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setChartType(I)V

    .line 27
    return-void
.end method
