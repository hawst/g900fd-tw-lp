.class public Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;
.source "SchartNonXYChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;-><init>(Landroid/content/Context;)V

    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScrollEnable(Z)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScrollEnable(Z)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mInteraction:Lcom/sec/dmc/sic/android/property/InteractionProperty;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/InteractionProperty;->setScrollEnable(Z)V

    .line 46
    return-void
.end method


# virtual methods
.method public getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    check-cast v0, Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;

    return-object v0
.end method

.method public setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V
    .locals 13
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    .prologue
    .line 55
    iget-object v11, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->processProperty:Ljava/util/Vector;

    monitor-enter v11

    .line 56
    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    .line 58
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;

    move-object v6, v0

    .line 60
    .local v6, "nonXYChartStyle":Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;->getChartProperty()Lcom/sec/dmc/sic/android/property/ChartProperty;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    .line 61
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 63
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-lt v5, v10, :cond_0

    .line 68
    const/4 v5, 0x0

    :goto_1
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-lt v5, v10, :cond_1

    .line 73
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setEnable(Z)V

    .line 74
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->setVisible(Z)V

    .line 76
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 77
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 80
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;->getSeriesStyleSize()I

    move-result v9

    .line 82
    .local v9, "seriesStyleCnt":I
    const/4 v5, 0x0

    :goto_2
    if-lt v5, v9, :cond_2

    .line 123
    invoke-virtual {v6}, Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;->getLegendProperty()Lcom/sec/dmc/sic/android/property/LegendProperty;

    move-result-object v10

    iput-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    .line 124
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 55
    monitor-exit v11

    .line 126
    return-void

    .line 64
    .end local v9    # "seriesStyleCnt":I
    :cond_0
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisible(Z)V

    .line 65
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mXAxisVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 63
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 69
    :cond_1
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/AxisProperty;

    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setVisible(Z)V

    .line 70
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mYAxisVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 68
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 83
    .restart local v9    # "seriesStyleCnt":I
    :cond_2
    invoke-virtual {v6, v5}, Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;->getSeriesStyle(I)Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;

    .line 85
    .local v7, "nonXYSeriesStyle":Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;
    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mChartType:I

    const/4 v12, 0x5

    if-eq v10, v12, :cond_3

    iget v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mChartType:I

    const/16 v12, 0x9

    if-eq v10, v12, :cond_3

    .line 87
    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;->getSeriesType()I

    move-result v10

    iget v12, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mChartType:I

    if-eq v10, v12, :cond_3

    .line 88
    new-instance v10, Ljava/lang/ClassCastException;

    const-string v12, "SeriesStyle and chartType mismatch"

    invoke-direct {v10, v12}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 55
    .end local v5    # "i":I
    .end local v6    # "nonXYChartStyle":Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;
    .end local v7    # "nonXYSeriesStyle":Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;
    .end local v9    # "seriesStyleCnt":I
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 91
    .restart local v5    # "i":I
    .restart local v6    # "nonXYChartStyle":Lcom/samsung/android/sdk/chart/style/SchartNonXYChartStyle;
    .restart local v7    # "nonXYSeriesStyle":Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;
    .restart local v9    # "seriesStyleCnt":I
    :cond_3
    :try_start_1
    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;->getSeriesProperty()Lcom/sec/dmc/sic/android/property/SeriesProperty;

    move-result-object v8

    .line 92
    .local v8, "seriesProperty":Lcom/sec/dmc/sic/android/property/SeriesProperty;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;->getGraphProperty()Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-result-object v4

    .line 93
    .local v4, "graphProperty":Lcom/sec/dmc/sic/android/property/GraphProperty;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;->getChartValueProperty()Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    move-result-object v3

    .line 94
    .local v3, "chartValueProeprty":Lcom/sec/dmc/sic/android/property/GraphValueProperty;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;->getEventIconProperty()Lcom/sec/dmc/sic/android/property/EventIconProperty;

    move-result-object v1

    .line 95
    .local v1, "chartEventIconProeprty":Lcom/sec/dmc/sic/android/property/EventIconProperty;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;->getGoalEventIconProperty()Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    move-result-object v2

    .line 97
    .local v2, "chartGoalEventIconProeprty":Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mSeriesVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v10}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->getSeriesID()I

    move-result v10

    invoke-virtual {v8, v10}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setSeriesID(I)V

    .line 98
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mSeriesVector:Ljava/util/Vector;

    invoke-virtual {v10, v5, v8}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGraphVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v10}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getGraphID()I

    move-result v10

    invoke-virtual {v4, v10}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setGraphID(I)V

    .line 101
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGraphVector:Ljava/util/Vector;

    invoke-virtual {v10, v5, v4}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGraphValueVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v10}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getSeriesID()I

    move-result v10

    invoke-virtual {v3, v10}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setSeriesID(I)V

    .line 104
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGraphValueVector:Ljava/util/Vector;

    invoke-virtual {v10, v5, v3}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v10}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getSeriesID()I

    move-result v10

    invoke-virtual {v1, v10}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setSeriesID(I)V

    .line 107
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v5, v1}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGoalEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v10}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->getSeriesID()I

    move-result v10

    invoke-virtual {v2, v10}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setSeriesID(I)V

    .line 110
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGoalEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v5, v2}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mSeriesVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 113
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mNormalRangeVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 114
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mValueMarkingVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 115
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGoalLineVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 116
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGraphVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 117
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGraphValueVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 118
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 119
    iget-object v10, p0, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->mGoalEventIconVector:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/dmc/sic/android/property/BaseProperty;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/chart/view/SchartNonXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 82
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2
.end method
