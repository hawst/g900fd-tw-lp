.class public Lcom/samsung/android/sdk/chart/series/SchartTimeData;
.super Ljava/lang/Object;
.source "SchartTimeData.java"


# instance fields
.field protected mMultiValues:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field protected mTime:J

.field protected mValue:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    .line 16
    return-void
.end method

.method public constructor <init>(JD)V
    .locals 1
    .param p1, "time"    # J
    .param p3, "value"    # D

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    .line 20
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mTime:J

    .line 21
    iput-wide p3, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    .line 22
    return-void
.end method


# virtual methods
.method public add(JD)V
    .locals 0
    .param p1, "time"    # J
    .param p3, "value"    # D

    .prologue
    .line 25
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mTime:J

    .line 26
    iput-wide p3, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    .line 27
    return-void
.end method

.method public add(JDLjava/util/LinkedList;)V
    .locals 0
    .param p1, "time"    # J
    .param p3, "value"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JD",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p5, "multiValues":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/Double;>;"
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mTime:J

    .line 31
    iput-wide p3, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    .line 32
    iput-object p5, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    .line 33
    return-void
.end method

.method public addMultiValue(D)V
    .locals 2
    .param p1, "value"    # D

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public getMultiValue(I)D
    .locals 2
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IndexOutOfBoundsException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 45
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getMultiValues()Ljava/util/LinkedList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mTime:J

    return-wide v0
.end method

.method public getValue()D
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    return-wide v0
.end method

.method public getValueCount()I
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public setTime(J)V
    .locals 0
    .param p1, "time"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mTime:J

    .line 53
    return-void
.end method

.method public setValue(D)V
    .locals 0
    .param p1, "value"    # D

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    .line 57
    return-void
.end method

.method public setValue(DZ)V
    .locals 4
    .param p1, "value"    # D
    .param p3, "isDisconnect"    # Z

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    .line 61
    if-eqz p3, :cond_0

    const-wide/16 v0, 0x0

    .line 62
    .local v0, "disconnect":D
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mMultiValues:Ljava/util/LinkedList;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 63
    return-void

    .line 61
    .end local v0    # "disconnect":D
    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0
.end method
