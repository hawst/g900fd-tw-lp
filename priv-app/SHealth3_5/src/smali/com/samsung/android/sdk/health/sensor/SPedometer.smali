.class public Lcom/samsung/android/sdk/health/sensor/SPedometer;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SPedometer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SPedometer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public calories:F

.field public distance:F

.field public runStep:I

.field public speed:F

.field public time:J

.field public totalStep:I

.field public updownStep:I

.field public walkStep:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SPedometer$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SPedometer$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 67
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    const v2, 0x7fffffff

    .line 24
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 8
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->time:J

    .line 10
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->distance:F

    .line 12
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->calories:F

    .line 14
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->speed:F

    .line 16
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->totalStep:I

    .line 18
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->runStep:I

    .line 20
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->walkStep:I

    .line 22
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->updownStep:I

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const v3, 0x7f7fffff    # Float.MAX_VALUE

    const v2, 0x7fffffff

    .line 28
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 8
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->time:J

    .line 10
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->distance:F

    .line 12
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->calories:F

    .line 14
    iput v3, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->speed:F

    .line 16
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->totalStep:I

    .line 18
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->runStep:I

    .line 20
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->walkStep:I

    .line 22
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->updownStep:I

    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SPedometer;->readFromParcel(Landroid/os/Parcel;)V

    .line 31
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->time:J

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->distance:F

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->calories:F

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->speed:F

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->totalStep:I

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->runStep:I

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->walkStep:I

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->updownStep:I

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->extra:Landroid/os/Bundle;

    .line 80
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[PEDOMETER] \ntime ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->time:J

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", distance(m) ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->distance:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 86
    const-string v1, ", cal(kCal)= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->calories:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", total ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 87
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->totalStep:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", speed(m/s) = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->speed:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", runStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->runStep:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 88
    const-string v1, ", walkStep="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->walkStep:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", updownStep= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->updownStep:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 43
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->distance:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 44
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->calories:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 45
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->speed:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 46
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->totalStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 47
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->runStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->walkStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->updownStep:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SPedometer;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 51
    return-void
.end method
