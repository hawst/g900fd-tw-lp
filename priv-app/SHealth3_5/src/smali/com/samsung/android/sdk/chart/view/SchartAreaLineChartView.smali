.class public Lcom/samsung/android/sdk/chart/view/SchartAreaLineChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartXYChartView;
.source "SchartAreaLineChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;)V

    .line 39
    const/16 v0, 0xa

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartAreaLineChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    const/16 v0, 0xa

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 52
    return-void
.end method


# virtual methods
.method protected initSeriesAttribute(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->initSeriesAttribute(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method
