.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$BuddyInfoColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BuddyInfoColumns"
.end annotation


# static fields
.field public static final FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phone_number"

.field public static final SERVER_KEY:Ljava/lang/String; = "server_key"
