.class public Lcom/samsung/android/sdk/health/sensor/SCoachingResult;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SCoachingResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SCoachingResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public distance:D

.field public endTime:J

.field public eteMaxMET:I

.field public eteResourceRecovery:I

.field public eteTrainingLoadPeak:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SCoachingResult$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 83
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    .line 28
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 10
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    .line 12
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->distance:D

    .line 17
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteTrainingLoadPeak:I

    .line 24
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteMaxMET:I

    .line 26
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteResourceRecovery:I

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const v2, 0x7fffffff

    .line 32
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 10
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    .line 12
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->distance:D

    .line 17
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteTrainingLoadPeak:I

    .line 24
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteMaxMET:I

    .line 26
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteResourceRecovery:I

    .line 34
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->readFromParcel(Landroid/os/Parcel;)V

    .line 35
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->distance:D

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteTrainingLoadPeak:I

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteMaxMET:I

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteResourceRecovery:I

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->extra:Landroid/os/Bundle;

    .line 70
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[COACHINGRESULT]\n endTime ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 58
    const-string v1, ", distance = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->distance:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eteTrainingLoadPeak ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteTrainingLoadPeak:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 59
    const-string v1, ", eteMaxMET = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteMaxMET:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", eteResourceRecovery ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteResourceRecovery:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->endTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 47
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->distance:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 48
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteTrainingLoadPeak:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteMaxMET:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->eteResourceRecovery:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingResult;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method
