.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$SpeedUnit;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SpeedUnit"
.end annotation


# static fields
.field public static final KMPH:I = 0x30d41

.field public static final MPH:I = 0x30d42

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field
