.class public Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;
.source "SchartStringMultiChartView.java"


# instance fields
.field private nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;->setDataType(I)V

    .line 41
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;->createChart()V

    .line 43
    new-instance v0, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-super {p0, v7}, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;->setDataType(I)V

    .line 56
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;->createChart()V

    .line 58
    const/4 v4, 0x1

    .line 59
    .local v4, "validTypeArray":Z
    const/4 v3, 0x0

    .line 62
    .local v3, "typedArray1":Landroid/content/res/TypedArray;
    :try_start_0
    sget-object v5, Lcom/sec/dmc/sic/R$styleable;->SchartMultiChartView:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 67
    :goto_0
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 69
    if-eqz v4, :cond_0

    .line 71
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v3, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 73
    .local v0, "chartTypeArray":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v5, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mSeriesNum:I

    if-lt v2, v5, :cond_1

    .line 84
    .end local v0    # "chartTypeArray":[I
    .end local v2    # "i":I
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 86
    new-instance v5, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v5}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 87
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v6, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mChartHandle:J

    invoke-virtual {v5, v6, v7}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 88
    return-void

    .line 63
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v4, 0x0

    goto :goto_0

    .line 75
    .end local v1    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v0    # "chartTypeArray":[I
    .restart local v2    # "i":I
    :cond_1
    aget v5, v0, v2

    if-ne v5, v7, :cond_2

    .line 76
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->createSeries(I)V

    .line 73
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 78
    :cond_2
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->createSeries(I)V

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 99
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;-><init>(Landroid/content/Context;)V

    .line 102
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 113
    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfXAxis()I

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfYAxis()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->addAxis(II)V

    .line 115
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 117
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 131
    return-void

    .line 103
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    .line 105
    .local v1, "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 106
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->createSeries(I)V

    .line 102
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 107
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 108
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->createSeries(I)V

    goto :goto_2

    .line 119
    .end local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_3
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    .line 121
    .restart local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v4, :cond_5

    .line 123
    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;

    .end local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v4, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->addDataBack(IILjava/util/List;)V

    .line 117
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 125
    .restart local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v5, :cond_4

    .line 127
    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;

    .end local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartCandleStringSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v5, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->addDataBack(IILjava/util/List;)V

    goto :goto_3
.end method


# virtual methods
.method public addDataBack(IILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 193
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addData(IILjava/util/List;)V

    .line 197
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public addDataFront(IILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 212
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addDataFront(IILjava/util/List;)V

    .line 216
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 244
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->clearData()V

    goto :goto_0
.end method

.method public prepareChangeData(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 230
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->prepareChangeData(IILjava/util/List;)V

    goto :goto_0
.end method

.method public setStartIndex(I)V
    .locals 2
    .param p1, "startIndex"    # I

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 159
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartIndex(I)V

    .line 163
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartValidCount(I)V
    .locals 2
    .param p1, "validCount"    # I

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 175
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartIndex(I)V

    .line 179
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartVisual(II)V
    .locals 2
    .param p1, "startIndex"    # I
    .param p2, "validCount"    # I

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 143
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartVisual(II)V

    .line 147
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringMultiChartView;->notifyRenderThread()V

    goto :goto_0
.end method
