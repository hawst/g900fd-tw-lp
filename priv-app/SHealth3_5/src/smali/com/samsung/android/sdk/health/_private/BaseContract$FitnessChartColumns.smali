.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$FitnessChartColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FitnessChartColumns"
.end annotation


# static fields
.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final FEEDBACK_PHRASE_ARGUMENT:Ljava/lang/String; = "feedback_phrase_argument"

.field public static final FEEDBACK_PHRASE_NUMBER:Ljava/lang/String; = "feedback_phrase_number"

.field public static final FITNESS_CLASS:Ljava/lang/String; = "fitness_class"

.field public static final TIME:Ljava/lang/String; = "time"
