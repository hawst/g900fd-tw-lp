.class public Lcom/samsung/android/sdk/motion/SmotionActivity;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;,
        Lcom/samsung/android/sdk/motion/SmotionActivity$Info;,
        Lcom/samsung/android/sdk/motion/SmotionActivity$a;,
        Lcom/samsung/android/sdk/motion/SmotionActivity$b;
    }
.end annotation


# static fields
.field private static h:Z

.field private static n:Lcom/samsung/android/sdk/motion/Smotion;


# instance fields
.field private a:Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

.field private b:I

.field private c:J

.field private d:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

.field private e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

.field private f:Landroid/hardware/scontext/SContextListener;

.field private g:Landroid/os/PowerManager;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/util/Timer;

.field private m:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/motion/SmotionActivity;->n:Lcom/samsung/android/sdk/motion/Smotion;

    return-void
.end method

.method public constructor <init>(Landroid/os/Looper;Lcom/samsung/android/sdk/motion/Smotion;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->d:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->i:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->j:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->k:Z

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->l:Ljava/util/Timer;

    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->m:J

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionActivity : Looper is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SmotionActivity : Smotion is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivity : Smotion.initialize() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity : Smotion.initialize() is not successful. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    sput-object p2, Lcom/samsung/android/sdk/motion/SmotionActivity;->n:Lcom/samsung/android/sdk/motion/Smotion;

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/sdk/motion/SmotionActivity;->h:Z

    iget-object v0, p2, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    const-string/jumbo v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->g:Landroid/os/PowerManager;

    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionActivity;->h:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    return-void
.end method

.method static synthetic a()Lcom/samsung/android/sdk/motion/Smotion;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/motion/SmotionActivity;->n:Lcom/samsung/android/sdk/motion/Smotion;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivity;)Lcom/samsung/android/sdk/motion/SmotionActivity$Info;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->a:Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    return-object v0
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivity;JII)Lcom/samsung/android/sdk/motion/SmotionActivity$Info;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    invoke-direct {v0}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;-><init>()V

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;J)V

    packed-switch p3, :pswitch_data_0

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    :goto_0
    packed-switch p4, :pswitch_data_1

    :goto_1
    return-object v0

    :pswitch_0
    invoke-static {v0, v3}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    goto :goto_0

    :pswitch_1
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->a(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    goto :goto_0

    :pswitch_4
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->b(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    goto :goto_1

    :pswitch_5
    invoke-static {v0, v3}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->b(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    goto :goto_1

    :pswitch_6
    invoke-static {v0, v4}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;->b(Lcom/samsung/android/sdk/motion/SmotionActivity$Info;I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivity;Lcom/samsung/android/sdk/motion/SmotionActivity$Info;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->a:Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionActivity;Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->k:Z

    return-void
.end method

.method static synthetic b(Lcom/samsung/android/sdk/motion/SmotionActivity;Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->i:Z

    return-void
.end method

.method static synthetic b()Z
    .locals 1

    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionActivity;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/samsung/android/sdk/motion/SmotionActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->i:Z

    return v0
.end method

.method static synthetic c(Lcom/samsung/android/sdk/motion/SmotionActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->c:J

    return-wide v0
.end method

.method private c()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity : start() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->requestToUpdate(Landroid/hardware/scontext/SContextListener;I)V

    return-void
.end method

.method static synthetic c(Lcom/samsung/android/sdk/motion/SmotionActivity;Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->j:Z

    return-void
.end method

.method private d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->k:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->l:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->l:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->l:Ljava/util/Timer;

    :cond_0
    return-void
.end method


# virtual methods
.method public getInfo()Lcom/samsung/android/sdk/motion/SmotionActivity$Info;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->d:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity : start() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->i:Z

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->g:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_4

    iput-boolean v4, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->j:Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/motion/SmotionActivity;->c()V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->l:Ljava/util/Timer;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->l:Ljava/util/Timer;

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->l:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/motion/SmotionActivity$b;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/motion/SmotionActivity$b;-><init>(Lcom/samsung/android/sdk/motion/SmotionActivity;B)V

    iget-wide v2, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->m:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->j:Z

    if-eqz v0, :cond_5

    :goto_1
    iput-boolean v4, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->j:Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/motion/SmotionActivity;->d()V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->a:Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->k:Z

    if-eqz v0, :cond_3

    const-string v0, "SmotionActivity"

    const-string v1, "SmotionActivity : getInfo() Time out !!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public start(ILcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;)V
    .locals 5

    const/16 v4, 0x1a

    const/16 v3, 0x19

    const/4 v2, 0x2

    if-ltz p1, :cond_0

    if-le p1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivity : Mode value is wrong. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionActivity : ChangeListener is null. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-boolean v0, Lcom/samsung/android/sdk/motion/SmotionActivity;->h:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->d:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    if-nez v0, :cond_5

    iput-object p2, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->d:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    invoke-direct {v0}, Lcom/samsung/android/sdk/motion/SmotionActivity$Info;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->a:Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    if-nez p2, :cond_6

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/motion/SmotionActivity;->c()V

    :cond_4
    :goto_1
    return-void

    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity : ChangeListener is already registered. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Lcom/samsung/android/sdk/motion/a;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/motion/a;-><init>(Lcom/samsung/android/sdk/motion/SmotionActivity;Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;)V

    goto :goto_0

    :cond_7
    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    if-ne v0, v2, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->c:J

    goto :goto_1

    :cond_8
    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/motion/SmotionActivity;->c()V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    goto :goto_1
.end method

.method public stop()V
    .locals 5

    const/16 v4, 0x1a

    const/16 v3, 0x19

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->d:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionActivity : start() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/motion/SmotionActivity;->d()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    iput-object v2, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->d:Lcom/samsung/android/sdk/motion/SmotionActivity$ChangeListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->a:Lcom/samsung/android/sdk/motion/SmotionActivity$Info;

    iput-object v2, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    return-void

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->b:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->e:Lcom/samsung/android/sdk/motion/SmotionActivity$a;

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/SmotionActivity;->f:Landroid/hardware/scontext/SContextListener;

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/sdk/motion/SmotionActivity$a;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    goto :goto_0
.end method
