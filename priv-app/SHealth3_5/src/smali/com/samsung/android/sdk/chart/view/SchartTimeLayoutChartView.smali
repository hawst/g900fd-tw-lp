.class public Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;
.source "SchartTimeLayoutChartView.java"


# instance fields
.field private timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v0, 0x2

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;->setDataType(I)V

    .line 41
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;->createChart()V

    .line 44
    new-instance v0, Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setChartHandle(J)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    invoke-super {p0, v8}, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;->setDataType(I)V

    .line 58
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;->createChart()V

    .line 60
    const/4 v4, 0x1

    .line 61
    .local v4, "validTypeArray":Z
    const/4 v3, 0x0

    .line 64
    .local v3, "typedArray1":Landroid/content/res/TypedArray;
    :try_start_0
    sget-object v5, Lcom/sec/dmc/sic/R$styleable;->SchartMultiChartView:[I

    invoke-virtual {p1, p2, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 69
    :goto_0
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 71
    if-eqz v4, :cond_0

    .line 73
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v3, v6, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 75
    .local v0, "chartTypeArray":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v5, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mSeriesNum:I

    if-lt v2, v5, :cond_1

    .line 86
    .end local v0    # "chartTypeArray":[I
    .end local v2    # "i":I
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 89
    new-instance v5, Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-direct {v5}, Lcom/sec/dmc/sic/android/view/TimeChart;-><init>()V

    iput-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    .line 90
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    iget-wide v6, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mChartHandle:J

    invoke-virtual {v5, v6, v7}, Lcom/sec/dmc/sic/android/view/TimeChart;->setChartHandle(J)V

    .line 91
    return-void

    .line 65
    :catch_0
    move-exception v1

    .line 67
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v4, 0x0

    goto :goto_0

    .line 77
    .end local v1    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v0    # "chartTypeArray":[I
    .restart local v2    # "i":I
    :cond_1
    aget v5, v0, v2

    if-ne v5, v7, :cond_2

    .line 78
    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->createSeries(I)V

    .line 75
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 80
    :cond_2
    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->createSeries(I)V

    goto :goto_2
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 102
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;-><init>(Landroid/content/Context;)V

    .line 105
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 116
    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfXAxis()I

    move-result v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfYAxis()I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->addAxis(II)V

    .line 118
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 120
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 134
    return-void

    .line 106
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    .line 108
    .local v1, "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 109
    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->createSeries(I)V

    .line 105
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v5, :cond_1

    .line 111
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->createSeries(I)V

    goto :goto_2

    .line 122
    .end local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_3
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    .line 124
    .restart local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v4, :cond_5

    .line 126
    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    .end local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v4, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->addData(IILjava/util/List;)V

    .line 120
    :cond_4
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 128
    .restart local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    :cond_5
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartSeries;->getSeriesDataType()I

    move-result v2

    if-ne v2, v5, :cond_4

    .line 130
    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;

    .end local v1    # "tempSeries":Lcom/samsung/android/sdk/chart/series/SchartSeries;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartCandleTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v0, v5, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->addData(IILjava/util/List;)V

    goto :goto_3
.end method


# virtual methods
.method public addData(IILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 225
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/dmc/sic/android/view/TimeChart;->addData(IILjava/util/List;)V

    .line 229
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 258
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :goto_0
    return-void

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;->clearData()V

    goto :goto_0
.end method

.method public prepareChangeData(IILjava/util/List;)V
    .locals 2
    .param p1, "streamID"    # I
    .param p2, "seriesDataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    .local p3, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 243
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/dmc/sic/android/view/TimeChart;->prepareChangeData(IILjava/util/List;)V

    goto :goto_0
.end method

.method public setHandlerStartDate(D)V
    .locals 2
    .param p1, "handlerStartDate"    # D

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 275
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setHandlerStartDate(D)V

    goto :goto_0
.end method

.method public setScrollRangeDepthLevel(I)V
    .locals 1
    .param p1, "timeLevel"    # I

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setScrollRangeDepthLevel(I)V

    .line 294
    return-void
.end method

.method public setScrollRangeType(I)V
    .locals 1
    .param p1, "rangeType"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setScrollRangeType(I)V

    .line 308
    return-void
.end method

.method public setStartDate(D)V
    .locals 2
    .param p1, "startDate"    # D

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 187
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartDate(D)V

    .line 191
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartDepthLevel(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 170
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartDepthLevel(I)V

    .line 174
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartMainMarking(II)V
    .locals 2
    .param p1, "scaleIntervalMainMarking"    # I
    .param p2, "nMainMarkingCount"    # I

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 205
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartMainMarking(II)V

    .line 209
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartVisual(IDII)V
    .locals 6
    .param p1, "level"    # I
    .param p2, "startDate"    # D
    .param p4, "scaleIntervalMainMarking"    # I
    .param p5, "nMainMarkingCount"    # I

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 152
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartVisual(IDII)V

    .line 156
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setUserScrollRange(JJ)V
    .locals 1
    .param p1, "userLeftStart"    # J
    .param p3, "userRightEnd"    # J

    .prologue
    .line 320
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeLayoutChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/dmc/sic/android/view/TimeChart;->setUserScrollRange(JJ)V

    .line 321
    return-void
.end method
