.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$DeviceInfoColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceInfoColumns"
.end annotation


# static fields
.field public static final CONNECTIVITY:Ljava/lang/String; = "connectivity"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "device_type"

.field public static final MANUFACTURE:Ljava/lang/String; = "manufacture"

.field public static final MODEL:Ljava/lang/String; = "model"

.field public static final REF_WEB_SITE:Ljava/lang/String; = "ref_web_site"
