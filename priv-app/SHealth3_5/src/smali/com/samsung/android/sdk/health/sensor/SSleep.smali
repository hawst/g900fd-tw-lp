.class public Lcom/samsung/android/sdk/health/sensor/SSleep;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SSleep.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SSleep;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public efficiency:D

.field public endTime:J

.field public startTime:J

.field public status:[I

.field public time:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SSleep$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SSleep$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SSleep;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 80
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide v2, 0x7fffffffffffffffL

    .line 19
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 9
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->startTime:J

    .line 11
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->endTime:J

    .line 13
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->efficiency:D

    .line 15
    new-array v0, v5, [I

    const v1, 0x7fffffff

    aput v1, v0, v4

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->status:[I

    .line 17
    new-array v0, v5, [J

    aput-wide v2, v0, v4

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->time:[J

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const-wide v2, 0x7fffffffffffffffL

    .line 23
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 9
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->startTime:J

    .line 11
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->endTime:J

    .line 13
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->efficiency:D

    .line 15
    new-array v0, v5, [I

    const v1, 0x7fffffff

    aput v1, v0, v4

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->status:[I

    .line 17
    new-array v0, v5, [J

    aput-wide v2, v0, v4

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->time:[J

    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SSleep;->readFromParcel(Landroid/os/Parcel;)V

    .line 26
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->startTime:J

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->endTime:J

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->efficiency:D

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->status:[I

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->time:[J

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->extra:Landroid/os/Bundle;

    .line 67
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[SLEEP]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 49
    .local v1, "sb":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\nstartTime ="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->startTime:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->startTime:J

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", endTime ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->endTime:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->endTime:J

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", efficiency = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->efficiency:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    const-string v2, "\n(status,time) = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->status:[I

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 56
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 52
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->status:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->time:[J

    aget-wide v3, v3, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    rem-int/lit8 v2, v0, 0xa

    if-nez v2, :cond_1

    .line 54
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->startTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->endTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->efficiency:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->status:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->time:[J

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeLongArray([J)V

    .line 42
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SSleep;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 43
    return-void
.end method
