.class public Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;
.super Ljava/lang/Object;
.source "ShealthPlatformBinderFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    }
.end annotation


# static fields
.field public static final DEVICE_TYPE_PLATFORM_DEVICE:I = 0x6a

.field public static final DEVICE_TYPE_PLATFORM_FINDER:I = 0x69

.field private static SERVICE_CONNECTION_ESTABLISHED:I = 0x0

.field private static SERVICE_CONNECTION_LOST:I = 0x0

.field private static SERVICE_CONNECTION_REQUESTED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ShealthPlatformBinderFactory"

.field private static sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;


# instance fields
.field private mCallBackHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mObjDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceConnectionState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    .line 326
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    .line 327
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    .line 328
    const/4 v0, 0x2

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 258
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 304
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    .line 308
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    .line 120
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mContext:Landroid/content/Context;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    .line 122
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->initializeCallbackHandlerThread()Z

    .line 123
    return-void
.end method

.method private NotifyServiceConnectionStatus(ZI)Z
    .locals 9
    .param p1, "bTrueConneced"    # Z
    .param p2, "ErrorNumber"    # I

    .prologue
    const/4 v4, 0x1

    .line 216
    const/4 v0, 0x0

    .line 219
    .local v0, "bNotified":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 221
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 222
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 224
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;

    .line 226
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

    if-eqz v6, :cond_0

    .line 228
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$2;

    invoke-direct {v2, p0, p1, v3, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$2;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;ZLcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;I)V

    .line 243
    .local v2, "r":Ljava/lang/Runnable;
    const/4 v0, 0x1

    .line 244
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 245
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x32

    invoke-virtual {v6, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 249
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    .end local v2    # "r":Ljava/lang/Runnable;
    .end local v3    # "u":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 250
    if-ne v0, v4, :cond_2

    .line 255
    :goto_1
    return v4

    .line 254
    :cond_2
    const-string v4, "ShealthPlatformBinderFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No Listener was notified,only Device Objects? MUST not happen: bConnect:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private NotifyServiceConnectionStatus(ZILcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)Z
    .locals 9
    .param p1, "bTrueConneced"    # Z
    .param p2, "ErrorNumber"    # I
    .param p3, "uListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

    .prologue
    const/4 v4, 0x1

    .line 172
    const/4 v0, 0x0

    .line 175
    .local v0, "bNotified":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 177
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 178
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 180
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;

    .line 182
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

    if-ne v6, p3, :cond_0

    .line 185
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$1;

    invoke-direct {v2, p0, p1, v3, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$1;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;ZLcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;I)V

    .line 199
    .local v2, "r":Ljava/lang/Runnable;
    const/4 v0, 0x1

    .line 200
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 201
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x32

    invoke-virtual {v6, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 205
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    .end local v2    # "r":Ljava/lang/Runnable;
    .end local v3    # "u":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 206
    if-ne v0, v4, :cond_2

    .line 211
    :goto_1
    return v4

    .line 210
    :cond_2
    const-string v4, "ShealthPlatformBinderFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No Listener was notified,only Device Objects? MUST not happen: bConnect:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v4, 0x0

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;ZI)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->NotifyServiceConnectionStatus(ZI)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    return p1
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    return v0
.end method

.method static synthetic access$300()Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    return-object p1
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    return v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    return v0
.end method

.method private dropServiceConnection()Z
    .locals 2

    .prologue
    .line 162
    const-string v0, "ShealthPlatformBinderFactory"

    const-string v1, "dropServiceConnection: Unbinding with Health Service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 165
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public static declared-synchronized getDefaultBinder(Landroid/content/Context;IJLcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "Devicetype"    # I
    .param p2, "ObjectId"    # J
    .param p4, "uListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;

    .prologue
    .line 68
    const-class v7, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    monitor-enter v7

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    if-nez v1, :cond_0

    .line 70
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    .line 73
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v1, v2, :cond_1

    .line 75
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 76
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 79
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;IJLcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)V

    .line 80
    .local v0, "arg0":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v1, v2, :cond_3

    .line 84
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 85
    .local v6, "it":Landroid/content/Intent;
    const-string v1, "com.sec.android.service.health.sensor.PrivilegeSensorService"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mContext:Landroid/content/Context;

    .line 87
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v6, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 89
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v1, v2, :cond_2

    .line 90
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    .line 91
    :cond_2
    const-string v1, "ShealthPlatformBinderFactory"

    const-string v2, "Requested Binding to Health Service"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    .end local v6    # "it":Landroid/content/Intent;
    :cond_3
    if-eqz p4, :cond_4

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    if-ne v1, v2, :cond_4

    .line 105
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->NotifyServiceConnectionStatus(ZILcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;)Z

    .line 108
    :cond_4
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v7

    return-object v1

    .line 96
    .restart local v6    # "it":Landroid/content/Intent;
    :cond_5
    :try_start_1
    const-string v1, "ShealthPlatformBinderFactory"

    const-string v2, "Binding to Shealth Service wont happen.."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    const/4 v1, 0x0

    goto :goto_0

    .line 68
    .end local v0    # "arg0":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    .end local v6    # "it":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit v7

    throw v1
.end method

.method public static declared-synchronized getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    .locals 8
    .param p0, "Devicetype"    # I
    .param p1, "ObjectId"    # J

    .prologue
    .line 39
    const-class v4, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    monitor-enter v4

    const/4 v2, 0x0

    .line 41
    .local v2, "u":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    :try_start_0
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v5, v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 43
    :try_start_1
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 44
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;

    move-object v2, v0

    .line 47
    iget-wide v6, v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;->mObjId:J

    cmp-long v3, v6, p1

    if-nez v3, :cond_0

    .line 52
    :cond_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    if-nez v2, :cond_2

    .line 55
    :try_start_2
    const-string v3, "ShealthPlatformBinderFactory"

    const-string v5, "Looke like getDefaultBinder was not called"

    invoke-static {v3, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 56
    const/4 v3, 0x0

    .line 61
    :goto_0
    monitor-exit v4

    return-object v3

    .line 52
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 39
    :catchall_1
    move-exception v3

    monitor-exit v4

    throw v3

    .line 61
    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_2
    :try_start_5
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0
.end method

.method private initializeCallbackHandlerThread()Z
    .locals 3

    .prologue
    .line 288
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ShealthPlatformBinderFactory-Callback"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 289
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 290
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    .line 292
    const/4 v1, 0x1

    return v1
.end method

.method public static declared-synchronized releaseReference(II)Z
    .locals 4
    .param p0, "Devicetype"    # I
    .param p1, "ObjectId"    # I

    .prologue
    .line 114
    const-class v1, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    int-to-long v2, p1

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->removeMatchingObject(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    const/4 v0, 0x0

    monitor-exit v1

    return v0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 299
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 300
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 302
    :cond_0
    return-void
.end method

.method removeMatchingObject(IJ)Z
    .locals 8
    .param p1, "Devicetype"    # I
    .param p2, "ObjectId"    # J

    .prologue
    const/4 v4, 0x1

    .line 127
    const/4 v2, 0x0

    .line 130
    .local v2, "u":Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 132
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 133
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 135
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;

    move-object v2, v0

    .line 136
    iget-wide v6, v2, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;->mObjId:J

    cmp-long v3, v6, p2

    if-nez v3, :cond_0

    .line 141
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    if-nez v2, :cond_2

    .line 145
    const-string v3, "ShealthPlatformBinderFactory"

    const-string/jumbo v4, "removeMatchingObject:Matching Object not found"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    const/4 v3, 0x0

    .line 156
    :goto_0
    return v3

    .line 141
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 151
    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$SensorObjectDetails;>;"
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 152
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-ne v3, v4, :cond_3

    .line 154
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->dropServiceConnection()Z

    :cond_3
    move v3, v4

    .line 156
    goto :goto_0
.end method
