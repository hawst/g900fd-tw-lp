.class Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
.super Ljava/lang/Thread;
.source "WearableCommServerSocket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServerSocketThread"
.end annotation


# instance fields
.field private volatile mServerSocketLooper:Landroid/os/Looper;

.field private mSocketHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)V
    .locals 1

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->mServerSocketLooper:Landroid/os/Looper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;
    .param p2, "x1"    # Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$1;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;-><init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)V

    return-void
.end method


# virtual methods
.method public destroyLooper()V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->this$0:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;

    # getter for: Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->serverSocketThread:Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;->access$1000(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket;)Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->mServerSocketLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 396
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->mServerSocketLooper:Landroid/os/Looper;

    .line 398
    :cond_0
    return-void
.end method

.method public getServerSocketMsgHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->mSocketHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->mSocketHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 156
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 158
    new-instance v0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread$1;-><init>(Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->mSocketHandler:Landroid/os/Handler;

    .line 378
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/wearablecomm/socket/WearableCommServerSocket$ServerSocketThread;->mServerSocketLooper:Landroid/os/Looper;

    .line 379
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 380
    return-void
.end method
