.class public Lcom/samsung/android/sdk/health/wearablecomm/jdata/JWearableData;
.super Ljava/lang/Object;
.source "JWearableData.java"


# instance fields
.field public coachingEnergy:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingEnergy;

.field public coachingProfile:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingProfile;

.field public coachingResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

.field public coachingRunningExercise:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingRunningExercise;

.field public coachingVar:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoaching;

.field public device:Ljava/lang/String;

.field public exerciseResult:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;

.field public heartMonitor:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHeartRateMonitor;

.field public pedometer:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;

.field public pedometerGoal:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

.field public pedometerGoalHistory:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JGoal;

.field public resetTime:J

.field public sleep:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JSleep;

.field public stress:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JStress;

.field public userProfile:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JProfile;

.field public uvProfile:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRayProfile;

.field public uvRay:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JUvRay;

.field public version:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
