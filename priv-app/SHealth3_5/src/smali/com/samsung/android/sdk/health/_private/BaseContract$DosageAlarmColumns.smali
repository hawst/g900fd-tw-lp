.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$DosageAlarmColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DosageAlarmColumns"
.end annotation


# static fields
.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PERIOD:Ljava/lang/String; = "period"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final TYPE:Ljava/lang/String; = "type"
