.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseColumns"
.end annotation


# static fields
.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final _ID:Ljava/lang/String; = "_id"
