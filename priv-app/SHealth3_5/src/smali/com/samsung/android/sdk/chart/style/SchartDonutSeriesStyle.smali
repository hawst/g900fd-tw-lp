.class public Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;
.super Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;
.source "SchartDonutSeriesStyle.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/style/SchartNonXYSeriesStyle;-><init>()V

    .line 8
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mSeriesType:I

    .line 9
    return-void
.end method


# virtual methods
.method public getHoleRadiusRatio()F
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieHoleRadiusRatio()F

    move-result v0

    return v0
.end method

.method public getItemColor(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieItemColor(I)I

    move-result v0

    return v0
.end method

.method public getItemPercentLabel(I)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPiePercentLabel(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemSeparatorColor(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieItemSeparatorColor(I)I

    move-result v0

    return v0
.end method

.method public getItemSeparatorWidth(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieItemSeparatorWidth(I)I

    move-result v0

    return v0
.end method

.method public getLabelVisible(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieLabelVisible(I)Z

    move-result v0

    return v0
.end method

.method public getRadiusRatio()F
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieRadiusRatio()F

    move-result v0

    return v0
.end method

.method public getStartAngleRadian()F
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieStartAngleRadian()F

    move-result v0

    return v0
.end method

.method public getTextCenterRatio(I)F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieTextCenterRadiusRatio(I)F

    move-result v0

    return v0
.end method

.method public getTextGuidelineVisible(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieTextGuidelineVisible(I)Z

    move-result v0

    return v0
.end method

.method public getValueVisible(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getPieValueVisible(I)Z

    move-result v0

    return v0
.end method

.method public setHoleRadiusRatio(F)V
    .locals 1
    .param p1, "radiusRatio"    # F

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieHoleRadiusRatio(F)V

    .line 40
    return-void
.end method

.method public setItemColor(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "color"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieItemColor(II)V

    .line 178
    return-void
.end method

.method public setItemPercentLabel(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "a"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPiePercentLabel(ILjava/lang/String;)V

    .line 202
    return-void
.end method

.method public setItemSeparatorColor(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "color"    # I

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieItemSeparatorColor(II)V

    .line 226
    return-void
.end method

.method public setItemSeparatorWidth(II)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "width"    # I

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieItemSeparatorWidth(II)V

    .line 250
    return-void
.end method

.method public setLabelTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieLabelTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 121
    return-void
.end method

.method public setLabelVisible(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieLabelVisible(IZ)V

    .line 100
    return-void
.end method

.method public setRadiusRatio(F)V
    .locals 1
    .param p1, "radiusRatio"    # F

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieRadiusRatio(F)V

    .line 20
    return-void
.end method

.method public setStartAngleRadian(F)V
    .locals 1
    .param p1, "radian"    # F

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieStartAngleRadian(F)V

    .line 272
    return-void
.end method

.method public setTextCenterRatio(IF)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "radiusRatio"    # F

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieTextCenterRadiusRatio(IF)V

    .line 80
    return-void
.end method

.method public setTextGuidelineVisible(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieTextGuidelineVisible(IZ)V

    .line 60
    return-void
.end method

.method public setValuePercentTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieValuePercentTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 165
    return-void
.end method

.method public setValueTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieValueTextStyle(ILcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 153
    return-void
.end method

.method public setValueVisible(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartDonutSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setPieValueVisible(IZ)V

    .line 132
    return-void
.end method
