.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$FirstBeatCoachingVariableColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FirstBeatCoachingVariableColumns"
.end annotation


# static fields
.field public static final AC:Ljava/lang/String; = "ac"

.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final LATEST_EXERCISE_TIME:Ljava/lang/String; = "latest_exercise_time"

.field public static final LATEST_FEEDBACK_PHRASE_NUMBER:Ljava/lang/String; = "latest_feedback_phrase_number"

.field public static final MAXIMUM_HEART_RATE:Ljava/lang/String; = "maximum_heart_rate"

.field public static final MAXIMUM_MET:Ljava/lang/String; = "maximum_met"

.field public static final PREVIOUS_TO_PREVIOUS_TRAINING_LEVEL:Ljava/lang/String; = "previous_to_previous_training_level"

.field public static final PREVIOUS_TRAINING_LEVEL:Ljava/lang/String; = "previous_training_level"

.field public static final RECOVERY_RESOURCE:Ljava/lang/String; = "recovery_resource"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final TRAINING_LEVEL:Ljava/lang/String; = "training_level"

.field public static final TRAINING_LEVEL_UPDATE:Ljava/lang/String; = "training_level_update"
