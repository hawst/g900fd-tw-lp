.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$ActivityType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ActivityType"
.end annotation


# static fields
.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final TYPE_ACTIVITY_HEAVY:I = 0x2bf24

.field public static final TYPE_ACTIVITY_LIGHT:I = 0x2bf22

.field public static final TYPE_ACTIVITY_LITTLE_TO_NO:I = 0x2bf21

.field public static final TYPE_ACTIVITY_MODERATE:I = 0x2bf23

.field public static final TYPE_ACTIVITY_VERY_HEAVY:I = 0x2bf25
