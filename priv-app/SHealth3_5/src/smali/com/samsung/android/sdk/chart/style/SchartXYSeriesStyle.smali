.class public Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
.super Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;
.source "SchartXYSeriesStyle.java"


# static fields
.field public static final GOAL_LINE_TEXT_ALIGN_CENTER:I = 0x1

.field public static final GOAL_LINE_TEXT_ALIGN_LEFT:I = 0x0

.field public static final GOAL_LINE_TEXT_ALIGN_RIGHT:I = 0x2

.field public static final GOAL_LINE_TEXT_ORIGIN_LEFT:I = 0x0

.field public static final GOAL_LINE_TEXT_ORIGIN_RIGHT:I = 0x1

.field public static final VALUE_MARKING_HANDLER_OVER_VISIBLE:I = 0x2

.field public static final VALUE_MARKING_NORMAL_VISIBLE:I = 0x1

.field public static final VALUE_MARKING_SHOW_ALL:I = 0x0

.field public static final VALUE_MARKING_SHOW_NOT_ALL:I = 0x1


# instance fields
.field protected mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

.field protected mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

.field protected mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;-><init>()V

    .line 46
    new-instance v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    .line 47
    new-instance v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    .line 48
    new-instance v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    .line 50
    return-void
.end method


# virtual methods
.method public getFixedMaxY()F
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->getFixedMaxYValue()F

    move-result v0

    return v0
.end method

.method public getFixedMinY()F
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->getFixedMinYValue()F

    move-result v0

    return v0
.end method

.method public getGoalEnableUpdateValue()Z
    .locals 1

    .prologue
    .line 773
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalEnableUpdateValue()Z

    move-result v0

    return v0
.end method

.method public getGoalLineColor()I
    .locals 4

    .prologue
    .line 748
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineColorAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 749
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 750
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 751
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 748
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getGoalLineProperty()Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    return-object v0
.end method

.method public getGoalLineTextAlign()I
    .locals 1

    .prologue
    .line 712
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalTextAlign()I

    move-result v0

    return v0
.end method

.method public getGoalLineTextOrigin()I
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalTextOrigin()I

    move-result v0

    return v0
.end method

.method public getGoalLineTextPostfix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalPostfixStr()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGoalLineTextPostfixStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalPostfixStryle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getGoalLineTextPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalPrefixStr()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGoalLineTextPrefixStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalPrefixStryle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getGoalLineTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getGoalLineTextVisible()Z
    .locals 1

    .prologue
    .line 648
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalTextVisible()Z

    move-result v0

    return v0
.end method

.method public getGoalLineTextoffsetX()F
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalTextOffsetX()F

    move-result v0

    return v0
.end method

.method public getGoalLineTextoffsetY()F
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalTextOffsetY()F

    move-result v0

    return v0
.end method

.method public getGoalLineThickness()F
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineThickness()F

    move-result v0

    return v0
.end method

.method public getGoalLineValue()F
    .locals 1

    .prologue
    .line 638
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalValue()F

    move-result v0

    return v0
.end method

.method public getGoalLineVisible()Z
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->isVisible()Z

    move-result v0

    return v0
.end method

.method public getGoalOverColor()I
    .locals 4

    .prologue
    .line 760
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalOverColorAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 761
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalOverColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 762
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalOverColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 763
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalOverColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 760
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getIsFixedMaxY()Z
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->isFixedMaxY()Z

    move-result v0

    return v0
.end method

.method public getIsFixedMinY()Z
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->isFixedMinY()Z

    move-result v0

    return v0
.end method

.method public getNormalRangeColorInRange()I
    .locals 4

    .prologue
    .line 527
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getColorInRangeAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 528
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getColorInRangeR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 529
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getColorInRangeG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 530
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getColorInRangeB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 527
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getNormalRangeFillColor()I
    .locals 4

    .prologue
    .line 540
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getFillAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 541
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getFillColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 542
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getFillColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 543
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getFillColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 540
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getNormalRangeMaxValue()F
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getMaxValue()F

    move-result v0

    return v0
.end method

.method public getNormalRangeMinValue()F
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getMinValue()F

    move-result v0

    return v0
.end method

.method public getNormalRangeProperty()Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    return-object v0
.end method

.method public getNormalRangeVisible()Z
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->isVisible()Z

    move-result v0

    return v0
.end method

.method public getValueMarkingHandlerOverColor()I
    .locals 4

    .prologue
    .line 606
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getHandlerOverColorAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 607
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getHandlerOverColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 608
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getHandlerOverColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 609
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getHandlerOverColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 606
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getValueMarkingInRangeColor()I
    .locals 4

    .prologue
    .line 593
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getInRangeColorAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 594
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getInRangeColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 595
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getInRangeColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 596
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getInRangeColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 593
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getValueMarkingNormalColor()I
    .locals 4

    .prologue
    .line 580
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getNormalColorAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 581
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getNormalColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 582
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getNormalColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 583
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getNormalColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 580
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getValueMarkingProperty()Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    return-object v0
.end method

.method public getValueMarkingSelectedColor()I
    .locals 4

    .prologue
    .line 617
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getPressedColorAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 618
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getPressedColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 619
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getPressedColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 620
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getPressedColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 617
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getValueMarkingSize()F
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getRadius()F

    move-result v0

    return v0
.end method

.method public getValueMarkingVisible()Z
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->isVisible()Z

    move-result v0

    return v0
.end method

.method public getValueMarkingVisibleOption()I
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getVisibleOption()I

    move-result v0

    return v0
.end method

.method public setFixedMinMaxY(ZZFF)V
    .locals 1
    .param p1, "fixedMin"    # Z
    .param p2, "fixedMax"    # Z
    .param p3, "minY"    # F
    .param p4, "maxY"    # F

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setFixedMinY(Z)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setFixedMaxY(Z)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0, p3}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setFixedMinYValue(F)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0, p4}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setFixedMaxYValue(F)V

    .line 99
    return-void
.end method

.method public setGoalEnableUpdateValue(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 451
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalEnableUpdateValue(Z)V

    .line 452
    return-void
.end method

.method public setGoalLineColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalLineColor(IIII)V

    .line 434
    return-void
.end method

.method public setGoalLineTextAlign(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalTextAlign(I)V

    .line 398
    return-void
.end method

.method public setGoalLineTextOffsetX(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 406
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalTextOffsetX(F)V

    .line 407
    return-void
.end method

.method public setGoalLineTextOffsetY(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 415
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalTextOffsetY(F)V

    .line 416
    return-void
.end method

.method public setGoalLineTextOrigin(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 388
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalTextOrigin(I)V

    .line 389
    return-void
.end method

.method public setGoalLineTextPostfix(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalPostfixStr(Ljava/lang/String;)V

    .line 362
    return-void
.end method

.method public setGoalLineTextPostfixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 379
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalPostfixStryle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 380
    return-void
.end method

.method public setGoalLineTextPrefix(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalPrefixStr(Ljava/lang/String;)V

    .line 353
    return-void
.end method

.method public setGoalLineTextPrefixStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalPrefixStryle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 371
    return-void
.end method

.method public setGoalLineTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 344
    return-void
.end method

.method public setGoalLineTextVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 334
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalTextVisible(Z)V

    .line 335
    return-void
.end method

.method public setGoalLineThickness(F)V
    .locals 1
    .param p1, "thickness"    # F

    .prologue
    .line 424
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalLineThickness(F)V

    .line 425
    return-void
.end method

.method public setGoalLineValue(F)V
    .locals 1
    .param p1, "value"    # F

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalValue(F)V

    .line 326
    return-void
.end method

.method public setGoalLineVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setVisible(Z)V

    .line 317
    return-void
.end method

.method public setGoalOverColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 442
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mGoalLineProperty:Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalOverColor(IIII)V

    .line 443
    return-void
.end method

.method public setNormalRangeColorInRange(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setColorInNormalRange(IIII)V

    .line 148
    return-void
.end method

.method public setNormalRangeFillColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setNormalRangeFillColor(IIII)V

    .line 157
    return-void
.end method

.method public setNormalRangeMaxValue(F)V
    .locals 1
    .param p1, "max"    # F

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setMaxValue(F)V

    .line 126
    return-void
.end method

.method public setNormalRangeMinMaxValue(FF)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setMaxValue(F)V

    .line 136
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setMaxValue(F)V

    .line 137
    return-void
.end method

.method public setNormalRangeMinValue(F)V
    .locals 1
    .param p1, "min"    # F

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setMinValue(F)V

    .line 117
    return-void
.end method

.method public setNormalRangeVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mNormalRangeProperty:Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setVisible(Z)V

    .line 108
    return-void
.end method

.method public setValueMarkingEnableHitTest(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setEnableHittest(Z)V

    .line 175
    return-void
.end method

.method public setValueMarkingHandlerOverColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->handlerOverColor(IIII)V

    .line 245
    return-void
.end method

.method public setValueMarkingHandlerOverInRangeImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setHandlerOverInRangeImage(Landroid/graphics/Bitmap;)V

    .line 255
    return-void
.end method

.method public setValueMarkingHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setHandlerOverOutRangeImage(Landroid/graphics/Bitmap;)V

    .line 265
    return-void
.end method

.method public setValueMarkingInRangeColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setinRangeColor(IIII)V

    .line 226
    return-void
.end method

.method public setValueMarkingInRangeImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setInRangeImage(Landroid/graphics/Bitmap;)V

    .line 236
    return-void
.end method

.method public setValueMarkingNormalColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setNormalColor(IIII)V

    .line 207
    return-void
.end method

.method public setValueMarkingNormalImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setNormalImage(Landroid/graphics/Bitmap;)V

    .line 217
    return-void
.end method

.method public setValueMarkingSelectedColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->pressedColor(IIII)V

    .line 274
    return-void
.end method

.method public setValueMarkingSelectedImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setPressedImage(Landroid/graphics/Bitmap;)V

    .line 284
    return-void
.end method

.method public setValueMarkingSize(F)V
    .locals 1
    .param p1, "radius"    # F

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setRadius(F)V

    .line 198
    return-void
.end method

.method public setValueMarkingSpotBitmapList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "bitmapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setBitmapList(Ljava/util/ArrayList;)V

    .line 296
    return-void
.end method

.method public setValueMarkingSpotShowList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 300
    .local p1, "spotShowInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/chart/view/SChartSpotShowInfo;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setSpotShowList(Ljava/util/ArrayList;)V

    .line 301
    return-void
.end method

.method public setValueMarkingSpotShowListener(Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;

    .prologue
    .line 305
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setListener(Lcom/samsung/android/sdk/chart/style/SchartValueMakringListener;)V

    .line 306
    return-void
.end method

.method public setValueMarkingSpotShowOption(I)V
    .locals 1
    .param p1, "showOption"    # I

    .prologue
    .line 290
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setMarkingShowOption(I)V

    .line 291
    return-void
.end method

.method public setValueMarkingVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setVisible(Z)V

    .line 166
    return-void
.end method

.method public setValueMarkingVisibleOption(I)V
    .locals 1
    .param p1, "option"    # I

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->mValueMarkingProperty:Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setVisibleOption(I)V

    .line 189
    return-void
.end method
