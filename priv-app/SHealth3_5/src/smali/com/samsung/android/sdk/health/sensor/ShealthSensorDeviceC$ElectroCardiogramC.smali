.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$ElectroCardiogramC;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ElectroCardiogramC"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toBulkElectroCardiogram([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;
    .locals 3
    .param p0, "data"    # [Landroid/os/Bundle;

    .prologue
    .line 981
    array-length v2, p0

    new-array v0, v2, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;

    .line 982
    .local v0, "ecgArray":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 984
    aget-object v2, p0, v1

    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$ElectroCardiogramC;->toElectroCardiogram(Landroid/os/Bundle;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;

    move-result-object v2

    aput-object v2, v0, v1

    .line 982
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 986
    :cond_0
    return-object v0
.end method

.method public static toElectroCardiogram(Landroid/os/Bundle;)Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;
    .locals 3
    .param p0, "data"    # Landroid/os/Bundle;

    .prologue
    .line 969
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;-><init>()V

    .line 970
    .local v0, "ecg":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;
    if-eqz p0, :cond_0

    .line 972
    const-string v1, "ecg_time"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->time:J

    .line 973
    const-string v1, "ecg_heartrate"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->heartRate:I

    .line 974
    const-string v1, "ecg_electrowave"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->electroWave:[F

    .line 976
    :cond_0
    return-object v0
.end method
