.class public Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;
.super Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
.source "SchartCandleSeriesStyle.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;-><init>()V

    .line 18
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mSeriesType:I

    .line 20
    return-void
.end method


# virtual methods
.method public getBodyWidth()F
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleBodyWidth()F

    move-result v0

    return v0
.end method

.method public getCloseConnectLineColor()I
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleLineCloseColor()I

    move-result v0

    return v0
.end method

.method public getConnectLineThickness()F
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleLineThickness()F

    move-result v0

    return v0
.end method

.method public getDecreaseBodyColor()I
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleBodyDecreaseColor()I

    move-result v0

    return v0
.end method

.method public getHighConnectLineColor()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleLineHighColor()I

    move-result v0

    return v0
.end method

.method public getIncreaseBodyColor()I
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleBodyIncreaseColor()I

    move-result v0

    return v0
.end method

.method public getLowConnectLineColor()I
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleLineLowColor()I

    move-result v0

    return v0
.end method

.method public getOpenConnectLineColor()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleLineOpenColor()I

    move-result v0

    return v0
.end method

.method public getShadowColor()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleShadowColor()I

    move-result v0

    return v0
.end method

.method public getShadowThickness()F
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleShadowThickness()F

    move-result v0

    return v0
.end method

.method public getShadowWidth()F
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getmCandleShadowWidth()F

    move-result v0

    return v0
.end method

.method public setBodyWidth(F)V
    .locals 1
    .param p1, "t"    # F

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleBodyWidth(F)V

    .line 56
    return-void
.end method

.method public setCloseConnectLineColor(I)V
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleLineCloseColor(I)V

    .line 119
    return-void
.end method

.method public setConnectLineThickness(F)V
    .locals 1
    .param p1, "w"    # F

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleLineThickness(F)V

    .line 83
    return-void
.end method

.method public setDecreaseBodyColor(I)V
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleBodyDecreaseColor(I)V

    .line 74
    return-void
.end method

.method public setHighConnectLineColor(I)V
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleLineHighColor(I)V

    .line 92
    return-void
.end method

.method public setIncreaseBodyColor(I)V
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleBodyIncreaseColor(I)V

    .line 65
    return-void
.end method

.method public setLowConnectLineColor(I)V
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleLineLowColor(I)V

    .line 101
    return-void
.end method

.method public setOpenConnectLineColor(I)V
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleLineOpenColor(I)V

    .line 110
    return-void
.end method

.method public setShadowColor(I)V
    .locals 1
    .param p1, "c"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleShadowColor(I)V

    .line 47
    return-void
.end method

.method public setShadowThickness(F)V
    .locals 1
    .param p1, "t"    # F

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleShadowThickness(F)V

    .line 38
    return-void
.end method

.method public setShadowWidth(F)V
    .locals 1
    .param p1, "w"    # F

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartCandleSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setmCandleShadowWidth(F)V

    .line 29
    return-void
.end method
