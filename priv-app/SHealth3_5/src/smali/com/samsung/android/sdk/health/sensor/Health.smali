.class public Lcom/samsung/android/sdk/health/sensor/Health;
.super Ljava/lang/Object;
.source "Health.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/Health$TimeChange;
    }
.end annotation


# static fields
.field public static final NOT_ASSIGNED_CHAR:C = '\uffff'

.field public static final NOT_ASSIGNED_DOUBLE:D = 1.7976931348623157E308

.field public static final NOT_ASSIGNED_FLOAT:F = 3.4028235E38f

.field public static final NOT_ASSIGNED_INT:I = 0x7fffffff

.field public static final NOT_ASSIGNED_LONG:J = 0x7fffffffffffffffL

.field public static final VERSION_1_0:Ljava/lang/String; = "VERSION 1.0"


# instance fields
.field public extra:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/Health;->extra:Landroid/os/Bundle;

    .line 13
    return-void
.end method
