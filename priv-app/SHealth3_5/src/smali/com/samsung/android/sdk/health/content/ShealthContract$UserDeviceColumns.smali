.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$UserDeviceColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UserDeviceColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CONNECTIVITY_TYPE:Ljava/lang/String; = "connectivity_type"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final CUSTOM_NAME:Ljava/lang/String; = "custom_name"

.field public static final DAYLIGHT_SAVING:Ljava/lang/String; = "daylight_saving"

.field public static final DEVICE_GROUP_TYPE:Ljava/lang/String; = "device_group_type"

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "device_type"

.field public static final HDID:Ljava/lang/String; = "hdid"

.field public static final MANUFACTURE:Ljava/lang/String; = "manufacture"

.field public static final MODEL:Ljava/lang/String; = "model"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final _ID:Ljava/lang/String; = "_id"
