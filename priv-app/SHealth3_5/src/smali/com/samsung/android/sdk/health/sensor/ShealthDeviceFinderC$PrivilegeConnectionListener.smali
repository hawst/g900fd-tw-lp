.class Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;
.super Ljava/lang/Object;
.source "ShealthDeviceFinderC.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrivilegeConnectionListener"
.end annotation


# instance fields
.field isConnected:Z

.field isDisconnected:Z

.field mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;)V
    .locals 1
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .prologue
    const/4 v0, 0x0

    .line 205
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected:Z

    .line 202
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected:Z

    .line 206
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    .line 207
    return-void
.end method


# virtual methods
.method isConnected()Z
    .locals 1

    .prologue
    .line 240
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected:Z

    return v0
.end method

.method isDisconnected()Z
    .locals 1

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected:Z

    return v0
.end method

.method public onServiceConnected(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 213
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isConnected:Z

    .line 214
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthDeviceFinderC PrivilegeConnectionListener onServiceConnected() is called + normal finder connected status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$500(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isConnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$500(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;->onServiceConnected(I)V

    .line 219
    :cond_0
    monitor-exit v1

    .line 220
    return-void

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 226
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->isDisconnected:Z

    .line 227
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$200()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ShealthDeviceFinderC PrivilegeConnectionListener onServiceDisconnected() is called + normal finder disconnected status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinderListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$500(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$NormalConnectionListener;->isDisconnected()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$400(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$400(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->close()V

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mNormalFinder:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->mActualListener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$ServiceConnectionListener;->onServiceDisconnected(I)V

    .line 234
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC$PrivilegeConnectionListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->mPrivilegeFinder:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;->access$302(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinderC;Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;)Lcom/samsung/android/sdk/health/sensor/ShealthPlatformDeviceFinder;

    .line 235
    monitor-exit v1

    .line 236
    return-void

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
