.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$ExerciseInfoType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Exercise"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise$FitnessLevel;
    }
.end annotation


# instance fields
.field public averageSpeed:F

.field public calorie:D

.field public coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

.field public crud:I

.field public devicePkId:Ljava/lang/String;

.field public distance:D

.field public duration:J

.field public exerciseGoal:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$ExerciseGoal;

.field public fatBurnTime:J

.field public fitnessLevel:I

.field public heartRate:D

.field public heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

.field public location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

.field public maxAltitude:F

.field public maxHeartRate:F

.field public maxSpeed:F

.field public minAltitude:F

.field public recoveryTime:J

.field public trainingEffect:F

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const v7, 0x7fffffff

    const-wide v5, 0x7fffffffffffffffL

    const/4 v4, 0x0

    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 2683
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2697
    const/16 v0, 0x4651

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->type:I

    .line 2699
    iput-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->duration:J

    .line 2701
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->calorie:D

    .line 2703
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->heartRate:D

    .line 2705
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->distance:D

    .line 2713
    iput v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->fitnessLevel:I

    .line 2716
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->averageSpeed:F

    .line 2719
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->maxSpeed:F

    .line 2722
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->maxHeartRate:F

    .line 2726
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->coachingResult:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;

    .line 2728
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->location:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Location;

    .line 2730
    iput-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->heartRateRawData:[Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;

    .line 2733
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->maxAltitude:F

    .line 2735
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->minAltitude:F

    .line 2739
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->trainingEffect:F

    .line 2741
    iput-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->recoveryTime:J

    .line 2749
    iput-wide v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->fatBurnTime:J

    .line 2757
    iput v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Exercise;->crud:I

    return-void
.end method
