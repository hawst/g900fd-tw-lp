.class public Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
.super Ljava/lang/Object;
.source "ShealthDeviceFinder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;,
        Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCallBackHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDiscoveredDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mLock:Ljava/lang/Object;

.field private mObjectId:I

.field private mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 225
    const-class v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;

    .line 652
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    .line 240
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinder() is called"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 243
    :cond_0
    const-string v0, "PEDOSTART"

    const-string v1, "ShealthDevice Finder :: Context and Listener can\'t be null"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Context and Listener can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->getInstance()Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/_private/ShealthState;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 248
    const-string v0, "PEDOSTART"

    const-string v1, "ShealthDevice Finder :: Shealth not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Shealth not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_2
    const-string v0, "PEDOSTART"

    const-string v1, "ShealthDevice Finder :: initializing"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinder constructor : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mContext:Landroid/content/Context;

    .line 254
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->getNextInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    .line 256
    const/16 v0, 0x64

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    int-to-long v1, v1

    invoke-static {p1, v0, v1, v2, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->getDefaultBinder(Landroid/content/Context;IJLcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 258
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->addDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    return-void
.end method

.method private addDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    .prologue
    .line 645
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 647
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 648
    monitor-exit v1

    .line 649
    return-void

    .line 648
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private cleanupDevices()V
    .locals 8

    .prologue
    .line 607
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    monitor-enter v5

    .line 609
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 613
    .local v0, "d":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :try_start_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 622
    :goto_1
    :try_start_2
    const-class v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    const-string v6, "deinitialize"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v4, v6, v7}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 623
    .local v3, "privateDeviceMethod":Ljava/lang/reflect/Method;
    if-eqz v3, :cond_0

    .line 625
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 626
    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 629
    .end local v3    # "privateDeviceMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 631
    .local v1, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 636
    .end local v0    # "d":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 615
    .restart local v0    # "d":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v1

    .line 617
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 635
    .end local v0    # "d":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mDiscoveredDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 636
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 637
    return-void
.end method

.method private getNextInt()I
    .locals 3

    .prologue
    .line 262
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    .line 263
    .local v0, "random":Ljava/util/Random;
    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v1

    return v1
.end method

.method private initializeCallbackHandlerThread()V
    .locals 3

    .prologue
    .line 268
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "callBackHandlerWorker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 269
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 270
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    .line 271
    return-void
.end method

.method private isValidConnectivityType(I)Z
    .locals 1
    .param p1, "connectivityType"    # I

    .prologue
    .line 576
    if-ltz p1, :cond_0

    const/16 v0, 0x8

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDataType(I)Z
    .locals 1
    .param p1, "dataType"    # I

    .prologue
    .line 596
    if-ltz p1, :cond_0

    const/16 v0, 0x16

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDeviceType(I)Z
    .locals 1
    .param p1, "deviceType"    # I

    .prologue
    .line 586
    const/16 v0, 0x2711

    if-lt p1, v0, :cond_0

    const/16 v0, 0x2730

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 280
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ShealthDeviceFinder cleanup finished, this instance is now unusable : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->cleanupDevices()V

    .line 284
    const/16 v0, 0x64

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->releaseReference(II)Z

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 287
    return-void
.end method

.method public getConnectedDeviceList(III)Ljava/util/List;
    .locals 9
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 456
    sget-object v6, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ShealthDeviceFinder getConnectedDeviceList - connectivityType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " deviceType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " dataType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    const/16 v6, 0x64

    iget v7, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    int-to-long v7, v7

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 458
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidConnectivityType(I)Z

    move-result v6

    if-nez v6, :cond_1

    move-object v1, v5

    .line 504
    :cond_0
    :goto_0
    return-object v1

    .line 464
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidDeviceType(I)Z

    move-result v6

    if-nez v6, :cond_2

    move-object v1, v5

    .line 466
    goto :goto_0

    .line 470
    :cond_2
    invoke-direct {p0, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidDataType(I)Z

    move-result v6

    if-nez v6, :cond_3

    move-object v1, v5

    .line 472
    goto :goto_0

    .line 476
    :cond_3
    invoke-virtual {p0, p1, p2, p3, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    move-object v1, v5

    .line 478
    goto :goto_0

    .line 482
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v6, :cond_7

    .line 484
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    invoke-interface {v6, p1, p2, p3}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->getConnectedDevices(III)Ljava/util/List;

    move-result-object v0

    .line 485
    .local v0, "_deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 487
    .local v1, "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    if-nez v0, :cond_5

    move-object v1, v5

    .line 489
    goto :goto_0

    .line 492
    :cond_5
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 494
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;

    .line 495
    .local v2, "devicee":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    if-eqz v2, :cond_6

    .line 498
    new-instance v4, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;

    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mContext:Landroid/content/Context;

    invoke-direct {v4, v2, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>(Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;Landroid/content/Context;)V

    .line 499
    .local v4, "sensorDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->addDeviceFound(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)V

    .line 492
    .end local v4    # "sensorDevice":Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 508
    .end local v0    # "_deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;>;"
    .end local v1    # "deviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;>;"
    .end local v2    # "devicee":Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
    .end local v3    # "index":I
    :cond_7
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v6, "getConnectedDeviceList - ShealthSensorService is not bounded."

    invoke-direct {v5, v6}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public isAvailable(IIILjava/lang/String;)Z
    .locals 5
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .param p4, "protocol"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 528
    const/16 v2, 0x64

    iget v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    int-to-long v3, v3

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 529
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isAvailable - connectivityType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " deviceType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " dataType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " protocol : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 531
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidConnectivityType(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 562
    :cond_0
    :goto_0
    return v1

    .line 537
    :cond_1
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidDeviceType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 543
    invoke-direct {p0, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidDataType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 549
    const/4 v0, 0x1

    .line 551
    .local v0, "code":I
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v2, :cond_2

    .line 553
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->checkAvailability(IIILjava/lang/String;)I

    move-result v0

    .line 560
    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isAvailable - code : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    if-nez v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 557
    :cond_2
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v2, "isAvailable - ShealthSensorService is not bounded."

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public startScan(IIIILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V
    .locals 6
    .param p1, "connectivityType"    # I
    .param p2, "deviceType"    # I
    .param p3, "dataType"    # I
    .param p4, "durationSeconds"    # I
    .param p5, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x64

    .line 310
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "startScan() connectivityType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " deviceType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " dataType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " durationSeconds : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    int-to-long v0, v0

    invoke-static {v3, v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 313
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidConnectivityType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connectivity Type ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 318
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidDeviceType(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 320
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device Type ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_1
    invoke-direct {p0, p3}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidDataType(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 325
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Data Type ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_2
    if-nez p5, :cond_3

    .line 329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ScanListener is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331
    :cond_3
    const/16 v0, 0xa

    if-lt p4, v0, :cond_4

    if-ge v3, p4, :cond_5

    .line 332
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid duration : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 334
    :cond_5
    invoke-virtual {p0, p1, p2, p3, v4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 336
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>()V

    throw v0

    .line 339
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 341
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 343
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 344
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    .line 346
    :cond_7
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v0, :cond_8

    .line 350
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->initializeCallbackHandlerThread()V

    .line 351
    new-instance v5, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    invoke-direct {v5, p0, p5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    .line 352
    .local v5, "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->startScan(IIIILcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;)I

    .line 358
    return-void

    .line 346
    .end local v5    # "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 356
    :cond_8
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string/jumbo v1, "startScan - ShealthSensorService is not bounded."

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startScan(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V
    .locals 6
    .param p1, "connectivityType"    # I
    .param p2, "deviceId"    # Ljava/lang/String;
    .param p3, "durationSeconds"    # I
    .param p4, "listener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Ljava/lang/IllegalArgumentException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x64

    .line 376
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startScan - connectivityType : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deviceId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " durationSeconds : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    int-to-long v1, v1

    invoke-static {v4, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 379
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isValidConnectivityType(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 381
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Connectivity Type ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not supported."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 384
    :cond_0
    if-nez p2, :cond_1

    .line 386
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "deviceId is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 389
    :cond_1
    if-nez p4, :cond_2

    .line 391
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "ScanListener is null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 394
    :cond_2
    const/16 v1, 0xa

    if-lt p3, v1, :cond_3

    if-ge v4, p3, :cond_4

    .line 395
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "invalid duration : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 397
    :cond_4
    const/16 v1, 0x2711

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2, v5}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->isAvailable(IIILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 399
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;

    invoke-direct {v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorNotSupportedException;-><init>()V

    throw v1

    .line 402
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 404
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 406
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    .line 407
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mCallBackHandler:Landroid/os/Handler;

    .line 409
    :cond_6
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v1, :cond_7

    .line 413
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->initializeCallbackHandlerThread()V

    .line 414
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;

    invoke-direct {v0, p0, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ScanListener;)V

    .line 415
    .local v0, "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->startScanByDeviceId(ILjava/lang/String;ILcom/samsung/android/sdk/health/sensor/_SensorServiceScanListener;)I

    .line 422
    return-void

    .line 409
    .end local v0    # "wrapperListener":Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$WrapperScanListener;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 420
    :cond_7
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string/jumbo v2, "startScan with device id - ShealthSensorService is not bounded."

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public stopScan()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;
        }
    .end annotation

    .prologue
    .line 431
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->TAG:Ljava/lang/String;

    const-string v1, "ShealthDeviceFinder - stopScan"

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    const/16 v0, 0x64

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mObjectId:I

    int-to-long v1, v1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 433
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    invoke-interface {v0}, Lcom/samsung/android/sdk/health/sensor/_SensorService;->stopScan()V

    .line 437
    return-void

    .line 436
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;

    const-string v1, "ShealthSensorService is not bounded."

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/health/sensor/exception/ShealthSensorServiceNotBoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
