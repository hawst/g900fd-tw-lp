.class Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;
.super Ljava/lang/Object;
.source "ShealthBinderFactory.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 300
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I

    move-result v1

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$500()I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I

    move-result v1

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$200()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 302
    :cond_0
    const-string v1, "ShealthBinderFactory"

    const-string v2, "Service Connected"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const-string v1, "[ShealthBinderFactory] onServiceConnected"

    invoke-static {v1}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 305
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    check-cast p2, Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .end local p2    # "binder":Landroid/os/IBinder;
    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    invoke-static {v1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;Lcom/samsung/android/sdk/health/sensor/_SensorService;)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$600()I

    move-result v2

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$102(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;I)I

    .line 310
    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    const/4 v2, 0x1

    const/4 v3, 0x0

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZI)Z
    invoke-static {v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZI)Z

    .line 316
    :goto_1
    return-void

    .line 306
    :catch_0
    move-exception v0

    .line 307
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ShealthBinderFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "case error : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 314
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local p2    # "binder":Landroid/os/IBinder;
    :cond_1
    const-string v1, "ShealthBinderFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Incorrect state found : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 290
    const-string v0, "ShealthBinderFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service disconnected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const-string v0, "[ShealthBinderFactory] onServiceDisconnected"

    invoke-static {v0}, Lcom/samsung/android/sdk/health/Shealth;->printLog(Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZI)Z
    invoke-static {v0, v3, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZI)Z

    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$200()I

    move-result v1

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$102(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;I)I

    .line 294
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$300()Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    move-result-object v0

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;Lcom/samsung/android/sdk/health/sensor/_SensorService;)Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 295
    return-void
.end method
