.class public Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;
.super Lcom/samsung/android/sdk/chart/series/SchartStringData;
.source "SchartStringCandleData.java"


# instance fields
.field protected mValue1:D

.field protected mValueMax:D

.field protected mValueMin:D


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartStringData;-><init>()V

    .line 14
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;DDDD)V
    .locals 0
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "high"    # D
    .param p4, "low"    # D
    .param p6, "open"    # D
    .param p8, "close"    # D

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartStringData;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mString:Ljava/lang/String;

    .line 18
    iput-wide p2, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValueMax:D

    .line 19
    iput-wide p4, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValueMin:D

    .line 20
    iput-wide p6, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValue:D

    .line 21
    iput-wide p8, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValue1:D

    .line 22
    return-void
.end method


# virtual methods
.method public getValueClose()D
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValue1:D

    return-wide v0
.end method

.method public getValueHigh()D
    .locals 2

    .prologue
    .line 26
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValueMax:D

    return-wide v0
.end method

.method public getValueLow()D
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValueMin:D

    return-wide v0
.end method

.method public getValueOpen()D
    .locals 2

    .prologue
    .line 42
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValue:D

    return-wide v0
.end method

.method public setValueClose(D)V
    .locals 0
    .param p1, "close"    # D

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValue1:D

    .line 55
    return-void
.end method

.method public setValueHigh(D)V
    .locals 0
    .param p1, "high"    # D

    .prologue
    .line 30
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValueMax:D

    .line 31
    return-void
.end method

.method public setValueLow(D)V
    .locals 0
    .param p1, "low"    # D

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValueMin:D

    .line 39
    return-void
.end method

.method public setValueOpen(D)V
    .locals 0
    .param p1, "open"    # D

    .prologue
    .line 46
    iput-wide p1, p0, Lcom/samsung/android/sdk/chart/series/SchartStringCandleData;->mValue:D

    .line 47
    return-void
.end method
