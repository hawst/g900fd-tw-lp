.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$PulseOximeterC;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PulseOximeterC"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static toBulkPulseOximeter([Landroid/os/Bundle;)[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;
    .locals 6
    .param p0, "data"    # [Landroid/os/Bundle;

    .prologue
    .line 916
    if-eqz p0, :cond_1

    .line 918
    array-length v3, p0

    new-array v2, v3, [Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;

    .line 919
    .local v2, "pulseOximeter":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    array-length v3, p0

    if-ge v1, v3, :cond_2

    .line 921
    aget-object v0, p0, v1

    .line 922
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 924
    new-instance v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;

    invoke-direct {v3}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;-><init>()V

    aput-object v3, v2, v1

    .line 925
    aget-object v3, v2, v1

    const-string v4, "heart_rate"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;->heartRate:I

    .line 926
    aget-object v3, v2, v1

    const-string/jumbo v4, "time_stamp"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;->time:J

    .line 927
    aget-object v3, v2, v1

    const-string v4, "heart_rate_interval"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;->interval:J

    .line 928
    aget-object v3, v2, v1

    const-string v4, "heart_rate_snr"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;->SNR:F

    .line 929
    aget-object v3, v2, v1

    const-string/jumbo v4, "pulse_oximetry"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;->pulseOximetry:F

    .line 930
    aget-object v3, v2, v1

    const-string v4, "heart_rate_unit"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, v3, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;->SNRUnit:I

    .line 919
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 935
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "index":I
    .end local v2    # "pulseOximeter":[Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;
    :cond_1
    const/4 v2, 0x0

    :cond_2
    return-object v2
.end method
