.class final Lcom/samsung/android/sdk/motion/d;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# instance fields
.field private synthetic a:Lcom/samsung/android/sdk/motion/SmotionPedometer;

.field private final synthetic b:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/motion/SmotionPedometer;Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/motion/d;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer;

    iput-object p2, p0, Lcom/samsung/android/sdk/motion/d;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 11

    const/4 v0, 0x3

    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget-object v5, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    invoke-virtual {v5}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v5

    if-ne v5, v3, :cond_0

    new-instance v5, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;

    invoke-direct {v5}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;-><init>()V

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getPedometerContext()Landroid/hardware/scontext/SContextPedometer;

    move-result-object v8

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;J)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeDistance()D

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;D)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeCalorie()D

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->b(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;D)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getSpeed()D

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->c(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;D)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeTotalStepCount()J

    move-result-wide v6

    invoke-static {v5, v1, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V

    const/4 v6, 0x5

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeRunDownStepCount()J

    move-result-wide v9

    invoke-static {v5, v6, v9, v10}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V

    const/4 v6, 0x4

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeRunUpStepCount()J

    move-result-wide v9

    invoke-static {v5, v6, v9, v10}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V

    const/4 v6, 0x6

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeRunStepCount()J

    move-result-wide v9

    invoke-static {v5, v6, v9, v10}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeWalkDownStepCount()J

    move-result-wide v6

    invoke-static {v5, v3, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeWalkUpStepCount()J

    move-result-wide v6

    invoke-static {v5, v2, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getCumulativeWalkStepCount()J

    move-result-wide v6

    invoke-static {v5, v0, v6, v7}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V

    invoke-virtual {v8}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v4

    :goto_0
    :pswitch_1
    invoke-static {v5, v0}, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/d;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer;

    invoke-static {v0, v5}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a(Lcom/samsung/android/sdk/motion/SmotionPedometer;Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/d;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b(Lcom/samsung/android/sdk/motion/SmotionPedometer;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/d;->b:Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;

    invoke-interface {v0, v5}, Lcom/samsung/android/sdk/motion/SmotionPedometer$ChangeListener;->onChanged(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/d;->a:Lcom/samsung/android/sdk/motion/SmotionPedometer;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->c(Lcom/samsung/android/sdk/motion/SmotionPedometer;Z)V

    :cond_0
    return-void

    :pswitch_2
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_3
    move v0, v1

    goto :goto_0

    :pswitch_4
    move v0, v2

    goto :goto_0

    :pswitch_5
    move v0, v3

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_8
    move v0, v4

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_8
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
