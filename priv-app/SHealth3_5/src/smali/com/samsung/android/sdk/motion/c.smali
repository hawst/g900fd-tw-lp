.class final Lcom/samsung/android/sdk/motion/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/motion/MRListener;


# instance fields
.field private final synthetic a:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/motion/SmotionCall;Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;)V
    .locals 0

    iput-object p2, p0, Lcom/samsung/android/sdk/motion/c;->a:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMotionListener(Landroid/hardware/motion/MREvent;)V
    .locals 4

    new-instance v2, Lcom/samsung/android/sdk/motion/SmotionCall$Info;

    invoke-direct {v2}, Lcom/samsung/android/sdk/motion/SmotionCall$Info;-><init>()V

    invoke-virtual {p1}, Landroid/hardware/motion/MREvent;->getMotion()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/motion/SmotionCall$Info;->a(I)V

    :goto_1
    invoke-static {v2, v0, v1}, Lcom/samsung/android/sdk/motion/SmotionCall$Info;->a(Lcom/samsung/android/sdk/motion/SmotionCall$Info;J)V

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/c;->a:Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/motion/SmotionCall$ChangeListener;->onChanged(Lcom/samsung/android/sdk/motion/SmotionCall$Info;)V

    goto :goto_0

    :pswitch_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/motion/SmotionCall$Info;->a(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
