.class public Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartDonutChartView;
.source "SchartStringDonutChartView.java"


# instance fields
.field private nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartDonutChartView;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartDonutChartView;->setDataType(I)V

    .line 24
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartDonutChartView;->createChart()V

    .line 26
    new-instance v0, Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setChartHandle(J)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;-><init>(Landroid/content/Context;)V

    .line 59
    const/4 v1, 0x1

    .line 60
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    .line 59
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->createSeries(II)V

    .line 62
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 64
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 69
    return-void

    .line 66
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;

    .line 67
    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYStringSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    .line 65
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->addDataBack(ILjava/util/List;)V

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addDataBack(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 86
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addData(IILjava/util/List;)V

    .line 91
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public addDataFront(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartStringData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartStringData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 98
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->addDataFront(IILjava/util/List;)V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 110
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->clearData()V

    goto :goto_0
.end method

.method public setStartVisual(II)V
    .locals 2
    .param p1, "startIndex"    # I
    .param p2, "validCount"    # I

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 75
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->nonTimeChart:Lcom/sec/dmc/sic/android/view/NonTimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/NonTimeChart;->setStartVisual(II)V

    .line 79
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartStringDonutChartView;->notifyRenderThread()V

    goto :goto_0
.end method
