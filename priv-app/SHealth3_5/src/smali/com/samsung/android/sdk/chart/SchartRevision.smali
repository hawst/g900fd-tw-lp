.class public Lcom/samsung/android/sdk/chart/SchartRevision;
.super Ljava/lang/Object;
.source "SchartRevision.java"


# static fields
.field static final BUILD_DATE:Ljava/lang/String; = "2014-12-04 16:54:17.97"

.field static final REVSION_INFO:Ljava/lang/String; = "Change 624133 on 2014/10/27 by byoungho.yun@SPG_visualization_ByounghoYun_SIC_1740 \'[Project] SIC1 [Title] Fixed  t\'\n"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetBuildDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "2014-12-04 16:54:17.97"

    return-object v0
.end method

.method public static GetRevision()Ljava/lang/String;
    .locals 6

    .prologue
    .line 15
    const-string v3, "Change 624133 on 2014/10/27 by byoungho.yun@SPG_visualization_ByounghoYun_SIC_1740 \'[Project] SIC1 [Title] Fixed  t\'\n"

    .line 17
    .local v3, "temp":Ljava/lang/String;
    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 18
    .local v0, "parseStr":[Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 20
    .local v1, "result":Ljava/lang/String;
    array-length v4, v0

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 22
    const/4 v4, 0x1

    aget-object v4, v0, v4

    const-string/jumbo v5, "on"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 23
    .local v2, "t":[Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    aget-object v5, v2, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 26
    .end local v2    # "t":[Ljava/lang/String;
    :cond_0
    return-object v1
.end method
