.class public Lcom/samsung/android/sdk/motion/Smotion;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/samsung/android/sdk/SsdkInterface;


# static fields
.field public static final TYPE_ACTIVITY:I = 0x3

.field public static final TYPE_ACTIVITY_NOTIFICATION:I = 0x4

.field public static final TYPE_CALL:I = 0x0

.field public static final TYPE_PEDOMETER:I = 0x1

.field public static final TYPE_PEDOMETER_WITH_UPDOWN_STEP:I = 0x2


# instance fields
.field a:Z

.field b:Landroid/content/Context;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->c:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->d:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->e:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->f:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->g:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->h:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    return-void
.end method


# virtual methods
.method public getVersionCode()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    const-string v0, "Motion-Ver 2.0.0"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/sdk/SsdkUnsupportedException;
        }
    .end annotation

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Smotion : Context is null. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/SsdkVendorCheck;->isSamsungDevice()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    new-instance v1, Ljava/lang/StringBuilder;

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " is not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Smotion : Context is null. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    :cond_2
    :goto_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/motion/Smotion;->isFeatureEnabled(I)Z

    move-result v0

    if-nez v0, :cond_9

    iput-boolean v6, p0, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string v1, "Smotion : This Device is not supported."

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_3
    :try_start_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->c:Z
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v2

    if-eqz v2, :cond_2

    :try_start_3
    const-class v0, Landroid/hardware/scontext/SContextManager;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v3

    const-string v3, "getFeatureLevel"

    invoke-virtual {v0, v3, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v0, "com.sec.feature.sensorhub"

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "com.sec.feature.scontext_lite"

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    const-string/jumbo v1, "scontext"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_7

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/hardware/scontext/SContextManager;->getFeatureLevel(I)I

    move-result v1

    if-lez v1, :cond_5

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/motion/Smotion;->d:Z

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    const-string/jumbo v3, "sensor"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    if-eqz v1, :cond_5

    const/4 v3, 0x6

    invoke-virtual {v1, v3}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/motion/Smotion;->e:Z

    :cond_5
    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Landroid/hardware/scontext/SContextManager;->getFeatureLevel(I)I

    move-result v1

    if-lez v1, :cond_6

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/motion/Smotion;->g:Z

    :cond_6
    const/16 v1, 0x1b

    invoke-virtual {v0, v1}, Landroid/hardware/scontext/SContextManager;->getFeatureLevel(I)I

    move-result v0

    if-lez v0, :cond_7

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->h:Z

    :cond_7
    const-class v0, Landroid/hardware/motion/MotionRecognitionManager;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/hardware/motion/MRListener;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v3

    const/4 v3, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-class v4, Landroid/os/Handler;

    aput-object v4, v1, v3

    const-string/jumbo v3, "registerListenerEvent"

    invoke-virtual {v0, v3, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "android.hardware.sensor.accelerometer"

    invoke-virtual {v2, v0}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "android.hardware.sensor.gyroscope"

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    const-string v3, "android.hardware.sensor.proximity"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v0, :cond_8

    if-eqz v1, :cond_8

    if-eqz v2, :cond_8

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->f:Z

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->c:Z

    goto/16 :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Smotion : Context is wrong. "

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_9
    return-void
.end method

.method public isFeatureEnabled(I)Z
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x4

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Smotion : Type value is wrong. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->b:Landroid/content/Context;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Smotion : initialize() is not called. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->a:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Smotion : initialize() is not successful. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->f:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->d:Z

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->e:Z

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->g:Z

    goto :goto_0

    :pswitch_4
    iget-boolean v0, p0, Lcom/samsung/android/sdk/motion/Smotion;->h:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
