.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CoachingResult"
.end annotation


# instance fields
.field public devicePkId:Ljava/lang/String;

.field public distance:D

.field public endTime:J

.field public eteMaxMET:I

.field public eteResourceRecovery:I

.field public eteTrainingLoadPeak:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const v2, 0x7fffffff

    .line 2654
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2656
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->endTime:J

    .line 2658
    const-wide v0, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->distance:D

    .line 2663
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteTrainingLoadPeak:I

    .line 2670
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteMaxMET:I

    .line 2672
    iput v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$CoachingResult;->eteResourceRecovery:I

    return-void
.end method
