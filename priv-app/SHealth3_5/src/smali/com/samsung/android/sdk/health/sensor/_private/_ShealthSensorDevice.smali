.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$TimeChange;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvProfile;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_UvRay;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_InactiveTimer;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_StepLevel;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Profile;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_ExerciseGoal;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Exercise;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_CoachingResult;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Coaching;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_DefaultHealth;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Stress;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Sleep;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Temperature;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Humidity;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_PulseOximeter;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Fitness;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BodyTemperature;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Extra;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Location;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Pedometer;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateRawData;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HeartRateMonitor;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodGlucose;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_BloodPressure;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Weight;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_HealthParcelable;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Response;,
        Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$Command;
    }
.end annotation


# static fields
.field public static final CONNECTIVITY_TYPE_MAX:I = 0x8

.field public static final CONNECTIVITY_TYPE_MIN:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;",
            ">;"
        }
    .end annotation
.end field

.field public static final DATA_TYPE_MAX:I = 0x16

.field public static final DATA_TYPE_MIN:I = 0x1

.field public static final DEFAULT_EXERCISEID:I = -0x1

.field private static final DEVICE_KEY:Ljava/lang/String; = "device_key"


# instance fields
.field private connectivityType:I

.field private dataTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private deviceId:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;

.field private deviceSubType:I

.field private deviceType:I

.field private devices:Landroid/os/Bundle;

.field private extraDeviceParams:Landroid/os/Bundle;

.field private isConnected:Z

.field private mObjectId:I

.field private manufacturer:Ljava/lang/String;

.field private originalDeviceName:Ljava/lang/String;

.field private protocol:Ljava/lang/String;

.field private serialNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 183
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 158
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->readFromParcel(Landroid/os/Parcel;)V

    .line 159
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 190
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 191
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "connectivityType"    # I
    .param p4, "deviceType"    # I
    .param p5, "dataType"    # I
    .param p6, "protocol"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 205
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 206
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    .line 207
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    .line 208
    iput p3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    .line 209
    iput p4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    .line 210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iput-object p6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    .line 213
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "connectivityType"    # I
    .param p4, "deviceType"    # I
    .param p6, "protocol"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 225
    .local p5, "dataTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 226
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 227
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    .line 228
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    .line 229
    iput p3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    .line 230
    iput p4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    .line 231
    iput-object p5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 232
    iput-object p6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    .line 233
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "originalDeviceName"    # Ljava/lang/String;
    .param p4, "connectivityType"    # I
    .param p5, "deviceType"    # I
    .param p6, "dataType"    # I
    .param p7, "protocol"    # Ljava/lang/String;

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 247
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 248
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    .line 249
    iput-object p3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    .line 250
    iput p4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    .line 251
    iput p5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    .line 252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 253
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    iput-object p7, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1, "deviceId"    # Ljava/lang/String;
    .param p2, "deviceName"    # Ljava/lang/String;
    .param p3, "originalDeviceName"    # Ljava/lang/String;
    .param p4, "connectivityType"    # I
    .param p5, "deviceType"    # I
    .param p7, "protocol"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 268
    .local p6, "dataTypeList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 269
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 270
    iput-object p2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    .line 271
    iput-object p3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    .line 272
    iput p4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    .line 273
    iput p5, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    .line 274
    iput-object p6, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 275
    iput-object p7, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    .line 276
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->manufacturer:Ljava/lang/String;

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->serialNumber:Ljava/lang/String;

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceSubType:I

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    .line 173
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    const-class v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->mObjectId:I

    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    .line 176
    return-void
.end method


# virtual methods
.method public addDataType(I)V
    .locals 2
    .param p1, "dataType"    # I

    .prologue
    .line 407
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 409
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 414
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 416
    :cond_1
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public getConnectedDevice()Landroid/os/Parcelable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">()TT;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    const-string v1, "device_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public getConnectedState()Z
    .locals 1

    .prologue
    .line 476
    iget-boolean v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->isConnected:Z

    return v0
.end method

.method public getConnectivityType()I
    .locals 1

    .prologue
    .line 442
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    return v0
.end method

.method public getDataType()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    return-object v0
.end method

.method public getDeviceOriginalName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 325
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDeviceOriginalName() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->manufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 303
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getName() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getObjectId()I
    .locals 1

    .prologue
    .line 3100
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->mObjectId:I

    return v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->serialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getSubType()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceSubType:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    return v0
.end method

.method public putConnectedDevice(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "device"    # Landroid/os/Parcelable;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    const-string v1, "device_key"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 77
    return-void
.end method

.method public setConnectedState(Z)V
    .locals 0
    .param p1, "bConnected"    # Z

    .prologue
    .line 485
    iput-boolean p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->isConnected:Z

    .line 486
    return-void
.end method

.method public setConnectivityType(I)V
    .locals 0
    .param p1, "sensorType"    # I

    .prologue
    .line 451
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    .line 452
    return-void
.end method

.method public setDataType(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 398
    .local p1, "dataType":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    .line 399
    return-void
.end method

.method public setDeviceOriginalName(Ljava/lang/String;)V
    .locals 3
    .param p1, "originalDeviceName"    # Ljava/lang/String;

    .prologue
    .line 319
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setDeviceOriginalName() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    .line 321
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    .line 295
    return-void
.end method

.method public setManufacturer(Ljava/lang/String;)V
    .locals 0
    .param p1, "manufacturer"    # Ljava/lang/String;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->manufacturer:Ljava/lang/String;

    .line 345
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 313
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setName() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    .line 315
    return-void
.end method

.method public setObjectId(I)V
    .locals 0
    .param p1, "objId"    # I

    .prologue
    .line 3104
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->mObjectId:I

    .line 3105
    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 433
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    .line 434
    return-void
.end method

.method public setSerialNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "serialNumber"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->serialNumber:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setSubType(I)V
    .locals 0
    .param p1, "subType"    # I

    .prologue
    .line 380
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceSubType:I

    .line 381
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "devicType"    # I

    .prologue
    .line 362
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    .line 363
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_ShealthSensorDevice : deviceId == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", originalDeviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectivityType == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceType == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dataTypeList == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", protocol == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", deviceName == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->originalDeviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->manufacturer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->serialNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 125
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 126
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->deviceSubType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->protocol:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 128
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->connectivityType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->devices:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->dataTypeList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 131
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->mObjectId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;->extraDeviceParams:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 133
    return-void
.end method
