.class public interface abstract Lcom/samsung/android/sdk/health/sensor/SProfile$WeightUnit;
.super Ljava/lang/Object;
.source "SProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/SProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "WeightUnit"
.end annotation


# static fields
.field public static final KILOGRAM:I = 0x1fbd1

.field public static final LBS:I = 0x1fbd2

.field public static final NOT_DEFINED:I = -0x1
