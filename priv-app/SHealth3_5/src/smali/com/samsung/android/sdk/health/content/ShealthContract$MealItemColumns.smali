.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$MealItemColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MealItemColumns"
.end annotation


# static fields
.field public static final AMOUNT:Ljava/lang/String; = "amount"

.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final FAVORITE:Ljava/lang/String; = "favorite"

.field public static final FOOD_INFO_ID:Ljava/lang/String; = "food_info__id"

.field public static final MEAL_ID:Ljava/lang/String; = "meal__id"

.field public static final UNIT:Ljava/lang/String; = "unit"
