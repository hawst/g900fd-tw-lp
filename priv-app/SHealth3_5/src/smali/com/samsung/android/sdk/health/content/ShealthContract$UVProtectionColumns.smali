.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$UVProtectionColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UVProtectionColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final RECOMMENDED_DURATION:Ljava/lang/String; = "recommended_duration"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final SUN_PROTECTION:Ljava/lang/String; = "sun_protection"
