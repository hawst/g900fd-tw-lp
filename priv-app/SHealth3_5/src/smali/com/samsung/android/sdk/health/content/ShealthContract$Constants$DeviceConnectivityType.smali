.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DeviceConnectivityType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeviceConnectivityType"
.end annotation


# static fields
.field public static final CONNECTIVITY_TYPE_ANT:I = 0x5

.field public static final CONNECTIVITY_TYPE_BLUETOOTH:I = 0x1

.field public static final CONNECTIVITY_TYPE_INTERNAL:I = 0x4
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final CONNECTIVITY_TYPE_NFC:I = 0x6

.field public static final CONNECTIVITY_TYPE_SAMSUNG_ACCESSORY:I = 0x7

.field public static final CONNECTIVITY_TYPE_SAP:I = 0x3

.field public static final CONNECTIVITY_TYPE_USB:I = 0x2
