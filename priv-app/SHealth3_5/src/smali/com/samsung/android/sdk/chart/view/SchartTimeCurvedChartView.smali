.class public Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartCurvedChartView;
.source "SchartTimeCurvedChartView.java"


# instance fields
.field private timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartCurvedChartView;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x2

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartCurvedChartView;->setDataType(I)V

    .line 36
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartCurvedChartView;->createChart()V

    .line 39
    new-instance v0, Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setChartHandle(J)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartCurvedChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    const/4 v0, 0x2

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartCurvedChartView;->setDataType(I)V

    .line 54
    invoke-super {p0}, Lcom/samsung/android/sdk/chart/view/SchartCurvedChartView;->createChart()V

    .line 56
    const/4 v0, 0x1

    iget v1, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mSeriesNum:I

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->createSeries(II)V

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 61
    new-instance v0, Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    iget-wide v1, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mChartHandle:J

    invoke-virtual {v0, v1, v2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setChartHandle(J)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;Lcom/samsung/android/sdk/chart/series/SchartDataSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "chartStyle"    # Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .param p3, "dataSet"    # Lcom/samsung/android/sdk/chart/series/SchartDataSet;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;-><init>(Landroid/content/Context;)V

    .line 76
    const/4 v1, 0x1

    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->createSeries(II)V

    .line 78
    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfXAxis()I

    move-result v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfYAxis()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->addAxis(II)V

    .line 80
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V

    .line 83
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p3}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 86
    return-void

    .line 84
    :cond_0
    invoke-virtual {p3, v0}, Lcom/samsung/android/sdk/chart/series/SchartDataSet;->getSeriesAt(I)Lcom/samsung/android/sdk/chart/series/SchartSeries;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->getAllData()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->addData(ILjava/util/List;)V

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addData(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 176
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->addData(IILjava/util/List;)V

    .line 182
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public addDataWithAni(ILjava/util/List;I)V
    .locals 3
    .param p1, "seriesID"    # I
    .param p3, "duration"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    const/4 v2, 0x1

    .line 195
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 197
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_1

    .line 202
    const-string v0, "SIC Curved Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 206
    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mChartType:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mChartType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    if-nez p3, :cond_4

    .line 207
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, v2, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->addData(IILjava/util/List;)V

    .line 211
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->notifyRenderThread()V

    goto :goto_0

    .line 209
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, v2, p2, p3}, Lcom/sec/dmc/sic/android/view/TimeChart;->addDataWithAni(IILjava/util/List;I)V

    goto :goto_1
.end method

.method public clearAllData()V
    .locals 2

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 239
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;->clearData()V

    goto :goto_0
.end method

.method public getEpochLeftEndofData()J
    .locals 2

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 254
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const-wide/16 v0, 0x0

    .line 257
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;->getEpochLeftEndofData()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getEpochRightEndofData()J
    .locals 2

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 268
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const-wide/16 v0, 0x0

    .line 271
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/view/TimeChart;->getEpochRightEndofData()J

    move-result-wide v0

    goto :goto_0
.end method

.method public prepareChangeData(ILjava/util/List;)V
    .locals 2
    .param p1, "seriesID"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p2, "listRawDatas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 225
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->prepareChangeData(IILjava/util/List;)V

    goto :goto_0
.end method

.method public setHandlerStartDate(D)V
    .locals 2
    .param p1, "handlerStartDate"    # D

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 284
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setHandlerStartDate(D)V

    goto :goto_0
.end method

.method public setScrollRangeDepthLevel(I)V
    .locals 1
    .param p1, "timeLevel"    # I

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setScrollRangeDepthLevel(I)V

    .line 303
    return-void
.end method

.method public setScrollRangeType(I)V
    .locals 1
    .param p1, "rangeType"    # I

    .prologue
    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setScrollRangeType(I)V

    .line 317
    return-void
.end method

.method public setStartDate(D)V
    .locals 2
    .param p1, "startDate"    # D

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 139
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartDate(D)V

    .line 144
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartDepthLevel(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 122
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartDepthLevel(I)V

    .line 127
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartMainMarking(II)V
    .locals 2
    .param p1, "scaleIntervalMainMarking"    # I
    .param p2, "nMainMarkingCount"    # I

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 157
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartMainMarking(II)V

    .line 162
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setStartVisual(IDII)V
    .locals 6
    .param p1, "level"    # I
    .param p2, "startDate"    # D
    .param p4, "scaleIntervalMainMarking"    # I
    .param p5, "nMainMarkingCount"    # I

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->mIsChartInitFinish:Z

    if-nez v0, :cond_0

    .line 105
    const-string v0, "SIC Chart"

    const-string/jumbo v1, "mIsChartInitFinish is false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/dmc/sic/android/view/TimeChart;->setStartVisual(IDII)V

    .line 109
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->notifyRenderThread()V

    goto :goto_0
.end method

.method public setUserScrollRange(JJ)V
    .locals 1
    .param p1, "userLeftStart"    # J
    .param p3, "userRightEnd"    # J

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartTimeCurvedChartView;->timeChart:Lcom/sec/dmc/sic/android/view/TimeChart;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/dmc/sic/android/view/TimeChart;->setUserScrollRange(JJ)V

    .line 330
    return-void
.end method
