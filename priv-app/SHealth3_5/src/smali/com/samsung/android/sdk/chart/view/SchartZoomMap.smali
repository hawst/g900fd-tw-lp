.class public Lcom/samsung/android/sdk/chart/view/SchartZoomMap;
.super Landroid/view/View;
.source "SchartZoomMap.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private bottom:I

.field private currentRegionHeight:F

.field private currentRegionWidth:F

.field private left:I

.field private m_firstPoint:Landroid/graphics/PointF;

.field private m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

.field private m_secondPoint:Landroid/graphics/PointF;

.field private mapBitmap:Landroid/graphics/Bitmap;

.field private oriHeight:F

.field private oriWidth:F

.field private originalRegionHeight:F

.field private originalRegionWidth:F

.field private right:I

.field private top:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 45
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 46
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setVisibility(I)V

    .line 48
    const/high16 v1, -0x10000

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setBackgroundColor(I)V

    .line 50
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0xc8

    const/16 v2, 0x12c

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 51
    .local v0, "param1":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setX(F)V

    .line 54
    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setY(F)V

    .line 56
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    .line 57
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    .line 58
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    .line 59
    iput v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->bottom:I

    .line 61
    iput v3, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriWidth:F

    .line 62
    iput v3, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriHeight:F

    .line 64
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->mapBitmap:Landroid/graphics/Bitmap;

    .line 66
    invoke-virtual {p0, p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 68
    iput v3, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->currentRegionWidth:F

    .line 69
    iput v3, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->currentRegionHeight:F

    .line 71
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    .line 72
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_secondPoint:Landroid/graphics/PointF;

    .line 73
    return-void
.end method

.method private getRealPos(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 4
    .param p1, "pos"    # Landroid/graphics/PointF;

    .prologue
    .line 231
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 233
    .local v0, "temp":Landroid/graphics/PointF;
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->currentRegionWidth:F

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->originalRegionWidth:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriWidth:F

    div-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->currentRegionHeight:F

    mul-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->originalRegionHeight:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriHeight:F

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    .line 235
    return-object v0
.end method


# virtual methods
.method getOriHeight()F
    .locals 1

    .prologue
    .line 254
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriHeight:F

    return v0
.end method

.method getOriWidth()F
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriWidth:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v10, 0x40800000    # 4.0f

    const/16 v9, 0xff

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 77
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 79
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->mapBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    .line 81
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->mapBitmap:Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v7, v7, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 83
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 84
    .local v0, "borderPaint":Landroid/graphics/Paint;
    invoke-virtual {v0, v9, v8, v8, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 85
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 86
    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 88
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 89
    .local v3, "innerPaint":Landroid/graphics/Paint;
    invoke-virtual {v3, v8, v8, v8, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 90
    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 91
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 93
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 95
    .local v1, "drawRect":Landroid/graphics/RectF;
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1, v7, v7, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 97
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 98
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 102
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 103
    .local v2, "halfAlpahPaint":Landroid/graphics/Paint;
    const/16 v4, 0xc8

    invoke-virtual {v2, v4, v9, v9, v9}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 104
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 107
    iget v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v1, v7, v7, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 108
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 110
    iget v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v1, v4, v7, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 111
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 113
    iget v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    int-to-float v6, v6

    invoke-virtual {v1, v4, v7, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 114
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 116
    iget v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->bottom:I

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    int-to-float v6, v6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 117
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 121
    new-instance v0, Landroid/graphics/Paint;

    .end local v0    # "borderPaint":Landroid/graphics/Paint;
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 122
    .restart local v0    # "borderPaint":Landroid/graphics/Paint;
    const/16 v4, 0x92

    const/16 v5, 0xc5

    const/16 v6, 0xe7

    invoke-virtual {v0, v9, v4, v5, v6}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 123
    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 124
    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 126
    new-instance v3, Landroid/graphics/Paint;

    .end local v3    # "innerPaint":Landroid/graphics/Paint;
    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 127
    .restart local v3    # "innerPaint":Landroid/graphics/Paint;
    invoke-virtual {v3, v8, v8, v8, v8}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 128
    invoke-virtual {v3, v8}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 129
    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 131
    new-instance v1, Landroid/graphics/RectF;

    .end local v1    # "drawRect":Landroid/graphics/RectF;
    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 133
    .restart local v1    # "drawRect":Landroid/graphics/RectF;
    iget v4, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->bottom:I

    int-to-float v7, v7

    invoke-virtual {v1, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    .line 135
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 136
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 138
    .end local v0    # "borderPaint":Landroid/graphics/Paint;
    .end local v1    # "drawRect":Landroid/graphics/RectF;
    .end local v2    # "halfAlpahPaint":Landroid/graphics/Paint;
    .end local v3    # "innerPaint":Landroid/graphics/Paint;
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 172
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 173
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v5, v5, 0xff

    packed-switch v5, :pswitch_data_0

    .line 226
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v5, 0x1

    return v5

    .line 175
    :pswitch_1
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 177
    const/4 v0, 0x0

    .line 178
    .local v0, "X":F
    const/4 v1, 0x0

    .line 180
    .local v1, "Y":F
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_1

    .line 181
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    int-to-float v6, v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->bottom:I

    int-to-float v6, v6

    cmpg-float v5, v5, v6

    if-ltz v5, :cond_0

    .line 186
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getRealPos(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/PointF;->x:F

    new-instance v6, Landroid/graphics/PointF;

    iget v7, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    int-to-float v8, v8

    div-float/2addr v8, v10

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    int-to-float v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->bottom:I

    int-to-float v9, v9

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getRealPos(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float v0, v5, v6

    .line 187
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getRealPos(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v5, v5, Landroid/graphics/PointF;->y:F

    new-instance v6, Landroid/graphics/PointF;

    iget v7, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    int-to-float v8, v8

    div-float/2addr v8, v10

    add-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    int-to-float v8, v8

    iget v9, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->bottom:I

    int-to-float v9, v9

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getRealPos(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float v1, v5, v6

    .line 189
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v6, v6, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    neg-float v8, v0

    neg-float v9, v1

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetZoomPanning(JFF)V

    .line 191
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v6, v5, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    monitor-enter v6

    .line 192
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v5, v5, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 191
    monitor-exit v6

    goto/16 :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 199
    .end local v0    # "X":F
    .end local v1    # "Y":F
    :pswitch_2
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_secondPoint:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/PointF;->set(FF)V

    .line 201
    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    .line 202
    .local v2, "m_tempSecondPoint":Landroid/graphics/PointF;
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_secondPoint:Landroid/graphics/PointF;

    .line 204
    const/4 v3, 0x0

    .line 205
    .local v3, "offsetX":F
    const/4 v4, 0x0

    .line 208
    .local v4, "offsetY":F
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getRealPos(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    .line 209
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_secondPoint:Landroid/graphics/PointF;

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getRealPos(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_secondPoint:Landroid/graphics/PointF;

    .line 211
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_secondPoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float v3, v5, v6

    .line 212
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_secondPoint:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float v4, v5, v6

    .line 215
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-wide v6, v6, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mChartHandle:J

    neg-float v8, v3

    neg-float v9, v4

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->SetZoomPanning(JFF)V

    .line 217
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v6, v5, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    monitor-enter v6

    .line 218
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    iget-object v5, v5, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;->mRenderThread:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView$RenderThread;

    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 217
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 221
    iget-object v5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_firstPoint:Landroid/graphics/PointF;

    invoke-virtual {v5, v2}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto/16 :goto_0

    .line 217
    :catchall_1
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v5

    .line 173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected setMapBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->setVisibility(I)V

    .line 144
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->mapBitmap:Landroid/graphics/Bitmap;

    .line 145
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->invalidate()V

    .line 146
    return-void
.end method

.method setOriHeight(F)V
    .locals 0
    .param p1, "oriHeight"    # F

    .prologue
    .line 258
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriHeight:F

    .line 259
    return-void
.end method

.method setOriWidth(F)V
    .locals 0
    .param p1, "oriWidth"    # F

    .prologue
    .line 249
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->oriWidth:F

    .line 251
    return-void
.end method

.method setRefChartView(Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;)V
    .locals 0
    .param p1, "view"    # Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->m_refView:Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;

    .line 242
    return-void
.end method

.method updateZoomRegionRect(FFFFFFFF)V
    .locals 4
    .param p1, "leftRate"    # F
    .param p2, "topRate"    # F
    .param p3, "widthRate"    # F
    .param p4, "heightRate"    # F
    .param p5, "currentRegionWidth"    # F
    .param p6, "currentRegionHeight"    # F
    .param p7, "originalRegionWidth"    # F
    .param p8, "originalRegionHeight"    # F

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getWidth()I

    move-result v1

    .line 151
    .local v1, "w":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->getHeight()I

    move-result v0

    .line 153
    .local v0, "h":I
    int-to-float v2, v1

    mul-float/2addr v2, p1

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    .line 154
    int-to-float v2, v0

    mul-float/2addr v2, p2

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    .line 155
    iget v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->left:I

    int-to-float v2, v2

    int-to-float v3, v1

    mul-float/2addr v3, p3

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->right:I

    .line 156
    iget v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->top:I

    int-to-float v2, v2

    int-to-float v3, v0

    mul-float/2addr v3, p4

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->bottom:I

    .line 159
    iput p5, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->currentRegionWidth:F

    .line 160
    iput p6, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->currentRegionHeight:F

    .line 162
    iput p7, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->originalRegionWidth:F

    .line 163
    iput p8, p0, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->originalRegionHeight:F

    .line 166
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/view/SchartZoomMap;->invalidate()V

    .line 167
    return-void
.end method
