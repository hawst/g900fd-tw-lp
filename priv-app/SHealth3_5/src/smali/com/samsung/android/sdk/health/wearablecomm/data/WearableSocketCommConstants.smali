.class public Lcom/samsung/android/sdk/health/wearablecomm/data/WearableSocketCommConstants;
.super Ljava/lang/Object;
.source "WearableSocketCommConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/wearablecomm/data/WearableSocketCommConstants$ErrorCodes;,
        Lcom/samsung/android/sdk/health/wearablecomm/data/WearableSocketCommConstants$HandlerMessages;
    }
.end annotation


# static fields
.field public static final CHUNK_SIZE:I = 0x10000

.field public static final SERVER_ADDRESS:Ljava/lang/String; = "HEALTH_SERVICE_SOCKET"

.field public static final STRING_DATA_SEND_COMPLETE:Ljava/lang/String; = "END_OF_DATA"

.field public static final UTF_8:Ljava/lang/String; = "UTF-8"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method
