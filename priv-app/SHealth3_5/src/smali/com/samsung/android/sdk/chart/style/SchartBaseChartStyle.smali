.class public Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;
.super Ljava/lang/Object;
.source "SchartBaseChartStyle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;
    }
.end annotation


# instance fields
.field protected mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

.field protected mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

.field protected mSeriesStyles:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    .line 45
    new-instance v0, Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    .line 46
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mSeriesStyles:Ljava/util/Vector;

    .line 48
    return-void
.end method


# virtual methods
.method public addSeriesStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;)V
    .locals 2
    .param p1, "seriesStyle"    # Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;

    .prologue
    .line 403
    instance-of v1, p1, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 404
    check-cast v1, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getGoalLineProperty()Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    move-result-object v0

    .line 405
    .local v0, "goalLineProperty":Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->makeGoalLinePostfixBitmap(Lcom/sec/dmc/sic/android/property/GoalLineProperty;)V

    .line 408
    .end local v0    # "goalLineProperty":Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mSeriesStyles:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 409
    return-void
.end method

.method public getChartBackgroundColor()I
    .locals 4

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getChartBackGroundAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 177
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getChartBackGroundColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 178
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getChartBackGroundColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 179
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getChartBackGroundColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 176
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getChartProperty()Lcom/sec/dmc/sic/android/property/ChartProperty;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    return-object v0
.end method

.method public getGraphBackgroundColor()I
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphBackgroundAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 198
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphBackgroundColorR()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 199
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphBackgroundColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 200
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphBackgroundColorB()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 197
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getGraphBackgroundColors()[I
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphBackGroundColors()[I

    move-result-object v0

    return-object v0
.end method

.method public getGraphBackgroundVaues()[F
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphBackGroundValues()[F

    move-result-object v0

    return-object v0
.end method

.method public getGraphSeparatorColor()I
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphSeparatorColor()I

    move-result v0

    return v0
.end method

.method public getGraphSeparatorVisible()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphSeparatorVisible()Z

    move-result v0

    return v0
.end method

.method public getGraphSeparatorWidth()F
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphSeparatorWidth()F

    move-result v0

    return v0
.end method

.method public getGraphTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphTitleTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getGraphTitleVisible()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getGraphTitleVisible()Z

    move-result v0

    return v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getHeight()F

    move-result v0

    return v0
.end method

.method public getLegendDirection()I
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getDirection()I

    move-result v0

    return v0
.end method

.method public getLegendDisableColor()I
    .locals 4

    .prologue
    .line 618
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getDisableAlpha()F

    move-result v0

    float-to-int v0, v0

    mul-int/lit16 v0, v0, 0xff

    .line 619
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getDisableColorB()F

    move-result v1

    float-to-int v1, v1

    mul-int/lit16 v1, v1, 0xff

    .line 620
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v2}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getDisableColorG()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit16 v2, v2, 0xff

    .line 621
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v3}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getDisableColorR()F

    move-result v3

    float-to-int v3, v3

    mul-int/lit16 v3, v3, 0xff

    .line 618
    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public getLegendItemHeight()F
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getItemHeight()F

    move-result v0

    return v0
.end method

.method public getLegendItemHeightSpace()F
    .locals 1

    .prologue
    .line 591
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getItemHSpace()F

    move-result v0

    return v0
.end method

.method public getLegendItemInnerSpace()F
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getItemInSpace()F

    move-result v0

    return v0
.end method

.method public getLegendItemWidth()F
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getItemWidth()F

    move-result v0

    return v0
.end method

.method public getLegendItemWidthSpace()F
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getItemWSpace()F

    move-result v0

    return v0
.end method

.method public getLegendProperty()Lcom/sec/dmc/sic/android/property/LegendProperty;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    return-object v0
.end method

.method public getLegendTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->getTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public getLegendVisible()Z
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/LegendProperty;->isVisible()Z

    move-result v0

    return v0
.end method

.method public getPadding([F)V
    .locals 2
    .param p1, "Padding"    # [F

    .prologue
    .line 156
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getPaddingLeft()F

    move-result v1

    aput v1, p1, v0

    .line 157
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getPaddingRight()F

    move-result v1

    aput v1, p1, v0

    .line 158
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getPaddingTop()F

    move-result v1

    aput v1, p1, v0

    .line 159
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getPaddingBottom()F

    move-result v1

    aput v1, p1, v0

    .line 160
    return-void
.end method

.method public getPositionX()F
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getPosX()F

    move-result v0

    return v0
.end method

.method public getPositionY()F
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getPosY()F

    move-result v0

    return v0
.end method

.method public getSeriesStyle(I)Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 453
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mSeriesStyles:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    :goto_0
    return-object v1

    .line 455
    :catch_0
    move-exception v0

    .line 457
    .local v0, "indexOutofException":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSeriesStyleSize()I
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mSeriesStyles:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getWidth()F
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/ChartProperty;->getWidth()F

    move-result v0

    return v0
.end method

.method protected makeGoalLinePostfixBitmap(Lcom/sec/dmc/sic/android/property/GoalLineProperty;)V
    .locals 0
    .param p1, "property"    # Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    .prologue
    .line 443
    return-void
.end method

.method public setChartBackgroundColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setChartBackgroundColor(IIII)V

    .line 169
    return-void
.end method

.method public setGraphBackgroundColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphBackgroundColor(IIII)V

    .line 190
    return-void
.end method

.method public setGraphBackgroundColors([I[F)V
    .locals 4
    .param p1, "colors"    # [I
    .param p2, "values"    # [F

    .prologue
    .line 243
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 245
    array-length v0, p1

    .line 246
    .local v0, "colorsSize":I
    array-length v1, p2

    .line 248
    .local v1, "valuesSize":I
    if-eq v0, v1, :cond_0

    .line 250
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "graph background colors set error"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 253
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v2, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphBackGroundColors([I)V

    .line 254
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v2, p2}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphBackGroundValues([F)V

    .line 257
    .end local v0    # "colorsSize":I
    .end local v1    # "valuesSize":I
    :cond_1
    return-void
.end method

.method public setGraphBackgroundImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphBackgroundImage(Landroid/graphics/Bitmap;)V

    .line 286
    return-void
.end method

.method public setGraphBackgroundTexts([Ljava/lang/String;[F[Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 5
    .param p1, "strs"    # [Ljava/lang/String;
    .param p2, "values"    # [F
    .param p3, "styles"    # [Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 214
    if-eqz p1, :cond_2

    if-eqz p3, :cond_2

    if-eqz p2, :cond_2

    .line 216
    array-length v0, p1

    .line 217
    .local v0, "strsSize":I
    array-length v1, p3

    .line 218
    .local v1, "stylesSize":I
    array-length v2, p2

    .line 220
    .local v2, "valuesSize":I
    if-ne v0, v1, :cond_0

    if-ne v0, v2, :cond_0

    .line 221
    if-eq v1, v2, :cond_1

    .line 223
    :cond_0
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "graph background texts set error"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 226
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v3, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphBackGroundTexts([Ljava/lang/String;)V

    .line 227
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v3, p2}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphBackGroundTextValues([F)V

    .line 228
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v3, p3}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphBackGroundTextStyles([Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 230
    .end local v0    # "strsSize":I
    .end local v1    # "stylesSize":I
    .end local v2    # "valuesSize":I
    :cond_2
    return-void
.end method

.method public setGraphSeparatorColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 348
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphSeparatorColor(I)V

    .line 349
    return-void
.end method

.method public setGraphSeparatorVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 377
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphSeparatorVisible(Z)V

    .line 378
    return-void
.end method

.method public setGraphSeparatorWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphSeparatorWidth(F)V

    .line 329
    return-void
.end method

.method public setGraphTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphTitleTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 309
    return-void
.end method

.method public setGraphTitleVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 368
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setGraphTitleVisible(Z)V

    .line 369
    return-void
.end method

.method public setLegendDirection(I)V
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 486
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setDirection(I)V

    .line 487
    return-void
.end method

.method public setLegendDisableColor(I)V
    .locals 5
    .param p1, "color"    # I

    .prologue
    .line 611
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setDisableColor(IIII)V

    .line 612
    return-void
.end method

.method public setLegendDisableItemBitmapArray(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 640
    .local p1, "bitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setItemDisableBitmap(Ljava/util/ArrayList;)V

    .line 641
    return-void
.end method

.method public setLegendItemBitmapArray(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 630
    .local p1, "bitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setItemBimtap(Ljava/util/ArrayList;)V

    .line 631
    return-void
.end method

.method public setLegendItemHeight(F)V
    .locals 1
    .param p1, "height"    # F

    .prologue
    .line 505
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setItemHeight(F)V

    .line 506
    return-void
.end method

.method public setLegendItemHeightSpace(F)V
    .locals 1
    .param p1, "space"    # F

    .prologue
    .line 525
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setItemHSpace(F)V

    .line 526
    return-void
.end method

.method public setLegendItemInnerSpace(F)V
    .locals 1
    .param p1, "space"    # F

    .prologue
    .line 535
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setItemInSpace(F)V

    .line 536
    return-void
.end method

.method public setLegendItemWidth(F)V
    .locals 1
    .param p1, "width"    # F

    .prologue
    .line 496
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setItemWidth(F)V

    .line 497
    return-void
.end method

.method public setLegendItemWidthSpace(F)V
    .locals 1
    .param p1, "space"    # F

    .prologue
    .line 515
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setItemWSpace(F)V

    .line 516
    return-void
.end method

.method public setLegendTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 649
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 650
    return-void
.end method

.method public setLegendVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 476
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mLegendProperty:Lcom/sec/dmc/sic/android/property/LegendProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->setVisible(Z)V

    .line 477
    return-void
.end method

.method public setMarkingLineBase(I)V
    .locals 1
    .param p1, "base"    # I

    .prologue
    .line 298
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setMarkingLineBase(I)V

    .line 299
    return-void
.end method

.method public setPadding(FFFF)V
    .locals 1
    .param p1, "lPadding"    # F
    .param p2, "rPadding"    # F
    .param p3, "tPadding"    # F
    .param p4, "bPadding"    # F

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setPaddingLeft(F)V

    .line 145
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setPaddingRight(F)V

    .line 146
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p3}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setPaddingTop(F)V

    .line 147
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p4}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setPaddingBottom(F)V

    .line 149
    return-void
.end method

.method public setPopupListener(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;

    .prologue
    .line 668
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setListener(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle$PopupListener;)V

    .line 669
    return-void
.end method

.method public setPosition(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setPosX(F)V

    .line 115
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setPosX(F)V

    .line 116
    return-void
.end method

.method public setWidthHeight(FF)V
    .locals 1
    .param p1, "w"    # F
    .param p2, "h"    # F

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setWidth(F)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;->mChartProperty:Lcom/sec/dmc/sic/android/property/ChartProperty;

    invoke-virtual {v0, p2}, Lcom/sec/dmc/sic/android/property/ChartProperty;->setHeight(F)V

    .line 87
    return-void
.end method
