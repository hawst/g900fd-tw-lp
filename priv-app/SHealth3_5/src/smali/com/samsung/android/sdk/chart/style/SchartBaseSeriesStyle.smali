.class public Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;
.super Ljava/lang/Object;
.source "SchartBaseSeriesStyle.java"


# static fields
.field public static final GRAPH_VALUE_LAST_ON_SCREEN:I = 0x2

.field public static final GRAPH_VALUE_ONLY_SELECTED:I = 0x4

.field public static final GRAPH_VALUE_SHOW_ALL:I = 0x1


# instance fields
.field protected mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

.field protected mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

.field protected mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

.field protected mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

.field protected mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

.field protected mSeriesType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    .line 36
    new-instance v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    .line 37
    new-instance v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    .line 38
    new-instance v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/SeriesProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    .line 39
    new-instance v0, Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-direct {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    .line 40
    return-void
.end method


# virtual methods
.method public GetEventIconListener()Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getListener()Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    move-result-object v0

    return-object v0
.end method

.method public GetEventIconOffGetX()F
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getOffsetX()F

    move-result v0

    return v0
.end method

.method public GetEventIconOffGetY()F
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getOffsetY()F

    move-result v0

    return v0
.end method

.method public GetEventIconVisible()Z
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getVisible()Z

    move-result v0

    return v0
.end method

.method public GetGoalEventIconOffGetX()F
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->getOffsetX()F

    move-result v0

    return v0
.end method

.method public GetGoalEventIconOffGetY()F
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->getOffsetY()F

    move-result v0

    return v0
.end method

.method public GetGoalEventIconVisible()Z
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->getVisible()Z

    move-result v0

    return v0
.end method

.method public GetGraphValueOffGetX()F
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getOffsetX()F

    move-result v0

    return v0
.end method

.method public GetGraphValueOffGetY()F
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getOffsetY()F

    move-result v0

    return v0
.end method

.method public GetGraphValuePostfix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getPostfix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public GetGraphValuePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getPrefix()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public GetGraphValueTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getTextStyle()Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    move-result-object v0

    return-object v0
.end method

.method public GetGraphValueVisible()Z
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getVisible()Z

    move-result v0

    return v0
.end method

.method public GetGraphVisible()Z
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->isVisible()Z

    move-result v0

    return v0
.end method

.method public GetseriesName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->getSereisName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChartValueProperty()Lcom/sec/dmc/sic/android/property/GraphValueProperty;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    return-object v0
.end method

.method public getEventIconProperty()Lcom/sec/dmc/sic/android/property/EventIconProperty;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    return-object v0
.end method

.method public getGoalEventIconProperty()Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    return-object v0
.end method

.method public getGraphProperty()Lcom/sec/dmc/sic/android/property/GraphProperty;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    return-object v0
.end method

.method public getSeriesProperty()Lcom/sec/dmc/sic/android/property/SeriesProperty;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    return-object v0
.end method

.method public getSeriesType()I
    .locals 1

    .prologue
    .line 471
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mSeriesType:I

    return v0
.end method

.method public setEventIconBitmapList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233
    .local p1, "bitmaps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setEventIconBitmapList(Ljava/util/ArrayList;)V

    .line 234
    return-void
.end method

.method public setEventIconListener(Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setListener(Lcom/samsung/android/sdk/chart/style/SchartEventIconListener;)V

    .line 223
    return-void
.end method

.method public setEventIconOffsetX(F)V
    .locals 1
    .param p1, "offsetX"    # F

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setOffsetX(F)V

    .line 201
    return-void
.end method

.method public setEventIconOffsetY(F)V
    .locals 1
    .param p1, "offsetX"    # F

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setOffsetY(F)V

    .line 212
    return-void
.end method

.method public setEventIconScalable(Z)V
    .locals 1
    .param p1, "scalable"    # Z

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setScalable(Z)V

    .line 245
    return-void
.end method

.method public setEventIconVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mEventIconProperty:Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setVisible(Z)V

    .line 190
    return-void
.end method

.method public setGoalEventIconBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 289
    return-void
.end method

.method public setGoalEventIconOffsetX(F)V
    .locals 1
    .param p1, "offsetX"    # F

    .prologue
    .line 266
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setOffsetX(F)V

    .line 267
    return-void
.end method

.method public setGoalEventIconOffsetY(F)V
    .locals 1
    .param p1, "offsetX"    # F

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setOffsetY(F)V

    .line 278
    return-void
.end method

.method public setGoalEventIconScalable(Z)V
    .locals 1
    .param p1, "scalable"    # Z

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setScalable(Z)V

    .line 300
    return-void
.end method

.method public setGoalEventIconVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGoalEventIconProperty:Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setVisible(Z)V

    .line 256
    return-void
.end method

.method public setGraphValueOffsetX(F)V
    .locals 1
    .param p1, "offsetX"    # F

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setOffsetX(F)V

    .line 125
    return-void
.end method

.method public setGraphValueOffsetY(F)V
    .locals 1
    .param p1, "offsetX"    # F

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setOffsetY(F)V

    .line 136
    return-void
.end method

.method public setGraphValuePostfix(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setPostfix(Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public setGraphValuePrefix(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setPrefix(Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public setGraphValueSelectedShowItems(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    .local p1, "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setSelectedShowItemList(Ljava/util/ArrayList;)V

    .line 169
    return-void
.end method

.method public setGraphValueTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V
    .locals 1
    .param p1, "textStyle"    # Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setTextStyle(Lcom/samsung/android/sdk/chart/style/SchartTextStyle;)V

    .line 114
    return-void
.end method

.method public setGraphValueVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setVisible(Z)V

    .line 102
    return-void
.end method

.method public setGraphValueVisibleOption(I)V
    .locals 1
    .param p1, "visibleOption"    # I

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphValueProperty:Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setVisibleOption(I)V

    .line 179
    return-void
.end method

.method public setGraphVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mGraphProperty:Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setVisible(Z)V

    .line 311
    return-void
.end method

.method public setSeriesName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mSeriesProperty:Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual {v0, p1}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setSereisName(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method public setSeriesType(I)V
    .locals 0
    .param p1, "seriesType"    # I

    .prologue
    .line 475
    iput p1, p0, Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;->mSeriesType:I

    .line 476
    return-void
.end method
