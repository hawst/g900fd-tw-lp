.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$GoalSubType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GoalSubType"
.end annotation


# static fields
.field public static final HALF_KG_ONE_WEEK:I = 0xc351

.field public static final HALF_KG_TWO_WEEKS:I = 0xc353

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final ONE_KG_ONE_WEEK:I = 0xc352

.field public static final ONE_KG_TWO_WEEKS:I = 0xc354
