.class public interface abstract Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorConstants;
.super Ljava/lang/Object;
.source "PrivilegeSensorConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorConstants$TemperatureType;,
        Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorConstants$TemperatureUnit;,
        Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorConstants$BloodGlucoseSampleType;,
        Lcom/samsung/android/sdk/health/sensor/PrivilegeSensorConstants$BloodGlucosetUnit;
    }
.end annotation


# static fields
.field public static final BLOOD_GLUCOSE:I = 0x2714

.field public static final BODY_TEMPERATURE:I = 0x2716

.field public static final CONNECTIVITY_TYPE_ALL:I = 0x0

.field public static final CONNECTIVITY_TYPE_INTERNAL:I = 0x1

.field public static final DATA_TYPE_ALL:I = 0x0

.field public static final DATA_TYPE_BLOODGLUCOSE:I = 0x3

.field public static final DATA_TYPE_BODYTEMPERATURE:I = 0x7

.field public static final DATA_TYPE_ELECTROCARDIOGRAM:I = 0x4

.field public static final DATA_TYPE_HEARTRATEMONITOR:I = 0x1

.field public static final DATA_TYPE_PULSEOXIMETER:I = 0x2

.field public static final DEVICE_TYPE_ALL:I = 0x0

.field public static final ECG_ELECTROWAVE:Ljava/lang/String; = "ecg_electrowave"

.field public static final ECG_HEARTRATE:Ljava/lang/String; = "ecg_heartrate"

.field public static final ECG_TIME:Ljava/lang/String; = "ecg_time"

.field public static final ELECTROCARDIOGRAM:I = 0x2729

.field public static final GLUCOSE:Ljava/lang/String; = "glucose"

.field public static final GLUCOSE_ERROR_DETAIL:Ljava/lang/String; = "glucose_error_detail"

.field public static final GLUCOSE_SENSOR_STATE:Ljava/lang/String; = "glucose_sensor_state"

.field public static final GLUCOSE_UNIT:Ljava/lang/String; = "glucose_unit"

.field public static final HEART_RATE:Ljava/lang/String; = "heart_rate"

.field public static final HEART_RATE_INTERVAL:Ljava/lang/String; = "heart_rate_interval"

.field public static final HEART_RATE_MONITOR:I = 0x2718

.field public static final HEART_RATE_SNR:Ljava/lang/String; = "heart_rate_snr"

.field public static final HEART_RATE_UNIT:Ljava/lang/String; = "heart_rate_unit"

.field public static final MAX_CONNECTIVITY:I = 0x2

.field public static final MIN_CONNECTIVITY:I = 0x0

.field public static final PULSEOXIMETER:I = 0x272a

.field public static final PULSE_OXIMETRY:Ljava/lang/String; = "pulse_oximetry"

.field public static final SAMPLE_TYPE:Ljava/lang/String; = "sample_type"

.field public static final TEMPERATURE:Ljava/lang/String; = "temperature"

.field public static final TEMPERATURE_TYPE:Ljava/lang/String; = "temperature_type"

.field public static final TEMPERATURE_UNIT:Ljava/lang/String; = "temperature_unit"

.field public static final TIME_STAMP:Ljava/lang/String; = "time_stamp"
