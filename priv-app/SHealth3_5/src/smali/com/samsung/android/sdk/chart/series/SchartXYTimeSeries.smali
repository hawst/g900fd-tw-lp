.class public Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;
.super Lcom/samsung/android/sdk/chart/series/SchartSeries;
.source "SchartXYTimeSeries.java"


# instance fields
.field protected mDatas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/android/sdk/chart/series/SchartSeries;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    .line 19
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mSeriesDataType:I

    .line 20
    return-void
.end method


# virtual methods
.method public add(JD)V
    .locals 2
    .param p1, "xValue"    # J
    .param p3, "yValue"    # D

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;-><init>(JD)V

    .line 32
    .local v0, "timeData":Lcom/samsung/android/sdk/chart/series/SchartTimeData;
    iget-object v1, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method public add(Lcom/samsung/android/sdk/chart/series/SchartTimeData;)V
    .locals 1
    .param p1, "timeData"    # Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    return-void
.end method

.method public getAllData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getX(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 54
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 56
    :cond_0
    const-wide/16 v0, -0x1

    .line 58
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->getTime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getY(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 69
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 71
    :cond_0
    const-wide/16 v0, 0x1

    .line 73
    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;

    iget-wide v0, v0, Lcom/samsung/android/sdk/chart/series/SchartTimeData;->mValue:D

    goto :goto_0
.end method

.method public setAllData(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/chart/series/SchartTimeData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "datas":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/sdk/chart/series/SchartTimeData;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/series/SchartXYTimeSeries;->mDatas:Ljava/util/List;

    .line 100
    return-void
.end method
