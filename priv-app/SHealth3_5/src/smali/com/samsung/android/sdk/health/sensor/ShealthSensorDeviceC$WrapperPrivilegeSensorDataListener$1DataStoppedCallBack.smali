.class Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;
.super Ljava/lang/Object;
.source "ShealthSensorDeviceC.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->onStopped(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataStoppedCallBack"
.end annotation


# instance fields
.field dataType:I

.field error:I

.field final synthetic this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;II)V
    .locals 0
    .param p2, "dataType"    # I
    .param p3, "error"    # I

    .prologue
    .line 214
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    iput p2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->dataType:I

    .line 216
    iput p3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->error:I

    .line 217
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 221
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->error:I

    if-nez v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STOPPED_SUCCESS:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    .line 227
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->mDataManager:Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getDeviceId(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getType()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->getDataType()Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/health/sensor/_private/_IDataManager;->stop(Ljava/lang/String;ILjava/util/List;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->listener:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->dataType:I

    iget v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->error:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$DataListener;->onStopped(II)V

    .line 231
    return-void

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener$1DataStoppedCallBack;->this$1:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;

    iget-object v0, v0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC$WrapperPrivilegeSensorDataListener;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;->ON_DATA_STOPPED_FAIL:Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDeviceC;->processStateChangedEvent(Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$StateTransitionEvent;)V

    goto :goto_0
.end method
