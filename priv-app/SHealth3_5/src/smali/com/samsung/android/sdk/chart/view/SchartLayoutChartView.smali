.class public Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartXYChartView;
.source "SchartLayoutChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;)V

    .line 53
    const/16 v0, 0x9

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    const/16 v0, 0x9

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 67
    return-void
.end method


# virtual methods
.method public setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V
    .locals 1
    .param p1, "interaction"    # Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setScaleZoomInteractionEnabled(Z)V

    .line 79
    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;->setPatialZoomInteractionEnabled(Z)V

    .line 80
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartInteraction(Lcom/samsung/android/sdk/chart/interaction/SchartInteraction;)V

    .line 81
    return-void
.end method

.method public setGraphType(II)V
    .locals 2
    .param p1, "seriesId"    # I
    .param p2, "type"    # I

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartLayoutChartView;->mChartHandle:J

    invoke-static {v0, v1, p1, p2}, Lcom/sec/dmc/sic/jni/ChartJNI;->setGraphType(JII)V

    .line 73
    return-void
.end method
