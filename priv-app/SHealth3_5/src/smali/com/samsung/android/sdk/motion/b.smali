.class final Lcom/samsung/android/sdk/motion/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# instance fields
.field private synthetic a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification;

.field private final synthetic b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/motion/SmotionActivityNotification;Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/motion/b;->a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification;

    iput-object p2, p0, Lcom/samsung/android/sdk/motion/b;->b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 5

    iget-object v0, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    invoke-virtual {v0}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v0

    const/16 v1, 0x1b

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getActivityNotificationContext()Landroid/hardware/scontext/SContextActivityNotification;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityNotification;->getStatus()I

    move-result v1

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityNotification;->getAccuracy()I

    move-result v2

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextActivityNotification;->getTimeStamp()J

    move-result-wide v3

    iget-object v0, p0, Lcom/samsung/android/sdk/motion/b;->a:Lcom/samsung/android/sdk/motion/SmotionActivityNotification;

    invoke-static {v0, v3, v4, v1, v2}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification;->a(Lcom/samsung/android/sdk/motion/SmotionActivityNotification;JII)Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/motion/b;->b:Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/motion/SmotionActivityNotification$ChangeListener;->onChanged(Lcom/samsung/android/sdk/motion/SmotionActivityNotification$Info;)V

    :cond_0
    return-void
.end method
