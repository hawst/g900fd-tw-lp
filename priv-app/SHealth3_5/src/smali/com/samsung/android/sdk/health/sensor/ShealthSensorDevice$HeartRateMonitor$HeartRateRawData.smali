.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;
.super Ljava/lang/Object;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HeartRateRawData"
.end annotation


# instance fields
.field public heartRate:I

.field public samplingTime:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->samplingTime:J

    .line 2208
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$HeartRateMonitor$HeartRateRawData;->heartRate:I

    return-void
.end method
