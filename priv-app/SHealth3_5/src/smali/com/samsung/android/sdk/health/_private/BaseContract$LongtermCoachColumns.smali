.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$LongtermCoachColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LongtermCoachColumns"
.end annotation


# static fields
.field public static final COMPLETED:Ljava/lang/String; = "completed"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final PHRASE_NUMBER:Ljava/lang/String; = "phrase_number"

.field public static final TRAINING_EFFECT:Ljava/lang/String; = "training_effect"
