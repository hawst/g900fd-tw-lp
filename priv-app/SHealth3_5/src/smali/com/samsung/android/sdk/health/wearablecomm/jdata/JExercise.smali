.class public Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExercise;
.super Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHealth;
.source "JExercise.java"


# instance fields
.field public calorie:D

.field public coachingResult:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JCoachingResult;

.field public crud:I

.field public devicePkId:Ljava/lang/String;

.field public distance:D

.field public duration:J

.field public exerciseGoal:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseGoal;

.field public extraExercise:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JExerciseBundle;

.field public fatBurnTime:J

.field public fitnessLevel:I

.field public heartRate:D

.field public location:[Lcom/samsung/android/sdk/health/wearablecomm/jdata/JLocation;

.field public recoveryTime:J

.field public time:J

.field public trainingEffect:F

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHealth;-><init>()V

    return-void
.end method
