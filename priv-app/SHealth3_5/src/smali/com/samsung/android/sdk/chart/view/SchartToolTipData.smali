.class public Lcom/samsung/android/sdk/chart/view/SchartToolTipData;
.super Ljava/lang/Object;
.source "SchartToolTipData.java"


# instance fields
.field pieRadius:F

.field pieThetaBasedXAxis:F

.field private posX:F

.field private posYList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private seriesId:I

.field private seriesXValue:Ljava/lang/String;

.field private seriesYValueList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->posX:F

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesId:I

    .line 18
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->posYList:Ljava/util/Vector;

    .line 19
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesYValueList:Ljava/util/Vector;

    .line 20
    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->pieThetaBasedXAxis:F

    .line 21
    iput v1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->pieRadius:F

    .line 22
    return-void
.end method


# virtual methods
.method public getPieRadius()F
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->pieRadius:F

    return v0
.end method

.method public getPieThetaBasedXAxis()F
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->pieThetaBasedXAxis:F

    return v0
.end method

.method public getPosX()F
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->posX:F

    return v0
.end method

.method public getPosYList()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->posYList:Ljava/util/Vector;

    return-object v0
.end method

.method public getSeriesId()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesId:I

    return v0
.end method

.method public getSeriesXValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesXValue:Ljava/lang/String;

    return-object v0
.end method

.method public getSeriesYValueList()Ljava/util/Vector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesYValueList:Ljava/util/Vector;

    return-object v0
.end method

.method public setPieRadius(F)V
    .locals 0
    .param p1, "pieRadius"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->pieRadius:F

    .line 38
    return-void
.end method

.method public setPieThetaBasedXAxis(F)V
    .locals 0
    .param p1, "pieThetaBasedXAxis"    # F

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->pieThetaBasedXAxis:F

    .line 30
    return-void
.end method

.method public setPosX(F)V
    .locals 0
    .param p1, "posX"    # F

    .prologue
    .line 45
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->posX:F

    .line 46
    return-void
.end method

.method public setPosYList(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "posYList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Float;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->posYList:Ljava/util/Vector;

    .line 70
    return-void
.end method

.method public setSeriesId(I)V
    .locals 0
    .param p1, "seriesId"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesId:I

    .line 54
    return-void
.end method

.method public setSeriesXValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "seriesXValue"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesXValue:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setSeriesYValueList(Ljava/util/Vector;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Double;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "seriesYValueList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Double;>;"
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/view/SchartToolTipData;->seriesYValueList:Ljava/util/Vector;

    .line 78
    return-void
.end method
