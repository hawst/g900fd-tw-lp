.class public Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;
.source "_ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Health;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "_Electrocardiogram"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1914
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1889
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;-><init>()V

    .line 1890
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1893
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;-><init>()V

    .line 1894
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->readFromParcel(Landroid/os/Parcel;)V

    .line 1895
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1929
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->time:J

    .line 1930
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->heartRate:I

    .line 1931
    invoke-virtual {p1}, Landroid/os/Parcel;->createFloatArray()[F

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->electroWave:[F

    .line 1932
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1900
    const/4 v0, 0x0

    return v0
.end method

.method public getDataType()I
    .locals 1

    .prologue
    .line 1937
    const/16 v0, 0x10

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1944
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1945
    .local v0, "ewRes":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 1947
    .local v1, "ewResStr":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->electroWave:[F

    if-eqz v3, :cond_0

    .line 1948
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->electroWave:[F

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 1949
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->electroWave:[F

    aget v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1948
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1952
    .end local v2    # "i":I
    :cond_0
    if-eqz v0, :cond_1

    .line 1953
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1957
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Electrocardiogram] time ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->time:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->time:J

    invoke-static {v4, v5}, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$TimeChange;->changeTimeToString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", heartRate ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->heartRate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", electroWave ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1906
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->time:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1907
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->heartRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1908
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/_private/_ShealthSensorDevice$_Electrocardiogram;->electroWave:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 1909
    return-void
.end method
