.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$CaloriesUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$DistanceUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$SpeedUnit;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Fitness"
.end annotation


# instance fields
.field public cadence:J

.field public cadencePerMinute:F

.field public calorieBurnRate:F

.field public cumulativeAltitudeGain:D

.field public cumulativeAltitudeLoss:D

.field public cumulativeCalories:F

.field public cumulativeCaloriesUnit:I

.field public cumulativeDistance:F

.field public cumulativeDistanceUnit:I

.field public cycleLength:F

.field public duration:J

.field public equipmentType:I

.field public inclinePercent:F

.field public instantaneousHeartRate:I

.field public instantaneousSpeed:F

.field public intantaneousMet:F

.field public maxCadence:J

.field public maxHeartRate:I

.field public maxSpeed:F

.field public meanCadence:F

.field public meanHeartRate:F

.field public meanSpeed:F

.field public power:F

.field public resistanceLevel:I

.field public speedUnit:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7fffffff

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 2415
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2418
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->equipmentType:I

    .line 2421
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->duration:J

    .line 2424
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeDistance:F

    .line 2431
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeDistanceUnit:I

    .line 2434
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->instantaneousSpeed:F

    .line 2437
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->speedUnit:I

    .line 2440
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->instantaneousHeartRate:I

    .line 2443
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cycleLength:F

    .line 2446
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->inclinePercent:F

    .line 2449
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->resistanceLevel:I

    .line 2452
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->intantaneousMet:F

    .line 2455
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->calorieBurnRate:F

    .line 2458
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeCalories:F

    .line 2461
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeCaloriesUnit:I

    .line 2464
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cadencePerMinute:F

    .line 2467
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cadence:J

    .line 2470
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeAltitudeGain:D

    .line 2473
    iput-wide v4, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->cumulativeAltitudeLoss:D

    .line 2476
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->meanSpeed:F

    .line 2479
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->maxSpeed:F

    .line 2482
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->meanHeartRate:F

    .line 2485
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->maxHeartRate:I

    .line 2488
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->meanCadence:F

    .line 2491
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->maxCadence:J

    .line 2494
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Fitness;->power:F

    return-void
.end method
