.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$ServiceType;
.super Ljava/lang/Object;
.source "BaseContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ServiceType"
.end annotation


# static fields
.field public static final BLOOD_GLUCOSE:I = 0x4

.field public static final BLOOD_PRESSURE:I = 0x5

.field public static final BODY_TEMPERATURE:I = 0xd

.field public static final CAREGIVER:I = 0x10

.field public static final CHALLENGE:I = 0xf

.field public static final CIGNA_COACH:I = 0x11

.field public static final DOSAGE:I = 0xe

.field public static final EXERCISE_MATE:I = 0x2

.field public static final EXERCISE_PRO:I = 0x1

.field public static final FOOD:I = 0x7

.field public static final HEART_BEAT:I = 0x9

.field public static final NOT_DEFINED:I = -0x1

.field public static final PEDOMETER:I = 0x6

.field public static final SLEEP:I = 0xb

.field public static final STRESS:I = 0xa

.field public static final THERMO_HYGROMETER:I = 0xc

.field public static final WATER_INTAKE:I = 0x8

.field public static final WEIGHT:I = 0x3
