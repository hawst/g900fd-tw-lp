.class public interface abstract Lcom/samsung/android/sdk/health/_private/BaseContract$ChallengesMemberColumns;
.super Ljava/lang/Object;
.source "BaseContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/_private/BaseContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ChallengesMemberColumns"
.end annotation


# static fields
.field public static final BUDDY_INFO_ID:Ljava/lang/String; = "buddy_info__id"

.field public static final CHALLENGES_INFO_ID:Ljava/lang/String; = "challenges_info__id"

.field public static final MEMBER_INITAL_VALUE:Ljava/lang/String; = "member_inital_value"

.field public static final MEMBER_RANKING:Ljava/lang/String; = "member_ranking"

.field public static final MEMBER_RECORD:Ljava/lang/String; = "member_record"

.field public static final MEMBER_STATE:Ljava/lang/String; = "member_state"
