.class public final Lcom/samsung/android/sdk/health/_private/BaseContract$PrivateConstants$CPConstants;
.super Ljava/lang/Object;
.source "BaseContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract$PrivateConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CPConstants"
.end annotation


# static fields
.field public static final APP_REG_REPLACE:Ljava/lang/String; = "app_registry_replace"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final DB_FILE_READ_NORMAL:Ljava/lang/String; = "Shealthdb_read"

.field public static final DB_FILE_READ_SECURE:Ljava/lang/String; = "Shealth_securedb_read"

.field public static final MIGRATION_RAWQUERY:Ljava/lang/String; = "migration"

.field public static final RAWQUERY:Ljava/lang/String; = "rawquery"

.field public static final SHARED_PREF_FILE_READ:Ljava/lang/String; = "Shared_prefs_read"

.field public static final TIME_ZONE:Ljava/lang/String; = "time_zone"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
