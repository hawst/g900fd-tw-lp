.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Electrocardiogram"
.end annotation


# instance fields
.field public electroWave:[F

.field public heartRate:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2500
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2503
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;->heartRate:I

    .line 2506
    const/4 v0, 0x1

    new-array v0, v0, [F

    const/4 v1, 0x0

    const v2, 0x7f7fffff    # Float.MAX_VALUE

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Electrocardiogram;->electroWave:[F

    return-void
.end method
