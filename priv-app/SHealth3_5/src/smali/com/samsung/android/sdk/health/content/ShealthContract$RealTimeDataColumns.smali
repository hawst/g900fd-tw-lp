.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$RealTimeDataColumns;
.super Ljava/lang/Object;
.source "ShealthContract.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RealTimeDataColumns"
.end annotation


# static fields
.field public static final APPLICATION__ID:Ljava/lang/String; = "application__id"

.field public static final CADENCE_RATE_PER_MIN:Ljava/lang/String; = "cadence_rate_per_min"

.field public static final CALORIE_BURN_PER_MIN:Ljava/lang/String; = "calorie_burn_per_min"

.field public static final DATA_TYPE:Ljava/lang/String; = "data_type"

.field public static final EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field public static final HEART_RATE_PER_MIN:Ljava/lang/String; = "heart_rate_per_min"

.field public static final INCLINE:Ljava/lang/String; = "incline"

.field public static final LAST_HEART_BEAT_TIME:Ljava/lang/String; = "last_heart_beat_time"

.field public static final MET:Ljava/lang/String; = "met"

.field public static final POWER_WATT:Ljava/lang/String; = "power_watt"

.field public static final RESISTANCE:Ljava/lang/String; = "resistance"

.field public static final SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field public static final SPEED_PER_HOUR:Ljava/lang/String; = "speed_per_hour"

.field public static final STRIDE_LENGTH:Ljava/lang/String; = "stride_length"

.field public static final USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"
