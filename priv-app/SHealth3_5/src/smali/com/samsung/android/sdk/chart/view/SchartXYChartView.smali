.class public Lcom/samsung/android/sdk/chart/view/SchartXYChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;
.source "SchartXYChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;-><init>(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartChartBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    return-void
.end method


# virtual methods
.method public getChartStyle()Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    check-cast v0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    return-object v0
.end method

.method protected makeAxisTitleBitmap(Lcom/sec/dmc/sic/android/property/AxisProperty;)V
    .locals 0
    .param p1, "property"    # Lcom/sec/dmc/sic/android/property/AxisProperty;

    .prologue
    .line 398
    return-void
.end method

.method public setChartStyle(Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;)V
    .locals 23
    .param p1, "style"    # Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    .prologue
    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->processProperty:Ljava/util/Vector;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 71
    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChartStyle:Lcom/samsung/android/sdk/chart/style/SchartBaseChartStyle;

    .line 73
    move-object/from16 v0, p1

    check-cast v0, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;

    move-object v2, v0

    .line 75
    .local v2, "XYChartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    move-object/from16 v20, v0

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getChartProperty()Lcom/sec/dmc/sic/android/property/ChartProperty;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/ChartProperty;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v20

    if-nez v20, :cond_0

    .line 78
    :try_start_1
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getChartProperty()Lcom/sec/dmc/sic/android/property/ChartProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/ChartProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/ChartProperty;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    :goto_0
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChart:Lcom/sec/dmc/sic/android/property/ChartProperty;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 87
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfXAxis()I

    move-result v16

    .line 88
    .local v16, "xAxisCnt":I
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getNumberOfYAxis()I

    move-result v18

    .line 90
    .local v18, "yAxisCnt":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move/from16 v0, v16

    if-lt v10, v0, :cond_5

    .line 123
    const/4 v10, 0x0

    :goto_2
    move/from16 v0, v18

    if-lt v10, v0, :cond_8

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    move-object/from16 v20, v0

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getGridLineProperty()Lcom/sec/dmc/sic/android/property/GridLineProperty;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v20

    if-nez v20, :cond_1

    .line 164
    :try_start_3
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getGridLineProperty()Lcom/sec/dmc/sic/android/property/GridLineProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GridLineProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GridLineProperty;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;
    :try_end_3
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 170
    :goto_3
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGridLine:Lcom/sec/dmc/sic/android/property/GridLineProperty;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 173
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    move-object/from16 v20, v0

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getInfoProperty()Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v20

    if-nez v20, :cond_2

    .line 176
    :try_start_5
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getInfoProperty()Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/InfoPointProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;
    :try_end_5
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 182
    :goto_4
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mInfoPoint:Lcom/sec/dmc/sic/android/property/InfoPointProperty;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 185
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    move-object/from16 v20, v0

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerProperty()Lcom/sec/dmc/sic/android/property/HandlerProperty;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->equals(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v20

    if-nez v20, :cond_3

    .line 188
    :try_start_7
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerProperty()Lcom/sec/dmc/sic/android/property/HandlerProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/HandlerProperty;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;
    :try_end_7
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_7 .. :try_end_7} :catch_5
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 194
    :goto_5
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mHandler:Lcom/sec/dmc/sic/android/property/HandlerProperty;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->resetHandlerFadeInOut()V

    .line 197
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getHandlerProperty()Lcom/sec/dmc/sic/android/property/HandlerProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/HandlerProperty;->resetHandlerFadeInOut()V

    .line 200
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeriesStyleSize()I

    move-result v14

    .line 202
    .local v14, "seriesStyleCnt":I
    const/4 v10, 0x0

    :goto_6
    if-lt v10, v14, :cond_b

    .line 342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    move-object/from16 v20, v0

    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getLegendProperty()Lcom/sec/dmc/sic/android/property/LegendProperty;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/LegendProperty;->equals(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result v20

    if-nez v20, :cond_4

    .line 345
    :try_start_9
    invoke-virtual {v2}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getLegendProperty()Lcom/sec/dmc/sic/android/property/LegendProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/LegendProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/LegendProperty;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;
    :try_end_9
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_9 .. :try_end_9} :catch_e
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 350
    :goto_7
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mLegend:Lcom/sec/dmc/sic/android/property/LegendProperty;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 70
    :cond_4
    monitor-exit v21

    .line 354
    return-void

    .line 79
    .end local v10    # "i":I
    .end local v14    # "seriesStyleCnt":I
    .end local v16    # "xAxisCnt":I
    .end local v18    # "yAxisCnt":I
    :catch_0
    move-exception v7

    .line 81
    .local v7, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_0

    .line 70
    .end local v2    # "XYChartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catchall_0
    move-exception v20

    monitor-exit v21
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    throw v20

    .line 91
    .restart local v2    # "XYChartStyle":Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;
    .restart local v10    # "i":I
    .restart local v16    # "xAxisCnt":I
    .restart local v18    # "yAxisCnt":I
    :cond_5
    :try_start_b
    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getXAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-result-object v17

    .line 93
    .local v17, "xAxisProperty":Lcom/sec/dmc/sic/android/property/AxisProperty;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result v20

    if-nez v20, :cond_6

    .line 96
    :try_start_c
    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getXAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-object/from16 v17, v0
    :try_end_c
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 102
    :goto_8
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->size()I

    move-result v20

    move/from16 v0, v20

    if-lt v10, v0, :cond_7

    .line 103
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 105
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v12, v0, [I

    const/16 v20, 0x0

    aput v10, v12, v20

    .line 106
    .local v12, "seiresIDs":[I
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V

    .line 107
    const/16 v20, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 119
    .end local v12    # "seiresIDs":[I
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 90
    :cond_6
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 97
    :catch_1
    move-exception v7

    .line 99
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_8

    .line 111
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAxisID()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 113
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAssociatedSeriesIDs()[I

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V

    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAssociatedSeriesIDsCount()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mXAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v10, v1}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_9

    .line 125
    .end local v17    # "xAxisProperty":Lcom/sec/dmc/sic/android/property/AxisProperty;
    :cond_8
    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getYAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-result-object v19

    .line 127
    .local v19, "yAxisProperty":Lcom/sec/dmc/sic/android/property/AxisProperty;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/dmc/sic/android/property/AxisProperty;->equals(Ljava/lang/Object;)Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    move-result v20

    if-nez v20, :cond_9

    .line 130
    :try_start_e
    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getYAxisProperty(I)Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/AxisProperty;

    move-object/from16 v19, v0

    .line 131
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->makeAxisTitleBitmap(Lcom/sec/dmc/sic/android/property/AxisProperty;)V
    :try_end_e
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 138
    :goto_a
    :try_start_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Vector;->size()I

    move-result v20

    move/from16 v0, v20

    if-lt v10, v0, :cond_a

    .line 139
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 141
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v12, v0, [I

    const/16 v20, 0x0

    aput v10, v12, v20

    .line 142
    .restart local v12    # "seiresIDs":[I
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V

    .line 143
    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 144
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 156
    .end local v12    # "seiresIDs":[I
    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 123
    :cond_9
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    .line 132
    :catch_2
    move-exception v7

    .line 134
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_a

    .line 147
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAxisID()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAxisID(I)V

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAssociatedSeriesIDs()[I

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDs([I)V

    .line 150
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/AxisProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->getAssociatedSeriesIDsCount()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Lcom/sec/dmc/sic/android/property/AxisProperty;->setAssociatedSeriesIDsCount(I)V

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mYAxisVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v10, v1}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 165
    .end local v19    # "yAxisProperty":Lcom/sec/dmc/sic/android/property/AxisProperty;
    :catch_3
    move-exception v7

    .line 167
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_3

    .line 177
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_4
    move-exception v7

    .line 179
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_4

    .line 189
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_5
    move-exception v7

    .line 191
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_5

    .line 203
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v14    # "seriesStyleCnt":I
    :cond_b
    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/chart/style/SchartXYChartStyle;->getSeriesStyle(I)Lcom/samsung/android/sdk/chart/style/SchartBaseSeriesStyle;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;

    .line 204
    .local v3, "XYSeriesStyle":Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChartType:I

    move/from16 v20, v0

    const/16 v22, 0x5

    move/from16 v0, v20

    move/from16 v1, v22

    if-eq v0, v1, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChartType:I

    move/from16 v20, v0

    const/16 v22, 0x9

    move/from16 v0, v20

    move/from16 v1, v22

    if-eq v0, v1, :cond_c

    .line 206
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getSeriesType()I

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mChartType:I

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    if-eq v0, v1, :cond_c

    .line 207
    new-instance v20, Ljava/lang/ClassCastException;

    const-string v22, "SeriesStyle and chartType mismatch"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v20

    .line 210
    :cond_c
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getSeriesProperty()Lcom/sec/dmc/sic/android/property/SeriesProperty;

    move-result-object v13

    .line 211
    .local v13, "seriesProperty":Lcom/sec/dmc/sic/android/property/SeriesProperty;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getNormalRangeProperty()Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    move-result-object v11

    .line 212
    .local v11, "normalRangeProperty":Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getValueMarkingProperty()Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    move-result-object v15

    .line 213
    .local v15, "valueMarkingProperty":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getGoalLineProperty()Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    move-result-object v8

    .line 214
    .local v8, "goalLineProperty":Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getGraphProperty()Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-result-object v9

    .line 215
    .local v9, "graphProperty":Lcom/sec/dmc/sic/android/property/GraphProperty;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getChartValueProperty()Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    move-result-object v6

    .line 216
    .local v6, "chartValueProeprty":Lcom/sec/dmc/sic/android/property/GraphValueProperty;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getEventIconProperty()Lcom/sec/dmc/sic/android/property/EventIconProperty;

    move-result-object v4

    .line 217
    .local v4, "chartEventIconProeprty":Lcom/sec/dmc/sic/android/property/EventIconProperty;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getGoalEventIconProperty()Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    move-result-object v5

    .line 219
    .local v5, "chartGoalEventIconProeprty":Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mSeriesVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->getSeriesID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->setSeriesID(I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mSeriesVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->equals(Ljava/lang/Object;)Z
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    move-result v20

    if-nez v20, :cond_d

    .line 223
    :try_start_10
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getSeriesProperty()Lcom/sec/dmc/sic/android/property/SeriesProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/SeriesProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/SeriesProperty;

    move-object v13, v0
    :try_end_10
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 229
    :goto_c
    :try_start_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mSeriesVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v13}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 230
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mSeriesVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 234
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mNormalRangeVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->getNormalRangeID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->setNormalRangeID(I)V

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mNormalRangeVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->equals(Ljava/lang/Object;)Z
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    move-result v20

    if-nez v20, :cond_e

    .line 238
    :try_start_12
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getNormalRangeProperty()Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/NormalRangeProperty;

    move-object v11, v0
    :try_end_12
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_12 .. :try_end_12} :catch_7
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 244
    :goto_d
    :try_start_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mNormalRangeVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v11}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mNormalRangeVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 249
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mValueMarkingVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->getValueMarkingID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->setValueMarkingID(I)V

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mValueMarkingVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->equals(Ljava/lang/Object;)Z
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    move-result v20

    if-nez v20, :cond_f

    .line 253
    :try_start_14
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getValueMarkingProperty()Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;

    move-object v15, v0
    :try_end_14
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_14 .. :try_end_14} :catch_8
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 259
    :goto_e
    :try_start_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mValueMarkingVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v15}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mValueMarkingVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 263
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalLineVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->getGoalLineID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->setGoalLineID(I)V

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalLineVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->equals(Ljava/lang/Object;)Z
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    move-result v20

    if-nez v20, :cond_10

    .line 267
    :try_start_16
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getGoalLineProperty()Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GoalLineProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/GoalLineProperty;

    move-object v8, v0
    :try_end_16
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_16 .. :try_end_16} :catch_9
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 274
    :goto_f
    :try_start_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalLineVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v8}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalLineVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 278
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GraphProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GraphProperty;->getGraphID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Lcom/sec/dmc/sic/android/property/GraphProperty;->setGraphID(I)V

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/sec/dmc/sic/android/property/GraphProperty;->equals(Ljava/lang/Object;)Z
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    move-result v20

    if-nez v20, :cond_11

    .line 282
    :try_start_18
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getGraphProperty()Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GraphProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphProperty;

    move-object v9, v0
    :try_end_18
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_18 .. :try_end_18} :catch_a
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 289
    :goto_10
    :try_start_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v9}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 293
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphValueVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->getSeriesID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->setSeriesID(I)V

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphValueVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->equals(Ljava/lang/Object;)Z
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    move-result v20

    if-nez v20, :cond_12

    .line 297
    :try_start_1a
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getChartValueProperty()Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GraphValueProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/GraphValueProperty;

    move-object v6, v0
    :try_end_1a
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1a .. :try_end_1a} :catch_b
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    .line 304
    :goto_11
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphValueVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v6}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 305
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGraphValueVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 308
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->getSeriesID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->setSeriesID(I)V

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->equals(Ljava/lang/Object;)Z
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    move-result v20

    if-nez v20, :cond_13

    .line 312
    :try_start_1c
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getEventIconProperty()Lcom/sec/dmc/sic/android/property/EventIconProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/EventIconProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/EventIconProperty;

    move-object v4, v0
    :try_end_1c
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1c .. :try_end_1c} :catch_c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    .line 319
    :goto_12
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v4}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 324
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->getSeriesID()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->setSeriesID(I)V

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->equals(Ljava/lang/Object;)Z
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    move-result v20

    if-nez v20, :cond_14

    .line 328
    :try_start_1e
    invoke-virtual {v3}, Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;->getGoalEventIconProperty()Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;->clone()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    check-cast v0, Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;

    move-object v5, v0
    :try_end_1e
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1e .. :try_end_1e} :catch_d
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    .line 335
    :goto_13
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10, v5}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->mGoalEventIconVector:Ljava/util/Vector;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/dmc/sic/android/property/BaseProperty;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setProperty(Lcom/sec/dmc/sic/android/property/BaseProperty;)V

    .line 202
    :cond_14
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_6

    .line 224
    :catch_6
    move-exception v7

    .line 226
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_c

    .line 239
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_7
    move-exception v7

    .line 241
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_d

    .line 254
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_8
    move-exception v7

    .line 256
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_e

    .line 268
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_9
    move-exception v7

    .line 270
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_f

    .line 283
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_a
    move-exception v7

    .line 285
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_10

    .line 298
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_b
    move-exception v7

    .line 300
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_11

    .line 313
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_c
    move-exception v7

    .line 315
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto/16 :goto_12

    .line 329
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    :catch_d
    move-exception v7

    .line 331
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V

    goto :goto_13

    .line 346
    .end local v3    # "XYSeriesStyle":Lcom/samsung/android/sdk/chart/style/SchartXYSeriesStyle;
    .end local v4    # "chartEventIconProeprty":Lcom/sec/dmc/sic/android/property/EventIconProperty;
    .end local v5    # "chartGoalEventIconProeprty":Lcom/sec/dmc/sic/android/property/GoalEventIconProperty;
    .end local v6    # "chartValueProeprty":Lcom/sec/dmc/sic/android/property/GraphValueProperty;
    .end local v7    # "e":Ljava/lang/CloneNotSupportedException;
    .end local v8    # "goalLineProperty":Lcom/sec/dmc/sic/android/property/GoalLineProperty;
    .end local v9    # "graphProperty":Lcom/sec/dmc/sic/android/property/GraphProperty;
    .end local v11    # "normalRangeProperty":Lcom/sec/dmc/sic/android/property/NormalRangeProperty;
    .end local v13    # "seriesProperty":Lcom/sec/dmc/sic/android/property/SeriesProperty;
    .end local v15    # "valueMarkingProperty":Lcom/sec/dmc/sic/android/property/ValueMarkingProperty;
    :catch_e
    move-exception v7

    .line 348
    .restart local v7    # "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {v7}, Ljava/lang/CloneNotSupportedException;->printStackTrace()V
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    goto/16 :goto_7
.end method
