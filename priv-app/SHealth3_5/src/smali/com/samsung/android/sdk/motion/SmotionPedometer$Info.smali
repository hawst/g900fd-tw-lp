.class public Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/motion/SmotionPedometer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# static fields
.field public static final COUNT_RUN_DOWN:I = 0x5

.field public static final COUNT_RUN_FLAT:I = 0x6

.field public static final COUNT_RUN_UP:I = 0x4

.field public static final COUNT_TOTAL:I = 0x0

.field public static final COUNT_WALK_DOWN:I = 0x2

.field public static final COUNT_WALK_FLAT:I = 0x3

.field public static final COUNT_WALK_UP:I = 0x1

.field public static final STATUS_RUN_DOWN:I = 0x5

.field public static final STATUS_RUN_FLAT:I = 0x6

.field public static final STATUS_RUN_UP:I = 0x4

.field public static final STATUS_STOP:I = 0x0

.field public static final STATUS_UNKNOWN:I = -0x1

.field public static final STATUS_WALK_DOWN:I = 0x2

.field public static final STATUS_WALK_FLAT:I = 0x3

.field public static final STATUS_WALK_UP:I = 0x1


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:J

.field private e:J

.field private f:J

.field private g:J

.field private h:D

.field private i:D

.field private j:D

.field private k:I

.field private l:J


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->a()Lcom/samsung/android/sdk/motion/Smotion;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer.Info : SmotionPedometer is not created. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/motion/SmotionPedometer;->b()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SmotionPedometer.Info : This device is not supported. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;D)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->i:D

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->k:I

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;IJ)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-wide p2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a:J

    goto :goto_0

    :pswitch_1
    iput-wide p2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->b:J

    goto :goto_0

    :pswitch_2
    iput-wide p2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->c:J

    goto :goto_0

    :pswitch_3
    iput-wide p2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->d:J

    goto :goto_0

    :pswitch_4
    iput-wide p2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->e:J

    goto :goto_0

    :pswitch_5
    iput-wide p2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->f:J

    goto :goto_0

    :pswitch_6
    iput-wide p2, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->g:J

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic a(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;J)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->l:J

    return-void
.end method

.method static synthetic b(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;D)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->j:D

    return-void
.end method

.method static synthetic c(Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;D)V
    .locals 0

    iput-wide p1, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->h:D

    return-void
.end method


# virtual methods
.method public getCalorie()D
    .locals 2

    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->j:D

    return-wide v0
.end method

.method public getCount(I)J
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x6

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SmotionPedometer : type value is wrong. "

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-wide/16 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-wide v0

    :pswitch_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->a:J

    goto :goto_0

    :pswitch_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->b:J

    goto :goto_0

    :pswitch_2
    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->c:J

    goto :goto_0

    :pswitch_3
    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->d:J

    goto :goto_0

    :pswitch_4
    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->e:J

    goto :goto_0

    :pswitch_5
    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->f:J

    goto :goto_0

    :pswitch_6
    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->g:J

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getDistance()D
    .locals 2

    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->i:D

    return-wide v0
.end method

.method public getSpeed()D
    .locals 2

    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->h:D

    return-wide v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->k:I

    return v0
.end method

.method public getTimeStamp()J
    .locals 2

    iget-wide v0, p0, Lcom/samsung/android/sdk/motion/SmotionPedometer$Info;->l:J

    return-wide v0
.end method
