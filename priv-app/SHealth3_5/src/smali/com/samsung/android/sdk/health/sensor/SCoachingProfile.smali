.class public Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;
.super Lcom/samsung/android/sdk/health/sensor/Health;
.source "SCoachingProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/SCoachingProfile$EXERCISE_TYPE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public caloryGoal:F

.field public distanceGoal:F

.field public timeGoal:J

.field public timeStamp:J

.field public trainingEffectIntensityGoal:F

.field public trainingEffectTimeGoal:J

.field public type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile$1;

    invoke-direct {v0}, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 23
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 15
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    .line 16
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    .line 17
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeGoal:J

    .line 18
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->distanceGoal:F

    .line 19
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->caloryGoal:F

    .line 20
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectIntensityGoal:F

    .line 21
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectTimeGoal:J

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 26
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/Health;-><init>()V

    .line 15
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    .line 16
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    .line 17
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeGoal:J

    .line 18
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->distanceGoal:F

    .line 19
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->caloryGoal:F

    .line 20
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectIntensityGoal:F

    .line 21
    iput-wide v2, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectTimeGoal:J

    .line 27
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->readFromParcel(Landroid/os/Parcel;)V

    .line 28
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeGoal:J

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->distanceGoal:F

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->caloryGoal:F

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectIntensityGoal:F

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectTimeGoal:J

    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->extra:Landroid/os/Bundle;

    .line 63
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[SCoachingProfile]\n type ="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timpStamp = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timeGoal ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 50
    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeGoal:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", distanceGoal = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->distanceGoal:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", caloryGoal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->caloryGoal:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 51
    const-string v1, ", trainingEffectIntencityGoal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectIntensityGoal:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", trainingEffectTimeGoal= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectTimeGoal:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->type:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeStamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->timeGoal:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 40
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->distanceGoal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 41
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->caloryGoal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 42
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectIntensityGoal:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 43
    iget-wide v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->trainingEffectTimeGoal:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/SCoachingProfile;->extra:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method
