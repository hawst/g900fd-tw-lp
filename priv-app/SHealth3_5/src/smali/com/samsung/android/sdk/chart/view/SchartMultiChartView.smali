.class public Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;
.super Lcom/samsung/android/sdk/chart/view/SchartXYChartView;
.source "SchartMultiChartView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;)V

    .line 52
    const/4 v0, 0x5

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    const/4 v0, 0x5

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/chart/view/SchartXYChartView;->setChartType(I)V

    .line 66
    return-void
.end method


# virtual methods
.method public setGraphType(II)V
    .locals 2
    .param p1, "seriesId"    # I
    .param p2, "type"    # I

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/android/sdk/chart/view/SchartMultiChartView;->mChartHandle:J

    invoke-static {v0, v1, p1, p2}, Lcom/sec/dmc/sic/jni/ChartJNI;->setGraphType(JII)V

    .line 72
    return-void
.end method
