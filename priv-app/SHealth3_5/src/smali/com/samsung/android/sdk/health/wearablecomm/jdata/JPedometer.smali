.class public Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometer;
.super Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHealth;
.source "JPedometer.java"


# instance fields
.field public activeTime:J

.field public calories:F

.field public climbStep:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public crud:I

.field public devicePkId:Ljava/lang/String;

.field public distance:F

.field public downStep:I

.field public extraPedometer:Lcom/samsung/android/sdk/health/wearablecomm/jdata/JPedometerBundle;

.field public healthyStep:I

.field public runStep:I

.field public runTime:J

.field public speed:F

.field public time:J

.field public totalStep:I

.field public upStep:I

.field public updownStep:I

.field public walkStep:I

.field public walkTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/wearablecomm/jdata/JHealth;-><init>()V

    return-void
.end method
