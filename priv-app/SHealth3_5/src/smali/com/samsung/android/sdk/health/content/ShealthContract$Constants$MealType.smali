.class public interface abstract Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$MealType;
.super Ljava/lang/Object;
.source "ShealthContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/content/ShealthContract$Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MealType"
.end annotation


# static fields
.field public static final BREAKFAST:I = 0x186a1

.field public static final DINNER:I = 0x186a3

.field public static final LUNCH:I = 0x186a2

.field public static final NOT_DEFINED:I = -0x1
    .annotation runtime Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$Default;
    .end annotation
.end field

.field public static final OTHER:I = 0x186a4
