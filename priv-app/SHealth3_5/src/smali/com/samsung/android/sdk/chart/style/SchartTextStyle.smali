.class public Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
.super Ljava/lang/Object;
.source "SchartTextStyle.java"


# static fields
.field public static final ANTI_ALIAS_FLAG:I = 0x1

.field public static final ASSET:I = 0x1

.field public static final BOLD:I = 0x1

.field public static final BOLD_ITALIC:I = 0x3

.field public static final CENTER:I = 0x1

.field public static final DEFAULT_FLAG:I = 0x0

.field public static final DEV_KERN_TEXT_FLAG:I = 0x100

.field public static final DITHER_FLAG:I = 0x4

.field public static final FAKE_BOLD_TEXT_FLAG:I = 0x20

.field public static final FILE:I = 0x2

.field public static final FILTER_BITMAP_FLAG:I = 0x2

.field public static final ITALIC:I = 0x2

.field public static final LEFT:I = 0x0

.field public static final LINEAR_TEXT_FLAG:I = 0x40

.field public static final NAME:I = 0x0

.field public static final NORMAL:I = 0x0

.field public static final RIGHT:I = 0x2

.field public static final STRIKE_THRU_TEXT_FLAG:I = 0x10

.field public static final SUBPIXEL_TEXT_FLAG:I = 0x80

.field public static final UNDERLINE_TEXT_FLAG:I = 0x8

.field public static defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

.field private static mCounter:I


# instance fields
.field private mAlign:I

.field private mAssetManager:Landroid/content/res/AssetManager;

.field private mColor:I

.field private mFlags:I

.field private mFontFamily:Ljava/lang/String;

.field private mFontStyle:I

.field private mPath:Ljava/lang/String;

.field private mStyleName:Ljava/lang/String;

.field private mTextSize:F

.field private mTypeFaceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-direct {v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 28
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    const-string v1, "default"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setStyleName(Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextSize(F)V

    .line 31
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setAntiAlias(Z)V

    .line 32
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    const/16 v1, 0xff

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 33
    sget-object v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->defaultStyle:Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setTextAlign(I)V

    .line 64
    sput v2, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mCounter:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    sget v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mCounter:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mCounter:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setStyleName(Ljava/lang/String;)V

    .line 89
    const/high16 v0, 0x41400000    # 12.0f

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTextSize:F

    .line 90
    const/16 v0, 0xff

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setARGB(IIII)V

    .line 91
    iput v1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 92
    iput v1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontStyle:I

    .line 93
    iput v1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAlign:I

    .line 94
    iput v1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTypeFaceType:I

    .line 95
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    .line 96
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    .line 97
    return-void
.end method


# virtual methods
.method public createTypeFaceFromName(Ljava/lang/String;)V
    .locals 1
    .param p1, "fontFamily"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTypeFaceType:I

    .line 126
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 575
    if-ne p0, p1, :cond_1

    .line 615
    :cond_0
    :goto_0
    return v1

    .line 577
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 578
    goto :goto_0

    .line 579
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 580
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 581
    check-cast v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;

    .line 582
    .local v0, "other":Lcom/samsung/android/sdk/chart/style/SchartTextStyle;
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAlign:I

    iget v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAlign:I

    if-eq v3, v4, :cond_4

    move v1, v2

    .line 583
    goto :goto_0

    .line 584
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAssetManager:Landroid/content/res/AssetManager;

    if-nez v3, :cond_5

    .line 585
    iget-object v3, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAssetManager:Landroid/content/res/AssetManager;

    if-eqz v3, :cond_6

    move v1, v2

    .line 586
    goto :goto_0

    .line 587
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAssetManager:Landroid/content/res/AssetManager;

    iget-object v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 588
    goto :goto_0

    .line 589
    :cond_6
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    iget v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    if-eq v3, v4, :cond_7

    move v1, v2

    .line 590
    goto :goto_0

    .line 591
    :cond_7
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    iget v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    if-eq v3, v4, :cond_8

    move v1, v2

    .line 592
    goto :goto_0

    .line 593
    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    if-nez v3, :cond_9

    .line 594
    iget-object v3, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    if-eqz v3, :cond_a

    move v1, v2

    .line 595
    goto :goto_0

    .line 596
    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 597
    goto :goto_0

    .line 598
    :cond_a
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontStyle:I

    iget v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontStyle:I

    if-eq v3, v4, :cond_b

    move v1, v2

    .line 599
    goto :goto_0

    .line 600
    :cond_b
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    if-nez v3, :cond_c

    .line 601
    iget-object v3, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    if-eqz v3, :cond_d

    move v1, v2

    .line 602
    goto :goto_0

    .line 603
    :cond_c
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    move v1, v2

    .line 604
    goto :goto_0

    .line 605
    :cond_d
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    if-nez v3, :cond_e

    .line 606
    iget-object v3, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    if-eqz v3, :cond_f

    move v1, v2

    .line 607
    goto :goto_0

    .line 608
    :cond_e
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    move v1, v2

    .line 609
    goto/16 :goto_0

    .line 610
    :cond_f
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTextSize:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    .line 611
    iget v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTextSize:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    .line 610
    if-eq v3, v4, :cond_10

    move v1, v2

    .line 612
    goto/16 :goto_0

    .line 613
    :cond_10
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTypeFaceType:I

    iget v4, v0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTypeFaceType:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 614
    goto/16 :goto_0
.end method

.method public getAlpha()I
    .locals 1

    .prologue
    .line 498
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public getAssetMgr()Landroid/content/res/AssetManager;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAssetManager:Landroid/content/res/AssetManager;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    return v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    return v0
.end method

.method public getStyle()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontStyle:I

    return v0
.end method

.method public getStyleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    return-object v0
.end method

.method public getTextSize()F
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTextSize:F

    return v0
.end method

.method public getTypeFacePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 554
    const/16 v0, 0x1f

    .line 555
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 556
    .local v1, "result":I
    iget v2, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAlign:I

    add-int/lit8 v1, v2, 0x1f

    .line 557
    mul-int/lit8 v4, v1, 0x1f

    .line 558
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAssetManager:Landroid/content/res/AssetManager;

    if-nez v2, :cond_0

    move v2, v3

    .line 557
    :goto_0
    add-int v1, v4, v2

    .line 559
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    add-int v1, v2, v4

    .line 560
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    add-int v1, v2, v4

    .line 561
    mul-int/lit8 v4, v1, 0x1f

    .line 562
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    .line 561
    :goto_1
    add-int v1, v4, v2

    .line 563
    mul-int/lit8 v2, v1, 0x1f

    iget v4, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontStyle:I

    add-int v1, v2, v4

    .line 564
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    add-int v1, v4, v2

    .line 565
    mul-int/lit8 v2, v1, 0x1f

    .line 566
    iget-object v4, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 565
    :goto_3
    add-int v1, v2, v3

    .line 567
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTextSize:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    add-int v1, v2, v3

    .line 568
    mul-int/lit8 v2, v1, 0x1f

    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTypeFaceType:I

    add-int v1, v2, v3

    .line 569
    return v1

    .line 558
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mAssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    .line 562
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontFamily:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 564
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 566
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_3
.end method

.method public final isAntiAlias()Z
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDither()Z
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFakeBoldText()Z
    .locals 1

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isFilterBitmap()Z
    .locals 1

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLinearText()Z
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isStrikeThruText()Z
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSubpixelText()Z
    .locals 1

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isUnderlineText()Z
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->getFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setARGB(IIII)V
    .locals 2
    .param p1, "a"    # I
    .param p2, "r"    # I
    .param p3, "g"    # I
    .param p4, "b"    # I

    .prologue
    .line 529
    shl-int/lit8 v0, p1, 0x18

    shl-int/lit8 v1, p2, 0x10

    or-int/2addr v0, v1

    shl-int/lit8 v1, p3, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, p4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 530
    return-void
.end method

.method public setAlpha(I)V
    .locals 5
    .param p1, "a"    # I

    .prologue
    .line 510
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    shr-int/lit8 v3, v3, 0x10

    and-int/lit16 v2, v3, 0xff

    .line 511
    .local v2, "r":I
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    shr-int/lit8 v3, v3, 0x8

    and-int/lit16 v1, v3, 0xff

    .line 512
    .local v1, "g":I
    iget v3, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    and-int/lit16 v0, v3, 0xff

    .line 514
    .local v0, "b":I
    shl-int/lit8 v3, p1, 0x18

    shl-int/lit8 v4, v2, 0x10

    or-int/2addr v3, v4

    shl-int/lit8 v4, v1, 0x8

    or-int/2addr v3, v4

    or-int/2addr v3, v0

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->setColor(I)V

    .line 515
    return-void
.end method

.method public setAntiAlias(Z)V
    .locals 1
    .param p1, "aa"    # Z

    .prologue
    .line 245
    if-eqz p1, :cond_0

    .line 247
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 253
    :goto_0
    return-void

    .line 251
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 486
    iput p1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mColor:I

    .line 487
    return-void
.end method

.method public setDither(Z)V
    .locals 1
    .param p1, "dither"    # Z

    .prologue
    .line 276
    if-eqz p1, :cond_0

    .line 278
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 284
    :goto_0
    return-void

    .line 282
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method

.method public setFakeBoldText(Z)V
    .locals 1
    .param p1, "fakeBoldText"    # Z

    .prologue
    .line 427
    if-eqz p1, :cond_0

    .line 429
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 435
    :goto_0
    return-void

    .line 433
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method

.method public setFilterBitmap(Z)V
    .locals 1
    .param p1, "filter"    # Z

    .prologue
    .line 457
    if-eqz p1, :cond_0

    .line 459
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 222
    iput p1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 223
    return-void
.end method

.method public setLinearText(Z)V
    .locals 1
    .param p1, "linearText"    # Z

    .prologue
    .line 307
    if-eqz p1, :cond_0

    .line 309
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 315
    :goto_0
    return-void

    .line 313
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method

.method public setStrikeThruText(Z)V
    .locals 1
    .param p1, "strikeThruText"    # Z

    .prologue
    .line 397
    if-eqz p1, :cond_0

    .line 399
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 405
    :goto_0
    return-void

    .line 403
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method

.method public setStyle(I)V
    .locals 0
    .param p1, "style"    # I

    .prologue
    .line 181
    iput p1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFontStyle:I

    .line 182
    return-void
.end method

.method public setStyleName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mStyleName"    # Ljava/lang/String;

    .prologue
    .line 539
    iput-object p1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mStyleName:Ljava/lang/String;

    .line 540
    return-void
.end method

.method public setSubpixelText(Z)V
    .locals 1
    .param p1, "subpixelText"    # Z

    .prologue
    .line 337
    if-eqz p1, :cond_0

    .line 339
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 345
    :goto_0
    return-void

    .line 343
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method

.method public setTextAlign(I)V
    .locals 0
    .param p1, "align"    # I

    .prologue
    .line 161
    return-void
.end method

.method public setTextSize(F)V
    .locals 0
    .param p1, "textSize"    # F

    .prologue
    .line 200
    iput p1, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mTextSize:F

    .line 201
    return-void
.end method

.method public setUnderlineText(Z)V
    .locals 1
    .param p1, "underlineText"    # Z

    .prologue
    .line 367
    if-eqz p1, :cond_0

    .line 369
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    .line 375
    :goto_0
    return-void

    .line 373
    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/samsung/android/sdk/chart/style/SchartTextStyle;->mFlags:I

    goto :goto_0
.end method
