.class final Lcom/samsung/android/sdk/motion/SmotionCall$a;
.super Landroid/hardware/motion/MotionRecognitionManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/motion/SmotionCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/hardware/motion/MotionRecognitionManager;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final registerListener(Landroid/hardware/motion/MRListener;I)V
    .locals 3

    const/high16 v0, 0x40000000    # 2.0f

    const/16 v1, 0x400

    const/4 v2, 0x0

    invoke-super {p0, p1, v0, v1, v2}, Landroid/hardware/motion/MotionRecognitionManager;->registerListenerEvent(Landroid/hardware/motion/MRListener;IILandroid/os/Handler;)V

    return-void
.end method

.method public final unregisterListener(Landroid/hardware/motion/MRListener;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/hardware/motion/MotionRecognitionManager;->unregisterListener(Landroid/hardware/motion/MRListener;)V

    return-void
.end method

.method public final unregisterListener(Landroid/hardware/motion/MRListener;I)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/hardware/motion/MotionRecognitionManager;->unregisterListener(Landroid/hardware/motion/MRListener;I)V

    return-void
.end method
