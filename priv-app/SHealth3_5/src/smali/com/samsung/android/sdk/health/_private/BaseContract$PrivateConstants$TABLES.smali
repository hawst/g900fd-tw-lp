.class public final Lcom/samsung/android/sdk/health/_private/BaseContract$PrivateConstants$TABLES;
.super Ljava/lang/Object;
.source "BaseContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/_private/BaseContract$PrivateConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TABLES"
.end annotation


# static fields
.field public static final APP_REGISTRY:Ljava/lang/String; = "app_registry"

.field public static final BUDDY_INFO:Ljava/lang/String; = "buddy_info"

.field public static final CHALLENGES_INFO:Ljava/lang/String; = "challenges_info"

.field public static final CHALLENGES_MEMBER:Ljava/lang/String; = "challenges_member"

.field public static final DEVICE_INFO:Ljava/lang/String; = "device_info"

.field public static final DOSAGE_ALARM_DATA:Ljava/lang/String; = "dosage_alarm_data"

.field public static final DOSAGE_ALARM_TIME:Ljava/lang/String; = "dosage_alarm_time"

.field public static final DOSAGE_TAKEN:Ljava/lang/String; = "dosage_taken"

.field public static final FITNESS_CHART:Ljava/lang/String; = "fitness_chart"

.field public static final HEALTHBOARD_CIGNA:Ljava/lang/String; = "healthboard_cigna"

.field public static final LONGTERM_COACH:Ljava/lang/String; = "longterm_coach"

.field public static final SENSOR_HANDLER_DETAILS:Ljava/lang/String; = "sensor_handler_details"

.field public static final TRAINING_LOAD_PEAK:Ljava/lang/String; = "training_load_peak"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final WATER_INTAKE_LOCAL:Ljava/lang/String; = "water_intake_local"

.field public static final WATER_TARE_ITEM:Ljava/lang/String; = "water_tare_item"

.field public static final WATER_TARE_TYPE:Ljava/lang/String; = "water_tare_type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
