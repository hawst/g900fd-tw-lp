.class public Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;
.super Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;
.source "ShealthSensorDevice.java"

# interfaces
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$HeightUnit;
.implements Lcom/samsung/android/sdk/health/content/ShealthContract$Constants$WeightUnit;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Weight"
.end annotation


# instance fields
.field public activityMetabolicRate:F

.field public bodyAge:I

.field public bodyFat:F

.field public bodyFatUnit:I

.field public bodyMassIndex:F

.field public bodyMetabolicRate:F

.field public bodyWater:F

.field public bodyWaterUnit:I

.field public boneMass:F

.field public boneMassUnit:I

.field public height:F

.field public heightUnit:I

.field public muscleMass:F

.field public muscleMassUnit:I

.field public skeletalMuscle:F

.field public skeletalMuscleUnit:I

.field public visceralFat:F

.field public visceralFatUnit:I

.field public weight:F

.field public weightUnit:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const v1, 0x7fffffff

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 2117
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Health;-><init>()V

    .line 2119
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->weight:F

    .line 2121
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->weightUnit:I

    .line 2123
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->height:F

    .line 2125
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->heightUnit:I

    .line 2127
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyFat:F

    .line 2129
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyFatUnit:I

    .line 2131
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->visceralFat:F

    .line 2133
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->visceralFatUnit:I

    .line 2135
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->skeletalMuscle:F

    .line 2137
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->skeletalMuscleUnit:I

    .line 2139
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyWater:F

    .line 2141
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyWaterUnit:I

    .line 2143
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->muscleMass:F

    .line 2145
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->muscleMassUnit:I

    .line 2147
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->boneMass:F

    .line 2149
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->boneMassUnit:I

    .line 2151
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyMassIndex:F

    .line 2153
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyMetabolicRate:F

    .line 2155
    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->activityMetabolicRate:F

    .line 2157
    iput v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthSensorDevice$Weight;->bodyAge:I

    return-void
.end method
