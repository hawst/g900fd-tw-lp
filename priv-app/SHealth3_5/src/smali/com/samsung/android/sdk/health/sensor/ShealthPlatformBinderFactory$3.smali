.class Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;
.super Ljava/lang/Object;
.source "ShealthPlatformBinderFactory.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;)V
    .locals 0

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;)I

    move-result v0

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_REQUESTED:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$500()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I
    invoke-static {v0}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;)I

    move-result v0

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$200()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 274
    :cond_0
    const-string v0, "ShealthPlatformBinderFactory"

    const-string v1, "Service Connected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    invoke-static {p2}, Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    move-result-object v1

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 276
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$600()I

    move-result v1

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$102(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;I)I

    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    const/4 v1, 0x1

    const/4 v2, 0x0

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->NotifyServiceConnectionStatus(ZI)Z
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;ZI)Z

    .line 283
    :goto_0
    return-void

    .line 281
    :cond_1
    const-string v0, "ShealthPlatformBinderFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Incorrect state found : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I
    invoke-static {v2}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$100(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 263
    const-string v0, "ShealthPlatformBinderFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Service disconnected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    # invokes: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->NotifyServiceConnectionStatus(ZI)Z
    invoke-static {v0, v3, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$000(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;ZI)Z

    .line 265
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory$3;->this$0:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->SERVICE_CONNECTION_LOST:I
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$200()I

    move-result v1

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mServiceConnectionState:I
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$102(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;I)I

    .line 266
    # getter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;
    invoke-static {}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$300()Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;

    move-result-object v0

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;->access$402(Lcom/samsung/android/sdk/health/sensor/ShealthPlatformBinderFactory;Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;)Lcom/samsung/android/sdk/health/sensor/_PlatformSensorService;

    .line 267
    return-void
.end method
