.class public Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
.super Ljava/lang/Object;
.source "ShealthBinderFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    }
.end annotation


# static fields
.field public static final DEVICE_TYPE_DEVICE:I = 0x65

.field public static final DEVICE_TYPE_FINDER:I = 0x64

.field private static SERVICE_CONNECTION_ESTABLISHED:I = 0x0

.field private static SERVICE_CONNECTION_LOST:I = 0x0

.field private static SERVICE_CONNECTION_REQUESTED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ShealthBinderFactory"

.field private static sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;


# instance fields
.field private mCallBackHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mObjDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

.field private mServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceConnectionState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    .line 359
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    .line 360
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    .line 361
    const/4 v0, 0x2

    sput v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$3;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 337
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    .line 341
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    .line 143
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    .line 145
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->initializeCallbackHandlerThread()Z

    .line 146
    return-void
.end method

.method private NotifyServiceConnectionStatus(ZI)Z
    .locals 9
    .param p1, "bTrueConneced"    # Z
    .param p2, "ErrorNumber"    # I

    .prologue
    const/4 v4, 0x1

    .line 243
    const/4 v0, 0x0

    .line 246
    .local v0, "bNotified":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 248
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 249
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 251
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    .line 253
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    if-eqz v6, :cond_0

    .line 255
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;

    invoke-direct {v2, p0, p1, v3, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$2;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZLcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;I)V

    .line 270
    .local v2, "r":Ljava/lang/Runnable;
    const/4 v0, 0x1

    .line 271
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 272
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x32

    invoke-virtual {v6, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 276
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    .end local v2    # "r":Ljava/lang/Runnable;
    .end local v3    # "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    if-ne v0, v4, :cond_2

    .line 282
    :goto_1
    return v4

    .line 281
    :cond_2
    const-string v4, "ShealthBinderFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No Listener was notified,only Device Objects? MUST not happen: bConnect:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private NotifyServiceConnectionStatus(ZILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Z
    .locals 9
    .param p1, "bTrueConneced"    # Z
    .param p2, "ErrorNumber"    # I
    .param p3, "uListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .prologue
    const/4 v4, 0x1

    .line 199
    const/4 v0, 0x0

    .line 202
    .local v0, "bNotified":Z
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 204
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 205
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 207
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    .line 209
    .local v3, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    if-eqz v6, :cond_0

    iget-object v6, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mlistener:Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    if-ne v6, p3, :cond_0

    .line 212
    new-instance v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$1;

    invoke-direct {v2, p0, p1, v3, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$1;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZLcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;I)V

    .line 226
    .local v2, "r":Ljava/lang/Runnable;
    const/4 v0, 0x1

    .line 227
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 228
    iget-object v6, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    const-wide/16 v7, 0x32

    invoke-virtual {v6, v2, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 232
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    .end local v2    # "r":Ljava/lang/Runnable;
    .end local v3    # "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_1
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    if-ne v0, v4, :cond_2

    .line 238
    :goto_1
    return v4

    .line 237
    :cond_2
    const-string v4, "ShealthBinderFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No Listener was notified,only Device Objects? MUST not happen: bConnect:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    const/4 v4, 0x0

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;ZI)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZI)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    return p1
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    return v0
.end method

.method static synthetic access$300()Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;Lcom/samsung/android/sdk/health/sensor/_SensorService;)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;
    .param p1, "x1"    # Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    return-object p1
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    return v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    return v0
.end method

.method private dropServiceConnection()Z
    .locals 2

    .prologue
    .line 185
    const-string v0, "ShealthBinderFactory"

    const-string v1, "dropServiceConnection: Unbinding with Health Service"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    if-ne v0, v1, :cond_0

    .line 189
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;

    .line 191
    sget v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    .line 193
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public static declared-synchronized getDefaultBinder(Landroid/content/Context;IJLcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "Devicetype"    # I
    .param p2, "ObjectId"    # J
    .param p4, "uListener"    # Lcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;

    .prologue
    .line 87
    const-class v7, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    monitor-enter v7

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    if-nez v1, :cond_0

    .line 89
    new-instance v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    .line 92
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v1, v2, :cond_1

    .line 94
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 95
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 98
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;-><init>(Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;IJLcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)V

    .line 99
    .local v0, "arg0":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v1, v2, :cond_3

    .line 103
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 104
    .local v6, "it":Landroid/content/Intent;
    const-string v1, "com.sec.android.service.health.sensor.SensorService"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    .line 110
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v2, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v6, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 112
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    if-ne v1, v2, :cond_2

    .line 113
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_REQUESTED:I

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    .line 114
    :cond_2
    const-string v1, "ShealthBinderFactory"

    const-string v2, "Requested Binding to Health Service"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    .end local v6    # "it":Landroid/content/Intent;
    :cond_3
    if-eqz p4, :cond_4

    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_ESTABLISHED:I

    if-ne v1, v2, :cond_4

    .line 128
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, p4}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->NotifyServiceConnectionStatus(ZILcom/samsung/android/sdk/health/sensor/ShealthDeviceFinder$ServiceConnectionListener;)Z

    .line 131
    :cond_4
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v1, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v7

    return-object v1

    .line 119
    .restart local v6    # "it":Landroid/content/Intent;
    :cond_5
    :try_start_1
    const-string v1, "ShealthBinderFactory"

    const-string v2, "Binding to Shealth Service wont happen.."

    invoke-static {v1, v2}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    sget-object v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    sget v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->SERVICE_CONNECTION_LOST:I

    iput v2, v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mServiceConnectionState:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    const/4 v1, 0x0

    goto :goto_0

    .line 87
    .end local v0    # "arg0":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    .end local v6    # "it":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit v7

    throw v1
.end method

.method public static declared-synchronized getMyBinder(IJ)Lcom/samsung/android/sdk/health/sensor/_SensorService;
    .locals 8
    .param p0, "Devicetype"    # I
    .param p1, "ObjectId"    # J

    .prologue
    .line 51
    const-class v4, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    monitor-enter v4

    const/4 v2, 0x0

    .line 53
    .local v2, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    :try_start_0
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    if-eqz v3, :cond_2

    .line 55
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v5, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 57
    :try_start_1
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 58
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    move-object v2, v0

    .line 61
    iget-wide v6, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mObjId:J

    cmp-long v3, v6, p1

    if-nez v3, :cond_0

    .line 66
    :cond_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :goto_0
    if-nez v2, :cond_3

    .line 74
    :try_start_2
    const-string v3, "ShealthBinderFactory"

    const-string v5, "Looke like getDefaultBinder was not called"

    invoke-static {v3, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 75
    const/4 v3, 0x0

    .line 80
    :goto_1
    monitor-exit v4

    return-object v3

    .line 66
    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 51
    :catchall_1
    move-exception v3

    monitor-exit v4

    throw v3

    .line 70
    :cond_2
    :try_start_5
    const-string v3, "ShealthBinderFactory"

    const-string/jumbo v5, "sSHealthServiceConnectionFactory or sSHealthServiceConnectionFactory.mObjDetailsList NULL"

    invoke-static {v3, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 80
    :cond_3
    sget-object v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    iget-object v3, v3, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mService:Lcom/samsung/android/sdk/health/sensor/_SensorService;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1
.end method

.method private initializeCallbackHandlerThread()Z
    .locals 3

    .prologue
    .line 321
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ShealthBinderFactory-Callback"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 322
    .local v0, "commandWorker":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 323
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    .line 325
    const/4 v1, 0x1

    return v1
.end method

.method public static declared-synchronized releaseReference(II)Z
    .locals 4
    .param p0, "Devicetype"    # I
    .param p1, "ObjectId"    # I

    .prologue
    .line 137
    const-class v1, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->sSHealthServiceConnectionFactory:Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;

    int-to-long v2, p1

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->removeMatchingObject(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    const/4 v0, 0x0

    monitor-exit v1

    return v0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 332
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 333
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mCallBackHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 335
    :cond_0
    return-void
.end method

.method removeMatchingObject(IJ)Z
    .locals 8
    .param p1, "Devicetype"    # I
    .param p2, "ObjectId"    # J

    .prologue
    const/4 v4, 0x1

    .line 150
    const/4 v2, 0x0

    .line 153
    .local v2, "u":Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;
    iget-object v5, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    monitor-enter v5

    .line 155
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 156
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 158
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;

    move-object v2, v0

    .line 159
    iget-wide v6, v2, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;->mObjId:J

    cmp-long v3, v6, p2

    if-nez v3, :cond_0

    .line 164
    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    if-nez v2, :cond_2

    .line 168
    const-string v3, "ShealthBinderFactory"

    const-string/jumbo v4, "removeMatchingObject:Matching Object not found"

    invoke-static {v3, v4}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    const/4 v3, 0x0

    .line 179
    :goto_0
    return v3

    .line 164
    .end local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 174
    .restart local v1    # "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory$SensorObjectDetails;>;"
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 175
    iget-object v3, p0, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->mObjDetailsList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-ne v3, v4, :cond_3

    .line 177
    invoke-direct {p0}, Lcom/samsung/android/sdk/health/sensor/ShealthBinderFactory;->dropServiceConnection()Z

    :cond_3
    move v3, v4

    .line 179
    goto :goto_0
.end method
