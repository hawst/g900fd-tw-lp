.class public Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;
.super Ljava/lang/Object;
.source "CoachingRunningExercise.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDistance:D

.field private mDuration:I

.field private mETEtrainingLoadPeak:I

.field private mEndTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise$1;

    invoke-direct {v0}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise$1;-><init>()V

    sput-object v0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 109
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    return-void
.end method

.method public constructor <init>(JIDI)V
    .locals 0
    .param p1, "endTime"    # J
    .param p3, "duration"    # I
    .param p4, "distance"    # D
    .param p6, "eTEtrainingLoadPeak"    # I

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mEndTime:J

    .line 68
    iput p3, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDuration:I

    .line 69
    iput-wide p4, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDistance:D

    .line 70
    iput p6, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mETEtrainingLoadPeak:I

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-direct {p0, p1}, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->readFromParcel(Landroid/os/Parcel;)V

    .line 75
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mEndTime:J

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDuration:I

    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDistance:D

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mETEtrainingLoadPeak:I

    .line 96
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDistance:D

    return-wide v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDuration:I

    return v0
.end method

.method public getETEtrainingLoadPeak()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mETEtrainingLoadPeak:I

    return v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mEndTime:J

    return-wide v0
.end method

.method public setDistance(D)V
    .locals 0
    .param p1, "distance"    # D

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDistance:D

    .line 42
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDuration:I

    .line 38
    return-void
.end method

.method public setETEtrainingLoadPeak(I)V
    .locals 0
    .param p1, "eTEtrainingLoadPeak"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mETEtrainingLoadPeak:I

    .line 50
    return-void
.end method

.method public setEndTime(J)V
    .locals 0
    .param p1, "endTime"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mEndTime:J

    .line 34
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCoachingRunningExercise:::  EndTime : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mEndTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Duration : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 56
    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDuration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Distance : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDistance:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mETEtrainingLoadPeak : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 57
    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mETEtrainingLoadPeak:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mEndTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 86
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDuration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mDistance:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 88
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingRunningExercise;->mETEtrainingLoadPeak:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    return-void
.end method
