.class public Lcom/samsung/android/wms/service/health/structure/CoachingVars;
.super Ljava/lang/Object;
.source "CoachingVars.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/wms/service/health/structure/CoachingVars;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAC:I

.field private mLastTrainingLevelUpdate:J

.field private mLatestExerciseTime:J

.field private mLatestFeedbackPhraseNumber:C

.field private mMaxHr:C

.field private mMaxMET:J

.field private mPreviousToPreviousTrainingLevel:I

.field private mPreviousTrainingLevel:I

.field private mRecourceRecovery:I

.field private mStartDate:J

.field private mTrainingLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/samsung/android/wms/service/health/structure/CoachingVars$1;

    invoke-direct {v0}, Lcom/samsung/android/wms/service/health/structure/CoachingVars$1;-><init>()V

    sput-object v0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 206
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    return-void
.end method

.method public constructor <init>(ICJIJIJIICJ)V
    .locals 0
    .param p1, "mAC"    # I
    .param p2, "mMaxHr"    # C
    .param p3, "mMaxMET"    # J
    .param p5, "mRecourceRecovery"    # I
    .param p6, "mStartDate"    # J
    .param p8, "mTrainingLevel"    # I
    .param p9, "mLastTrainingLevelUpdate"    # J
    .param p11, "mPreviousToPreviousTrainingLevel"    # I
    .param p12, "mPreviousTrainingLevel"    # I
    .param p13, "mLatestFeedbackPhraseNumber"    # C
    .param p14, "mLatestExerciseTime"    # J

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mAC:I

    .line 140
    iput-char p2, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxHr:C

    .line 141
    iput-wide p3, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxMET:J

    .line 142
    iput p5, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mRecourceRecovery:I

    .line 143
    iput-wide p6, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mStartDate:J

    .line 144
    iput p8, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mTrainingLevel:I

    .line 145
    iput-wide p9, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLastTrainingLevelUpdate:J

    .line 146
    iput p11, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousToPreviousTrainingLevel:I

    .line 147
    iput p12, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousTrainingLevel:I

    .line 148
    iput-char p13, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestFeedbackPhraseNumber:C

    .line 149
    iput-wide p14, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestExerciseTime:J

    .line 150
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    invoke-direct {p0, p1}, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->readFromParcel(Landroid/os/Parcel;)V

    .line 154
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 180
    const/4 v1, 0x2

    new-array v0, v1, [C

    .line 181
    .local v0, "temp":[C
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mAC:I

    .line 182
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    int-to-char v1, v1

    iput-char v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxHr:C

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxMET:J

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mRecourceRecovery:I

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mStartDate:J

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mTrainingLevel:I

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLastTrainingLevelUpdate:J

    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousToPreviousTrainingLevel:I

    .line 189
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousTrainingLevel:I

    .line 190
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    int-to-char v1, v1

    iput-char v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestFeedbackPhraseNumber:C

    .line 191
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestExerciseTime:J

    .line 193
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x0

    return v0
.end method

.method public getAC()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mAC:I

    return v0
.end method

.method public getLastTrainingLevelUpdate()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLastTrainingLevelUpdate:J

    return-wide v0
.end method

.method public getLatestExerciseTime()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestExerciseTime:J

    return-wide v0
.end method

.method public getLatestFeedbackPhraseNumber()C
    .locals 1

    .prologue
    .line 65
    iget-char v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestFeedbackPhraseNumber:C

    return v0
.end method

.method public getMaxHr()C
    .locals 1

    .prologue
    .line 33
    iget-char v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxHr:C

    return v0
.end method

.method public getMaxMET()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxMET:J

    return-wide v0
.end method

.method public getPreviousToPreviousTrainingLevel()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousToPreviousTrainingLevel:I

    return v0
.end method

.method public getPreviousTrainingLevel()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousTrainingLevel:I

    return v0
.end method

.method public getRecourceRecovery()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mRecourceRecovery:I

    return v0
.end method

.method public getStartDate()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mStartDate:J

    return-wide v0
.end method

.method public getTrainingLevel()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mTrainingLevel:I

    return v0
.end method

.method public setAC(I)V
    .locals 0
    .param p1, "aC"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mAC:I

    .line 74
    return-void
.end method

.method public setLastTrainingLevelUpdate(J)V
    .locals 0
    .param p1, "lastTrainingLevelUpdate"    # J

    .prologue
    .line 97
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLastTrainingLevelUpdate:J

    .line 98
    return-void
.end method

.method public setLatestExerciseTime(J)V
    .locals 0
    .param p1, "latestExerciseTime"    # J

    .prologue
    .line 113
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestExerciseTime:J

    .line 114
    return-void
.end method

.method public setLatestFeedbackPhraseNumber(C)V
    .locals 0
    .param p1, "latestFeedbackPhraseNumber"    # C

    .prologue
    .line 109
    iput-char p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestFeedbackPhraseNumber:C

    .line 110
    return-void
.end method

.method public setMaxHr(C)V
    .locals 0
    .param p1, "maxHr"    # C

    .prologue
    .line 77
    iput-char p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxHr:C

    .line 78
    return-void
.end method

.method public setMaxMET(J)V
    .locals 0
    .param p1, "maxMET"    # J

    .prologue
    .line 81
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxMET:J

    .line 82
    return-void
.end method

.method public setPreviousToPreviousTrainingLevel(I)V
    .locals 0
    .param p1, "previousToPreviousTrainingLevel"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousToPreviousTrainingLevel:I

    .line 102
    return-void
.end method

.method public setPreviousTrainingLevel(I)V
    .locals 0
    .param p1, "previousTrainingLevel"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousTrainingLevel:I

    .line 106
    return-void
.end method

.method public setRecourceRecovery(I)V
    .locals 0
    .param p1, "recourceRecovery"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mRecourceRecovery:I

    .line 86
    return-void
.end method

.method public setStartDate(J)V
    .locals 0
    .param p1, "startDate"    # J

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mStartDate:J

    .line 90
    return-void
.end method

.method public setTrainingLevel(I)V
    .locals 0
    .param p1, "trainingLevel"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mTrainingLevel:I

    .line 94
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCoachingVars:::  AC : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mAC:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MaxHr : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-char v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxHr:C

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " MaxMET : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 120
    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxMET:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " RecourceRecovery : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mRecourceRecovery:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " StatrDate : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 121
    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mStartDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TrainingLevel : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mTrainingLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LastTrainingLevelUpdate : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 122
    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLastTrainingLevelUpdate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " PreviousToPreviousTrainingLevel : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 123
    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousToPreviousTrainingLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " PreviousTrainingLevel : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 124
    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousTrainingLevel:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LatestFeedbackPhraseNumber : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 125
    iget-char v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestFeedbackPhraseNumber:C

    int-to-byte v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LatestExerciseTime : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 126
    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestExerciseTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 165
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mAC:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 166
    iget-char v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxHr:C

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 167
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mMaxMET:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 168
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mRecourceRecovery:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mStartDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 170
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mTrainingLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLastTrainingLevelUpdate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 172
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousToPreviousTrainingLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 173
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mPreviousTrainingLevel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 174
    iget-char v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestFeedbackPhraseNumber:C

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 175
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingVars;->mLatestExerciseTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 177
    return-void
.end method
