.class public Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;
.super Ljava/lang/Object;
.source "CoachingExerciseResult.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDistance:D

.field private mETEmaximalMET:I

.field private mETEresourceRecovery:I

.field private mETEtrainingLoadPeak:I

.field private mEndTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult$1;

    invoke-direct {v0}, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult$1;-><init>()V

    sput-object v0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 84
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public constructor <init>(JDIII)V
    .locals 0
    .param p1, "mEndTime"    # J
    .param p3, "mDistance"    # D
    .param p5, "mETEtrainingLoadPeak"    # I
    .param p6, "mETEmaximalMET"    # I
    .param p7, "mETEresourceRecovery"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mEndTime:J

    .line 38
    iput-wide p3, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mDistance:D

    .line 39
    iput p5, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEtrainingLoadPeak:I

    .line 40
    iput p6, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEmaximalMET:I

    .line 41
    iput p7, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEresourceRecovery:I

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->readFromParcel(Landroid/os/Parcel;)V

    .line 46
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mEndTime:J

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mDistance:D

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEtrainingLoadPeak:I

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEmaximalMET:I

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEresourceRecovery:I

    .line 71
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mDistance:D

    return-wide v0
.end method

.method public getETEmaximalMET()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEmaximalMET:I

    return v0
.end method

.method public getETEresourceRecovery()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEresourceRecovery:I

    return v0
.end method

.method public getETEtrainingLoadPeak()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEtrainingLoadPeak:I

    return v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mEndTime:J

    return-wide v0
.end method

.method public setDistance(D)V
    .locals 0
    .param p1, "mDistance"    # D

    .prologue
    .line 114
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mDistance:D

    .line 115
    return-void
.end method

.method public setETEmaximalMET(I)V
    .locals 0
    .param p1, "mETEmaximalMET"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEmaximalMET:I

    .line 123
    return-void
.end method

.method public setETEresourceRecovery(I)V
    .locals 0
    .param p1, "mETEresourceRecovery"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEresourceRecovery:I

    .line 127
    return-void
.end method

.method public setETEtrainingLoadPeak(I)V
    .locals 0
    .param p1, "mETEtrainingLoadPeak"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEtrainingLoadPeak:I

    .line 119
    return-void
.end method

.method public setEndTime(J)V
    .locals 0
    .param p1, "mEndTime"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mEndTime:J

    .line 111
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCoachingExerciseResult:::  EndTime : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mEndTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Distance : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 25
    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mDistance:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ETEtrainingLoadPeak : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEtrainingLoadPeak:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 26
    const-string v1, " ETEmaximalMET : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEmaximalMET:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mETEresourceRecovery : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 27
    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEresourceRecovery:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mEndTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 57
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mDistance:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 58
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEtrainingLoadPeak:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEmaximalMET:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingExerciseResult;->mETEresourceRecovery:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    return-void
.end method
