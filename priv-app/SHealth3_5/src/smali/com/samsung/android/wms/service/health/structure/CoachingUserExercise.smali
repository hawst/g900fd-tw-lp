.class public Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;
.super Ljava/lang/Object;
.source "CoachingUserExercise.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCalories:D

.field private mDuration:J

.field private mEndTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise$1;

    invoke-direct {v0}, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise$1;-><init>()V

    sput-object v0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 94
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method public constructor <init>(JJD)V
    .locals 0
    .param p1, "endTime"    # J
    .param p3, "duration"    # J
    .param p5, "calories"    # D

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mEndTime:J

    .line 53
    iput-wide p3, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mDuration:J

    .line 54
    iput-wide p5, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mCalories:D

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->readFromParcel(Landroid/os/Parcel;)V

    .line 63
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mEndTime:J

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mDuration:J

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mCalories:D

    .line 81
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public getCalories()D
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mCalories:D

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mDuration:J

    return-wide v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mEndTime:J

    return-wide v0
.end method

.method public setCalories(D)V
    .locals 0
    .param p1, "calories"    # D

    .prologue
    .line 40
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mCalories:D

    .line 41
    return-void
.end method

.method public setDuration(J)V
    .locals 0
    .param p1, "duration"    # J

    .prologue
    .line 36
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mDuration:J

    .line 37
    return-void
.end method

.method public setEndTime(J)V
    .locals 0
    .param p1, "endTime"    # J

    .prologue
    .line 32
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mEndTime:J

    .line 33
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCoachingUserExercise:::EndTime: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mEndTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Duration: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mDuration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 47
    const-string v1, "Calories :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mCalories:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mEndTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 73
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mDuration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 74
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingUserExercise;->mCalories:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 75
    return-void
.end method
