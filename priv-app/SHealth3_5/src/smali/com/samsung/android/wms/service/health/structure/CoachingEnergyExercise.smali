.class public Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;
.super Ljava/lang/Object;
.source "CoachingEnergyExercise.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCalories:I

.field private mDuration:J

.field private mETEtrainingLoadPeak:I

.field private mEndTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise$1;

    invoke-direct {v0}, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise$1;-><init>()V

    sput-object v0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 107
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    return-void
.end method

.method public constructor <init>(JJII)V
    .locals 0
    .param p1, "endTime"    # J
    .param p3, "duration"    # J
    .param p5, "calories"    # I
    .param p6, "eTEtrainingLoadPeak"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mEndTime:J

    .line 63
    iput-wide p3, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mDuration:J

    .line 64
    iput p5, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mCalories:I

    .line 65
    iput p6, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mETEtrainingLoadPeak:I

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->readFromParcel(Landroid/os/Parcel;)V

    .line 74
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mEndTime:J

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mDuration:J

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mCalories:I

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mETEtrainingLoadPeak:I

    .line 94
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method

.method public getCalories()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mCalories:I

    return v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mDuration:J

    return-wide v0
.end method

.method public getETEtrainingLoadPeak()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mETEtrainingLoadPeak:I

    return v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mEndTime:J

    return-wide v0
.end method

.method public setCalories(I)V
    .locals 0
    .param p1, "calories"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mCalories:I

    .line 42
    return-void
.end method

.method public setDuration(J)V
    .locals 0
    .param p1, "duration"    # J

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mDuration:J

    .line 38
    return-void
.end method

.method public setETEtrainingLoadPeak(I)V
    .locals 0
    .param p1, "eTEtrainingLoadPeak"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mETEtrainingLoadPeak:I

    .line 50
    return-void
.end method

.method public setEndTime(J)V
    .locals 0
    .param p1, "endTime"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mEndTime:J

    .line 34
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SCoachingEnergyExercise:::  EndTime : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mEndTime:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Duration : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 56
    iget-wide v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mDuration:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Calories : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mCalories:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mETEtrainingLoadPeak : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 57
    iget v1, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mETEtrainingLoadPeak:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mEndTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 84
    iget-wide v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mDuration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 85
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mCalories:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget v0, p0, Lcom/samsung/android/wms/service/health/structure/CoachingEnergyExercise;->mETEtrainingLoadPeak:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    return-void
.end method
