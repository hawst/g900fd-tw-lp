.class public abstract Lcom/samsung/android/health/HealthDataObserver;
.super Ljava/lang/Object;
.source "HealthDataObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/HealthDataObserver$DataHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Health.HealthDataObserver"

.field private static final sCallbacks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/health/HealthDataObserver;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mIObserver:Lcom/samsung/android/health/IHealthDataObserver$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/health/HealthDataObserver;->sCallbacks:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v1, Lcom/samsung/android/health/HealthDataObserver$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/health/HealthDataObserver$1;-><init>(Lcom/samsung/android/health/HealthDataObserver;)V

    iput-object v1, p0, Lcom/samsung/android/health/HealthDataObserver;->mIObserver:Lcom/samsung/android/health/IHealthDataObserver$Stub;

    .line 78
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .line 79
    .local v0, "looper":Landroid/os/Looper;
    :goto_0
    new-instance v1, Lcom/samsung/android/health/HealthDataObserver$DataHandler;

    invoke-direct {v1, p0, v0}, Lcom/samsung/android/health/HealthDataObserver$DataHandler;-><init>(Lcom/samsung/android/health/HealthDataObserver;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/health/HealthDataObserver;->mHandler:Landroid/os/Handler;

    .line 80
    return-void

    .line 78
    .end local v0    # "looper":Landroid/os/Looper;
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/health/HealthDataObserver;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDataObserver;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataObserver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static addObserver(Lcom/samsung/android/health/HealthDataStore;Ljava/lang/String;Lcom/samsung/android/health/HealthDataObserver;)V
    .locals 6
    .param p0, "store"    # Lcom/samsung/android/health/HealthDataStore;
    .param p1, "dataTypeName"    # Ljava/lang/String;
    .param p2, "observer"    # Lcom/samsung/android/health/HealthDataObserver;

    .prologue
    .line 101
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    instance-of v2, p2, Lcom/samsung/android/health/HealthDataObserver;

    if-nez v2, :cond_1

    .line 102
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Data type is invalid or no callback to unregister"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 105
    :cond_1
    sget-object v3, Lcom/samsung/android/health/HealthDataObserver;->sCallbacks:Ljava/util/ArrayList;

    monitor-enter v3

    .line 106
    :try_start_0
    sget-object v2, Lcom/samsung/android/health/HealthDataObserver;->sCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 107
    const-string v2, "Health.HealthDataObserver"

    const-string v4, "Same observer already registered"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    monitor-exit v3

    .line 120
    :goto_0
    return-void

    .line 110
    :cond_2
    sget-object v2, Lcom/samsung/android/health/HealthDataObserver;->sCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    invoke-static {p0}, Lcom/samsung/android/health/HealthDataStore;->getInterface(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/IHealth;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 115
    .local v1, "healthInterface":Lcom/samsung/android/health/IHealth;
    :try_start_1
    iget-object v2, p2, Lcom/samsung/android/health/HealthDataObserver;->mIObserver:Lcom/samsung/android/health/IHealthDataObserver$Stub;

    invoke-interface {v1, p1, v2}, Lcom/samsung/android/health/IHealth;->registerDataObserver(Ljava/lang/String;Lcom/samsung/android/health/IHealthDataObserver;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    :goto_1
    :try_start_2
    monitor-exit v3

    goto :goto_0

    .end local v1    # "healthInterface":Lcom/samsung/android/health/IHealth;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 116
    .restart local v1    # "healthInterface":Lcom/samsung/android/health/IHealth;
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v2, "Health.HealthDataObserver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Object "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " registration failed: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public static removeObserver(Lcom/samsung/android/health/HealthDataStore;Lcom/samsung/android/health/HealthDataObserver;)V
    .locals 7
    .param p0, "store"    # Lcom/samsung/android/health/HealthDataStore;
    .param p1, "observer"    # Lcom/samsung/android/health/HealthDataObserver;

    .prologue
    .line 132
    instance-of v3, p1, Lcom/samsung/android/health/HealthDataObserver;

    if-nez v3, :cond_0

    .line 133
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "No callback to unregister"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 136
    :cond_0
    sget-object v4, Lcom/samsung/android/health/HealthDataObserver;->sCallbacks:Ljava/util/ArrayList;

    monitor-enter v4

    .line 137
    :try_start_0
    sget-object v3, Lcom/samsung/android/health/HealthDataObserver;->sCallbacks:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    .line 138
    .local v2, "removeResult":Z
    if-nez v2, :cond_1

    .line 139
    const-string v3, "Health.HealthDataObserver"

    const-string v5, "Object is not found to unregister"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    monitor-exit v4

    .line 151
    :goto_0
    return-void

    .line 143
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/health/HealthDataStore;->getInterface(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/IHealth;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 146
    .local v1, "healthInterface":Lcom/samsung/android/health/IHealth;
    :try_start_1
    iget-object v3, p1, Lcom/samsung/android/health/HealthDataObserver;->mIObserver:Lcom/samsung/android/health/IHealthDataObserver$Stub;

    invoke-interface {v1, v3}, Lcom/samsung/android/health/IHealth;->unregisterDataObserver(Lcom/samsung/android/health/IHealthDataObserver;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 150
    :goto_1
    :try_start_2
    monitor-exit v4

    goto :goto_0

    .end local v1    # "healthInterface":Lcom/samsung/android/health/IHealth;
    .end local v2    # "removeResult":Z
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 147
    .restart local v1    # "healthInterface":Lcom/samsung/android/health/IHealth;
    .restart local v2    # "removeResult":Z
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v3, "Health.HealthDataObserver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Object unregistration failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public abstract onChange(Ljava/lang/String;)V
.end method
