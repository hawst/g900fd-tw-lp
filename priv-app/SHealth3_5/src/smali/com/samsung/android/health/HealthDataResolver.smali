.class public Lcom/samsung/android/health/HealthDataResolver;
.super Ljava/lang/Object;
.source "HealthDataResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/HealthDataResolver$1;,
        Lcom/samsung/android/health/HealthDataResolver$SortOrder;,
        Lcom/samsung/android/health/HealthDataResolver$Filter;,
        Lcom/samsung/android/health/HealthDataResolver$ReadResult;,
        Lcom/samsung/android/health/HealthDataResolver$ReadRequest;
    }
.end annotation


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private final mHealth:Lcom/samsung/android/health/IHealth;


# direct methods
.method public constructor <init>(Lcom/samsung/android/health/HealthDataStore;Landroid/os/Handler;)V
    .locals 1
    .param p1, "store"    # Lcom/samsung/android/health/HealthDataStore;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-static {p1}, Lcom/samsung/android/health/HealthDataStore;->getInterface(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/IHealth;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataResolver;->mHealth:Lcom/samsung/android/health/IHealth;

    .line 112
    iput-object p2, p0, Lcom/samsung/android/health/HealthDataResolver;->mHandler:Landroid/os/Handler;

    .line 113
    return-void
.end method


# virtual methods
.method public read(Lcom/samsung/android/health/HealthDataResolver$ReadRequest;)Lcom/samsung/android/health/HealthResultHolder;
    .locals 4
    .param p1, "request"    # Lcom/samsung/android/health/HealthDataResolver$ReadRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/health/HealthDataResolver$ReadRequest;",
            ")",
            "Lcom/samsung/android/health/HealthResultHolder",
            "<",
            "Lcom/samsung/android/health/HealthDataResolver$ReadResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 126
    instance-of v2, p1, Lcom/samsung/android/internal/ReadRequestImpl;

    if-nez v2, :cond_0

    .line 127
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid request instance"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 130
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/health/HealthDataResolver;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/health/HealthDataResolver;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    .local v0, "looper":Landroid/os/Looper;
    :goto_0
    move-object v1, p1

    .line 131
    check-cast v1, Lcom/samsung/android/internal/ReadRequestImpl;

    .line 133
    .local v1, "req":Lcom/samsung/android/internal/ReadRequestImpl;
    new-instance v2, Lcom/samsung/android/internal/HealthResultHolderImpl;

    iget-object v3, p0, Lcom/samsung/android/health/HealthDataResolver;->mHealth:Lcom/samsung/android/health/IHealth;

    invoke-interface {v3, v1}, Lcom/samsung/android/health/IHealth;->readData(Lcom/samsung/android/internal/ReadRequestImpl;)Lcom/samsung/android/health/HealthDataResolver$ReadResult;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/samsung/android/internal/HealthResultHolderImpl;-><init>(Lcom/samsung/android/health/HealthResultHolder$BaseResult;Landroid/os/Looper;)V

    return-object v2

    .line 130
    .end local v0    # "looper":Landroid/os/Looper;
    .end local v1    # "req":Lcom/samsung/android/internal/ReadRequestImpl;
    :cond_1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method
