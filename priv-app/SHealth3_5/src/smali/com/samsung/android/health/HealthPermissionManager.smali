.class public Lcom/samsung/android/health/HealthPermissionManager;
.super Ljava/lang/Object;
.source "HealthPermissionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/HealthPermissionManager$1;,
        Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;,
        Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;,
        Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "Health.HealthPermissionManager"


# instance fields
.field private final mStore:Lcom/samsung/android/health/HealthDataStore;


# direct methods
.method public constructor <init>(Lcom/samsung/android/health/HealthDataStore;)V
    .locals 0
    .param p1, "store"    # Lcom/samsung/android/health/HealthDataStore;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/samsung/android/health/HealthPermissionManager;->mStore:Lcom/samsung/android/health/HealthDataStore;

    .line 108
    return-void
.end method

.method static synthetic access$100(Landroid/os/Bundle;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-static {p0}, Lcom/samsung/android/health/HealthPermissionManager;->convertBundleToPermissionMap(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private static convertBundleToPermissionMap(Landroid/os/Bundle;)Ljava/util/Map;
    .locals 12
    .param p0, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 229
    .local v6, "permissionMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;Ljava/lang/Boolean;>;"
    if-eqz p0, :cond_3

    .line 231
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 236
    .local v4, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 237
    .local v3, "key":Ljava/lang/String;
    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v7

    .line 239
    .local v7, "permissionStateArray":[I
    invoke-static {}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->values()[Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    move-result-object v0

    .local v0, "arr$":[Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v8, v0, v2

    .line 240
    .local v8, "permissionType":Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    invoke-virtual {v8}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->getValue()I

    move-result v10

    aget v9, v7, v10

    .line 241
    .local v9, "result":I
    if-nez v9, :cond_2

    .line 242
    new-instance v10, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;

    invoke-direct {v10, v3, v8}, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;-><init>(Ljava/lang/String;Lcom/samsung/android/health/HealthPermissionManager$PermissionType;)V

    sget-object v11, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v6, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 243
    :cond_2
    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    .line 244
    new-instance v10, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;

    invoke-direct {v10, v3, v8}, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;-><init>(Ljava/lang/String;Lcom/samsung/android/health/HealthPermissionManager$PermissionType;)V

    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v6, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 249
    .end local v0    # "arr$":[Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    .end local v2    # "i$":I
    .end local v3    # "key":Ljava/lang/String;
    .end local v4    # "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "len$":I
    .end local v7    # "permissionStateArray":[I
    .end local v8    # "permissionType":Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    .end local v9    # "result":I
    :cond_3
    return-object v6
.end method

.method private static convertKeySetToBundle(Ljava/util/Set;)Landroid/os/Bundle;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;",
            ">;)",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 188
    .local p0, "permissionKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;>;"
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 193
    .local v8, "permissionKeyMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;

    .line 194
    .local v7, "key":Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;
    invoke-virtual {v7}, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->getDataType()Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "dataType":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 196
    new-instance v12, Ljava/lang/IllegalArgumentException;

    const-string v13, "The input argument includes null as a dataType of PermissionKey"

    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 199
    :cond_0
    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    .line 200
    .local v9, "permissionTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez v9, :cond_1

    .line 201
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "permissionTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 202
    .restart local v9    # "permissionTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v8, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_1
    invoke-virtual {v7}, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->getPermissionType()Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->getValue()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 207
    .end local v1    # "dataType":Ljava/lang/String;
    .end local v7    # "key":Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;
    .end local v9    # "permissionTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 211
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .end local v3    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 212
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    .line 213
    .restart local v9    # "permissionTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v11, v12, [I

    .line 214
    .local v11, "typeArray":[I
    const/4 v5, 0x0

    .line 216
    .local v5, "index":I
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    .line 217
    .local v10, "type":Ljava/lang/Integer;
    add-int/lit8 v6, v5, 0x1

    .end local v5    # "index":I
    .local v6, "index":I
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v12

    aput v12, v11, v5

    move v5, v6

    .line 218
    .end local v6    # "index":I
    .restart local v5    # "index":I
    goto :goto_2

    .line 219
    .end local v10    # "type":Ljava/lang/Integer;
    :cond_3
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v0, v12, v11}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1

    .line 222
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Integer;>;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "index":I
    .end local v9    # "permissionTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v11    # "typeArray":[I
    :cond_4
    return-object v0
.end method


# virtual methods
.method public isPermissionAcquired(Ljava/util/Set;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 162
    .local p1, "permissionKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;>;"
    if-nez p1, :cond_0

    .line 163
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "The input argument is null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 166
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 167
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "The input argument has no items"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 170
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/health/HealthPermissionManager;->mStore:Lcom/samsung/android/health/HealthDataStore;

    invoke-static {v4}, Lcom/samsung/android/health/HealthDataStore;->getInterface(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/IHealth;

    move-result-object v0

    .line 173
    .local v0, "healthInterface":Lcom/samsung/android/health/IHealth;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 174
    .local v1, "requestBundle":Landroid/os/Bundle;
    invoke-static {p1}, Lcom/samsung/android/health/HealthPermissionManager;->convertKeySetToBundle(Ljava/util/Set;)Landroid/os/Bundle;

    move-result-object v1

    .line 177
    const-string v4, "Health.HealthPermissionManager"

    const-string v5, "Checking the health data permissions are acquired..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-interface {v0, v1}, Lcom/samsung/android/health/IHealth;->isHealthDataPermissionAcquired(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 181
    .local v2, "resultBundle":Landroid/os/Bundle;
    invoke-static {v2}, Lcom/samsung/android/health/HealthPermissionManager;->convertBundleToPermissionMap(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v3

    .line 183
    .local v3, "resultMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;Ljava/lang/Boolean;>;"
    return-object v3
.end method

.method public requestPermissions(Ljava/util/Set;)Lcom/samsung/android/health/HealthResultHolder;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;",
            ">;)",
            "Lcom/samsung/android/health/HealthResultHolder",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "permissionKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;>;"
    if-nez p1, :cond_0

    .line 126
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The input argument is null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 129
    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 130
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "The input argument has no items"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 133
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/health/HealthPermissionManager;->mStore:Lcom/samsung/android/health/HealthDataStore;

    invoke-static {v2}, Lcom/samsung/android/health/HealthDataStore;->getInterface(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/IHealth;

    move-result-object v0

    .line 136
    .local v0, "healthInterface":Lcom/samsung/android/health/IHealth;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 137
    .local v1, "requestBundle":Landroid/os/Bundle;
    invoke-static {p1}, Lcom/samsung/android/health/HealthPermissionManager;->convertKeySetToBundle(Ljava/util/Set;)Landroid/os/Bundle;

    move-result-object v1

    .line 140
    const-string v2, "Health.HealthPermissionManager"

    const-string v3, "Trying to acquire the health data permissions..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    new-instance v2, Lcom/samsung/android/internal/HealthResultHolderImpl;

    invoke-interface {v0, v1}, Lcom/samsung/android/health/IHealth;->requestHealthDataPermissions(Landroid/os/Bundle;)Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;

    move-result-object v3

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/internal/HealthResultHolderImpl;-><init>(Lcom/samsung/android/health/HealthResultHolder$BaseResult;Landroid/os/Looper;)V

    return-object v2
.end method
