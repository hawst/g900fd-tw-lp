.class public Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;
.super Ljava/lang/Object;
.source "HealthPermissionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthPermissionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PermissionKey"
.end annotation


# instance fields
.field private final mDataType:Ljava/lang/String;

.field private final mPermissionType:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/samsung/android/health/HealthPermissionManager$PermissionType;)V
    .locals 0
    .param p1, "dataType"    # Ljava/lang/String;
    .param p2, "permissionType"    # Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    .prologue
    .line 324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 325
    iput-object p1, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    .line 326
    iput-object p2, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mPermissionType:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    .line 327
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 352
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 353
    check-cast v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;

    .line 356
    .local v0, "item":Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;
    iget-object v2, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 364
    .end local v0    # "item":Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;
    :cond_0
    :goto_0
    return v1

    .line 360
    .restart local v0    # "item":Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mPermissionType:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    invoke-virtual {v2}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->getValue()I

    move-result v2

    iget-object v3, v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mPermissionType:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    invoke-virtual {v3}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->getValue()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissionType()Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mPermissionType:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 371
    const/4 v0, 0x0

    .line 374
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mDataType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    div-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;->mPermissionType:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    invoke-virtual {v1}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->getValue()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method
