.class public interface abstract Lcom/samsung/android/health/HealthConstants$StepCount;
.super Ljava/lang/Object;
.source "HealthConstants.java"

# interfaces
.implements Lcom/samsung/android/health/HealthConstants$Measurement;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "StepCount"
.end annotation


# static fields
.field public static final CALORIE:Ljava/lang/String; = "calorie"

.field public static final DISTANCE:Ljava/lang/String; = "distance"

.field public static final DOWN_STEP:Ljava/lang/String; = "down_step"

.field public static final END_TIME:Ljava/lang/String; = "end_time"

.field public static final HEALTH_DATA_TYPE:Ljava/lang/String; = "com.samsung.health.step_count"

.field public static final RUN_STEP:Ljava/lang/String; = "run_step"

.field public static final SAMPLE_POSITION:Ljava/lang/String; = "sample_position"

.field public static final SAMPLE_POSITION_ANKLE:I = 0x38273

.field public static final SAMPLE_POSITION_ARM:I = 0x38274

.field public static final SAMPLE_POSITION_NOT_DEFINED:I = -0x1

.field public static final SAMPLE_POSITION_UNKNOWN:I = 0x38271

.field public static final SAMPLE_POSITION_WRIST:I = 0x38272

.field public static final SPEED:Ljava/lang/String; = "speed"

.field public static final TOTAL_STEP:Ljava/lang/String; = "total_step"

.field public static final UP_STEP:Ljava/lang/String; = "up_step"

.field public static final WALK_STEP:Ljava/lang/String; = "walk_step"
