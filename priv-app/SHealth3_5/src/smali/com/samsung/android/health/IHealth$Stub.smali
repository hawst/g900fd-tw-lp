.class public abstract Lcom/samsung/android/health/IHealth$Stub;
.super Landroid/os/Binder;
.source "IHealth.java"

# interfaces
.implements Lcom/samsung/android/health/IHealth;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/IHealth;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/IHealth$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.android.health.IHealth"

.field static final TRANSACTION_isHealthDataPermissionAcquired:I = 0x2

.field static final TRANSACTION_readData:I = 0x5

.field static final TRANSACTION_registerDataObserver:I = 0x3

.field static final TRANSACTION_requestHealthDataPermissions:I = 0x1

.field static final TRANSACTION_unregisterDataObserver:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 16
    const-string v0, "com.samsung.android.health.IHealth"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/android/health/IHealth$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/android/health/IHealth;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 24
    if-nez p0, :cond_0

    .line 25
    const/4 v0, 0x0

    .line 31
    :goto_0
    return-object v0

    .line 27
    :cond_0
    const-string v1, "com.samsung.android.health.IHealth"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 28
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/android/health/IHealth;

    if-eqz v1, :cond_1

    .line 29
    check-cast v0, Lcom/samsung/android/health/IHealth;

    goto :goto_0

    .line 31
    :cond_1
    new-instance v0, Lcom/samsung/android/health/IHealth$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/android/health/IHealth$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 39
    sparse-switch p1, :sswitch_data_0

    .line 130
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 43
    :sswitch_0
    const-string v4, "com.samsung.android.health.IHealth"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :sswitch_1
    const-string v4, "com.samsung.android.health.IHealth"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 51
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 56
    .local v0, "_arg0":Landroid/os/Bundle;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/health/IHealth$Stub;->requestHealthDataPermissions(Landroid/os/Bundle;)Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;

    move-result-object v2

    .line 57
    .local v2, "_result":Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 58
    if-eqz v2, :cond_1

    .line 59
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    invoke-virtual {v2, p3, v3}, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 54
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v2    # "_result":Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_1

    .line 63
    .restart local v2    # "_result":Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;
    :cond_1
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 69
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v2    # "_result":Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;
    :sswitch_2
    const-string v4, "com.samsung.android.health.IHealth"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    .line 72
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 77
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/health/IHealth$Stub;->isHealthDataPermissionAcquired(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 78
    .local v2, "_result":Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 79
    if-eqz v2, :cond_3

    .line 80
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    invoke-virtual {v2, p3, v3}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 75
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v2    # "_result":Landroid/os/Bundle;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_2

    .line 84
    .restart local v2    # "_result":Landroid/os/Bundle;
    :cond_3
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 90
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v2    # "_result":Landroid/os/Bundle;
    :sswitch_3
    const-string v4, "com.samsung.android.health.IHealth"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/health/IHealthDataObserver$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/health/IHealthDataObserver;

    move-result-object v1

    .line 95
    .local v1, "_arg1":Lcom/samsung/android/health/IHealthDataObserver;
    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/health/IHealth$Stub;->registerDataObserver(Ljava/lang/String;Lcom/samsung/android/health/IHealthDataObserver;)V

    .line 96
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 101
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/samsung/android/health/IHealthDataObserver;
    :sswitch_4
    const-string v4, "com.samsung.android.health.IHealth"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/health/IHealthDataObserver$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/health/IHealthDataObserver;

    move-result-object v0

    .line 104
    .local v0, "_arg0":Lcom/samsung/android/health/IHealthDataObserver;
    invoke-virtual {p0, v0}, Lcom/samsung/android/health/IHealth$Stub;->unregisterDataObserver(Lcom/samsung/android/health/IHealthDataObserver;)V

    .line 105
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 110
    .end local v0    # "_arg0":Lcom/samsung/android/health/IHealthDataObserver;
    :sswitch_5
    const-string v4, "com.samsung.android.health.IHealth"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    .line 113
    sget-object v4, Lcom/samsung/android/internal/ReadRequestImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/ReadRequestImpl;

    .line 118
    .local v0, "_arg0":Lcom/samsung/android/internal/ReadRequestImpl;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/samsung/android/health/IHealth$Stub;->readData(Lcom/samsung/android/internal/ReadRequestImpl;)Lcom/samsung/android/health/HealthDataResolver$ReadResult;

    move-result-object v2

    .line 119
    .local v2, "_result":Lcom/samsung/android/health/HealthDataResolver$ReadResult;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    if-eqz v2, :cond_5

    .line 121
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122
    invoke-virtual {v2, p3, v3}, Lcom/samsung/android/health/HealthDataResolver$ReadResult;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 116
    .end local v0    # "_arg0":Lcom/samsung/android/internal/ReadRequestImpl;
    .end local v2    # "_result":Lcom/samsung/android/health/HealthDataResolver$ReadResult;
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/samsung/android/internal/ReadRequestImpl;
    goto :goto_3

    .line 125
    .restart local v2    # "_result":Lcom/samsung/android/health/HealthDataResolver$ReadResult;
    :cond_5
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
