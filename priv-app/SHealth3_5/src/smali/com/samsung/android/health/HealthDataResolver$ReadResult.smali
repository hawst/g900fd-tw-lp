.class public Lcom/samsung/android/health/HealthDataResolver$ReadResult;
.super Lcom/samsung/android/health/HealthResultHolder$BaseResult;
.source "HealthDataResolver.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDataResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReadResult"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/health/HealthDataResolver$ReadResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mCursor:Lcom/samsung/android/internal/HealthDataCursor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 332
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$ReadResult$1;

    invoke-direct {v0}, Lcom/samsung/android/health/HealthDataResolver$ReadResult$1;-><init>()V

    sput-object v0, Lcom/samsung/android/health/HealthDataResolver$ReadResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 307
    invoke-direct {p0, p1}, Lcom/samsung/android/health/HealthResultHolder$BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 308
    const-class v0, Lcom/samsung/android/internal/HealthDataCursor;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/internal/HealthDataCursor;

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadResult;->mCursor:Lcom/samsung/android/internal/HealthDataCursor;

    .line 310
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/health/HealthDataResolver$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/android/health/HealthDataResolver$1;

    .prologue
    .line 294
    invoke-direct {p0, p1}, Lcom/samsung/android/health/HealthDataResolver$ReadResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/internal/HealthDataCursor;)V
    .locals 1
    .param p1, "cursor"    # Lcom/samsung/android/internal/HealthDataCursor;

    .prologue
    .line 302
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/health/HealthResultHolder$BaseResult;-><init>(I)V

    .line 303
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadResult;->mCursor:Lcom/samsung/android/internal/HealthDataCursor;

    .line 304
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return v0
.end method

.method public getResultCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 353
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadResult;->mCursor:Lcom/samsung/android/internal/HealthDataCursor;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 325
    invoke-super {p0, p1, p2}, Lcom/samsung/android/health/HealthResultHolder$BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 326
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadResult;->mCursor:Lcom/samsung/android/internal/HealthDataCursor;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 327
    return-void
.end method
