.class public final Lcom/samsung/android/health/HealthDevice;
.super Ljava/lang/Object;
.source "HealthDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/HealthDevice$Builder;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/health/HealthDevice;",
            ">;"
        }
    .end annotation
.end field

.field public static final GROUP_COMPANION:I = 0x57e43

.field public static final GROUP_EXTERNAL:I = 0x57e42

.field public static final GROUP_MOBILE:I = 0x57e41

.field public static final GROUP_UNKNOWN:I


# instance fields
.field private final mCustomName:Ljava/lang/String;

.field private final mGroup:I

.field private final mManufacturer:Ljava/lang/String;

.field private final mModel:Ljava/lang/String;

.field private final mSeed:Ljava/lang/String;

.field private final mUuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/samsung/android/health/HealthDevice$1;

    invoke-direct {v0}, Lcom/samsung/android/health/HealthDevice$1;-><init>()V

    sput-object v0, Lcom/samsung/android/health/HealthDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mUuid:Ljava/lang/String;

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mCustomName:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mModel:Ljava/lang/String;

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mManufacturer:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/health/HealthDevice;->mGroup:I

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    .line 101
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/health/HealthDevice$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/android/health/HealthDevice$1;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/android/health/HealthDevice;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/health/HealthDevice$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/samsung/android/health/HealthDevice$Builder;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mUuid:Ljava/lang/String;

    .line 87
    # getter for: Lcom/samsung/android/health/HealthDevice$Builder;->mCustomName:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/health/HealthDevice$Builder;->access$000(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mCustomName:Ljava/lang/String;

    .line 88
    # getter for: Lcom/samsung/android/health/HealthDevice$Builder;->mModel:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/health/HealthDevice$Builder;->access$100(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mModel:Ljava/lang/String;

    .line 89
    # getter for: Lcom/samsung/android/health/HealthDevice$Builder;->mManufacturer:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/health/HealthDevice$Builder;->access$200(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mManufacturer:Ljava/lang/String;

    .line 90
    # getter for: Lcom/samsung/android/health/HealthDevice$Builder;->mGroup:I
    invoke-static {p1}, Lcom/samsung/android/health/HealthDevice$Builder;->access$300(Lcom/samsung/android/health/HealthDevice$Builder;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/health/HealthDevice;->mGroup:I

    .line 91
    # getter for: Lcom/samsung/android/health/HealthDevice$Builder;->mSeed:Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/health/HealthDevice$Builder;->access$400(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    .line 92
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/health/HealthDevice$Builder;Lcom/samsung/android/health/HealthDevice$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/health/HealthDevice$Builder;
    .param p2, "x1"    # Lcom/samsung/android/health/HealthDevice$1;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/android/health/HealthDevice;-><init>(Lcom/samsung/android/health/HealthDevice$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;
    .param p2, "seed"    # Ljava/lang/String;
    .param p3, "manufacturer"    # Ljava/lang/String;
    .param p4, "model"    # Ljava/lang/String;
    .param p5, "name"    # Ljava/lang/String;
    .param p6, "group"    # I

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/samsung/android/health/HealthDevice;->mUuid:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/samsung/android/health/HealthDevice;->mManufacturer:Ljava/lang/String;

    .line 80
    iput-object p4, p0, Lcom/samsung/android/health/HealthDevice;->mModel:Ljava/lang/String;

    .line 81
    iput-object p5, p0, Lcom/samsung/android/health/HealthDevice;->mCustomName:Ljava/lang/String;

    .line 82
    iput p6, p0, Lcom/samsung/android/health/HealthDevice;->mGroup:I

    .line 83
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 145
    if-ne p1, p0, :cond_1

    .line 146
    const/4 v1, 0x1

    .line 160
    :cond_0
    :goto_0
    return v1

    .line 149
    :cond_1
    instance-of v2, p1, Lcom/samsung/android/health/HealthDevice;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 153
    check-cast v0, Lcom/samsung/android/health/HealthDevice;

    .line 156
    .local v0, "other":Lcom/samsung/android/health/HealthDevice;
    iget-object v2, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 160
    iget-object v1, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    iget-object v2, v0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getCustomName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mCustomName:Ljava/lang/String;

    return-object v0
.end method

.method public getGroup()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/samsung/android/health/HealthDevice;->mGroup:I

    return v0
.end method

.method public getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mManufacturer:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method public getSeed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    return-object v0
.end method

.method public getUniqueId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mUuid:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 167
    const/4 v0, 0x0

    .line 170
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mUuid:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mCustomName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mModel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mManufacturer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 122
    iget v0, p0, Lcom/samsung/android/health/HealthDevice;->mGroup:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice;->mSeed:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 124
    return-void
.end method
