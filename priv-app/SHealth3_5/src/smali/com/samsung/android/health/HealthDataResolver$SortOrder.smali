.class public abstract enum Lcom/samsung/android/health/HealthDataResolver$SortOrder;
.super Ljava/lang/Enum;
.source "HealthDataResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDataResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4409
    name = "SortOrder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/health/HealthDataResolver$SortOrder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/health/HealthDataResolver$SortOrder;

.field public static final enum ASC:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

.field public static final enum DESC:Lcom/samsung/android/health/HealthDataResolver$SortOrder;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 542
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder$1;

    const-string v1, "ASC"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/health/HealthDataResolver$SortOrder$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder;->ASC:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    .line 548
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder$2;

    const-string v1, "DESC"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/health/HealthDataResolver$SortOrder$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder;->DESC:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    .line 541
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    sget-object v1, Lcom/samsung/android/health/HealthDataResolver$SortOrder;->ASC:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/health/HealthDataResolver$SortOrder;->DESC:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder;->$VALUES:[Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 541
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/samsung/android/health/HealthDataResolver$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Lcom/samsung/android/health/HealthDataResolver$1;

    .prologue
    .line 541
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/health/HealthDataResolver$SortOrder;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/health/HealthDataResolver$SortOrder;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 541
    const-class v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/health/HealthDataResolver$SortOrder;
    .locals 1

    .prologue
    .line 541
    sget-object v0, Lcom/samsung/android/health/HealthDataResolver$SortOrder;->$VALUES:[Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    invoke-virtual {v0}, [Lcom/samsung/android/health/HealthDataResolver$SortOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    return-object v0
.end method


# virtual methods
.method public abstract toSqlLiteral()Ljava/lang/String;
.end method
