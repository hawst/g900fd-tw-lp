.class public Lcom/samsung/android/health/HealthDevice$Builder;
.super Ljava/lang/Object;
.source "HealthDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mCustomName:Ljava/lang/String;

.field private mGroup:I

.field private mManufacturer:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mSeed:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDevice$Builder;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mCustomName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDevice$Builder;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDevice$Builder;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mManufacturer:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/health/HealthDevice$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDevice$Builder;

    .prologue
    .line 235
    iget v0, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mGroup:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/health/HealthDevice$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDevice$Builder;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mSeed:Ljava/lang/String;

    return-object v0
.end method

.method private static checkGroup(I)V
    .locals 2
    .param p0, "group"    # I

    .prologue
    .line 328
    packed-switch p0, :pswitch_data_0

    .line 339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Device group is not set correctly"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 334
    :pswitch_0
    return-void

    .line 328
    :pswitch_data_0
    .packed-switch 0x57e41
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public build()Lcom/samsung/android/health/HealthDevice;
    .locals 2

    .prologue
    .line 319
    iget-object v0, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mSeed:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Seed is not specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_0
    iget v0, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mGroup:I

    invoke-static {v0}, Lcom/samsung/android/health/HealthDevice$Builder;->checkGroup(I)V

    .line 324
    new-instance v0, Lcom/samsung/android/health/HealthDevice;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/health/HealthDevice;-><init>(Lcom/samsung/android/health/HealthDevice$Builder;Lcom/samsung/android/health/HealthDevice$1;)V

    return-object v0
.end method

.method public setCustomName(Ljava/lang/String;)Lcom/samsung/android/health/HealthDevice$Builder;
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mCustomName:Ljava/lang/String;

    .line 284
    return-object p0
.end method

.method public setDeviceSeed(Ljava/lang/String;)Lcom/samsung/android/health/HealthDevice$Builder;
    .locals 0
    .param p1, "seed"    # Ljava/lang/String;

    .prologue
    .line 295
    iput-object p1, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mSeed:Ljava/lang/String;

    .line 296
    return-object p0
.end method

.method public setGroup(I)Lcom/samsung/android/health/HealthDevice$Builder;
    .locals 0
    .param p1, "group"    # I

    .prologue
    .line 307
    iput p1, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mGroup:I

    .line 308
    return-object p0
.end method

.method public setManufacturer(Ljava/lang/String;)Lcom/samsung/android/health/HealthDevice$Builder;
    .locals 0
    .param p1, "manufacturer"    # Ljava/lang/String;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mManufacturer:Ljava/lang/String;

    .line 272
    return-object p0
.end method

.method public setModel(Ljava/lang/String;)Lcom/samsung/android/health/HealthDevice$Builder;
    .locals 0
    .param p1, "model"    # Ljava/lang/String;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/android/health/HealthDevice$Builder;->mModel:Ljava/lang/String;

    .line 260
    return-object p0
.end method
