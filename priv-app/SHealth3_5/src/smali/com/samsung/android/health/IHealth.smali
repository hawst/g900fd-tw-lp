.class public interface abstract Lcom/samsung/android/health/IHealth;
.super Ljava/lang/Object;
.source "IHealth.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/IHealth$Stub;
    }
.end annotation


# virtual methods
.method public abstract isHealthDataPermissionAcquired(Landroid/os/Bundle;)Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract readData(Lcom/samsung/android/internal/ReadRequestImpl;)Lcom/samsung/android/health/HealthDataResolver$ReadResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract registerDataObserver(Ljava/lang/String;Lcom/samsung/android/health/IHealthDataObserver;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract requestHealthDataPermissions(Landroid/os/Bundle;)Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unregisterDataObserver(Lcom/samsung/android/health/IHealthDataObserver;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
