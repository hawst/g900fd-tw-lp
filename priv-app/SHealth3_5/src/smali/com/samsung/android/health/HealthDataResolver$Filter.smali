.class public Lcom/samsung/android/health/HealthDataResolver$Filter;
.super Ljava/lang/Object;
.source "HealthDataResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDataResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Filter"
.end annotation


# instance fields
.field private final mQuery:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 366
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataResolver$Filter;->mQuery:Ljava/lang/String;

    .line 368
    return-void
.end method

.method public static varargs and(Lcom/samsung/android/health/HealthDataResolver$Filter;[Lcom/samsung/android/health/HealthDataResolver$Filter;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 7
    .param p0, "filter"    # Lcom/samsung/android/health/HealthDataResolver$Filter;
    .param p1, "additionalFilters"    # [Lcom/samsung/android/health/HealthDataResolver$Filter;

    .prologue
    .line 393
    invoke-static {p0}, Lcom/samsung/android/health/HealthDataResolver$Filter;->checkFilter(Lcom/samsung/android/health/HealthDataResolver$Filter;)V

    .line 394
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/samsung/android/health/HealthDataResolver$Filter;->toQueryString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 395
    .local v4, "str":Ljava/lang/StringBuilder;
    move-object v0, p1

    .local v0, "arr$":[Lcom/samsung/android/health/HealthDataResolver$Filter;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 396
    .local v1, "filterEntry":Lcom/samsung/android/health/HealthDataResolver$Filter;
    invoke-static {v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;->checkFilter(Lcom/samsung/android/health/HealthDataResolver$Filter;)V

    .line 397
    const/4 v5, 0x0

    const/16 v6, 0x28

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 398
    const-string v5, ") AND ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;->toQueryString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x29

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 395
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 400
    .end local v1    # "filterEntry":Lcom/samsung/android/health/HealthDataResolver$Filter;
    :cond_0
    new-instance v5, Lcom/samsung/android/health/HealthDataResolver$Filter;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v5
.end method

.method private static checkFilter(Lcom/samsung/android/health/HealthDataResolver$Filter;)V
    .locals 2
    .param p0, "filter"    # Lcom/samsung/android/health/HealthDataResolver$Filter;

    .prologue
    .line 371
    instance-of v0, p0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    if-nez v0, :cond_0

    .line 372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid filter instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374
    :cond_0
    return-void
.end method

.method public static eq(Ljava/lang/String;Ljava/lang/Object;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/samsung/android/health/HealthDataResolver$Filter;"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "value":Ljava/lang/Object;, "TT;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 447
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid field or value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 449
    :cond_1
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 450
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    .line 452
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static greaterThan(Ljava/lang/String;Ljava/lang/Comparable;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/samsung/android/health/HealthDataResolver$Filter;"
        }
    .end annotation

    .prologue
    .line 513
    .local p1, "value":Ljava/lang/Comparable;, "TT;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 514
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid field or value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 516
    :cond_1
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static greaterThanEquals(Ljava/lang/String;Ljava/lang/Comparable;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/samsung/android/health/HealthDataResolver$Filter;"
        }
    .end annotation

    .prologue
    .line 529
    .local p1, "value":Ljava/lang/Comparable;, "TT;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 530
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid field or value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 532
    :cond_1
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static lessThan(Ljava/lang/String;Ljava/lang/Comparable;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/samsung/android/health/HealthDataResolver$Filter;"
        }
    .end annotation

    .prologue
    .line 481
    .local p1, "value":Ljava/lang/Comparable;, "TT;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 482
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid field or value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 484
    :cond_1
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static lessThanEquals(Ljava/lang/String;Ljava/lang/Comparable;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<TT;>;>(",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/samsung/android/health/HealthDataResolver$Filter;"
        }
    .end annotation

    .prologue
    .line 497
    .local p1, "value":Ljava/lang/Comparable;, "TT;"
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 498
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid field or value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_1
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static not(Lcom/samsung/android/health/HealthDataResolver$Filter;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 3
    .param p0, "filter"    # Lcom/samsung/android/health/HealthDataResolver$Filter;

    .prologue
    .line 432
    invoke-static {p0}, Lcom/samsung/android/health/HealthDataResolver$Filter;->checkFilter(Lcom/samsung/android/health/HealthDataResolver$Filter;)V

    .line 433
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NOT ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/health/HealthDataResolver$Filter;->toQueryString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static varargs or(Lcom/samsung/android/health/HealthDataResolver$Filter;[Lcom/samsung/android/health/HealthDataResolver$Filter;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 7
    .param p0, "filter"    # Lcom/samsung/android/health/HealthDataResolver$Filter;
    .param p1, "additionalFilters"    # [Lcom/samsung/android/health/HealthDataResolver$Filter;

    .prologue
    .line 413
    invoke-static {p0}, Lcom/samsung/android/health/HealthDataResolver$Filter;->checkFilter(Lcom/samsung/android/health/HealthDataResolver$Filter;)V

    .line 414
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/samsung/android/health/HealthDataResolver$Filter;->toQueryString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 415
    .local v4, "str":Ljava/lang/StringBuilder;
    move-object v0, p1

    .local v0, "arr$":[Lcom/samsung/android/health/HealthDataResolver$Filter;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 416
    .local v1, "filterEntry":Lcom/samsung/android/health/HealthDataResolver$Filter;
    invoke-static {v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;->checkFilter(Lcom/samsung/android/health/HealthDataResolver$Filter;)V

    .line 417
    const/4 v5, 0x0

    const/16 v6, 0x28

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    .line 418
    const-string v5, ") OR ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;->toQueryString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x29

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 415
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 420
    .end local v1    # "filterEntry":Lcom/samsung/android/health/HealthDataResolver$Filter;
    :cond_0
    new-instance v5, Lcom/samsung/android/health/HealthDataResolver$Filter;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v5
.end method

.method public static uuid(Ljava/lang/String;)Lcom/samsung/android/health/HealthDataResolver$Filter;
    .locals 3
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 465
    if-nez p0, :cond_0

    .line 466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    :cond_0
    new-instance v0, Lcom/samsung/android/health/HealthDataResolver$Filter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "datauuid = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/health/HealthDataResolver$Filter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public toQueryString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$Filter;->mQuery:Ljava/lang/String;

    return-object v0
.end method
