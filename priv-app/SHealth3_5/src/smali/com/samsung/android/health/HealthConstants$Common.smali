.class public interface abstract Lcom/samsung/android/health/HealthConstants$Common;
.super Ljava/lang/Object;
.source "HealthConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Common"
.end annotation


# static fields
.field public static final APPLICATION_ID:Ljava/lang/String; = "application_id"

.field public static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final DEVICE_UUID:Ljava/lang/String; = "deviceuuid"

.field public static final UPDATE_TIME:Ljava/lang/String; = "update_time"

.field public static final UUID:Ljava/lang/String; = "datauuid"
