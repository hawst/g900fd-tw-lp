.class public interface abstract Lcom/samsung/android/health/HealthResultHolder;
.super Ljava/lang/Object;
.source "HealthResultHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/HealthResultHolder$BaseResult;,
        Lcom/samsung/android/health/HealthResultHolder$ResultListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/samsung/android/health/HealthResultHolder$BaseResult;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract await()Lcom/samsung/android/health/HealthResultHolder$BaseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract cancel()V
.end method

.method public abstract setResultListener(Lcom/samsung/android/health/HealthResultHolder$ResultListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/health/HealthResultHolder$ResultListener",
            "<TT;>;)V"
        }
    .end annotation
.end method
