.class Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;
.super Landroid/os/Handler;
.source "HealthDataStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ErrorMessageHandler"
.end annotation


# instance fields
.field private final mStore:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/health/HealthDataStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/android/health/HealthDataStore;)V
    .locals 1
    .param p1, "store"    # Lcom/samsung/android/health/HealthDataStore;

    .prologue
    .line 205
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 206
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;->mStore:Ljava/lang/ref/WeakReference;

    .line 207
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 211
    iget-object v1, p0, Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;->mStore:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/health/HealthDataStore;

    .line 212
    .local v0, "store":Lcom/samsung/android/health/HealthDataStore;
    if-eqz v0, :cond_0

    .line 213
    iget v1, p1, Landroid/os/Message;->what:I

    # invokes: Lcom/samsung/android/health/HealthDataStore;->fireConnectionFailedEvent(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/health/HealthDataStore;->access$200(Lcom/samsung/android/health/HealthDataStore;I)V

    .line 215
    :cond_0
    return-void
.end method
