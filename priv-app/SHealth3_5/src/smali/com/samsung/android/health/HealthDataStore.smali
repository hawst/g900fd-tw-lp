.class public Lcom/samsung/android/health/HealthDataStore;
.super Ljava/lang/Object;
.source "HealthDataStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/health/HealthDataStore$ConnectionListener;,
        Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;
    }
.end annotation


# static fields
.field public static final CLIENT_KEY:Ljava/lang/String; = "client_version"

.field private static final CLIENT_VERSION:I = 0x1

.field private static final LOG_TAG:Ljava/lang/String; = "HealthDataStore"

.field private static final MESSAGE_DELAY_MSEC:I = 0x2


# instance fields
.field private final mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;

.field private mInterface:Lcom/samsung/android/health/IHealth;

.field private final mServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/health/HealthDataStore$ConnectionListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    new-instance v0, Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;-><init>(Lcom/samsung/android/health/HealthDataStore;)V

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataStore;->mHandler:Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;

    .line 173
    new-instance v0, Lcom/samsung/android/health/HealthDataStore$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/health/HealthDataStore$1;-><init>(Lcom/samsung/android/health/HealthDataStore;)V

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataStore;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 109
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataStore;->mContext:Landroid/content/Context;

    .line 110
    iput-object p2, p0, Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    .line 111
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/health/HealthDataStore;Lcom/samsung/android/health/IHealth;)Lcom/samsung/android/health/IHealth;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDataStore;
    .param p1, "x1"    # Lcom/samsung/android/health/IHealth;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataStore;->mInterface:Lcom/samsung/android/health/IHealth;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/HealthDataStore$ConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDataStore;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/health/HealthDataStore;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/health/HealthDataStore;
    .param p1, "x1"    # I

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/android/health/HealthDataStore;->fireConnectionFailedEvent(I)V

    return-void
.end method

.method private fireConnectionFailedEvent(I)V
    .locals 3
    .param p1, "errorCode"    # I

    .prologue
    .line 196
    const-string v0, "HealthDataStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to connect with Health Service fails (error code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    new-instance v1, Lcom/samsung/android/health/HealthConnectionErrorResult;

    invoke-direct {v1, p1}, Lcom/samsung/android/health/HealthConnectionErrorResult;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/samsung/android/health/HealthDataStore$ConnectionListener;->onConnectionFailed(Lcom/samsung/android/health/HealthConnectionErrorResult;)V

    .line 200
    :cond_0
    return-void
.end method

.method public static getInterface(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/IHealth;
    .locals 2
    .param p0, "store"    # Lcom/samsung/android/health/HealthDataStore;

    .prologue
    .line 162
    if-nez p0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "HealthDataStore is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore;->mInterface:Lcom/samsung/android/health/IHealth;

    if-nez v0, :cond_1

    .line 167
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Health data service is not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore;->mInterface:Lcom/samsung/android/health/IHealth;

    return-object v0
.end method


# virtual methods
.method public connectService()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 125
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.health.IHealthDataStore"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "client_version"

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 128
    iget-object v2, p0, Lcom/samsung/android/health/HealthDataStore;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 129
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Context is not specified(null)"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 132
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/health/HealthDataStore;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/health/HealthDataStore;->mServiceConnection:Landroid/content/ServiceConnection;

    const/16 v4, 0x81

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    .line 134
    .local v1, "isSuccessful":Z
    if-nez v1, :cond_1

    .line 135
    iget-object v2, p0, Lcom/samsung/android/health/HealthDataStore;->mHandler:Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;

    const-wide/16 v3, 0x2

    invoke-virtual {v2, v5, v3, v4}, Lcom/samsung/android/health/HealthDataStore$ErrorMessageHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 137
    :cond_1
    return-void
.end method

.method public disconnectService()V
    .locals 3

    .prologue
    .line 147
    iget-object v1, p0, Lcom/samsung/android/health/HealthDataStore;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/health/HealthDataStore;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/health/HealthDataStore;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "HealthDataStore"

    const-string v2, "disconnectService : IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
