.class public interface abstract Lcom/samsung/android/health/HealthConstants$Measurement;
.super Ljava/lang/Object;
.source "HealthConstants.java"

# interfaces
.implements Lcom/samsung/android/health/HealthConstants$Common;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Measurement"
.end annotation


# static fields
.field public static final COMMENT:Ljava/lang/String; = "comment"

.field public static final START_TIME:Ljava/lang/String; = "start_time"

.field public static final TIME_OFFSET:Ljava/lang/String; = "time_offset"
