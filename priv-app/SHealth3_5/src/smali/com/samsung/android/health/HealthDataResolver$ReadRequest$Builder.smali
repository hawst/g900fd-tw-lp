.class public Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;
.super Ljava/lang/Object;
.source "HealthDataResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDataResolver$ReadRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mBegin:J

.field private mDataType:Ljava/lang/String;

.field private mEnd:J

.field private mPackageName:Ljava/lang/String;

.field private mQuery:Ljava/lang/String;

.field private mSortField:Ljava/lang/String;

.field private mSortOrder:Lcom/samsung/android/health/HealthDataResolver$SortOrder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    return-void
.end method


# virtual methods
.method public build()Lcom/samsung/android/health/HealthDataResolver$ReadRequest;
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 254
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mDataType:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mDataType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 255
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data type or invalid data type is specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mBegin:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mEnd:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mBegin:J

    iget-wide v2, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mEnd:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    iget-wide v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mEnd:J

    iget-wide v2, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mBegin:J

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v5, 0x16e

    invoke-virtual {v2, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 258
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Illegal time range is specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :cond_3
    const/4 v4, 0x0

    .line 261
    .local v4, "sortOrder":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mSortField:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 262
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mSortOrder:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    if-eqz v0, :cond_5

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mSortField:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mSortOrder:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    invoke-virtual {v1}, Lcom/samsung/android/health/HealthDataResolver$SortOrder;->toSqlLiteral()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 269
    :cond_4
    :goto_0
    new-instance v0, Lcom/samsung/android/internal/ReadRequestImpl;

    iget-object v1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mDataType:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mPackageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mQuery:Ljava/lang/String;

    iget-wide v5, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mBegin:J

    iget-wide v7, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mEnd:J

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/internal/ReadRequestImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    return-object v0

    .line 265
    :cond_5
    iget-object v4, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mSortField:Ljava/lang/String;

    goto :goto_0
.end method

.method public final buildLastItem()Lcom/samsung/android/health/HealthDataResolver$ReadRequest;
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mDataType:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, ""

    iget-object v1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mDataType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 282
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No data type or invalid data type is specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mDataType:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/internal/ReadRequestImpl;->creatReadRequestForLastItem(Ljava/lang/String;)Lcom/samsung/android/internal/ReadRequestImpl;

    move-result-object v0

    return-object v0
.end method

.method public setDataType(Ljava/lang/String;)Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mDataType:Ljava/lang/String;

    .line 187
    return-object p0
.end method

.method public setFilter(Lcom/samsung/android/health/HealthDataResolver$Filter;)Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;
    .locals 1
    .param p1, "filter"    # Lcom/samsung/android/health/HealthDataResolver$Filter;

    .prologue
    .line 198
    instance-of v0, p1, Lcom/samsung/android/health/HealthDataResolver$Filter;

    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p1}, Lcom/samsung/android/health/HealthDataResolver$Filter;->toQueryString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mQuery:Ljava/lang/String;

    .line 203
    :goto_0
    return-object p0

    .line 201
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mQuery:Ljava/lang/String;

    goto :goto_0
.end method

.method public setPackageName(Ljava/lang/String;)Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mPackageName:Ljava/lang/String;

    .line 215
    return-object p0
.end method

.method public setSort(Ljava/lang/String;Lcom/samsung/android/health/HealthDataResolver$SortOrder;)Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;
    .locals 0
    .param p1, "field"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mSortField:Ljava/lang/String;

    .line 242
    iput-object p2, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mSortOrder:Lcom/samsung/android/health/HealthDataResolver$SortOrder;

    .line 243
    return-object p0
.end method

.method public setTimeRange(JJ)Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;
    .locals 0
    .param p1, "start"    # J
    .param p3, "end"    # J

    .prologue
    .line 227
    iput-wide p1, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mBegin:J

    .line 228
    iput-wide p3, p0, Lcom/samsung/android/health/HealthDataResolver$ReadRequest$Builder;->mEnd:J

    .line 229
    return-object p0
.end method
