.class Lcom/samsung/android/health/HealthDataStore$1;
.super Ljava/lang/Object;
.source "HealthDataStore.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDataStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/health/HealthDataStore;


# direct methods
.method constructor <init>(Lcom/samsung/android/health/HealthDataStore;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/samsung/android/health/HealthDataStore$1;->this$0:Lcom/samsung/android/health/HealthDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 176
    const-string v0, "HealthDataStore"

    const-string v1, "Service for HealthDataStore is connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore$1;->this$0:Lcom/samsung/android/health/HealthDataStore;

    invoke-static {p2}, Lcom/samsung/android/health/IHealth$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/health/IHealth;

    move-result-object v1

    # setter for: Lcom/samsung/android/health/HealthDataStore;->mInterface:Lcom/samsung/android/health/IHealth;
    invoke-static {v0, v1}, Lcom/samsung/android/health/HealthDataStore;->access$002(Lcom/samsung/android/health/HealthDataStore;Lcom/samsung/android/health/IHealth;)Lcom/samsung/android/health/IHealth;

    .line 179
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore$1;->this$0:Lcom/samsung/android/health/HealthDataStore;

    # getter for: Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/health/HealthDataStore;->access$100(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore$1;->this$0:Lcom/samsung/android/health/HealthDataStore;

    # getter for: Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/health/HealthDataStore;->access$100(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/health/HealthDataStore$ConnectionListener;->onConnected()V

    .line 182
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 186
    const-string v0, "HealthDataStore"

    const-string v1, "Service for HealthDataStore is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore$1;->this$0:Lcom/samsung/android/health/HealthDataStore;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/health/HealthDataStore;->mInterface:Lcom/samsung/android/health/IHealth;
    invoke-static {v0, v1}, Lcom/samsung/android/health/HealthDataStore;->access$002(Lcom/samsung/android/health/HealthDataStore;Lcom/samsung/android/health/IHealth;)Lcom/samsung/android/health/IHealth;

    .line 189
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore$1;->this$0:Lcom/samsung/android/health/HealthDataStore;

    # getter for: Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/health/HealthDataStore;->access$100(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/samsung/android/health/HealthDataStore$1;->this$0:Lcom/samsung/android/health/HealthDataStore;

    # getter for: Lcom/samsung/android/health/HealthDataStore;->mConnectionListener:Lcom/samsung/android/health/HealthDataStore$ConnectionListener;
    invoke-static {v0}, Lcom/samsung/android/health/HealthDataStore;->access$100(Lcom/samsung/android/health/HealthDataStore;)Lcom/samsung/android/health/HealthDataStore$ConnectionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/health/HealthDataStore$ConnectionListener;->onDisconnected()V

    .line 192
    :cond_0
    return-void
.end method
