.class Lcom/samsung/android/health/HealthDataObserver$DataHandler;
.super Landroid/os/Handler;
.source "HealthDataObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthDataObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DataHandler"
.end annotation


# instance fields
.field private final mObserver:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/health/HealthDataObserver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/android/health/HealthDataObserver;Landroid/os/Looper;)V
    .locals 1
    .param p1, "obs"    # Lcom/samsung/android/health/HealthDataObserver;
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 57
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 59
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/health/HealthDataObserver$DataHandler;->mObserver:Ljava/lang/ref/WeakReference;

    .line 60
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/samsung/android/health/HealthDataObserver$DataHandler;->mObserver:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/health/HealthDataObserver;

    .line 65
    .local v0, "obs":Lcom/samsung/android/health/HealthDataObserver;
    if-eqz v0, :cond_0

    .line 66
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/health/HealthDataObserver;->onChange(Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method
