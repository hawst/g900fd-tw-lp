.class public final enum Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
.super Ljava/lang/Enum;
.source "HealthPermissionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthPermissionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PermissionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/health/HealthPermissionManager$PermissionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

.field public static final enum READ:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

.field public static final enum WRITE:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 264
    new-instance v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    const-string v1, "READ"

    invoke-direct {v0, v1, v2, v2}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->READ:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    .line 270
    new-instance v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    const-string v1, "WRITE"

    invoke-direct {v0, v1, v3, v3}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->WRITE:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    .line 259
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    sget-object v1, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->READ:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->WRITE:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->$VALUES:[Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 276
    iput p3, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->mValue:I

    .line 277
    return-void
.end method

.method public static getType(I)Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    .locals 2
    .param p0, "value"    # I

    .prologue
    .line 296
    sget-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->READ:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    invoke-virtual {v0}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_0

    .line 297
    sget-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->READ:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    .line 299
    :goto_0
    return-object v0

    .line 298
    :cond_0
    sget-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->WRITE:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    invoke-virtual {v0}, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->getValue()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 299
    sget-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->WRITE:Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    goto :goto_0

    .line 301
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown input value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 259
    const-class v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/health/HealthPermissionManager$PermissionType;
    .locals 1

    .prologue
    .line 259
    sget-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->$VALUES:[Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    invoke-virtual {v0}, [Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/health/HealthPermissionManager$PermissionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionType;->mValue:I

    return v0
.end method
