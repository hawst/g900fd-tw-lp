.class public final Lcom/samsung/android/health/HealthConnectionErrorResult;
.super Ljava/lang/Object;
.source "HealthConnectionErrorResult.java"


# static fields
.field public static final CONNECTION_FAILURE:I = 0x1

.field public static final OLD_VERSION_PLATFORM:I = 0x4

.field public static final OLD_VERSION_SDK:I = 0x3

.field public static final PLATFORM_NOT_INSTALLED:I = 0x2

.field public static final TIMEOUT:I = 0x5

.field public static final UNKNOWN:I


# instance fields
.field private final mErrorCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput p1, p0, Lcom/samsung/android/health/HealthConnectionErrorResult;->mErrorCode:I

    .line 90
    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/samsung/android/health/HealthConnectionErrorResult;->mErrorCode:I

    return v0
.end method

.method public hasResolution()Z
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lcom/samsung/android/health/HealthConnectionErrorResult;->mErrorCode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/health/HealthConnectionErrorResult;->mErrorCode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 112
    :cond_0
    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resolve(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 125
    if-nez p1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The input argument is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    iget v0, p0, Lcom/samsung/android/health/HealthConnectionErrorResult;->mErrorCode:I

    packed-switch v0, :pswitch_data_0

    .line 137
    :pswitch_0
    return-void

    .line 129
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
