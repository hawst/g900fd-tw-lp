.class public Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;
.super Lcom/samsung/android/health/HealthResultHolder$BaseResult;
.source "HealthPermissionManager.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/health/HealthPermissionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PermissionResult"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mResultBundle:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 421
    new-instance v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult$1;

    invoke-direct {v0}, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult$1;-><init>()V

    sput-object v0, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 392
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/health/HealthResultHolder$BaseResult;-><init>(I)V

    .line 393
    iput-object p1, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;->mResultBundle:Landroid/os/Bundle;

    .line 394
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 397
    invoke-direct {p0, p1}, Lcom/samsung/android/health/HealthResultHolder$BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 398
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;->mResultBundle:Landroid/os/Bundle;

    .line 399
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/health/HealthPermissionManager$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/android/health/HealthPermissionManager$1;

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x0

    return v0
.end method

.method public getResultMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/samsung/android/health/HealthPermissionManager$PermissionKey;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 444
    iget-object v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;->mResultBundle:Landroid/os/Bundle;

    # invokes: Lcom/samsung/android/health/HealthPermissionManager;->convertBundleToPermissionMap(Landroid/os/Bundle;)Ljava/util/Map;
    invoke-static {v0}, Lcom/samsung/android/health/HealthPermissionManager;->access$100(Landroid/os/Bundle;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 414
    invoke-super {p0, p1, p2}, Lcom/samsung/android/health/HealthResultHolder$BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 415
    iget-object v0, p0, Lcom/samsung/android/health/HealthPermissionManager$PermissionResult;->mResultBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 416
    return-void
.end method
