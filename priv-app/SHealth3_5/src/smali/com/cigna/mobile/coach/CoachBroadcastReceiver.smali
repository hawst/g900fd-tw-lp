.class public Lcom/cigna/mobile/coach/CoachBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "CoachBroadcastReceiver.java"


# static fields
.field public static final ACTION_NAME:Ljava/lang/String; = "ACTION_NAME"

.field private static final CLASS_NAME:Ljava/lang/String;

.field public static final COACH_ACCOUNT_CHANGED:Ljava/lang/String; = "android.accounts.LOGIN_ACCOUNTS_CHANGED"

.field public static final COACH_STARTED_INTERNAL_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_STARTED"

.field public static final LOCALE_CHANGED_ACTION:Ljava/lang/String; = "android.intent.action.LOCALE_CHANGED"

.field public static final SHEALTH_BACKUP_STARTED:Ljava/lang/String; = "action_backup_started"

.field public static final SHEALTH_BOOT_COMPLETED:Ljava/lang/String; = "com.sec.shealth.action.SHEALTH_GET_BOOT_COMPLETED"

.field public static final SHEALTH_ERASE_STARTED:Ljava/lang/String; = "action_deletion_started"

.field public static final SHEALTH_INTENT_TIME_CHANGE:Ljava/lang/String; = "com.sec.android.app.shealth.cignacoach.TIME_CHANGE"

.field public static final SHEALTH_RESTORE_STARTED:Ljava/lang/String; = "action_restoration_started"

.field public static final SHEALTH_STARTED:Ljava/lang/String; = "com.sec.shealth.action.SHEALTH_STARTED"

.field public static final TIMEZONE_CHANGED_ACTION:Ljava/lang/String; = "android.intent.action.TIMEZONE_CHANGED"

.field public static final TIME_CHANGED_ACTION:Ljava/lang/String; = "android.intent.action.TIME_SET"

.field public static isCoachServiceStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->isCoachServiceStarted:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private startCoachService(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intentAction"    # Ljava/lang/String;

    .prologue
    .line 159
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/cigna/mobile/coach/CoachService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    .local v0, "service":Landroid/content/Intent;
    const-string v1, "ACTION_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 162
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v12, 0x0

    .line 68
    sget-object v9, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {p1, v9}, Lcom/cigna/coach/utils/SharedPrefUtils;->getCoachSettings(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Z

    move-result v5

    .line 69
    .local v5, "isCoachStartedAtLeastOnce":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, "intentAction":Ljava/lang/String;
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 72
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CoachBroadcastReceiver has received the following broadcast event: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    const-string v9, "com.sec.shealth.action.SHEALTH_GET_BOOT_COMPLETED"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "android.intent.action.TIME_SET"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v9, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 77
    :cond_1
    if-eqz v5, :cond_2

    .line 78
    invoke-direct {p0, p1, v4}, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->startCoachService(Landroid/content/Context;Ljava/lang/String;)V

    .line 156
    :cond_2
    :goto_0
    return-void

    .line 82
    :cond_3
    const-string v9, "action_backup_started"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 84
    const-string v9, "bStatus"

    invoke-virtual {p2, v9, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 85
    .local v8, "syncState":Z
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 86
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "syncState :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_4
    if-eqz v5, :cond_2

    .line 89
    invoke-direct {p0, p1, v4}, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->startCoachService(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 92
    .end local v8    # "syncState":Z
    :cond_5
    const-string v9, "action_restoration_started"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 94
    const-string v9, "bStatus"

    invoke-virtual {p2, v9, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 95
    .restart local v8    # "syncState":Z
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 96
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "syncState :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_6
    invoke-direct {p0, p1, v4}, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->startCoachService(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 100
    .end local v8    # "syncState":Z
    :cond_7
    const-string v9, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 102
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "ACCOUNT_CHANGED broadcast received"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 104
    .local v3, "b":Landroid/os/Bundle;
    if-eqz v5, :cond_2

    if-eqz v3, :cond_2

    .line 105
    const-string v9, "account"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 106
    .local v0, "accType":Landroid/accounts/Account;
    const-string/jumbo v9, "operation"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 107
    .local v6, "operation":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 108
    iget-object v2, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 109
    .local v2, "accountType":Ljava/lang/String;
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 110
    .local v1, "accountName":Ljava/lang/String;
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 111
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "accountName: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "accountType: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_8
    const-string v9, "com.osp.app.signin"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    if-eqz v6, :cond_2

    .line 115
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 116
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "operation: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_9
    const-string/jumbo v9, "remove"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 120
    sget-object v9, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->SAMSUNG_ACCOUNT_NAME:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {p1, v1, v9}, Lcom/cigna/coach/utils/SharedPrefUtils;->setCoachSetttingsString(Landroid/content/Context;Ljava/lang/String;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)V

    goto/16 :goto_0

    .line 125
    :cond_a
    const-string v9, "add"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 126
    sget-object v9, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->SAMSUNG_ACCOUNT_NAME:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {p1, v9}, Lcom/cigna/coach/utils/SharedPrefUtils;->getCoachSettingsString(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Ljava/lang/String;

    move-result-object v7

    .line 127
    .local v7, "samsungAccountName":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_b

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_b

    .line 128
    invoke-direct {p0, p1, v4}, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->startCoachService(Landroid/content/Context;Ljava/lang/String;)V

    .line 129
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 130
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Considered as Samsung account replace."

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 133
    :cond_b
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 134
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Ignored Account addition as the same user login again."

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    .end local v0    # "accType":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v2    # "accountType":Ljava/lang/String;
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v6    # "operation":Ljava/lang/String;
    .end local v7    # "samsungAccountName":Ljava/lang/String;
    :cond_c
    const-string v9, "action_deletion_started"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 142
    if-eqz v5, :cond_2

    .line 143
    invoke-direct {p0, p1, v4}, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->startCoachService(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 146
    :cond_d
    const-string v9, "com.cigna.mobile.coach.COACH_STARTED"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    sget-boolean v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->isCoachServiceStarted:Z

    if-nez v9, :cond_f

    .line 147
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 148
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CoachBroadcastReceiver service is not running already so, starting the service as boot load: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_e
    invoke-direct {p0, p1, v4}, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->startCoachService(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 152
    :cond_f
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 153
    sget-object v9, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "CoachBroadcastReceiver: ignoring the event: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
