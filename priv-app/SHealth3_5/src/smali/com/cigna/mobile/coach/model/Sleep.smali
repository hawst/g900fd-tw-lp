.class public Lcom/cigna/mobile/coach/model/Sleep;
.super Ljava/lang/Object;
.source "Sleep.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final COLUMN_AWAKE_COUNT:Ljava/lang/String; = "awake_count"

.field private static final COLUMN_BED_TIME:Ljava/lang/String; = "bed_time"

.field private static final COLUMN_CREATE_TIME:Ljava/lang/String; = "create_time"

.field private static final COLUMN_EFFICIENCY:Ljava/lang/String; = "efficiency"

.field private static final COLUMN_ID:Ljava/lang/String; = "_id"

.field private static final COLUMN_MOVEMENT:Ljava/lang/String; = "movement"

.field private static final COLUMN_NOISE:Ljava/lang/String; = "noise"

.field private static final COLUMN_QUALITY:Ljava/lang/String; = "quality"

.field private static final COLUMN_RISE_TIME:Ljava/lang/String; = "rise_time"

.field private static final COLUMN_SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field private static final COLUMN_SLEEP_ID:Ljava/lang/String; = "sleep__id"

.field private static final COLUMN_SLEEP_STATUS:Ljava/lang/String; = "sleep_status"

.field private static final COLUMN_UPDATE_TIME:Ljava/lang/String; = "update_time"

.field private static final COLUMN_USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"

.field private static columnNamesProjection:[Ljava/lang/String;

.field private static columnNamesProjection2:[Ljava/lang/String;

.field private static columnNamesRangeSelection:Ljava/lang/String;

.field private static columnNamesRangeSelection2:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 49
    const-class v0, Lcom/cigna/mobile/coach/model/Sleep;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    .line 73
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v0, v4

    const-string v1, "efficiency"

    aput-object v1, v0, v5

    const-string v1, "bed_time"

    aput-object v1, v0, v6

    const-string/jumbo v1, "rise_time"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "awake_count"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "quality"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "movement"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "noise"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "update_time"

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesProjection:[Ljava/lang/String;

    .line 76
    const-string v0, "(rise_time>bed_time) and (bed_time>=?) and (bed_time<=?)"

    sput-object v0, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesRangeSelection:Ljava/lang/String;

    .line 83
    new-array v0, v7, [Ljava/lang/String;

    const-string/jumbo v1, "sleep__id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "sleep_status"

    aput-object v1, v0, v4

    const-string/jumbo v1, "sample_time"

    aput-object v1, v0, v5

    const-string v1, "create_time"

    aput-object v1, v0, v6

    sput-object v0, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesProjection2:[Ljava/lang/String;

    .line 85
    const-string v0, "(sleep__id=?)"

    sput-object v0, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesRangeSelection2:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getSleepDataTrackerData(Ljava/util/List;Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepTrackerData;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 218
    .local p0, "lstd":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepTrackerData;>;"
    const/4 v6, 0x0

    .line 220
    .local v6, "cursor":Landroid/database/Cursor;
    sget-object v0, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 221
    sget-object v0, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Fetching Sleep Data Info from Tracker DB"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    if-eqz p0, :cond_3

    .line 227
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_3

    .line 228
    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/cigna/coach/dataobjects/SleepTrackerData;

    .line 229
    .local v11, "std":Lcom/cigna/coach/dataobjects/SleepTrackerData;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 236
    .local v9, "id":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_DATA_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v2, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesProjection2:[Ljava/lang/String;

    sget-object v3, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesRangeSelection2:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v9, v4, v5

    const-string v5, "create_time ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 239
    invoke-static {v6, p1}, Lcom/cigna/mobile/coach/model/Sleep;->processSleepDataResultSet(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/List;

    move-result-object v10

    .line 240
    .local v10, "lsdtd":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepDataTrackerData;>;"
    invoke-virtual {v11, v10}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setSleepDataTrackerDataList(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    if-eqz v6, :cond_1

    .line 247
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 227
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 242
    .end local v10    # "lsdtd":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepDataTrackerData;>;"
    :catch_0
    move-exception v7

    .line 243
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v0, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception querying TrackerDB: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    new-instance v0, Lcom/cigna/coach/exceptions/CoachException;

    invoke-virtual {v7}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 247
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 252
    .end local v8    # "i":I
    .end local v9    # "id":Ljava/lang/String;
    .end local v11    # "std":Lcom/cigna/coach/dataobjects/SleepTrackerData;
    :cond_3
    return-void
.end method

.method public static getSleepTrackerData(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/util/List;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startDate"    # Ljava/util/Calendar;
    .param p2, "endDate"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Calendar;",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepTrackerData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 104
    const/4 v7, 0x0

    .line 107
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    .line 111
    .local v12, "lstd":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepTrackerData;>;"
    :try_start_0
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Fetching Sleep Info from Tracker DB"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v14

    .line 116
    .local v14, "startTimeInMillis":J
    invoke-virtual/range {p2 .. p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v9

    .line 120
    .local v9, "endTimeInMillis":J
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v3, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesProjection:[Ljava/lang/String;

    sget-object v4, Lcom/cigna/mobile/coach/model/Sleep;->columnNamesRangeSelection:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v5, v6

    const/4 v6, 0x1

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v5, v6

    const-string/jumbo v6, "rise_time ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 123
    move-object/from16 v0, p0

    invoke-static {v7, v0}, Lcom/cigna/mobile/coach/model/Sleep;->processResultSet(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v12

    .line 129
    if-eqz v7, :cond_1

    .line 130
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 134
    :cond_1
    move-object/from16 v0, p0

    invoke-static {v12, v0}, Lcom/cigna/mobile/coach/model/Sleep;->getSleepDataTrackerData(Ljava/util/List;Landroid/content/Context;)V

    .line 137
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 138
    if-nez v12, :cond_3

    .line 139
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "From Sleep Tracker: \'No data returned Sleep table."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_2
    :goto_0
    return-object v12

    .line 142
    :cond_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .local v13, "msg":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    if-ge v11, v1, :cond_4

    .line 144
    const-string v1, " "

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/SleepTrackerData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string v1, "\n"

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 148
    :cond_4
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "From Sleep Tracker: \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 125
    .end local v9    # "endTimeInMillis":J
    .end local v11    # "j":I
    .end local v13    # "msg":Ljava/lang/StringBuilder;
    .end local v14    # "startTimeInMillis":J
    :catch_0
    move-exception v8

    .line 126
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception querying TrackerDB: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    new-instance v1, Lcom/cigna/coach/exceptions/CoachException;

    invoke-virtual {v8}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    move-object v2, v1

    if-eqz v7, :cond_5

    .line 130
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 134
    :cond_5
    move-object/from16 v0, p0

    invoke-static {v12, v0}, Lcom/cigna/mobile/coach/model/Sleep;->getSleepDataTrackerData(Ljava/util/List;Landroid/content/Context;)V

    .line 137
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 138
    if-nez v12, :cond_7

    .line 139
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "From Sleep Tracker: \'No data returned Sleep table."

    invoke-static {v1, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_6
    :goto_2
    throw v2

    .line 142
    :cond_7
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    .restart local v13    # "msg":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_3
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v1

    if-ge v11, v1, :cond_8

    .line 144
    const-string v1, " "

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-interface {v12, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/SleepTrackerData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string v1, "\n"

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 148
    :cond_8
    sget-object v1, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "From Sleep Tracker: \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private static processResultSet(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepTrackerData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    const/4 v0, 0x0

    .line 168
    .local v0, "listSleepTrackerData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepTrackerData;>;"
    if-eqz p0, :cond_2

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 169
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "listSleepTrackerData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepTrackerData;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .restart local v0    # "listSleepTrackerData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepTrackerData;>;"
    :cond_0
    new-instance v1, Lcom/cigna/coach/dataobjects/SleepTrackerData;

    invoke-direct {v1}, Lcom/cigna/coach/dataobjects/SleepTrackerData;-><init>()V

    .line 175
    .local v1, "std":Lcom/cigna/coach/dataobjects/SleepTrackerData;
    const-string v2, "_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setId(J)V

    .line 178
    const-string/jumbo v2, "user_device__id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setUserDeviceId(Ljava/lang/String;)V

    .line 181
    const-string v2, "efficiency"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setEfficiency(F)V

    .line 184
    const-string v2, "bed_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setBedTime(J)V

    .line 187
    const-string/jumbo v2, "rise_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setRiseTime(J)V

    .line 190
    const-string v2, "awake_count"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setAwakeCount(I)V

    .line 193
    const-string/jumbo v2, "quality"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setSleepQuality(I)V

    .line 196
    const-string/jumbo v2, "movement"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setMovement(I)V

    .line 199
    const-string/jumbo v2, "noise"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setNoise(Ljava/lang/String;)V

    .line 202
    const-string/jumbo v2, "update_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepTrackerData;->setUpdateTime(J)V

    .line 204
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    sget-object v2, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 209
    sget-object v2, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2, p0}, Lcom/cigna/coach/utils/Log;->printRowInCursor(Ljava/lang/String;Landroid/database/Cursor;)V

    .line 212
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 214
    .end local v1    # "std":Lcom/cigna/coach/dataobjects/SleepTrackerData;
    :cond_2
    return-object v0
.end method

.method private static processSleepDataResultSet(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepDataTrackerData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 263
    const/4 v0, 0x0

    .line 265
    .local v0, "listSleepDataTrackerData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepDataTrackerData;>;"
    if-eqz p0, :cond_2

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "listSleepDataTrackerData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepDataTrackerData;>;"
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 270
    .restart local v0    # "listSleepDataTrackerData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepDataTrackerData;>;"
    :cond_0
    new-instance v1, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;

    invoke-direct {v1}, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;-><init>()V

    .line 273
    .local v1, "sdtd":Lcom/cigna/coach/dataobjects/SleepDataTrackerData;
    const-string/jumbo v2, "sleep__id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->setSleepId(J)V

    .line 276
    const-string/jumbo v2, "sleep_status"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->setSleepStatus(J)V

    .line 279
    const-string/jumbo v2, "sample_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->setSampleTime(J)V

    .line 282
    const-string v2, "create_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->setCreateTime(J)V

    .line 284
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    sget-object v2, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 289
    sget-object v2, Lcom/cigna/mobile/coach/model/Sleep;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2, p0}, Lcom/cigna/coach/utils/Log;->printRowInCursor(Ljava/lang/String;Landroid/database/Cursor;)V

    .line 292
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 294
    .end local v1    # "sdtd":Lcom/cigna/coach/dataobjects/SleepDataTrackerData;
    :cond_2
    return-object v0
.end method
