.class public Lcom/cigna/mobile/coach/notification/BroadcastManager;
.super Ljava/lang/Object;
.source "BroadcastManager.java"


# static fields
.field private static final BACKUP_COMPLETED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_BACKUP_COMPLETED"

.field private static final BACKUP_FAILED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_BACKUP_FAILED"

.field private static final BACKUP_ON_PROGRESS_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_BACKUP_ON_PROGRESS"

.field private static final BACKUP_STARTED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_BACKUP_STARTED"

.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final COACH_NOT_SUPPORTED_LANGUAGE_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.NOT_SUPPORTED_LANGUAGE"

.field private static final COACH_PACKAGE_NAME:Ljava/lang/String; = "com.cigna.mobile.coach"

.field private static final COACH_WIDGET_INVALIDATE_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_WIDGET_INVALIDATE"

.field private static final CONTENT_LOADED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.CONTENT_LOAD_COMPLETE"

.field private static final CONTENT_UPDATE_REQUIRED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.CONTENT_UPDATE_REQUIRED"

.field private static final DEEP_LINK_SCORE:Ljava/lang/String; = "EXTRA_NAME_SCORE"

.field private static final DEEP_LINK_TRACKER_BASED:Ljava/lang/String; = "EXTRA_NAME_TRACKER_BASED"

.field private static final ERASE_COMPLETED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_ERASE_COMPLETED"

.field private static final ERASE_FAILED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_ERASE_FAILED"

.field private static final ERASE_ON_PROGRESS_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_ERASE_ON_PROGRESS"

.field private static final ERASE_STARTED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_ERASE_STARTED"

.field private static final GOAL_CANCELLED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.GOAL_CANCELLED"

.field private static final GOAL_COMPLETED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.GOAL_COMPLETED"

.field private static final MISSION_COMPLETED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.MISSION_COMPLETED"

.field private static final MISSION_FAILED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.MISSION_FAILED"

.field private static final RESTORE_COMPLETED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_RESTORE_COMPLETED"

.field private static final RESTORE_FAILED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_RESTORE_FAILED"

.field private static final RESTORE_ON_PROGRESS_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_RESTORE_ON_PROGRESS"

.field private static final RESTORE_STARTED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.COACH_RESTORE_STARTED"

.field private static final SCORE_CHANGED_INTENT:Ljava/lang/String; = "com.cigna.mobile.coach.SCORE_CHANGED"

.field private static instance:Lcom/cigna/mobile/coach/notification/BroadcastManager;


# instance fields
.field private context:Landroid/content/Context;

.field private executor:Lcom/cigna/coach/utils/IndempotentExecutor;

.field private isTrackerRunning:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    .line 51
    const/4 v0, 0x0

    sput-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->instance:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/cigna/coach/utils/IndempotentExecutor;

    const-wide/16 v1, 0x1388

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/IndempotentExecutor;-><init>(J)V

    iput-object v0, p0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->executor:Lcom/cigna/coach/utils/IndempotentExecutor;

    .line 57
    new-instance v0, Lcom/cigna/mobile/coach/notification/BroadcastManager$1;

    invoke-direct {v0, p0}, Lcom/cigna/mobile/coach/notification/BroadcastManager$1;-><init>(Lcom/cigna/mobile/coach/notification/BroadcastManager;)V

    iput-object v0, p0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning:Ljava/lang/ThreadLocal;

    .line 67
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 70
    sget-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->instance:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-direct {v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;-><init>()V

    sput-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->instance:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    .line 73
    :cond_0
    sget-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->instance:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    iput-object p0, v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->context:Landroid/content/Context;

    .line 74
    sget-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->instance:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    return-object v0
.end method


# virtual methods
.method public isTrackerRunning()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public sendBackupCompletedBroadcast()V
    .locals 3

    .prologue
    .line 186
    const-string v1, "com.cigna.mobile.coach.COACH_BACKUP_COMPLETED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 187
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING BACKUP_COMPLETED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 191
    return-void
.end method

.method public sendBackupFailedBroadcast()V
    .locals 3

    .prologue
    .line 194
    const-string v1, "com.cigna.mobile.coach.COACH_BACKUP_FAILED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 195
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING BACKUP_FAILED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 199
    return-void
.end method

.method public sendBackupOnProgressBroadcast()V
    .locals 3

    .prologue
    .line 178
    const-string v1, "com.cigna.mobile.coach.COACH_BACKUP_ON_PROGRESS"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 179
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 180
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING BACKUP_ON_PROGRESS_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 183
    return-void
.end method

.method public sendBackupStartedBroadcast()V
    .locals 3

    .prologue
    .line 170
    const-string v1, "com.cigna.mobile.coach.COACH_BACKUP_STARTED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 171
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING BACKUP_STARTED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 175
    return-void
.end method

.method public sendBroadcast(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending a broadcast:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    :try_start_0
    iget-object v1, p0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending a broadcast exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendContentUpdateCompleteBroadcast()V
    .locals 2

    .prologue
    .line 115
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cigna.mobile.coach.CONTENT_LOAD_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 116
    return-void
.end method

.method public sendEraseCompletedBroadcast()V
    .locals 3

    .prologue
    .line 250
    const-string v1, "com.cigna.mobile.coach.COACH_ERASE_COMPLETED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 251
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING ERASE_COMPLETED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 255
    return-void
.end method

.method public sendEraseFailedBroadcast()V
    .locals 3

    .prologue
    .line 258
    const-string v1, "com.cigna.mobile.coach.COACH_ERASE_FAILED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 259
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING ERASE_FAILED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 263
    return-void
.end method

.method public sendEraseOnProgresBroadcast()V
    .locals 3

    .prologue
    .line 242
    const-string v1, "com.cigna.mobile.coach.COACH_ERASE_ON_PROGRESS"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 243
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING ERASE_ON_PROGRESS_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 247
    return-void
.end method

.method public sendEraseStartedBroadcast()V
    .locals 3

    .prologue
    .line 234
    const-string v1, "com.cigna.mobile.coach.COACH_ERASE_STARTED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 235
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING ERASE_STARTED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 239
    return-void
.end method

.method public sendGoalCancelledBroadcast(Lcom/cigna/coach/dataobjects/UserGoalData;)V
    .locals 3
    .param p1, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    .line 161
    const-string v1, "com.cigna.mobile.coach.GOAL_CANCELLED"

    invoke-static {p1}, Lcom/cigna/coach/utils/CoachMessageHelper;->createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserGoalData;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 162
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_TRACKER_BASED"

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 163
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 164
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING GOAL_CANCELLED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 167
    return-void
.end method

.method public sendGoalCompletedBroadcast(Lcom/cigna/coach/dataobjects/UserGoalData;)V
    .locals 3
    .param p1, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    .line 152
    const-string v1, "com.cigna.mobile.coach.GOAL_COMPLETED"

    invoke-static {p1}, Lcom/cigna/coach/utils/CoachMessageHelper;->createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserGoalData;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 153
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_TRACKER_BASED"

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 154
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING GOAL_COMPLETED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 158
    return-void
.end method

.method public sendIndempotentBroadcast(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 98
    sget-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    sget-object v0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sending indempotent broadcast:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->executor:Lcom/cigna/coach/utils/IndempotentExecutor;

    new-instance v1, Lcom/cigna/mobile/coach/notification/BroadcastManager$2;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1}, Lcom/cigna/mobile/coach/notification/BroadcastManager$2;-><init>(Lcom/cigna/mobile/coach/notification/BroadcastManager;Ljava/lang/Object;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/IndempotentExecutor;->execute(Ljava/lang/Runnable;)V

    .line 109
    return-void
.end method

.method public sendInvalidateCoachWidget()V
    .locals 3

    .prologue
    .line 273
    const-string v1, "com.cigna.mobile.coach.COACH_WIDGET_INVALIDATE"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 274
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 275
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING COACH_WIDGET_INVALIDATE_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendIndempotentBroadcast(Landroid/content/Intent;)V

    .line 278
    return-void
.end method

.method public sendMissionCompletedBroadcast(Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 3
    .param p1, "mission"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    .line 134
    const-string v1, "com.cigna.mobile.coach.MISSION_COMPLETED"

    invoke-static {p1}, Lcom/cigna/coach/utils/CoachMessageHelper;->createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 135
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_TRACKER_BASED"

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 136
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING MISSION_COMPLETED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 140
    return-void
.end method

.method public sendMissionFailedBroadcast(Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 3
    .param p1, "mission"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    .line 143
    const-string v1, "com.cigna.mobile.coach.MISSION_FAILED"

    invoke-static {p1}, Lcom/cigna/coach/utils/CoachMessageHelper;->createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/Map;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_TRACKER_BASED"

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 145
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING MISSION_FAILED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 149
    return-void
.end method

.method public sendRestoreCompletedBroadcast()V
    .locals 3

    .prologue
    .line 218
    const-string v1, "com.cigna.mobile.coach.COACH_RESTORE_COMPLETED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 219
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING RESTORE_COMPLETED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 223
    return-void
.end method

.method public sendRestoreFailedBroadcast()V
    .locals 3

    .prologue
    .line 226
    const-string v1, "com.cigna.mobile.coach.COACH_RESTORE_FAILED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 227
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 228
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING RESTORE_FAILED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 231
    return-void
.end method

.method public sendRestoreOnProgresBroadcast()V
    .locals 3

    .prologue
    .line 210
    const-string v1, "com.cigna.mobile.coach.COACH_RESTORE_ON_PROGRESS"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 211
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING RESTORE_ON_PROGRESS_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 215
    return-void
.end method

.method public sendRestoreStartedBroadcast()V
    .locals 3

    .prologue
    .line 202
    const-string v1, "com.cigna.mobile.coach.COACH_RESTORE_STARTED"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 203
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    sget-object v1, Lcom/cigna/mobile/coach/notification/BroadcastManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "SENDING RESTORE_STARTED_INTENT..."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 207
    return-void
.end method

.method public sendScoreChangedBroadcast(I)V
    .locals 3
    .param p1, "score"    # I

    .prologue
    .line 266
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cigna.mobile.coach.SCORE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 267
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "EXTRA_NAME_SCORE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 268
    const-string v1, "EXTRA_NAME_TRACKER_BASED"

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 269
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 270
    return-void
.end method

.method public sendStartServiceCompleteBroadcast()V
    .locals 2

    .prologue
    .line 122
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.cigna.mobile.coach.COACH_STARTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBroadcast(Landroid/content/Intent;)V

    .line 123
    return-void
.end method

.method public setTrackerRunning(Z)V
    .locals 2
    .param p1, "active"    # Z

    .prologue
    .line 126
    iget-object v0, p0, Lcom/cigna/mobile/coach/notification/BroadcastManager;->isTrackerRunning:Ljava/lang/ThreadLocal;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 127
    return-void
.end method
