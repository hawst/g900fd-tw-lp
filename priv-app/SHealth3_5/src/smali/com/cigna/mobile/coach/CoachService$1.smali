.class Lcom/cigna/mobile/coach/CoachService$1;
.super Ljava/lang/Object;
.source "CoachService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/mobile/coach/CoachService;->createTimer(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/mobile/coach/CoachService;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/cigna/mobile/coach/CoachService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    iput-object p2, p0, Lcom/cigna/mobile/coach/CoachService$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 169
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 170
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Timer logic running. notificationType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    # getter for: Lcom/cigna/mobile/coach/CoachService;->notificationType:I
    invoke-static {v4}, Lcom/cigna/mobile/coach/CoachService;->access$100(Lcom/cigna/mobile/coach/CoachService;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_0
    new-instance v1, Lcom/cigna/mobile/coach/DataProcessor;

    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService$1;->val$context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/cigna/mobile/coach/DataProcessor;-><init>(Landroid/content/Context;)V

    .line 174
    .local v1, "dataProcessor":Lcom/cigna/mobile/coach/DataProcessor;
    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    # getter for: Lcom/cigna/mobile/coach/CoachService;->notificationType:I
    invoke-static {v2}, Lcom/cigna/mobile/coach/CoachService;->access$100(Lcom/cigna/mobile/coach/CoachService;)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 175
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 176
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Running STARTUP check for inactivity"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_1
    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    iget-object v3, p0, Lcom/cigna/mobile/coach/CoachService$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/cigna/mobile/coach/CoachService;->checkAndLoadLanguageContent(Landroid/content/Context;)V

    .line 181
    sget-object v2, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->STARTUP:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    invoke-virtual {v1, v2}, Lcom/cigna/mobile/coach/DataProcessor;->checkForInactivity(Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;)V

    .line 183
    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    const/4 v3, -0x1

    # setter for: Lcom/cigna/mobile/coach/CoachService;->notificationType:I
    invoke-static {v2, v3}, Lcom/cigna/mobile/coach/CoachService;->access$102(Lcom/cigna/mobile/coach/CoachService;I)I

    .line 186
    invoke-virtual {v1}, Lcom/cigna/mobile/coach/DataProcessor;->isNotificationTimeWindowOpen()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 187
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 188
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Service was created, checking for queued notifications"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_2
    invoke-virtual {v1}, Lcom/cigna/mobile/coach/DataProcessor;->deliverQueuedNotifications()V

    .line 218
    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    # getter for: Lcom/cigna/mobile/coach/CoachService;->timerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/cigna/mobile/coach/CoachService;->access$300(Lcom/cigna/mobile/coach/CoachService;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    iget-object v4, p0, Lcom/cigna/mobile/coach/CoachService$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/cigna/mobile/coach/CoachService;->getTimeDelayValue(Landroid/content/Context;)J
    invoke-static {v3, v4}, Lcom/cigna/mobile/coach/CoachService;->access$200(Lcom/cigna/mobile/coach/CoachService;Landroid/content/Context;)J

    move-result-wide v3

    invoke-virtual {v2, p0, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    .line 219
    .local v0, "bTimerStarted":Z
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 220
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Timer done still running="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_4
    return-void

    .line 192
    .end local v0    # "bTimerStarted":Z
    :cond_5
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 193
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Service was created, but not an appropriate time of day to check for queued notifications"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_6
    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    # getter for: Lcom/cigna/mobile/coach/CoachService;->notificationType:I
    invoke-static {v2}, Lcom/cigna/mobile/coach/CoachService;->access$100(Lcom/cigna/mobile/coach/CoachService;)I

    move-result v2

    if-nez v2, :cond_8

    .line 197
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 198
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Running DAILY check for inactivity"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_7
    sget-object v2, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DAILY:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    invoke-virtual {v1, v2}, Lcom/cigna/mobile/coach/DataProcessor;->checkForInactivity(Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;)V

    goto :goto_0

    .line 201
    :cond_8
    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService$1;->this$0:Lcom/cigna/mobile/coach/CoachService;

    # getter for: Lcom/cigna/mobile/coach/CoachService;->notificationType:I
    invoke-static {v2}, Lcom/cigna/mobile/coach/CoachService;->access$100(Lcom/cigna/mobile/coach/CoachService;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 204
    invoke-virtual {v1}, Lcom/cigna/mobile/coach/DataProcessor;->isNotificationTimeWindowOpen()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 205
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 206
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Checking for queued notifications"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_9
    invoke-virtual {v1}, Lcom/cigna/mobile/coach/DataProcessor;->deliverQueuedNotifications()V

    goto/16 :goto_0

    .line 210
    :cond_a
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 211
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Was supposed to check for queued notifications, but the notification start must have been changed in the meantime"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
