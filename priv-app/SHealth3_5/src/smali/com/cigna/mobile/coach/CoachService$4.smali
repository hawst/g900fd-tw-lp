.class Lcom/cigna/mobile/coach/CoachService$4;
.super Ljava/lang/Object;
.source "CoachService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/mobile/coach/CoachService;->checkAndLoadLanguageContent(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/mobile/coach/CoachService;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/cigna/mobile/coach/CoachService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/cigna/mobile/coach/CoachService$4;->this$0:Lcom/cigna/mobile/coach/CoachService;

    iput-object p2, p0, Lcom/cigna/mobile/coach/CoachService$4;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 603
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 604
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "loadLanguageContent start time = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 606
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v3, p0, Lcom/cigna/mobile/coach/CoachService$4;->val$context:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 607
    .local v1, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 609
    .local v0, "ch":Lcom/cigna/coach/utils/ContentHelper;
    :try_start_0
    invoke-virtual {v0}, Lcom/cigna/coach/utils/ContentHelper;->checkAndLoadContent()V

    .line 610
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 615
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 618
    :goto_0
    return-void

    .line 611
    :catch_0
    move-exception v2

    .line 612
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    :try_start_1
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CoachException trying to get new language loaded: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 615
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto :goto_0

    .end local v2    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v3
.end method
