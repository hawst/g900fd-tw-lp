.class public Lcom/cigna/mobile/coach/DataProcessor;
.super Ljava/lang/Object;
.source "DataProcessor.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final DEFAULT_USERID:Ljava/lang/String; = "1"

.field public static final SHEALTH_EXERCISE_INFO_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_EXERCISE_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_EXERCISE_TABLE:Ljava/lang/String; = "exercise"

.field public static final SHEALTH_EXERCISE_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_EXERCISE_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_FOOD_INFO_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_FOOD_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_FOOD_NUTRIENT_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_FOOD_NUTRIENT_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_FOOD_TABLE:Ljava/lang/String; = "meal_item"

.field public static final SHEALTH_MEAL_ITEM_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_MEAL_ITEM_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_MEAL_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_MEAL_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_PEDOMETER_TABLE:Ljava/lang/String; = "walk_info"

.field public static final SHEALTH_SLEEP_DATA_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_SLEEP_DATA_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_SLEEP_TABLE:Ljava/lang/String; = "sleep"

.field public static final SHEALTH_SLEEP_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_SLEEP_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_WALK_INFO_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_WALK_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

.field public static final SHEALTH_WEIGHT_TABLE:Ljava/lang/String; = "weight"

.field public static final SHEALTH_WEIGHT_TABLE_CP_URI:Landroid/net/Uri;

.field public static final SHEALTH_WEIGHT_TABLE_STRING_CP_URI:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/cigna/mobile/coach/DataProcessor;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    .line 83
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 84
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Exercise;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_TABLE_CP_URI:Landroid/net/Uri;

    .line 85
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 86
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$ExerciseInfo;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_INFO_TABLE_CP_URI:Landroid/net/Uri;

    .line 87
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_FOOD_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 88
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodInfo;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_FOOD_INFO_TABLE_CP_URI:Landroid/net/Uri;

    .line 89
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_FOOD_NUTRIENT_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 90
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$FoodNutrient;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_FOOD_NUTRIENT_TABLE_CP_URI:Landroid/net/Uri;

    .line 91
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_MEAL_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 92
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Meal;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_MEAL_TABLE_CP_URI:Landroid/net/Uri;

    .line 93
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_MEAL_ITEM_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 94
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$MealItem;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_MEAL_ITEM_TABLE_CP_URI:Landroid/net/Uri;

    .line 95
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WALK_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 96
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$WalkInfo;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WALK_INFO_TABLE_CP_URI:Landroid/net/Uri;

    .line 97
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 98
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Sleep;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_TABLE_CP_URI:Landroid/net/Uri;

    .line 99
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_DATA_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 100
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$SleepData;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_DATA_TABLE_CP_URI:Landroid/net/Uri;

    .line 101
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_STRING_CP_URI:Ljava/lang/String;

    .line 102
    sget-object v0, Lcom/samsung/android/sdk/health/content/ShealthContract$Weight;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_CP_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    .line 108
    invoke-direct {p0}, Lcom/cigna/mobile/coach/DataProcessor;->setContentProviderUris()V

    .line 110
    return-void
.end method

.method public static getMillisecondsSinceMidnight()J
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 443
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 444
    .local v1, "now":Ljava/util/Calendar;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 445
    .local v0, "midnight":Ljava/util/Calendar;
    const/16 v2, 0xb

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 446
    const/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 447
    const/16 v2, 0xd

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 448
    const/16 v2, 0xe

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 450
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    return-wide v2
.end method

.method public static getTrackerEventType(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    .locals 4
    .param p0, "trackerType"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    .local v0, "trackerEventType":Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 368
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTrackerEventType, trackerType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    :cond_0
    sget-object v1, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->EXERCISE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    if-ne p0, v1, :cond_3

    .line 372
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->EXERCISE:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 390
    :cond_1
    :goto_0
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 391
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TrackerEventType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :cond_2
    return-object v0

    .line 373
    :cond_3
    sget-object v1, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->PEDOMETER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    if-ne p0, v1, :cond_4

    .line 375
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->PEDOMETER_DATA:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 376
    :cond_4
    sget-object v1, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->WEIGHT:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    if-ne p0, v1, :cond_5

    .line 378
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 379
    :cond_5
    sget-object v1, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->FOOD:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    if-ne p0, v1, :cond_6

    .line 381
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->FOOD:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 382
    :cond_6
    sget-object v1, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->SLEEP:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    if-ne p0, v1, :cond_7

    .line 384
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->SLEEP:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 386
    :cond_7
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 387
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "CoachListener: Unknown trackerType"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getTrackerEventType(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    .locals 4
    .param p0, "sUri"    # Ljava/lang/String;

    .prologue
    .line 335
    const/4 v0, 0x0

    .line 336
    .local v0, "trackerEventType":Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 337
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CoachListener: sending Exercise event to Coach API: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_0
    const-string v1, "exercise"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 341
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->EXERCISE:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 359
    :cond_1
    :goto_0
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 360
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TrackerEventType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_2
    return-object v0

    .line 342
    :cond_3
    const-string/jumbo v1, "walk_info"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 344
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->PEDOMETER_DATA:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 345
    :cond_4
    const-string/jumbo v1, "weight"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 347
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 348
    :cond_5
    const-string/jumbo v1, "meal_item"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 350
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->FOOD:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 351
    :cond_6
    const-string/jumbo v1, "sleep"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 353
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->SLEEP:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    goto :goto_0

    .line 355
    :cond_7
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 356
    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "CoachListener: Unknown URI update. Ignore."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private logNotificationList(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 483
    .local p1, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 484
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/Notification;

    .line 485
    .local v1, "notification":Lcom/cigna/coach/apiobjects/Notification;
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Notification *****"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getFeature : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Notification;->getFeature()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getImage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Notification;->getImage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLanguageCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Notification;->getLanguageCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMessageLine1 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Notification;->getMessageLine1()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getMessageLine2 : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Notification;->getMessageLine2()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getTitle : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Notification;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getUiType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Notification;->getUiType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 495
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "notification":Lcom/cigna/coach/apiobjects/Notification;
    :cond_0
    return-void
.end method

.method private sendNotifications(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 187
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "CoachListener: processing notification List"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/NotificationEngine;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->sendNotifications(Ljava/util/List;)Z

    .line 196
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "No notifications to send"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setContentProviderUris()V
    .locals 3

    .prologue
    .line 122
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exercise ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ExerciseInfo ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FoodInfo ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_FOOD_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FoodNutrient ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_FOOD_NUTRIENT_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Meal ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_MEAL_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MealItem ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_MEAL_ITEM_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sleep ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Weight ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-object v0, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WalkInfo ContentProviderURI updated to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WALK_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_0
    return-void
.end method


# virtual methods
.method public checkForInactivity(Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;)V
    .locals 6
    .param p1, "activityEventType"    # Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    .prologue
    .line 420
    sget-object v3, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->DATA_PROCESSOR:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-static {v3}, Lcom/cigna/coach/factory/InternalCoachFactory;->getInstance(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)Lcom/cigna/coach/factory/InternalCoachFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/InternalCoachFactory;->getTracker(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ITracker;

    move-result-object v2

    .line 423
    .local v2, "tracker":Lcom/cigna/coach/interfaces/ITracker;
    :try_start_0
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 424
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "checkForInactivity()"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :cond_0
    invoke-interface {v2, p1}, Lcom/cigna/coach/interfaces/ITracker;->checkForInactivity(Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;)Ljava/util/List;

    move-result-object v1

    .line 427
    .local v1, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    invoke-direct {p0, v1}, Lcom/cigna/mobile/coach/DataProcessor;->logNotificationList(Ljava/util/List;)V

    .line 428
    invoke-direct {p0, v1}, Lcom/cigna/mobile/coach/DataProcessor;->sendNotifications(Ljava/util/List;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 434
    .end local v1    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :goto_0
    return-void

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CoachException triggering the check for inactivity in the Coach API: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deliverQueuedNotifications()V
    .locals 6

    .prologue
    .line 459
    sget-object v3, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->DATA_PROCESSOR:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-static {v3}, Lcom/cigna/coach/factory/InternalCoachFactory;->getInstance(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)Lcom/cigna/coach/factory/InternalCoachFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/InternalCoachFactory;->getTracker(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ITracker;

    move-result-object v2

    .line 462
    .local v2, "tracker":Lcom/cigna/coach/interfaces/ITracker;
    :try_start_0
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 463
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "deliverQueuedNotifications()"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_0
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/cigna/coach/interfaces/ITracker;->getQueuedNotifications(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 468
    .local v1, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    invoke-direct {p0, v1}, Lcom/cigna/mobile/coach/DataProcessor;->logNotificationList(Ljava/util/List;)V

    .line 469
    invoke-direct {p0, v1}, Lcom/cigna/mobile/coach/DataProcessor;->sendNotifications(Ljava/util/List;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 475
    .end local v1    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :goto_0
    return-void

    .line 470
    :catch_0
    move-exception v0

    .line 471
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CoachException triggering the check for inactivity in the Coach API: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getMealFoodTrackerInfo(JJ)Ljava/util/List;
    .locals 1
    .param p1, "startTimeInMillis"    # J
    .param p3, "endTimeInMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MealFoodTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 212
    iget-object v0, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/cigna/mobile/coach/model/Food;->getFoodData(Landroid/content/Context;JJ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMetExerciseTrackerInfo(JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;
    .locals 9
    .param p1, "startTimeInMillis"    # J
    .param p3, "endTimeInMillis"    # J
    .param p5, "exerciseType1"    # Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .param p6, "exerciseType2"    # Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 242
    new-instance v0, Lcom/cigna/mobile/coach/model/Exercise;

    invoke-direct {v0}, Lcom/cigna/mobile/coach/model/Exercise;-><init>()V

    .line 243
    .local v0, "exercise":Lcom/cigna/mobile/coach/model/Exercise;
    iget-object v1, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/cigna/mobile/coach/model/Exercise;->getExerciseData(Landroid/content/Context;JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;

    move-result-object v8

    .line 245
    .local v8, "listExerciseInfo":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    return-object v8
.end method

.method public getPedoData(Landroid/content/Context;JJ)Ljava/util/List;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "startTimeInMillis"    # J
    .param p4, "endTimeInMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 261
    new-instance v0, Lcom/cigna/mobile/coach/model/Exercise;

    invoke-direct {v0}, Lcom/cigna/mobile/coach/model/Exercise;-><init>()V

    .local v0, "exercise":Lcom/cigna/mobile/coach/model/Exercise;
    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 263
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/mobile/coach/model/Exercise;->getPedoData(Landroid/content/Context;JJ)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getSleepTrackerData(Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/util/List;
    .locals 1
    .param p1, "calStart"    # Ljava/util/Calendar;
    .param p2, "calEnd"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepTrackerData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 279
    iget-object v0, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/cigna/mobile/coach/model/Sleep;->getSleepTrackerData(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getUserProfileInfo()Lcom/cigna/coach/apiobjects/UserMetrics;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/cigna/mobile/coach/model/User;->getUserData(Landroid/content/Context;)Lcom/cigna/coach/apiobjects/UserMetrics;

    move-result-object v0

    return-object v0
.end method

.method public getWeightData(Landroid/content/Context;JJ)Ljava/util/List;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "startTimeInMillis"    # J
    .param p4, "endTimeInMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/WeightTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 304
    invoke-static {p1, p2, p3, p4, p5}, Lcom/cigna/mobile/coach/model/User;->getWeightData(Landroid/content/Context;JJ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isNotificationTimeWindowOpen()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 406
    :try_start_0
    sget-object v3, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->DATA_PROCESSOR:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-static {v3}, Lcom/cigna/coach/factory/InternalCoachFactory;->getInstance(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)Lcom/cigna/coach/factory/InternalCoachFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/InternalCoachFactory;->getTracker(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ITracker;

    move-result-object v1

    .line 407
    .local v1, "tracker":Lcom/cigna/coach/interfaces/ITracker;
    invoke-static {}, Lcom/cigna/mobile/coach/DataProcessor;->getMillisecondsSinceMidnight()J

    move-result-wide v3

    invoke-interface {v1}, Lcom/cigna/coach/interfaces/ITracker;->getNotificationStartTime()J
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 410
    .end local v1    # "tracker":Lcom/cigna/coach/interfaces/ITracker;
    :cond_0
    :goto_0
    return v2

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CoachException trying to get notification start time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateCoach(Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)V
    .locals 6
    .param p1, "trackerEventType"    # Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .prologue
    .line 315
    if-nez p1, :cond_0

    .line 332
    :goto_0
    return-void

    .line 318
    :cond_0
    const/4 v1, 0x0

    .line 319
    .local v1, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    sget-object v3, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->DATA_PROCESSOR:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-static {v3}, Lcom/cigna/coach/factory/InternalCoachFactory;->getInstance(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)Lcom/cigna/coach/factory/InternalCoachFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/mobile/coach/DataProcessor;->context:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/cigna/coach/factory/InternalCoachFactory;->getTracker(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ITracker;

    move-result-object v2

    .line 324
    .local v2, "tracker":Lcom/cigna/coach/interfaces/ITracker;
    if-eqz p1, :cond_1

    .line 325
    :try_start_0
    const-string v3, "1"

    invoke-interface {v2, v3, p1}, Lcom/cigna/coach/interfaces/ITracker;->processTrackerChange(Ljava/lang/String;Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)Ljava/util/List;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 331
    :cond_1
    :goto_1
    invoke-direct {p0, v1}, Lcom/cigna/mobile/coach/DataProcessor;->sendNotifications(Ljava/util/List;)V

    goto :goto_0

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CoachException notifying Coach API of the SHealth database change: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
