.class public Lcom/cigna/mobile/coach/model/Exercise;
.super Ljava/lang/Object;
.source "Exercise.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final COLUMN_APPLICATION_ID:Ljava/lang/String; = "application__id"

.field private static final COLUMN_DISTANCE:Ljava/lang/String; = "distance"

.field private static final COLUMN_DURATION_MSEC:Ljava/lang/String; = "duration_millisecond"

.field private static final COLUMN_END_TIME:Ljava/lang/String; = "end_time"

.field private static final COLUMN_EXERCISE_ID:Ljava/lang/String; = "exercise__id"

.field private static final COLUMN_EXERCISE_INFO:Ljava/lang/String; = "exercise_info__id"

.field private static final COLUMN_EXERCISE_TYPE:Ljava/lang/String; = "exercise_type"

.field private static final COLUMN_ID:Ljava/lang/String; = "_id"

.field private static final COLUMN_MET:Ljava/lang/String; = "met"

.field private static final COLUMN_NAME:Ljava/lang/String; = "name"

.field private static final COLUMN_RUN_STEP:Ljava/lang/String; = "run_step"

.field private static final COLUMN_SPEED:Ljava/lang/String; = "speed"

.field private static final COLUMN_START_TIME:Ljava/lang/String; = "start_time"

.field private static final COLUMN_TOTAL_STEP:Ljava/lang/String; = "total_step"

.field private static final COLUMN_UPDOWN_STEP:Ljava/lang/String; = "updown_step"

.field private static final COLUMN_USER_DEVICE_ID:Ljava/lang/String; = "user_device__id"

.field private static final COLUMN_WALK_STEP:Ljava/lang/String; = "walk_step"

.field private static exerciseColumnNamesProjection:[Ljava/lang/String;

.field private static exerciseColumnNamesRangeSelection:Ljava/lang/String;

.field private static exerciseInfoColumnNamesProjection:[Ljava/lang/String;

.field private static exerciseInfoContent:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;",
            ">;"
        }
    .end annotation
.end field

.field private static walkInfoColumnNamesProjection:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    const-class v0, Lcom/cigna/mobile/coach/model/Exercise;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    .line 105
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "exercise_info__id"

    aput-object v1, v0, v3

    const-string v1, "exercise_type"

    aput-object v1, v0, v4

    const-string/jumbo v1, "start_time"

    aput-object v1, v0, v5

    const-string v1, "duration_millisecond"

    aput-object v1, v0, v6

    sput-object v0, Lcom/cigna/mobile/coach/model/Exercise;->exerciseColumnNamesProjection:[Ljava/lang/String;

    .line 112
    const-string v0, "(start_time >=?) AND (start_time <=?) "

    sput-object v0, Lcom/cigna/mobile/coach/model/Exercise;->exerciseColumnNamesRangeSelection:Ljava/lang/String;

    .line 118
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string/jumbo v1, "met"

    aput-object v1, v0, v3

    const-string/jumbo v1, "name"

    aput-object v1, v0, v4

    sput-object v0, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoColumnNamesProjection:[Ljava/lang/String;

    .line 124
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "exercise__id"

    aput-object v1, v0, v2

    const-string v1, "application__id"

    aput-object v1, v0, v3

    const-string/jumbo v1, "user_device__id"

    aput-object v1, v0, v4

    const-string/jumbo v1, "start_time"

    aput-object v1, v0, v5

    const-string v1, "end_time"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "distance"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "speed"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "total_step"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "run_step"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "walk_step"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "updown_step"

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/mobile/coach/model/Exercise;->walkInfoColumnNamesProjection:[Ljava/lang/String;

    .line 128
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoContent:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addExerciseInfoContent(Landroid/content/Context;Ljava/util/List;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 425
    .local p2, "newExerciseInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 428
    new-instance v10, Ljava/lang/StringBuilder;

    const-string v0, "("

    invoke-direct {v10, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 429
    .local v10, "sbVariableColumnNamesRangeSelection":Ljava/lang/StringBuilder;
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 431
    .local v4, "columnValues":[Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 432
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 433
    .local v9, "id":Ljava/lang/String;
    if-lez v8, :cond_0

    .line 435
    const-string v0, " OR "

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 438
    :cond_0
    const-string v0, "("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 440
    const-string v0, "_id"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 442
    const-string v0, "=?)"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    aput-object v9, v4, v8

    .line 431
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 446
    .end local v9    # "id":Ljava/lang/String;
    :cond_1
    const-string v0, ")"

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    const/4 v6, 0x0

    .line 452
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_INFO_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v2, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoColumnNamesProjection:[Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 454
    invoke-direct {p0, v6}, Lcom/cigna/mobile/coach/model/Exercise;->processExerciseInfoResultSet(Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    if-eqz v6, :cond_2

    .line 461
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 462
    const/4 v6, 0x0

    .line 467
    .end local v4    # "columnValues":[Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v8    # "i":I
    .end local v10    # "sbVariableColumnNamesRangeSelection":Ljava/lang/StringBuilder;
    :cond_2
    return-void

    .line 456
    .restart local v4    # "columnValues":[Ljava/lang/String;
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v8    # "i":I
    .restart local v10    # "sbVariableColumnNamesRangeSelection":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v7

    .line 457
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v0, v7}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 461
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 462
    const/4 v6, 0x0

    :cond_3
    throw v0
.end method

.method private appendExerciseInfo(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 323
    .local p2, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    .local p3, "exerciseInfoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 326
    .local v4, "lmet":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 329
    .local v7, "newExerciseInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    if-ge v1, v8, :cond_1

    .line 330
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 331
    .local v3, "key":Ljava/lang/String;
    sget-object v8, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoContent:Ljava/util/HashMap;

    invoke-virtual {v8, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 332
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 338
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 339
    invoke-direct {p0, p1, v7}, Lcom/cigna/mobile/coach/model/Exercise;->addExerciseInfoContent(Landroid/content/Context;Ljava/util/List;)V

    .line 344
    :cond_2
    if-eqz p2, :cond_4

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 345
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_4

    .line 346
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    .line 347
    .local v6, "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseInfoId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 348
    .local v5, "lmetKey":Ljava/lang/String;
    sget-object v8, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoContent:Ljava/util/HashMap;

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;

    .line 349
    .local v0, "eit":Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;
    if-eqz v0, :cond_3

    .line 350
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->getMetName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setMetName(Ljava/lang/String;)V

    .line 351
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->getMet()F

    move-result v8

    invoke-virtual {v6, v8}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setMetValue(F)V

    .line 353
    :cond_3
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 357
    .end local v0    # "eit":Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;
    .end local v2    # "j":I
    .end local v5    # "lmetKey":Ljava/lang/String;
    .end local v6    # "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    :cond_4
    return-object v4
.end method

.method private appendExerciseInfoBuilder(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 372
    .local p2, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    .local p3, "exerciseInfoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 375
    .local v5, "locallmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 378
    .local v7, "newExerciseInfo":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    if-ge v1, v8, :cond_1

    .line 379
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 380
    .local v3, "key":Ljava/lang/String;
    sget-object v8, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoContent:Ljava/util/HashMap;

    invoke-virtual {v8, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 381
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 387
    .end local v3    # "key":Ljava/lang/String;
    :cond_1
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 388
    invoke-direct {p0, p1, v7}, Lcom/cigna/mobile/coach/model/Exercise;->addExerciseInfoContent(Landroid/content/Context;Ljava/util/List;)V

    .line 393
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 394
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_4

    .line 395
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    .line 396
    .local v6, "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseInfoId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 397
    .local v4, "lmetKey":Ljava/lang/String;
    sget-object v8, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoContent:Ljava/util/HashMap;

    invoke-virtual {v8, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;

    .line 398
    .local v0, "eit":Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;
    if-eqz v0, :cond_3

    .line 399
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->getMetName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setMetName(Ljava/lang/String;)V

    .line 400
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->getMet()F

    move-result v8

    invoke-virtual {v6, v8}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setMetValue(F)V

    .line 402
    :cond_3
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 406
    .end local v0    # "eit":Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;
    .end local v2    # "j":I
    .end local v4    # "lmetKey":Ljava/lang/String;
    .end local v6    # "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    :cond_4
    return-object v5
.end method

.method private appendWalkInfo(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 519
    .local p2, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v0, "("

    invoke-direct {v12, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 520
    .local v12, "sbVariableColumnNamesRangeSelection":Ljava/lang/StringBuilder;
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 522
    .local v4, "columnValues":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 523
    .local v8, "i":I
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 524
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    .line 525
    .local v11, "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 526
    .local v9, "id":Ljava/lang/String;
    if-lez v8, :cond_0

    .line 528
    const-string v0, " OR "

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    :cond_0
    const-string v0, "("

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 533
    const-string v0, "exercise__id"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 535
    const-string v0, "=?)"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    aput-object v9, v4, v8

    .line 523
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 539
    .end local v9    # "id":Ljava/lang/String;
    .end local v11    # "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    :cond_1
    const-string v0, ")"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 551
    const/4 v6, 0x0

    .line 555
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WALK_INFO_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v2, Lcom/cigna/mobile/coach/model/Exercise;->walkInfoColumnNamesProjection:[Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "exercise__id ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 557
    invoke-direct {p0, v6, p2}, Lcom/cigna/mobile/coach/model/Exercise;->processWalkInfoResultSet(Landroid/database/Cursor;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 563
    .local v10, "lmet":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    if-eqz v6, :cond_2

    .line 564
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 565
    const/4 v6, 0x0

    .line 569
    :cond_2
    return-object v10

    .line 559
    .end local v10    # "lmet":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    :catch_0
    move-exception v7

    .line 560
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v0, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v0, v7}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 563
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 564
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 565
    const/4 v6, 0x0

    :cond_3
    throw v0
.end method

.method private getExercise(Landroid/content/Context;JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "startTimeInMillis"    # J
    .param p4, "endTimeInMillis"    # J
    .param p6, "exerciseType1"    # Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .param p7, "exerciseType2"    # Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 216
    const/4 v8, 0x0

    .line 223
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x2

    .line 225
    .local v10, "index":I
    :try_start_0
    sget-object v5, Lcom/cigna/mobile/coach/model/Exercise;->exerciseColumnNamesRangeSelection:Ljava/lang/String;

    .line 227
    .local v5, "variableColumnNamesRangeSelection":Ljava/lang/String;
    if-eqz p6, :cond_0

    .line 228
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND (exercise_type=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 229
    add-int/lit8 v10, v10, 0x1

    .line 231
    :cond_0
    if-eqz p7, :cond_1

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    if-eq v0, v1, :cond_1

    .line 232
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR exercise_type=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 233
    add-int/lit8 v10, v10, 0x1

    .line 235
    :cond_1
    if-eqz p6, :cond_2

    .line 236
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 239
    :cond_2
    new-array v6, v10, [Ljava/lang/String;

    .line 240
    .local v6, "columnValues":[Ljava/lang/String;
    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 241
    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 242
    const/4 v2, 0x4

    if-ne v10, v2, :cond_5

    .line 243
    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p6 .. p6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 244
    const/4 v2, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p7 .. p7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    .line 257
    :cond_3
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->exerciseColumnNamesProjection:[Ljava/lang/String;

    const-string/jumbo v7, "start_time ASC"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 259
    invoke-direct {p0, v8}, Lcom/cigna/mobile/coach/model/Exercise;->processExerciseResultSet(Landroid/database/Cursor;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 264
    .local v11, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    if-eqz v8, :cond_4

    .line 265
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 266
    const/4 v8, 0x0

    .line 270
    :cond_4
    return-object v11

    .line 246
    .end local v11    # "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    :cond_5
    const/4 v2, 0x3

    if-ne v10, v2, :cond_3

    .line 247
    if-eqz p6, :cond_7

    .line 248
    const/4 v2, 0x2

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p6 .. p6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 261
    .end local v5    # "variableColumnNamesRangeSelection":Ljava/lang/String;
    .end local v6    # "columnValues":[Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 262
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v2, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v2, v9}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 264
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v8, :cond_6

    .line 265
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 266
    const/4 v8, 0x0

    :cond_6
    throw v2

    .line 250
    .restart local v5    # "variableColumnNamesRangeSelection":Ljava/lang/String;
    .restart local v6    # "columnValues":[Ljava/lang/String;
    :cond_7
    const/4 v2, 0x2

    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p7 .. p7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private static getExerciseType(I)Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .locals 1
    .param p0, "exerciseType"    # I

    .prologue
    .line 141
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->ACTIVITY:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v0

    if-ne p0, v0, :cond_0

    .line 142
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->ACTIVITY:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 148
    :goto_0
    return-object v0

    .line 143
    :cond_0
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->FITNESS:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v0

    if-ne p0, v0, :cond_1

    .line 144
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->FITNESS:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    goto :goto_0

    .line 145
    :cond_1
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->PEDOMETER:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v0

    if-ne p0, v0, :cond_2

    .line 146
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->PEDOMETER:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    goto :goto_0

    .line 148
    :cond_2
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->NOT_DEFINED:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    goto :goto_0
.end method

.method private getUniqueExerciseInfoIds(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 285
    .local p1, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v2, "exerciseInfoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 289
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 292
    .local v3, "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 293
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseInfoId()I

    move-result v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 294
    .local v1, "eiiStr":Ljava/lang/String;
    invoke-virtual {v3, v1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 298
    .end local v1    # "eiiStr":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 299
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 300
    .local v5, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 301
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 305
    .end local v3    # "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "i":I
    .end local v5    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 306
    .local v0, "e":Ljava/lang/Exception;
    new-instance v6, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v6, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 308
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    return-object v2
.end method

.method private processExerciseInfoResultSet(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 720
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 721
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "processExerciseInfoResultSet: Start"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_0
    if-eqz p1, :cond_4

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 724
    const/4 v0, 0x1

    .line 725
    .local v0, "count":I
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 726
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "total Number of records found : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :cond_1
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 730
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "reading record: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 732
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_2
    new-instance v2, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;-><init>()V

    .line 733
    .local v2, "eit":Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;
    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 734
    .local v3, "exerciseInfoId":I
    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->setExerciseInfoId(I)V

    .line 735
    const-string/jumbo v4, "met"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->setMet(F)V

    .line 736
    const-string/jumbo v4, "name"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->setMetName(Ljava/lang/String;)V

    .line 738
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->exerciseInfoContent:Ljava/util/HashMap;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 742
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 743
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/Log;->printRowInCursor(Ljava/lang/String;Landroid/database/Cursor;)V

    .line 746
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 748
    .end local v0    # "count":I
    .end local v2    # "eit":Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;
    .end local v3    # "exerciseInfoId":I
    :cond_4
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 749
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "processExerciseInfoResultSet: End"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    :cond_5
    return-void
.end method

.method private processExerciseResultSet(Landroid/database/Cursor;)Ljava/util/List;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 669
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 670
    .local v3, "listExerciseInfo":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 671
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "processExerciseResultSet: Start"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    :cond_0
    if-eqz p1, :cond_6

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 674
    const/4 v0, 0x1

    .line 675
    .local v0, "count":I
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 676
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " total Number of records found : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    :cond_1
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 680
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "reading record: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "count":I
    .local v1, "count":I
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 682
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_2
    new-instance v2, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;-><init>()V

    .line 683
    .local v2, "exerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setExerciseId(J)V

    .line 684
    const-string v4, "exercise_info__id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setExerciseInfoId(I)V

    .line 685
    const-string v4, "exercise_type"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/cigna/mobile/coach/model/Exercise;->getExerciseType(I)Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setExerciseType(Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)V

    .line 686
    const-string/jumbo v4, "start_time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setDateInMsec(J)V

    .line 687
    const-string v4, "duration_millisecond"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-float v4, v4

    const v5, 0x476a6000    # 60000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setDurationMin(I)V

    .line 689
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 693
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 694
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/Log;->printRowInCursor(Ljava/lang/String;Landroid/database/Cursor;)V

    .line 697
    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 703
    .end local v0    # "count":I
    .end local v2    # "exerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    :cond_4
    :goto_0
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 704
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "processExerciseResultSet: End"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :cond_5
    return-object v3

    .line 699
    :cond_6
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 700
    sget-object v4, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    const-string v5, " total Number of records found : 0"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processWalkInfoResultSet(Landroid/database/Cursor;Ljava/util/List;)Ljava/util/List;
    .locals 21
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 583
    .local p2, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 587
    .local v15, "lmet":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 589
    .local v10, "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;>;"
    const-wide/16 v13, -0x1

    .line 591
    .local v13, "lastId":J
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 594
    .local v8, "currentList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    if-eqz p1, :cond_4

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 595
    const/4 v4, 0x1

    .line 596
    .local v4, "count":I
    sget-object v18, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 597
    sget-object v18, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " total Number of records found : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->getCount()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    :cond_0
    sget-object v18, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 601
    sget-object v18, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v20, "reading record: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "count":I
    .local v5, "count":I
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 605
    .end local v5    # "count":I
    .restart local v4    # "count":I
    :cond_1
    const-string v18, "exercise__id"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 606
    .local v6, "currentId":J
    new-instance v17, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    invoke-direct/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;-><init>()V

    .line 608
    .local v17, "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    cmp-long v18, v6, v13

    if-eqz v18, :cond_2

    .line 609
    move-wide v13, v6

    .line 610
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "currentList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 611
    .restart local v8    # "currentList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v10, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    :cond_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setExerciseId(J)V

    .line 615
    const-string/jumbo v18, "user_device__id"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setUserDeviceId(Ljava/lang/String;)V

    .line 616
    const-string v18, "distance"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setDistance(F)V

    .line 617
    const-string/jumbo v18, "speed"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setSpeed(F)V

    .line 618
    const-string/jumbo v18, "total_step"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setTotalSteps(I)V

    .line 619
    const-string/jumbo v18, "run_step"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setRunningSteps(I)V

    .line 620
    const-string/jumbo v18, "walk_step"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setWalkingSteps(I)V

    .line 621
    const-string/jumbo v18, "updown_step"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setUpdownSteps(I)V

    .line 623
    move-object/from16 v0, v17

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 627
    sget-object v18, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 628
    sget-object v18, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->printRowInCursor(Ljava/lang/String;Landroid/database/Cursor;)V

    .line 631
    :cond_3
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v18

    if-nez v18, :cond_0

    .line 635
    .end local v4    # "count":I
    .end local v6    # "currentId":J
    .end local v17    # "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    :cond_4
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v11, v0, :cond_6

    .line 636
    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    .line 637
    .restart local v17    # "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseId()J

    move-result-wide v19

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 638
    .local v9, "exerciseId":Ljava/lang/String;
    invoke-virtual {v10, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/List;

    .line 639
    .local v16, "locallmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    if-eqz v16, :cond_5

    .line 641
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v12, v0, :cond_5

    .line 642
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->clone()Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    move-result-object v3

    .line 643
    .local v3, "copymetb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    move-object/from16 v0, v16

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    .line 644
    .local v2, "addmetb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getUserDeviceId()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setUserDeviceId(Ljava/lang/String;)V

    .line 645
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getDistance()F

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setDistance(F)V

    .line 646
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getSpeed()F

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setSpeed(F)V

    .line 647
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getTotalSteps()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setTotalSteps(I)V

    .line 649
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getRunningSteps()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setRunningSteps(I)V

    .line 650
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getWalkingSteps()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setWalkingSteps(I)V

    .line 651
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getUpdownSteps()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setUpdownSteps(I)V

    .line 652
    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 641
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 635
    .end local v2    # "addmetb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    .end local v3    # "copymetb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    .end local v12    # "j":I
    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 657
    .end local v9    # "exerciseId":Ljava/lang/String;
    .end local v16    # "locallmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    .end local v17    # "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    :cond_6
    return-object v15
.end method


# virtual methods
.method public getExerciseData(Landroid/content/Context;JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "startTimeInMillis"    # J
    .param p4, "endTimeInMillis"    # J
    .param p6, "exerciseType1"    # Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .param p7, "exerciseType2"    # Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 177
    sget-object v3, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 178
    sget-object v3, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Fetching Exercise Data Info from Tracker DB"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    :cond_0
    invoke-direct/range {p0 .. p7}, Lcom/cigna/mobile/coach/model/Exercise;->getExercise(Landroid/content/Context;JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;

    move-result-object v2

    .line 194
    .local v2, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    invoke-direct {p0, v2}, Lcom/cigna/mobile/coach/model/Exercise;->getUniqueExerciseInfoIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 197
    .local v0, "exerciseInfoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, v2, v0}, Lcom/cigna/mobile/coach/model/Exercise;->appendExerciseInfo(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 199
    .local v1, "lmet":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    return-object v1
.end method

.method public getPedoData(Landroid/content/Context;JJ)Ljava/util/List;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "startTimeInMillis"    # J
    .param p4, "endTimeInMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 472
    sget-object v0, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    sget-object v0, Lcom/cigna/mobile/coach/model/Exercise;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Fetching Pedometer Data Info from Tracker DB"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_0
    sget-object v6, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->ACTIVITY:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 479
    .local v6, "exerciseType1":Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    sget-object v7, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->PEDOMETER:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .local v7, "exerciseType2":Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    .line 488
    invoke-direct/range {v0 .. v7}, Lcom/cigna/mobile/coach/model/Exercise;->getExercise(Landroid/content/Context;JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;

    move-result-object v10

    .line 491
    .local v10, "lmetb":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;>;"
    invoke-direct {p0, v10}, Lcom/cigna/mobile/coach/model/Exercise;->getUniqueExerciseInfoIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 495
    .local v8, "exerciseInfoIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p1, v10, v8}, Lcom/cigna/mobile/coach/model/Exercise;->appendExerciseInfoBuilder(Landroid/content/Context;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    .line 498
    invoke-direct {p0, p1, v10}, Lcom/cigna/mobile/coach/model/Exercise;->appendWalkInfo(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 500
    .local v9, "lmet":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    return-object v9
.end method
