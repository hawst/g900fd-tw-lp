.class public Lcom/cigna/mobile/coach/ShealthContentObserver;
.super Landroid/database/ContentObserver;
.source "ShealthContentObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ShealthContentObserver"

.field static final WAIT_MIN_BEFORE_PROCESS_TRACKER:I = 0xea60

.field static final WAIT_MIN_BEFORE_QUEUED_PROCESS_TRACKER:I = 0x1388

.field private static count:I

.field private static isTimerScheduled:Ljava/lang/Boolean;

.field private static uriMap:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private context:Landroid/content/Context;

.field private final lockObject:Ljava/lang/Object;

.field private timerHandler:Landroid/os/Handler;

.field timerRunnable:Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/cigna/mobile/coach/ShealthContentObserver;->uriMap:Ljava/util/Hashtable;

    .line 47
    const/4 v0, 0x1

    sput v0, Lcom/cigna/mobile/coach/ShealthContentObserver;->count:I

    .line 48
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/ShealthContentObserver;->isTimerScheduled:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 52
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->timerHandler:Landroid/os/Handler;

    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->lockObject:Ljava/lang/Object;

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->timerRunnable:Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;

    .line 53
    iput-object p1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->context:Landroid/content/Context;

    .line 54
    return-void
.end method

.method static synthetic access$000()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/cigna/mobile/coach/ShealthContentObserver;->uriMap:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Boolean;

    .prologue
    .line 37
    sput-object p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->isTimerScheduled:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$200(Lcom/cigna/mobile/coach/ShealthContentObserver;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/mobile/coach/ShealthContentObserver;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/cigna/mobile/coach/ShealthContentObserver;Landroid/content/Context;J)Z
    .locals 1
    .param p0, "x0"    # Lcom/cigna/mobile/coach/ShealthContentObserver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # J

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/cigna/mobile/coach/ShealthContentObserver;->scheduleTimer(Landroid/content/Context;J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/cigna/mobile/coach/ShealthContentObserver;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/mobile/coach/ShealthContentObserver;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->lockObject:Ljava/lang/Object;

    return-object v0
.end method

.method private declared-synchronized scheduleTimer(Landroid/content/Context;J)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "delay"    # J

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->timerRunnable:Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;

    if-nez v1, :cond_0

    .line 93
    new-instance v1, Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;

    invoke-direct {v1, p0}, Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;-><init>(Lcom/cigna/mobile/coach/ShealthContentObserver;)V

    iput-object v1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->timerRunnable:Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->timerRunnable:Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;

    if-eqz v1, :cond_2

    .line 96
    iget-object v1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->timerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->timerRunnable:Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    .line 97
    .local v0, "bTimerStarted":Z
    const-string v1, "ShealthContentObserver"

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 98
    const-string v1, "ShealthContentObserver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>>>>>>>>>>>>>>>> Timer started="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "timer scheduled for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    .end local v0    # "bTimerStarted":Z
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 102
    :cond_2
    :try_start_1
    const-string v1, "ShealthContentObserver"

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    const-string v1, "ShealthContentObserver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Timer has not yet been created, cannot start timer: timerCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/cigna/mobile/coach/ShealthContentObserver;->count:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 87
    const-string v0, "ShealthContentObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoachListener: ContentObserver("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") has been notified of a change."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 89
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 61
    const-string v1, "ShealthContentObserver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CoachListener: ContentObserver("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") has been notified of a change: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 64
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/mobile/coach/DataProcessor;->getTrackerEventType(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-result-object v0

    .line 67
    .local v0, "trackerEventType":Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    if-ne v0, v1, :cond_1

    .line 68
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/ShealthContentObserver;->processCallTrackerChangeMethod(Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)V

    .line 80
    :goto_0
    const-string v1, "ShealthContentObserver"

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const-string v1, "ShealthContentObserver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "onChange :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_0
    return-void

    .line 70
    :cond_1
    iget-object v2, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->lockObject:Ljava/lang/Object;

    monitor-enter v2

    .line 71
    :try_start_0
    sget-object v1, Lcom/cigna/mobile/coach/ShealthContentObserver;->isTimerScheduled:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 72
    sget v1, Lcom/cigna/mobile/coach/ShealthContentObserver;->count:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/cigna/mobile/coach/ShealthContentObserver;->count:I

    .line 73
    iget-object v1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->context:Landroid/content/Context;

    const-wide/32 v3, 0xea60

    invoke-direct {p0, v1, v3, v4}, Lcom/cigna/mobile/coach/ShealthContentObserver;->scheduleTimer(Landroid/content/Context;J)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/cigna/mobile/coach/ShealthContentObserver;->isTimerScheduled:Ljava/lang/Boolean;

    .line 74
    invoke-virtual {p0, v0}, Lcom/cigna/mobile/coach/ShealthContentObserver;->processCallTrackerChangeMethod(Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)V

    .line 78
    :goto_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 76
    :cond_2
    :try_start_1
    sget-object v1, Lcom/cigna/mobile/coach/ShealthContentObserver;->uriMap:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method processCallTrackerChangeMethod(Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)V
    .locals 3
    .param p1, "trackerEventType"    # Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .prologue
    .line 139
    const-string v1, "ShealthContentObserver"

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 140
    const-string v1, "ShealthContentObserver"

    const-string v2, "ProcessTrackerTimerTask is running."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    new-instance v0, Lcom/cigna/mobile/coach/DataProcessor;

    iget-object v1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/cigna/mobile/coach/DataProcessor;-><init>(Landroid/content/Context;)V

    .line 143
    .local v0, "dataProcessor":Lcom/cigna/mobile/coach/DataProcessor;
    invoke-virtual {v0, p1}, Lcom/cigna/mobile/coach/DataProcessor;->updateCoach(Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)V

    .line 144
    const-string v1, "ShealthContentObserver"

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    const-string v1, "ShealthContentObserver"

    const-string v2, "ProcessTrackerTimerTask is ended."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_1
    return-void
.end method
