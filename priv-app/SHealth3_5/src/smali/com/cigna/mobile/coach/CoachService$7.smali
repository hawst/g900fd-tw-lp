.class Lcom/cigna/mobile/coach/CoachService$7;
.super Ljava/lang/Object;
.source "CoachService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/mobile/coach/CoachService;->performEraseData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/mobile/coach/CoachService;


# direct methods
.method constructor <init>(Lcom/cigna/mobile/coach/CoachService;)V
    .locals 0

    .prologue
    .line 669
    iput-object p1, p0, Lcom/cigna/mobile/coach/CoachService$7;->this$0:Lcom/cigna/mobile/coach/CoachService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 673
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 674
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "performEraseData start time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    iget-object v1, p0, Lcom/cigna/mobile/coach/CoachService$7;->this$0:Lcom/cigna/mobile/coach/CoachService;

    invoke-virtual {v1}, Lcom/cigna/mobile/coach/CoachService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;-><init>(Landroid/content/Context;)V

    .line 678
    .local v0, "helper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->nullActivityListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->erase(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    .line 679
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 680
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "performEraseData end time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    :cond_1
    return-void
.end method
