.class Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;
.super Ljava/lang/Object;
.source "ShealthContentObserver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/mobile/coach/ShealthContentObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TimerRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/mobile/coach/ShealthContentObserver;


# direct methods
.method constructor <init>(Lcom/cigna/mobile/coach/ShealthContentObserver;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;->this$0:Lcom/cigna/mobile/coach/ShealthContentObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 115
    const-string v2, "ShealthContentObserver"

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    const-string v2, "ShealthContentObserver"

    const-string v3, "Timer is Running : START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    # getter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->uriMap:Ljava/util/Hashtable;
    invoke-static {}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$000()Ljava/util/Hashtable;

    move-result-object v2

    if-eqz v2, :cond_3

    # getter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->uriMap:Ljava/util/Hashtable;
    invoke-static {}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$000()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Hashtable;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 119
    # getter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->uriMap:Ljava/util/Hashtable;
    invoke-static {}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$000()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Hashtable;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 120
    .local v1, "trackerEventTypeKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 121
    .local v0, "eventType":Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    const-string v2, "ShealthContentObserver"

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 122
    const-string v2, "ShealthContentObserver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TrackerEventType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_1
    iget-object v2, p0, Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;->this$0:Lcom/cigna/mobile/coach/ShealthContentObserver;

    invoke-virtual {v2, v0}, Lcom/cigna/mobile/coach/ShealthContentObserver;->processCallTrackerChangeMethod(Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)V

    .line 125
    # getter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->uriMap:Ljava/util/Hashtable;
    invoke-static {}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$000()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v2, p0, Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;->this$0:Lcom/cigna/mobile/coach/ShealthContentObserver;

    iget-object v3, p0, Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;->this$0:Lcom/cigna/mobile/coach/ShealthContentObserver;

    # getter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->context:Landroid/content/Context;
    invoke-static {v3}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$200(Lcom/cigna/mobile/coach/ShealthContentObserver;)Landroid/content/Context;

    move-result-object v3

    const-wide/16 v4, 0x1388

    # invokes: Lcom/cigna/mobile/coach/ShealthContentObserver;->scheduleTimer(Landroid/content/Context;J)Z
    invoke-static {v2, v3, v4, v5}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$300(Lcom/cigna/mobile/coach/ShealthContentObserver;Landroid/content/Context;J)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # setter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->isTimerScheduled:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$102(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 132
    .end local v0    # "eventType":Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    .end local v1    # "trackerEventTypeKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;>;"
    :goto_0
    const-string v2, "ShealthContentObserver"

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 133
    const-string v2, "ShealthContentObserver"

    const-string v3, "Timer is Running : END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_2
    return-void

    .line 128
    :cond_3
    iget-object v2, p0, Lcom/cigna/mobile/coach/ShealthContentObserver$TimerRunnable;->this$0:Lcom/cigna/mobile/coach/ShealthContentObserver;

    # getter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->lockObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$400(Lcom/cigna/mobile/coach/ShealthContentObserver;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 129
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    # setter for: Lcom/cigna/mobile/coach/ShealthContentObserver;->isTimerScheduled:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/cigna/mobile/coach/ShealthContentObserver;->access$102(Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 130
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
