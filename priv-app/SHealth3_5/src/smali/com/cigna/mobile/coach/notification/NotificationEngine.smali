.class public Lcom/cigna/mobile/coach/notification/NotificationEngine;
.super Ljava/lang/Object;
.source "NotificationEngine.java"


# static fields
.field private static final ADD_TIMELINE_URI:Ljava/lang/String; = "com.sec.android.app.shealthADD_TIMELINE"

.field private static final CLASS_NAME:Ljava/lang/String;

.field public static final CONTENT:Ljava/lang/String; = "content"

.field public static final CONTENT_COUNTRY:Ljava/lang/String; = "country"

.field public static final CONTENT_TEXT:Ljava/lang/String; = "text"

.field public static final CONTENT_TEXT_VALUE:Ljava/lang/String; = "value"

.field public static final CONTENT_TITLE:Ljava/lang/String; = "title"

.field public static final FEATURE:Ljava/lang/String; = "feature"

.field public static final IMAGE:Ljava/lang/String; = "image"

.field private static final TIMELINE_INFORMATION_EXTRA:Ljava/lang/String; = "TIMELINE_INFORMATION"

.field private static final TIMELINE_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.shealth"

.field public static final UI_TYPE:Ljava/lang/String; = "ui_type"

.field private static context:Landroid/content/Context;

.field private static instance:Lcom/cigna/mobile/coach/notification/NotificationEngine;

.field private static notificationManager:Landroid/app/NotificationManager;

.field private static notificationTrayId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    const-class v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    sput v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->notificationTrayId:I

    .line 64
    sput-object v1, Lcom/cigna/mobile/coach/notification/NotificationEngine;->instance:Lcom/cigna/mobile/coach/notification/NotificationEngine;

    .line 66
    sput-object v1, Lcom/cigna/mobile/coach/notification/NotificationEngine;->context:Landroid/content/Context;

    .line 67
    sput-object v1, Lcom/cigna/mobile/coach/notification/NotificationEngine;->notificationManager:Landroid/app/NotificationManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    return-void
.end method

.method public static createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;
    .locals 8
    .param p0, "intentAction"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 93
    .local v0, "activityIntent":Landroid/content/Intent;
    if-eqz p1, :cond_2

    .line 94
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 95
    .local v1, "deepLinkKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 96
    .local v3, "key":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "INTENT_ACTION"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 97
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    .line 98
    .local v4, "value":Ljava/lang/Object;
    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 100
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v6, v4

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    sget-object v5, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 102
    sget-object v5, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "String value:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_1
    instance-of v5, v4, Ljava/lang/Integer;

    if-eqz v5, :cond_0

    .line 107
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object v6, v4

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 108
    sget-object v5, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 109
    sget-object v5, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Integer value:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " key: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 116
    .end local v1    # "deepLinkKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "key":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4    # "value":Ljava/lang/Object;
    :cond_2
    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/NotificationEngine;
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 81
    sget-object v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->instance:Lcom/cigna/mobile/coach/notification/NotificationEngine;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;

    invoke-direct {v0}, Lcom/cigna/mobile/coach/notification/NotificationEngine;-><init>()V

    sput-object v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->instance:Lcom/cigna/mobile/coach/notification/NotificationEngine;

    .line 83
    sput-object p0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->context:Landroid/content/Context;

    .line 84
    sget-object v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->context:Landroid/content/Context;

    const-string/jumbo v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    sput-object v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->notificationManager:Landroid/app/NotificationManager;

    .line 87
    :cond_0
    sget-object v0, Lcom/cigna/mobile/coach/notification/NotificationEngine;->instance:Lcom/cigna/mobile/coach/notification/NotificationEngine;

    return-object v0
.end method

.method private updateAndroidNotificationTray(Lcom/cigna/coach/apiobjects/Notification;Z)Z
    .locals 12
    .param p1, "notification"    # Lcom/cigna/coach/apiobjects/Notification;
    .param p2, "vibrate"    # Z

    .prologue
    const/4 v11, 0x1

    .line 202
    const/4 v0, 0x0

    .line 203
    .local v0, "activityIntent":Landroid/content/Intent;
    const/4 v6, 0x0

    .line 204
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    const/4 v4, 0x0

    .line 206
    .local v4, "intentAction":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Notification;->getDeepLinkInfo()Ljava/util/Map;

    move-result-object v2

    .line 207
    .local v2, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v2, :cond_0

    .line 208
    const-string v8, "EXTRA_NAME_DESTINATION"

    invoke-interface {v2, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "intentAction":Ljava/lang/String;
    check-cast v4, Ljava/lang/String;

    .line 209
    .restart local v4    # "intentAction":Ljava/lang/String;
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 210
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Launch Intent Action= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_0
    if-eqz v2, :cond_1

    if-eqz v4, :cond_1

    :try_start_0
    const-string v8, ""

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 223
    const-string v8, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-static {v8, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_1

    .line 227
    const-string/jumbo v8, "widgetActivityAction"

    const-string v9, "com.sec.shealth.action.COACH"

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 228
    const-string/jumbo v8, "widgetActivityPackage"

    const-string v9, "com.sec.android.app.shealth"

    invoke-virtual {v0, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->context:Landroid/content/Context;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v0, v10}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 237
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Notification;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 238
    .local v7, "tickerText":Ljava/lang/String;
    new-instance v1, Landroid/app/Notification$Builder;

    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->context:Landroid/content/Context;

    invoke-direct {v1, v8}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 239
    .local v1, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v8

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v8

    sget v9, Lcom/cigna/coach/R$drawable;->s_health_indicator_icon:I

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    sget-object v9, Lcom/cigna/mobile/coach/notification/NotificationEngine;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/cigna/coach/R$drawable;->s_health_quickpanel_icon_coach:I

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Notification;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/Notification;->getMessageLine1()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 248
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    .line 249
    .local v5, "n":Landroid/app/Notification;
    if-eqz v6, :cond_2

    .line 250
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 251
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Launch Intent = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Landroid/app/PendingIntent;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "notification = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Landroid/app/Notification;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_2
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->notificationManager:Landroid/app/NotificationManager;

    sget v9, Lcom/cigna/mobile/coach/notification/NotificationEngine;->notificationTrayId:I

    add-int/lit8 v10, v9, 0x1

    sput v10, Lcom/cigna/mobile/coach/notification/NotificationEngine;->notificationTrayId:I

    invoke-virtual {v8, v9, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 260
    return v11

    .line 232
    .end local v1    # "builder":Landroid/app/Notification$Builder;
    .end local v5    # "n":Landroid/app/Notification;
    .end local v7    # "tickerText":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 233
    .local v3, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/cigna/mobile/coach/notification/NotificationEngine;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "exception occured in getting the intent :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private updateSHealthTimeline(Lcom/cigna/coach/apiobjects/Notification;)Z
    .locals 1
    .param p1, "notification"    # Lcom/cigna/coach/apiobjects/Notification;

    .prologue
    .line 327
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public sendNotifications(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    const/4 v3, 0x1

    .line 286
    const/4 v0, 0x0

    .line 287
    .local v0, "failure":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 288
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/Notification;

    .line 290
    .local v2, "notification":Lcom/cigna/coach/apiobjects/Notification;
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/Notification;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->ANDROID_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    if-eq v4, v5, :cond_0

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/Notification;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->BOTH_ANDROID_AND_TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    if-ne v4, v5, :cond_1

    .line 293
    :cond_0
    invoke-direct {p0, v2, v3}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->updateAndroidNotificationTray(Lcom/cigna/coach/apiobjects/Notification;Z)Z

    move-result v4

    if-nez v4, :cond_1

    .line 294
    add-int/lit8 v0, v0, 0x1

    .line 296
    :cond_1
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/Notification;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    if-eq v4, v5, :cond_2

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/Notification;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->BOTH_ANDROID_AND_TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    if-ne v4, v5, :cond_3

    .line 299
    :cond_2
    invoke-direct {p0, v2}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->updateSHealthTimeline(Lcom/cigna/coach/apiobjects/Notification;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 300
    add-int/lit8 v0, v0, 0x1

    .line 287
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 304
    .end local v2    # "notification":Lcom/cigna/coach/apiobjects/Notification;
    :cond_4
    if-lez v0, :cond_5

    :goto_1
    return v3

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method
