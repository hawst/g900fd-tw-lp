.class public Lcom/cigna/mobile/coach/model/Food;
.super Ljava/lang/Object;
.source "Food.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final COLUMN_AMOUNT:Ljava/lang/String; = "amount"

.field private static final COLUMN_CALCUIM:Ljava/lang/String; = "calcium"

.field private static final COLUMN_CARBOHYDRATE:Ljava/lang/String; = "carbohydrate"

.field private static final COLUMN_CHOLESTEROL:Ljava/lang/String; = "cholesterol"

.field private static final COLUMN_DIETARY_FIBER:Ljava/lang/String; = "dietary_fiber"

.field private static final COLUMN_IRON:Ljava/lang/String; = "iron"

.field private static final COLUMN_KCAL:Ljava/lang/String; = "kcal"

.field private static final COLUMN_KCAL_IN_UNIT:Ljava/lang/String; = "kcal_in_unit"

.field private static final COLUMN_METRIC_SERVING_AMOUNT:Ljava/lang/String; = "metric_serving_amount"

.field private static final COLUMN_METRIC_SERVING_UNIT:Ljava/lang/String; = "metric_serving_unit"

.field private static final COLUMN_MONOSTATURED_FAT:Ljava/lang/String; = "monosaturated_fat"

.field private static final COLUMN_NAME:Ljava/lang/String; = "name"

.field private static final COLUMN_POLYSTATURATED_FAT:Ljava/lang/String; = "polysaturated_fat"

.field private static final COLUMN_POTASSIUM:Ljava/lang/String; = "potassium"

.field private static final COLUMN_PROTEIN:Ljava/lang/String; = "protein"

.field private static final COLUMN_SAMPLE_TIME:Ljava/lang/String; = "sample_time"

.field private static final COLUMN_SATURATED_FAT:Ljava/lang/String; = "saturated_fat"

.field private static final COLUMN_SERVER_ROOT_CATEGORY:Ljava/lang/String; = "server_root_category"

.field private static final COLUMN_SERVER_ROOT_CATEGORY_ID:Ljava/lang/String; = "server_root_category_id"

.field private static final COLUMN_SERVER_SOURCE_TYPE:Ljava/lang/String; = "server_source_type"

.field private static final COLUMN_SERVER_SUB_CATEGORY:Ljava/lang/String; = "server_sub_category"

.field private static final COLUMN_SODIUM:Ljava/lang/String; = "sodium"

.field private static final COLUMN_SUGAR:Ljava/lang/String; = "sugar"

.field private static final COLUMN_TOTAL_FAT:Ljava/lang/String; = "total_fat"

.field private static final COLUMN_TRANS_FAT:Ljava/lang/String; = "trans_fat"

.field private static final COLUMN_UNIT:Ljava/lang/String; = "unit"

.field private static final COLUMN_VITAMIN_A:Ljava/lang/String; = "vitamin_a"

.field private static final COLUMN_VITAMIN_C:Ljava/lang/String; = "vitamin_c"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/cigna/mobile/coach/model/Food;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/model/Food;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFoodData(Landroid/content/Context;JJ)Ljava/util/List;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startTimeInMillis"    # J
    .param p3, "endTimeInMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MealFoodTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 91
    const/4 v0, 0x0

    return-object v0
.end method

.method private static processResultSet(Landroid/database/Cursor;)Ljava/util/List;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MealFoodTracker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    const/4 v1, 0x0

    .line 202
    .local v1, "listFoodTracker":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MealFoodTracker;>;"
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "listFoodTracker":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MealFoodTracker;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 205
    .restart local v1    # "listFoodTracker":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MealFoodTracker;>;"
    :cond_0
    new-instance v0, Lcom/cigna/coach/dataobjects/MealFoodTracker;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/MealFoodTracker;-><init>()V

    .line 206
    .local v0, "foodTracker":Lcom/cigna/coach/dataobjects/MealFoodTracker;
    const-string v2, "carbohydrate"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setCarbohydrate(F)V

    .line 208
    const-string v2, "cholesterol"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setCholesterol(F)V

    .line 210
    const-string/jumbo v2, "server_root_category_id"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setFoodCategoryId(Ljava/lang/String;)V

    .line 212
    const-string/jumbo v2, "name"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setFoodName(Ljava/lang/String;)V

    .line 213
    const-string/jumbo v2, "server_sub_category"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setFoodSubCategory(Ljava/lang/String;)V

    .line 215
    const-string v2, "kcal"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setKilogramCalorie(F)V

    .line 216
    const-string/jumbo v2, "sample_time"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setMealDate(J)V

    .line 217
    const-string/jumbo v2, "metric_serving_amount"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setMetricServingAmount(I)V

    .line 219
    const-string/jumbo v2, "metric_serving_unit"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setMetricServingUnit(Ljava/lang/String;)V

    .line 221
    const-string v2, "amount"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setServingAmount(F)V

    .line 222
    const-string/jumbo v2, "sodium"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setSodium(F)V

    .line 223
    const-string/jumbo v2, "sugar"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setSugar(F)V

    .line 224
    const-string/jumbo v2, "total_fat"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setTotalFat(F)V

    .line 226
    const-string/jumbo v2, "unit"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setFoodUnit(Ljava/lang/String;)V

    .line 227
    const-string/jumbo v2, "server_root_category"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setFoodCategory(Ljava/lang/String;)V

    .line 229
    const-string/jumbo v2, "server_source_type"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setDatabaseId(I)V

    .line 231
    const-string/jumbo v2, "saturated_fat"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setSaturatedFat(F)V

    .line 233
    const-string/jumbo v2, "polysaturated_fat"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setPolysaturatedFat(F)V

    .line 235
    const-string/jumbo v2, "monosaturated_fat"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setMonosaturatedFat(F)V

    .line 237
    const-string/jumbo v2, "trans_fat"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setTransFat(F)V

    .line 238
    const-string v2, "dietary_fiber"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setDietaryFiber(F)V

    .line 240
    const-string v2, "kcal_in_unit"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setKilogramCalorieTotal(F)V

    .line 242
    const-string/jumbo v2, "protein"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setProtein(F)V

    .line 243
    const-string v2, "calcium"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setCalcium(F)V

    .line 244
    const-string/jumbo v2, "potassium"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setPotassium(F)V

    .line 245
    const-string v2, "iron"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setIron(F)V

    .line 246
    const-string/jumbo v2, "vitamin_a"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setVitaminA(F)V

    .line 247
    const-string/jumbo v2, "vitamin_c"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getFloat(I)F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/MealFoodTracker;->setVitaminC(F)V

    .line 249
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 253
    .end local v0    # "foodTracker":Lcom/cigna/coach/dataobjects/MealFoodTracker;
    :cond_1
    return-object v1
.end method
