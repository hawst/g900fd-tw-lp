.class public Lcom/cigna/mobile/coach/model/User;
.super Ljava/lang/Object;
.source "User.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final COLUMN_HEIGHT:Ljava/lang/String; = "height"

.field private static final COLUMN_SYNC_STATUS:Ljava/lang/String; = "sync_status"

.field private static final COLUMN_WEIGHT:Ljava/lang/String; = "weight"

.field private static final CREATE_TIME:Ljava/lang/String; = "create_time"

.field public static final DELETED:I = 0x29814

.field private static columnNameProjection:[Ljava/lang/String;

.field private static columnNamesWeightBeforeSelection:Ljava/lang/String;

.field private static columnNamesWeightProject:[Ljava/lang/String;

.field private static columnNamesWeightRangeSelection:Ljava/lang/String;

.field private static columnNamesWeightSelection:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    const-class v0, Lcom/cigna/mobile/coach/model/User;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    .line 67
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "height"

    aput-object v1, v0, v2

    const-string/jumbo v1, "weight"

    aput-object v1, v0, v3

    sput-object v0, Lcom/cigna/mobile/coach/model/User;->columnNameProjection:[Ljava/lang/String;

    .line 75
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "height"

    aput-object v1, v0, v2

    const-string/jumbo v1, "weight"

    aput-object v1, v0, v3

    const-string v1, "create_time"

    aput-object v1, v0, v4

    sput-object v0, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightProject:[Ljava/lang/String;

    .line 76
    const-string v0, "(sync_status != 170004)"

    sput-object v0, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightSelection:Ljava/lang/String;

    .line 77
    const-string v0, "(create_time<?) and (sync_status != 170004)"

    sput-object v0, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightBeforeSelection:Ljava/lang/String;

    .line 79
    const-string v0, "(create_time>=?) and (create_time<=?) and (sync_status != 170004)"

    sput-object v0, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightRangeSelection:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getUserData(Landroid/content/Context;)Lcom/cigna/coach/apiobjects/UserMetrics;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 94
    const/4 v6, 0x0

    .line 95
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 96
    .local v9, "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    const/high16 v8, -0x40800000    # -1.0f

    .line 97
    .local v8, "height":F
    const/high16 v11, -0x40800000    # -1.0f

    .line 101
    .local v11, "weight":F
    :try_start_0
    sget-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    sget-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Fetching Height/Weight from Tracker DB"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v2, Lcom/cigna/mobile/coach/model/User;->columnNameProjection:[Ljava/lang/String;

    sget-object v3, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightSelection:Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "create_time DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 111
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    const-string v0, "height"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v8

    .line 117
    const-string/jumbo v0, "weight"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v11

    .line 118
    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr v11, v0

    .line 120
    new-instance v10, Lcom/cigna/coach/apiobjects/UserMetrics;

    invoke-direct {v10}, Lcom/cigna/coach/apiobjects/UserMetrics;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    .end local v9    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    .local v10, "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    :try_start_1
    invoke-virtual {v10, v8}, Lcom/cigna/coach/apiobjects/UserMetrics;->setHeight(F)V

    .line 122
    invoke-virtual {v10, v11}, Lcom/cigna/coach/apiobjects/UserMetrics;->setWeight(F)V

    .line 123
    sget-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    sget-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUserData: from Weight tracker Weight:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "g; height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cm"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_1
    move-object v9, v10

    .line 132
    .end local v10    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    .restart local v9    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    :cond_2
    if-eqz v6, :cond_3

    .line 133
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_3
    sget-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 138
    sget-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "From Tracker DB: Height=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' cm., Weight=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' grams."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_4
    return-object v9

    .line 128
    :catch_0
    move-exception v7

    .line 129
    .local v7, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v0, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception querying Weight TrackerDB: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    new-instance v0, Lcom/cigna/coach/exceptions/CoachException;

    invoke-virtual {v7}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 132
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v6, :cond_5

    .line 133
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_5
    sget-object v1, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 138
    sget-object v1, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "From Tracker DB: Height=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' cm., Weight=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' grams."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v0

    .line 132
    .end local v9    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    .restart local v10    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    :catchall_1
    move-exception v0

    move-object v9, v10

    .end local v10    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    .restart local v9    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    goto :goto_1

    .line 128
    .end local v9    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    .restart local v10    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    :catch_1
    move-exception v7

    move-object v9, v10

    .end local v10    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    .restart local v9    # "um":Lcom/cigna/coach/apiobjects/UserMetrics;
    goto :goto_0
.end method

.method public static getWeightData(Landroid/content/Context;JJ)Ljava/util/List;
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startTimeInMillis"    # J
    .param p3, "endTimeInMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "JJ)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/WeightTracker;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 160
    const/4 v11, 0x0

    .line 161
    .local v11, "cursor":Landroid/database/Cursor;
    const/high16 v13, -0x40800000    # -1.0f

    .line 162
    .local v13, "height":F
    const/high16 v16, -0x40800000    # -1.0f

    .line 163
    .local v16, "weight":F
    const-wide/16 v9, -0x1

    .line 164
    .local v9, "createTime":J
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 165
    .local v19, "wtList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/WeightTracker;>;"
    const/16 v17, 0x0

    .line 166
    .local v17, "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    const/4 v15, 0x0

    .line 167
    .local v15, "lastWeight":F
    const/4 v14, 0x0

    .line 170
    .local v14, "lastHeight":F
    sget-object v3, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    sget-object v3, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Fetching Weight/Height Info from Tracker DB"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v5, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightProject:[Ljava/lang/String;

    sget-object v6, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightBeforeSelection:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v7, v8

    const-string v8, "create_time DESC"

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 183
    if-eqz v11, :cond_2

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 186
    const-string v3, "height"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v13

    .line 189
    const-string/jumbo v3, "weight"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v16

    .line 190
    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float v16, v16, v3

    .line 192
    const-string v3, "create_time"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 194
    cmpl-float v3, v16, v15

    if-nez v3, :cond_1

    cmpl-float v3, v13, v14

    if-eqz v3, :cond_2

    .line 195
    :cond_1
    new-instance v18, Lcom/cigna/coach/dataobjects/WeightTracker;

    invoke-direct/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/WeightTracker;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    .end local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .local v18, "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :try_start_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v10}, Lcom/cigna/coach/dataobjects/WeightTracker;->setDateInMsec(J)V

    .line 197
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/cigna/coach/dataobjects/WeightTracker;->setHeight(F)V

    .line 198
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/WeightTracker;->setWeight(F)V

    .line 199
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 200
    move/from16 v15, v16

    .line 201
    move v14, v13

    move-object/from16 v17, v18

    .line 208
    .end local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :cond_2
    if-eqz v11, :cond_3

    .line 209
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 217
    :cond_3
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_CP_URI:Landroid/net/Uri;

    sget-object v5, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightProject:[Ljava/lang/String;

    sget-object v6, Lcom/cigna/mobile/coach/model/User;->columnNamesWeightRangeSelection:Ljava/lang/String;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v7, v8

    const/4 v8, 0x1

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v7, v8

    const-string v8, "create_time ASC"

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 222
    if-eqz v11, :cond_6

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v3

    if-eqz v3, :cond_6

    :cond_4
    move-object/from16 v18, v17

    .line 226
    .end local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :try_start_3
    const-string v3, "height"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v13

    .line 229
    const-string/jumbo v3, "weight"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getFloat(I)F

    move-result v16

    .line 230
    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float v16, v16, v3

    .line 232
    const-string v3, "create_time"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 233
    sub-float v3, v16, v15

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v3, v3

    const-wide v5, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v3, v3, v5

    if-gtz v3, :cond_5

    sub-float v3, v13, v14

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-double v3, v3

    const-wide v5, 0x3f847ae147ae147bL    # 0.01

    cmpl-double v3, v3, v5

    if-lez v3, :cond_c

    .line 234
    :cond_5
    new-instance v17, Lcom/cigna/coach/dataobjects/WeightTracker;

    invoke-direct/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/WeightTracker;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 235
    .end local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :try_start_4
    move-object/from16 v0, v17

    invoke-virtual {v0, v9, v10}, Lcom/cigna/coach/dataobjects/WeightTracker;->setDateInMsec(J)V

    .line 236
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/cigna/coach/dataobjects/WeightTracker;->setHeight(F)V

    .line 237
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/WeightTracker;->setWeight(F)V

    .line 238
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    move/from16 v15, v16

    .line 240
    move v14, v13

    .line 243
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v3

    if-nez v3, :cond_4

    .line 249
    :cond_6
    if-eqz v11, :cond_7

    .line 250
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 254
    :cond_7
    sget-object v3, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 255
    sget-object v3, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "From Tracker DB: Latest Height=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' cm., Latest Weight=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' grams."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_8
    return-object v19

    .line 204
    :catch_0
    move-exception v12

    .line 205
    .local v12, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_5
    sget-object v3, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception querying Weight TrackerDB: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-virtual {v12}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 208
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    :goto_2
    if-eqz v11, :cond_9

    .line 209
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v3

    .line 245
    :catch_1
    move-exception v12

    .line 246
    .restart local v12    # "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    sget-object v3, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception querying TrackerDB: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-virtual {v12}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 249
    .end local v12    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v3

    :goto_4
    if-eqz v11, :cond_a

    .line 250
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 254
    :cond_a
    sget-object v4, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 255
    sget-object v4, Lcom/cigna/mobile/coach/model/User;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "From Tracker DB: Latest Height=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' cm., Latest Weight=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' grams."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    throw v3

    .line 249
    .end local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :catchall_2
    move-exception v3

    move-object/from16 v17, v18

    .end local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    goto :goto_4

    .line 245
    .end local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :catch_2
    move-exception v12

    move-object/from16 v17, v18

    .end local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    goto :goto_3

    .line 208
    .end local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :catchall_3
    move-exception v3

    move-object/from16 v17, v18

    .end local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    goto :goto_2

    .line 204
    .end local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :catch_3
    move-exception v12

    move-object/from16 v17, v18

    .end local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    goto/16 :goto_1

    .end local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    :cond_c
    move-object/from16 v17, v18

    .end local v18    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    .restart local v17    # "wt":Lcom/cigna/coach/dataobjects/WeightTracker;
    goto/16 :goto_0
.end method
