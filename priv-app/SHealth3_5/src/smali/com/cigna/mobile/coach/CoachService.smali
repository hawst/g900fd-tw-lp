.class public Lcom/cigna/mobile/coach/CoachService;
.super Landroid/app/Service;
.source "CoachService.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# static fields
.field private static final CHECK_DAILY:I = 0x0

.field private static final CHECK_QUEUED:I = 0x1

.field private static final CHECK_STARTUP:I = 0x2

.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final DETERMINE_CHECK:I = -0x1

.field private static final MILLISECONDS_IN_ONE_DAY:J = 0x5265c00L


# instance fields
.field private notificationType:I

.field private observer:Landroid/database/ContentObserver;

.field private resolver:Landroid/content/ContentResolver;

.field private timerHandler:Landroid/os/Handler;

.field private timerRunnable:Ljava/lang/Runnable;

.field private updateHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcom/cigna/mobile/coach/CoachService;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 85
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerHandler:Landroid/os/Handler;

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    .line 226
    new-instance v0, Lcom/cigna/mobile/coach/CoachService$2;

    invoke-direct {v0, p0}, Lcom/cigna/mobile/coach/CoachService$2;-><init>(Lcom/cigna/mobile/coach/CoachService;)V

    iput-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->updateHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/cigna/mobile/coach/CoachService;)I
    .locals 1
    .param p0, "x0"    # Lcom/cigna/mobile/coach/CoachService;

    .prologue
    .line 77
    iget v0, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    return v0
.end method

.method static synthetic access$102(Lcom/cigna/mobile/coach/CoachService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/cigna/mobile/coach/CoachService;
    .param p1, "x1"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    return p1
.end method

.method static synthetic access$200(Lcom/cigna/mobile/coach/CoachService;Landroid/content/Context;)J
    .locals 2
    .param p0, "x0"    # Lcom/cigna/mobile/coach/CoachService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/cigna/mobile/coach/CoachService;->getTimeDelayValue(Landroid/content/Context;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$300(Lcom/cigna/mobile/coach/CoachService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/mobile/coach/CoachService;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private createTimer(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 162
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Creating timer"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_0
    new-instance v0, Lcom/cigna/mobile/coach/CoachService$1;

    invoke-direct {v0, p0, p1}, Lcom/cigna/mobile/coach/CoachService$1;-><init>(Lcom/cigna/mobile/coach/CoachService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    .line 224
    return-void
.end method

.method private getMillisecondsAsString(J)Ljava/lang/String;
    .locals 10
    .param p1, "milliseconds"    # J
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DefaultLocale"
        }
    .end annotation

    .prologue
    const-wide/16 v8, 0x3c

    .line 531
    const-wide/16 v6, 0x3e8

    div-long v6, p1, v6

    rem-long v4, v6, v8

    .line 532
    .local v4, "second":J
    const-wide/32 v6, 0xea60

    div-long v6, p1, v6

    rem-long v2, v6, v8

    .line 533
    .local v2, "minute":J
    const-wide/32 v6, 0x36ee80

    div-long v6, p1, v6

    const-wide/16 v8, 0x18

    rem-long v0, v6, v8

    .line 535
    .local v0, "hour":J
    const-string v6, "%02dh %02dm %02d.%ds"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method private getTimeDelayValue(Landroid/content/Context;)J
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 470
    invoke-static {}, Lcom/cigna/mobile/coach/DataProcessor;->getMillisecondsSinceMidnight()J

    move-result-wide v5

    .line 471
    .local v5, "lNow":J
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 472
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Current time is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", midnight="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-wide/32 v10, 0x5265c00

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    :cond_0
    iget v8, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    .line 478
    const-wide/16 v0, 0x1

    .line 516
    .local v0, "delay":J
    :goto_0
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 517
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getTimeDelayValue()="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " ("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-direct {p0, v0, v1}, Lcom/cigna/mobile/coach/CoachService;->getMillisecondsAsString(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_1
    return-wide v0

    .line 480
    .end local v0    # "delay":J
    :cond_2
    sget-object v8, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->COACH_SERVICE:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-static {v8}, Lcom/cigna/coach/factory/InternalCoachFactory;->getInstance(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)Lcom/cigna/coach/factory/InternalCoachFactory;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/cigna/coach/factory/InternalCoachFactory;->getTracker(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ITracker;

    move-result-object v7

    .line 481
    .local v7, "tracker":Lcom/cigna/coach/interfaces/ITracker;
    const-wide/16 v3, -0x1

    .line 483
    .local v3, "lNotifyStartTime":J
    :try_start_0
    invoke-interface {v7}, Lcom/cigna/coach/interfaces/ITracker;->getNotificationStartTime()J
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    .line 491
    :goto_1
    const-wide/16 v8, 0x0

    cmp-long v8, v3, v8

    if-ltz v8, :cond_4

    const-wide/16 v8, 0x0

    cmp-long v8, v5, v8

    if-ltz v8, :cond_4

    cmp-long v8, v5, v3

    if-gez v8, :cond_4

    .line 494
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 495
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Current time is before notification start time. Wake up again at Notification Start Time="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", which is in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sub-long v10, v3, v5

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    :cond_3
    const/4 v8, 0x1

    iput v8, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    .line 498
    sub-long v0, v3, v5

    .restart local v0    # "delay":J
    goto/16 :goto_0

    .line 484
    .end local v0    # "delay":J
    :catch_0
    move-exception v2

    .line 485
    .local v2, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CoachException trying to get notification start time: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 499
    .end local v2    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_4
    const-wide/32 v8, 0x5265c00

    sub-long/2addr v8, v5

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_6

    .line 502
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 503
    sget-object v8, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Current time is after notification start time, so we next need to run at midnight which is in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-wide/32 v10, 0x5265c00

    sub-long/2addr v10, v5

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :cond_5
    const/4 v8, 0x0

    iput v8, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    .line 506
    const-wide/32 v8, 0x5265c00

    sub-long v0, v8, v5

    .restart local v0    # "delay":J
    goto/16 :goto_0

    .line 511
    .end local v0    # "delay":J
    :cond_6
    const/4 v8, 0x0

    iput v8, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    .line 512
    const-wide/32 v0, 0x5265c00

    .restart local v0    # "delay":J
    goto/16 :goto_0
.end method

.method private registerContentObservers()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 112
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v1, ">>>>>>> CoachListener: registerContentObservers()"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_0
    new-instance v0, Lcom/cigna/mobile/coach/ShealthContentObserver;

    iget-object v1, p0, Lcom/cigna/mobile/coach/CoachService;->updateHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/cigna/mobile/coach/ShealthContentObserver;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->observer:Landroid/database/ContentObserver;

    .line 117
    invoke-direct {p0}, Lcom/cigna/mobile/coach/CoachService;->verifyContentProviderURI()V

    .line 120
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_TABLE_CP_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService;->observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 121
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>>>>> CoachListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_EXERCISE_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WALK_INFO_TABLE_CP_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService;->observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 127
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 128
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>>>>> CoachListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WALK_INFO_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_CP_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService;->observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 133
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 134
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>>>>> CoachListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_WEIGHT_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_TABLE_CP_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService;->observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 139
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 140
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>>>>> CoachListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/mobile/coach/DataProcessor;->SHEALTH_SLEEP_TABLE_STRING_CP_URI:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_4
    return-void
.end method

.method private resetTimersAndCheckForInactivity()V
    .locals 3

    .prologue
    .line 357
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 358
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "User has changed the TIME or TIMEZONE on the device"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_0
    new-instance v0, Lcom/cigna/mobile/coach/DataProcessor;

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cigna/mobile/coach/DataProcessor;-><init>(Landroid/content/Context;)V

    .line 366
    .local v0, "processor":Lcom/cigna/mobile/coach/DataProcessor;
    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DATE_CHANGE:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    invoke-virtual {v0, v1}, Lcom/cigna/mobile/coach/DataProcessor;->checkForInactivity(Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;)V

    .line 371
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/mobile/coach/CoachService;->resetAllTimers(Landroid/content/Context;)Z

    .line 377
    return-void
.end method

.method private runCoachServiceActionsAfterBoot(I)V
    .locals 1
    .param p1, "notificationType"    # I

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->initializeCoachDB()V

    .line 340
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->resolver:Landroid/content/ContentResolver;

    .line 341
    invoke-direct {p0}, Lcom/cigna/mobile/coach/CoachService;->registerContentObservers()V

    .line 344
    iput p1, p0, Lcom/cigna/mobile/coach/CoachService;->notificationType:I

    .line 350
    invoke-direct {p0, p0}, Lcom/cigna/mobile/coach/CoachService;->createTimer(Landroid/content/Context;)V

    .line 351
    invoke-direct {p0, p0}, Lcom/cigna/mobile/coach/CoachService;->startTimer(Landroid/content/Context;)Z

    .line 352
    return-void
.end method

.method private startTimer(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 389
    iget-object v1, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_1

    .line 390
    iget-object v1, p0, Lcom/cigna/mobile/coach/CoachService;->timerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    invoke-direct {p0, p1}, Lcom/cigna/mobile/coach/CoachService;->getTimeDelayValue(Landroid/content/Context;)J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v0

    .line 392
    .local v0, "bTimerStarted":Z
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 393
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>>>>>>>>>>>>>>>> Timer started="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    .end local v0    # "bTimerStarted":Z
    :cond_0
    :goto_0
    return v0

    .line 397
    :cond_1
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 398
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Timer has not yet been created, cannot start timer"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private verifyContentProviderURI()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method


# virtual methods
.method public checkAndLoadLanguageContent(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 599
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cigna/mobile/coach/CoachService$4;

    invoke-direct {v1, p0, p1}, Lcom/cigna/mobile/coach/CoachService$4;-><init>(Lcom/cigna/mobile/coach/CoachService;Landroid/content/Context;)V

    const-string v2, "checkAndLoadLanguageContentThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 620
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 621
    return-void
.end method

.method public initializeCoachDB()V
    .locals 6

    .prologue
    .line 543
    sget-object v3, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 544
    sget-object v3, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Initialize CoachDB"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_0
    const/4 v0, 0x0

    .line 548
    .local v0, "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 554
    .end local v0    # "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v1, "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    if-eqz v1, :cond_4

    .line 555
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    move-object v0, v1

    .line 558
    .end local v1    # "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v0    # "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :cond_1
    :goto_0
    return-void

    .line 549
    :catch_0
    move-exception v2

    .line 550
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 551
    sget-object v3, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "error duting initializeCoachDB "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 554
    :cond_2
    if-eqz v0, :cond_1

    .line 555
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto :goto_0

    .line 554
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_3

    .line 555
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_3
    throw v3

    .end local v0    # "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v1    # "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :cond_4
    move-object v0, v1

    .end local v1    # "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v0    # "dbhelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_0
.end method

.method public loadLanguageContent()V
    .locals 3

    .prologue
    .line 565
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cigna/mobile/coach/CoachService$3;

    invoke-direct {v1, p0}, Lcom/cigna/mobile/coach/CoachService$3;-><init>(Lcom/cigna/mobile/coach/CoachService;)V

    const-string v2, "loadLanguageContentThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 591
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 592
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 449
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v1, ">>>>>>> CoachListener: CoachService.onBind()"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 381
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v1, ">>>>>>> CoachListener: CoachService.onCreate()"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/cigna/mobile/coach/CoachService;->runCoachServiceActionsAfterBoot(I)V

    .line 385
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 386
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 433
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v1, ">>>>>>> CoachListener: CoachService.onDestroy()"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 437
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->resolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->observer:Landroid/database/ContentObserver;

    if-eqz v0, :cond_2

    .line 438
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v1, ">>>>>>> CoachListener: Unregistering the Content Observers"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/cigna/mobile/coach/CoachService;->observer:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 443
    :cond_2
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 444
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 445
    :cond_3
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 247
    sput-boolean v3, Lcom/cigna/mobile/coach/CoachBroadcastReceiver;->isCoachServiceStarted:Z

    .line 248
    if-eqz p1, :cond_1

    .line 249
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 250
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 251
    const-string v1, "com.sec.shealth.action.SHEALTH_GET_BOOT_COMPLETED"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    invoke-direct {p0, v4}, Lcom/cigna/mobile/coach/CoachService;->runCoachServiceActionsAfterBoot(I)V

    .line 293
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V

    .line 299
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    return v3

    .line 255
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 257
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->loadLanguageContent()V

    goto :goto_0

    .line 259
    :cond_3
    const-string v1, "com.cigna.mobile.coach.COACH_STARTED"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 263
    invoke-direct {p0, v4}, Lcom/cigna/mobile/coach/CoachService;->runCoachServiceActionsAfterBoot(I)V

    goto :goto_0

    .line 265
    :cond_4
    const-string v1, "android.intent.action.TIME_SET"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 268
    :cond_5
    invoke-direct {p0}, Lcom/cigna/mobile/coach/CoachService;->resetTimersAndCheckForInactivity()V

    goto :goto_0

    .line 271
    :cond_6
    const-string v1, "action_backup_started"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 272
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 273
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Coach BACKUP requested."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    :cond_7
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->performBackup()V

    goto :goto_0

    .line 277
    :cond_8
    const-string v1, "action_restoration_started"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 278
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 279
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Coach RESTORE requested."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :cond_9
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->performRestore()V

    goto/16 :goto_0

    .line 283
    :cond_a
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 284
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 285
    sget-object v1, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Coach ACCOUNT_CHANGED requested."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_b
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->saveAccountChangeIndicator()V

    goto/16 :goto_0

    .line 289
    :cond_c
    const-string v1, "action_deletion_started"

    const-string v2, "ACTION_NAME"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 290
    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->performEraseData()V

    goto/16 :goto_0
.end method

.method public performBackup()V
    .locals 3

    .prologue
    .line 628
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cigna/mobile/coach/CoachService$5;

    invoke-direct {v1, p0}, Lcom/cigna/mobile/coach/CoachService$5;-><init>(Lcom/cigna/mobile/coach/CoachService;)V

    const-string/jumbo v2, "performBackup"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 643
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 644
    return-void
.end method

.method public performEraseData()V
    .locals 3

    .prologue
    .line 669
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cigna/mobile/coach/CoachService$7;

    invoke-direct {v1, p0}, Lcom/cigna/mobile/coach/CoachService$7;-><init>(Lcom/cigna/mobile/coach/CoachService;)V

    const-string/jumbo v2, "performEraseData"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 684
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 685
    return-void
.end method

.method public performRestore()V
    .locals 3

    .prologue
    .line 650
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/cigna/mobile/coach/CoachService$6;

    invoke-direct {v1, p0}, Lcom/cigna/mobile/coach/CoachService$6;-><init>(Lcom/cigna/mobile/coach/CoachService;)V

    const-string/jumbo v2, "performRestore"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 665
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 666
    return-void
.end method

.method public resetAllTimers(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 419
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 420
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    sget-object v0, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Removing any running timers"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/cigna/mobile/coach/CoachService;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cigna/mobile/coach/CoachService;->timerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 424
    invoke-direct {p0, p1}, Lcom/cigna/mobile/coach/CoachService;->startTimer(Landroid/content/Context;)Z

    move-result v0

    .line 427
    :goto_0
    return v0

    .line 426
    :cond_1
    invoke-direct {p0, p1}, Lcom/cigna/mobile/coach/CoachService;->createTimer(Landroid/content/Context;)V

    .line 427
    invoke-direct {p0, p1}, Lcom/cigna/mobile/coach/CoachService;->startTimer(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public saveAccountChangeIndicator()V
    .locals 9

    .prologue
    .line 307
    const/4 v0, 0x0

    .line 309
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {p0}, Lcom/cigna/mobile/coach/CoachService;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct {v1, v6, v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 310
    .end local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v1, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v4, Lcom/cigna/coach/db/UserMetricsDBManager;

    invoke-direct {v4, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 311
    .local v4, "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    new-instance v5, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v5, v1}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 312
    .local v5, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual {v5}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v3

    .line 314
    .local v3, "userIdTxt":Ljava/lang/String;
    sget-object v6, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    invoke-virtual {v4, v3, v6}, Lcom/cigna/coach/db/UserMetricsDBManager;->setBackupType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;)V

    .line 316
    if-eqz v1, :cond_0

    .line 318
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 325
    :cond_0
    if-eqz v1, :cond_4

    .line 326
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    move-object v0, v1

    .line 329
    .end local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v3    # "userIdTxt":Ljava/lang/String;
    .end local v4    # "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    .end local v5    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    .restart local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :cond_1
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v2

    .line 321
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v6, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 322
    sget-object v6, Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "error duRing  saveAccountChangeIndicator"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 325
    :cond_2
    if-eqz v0, :cond_1

    .line 326
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto :goto_0

    .line 325
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v0, :cond_3

    .line 326
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_3
    throw v6

    .line 325
    .end local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_2

    .line 320
    .end local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_1

    .end local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v3    # "userIdTxt":Ljava/lang/String;
    .restart local v4    # "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    .restart local v5    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :cond_4
    move-object v0, v1

    .end local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_0
.end method
