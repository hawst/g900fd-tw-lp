.class Lcom/cigna/mobile/coach/CoachService$2;
.super Landroid/os/Handler;
.source "CoachService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/mobile/coach/CoachService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/mobile/coach/CoachService;


# direct methods
.method constructor <init>(Lcom/cigna/mobile/coach/CoachService;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lcom/cigna/mobile/coach/CoachService$2;->this$0:Lcom/cigna/mobile/coach/CoachService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 229
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    # getter for: Lcom/cigna/mobile/coach/CoachService;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/mobile/coach/CoachService;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>>>>> CoachListener: updateHandler.handleMessage(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 233
    return-void
.end method
