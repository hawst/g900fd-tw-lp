.class public Lcom/cigna/coach/settings/CoachConfig;
.super Ljava/lang/Object;
.source "CoachConfig.java"


# static fields
.field public static final DB_VERSION:I = 0x4

.field public static final codeDropVersionID:I = 0x71

.field private static config_:Lcom/cigna/coach/settings/CoachConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getConfig()Lcom/cigna/coach/settings/CoachConfig;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/cigna/coach/settings/CoachConfig;->config_:Lcom/cigna/coach/settings/CoachConfig;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/cigna/coach/settings/CoachConfig;

    invoke-direct {v0}, Lcom/cigna/coach/settings/CoachConfig;-><init>()V

    sput-object v0, Lcom/cigna/coach/settings/CoachConfig;->config_:Lcom/cigna/coach/settings/CoachConfig;

    .line 47
    :cond_0
    sget-object v0, Lcom/cigna/coach/settings/CoachConfig;->config_:Lcom/cigna/coach/settings/CoachConfig;

    return-object v0
.end method

.method public static final setConfig(Lcom/cigna/coach/settings/CoachConfig;)V
    .locals 0
    .param p0, "config"    # Lcom/cigna/coach/settings/CoachConfig;

    .prologue
    .line 51
    sput-object p0, Lcom/cigna/coach/settings/CoachConfig;->config_:Lcom/cigna/coach/settings/CoachConfig;

    .line 52
    return-void
.end method


# virtual methods
.method public multipleUsersAllowed()Z
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method
