.class public Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;
.super Ljava/lang/Object;
.source "HealthLibrary.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/HealthLibrary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ArticleTitleComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
        ">;"
    }
.end annotation


# instance fields
.field private final ascending:Z

.field private final collator:Ljava/text/Collator;


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "ascending"    # Z

    .prologue
    .line 430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 431
    iput-boolean p1, p0, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;->ascending:Z

    .line 432
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;->collator:Ljava/text/Collator;

    .line 433
    return-void
.end method

.method public constructor <init>(ZLjava/util/Locale;)V
    .locals 1
    .param p1, "ascending"    # Z
    .param p2, "locale"    # Ljava/util/Locale;

    .prologue
    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442
    iput-boolean p1, p0, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;->ascending:Z

    .line 443
    invoke-static {p2}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;->collator:Ljava/text/Collator;

    .line 444
    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/apiobjects/HealthLibraryArticle;Lcom/cigna/coach/apiobjects/HealthLibraryArticle;)I
    .locals 4
    .param p1, "lhs"    # Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .param p2, "rhs"    # Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .prologue
    .line 451
    const/4 v0, 0x0

    .line 453
    .local v0, "result":I
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 454
    iget-object v1, p0, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;->collator:Ljava/text/Collator;

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 457
    :cond_0
    iget-boolean v1, p0, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;->ascending:Z

    if-nez v1, :cond_1

    .line 458
    mul-int/lit8 v0, v0, -0x1

    .line 461
    :cond_1
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 418
    check-cast p1, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;->compare(Lcom/cigna/coach/apiobjects/HealthLibraryArticle;Lcom/cigna/coach/apiobjects/HealthLibraryArticle;)I

    move-result v0

    return v0
.end method
