.class public Lcom/cigna/coach/Tracker;
.super Ljava/lang/Object;
.source "Tracker.java"

# interfaces
.implements Lcom/cigna/coach/interfaces/ITracker;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-class v0, Lcom/cigna/coach/Tracker;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    .line 83
    return-void
.end method

.method private checkForQueueing(Ljava/util/List;Lcom/cigna/coach/db/CoachSQLiteHelper;)Ljava/util/List;
    .locals 30
    .param p2, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;",
            "Lcom/cigna/coach/db/CoachSQLiteHelper;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418
    .local p1, "listNot":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    sget-object v27, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v27 .. v27}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_0

    .line 419
    sget-object v27, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v28, "checkForQueueing : START"

    invoke-static/range {v27 .. v28}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 423
    .local v7, "listDeliver":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 427
    .local v9, "listQueued":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :try_start_0
    new-instance v13, Lcom/cigna/coach/db/NotificationDBManager;

    move-object/from16 v0, p2

    invoke-direct {v13, v0}, Lcom/cigna/coach/db/NotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 429
    .local v13, "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v2

    .line 430
    .local v2, "currentDate":Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 432
    .local v3, "currentTime":J
    invoke-static {v2}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v10

    .line 433
    .local v10, "midnightDate":Ljava/util/Calendar;
    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v11

    .line 435
    .local v11, "midnightTime":J
    invoke-virtual {v13}, Lcom/cigna/coach/db/NotificationDBManager;->getNotificationStateInfo()Lcom/cigna/coach/dataobjects/NotificationStateData;

    move-result-object v26

    .line 437
    .local v26, "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    invoke-virtual/range {v26 .. v26}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationStartTime()J

    move-result-wide v17

    .line 438
    .local v17, "notifcationStartTime":J
    invoke-virtual/range {v26 .. v26}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationEndTime()J

    move-result-wide v15

    .line 440
    .local v15, "notifcationEndTime":J
    add-long v22, v17, v11

    .line 441
    .local v22, "notificationStartDate":J
    add-long v20, v15, v11

    .line 444
    .local v20, "notificationEndDate":J
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v8

    .line 445
    .local v8, "listNotSize":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v8, :cond_5

    .line 446
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/dataobjects/NotificationData;

    .line 449
    .local v14, "not":Lcom/cigna/coach/dataobjects/NotificationData;
    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationDate()Ljava/util/Calendar;

    move-result-object v19

    .line 450
    .local v19, "notificationDate":Ljava/util/Calendar;
    if-nez v19, :cond_1

    .line 451
    invoke-virtual {v14, v2}, Lcom/cigna/coach/dataobjects/NotificationData;->setNotificationDate(Ljava/util/Calendar;)V

    .line 452
    move-object/from16 v19, v2

    .line 456
    :cond_1
    invoke-virtual/range {v19 .. v19}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v24

    .line 457
    .local v24, "notificationTime":J
    cmp-long v27, v3, v22

    if-ltz v27, :cond_2

    cmp-long v27, v3, v20

    if-gtz v27, :cond_2

    cmp-long v27, v24, v20

    if-lez v27, :cond_3

    .line 458
    :cond_2
    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 460
    :cond_3
    invoke-interface {v7, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 470
    .end local v2    # "currentDate":Ljava/util/Calendar;
    .end local v3    # "currentTime":J
    .end local v6    # "i":I
    .end local v8    # "listNotSize":I
    .end local v10    # "midnightDate":Ljava/util/Calendar;
    .end local v11    # "midnightTime":J
    .end local v13    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v14    # "not":Lcom/cigna/coach/dataobjects/NotificationData;
    .end local v15    # "notifcationEndTime":J
    .end local v17    # "notifcationStartTime":J
    .end local v19    # "notificationDate":Ljava/util/Calendar;
    .end local v20    # "notificationEndDate":J
    .end local v22    # "notificationStartDate":J
    .end local v24    # "notificationTime":J
    .end local v26    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :catch_0
    move-exception v5

    .line 471
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v27, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v28, "Error while checkForQueueing sending original notification"

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-static {v0, v1, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 472
    move-object/from16 v7, p1

    .line 475
    sget-object v27, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v27 .. v27}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 476
    sget-object v27, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v28, "checkForQueueing : END"

    invoke-static/range {v27 .. v28}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_2
    return-object v7

    .line 465
    .restart local v2    # "currentDate":Ljava/util/Calendar;
    .restart local v3    # "currentTime":J
    .restart local v6    # "i":I
    .restart local v8    # "listNotSize":I
    .restart local v10    # "midnightDate":Ljava/util/Calendar;
    .restart local v11    # "midnightTime":J
    .restart local v13    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v15    # "notifcationEndTime":J
    .restart local v17    # "notifcationStartTime":J
    .restart local v20    # "notificationEndDate":J
    .restart local v22    # "notificationStartDate":J
    .restart local v26    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :cond_5
    :try_start_2
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v27

    if-lez v27, :cond_6

    .line 466
    invoke-virtual {v13, v9}, Lcom/cigna/coach/db/NotificationDBManager;->addNotificationToQueue(Ljava/util/List;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 475
    :cond_6
    sget-object v27, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v27 .. v27}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_4

    .line 476
    sget-object v27, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v28, "checkForQueueing : END"

    invoke-static/range {v27 .. v28}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 475
    .end local v2    # "currentDate":Ljava/util/Calendar;
    .end local v3    # "currentTime":J
    .end local v6    # "i":I
    .end local v8    # "listNotSize":I
    .end local v10    # "midnightDate":Ljava/util/Calendar;
    .end local v11    # "midnightTime":J
    .end local v13    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v15    # "notifcationEndTime":J
    .end local v17    # "notifcationStartTime":J
    .end local v20    # "notificationEndDate":J
    .end local v22    # "notificationStartDate":J
    .end local v26    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :catchall_0
    move-exception v27

    sget-object v28, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v28 .. v28}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 476
    sget-object v28, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v29, "checkForQueueing : END"

    invoke-static/range {v28 .. v29}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v27
.end method


# virtual methods
.method public checkForInactivity(Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;)Ljava/util/List;
    .locals 27
    .param p1, "activtyEventType"    # Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 102
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 103
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v25, "checkForInactivity : START"

    invoke-static/range {v24 .. v25}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    const/16 v17, 0x0

    .line 108
    .local v17, "performActivityCheck":Z
    const/4 v4, 0x0

    .line 110
    .local v4, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v14, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfToday()Ljava/util/Calendar;

    move-result-object v10

    .line 115
    .local v10, "midnightDate":Ljava/util/Calendar;
    :try_start_0
    new-instance v5, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v5, v0, v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v5, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v11, Lcom/cigna/coach/db/NotificationDBManager;

    invoke-direct {v11, v5}, Lcom/cigna/coach/db/NotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 119
    .local v11, "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    invoke-virtual {v11}, Lcom/cigna/coach/db/NotificationDBManager;->getNotificationStateInfo()Lcom/cigna/coach/dataobjects/NotificationStateData;

    move-result-object v15

    .line 122
    .local v15, "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    sget-object v24, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->STARTUP:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_b

    .line 123
    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastStartupDate()Ljava/util/Calendar;

    move-result-object v8

    .line 124
    .local v8, "lastStartupDate":Ljava/util/Calendar;
    if-eqz v8, :cond_1

    invoke-virtual {v8, v10}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    .line 125
    :cond_1
    const/16 v17, 0x1

    .line 137
    .end local v8    # "lastStartupDate":Ljava/util/Calendar;
    :cond_2
    :goto_0
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_5

    .line 142
    if-eqz v5, :cond_3

    .line 143
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 144
    const/4 v4, 0x0

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    move-object v5, v4

    .line 150
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :cond_3
    :try_start_2
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 151
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "checkForInactivity : performActivityCheck : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_4
    if-eqz v17, :cond_11

    .line 155
    new-instance v4, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-direct {v4, v0, v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 156
    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_3
    new-instance v22, Lcom/cigna/coach/utils/UserIdUtils;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 157
    .local v22, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v21

    .line 160
    .local v21, "userId":Ljava/lang/String;
    new-instance v13, Lcom/cigna/coach/utils/NotificationHelper;

    invoke-direct {v13, v4}, Lcom/cigna/coach/utils/NotificationHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 161
    .local v13, "nh":Lcom/cigna/coach/utils/NotificationHelper;
    new-instance v19, Lcom/cigna/coach/utils/TrackerHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-direct {v0, v4, v1}, Lcom/cigna/coach/utils/TrackerHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;Landroid/content/Context;)V

    .line 162
    .local v19, "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/TrackerHelper;->processTrackerMissionForChangeWhileOffline(Ljava/lang/String;)Ljava/util/List;

    move-result-object v20

    .line 163
    .local v20, "trackerNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    if-eqz v20, :cond_5

    .line 164
    move-object/from16 v0, v20

    invoke-interface {v14, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 168
    :cond_5
    move-object/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/cigna/coach/utils/NotificationHelper;->getCheckUserProgressNotifications(Ljava/lang/String;)Ljava/util/List;

    move-result-object v18

    .line 169
    .local v18, "progressNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    if-eqz v18, :cond_6

    .line 170
    new-instance v3, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v3, v4}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 171
    .local v3, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 172
    move-object/from16 v0, v18

    invoke-interface {v14, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 176
    .end local v3    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    :cond_6
    new-instance v7, Lcom/cigna/coach/utils/GoalMissionHelper;

    invoke-direct {v7, v4}, Lcom/cigna/coach/utils/GoalMissionHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 177
    .local v7, "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Lcom/cigna/coach/utils/GoalMissionHelper;->checkForMissionExpiry(Ljava/lang/String;)V

    .line 181
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 182
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "checkForInactivity : notification size : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_7
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v24

    if-lez v24, :cond_8

    .line 186
    new-instance v23, Lcom/cigna/coach/utils/WidgetHelper;

    move-object/from16 v0, v23

    invoke-direct {v0, v4}, Lcom/cigna/coach/utils/WidgetHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 187
    .local v23, "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/WidgetHelper;->cleanupWidgetNotifications(Ljava/lang/String;)V

    .line 188
    sget-object v24, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v14, v2}, Lcom/cigna/coach/utils/WidgetHelper;->setWidgetNotifications(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V

    .line 190
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v4}, Lcom/cigna/coach/Tracker;->checkForQueueing(Ljava/util/List;Lcom/cigna/coach/db/CoachSQLiteHelper;)Ljava/util/List;

    move-result-object v14

    .line 193
    .end local v23    # "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    :cond_8
    new-instance v16, Lcom/cigna/coach/dataobjects/NotificationStateData;

    invoke-direct/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/NotificationStateData;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 194
    .end local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .local v16, "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :try_start_4
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->setLastStartupDate(Ljava/util/Calendar;)V

    .line 195
    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->setLastTimeChangeDate(Ljava/util/Calendar;)V

    .line 196
    new-instance v12, Lcom/cigna/coach/db/NotificationDBManager;

    invoke-direct {v12, v4}, Lcom/cigna/coach/db/NotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 197
    .end local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .local v12, "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :try_start_5
    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Lcom/cigna/coach/db/NotificationDBManager;->setNotificationStateInfo(Lcom/cigna/coach/dataobjects/NotificationStateData;)Z

    .line 199
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-object/from16 v15, v16

    .end local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    move-object v11, v12

    .line 205
    .end local v7    # "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    .end local v12    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v13    # "nh":Lcom/cigna/coach/utils/NotificationHelper;
    .end local v18    # "progressNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v19    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    .end local v20    # "trackerNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v21    # "userId":Ljava/lang/String;
    .end local v22    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    .restart local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :goto_1
    if-eqz v4, :cond_9

    .line 206
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 208
    :cond_9
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 209
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v25, "checkForInactivity : END"

    invoke-static/range {v24 .. v25}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    :cond_a
    return-object v14

    .line 127
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :cond_b
    :try_start_6
    sget-object v24, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DATE_CHANGE:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_d

    .line 128
    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastTimeChangeDate()Ljava/util/Calendar;

    move-result-object v9

    .line 129
    .local v9, "lastTimeChangeDate":Ljava/util/Calendar;
    if-eqz v9, :cond_c

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_2

    .line 130
    :cond_c
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 132
    .end local v9    # "lastTimeChangeDate":Ljava/util/Calendar;
    :cond_d
    sget-object v24, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DAILY:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 133
    const/16 v17, 0x1

    .line 135
    const-string v24, "Tracker:checkForInactivity"

    const-string v25, "New Log file creation requested at midnight."

    invoke-static/range {v24 .. v25}, Lcom/cigna/coach/utils/Log;->create(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_5

    goto/16 :goto_0

    .line 138
    .end local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :catch_0
    move-exception v6

    move-object v4, v5

    .line 139
    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v6, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_7
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v25, "Error while checkForInactivity"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v0, v1, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 140
    new-instance v24, Lcom/cigna/coach/exceptions/CoachException;

    const-string v25, "Error while checkForInactivity."

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v6}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v24
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 142
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v24

    :goto_3
    if-eqz v4, :cond_e

    .line 143
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 144
    const/4 v4, 0x0

    :cond_e
    throw v24

    .line 201
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :catch_1
    move-exception v6

    move-object v4, v5

    .line 202
    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v6    # "e":Ljava/lang/Exception;
    :goto_4
    :try_start_8
    sget-object v24, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v25, "Error while checkForInactivity"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-static {v0, v1, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 203
    new-instance v24, Lcom/cigna/coach/exceptions/CoachException;

    const-string v25, "Error while checkForInactivity."

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v6}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v24
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 205
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v24

    :goto_5
    if-eqz v4, :cond_f

    .line 206
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 208
    :cond_f
    sget-object v25, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 209
    sget-object v25, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "checkForInactivity : END"

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    throw v24

    .line 205
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_2
    move-exception v24

    move-object v4, v5

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_5

    .end local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v7    # "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    .restart local v13    # "nh":Lcom/cigna/coach/utils/NotificationHelper;
    .restart local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v18    # "progressNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .restart local v19    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    .restart local v20    # "trackerNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .restart local v21    # "userId":Ljava/lang/String;
    .restart local v22    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catchall_3
    move-exception v24

    move-object/from16 v15, v16

    .end local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    goto :goto_5

    .end local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v12    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :catchall_4
    move-exception v24

    move-object/from16 v15, v16

    .end local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    move-object v11, v12

    .end local v12    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    goto :goto_5

    .line 201
    .end local v7    # "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    .end local v13    # "nh":Lcom/cigna/coach/utils/NotificationHelper;
    .end local v18    # "progressNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v19    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    .end local v20    # "trackerNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v21    # "userId":Ljava/lang/String;
    .end local v22    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catch_2
    move-exception v6

    goto :goto_4

    .end local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v7    # "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    .restart local v13    # "nh":Lcom/cigna/coach/utils/NotificationHelper;
    .restart local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v18    # "progressNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .restart local v19    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    .restart local v20    # "trackerNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .restart local v21    # "userId":Ljava/lang/String;
    .restart local v22    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catch_3
    move-exception v6

    move-object/from16 v15, v16

    .end local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    goto :goto_4

    .end local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v12    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :catch_4
    move-exception v6

    move-object/from16 v15, v16

    .end local v16    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    move-object v11, v12

    .end local v12    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    goto :goto_4

    .line 142
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v7    # "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    .end local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v13    # "nh":Lcom/cigna/coach/utils/NotificationHelper;
    .end local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .end local v18    # "progressNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v19    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    .end local v20    # "trackerNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v21    # "userId":Ljava/lang/String;
    .end local v22    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_5
    move-exception v24

    move-object v4, v5

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_3

    .line 138
    :catch_5
    move-exception v6

    goto :goto_2

    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v11    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v15    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    :cond_11
    move-object v4, v5

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto/16 :goto_1
.end method

.method public getNotificationStartTime()J
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 297
    sget-object v7, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 298
    sget-object v7, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getNotificationStartTime : START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_0
    const-wide/16 v4, 0x0

    .line 301
    .local v4, "notifcationStartTime":J
    const/4 v0, 0x0

    .line 305
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v7, p0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-direct {v1, v7, v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    .end local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v1, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v3, Lcom/cigna/coach/db/NotificationDBManager;

    invoke-direct {v3, v1}, Lcom/cigna/coach/db/NotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 308
    .local v3, "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    invoke-virtual {v3}, Lcom/cigna/coach/db/NotificationDBManager;->getNotificationStateInfo()Lcom/cigna/coach/dataobjects/NotificationStateData;

    move-result-object v6

    .line 310
    .local v6, "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationStartTime()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v4

    .line 315
    if-eqz v1, :cond_1

    .line 316
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 318
    :cond_1
    sget-object v7, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 319
    sget-object v7, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getNotificationStartTime : END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_2
    return-wide v4

    .line 311
    .end local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v3    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .end local v6    # "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    .restart local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_0
    move-exception v2

    .line 312
    .local v2, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v7, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error while getNotificationStartTime"

    invoke-static {v7, v8, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 313
    new-instance v7, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Error while getNotificationStartTime."

    invoke-direct {v7, v8, v2}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 315
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    :goto_1
    if-eqz v0, :cond_3

    .line 316
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 318
    :cond_3
    sget-object v8, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 319
    sget-object v8, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getNotificationStartTime : END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v7

    .line 315
    .end local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_1

    .line 311
    .end local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_1
    move-exception v2

    move-object v0, v1

    .end local v1    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v0    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_0
.end method

.method public getQueuedNotifications(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 226
    const/4 v2, 0x0

    .line 227
    .local v2, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    const/4 v5, 0x0

    .line 230
    .local v5, "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 231
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getQueuedNotifications : START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_0
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v1

    .line 238
    .local v1, "currentDate":Ljava/util/Calendar;
    :try_start_0
    new-instance v3, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v9, p0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    const/4 v10, 0x0

    invoke-direct {v3, v9, v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v3, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v6, Lcom/cigna/coach/db/NotificationDBManager;

    invoke-direct {v6, v3}, Lcom/cigna/coach/db/NotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 241
    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .local v6, "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :try_start_2
    invoke-virtual {v6, v1}, Lcom/cigna/coach/db/NotificationDBManager;->getNotificationsFromQueue(Ljava/util/Calendar;)Ljava/util/List;

    move-result-object v7

    .line 245
    .local v7, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v0, v3}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 246
    .local v0, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v0, v7}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    .line 251
    if-eqz v3, :cond_1

    .line 252
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 253
    const/4 v2, 0x0

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    move-object v3, v2

    .line 259
    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :cond_1
    :try_start_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_9

    .line 260
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 261
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getQueuedNotifications : found notifications in queue:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_2
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v9, p0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-direct {v2, v9, v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 265
    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_4
    new-instance v5, Lcom/cigna/coach/db/NotificationDBManager;

    invoke-direct {v5, v2}, Lcom/cigna/coach/db/NotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 267
    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :try_start_5
    invoke-virtual {v5, v1}, Lcom/cigna/coach/db/NotificationDBManager;->deleteNotificationsFromQueue(Ljava/util/Calendar;)Z

    move-result v8

    .line 268
    .local v8, "success":Z
    if-nez v8, :cond_6

    .line 269
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Error while deleting notifications in getQueuedNotifications"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 279
    .end local v8    # "success":Z
    :goto_0
    if-eqz v2, :cond_3

    .line 280
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 282
    :cond_3
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 283
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getQueuedNotifications : END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_4
    return-object v7

    .line 247
    .end local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v7    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :catch_0
    move-exception v4

    .line 248
    .local v4, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_6
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Error while getQueuedNotifications"

    invoke-static {v9, v10, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 249
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    const-string v10, "Error while getQueuedNotifications."

    invoke-direct {v9, v10, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 251
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    :goto_2
    if-eqz v2, :cond_5

    .line 252
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 253
    const/4 v2, 0x0

    :cond_5
    throw v9

    .line 271
    .restart local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v7    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .restart local v8    # "success":Z
    :cond_6
    :try_start_7
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 275
    .end local v8    # "success":Z
    :catch_1
    move-exception v4

    .line 276
    .restart local v4    # "e":Ljava/lang/Exception;
    :goto_3
    :try_start_8
    sget-object v9, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Error while getQueuedNotifications"

    invoke-static {v9, v10, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 277
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    const-string v10, "Error while getQueuedNotifications."

    invoke-direct {v9, v10, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 279
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v9

    :goto_4
    if-eqz v2, :cond_7

    .line 280
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 282
    :cond_7
    sget-object v10, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 283
    sget-object v10, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getQueuedNotifications : END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v9

    .line 279
    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :catchall_2
    move-exception v9

    move-object v5, v6

    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    move-object v2, v3

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_4

    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :catchall_3
    move-exception v9

    move-object v5, v6

    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    goto :goto_4

    .line 275
    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :catch_2
    move-exception v4

    move-object v5, v6

    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    move-object v2, v3

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_3

    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :catch_3
    move-exception v4

    move-object v5, v6

    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    goto :goto_3

    .line 251
    .end local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v7    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_4
    move-exception v9

    move-object v2, v3

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_2

    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :catchall_5
    move-exception v9

    move-object v5, v6

    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    move-object v2, v3

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_2

    .line 247
    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_4
    move-exception v4

    move-object v2, v3

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_1

    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    :catch_5
    move-exception v4

    move-object v5, v6

    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    move-object v2, v3

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_1

    .end local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v7    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :cond_9
    move-object v5, v6

    .end local v6    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    .restart local v5    # "ndbm":Lcom/cigna/coach/db/NotificationDBManager;
    move-object v2, v3

    .end local v3    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v2    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_0
.end method

.method public declared-synchronized processTrackerChange(Ljava/lang/String;Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)Ljava/util/List;
    .locals 20
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "trackerEventType"    # Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 336
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v18, "processTrackerChange : START"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    :cond_0
    const/4 v12, 0x0

    .line 340
    .local v12, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    const/4 v7, 0x0

    .line 341
    .local v7, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 344
    .local v6, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    const/16 v17, 0x1

    :try_start_1
    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->setTrackerRunning(Z)V

    .line 345
    new-instance v8, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v8, v0, v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    .end local v7    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v8, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_2
    new-instance v15, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v15, v8}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 347
    .local v15, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    new-instance v4, Lcom/cigna/coach/utils/BadgeAwarder;

    invoke-virtual {v15}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v4, v0, v8}, Lcom/cigna/coach/utils/BadgeAwarder;-><init>(Ljava/lang/String;Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 349
    .local v4, "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    sget-object v17, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->EXERCISE:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    sget-object v17, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->PEDOMETER_DATA:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_5

    .line 350
    :cond_1
    new-instance v14, Lcom/cigna/coach/utils/TrackerHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v14, v8, v0}, Lcom/cigna/coach/utils/TrackerHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;Landroid/content/Context;)V

    .line 351
    .local v14, "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    invoke-virtual {v15}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v14, v0, v1}, Lcom/cigna/coach/utils/TrackerHelper;->processTrackerMissionsForChange(Ljava/lang/String;Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)Ljava/util/List;

    move-result-object v12

    .line 372
    .end local v14    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    :cond_2
    :goto_0
    invoke-virtual {v4}, Lcom/cigna/coach/utils/BadgeAwarder;->awardBadges()Ljava/util/List;

    move-result-object v5

    .line 373
    .local v5, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 374
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Badges earned: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/Badge;

    .line 376
    .local v3, "badge":Lcom/cigna/coach/apiobjects/Badge;
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "    badge id: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    .line 391
    .end local v3    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    .end local v4    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .end local v5    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v15    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catch_0
    move-exception v9

    move-object v7, v8

    .line 392
    .end local v8    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v7    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v9, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Error while processTrackerChange."

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 393
    new-instance v17, Lcom/cigna/coach/exceptions/CoachException;

    const-string v18, "Error while processTrackerChange."

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v9}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v17
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 395
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v17

    :goto_3
    if-eqz v7, :cond_3

    .line 396
    :try_start_4
    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 398
    :cond_3
    sget-object v18, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 399
    sget-object v18, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v19, "processTrackerChange : END"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_4
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->setTrackerRunning(Z)V

    throw v17
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 335
    .end local v6    # "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    .end local v7    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v12    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :catchall_1
    move-exception v17

    monitor-exit p0

    throw v17

    .line 353
    .restart local v4    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .restart local v6    # "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    .restart local v8    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v12    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .restart local v15    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :cond_5
    :try_start_5
    sget-object v17, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 354
    new-instance v11, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-direct {v11, v8}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 355
    .local v11, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    sget-object v17, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v15}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v11, v0, v1, v2}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserScoreForCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/util/Calendar;)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 356
    .local v13, "scoreForCategory":I
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 357
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "scoreForCategory: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :cond_6
    if-ltz v13, :cond_7

    .line 360
    new-instance v17, Lcom/cigna/coach/utils/WeightChangeListener;

    move-object/from16 v0, v17

    invoke-direct {v0, v8}, Lcom/cigna/coach/utils/WeightChangeListener;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v15}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Lcom/cigna/coach/utils/WeightChangeListener;->updateWeightGoalAndScore(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 395
    .end local v4    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .end local v11    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v13    # "scoreForCategory":I
    .end local v15    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catchall_2
    move-exception v17

    move-object v7, v8

    .end local v8    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v7    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_3

    .line 364
    .end local v7    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .restart local v8    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v11    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .restart local v13    # "scoreForCategory":I
    .restart local v15    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :cond_7
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 365
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Not considering the Weight score update because the Weight assesment is not complete even once. WeightChangeListener: true &  scoreForCategory : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 379
    .end local v11    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v13    # "scoreForCategory":I
    .restart local v5    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :cond_8
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 380
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "notifications : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_9
    if-eqz v12, :cond_a

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_a

    .line 384
    new-instance v16, Lcom/cigna/coach/utils/WidgetHelper;

    move-object/from16 v0, v16

    invoke-direct {v0, v8}, Lcom/cigna/coach/utils/WidgetHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 385
    .local v16, "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    invoke-virtual {v15}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v17

    sget-object v18, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v12, v2}, Lcom/cigna/coach/utils/WidgetHelper;->setWidgetNotifications(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/Tracker;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V

    .line 387
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v8}, Lcom/cigna/coach/Tracker;->checkForQueueing(Ljava/util/List;Lcom/cigna/coach/db/CoachSQLiteHelper;)Ljava/util/List;

    move-result-object v12

    .line 390
    .end local v16    # "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    :cond_a
    invoke-virtual {v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 395
    if-eqz v8, :cond_b

    .line 396
    :try_start_6
    invoke-virtual {v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 398
    :cond_b
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 399
    sget-object v17, Lcom/cigna/coach/Tracker;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v18, "processTrackerChange : END"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    :cond_c
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->setTrackerRunning(Z)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 404
    monitor-exit p0

    return-object v12

    .line 391
    .end local v4    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .end local v5    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v8    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v15    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    .restart local v7    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_1
    move-exception v9

    goto/16 :goto_2
.end method
