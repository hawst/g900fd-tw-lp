.class public final enum Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
.super Ljava/lang/Enum;
.source "UserMetrics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/UserMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserMetricGender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

.field public static final enum FEMALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

.field public static final enum MALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

.field public static final enum OTHER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

.field public static final enum UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field genderTypeKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x0

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 82
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    const-string v5, "FEMALE"

    invoke-direct {v4, v5, v9, v6}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->FEMALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .line 83
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    const-string v5, "MALE"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->MALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .line 84
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    const-string v5, "OTHER"

    invoke-direct {v4, v5, v7, v8}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->OTHER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .line 85
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    const-string v5, "UNKNOWN"

    invoke-direct {v4, v5, v8, v10}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .line 81
    new-array v4, v10, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->FEMALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->MALE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->OTHER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    aput-object v5, v4, v8

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .line 98
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->codeValueMap:Ljava/util/HashMap;

    .line 100
    invoke-static {}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 101
    .local v3, "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    sget-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->getGenderTypeKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "genderTypeKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 90
    iput p3, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->genderTypeKey:I

    .line 91
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    .locals 2
    .param p0, "genderTypeKey"    # I

    .prologue
    .line 106
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 81
    const-class v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    return-object v0
.end method


# virtual methods
.method public getGenderTypeKey()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->genderTypeKey:I

    return v0
.end method
