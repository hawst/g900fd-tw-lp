.class public Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
.super Ljava/lang/Object;
.source "GoalsMissionsHistory.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private completedGoalsCount:I

.field private completedGoalsLabel:Ljava/lang/String;

.field private completedMissionsCount:I

.field private completedMissionsLabel:Ljava/lang/String;

.field private goalInfoMissionsInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCompletedGoalsCount()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedGoalsCount:I

    return v0
.end method

.method public getCompletedGoalsLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedGoalsLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getCompletedMissionsCount()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedMissionsCount:I

    return v0
.end method

.method public getCompletedMissionsLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedMissionsLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalInfoMissionsInfos()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->goalInfoMissionsInfos:Ljava/util/List;

    return-object v0
.end method

.method public setCompletedGoalsCount(I)V
    .locals 0
    .param p1, "completedGoalsCount"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedGoalsCount:I

    .line 69
    return-void
.end method

.method public setCompletedGoalsLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "completedGoalsLabel"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedGoalsLabel:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setCompletedMissionsCount(I)V
    .locals 0
    .param p1, "completedMissionsCount"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedMissionsCount:I

    .line 87
    return-void
.end method

.method public setCompletedMissionsLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "completedMissionsLabel"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->completedMissionsLabel:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setGoalInfoMissionsInfos(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "goalInfoMissionsInfos":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;->goalInfoMissionsInfos:Ljava/util/List;

    .line 142
    return-void
.end method
