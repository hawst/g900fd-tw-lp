.class public Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;
.super Ljava/lang/Object;
.source "HealthLibrarySubCategory.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field protected categoryId:I

.field protected description:Ljava/lang/String;

.field protected image:Ljava/lang/String;

.field protected subCategoryId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategoryId()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;->categoryId:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;->image:Ljava/lang/String;

    return-object v0
.end method

.method public getSubCategoryId()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;->subCategoryId:I

    return v0
.end method
