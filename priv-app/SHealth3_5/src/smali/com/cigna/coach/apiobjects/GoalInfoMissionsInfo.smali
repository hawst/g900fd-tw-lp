.class public Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
.super Lcom/cigna/coach/apiobjects/GoalInfo;
.source "GoalInfoMissionsInfo.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

.field private goalMissionDetailInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/GoalInfo;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public getCoachMessage()Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

    return-object v0
.end method

.method public getGoalMissionDetailInfoList()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->goalMissionDetailInfoList:Ljava/util/List;

    return-object v0
.end method

.method public setCoachMessage(Lcom/cigna/coach/apiobjects/CoachMessage;)V
    .locals 0
    .param p1, "coachMessage"    # Lcom/cigna/coach/apiobjects/CoachMessage;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 56
    return-void
.end method

.method public setGoalMissionDetailInfoList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "goalMissionDetailInfoList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->goalMissionDetailInfoList:Ljava/util/List;

    .line 79
    return-void
.end method
