.class public Lcom/cigna/coach/apiobjects/QuestionAnswers;
.super Ljava/lang/Object;
.source "QuestionAnswers.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private answerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;"
        }
    .end annotation
.end field

.field private question:Ljava/lang/String;

.field private questionId:I

.field private questionImage:Ljava/lang/String;

.field private trackerSaysLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->questionId:I

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->question:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->questionImage:Ljava/lang/String;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->trackerSaysLabel:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public getAnswerList()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->answerList:Ljava/util/List;

    return-object v0
.end method

.method public getQuestion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->question:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionId()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->questionId:I

    return v0
.end method

.method public getQuestionImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->questionImage:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackerSaysLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->trackerSaysLabel:Ljava/lang/String;

    return-object v0
.end method

.method public setAnswerList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "answerList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->answerList:Ljava/util/List;

    .line 97
    return-void
.end method

.method public setQuestion(Ljava/lang/String;)V
    .locals 0
    .param p1, "question"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->question:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setQuestionId(I)V
    .locals 0
    .param p1, "questionId"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->questionId:I

    .line 116
    return-void
.end method

.method public setQuestionImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "questionImage"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->questionImage:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setTrackerSaysLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackerSaysLabel"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/QuestionAnswers;->trackerSaysLabel:Ljava/lang/String;

    .line 152
    return-void
.end method
