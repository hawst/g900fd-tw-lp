.class public final enum Lcom/cigna/coach/apiobjects/Notification$NotificationType;
.super Ljava/lang/Enum;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NotificationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/Notification$NotificationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/Notification$NotificationType;

.field public static final enum ANDROID_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

.field public static final enum BOTH_ANDROID_AND_TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

.field public static final enum COACH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

.field public static final enum SHEALTH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

.field public static final enum TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/Notification$NotificationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field notificationTypeKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 73
    new-instance v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    const-string v5, "TIMELINE_NOTIFICATION"

    invoke-direct {v4, v5, v11, v7}, Lcom/cigna/coach/apiobjects/Notification$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 74
    new-instance v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    const-string v5, "ANDROID_NOTFICATION"

    invoke-direct {v4, v5, v7, v8}, Lcom/cigna/coach/apiobjects/Notification$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->ANDROID_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 75
    new-instance v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    const-string v5, "BOTH_ANDROID_AND_TIMELINE_NOTIFICATION"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/apiobjects/Notification$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->BOTH_ANDROID_AND_TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 76
    new-instance v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    const-string v5, "COACH_WIDGET_NOTFICATION"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/apiobjects/Notification$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->COACH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 77
    new-instance v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    const-string v5, "SHEALTH_WIDGET_NOTFICATION"

    const/4 v6, 0x5

    invoke-direct {v4, v5, v10, v6}, Lcom/cigna/coach/apiobjects/Notification$NotificationType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->SHEALTH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 72
    const/4 v4, 0x5

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->ANDROID_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->BOTH_ANDROID_AND_TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->COACH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->SHEALTH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    aput-object v5, v4, v10

    sput-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->$VALUES:[Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 89
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->codeValueMap:Ljava/util/HashMap;

    .line 91
    invoke-static {}, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->values()[Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 92
    .local v3, "type":Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    sget-object v4, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->getNotificationTypeKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "notificationTypeKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    iput p3, p0, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->notificationTypeKey:I

    .line 85
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    .locals 2
    .param p0, "notificationTypeKey"    # I

    .prologue
    .line 103
    sget-object v0, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->$VALUES:[Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/Notification$NotificationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    return-object v0
.end method


# virtual methods
.method public getNotificationTypeKey()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->notificationTypeKey:I

    return v0
.end method
