.class public Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
.super Ljava/lang/Object;
.source "HealthLibraryArticle.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field protected category:Ljava/lang/String;

.field protected categoryId:I

.field protected categoryImage:Ljava/lang/String;

.field protected detail:Ljava/lang/String;

.field protected favorite:Z

.field protected id:I

.field protected subCategory:Ljava/lang/String;

.field protected subCategoryId:I

.field protected subCategoryImage:Ljava/lang/String;

.field protected title:Ljava/lang/String;

.field protected titleImg:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryId()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->categoryId:I

    return v0
.end method

.method public getCategoryImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->categoryImage:Ljava/lang/String;

    return-object v0
.end method

.method public getDetail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->detail:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 126
    iget v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->id:I

    return v0
.end method

.method public getSubCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->subCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getSubCategoryId()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->subCategoryId:I

    return v0
.end method

.method public getSubCategoryImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->subCategoryImage:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleImg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->titleImg:Ljava/lang/String;

    return-object v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;->favorite:Z

    return v0
.end method
