.class public final enum Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
.super Ljava/lang/Enum;
.source "CoachMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/CoachMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoachSpecificMessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_ABOUT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CATEGORY_EXERCISE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CATEGORY_FOOD:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CATEGORY_SLEEP:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CATEGORY_STRESS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CATEGORY_WEIGHT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CORNER:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CORNER_HELLO:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field public static final enum COACH_MESSAGE_CORNER_SCORE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field specificMessageType:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 94
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CORNER"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 95
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_ABOUT"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_ABOUT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 96
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CATEGORY_EXERCISE"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_EXERCISE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 97
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CATEGORY_FOOD"

    invoke-direct {v4, v5, v10, v11}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_FOOD:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 98
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CATEGORY_SLEEP"

    invoke-direct {v4, v5, v11, v12}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_SLEEP:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 99
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CATEGORY_STRESS"

    const/4 v6, 0x6

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_STRESS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 100
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CATEGORY_WEIGHT"

    const/4 v6, 0x6

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_WEIGHT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 101
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CORNER_HELLO"

    const/4 v6, 0x7

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER_HELLO:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 102
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const-string v5, "COACH_MESSAGE_CORNER_SCORE"

    const/16 v6, 0x8

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER_SCORE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 93
    const/16 v4, 0x9

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_ABOUT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_EXERCISE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_FOOD:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_SLEEP:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_STRESS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CATEGORY_WEIGHT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER_HELLO:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->COACH_MESSAGE_CORNER_SCORE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->$VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    .line 114
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->codeValueMap:Ljava/util/HashMap;

    .line 116
    invoke-static {}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->values()[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 117
    .local v3, "type":Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->getMessageType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 119
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "mType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 109
    iput p3, p0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->specificMessageType:I

    .line 110
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    .locals 2
    .param p0, "coachSpecificMessageTypeKey"    # I

    .prologue
    .line 128
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 93
    const-class v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->$VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;

    return-object v0
.end method


# virtual methods
.method public getMessageType()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->specificMessageType:I

    return v0
.end method
