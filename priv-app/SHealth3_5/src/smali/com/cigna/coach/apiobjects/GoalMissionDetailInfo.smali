.class public Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
.super Lcom/cigna/coach/apiobjects/GoalMissionInfo;
.source "GoalMissionDetailInfo.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

.field private missionActivityCompletedTotalCount:I

.field private missionDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation
.end field

.field private missionProgressStatus:Ljava/lang/String;

.field private missionProgressStatusImage:Ljava/lang/String;

.field private missionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field private missionRepeatable:Z

.field private selectedFrequency:Ljava/lang/String;

.field private tips:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionTip;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;-><init>()V

    .line 75
    return-void
.end method


# virtual methods
.method public getCoachMessage()Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

    return-object v0
.end method

.method public getMissionActivityCompletedTotalCount()I
    .locals 1

    .prologue
    .line 244
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionActivityCompletedTotalCount:I

    return v0
.end method

.method public getMissionDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionDetails:Ljava/util/List;

    return-object v0
.end method

.method public getMissionProgressStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionProgressStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionProgressStatusImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionProgressStatusImage:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    return-object v0
.end method

.method public getSelectedFrequency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->selectedFrequency:Ljava/lang/String;

    return-object v0
.end method

.method public getTips()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionTip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->tips:Ljava/util/List;

    return-object v0
.end method

.method public isMissionRepeatable()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionRepeatable:Z

    return v0
.end method

.method public setCoachMessage(Lcom/cigna/coach/apiobjects/CoachMessage;)V
    .locals 0
    .param p1, "coachMessage"    # Lcom/cigna/coach/apiobjects/CoachMessage;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 95
    return-void
.end method

.method public setMissionActivityCompletedTotalCount(I)V
    .locals 0
    .param p1, "missionActivityCompletedTotalCount"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionActivityCompletedTotalCount:I

    .line 256
    return-void
.end method

.method public setMissionDetails(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionDetails:Ljava/util/List;

    .line 236
    return-void
.end method

.method public setMissionProgressStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressStatus"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionProgressStatus:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setMissionProgressStatusImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressStatusImage"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionProgressStatusImage:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setMissionProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V
    .locals 0
    .param p1, "missionProgressStatusType"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 155
    return-void
.end method

.method public setMissionRepeatable(Z)V
    .locals 0
    .param p1, "missionRepeatable"    # Z

    .prologue
    .line 174
    iput-boolean p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->missionRepeatable:Z

    .line 175
    return-void
.end method

.method public setSelectedFrequency(Ljava/lang/String;)V
    .locals 0
    .param p1, "selectedFrequency"    # Ljava/lang/String;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->selectedFrequency:Ljava/lang/String;

    .line 216
    return-void
.end method

.method public setTips(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionTip;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 195
    .local p1, "tips":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->tips:Ljava/util/List;

    .line 196
    return-void
.end method
