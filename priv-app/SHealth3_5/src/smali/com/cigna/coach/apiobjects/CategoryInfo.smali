.class public Lcom/cigna/coach/apiobjects/CategoryInfo;
.super Ljava/lang/Object;
.source "CategoryInfo.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private category:Ljava/lang/String;

.field private categoryImage:Ljava/lang/String;

.field private categoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private isActive:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->categoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->category:Ljava/lang/String;

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->categoryImage:Ljava/lang/String;

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->isActive:I

    .line 53
    return-void
.end method


# virtual methods
.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->categoryImage:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->categoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public getIsActive()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->isActive:I

    return v0
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->category:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public setCategoryImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "categoryImage"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->categoryImage:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 0
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->categoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 73
    return-void
.end method

.method public setIsActive(I)V
    .locals 0
    .param p1, "isActive"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/cigna/coach/apiobjects/CategoryInfo;->isActive:I

    .line 132
    return-void
.end method
