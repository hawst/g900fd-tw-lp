.class public Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
.super Ljava/lang/Object;
.source "AndroidWidgetInfo.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private cignaLogoURI:Ljava/lang/String;

.field private lifeStyleScore:I

.field private messageIntent:Landroid/content/Intent;

.field private messageText:Ljava/lang/String;

.field private titleIntent:Landroid/content/Intent;

.field private titleText:Ljava/lang/String;

.field private widgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field private widgetType:Lcom/cigna/coach/interfaces/IWidget$WidgetType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCignaLogoImageURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->cignaLogoURI:Ljava/lang/String;

    return-object v0
.end method

.method public getLifeStyleScore()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->lifeStyleScore:I

    return v0
.end method

.method public getMessageIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->messageIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getMessageText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->messageText:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->titleIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getTitleText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->titleText:Ljava/lang/String;

    return-object v0
.end method

.method public getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->widgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    return-object v0
.end method

.method public getWidgetType()Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->widgetType:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    return-object v0
.end method

.method public setCignaLogoImageURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "cignaLogoURI"    # Ljava/lang/String;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->cignaLogoURI:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public setLifeStyleScore(I)V
    .locals 0
    .param p1, "lifeStyleScore"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->lifeStyleScore:I

    .line 84
    return-void
.end method

.method public setMessageIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "messageIntent"    # Landroid/content/Intent;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->messageIntent:Landroid/content/Intent;

    .line 161
    return-void
.end method

.method public setMessageText(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageText"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->messageText:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setTitleIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "titleIntent"    # Landroid/content/Intent;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->titleIntent:Landroid/content/Intent;

    .line 122
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleText"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->titleText:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .locals 0
    .param p1, "widgetInfoType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->widgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 182
    return-void
.end method

.method public setWidgetType(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V
    .locals 0
    .param p1, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->widgetType:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .line 221
    return-void
.end method
