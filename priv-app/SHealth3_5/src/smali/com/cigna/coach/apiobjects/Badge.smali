.class public Lcom/cigna/coach/apiobjects/Badge;
.super Ljava/lang/Object;
.source "Badge.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private badge:Ljava/lang/String;

.field private badgeBackgroundImage:Ljava/lang/String;

.field private badgeDescription:Ljava/lang/String;

.field private badgeEarnedDate:Ljava/util/Calendar;

.field private badgeId:I

.field private badgeImage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    return-void
.end method


# virtual methods
.method public getBadge()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Badge;->badge:Ljava/lang/String;

    return-object v0
.end method

.method public getBadgeBackgroundImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeBackgroundImage:Ljava/lang/String;

    return-object v0
.end method

.method public getBadgeDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getBadgeEarnedDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeEarnedDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getBadgeId()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeId:I

    return v0
.end method

.method public getBadgeImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeImage:Ljava/lang/String;

    return-object v0
.end method

.method public setBadge(Ljava/lang/String;)V
    .locals 0
    .param p1, "badge"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Badge;->badge:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setBadgeBackgroundImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "badgeBackgroundImage"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeBackgroundImage:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setBadgeDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "badgeDescription"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeDescription:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public setBadgeEarnedDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "badgeEarnedDate"    # Ljava/util/Calendar;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeEarnedDate:Ljava/util/Calendar;

    .line 161
    return-void
.end method

.method public setBadgeId(I)V
    .locals 0
    .param p1, "badgeId"    # I

    .prologue
    .line 140
    iput p1, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeId:I

    .line 141
    return-void
.end method

.method public setBadgeImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "badgeImage"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Badge;->badgeImage:Ljava/lang/String;

    .line 180
    return-void
.end method
