.class public Lcom/cigna/coach/apiobjects/LifeStyleScores;
.super Ljava/lang/Object;
.source "LifeStyleScores.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    }
.end annotation


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

.field private score:I

.field private scoreAssignmentDate:Ljava/util/Calendar;

.field private scoreType:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

.field private varianceText:Ljava/lang/String;

.field private varianceValue:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->score:I

    .line 111
    return-void
.end method


# virtual methods
.method public getScore()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->score:I

    return v0
.end method

.method public getScoreAssignmentDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->scoreAssignmentDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getScoreType()Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->scoreType:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    return-object v0
.end method

.method public getVarianceText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->varianceText:Ljava/lang/String;

    return-object v0
.end method

.method public getVarianceValue()I
    .locals 1

    .prologue
    .line 209
    iget v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->varianceValue:I

    return v0
.end method

.method public getcoachMessage()Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

    return-object v0
.end method

.method public setCoachMessage(Lcom/cigna/coach/apiobjects/CoachMessage;)V
    .locals 0
    .param p1, "coachMessage"    # Lcom/cigna/coach/apiobjects/CoachMessage;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->coachMessage:Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 129
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 164
    iput p1, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->score:I

    .line 165
    return-void
.end method

.method public setScoreAssignmentDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "scoreAssignmentDate"    # Ljava/util/Calendar;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->scoreAssignmentDate:Ljava/util/Calendar;

    .line 147
    return-void
.end method

.method public setScoreType(Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)V
    .locals 0
    .param p1, "scoreType"    # Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->scoreType:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .line 183
    return-void
.end method

.method public setVarianceText(Ljava/lang/String;)V
    .locals 0
    .param p1, "varianceText"    # Ljava/lang/String;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->varianceText:Ljava/lang/String;

    .line 201
    return-void
.end method

.method public setVarianceValue(I)V
    .locals 0
    .param p1, "varianceValue"    # I

    .prologue
    .line 218
    iput p1, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores;->varianceValue:I

    .line 219
    return-void
.end method
