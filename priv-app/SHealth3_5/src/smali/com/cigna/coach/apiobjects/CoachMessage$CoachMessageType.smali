.class public final enum Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
.super Ljava/lang/Enum;
.source "CoachMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/CoachMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoachMessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum CATEGORY_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum DUE_MISSIONS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum INSTRUCTIONAL_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum OUT_OF_APPLICATION_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum REASSESS_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum REASSESS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum REASSESS_WITH_GOALS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field public static final enum TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field messageType:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 50
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "CANCEL_UNALIGNED_GOALS"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 51
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "CATEGORY_SPECIFIC_MESSAGE"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CATEGORY_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 52
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "COACH_MESSAGE"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 53
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "DUE_MISSIONS"

    invoke-direct {v4, v5, v10, v11}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->DUE_MISSIONS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 54
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "GOAL_SPECIFIC_MESSAGE"

    invoke-direct {v4, v5, v11, v12}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 55
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "REASSESS_MESSAGE"

    const/4 v6, 0x6

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 56
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "REASSESS_WARNING"

    const/4 v6, 0x6

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 57
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "TIMELINE"

    const/4 v6, 0x7

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 58
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "OUT_OF_APPLICATION_NOTIFICATION"

    const/16 v6, 0x8

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->OUT_OF_APPLICATION_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 59
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "INSTRUCTIONAL_MESSAGE"

    const/16 v6, 0x9

    const/16 v7, 0xa

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->INSTRUCTIONAL_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 60
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "REASSESS_WITH_GOALS_WARNING"

    const/16 v6, 0xa

    const/16 v7, 0xb

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WITH_GOALS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 61
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "COACH_WIDGET_NOTIFICATION"

    const/16 v6, 0xb

    const/16 v7, 0xc

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 62
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const-string v5, "SHEALTH_WIDGET_NOTIFICATION"

    const/16 v6, 0xc

    const/16 v7, 0xd

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 49
    const/16 v4, 0xd

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CATEGORY_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->DUE_MISSIONS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->OUT_OF_APPLICATION_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->INSTRUCTIONAL_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WITH_GOALS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->$VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 74
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->codeValueMap:Ljava/util/HashMap;

    .line 76
    invoke-static {}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->values()[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 77
    .local v3, "type":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "mType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 69
    iput p3, p0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->messageType:I

    .line 70
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .locals 2
    .param p0, "coachMessageTypeKey"    # I

    .prologue
    .line 88
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->$VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    return-object v0
.end method


# virtual methods
.method public getMessageType()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->messageType:I

    return v0
.end method
