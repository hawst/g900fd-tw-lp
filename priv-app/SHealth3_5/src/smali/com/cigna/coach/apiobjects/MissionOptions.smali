.class public Lcom/cigna/coach/apiobjects/MissionOptions;
.super Ljava/lang/Object;
.source "MissionOptions.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private missionStartDate:Ljava/util/Calendar;

.field private missionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field private selectedFrequency:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method


# virtual methods
.method public getMissionStartDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/MissionOptions;->missionStartDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getMissionStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/MissionOptions;->missionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    return-object v0
.end method

.method public getSelectedFrequency()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/cigna/coach/apiobjects/MissionOptions;->selectedFrequency:I

    return v0
.end method

.method public setMissionStartDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "missionStartDate"    # Ljava/util/Calendar;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/MissionOptions;->missionStartDate:Ljava/util/Calendar;

    .line 68
    return-void
.end method

.method public setMissionStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V
    .locals 0
    .param p1, "missionStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/MissionOptions;->missionStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 87
    return-void
.end method

.method public setSelectedFrequency(I)V
    .locals 0
    .param p1, "selectedFrequency"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/cigna/coach/apiobjects/MissionOptions;->selectedFrequency:I

    .line 105
    return-void
.end method
