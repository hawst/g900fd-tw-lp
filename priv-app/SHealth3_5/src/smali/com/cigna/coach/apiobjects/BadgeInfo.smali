.class public Lcom/cigna/coach/apiobjects/BadgeInfo;
.super Ljava/lang/Object;
.source "BadgeInfo.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private availableBadges:I

.field private badges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation
.end field

.field private earnedBadges:I

.field private remainingBadges:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v0, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->availableBadges:I

    .line 46
    iput v0, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->earnedBadges:I

    .line 49
    iput v0, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->remainingBadges:I

    .line 56
    return-void
.end method


# virtual methods
.method public getAvailableBadges()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->availableBadges:I

    return v0
.end method

.method public getBadges()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->badges:Ljava/util/List;

    return-object v0
.end method

.method public getEarnedBadges()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->earnedBadges:I

    return v0
.end method

.method public getRemainingBadges()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->remainingBadges:I

    return v0
.end method

.method public setAvailableBadges(I)V
    .locals 0
    .param p1, "availableBadges"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->availableBadges:I

    .line 76
    return-void
.end method

.method public setBadges(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->badges:Ljava/util/List;

    .line 137
    return-void
.end method

.method public setEarnedBadges(I)V
    .locals 0
    .param p1, "earnedBadges"    # I

    .prologue
    .line 95
    iput p1, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->earnedBadges:I

    .line 96
    return-void
.end method

.method public setRemainingBadges(I)V
    .locals 0
    .param p1, "remainingBadges"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/cigna/coach/apiobjects/BadgeInfo;->remainingBadges:I

    .line 116
    return-void
.end method
