.class public Lcom/cigna/coach/apiobjects/MissionDetails;
.super Ljava/lang/Object;
.source "MissionDetails.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private exerciseIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private missionActivityCompletedDate:Ljava/util/Calendar;

.field private noOfActivities:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method


# virtual methods
.method public getExerciseId()I
    .locals 3

    .prologue
    .line 69
    iget-object v1, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->exerciseIds:Ljava/util/List;

    if-nez v1, :cond_1

    .line 70
    const/4 v0, -0x1

    .line 77
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    const/4 v0, -0x1

    .line 73
    .local v0, "exerciseId":I
    iget-object v1, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->exerciseIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->exerciseIds:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public getExerciseIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->exerciseIds:Ljava/util/List;

    return-object v0
.end method

.method public getMissionActivityCompletedDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->missionActivityCompletedDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getNoOfActivities()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->noOfActivities:I

    return v0
.end method

.method public setMissionActivityCompletedDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "missionActivityCompletedDate"    # Ljava/util/Calendar;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->missionActivityCompletedDate:Ljava/util/Calendar;

    .line 105
    return-void
.end method

.method public setNoOfActivities(I)V
    .locals 0
    .param p1, "noOfActivities"    # I

    .prologue
    .line 121
    iput p1, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->noOfActivities:I

    .line 122
    return-void
.end method

.method public setexerciseIds(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p1, "exerciseIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/MissionDetails;->exerciseIds:Ljava/util/List;

    .line 96
    return-void
.end method
