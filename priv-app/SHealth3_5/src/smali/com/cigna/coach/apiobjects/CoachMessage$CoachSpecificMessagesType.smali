.class public final enum Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
.super Ljava/lang/Enum;
.source "CoachMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/CoachMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoachSpecificMessagesType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

.field public static final enum COACH_MESSAGE_LIFESTYLE_REASSESSMENT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field specificMessagesType:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 134
    new-instance v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    const-string v5, "COACH_MESSAGE_LIFESTYLE_REASSESSMENT"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->COACH_MESSAGE_LIFESTYLE_REASSESSMENT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    .line 133
    new-array v4, v7, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->COACH_MESSAGE_LIFESTYLE_REASSESSMENT:Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    aput-object v5, v4, v6

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->$VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    .line 146
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->codeValueMap:Ljava/util/HashMap;

    .line 148
    invoke-static {}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->values()[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 149
    .local v3, "type":Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->getMessagesType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 151
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "mType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 141
    iput p3, p0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->specificMessagesType:I

    .line 142
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
    .locals 2
    .param p0, "coachSpecificMessagesTypeKey"    # I

    .prologue
    .line 160
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 133
    const-class v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
    .locals 1

    .prologue
    .line 133
    sget-object v0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->$VALUES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;

    return-object v0
.end method


# virtual methods
.method public getMessagesType()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->specificMessagesType:I

    return v0
.end method
