.class public Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
.super Ljava/lang/Object;
.source "QuestionsAnswersGroup.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private questionAnswers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionAnswers;",
            ">;"
        }
    .end annotation
.end field

.field private questionGroupId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->questionGroupId:I

    .line 46
    return-void
.end method


# virtual methods
.method public getQuestionAnswer()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionAnswers;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->questionAnswers:Ljava/util/List;

    return-object v0
.end method

.method public getQuestionGroupId()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->questionGroupId:I

    return v0
.end method

.method public setAnswer(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionAnswers;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "questionAnswers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->questionAnswers:Ljava/util/List;

    .line 67
    return-void
.end method

.method public setQuestionGroupId(I)V
    .locals 0
    .param p1, "questionGroupId"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->questionGroupId:I

    .line 87
    return-void
.end method
