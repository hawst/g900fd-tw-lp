.class public Lcom/cigna/coach/apiobjects/Answer;
.super Ljava/lang/Object;
.source "Answer.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private answer:Ljava/lang/String;

.field private answerId:I

.field private answerSummary:Ljava/lang/String;

.field private answerType:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

.field private isPreselected:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/Answer;->answer:Ljava/lang/String;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/cigna/coach/apiobjects/Answer;->answerId:I

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/Answer;->answerSummary:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public getAnswer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Answer;->answer:Ljava/lang/String;

    return-object v0
.end method

.method public getAnswerId()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/cigna/coach/apiobjects/Answer;->answerId:I

    return v0
.end method

.method public getAnswerSummary()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Answer;->answerSummary:Ljava/lang/String;

    return-object v0
.end method

.method public getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Answer;->answerType:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    return-object v0
.end method

.method public isPreselected()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/cigna/coach/apiobjects/Answer;->isPreselected:Z

    return v0
.end method

.method public setAnswer(Ljava/lang/String;)V
    .locals 0
    .param p1, "answer"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Answer;->answer:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setAnswerId(I)V
    .locals 0
    .param p1, "answerId"    # I

    .prologue
    .line 94
    iput p1, p0, Lcom/cigna/coach/apiobjects/Answer;->answerId:I

    .line 95
    return-void
.end method

.method public setAnswerSummary(Ljava/lang/String;)V
    .locals 0
    .param p1, "answerSummary"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Answer;->answerSummary:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setAnswerType(Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;)V
    .locals 0
    .param p1, "answerType"    # Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Answer;->answerType:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    .line 115
    return-void
.end method

.method public setPreselected(Z)V
    .locals 0
    .param p1, "isPreselected"    # Z

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/cigna/coach/apiobjects/Answer;->isPreselected:Z

    .line 153
    return-void
.end method
