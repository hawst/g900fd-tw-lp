.class public Lcom/cigna/coach/apiobjects/MissionTip;
.super Ljava/lang/Object;
.source "MissionTip.java"


# instance fields
.field private tip:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/MissionTip;->tip:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/MissionTip;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setTip(Ljava/lang/String;)V
    .locals 0
    .param p1, "tip"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/MissionTip;->tip:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/MissionTip;->title:Ljava/lang/String;

    .line 51
    return-void
.end method
