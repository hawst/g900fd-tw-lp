.class public Lcom/cigna/coach/apiobjects/ScoreInfo;
.super Ljava/lang/Object;
.source "ScoreInfo.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private isTimeToReassess:Z

.field private lastUpdateMessage:Ljava/lang/String;

.field private lssList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation
.end field

.field private outOfMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->isTimeToReassess:Z

    .line 62
    return-void
.end method


# virtual methods
.method public final getLastUpdateMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->lastUpdateMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getLssList()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->lssList:Ljava/util/List;

    return-object v0
.end method

.method public getOutOfMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->outOfMessage:Ljava/lang/String;

    return-object v0
.end method

.method public isTimeToReassess()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->isTimeToReassess:Z

    return v0
.end method

.method public setLastUpdateMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "lastUpdateMessage"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->lastUpdateMessage:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setLssList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "lssList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->lssList:Ljava/util/List;

    .line 117
    return-void
.end method

.method public setOutOfMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "outOfMessage"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->outOfMessage:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setTimeToReassess(Z)V
    .locals 0
    .param p1, "isTimeToReassess"    # Z

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/cigna/coach/apiobjects/ScoreInfo;->isTimeToReassess:Z

    .line 80
    return-void
.end method
