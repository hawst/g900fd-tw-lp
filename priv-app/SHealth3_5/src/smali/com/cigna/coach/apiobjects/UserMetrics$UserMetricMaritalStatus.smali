.class public final enum Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
.super Ljava/lang/Enum;
.source "UserMetrics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/UserMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserMetricMaritalStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

.field public static final enum DIVORCED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

.field public static final enum MARRIED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

.field public static final enum SINGLE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

.field public static final enum UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

.field public static final enum WIDOWED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field maritalTypeKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 112
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    const-string v5, "SINGLE"

    invoke-direct {v4, v5, v11, v7}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->SINGLE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .line 113
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    const-string v5, "MARRIED"

    invoke-direct {v4, v5, v7, v8}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->MARRIED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .line 114
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    const-string v5, "DIVORCED"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->DIVORCED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .line 115
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    const-string v5, "WIDOWED"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->WIDOWED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .line 116
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    const-string v5, "UNKNOWN"

    const/4 v6, 0x5

    invoke-direct {v4, v5, v10, v6}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .line 111
    const/4 v4, 0x5

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->SINGLE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->MARRIED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->DIVORCED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->WIDOWED:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    aput-object v5, v4, v10

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .line 129
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->codeValueMap:Ljava/util/HashMap;

    .line 131
    invoke-static {}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 132
    .local v3, "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    sget-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->getMaritalStatusKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 134
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "maritalTypeKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 121
    iput p3, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->maritalTypeKey:I

    .line 122
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    .locals 2
    .param p0, "genderTypeKey"    # I

    .prologue
    .line 137
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 111
    const-class v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    return-object v0
.end method


# virtual methods
.method public getMaritalStatusKey()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->maritalTypeKey:I

    return v0
.end method
