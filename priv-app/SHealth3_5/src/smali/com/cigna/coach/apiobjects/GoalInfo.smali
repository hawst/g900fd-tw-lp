.class public Lcom/cigna/coach/apiobjects/GoalInfo;
.super Ljava/lang/Object;
.source "GoalInfo.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private categoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private completedDate:Ljava/util/Calendar;

.field private currentGoalStatus:Ljava/lang/String;

.field private goal:Ljava/lang/String;

.field private goalFrequency:Ljava/lang/String;

.field private goalId:I

.field private goalImage:Ljava/lang/String;

.field private isGoalOnTrack:Z

.field private questionsAnswerGroup:Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

.field private whyDoIt:Ljava/lang/String;

.field private whyDoItImage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->isGoalOnTrack:Z

    .line 87
    return-void
.end method


# virtual methods
.method public getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->categoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public getCompletedDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->completedDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getCurrentGoalStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->currentGoalStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getGoal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goal:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalFrequency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goalFrequency:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalId()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goalId:I

    return v0
.end method

.method public getGoalImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goalImage:Ljava/lang/String;

    return-object v0
.end method

.method public getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .prologue
    .line 211
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->questionsAnswerGroup:Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    return-object v0
.end method

.method public getWhyDoIt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->whyDoIt:Ljava/lang/String;

    return-object v0
.end method

.method public getWhyDoItImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->whyDoItImage:Ljava/lang/String;

    return-object v0
.end method

.method public isGoalOnTrack()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->isGoalOnTrack:Z

    return v0
.end method

.method public setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 0
    .param p1, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->categoryType:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 106
    return-void
.end method

.method public setCompletedDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "completedDate"    # Ljava/util/Calendar;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->completedDate:Ljava/util/Calendar;

    .line 302
    return-void
.end method

.method public setCurrentGoalStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentGoalStatus"    # Ljava/lang/String;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->currentGoalStatus:Ljava/lang/String;

    .line 282
    return-void
.end method

.method public setGoal(Ljava/lang/String;)V
    .locals 0
    .param p1, "goal"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goal:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setGoalFrequency(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalFrequency"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goalFrequency:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setGoalId(I)V
    .locals 0
    .param p1, "goalId"    # I

    .prologue
    .line 125
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goalId:I

    .line 126
    return-void
.end method

.method public setGoalImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalImage"    # Ljava/lang/String;

    .prologue
    .line 183
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->goalImage:Ljava/lang/String;

    .line 184
    return-void
.end method

.method public setGoalOnTrack(Z)V
    .locals 0
    .param p1, "isGoalOnTrack"    # Z

    .prologue
    .line 201
    iput-boolean p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->isGoalOnTrack:Z

    .line 202
    return-void
.end method

.method public setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V
    .locals 0
    .param p1, "questionsAnswerGroup"    # Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->questionsAnswerGroup:Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    .line 222
    return-void
.end method

.method public setWhyDoIt(Ljava/lang/String;)V
    .locals 0
    .param p1, "whyDoIt"    # Ljava/lang/String;

    .prologue
    .line 241
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->whyDoIt:Ljava/lang/String;

    .line 242
    return-void
.end method

.method public setWhyDoItImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "whyDoItImage"    # Ljava/lang/String;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalInfo;->whyDoItImage:Ljava/lang/String;

    .line 262
    return-void
.end method
