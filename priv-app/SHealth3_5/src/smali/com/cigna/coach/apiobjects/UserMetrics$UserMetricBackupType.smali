.class public final enum Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
.super Ljava/lang/Enum;
.source "UserMetrics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/UserMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserMetricBackupType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

.field public static final enum FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

.field public static final enum INCREMENTAL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field backupTypeKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 143
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    const-string v5, "INCREMENTAL"

    invoke-direct {v4, v5, v6, v6}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->INCREMENTAL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .line 144
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    const-string v5, "FULL"

    invoke-direct {v4, v5, v7, v7}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .line 142
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->INCREMENTAL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    aput-object v5, v4, v7

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .line 157
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->codeValueMap:Ljava/util/HashMap;

    .line 159
    invoke-static {}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 160
    .local v3, "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    sget-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->getBackupTypeKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "backupTypeKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    iput p3, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->backupTypeKey:I

    .line 150
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .locals 2
    .param p0, "genderTypeKey"    # I

    .prologue
    .line 165
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    const-class v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    return-object v0
.end method


# virtual methods
.method public getBackupTypeKey()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->backupTypeKey:I

    return v0
.end method
