.class public Lcom/cigna/coach/apiobjects/UserMetrics;
.super Ljava/util/Hashtable;
.source "UserMetrics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;,
        Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;,
        Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;,
        Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/Hashtable",
        "<",
        "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field protected static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/util/Hashtable;-><init>()V

    .line 173
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    return-void
.end method


# virtual methods
.method public getCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->COUNTRY:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0}, Lcom/cigna/coach/apiobjects/UserMetrics;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDateOfBirth()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 210
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->DATE_OF_BIRTH:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0}, Lcom/cigna/coach/apiobjects/UserMetrics;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    return-object v0
.end method

.method public getGender()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    .locals 1

    .prologue
    .line 228
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0}, Lcom/cigna/coach/apiobjects/UserMetrics;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    return-object v0
.end method

.method public getHeight()F
    .locals 2

    .prologue
    .line 265
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 266
    .local v0, "height":Ljava/lang/Float;
    if-nez v0, :cond_0

    .line 267
    const/4 v1, 0x0

    .line 269
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    goto :goto_0
.end method

.method public getMaritalStatus()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0}, Lcom/cigna/coach/apiobjects/UserMetrics;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    return-object v0
.end method

.method public getWeight()F
    .locals 2

    .prologue
    .line 289
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->WEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 290
    .local v0, "weight":Ljava/lang/Float;
    if-nez v0, :cond_0

    .line 291
    const/4 v1, 0x0

    .line 293
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    goto :goto_0
.end method

.method public setCountry(Ljava/lang/String;)V
    .locals 1
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 183
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->COUNTRY:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0, p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    return-void
.end method

.method public setDateOfBirth(Ljava/util/Calendar;)V
    .locals 1
    .param p1, "dob"    # Ljava/util/Calendar;

    .prologue
    .line 201
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->DATE_OF_BIRTH:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0, p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    return-void
.end method

.method public setGender(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;)V
    .locals 1
    .param p1, "gender"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .prologue
    .line 219
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0, p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    return-void
.end method

.method public setHeight(F)V
    .locals 2
    .param p1, "height"    # F

    .prologue
    .line 256
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    return-void
.end method

.method public setMaritalStatus(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;)V
    .locals 1
    .param p1, "maritalStatus"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .prologue
    .line 237
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, v0, p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    return-void
.end method

.method public setWeight(F)V
    .locals 2
    .param p1, "weight"    # F

    .prologue
    .line 280
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->WEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    return-void
.end method
