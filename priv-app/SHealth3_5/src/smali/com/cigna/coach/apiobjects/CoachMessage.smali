.class public Lcom/cigna/coach/apiobjects/CoachMessage;
.super Ljava/lang/Object;
.source "CoachMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;,
        Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;,
        Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    }
.end annotation


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private message:Ljava/lang/String;

.field private messageDescription:Ljava/lang/String;

.field private messageImage:Ljava/lang/String;

.field private messageType:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->messageDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->messageImage:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->messageType:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    return-object v0
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->message:Ljava/lang/String;

    .line 201
    return-void
.end method

.method public setMessageDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageDescription"    # Ljava/lang/String;

    .prologue
    .line 218
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->messageDescription:Ljava/lang/String;

    .line 219
    return-void
.end method

.method public setMessageImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageImage"    # Ljava/lang/String;

    .prologue
    .line 236
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->messageImage:Ljava/lang/String;

    .line 237
    return-void
.end method

.method public setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V
    .locals 0
    .param p1, "messageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/CoachMessage;->messageType:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 255
    return-void
.end method
