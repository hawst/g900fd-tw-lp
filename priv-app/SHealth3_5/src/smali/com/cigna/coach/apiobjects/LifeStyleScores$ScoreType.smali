.class public final enum Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
.super Ljava/lang/Enum;
.source "LifeStyleScores.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/LifeStyleScores;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScoreType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

.field public static final enum CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

.field public static final enum CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

.field public static final enum CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

.field public static final enum LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field scoreType:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x0

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 49
    new-instance v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    const-string v5, "LIFESTYLE_OVERALL"

    invoke-direct {v4, v5, v9, v6}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .line 52
    new-instance v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    const-string v5, "CATEGORY_LIFESTYLE"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .line 55
    new-instance v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    const-string v5, "CATEGORY_CURRENT"

    invoke-direct {v4, v5, v7, v8}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .line 58
    new-instance v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    const-string v5, "CURRENT_OVERALL"

    invoke-direct {v4, v5, v8, v10}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .line 46
    new-array v4, v10, [Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    sget-object v5, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    aput-object v5, v4, v8

    sput-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->$VALUES:[Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .line 70
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->codeValueMap:Ljava/util/HashMap;

    .line 72
    invoke-static {}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->values()[Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 73
    .local v3, "type":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    sget-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "sType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->scoreType:I

    .line 66
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    .locals 2
    .param p0, "scoreTypeKey"    # I

    .prologue
    .line 84
    sget-object v0, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v0, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->$VALUES:[Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    return-object v0
.end method


# virtual methods
.method public getScoreType()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->scoreType:I

    return v0
.end method
