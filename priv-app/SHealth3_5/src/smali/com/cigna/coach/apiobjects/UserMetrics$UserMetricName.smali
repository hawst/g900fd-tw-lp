.class public final enum Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
.super Ljava/lang/Enum;
.source "UserMetrics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/UserMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UserMetricName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum BACKUP_TYPE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum COUNTRY:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum DATE_OF_BIRTH:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum HAS_SEEN_WELCOME_SCREEN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum HEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field public static final enum WEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field metricNameKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 47
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "COUNTRY"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->COUNTRY:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 48
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "DATE_OF_BIRTH"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->DATE_OF_BIRTH:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 49
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "GENDER"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 50
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "HEIGHT"

    invoke-direct {v4, v5, v10, v11}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 51
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "MARITAL_STATUS"

    invoke-direct {v4, v5, v11, v12}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 52
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "WEIGHT"

    const/4 v6, 0x6

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->WEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 53
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "BACKUP_TYPE"

    const/4 v6, 0x6

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->BACKUP_TYPE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 54
    new-instance v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const-string v5, "HAS_SEEN_WELCOME_SCREEN"

    const/4 v6, 0x7

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HAS_SEEN_WELCOME_SCREEN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 46
    const/16 v4, 0x8

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->COUNTRY:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->DATE_OF_BIRTH:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->WEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->BACKUP_TYPE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HAS_SEEN_WELCOME_SCREEN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .line 68
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->codeValueMap:Ljava/util/HashMap;

    .line 70
    invoke-static {}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 71
    .local v3, "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    sget-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->getMetricNameKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 73
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "metricNameKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 60
    iput p3, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->metricNameKey:I

    .line 61
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    .locals 2
    .param p0, "metricNameKey"    # I

    .prologue
    .line 76
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    const-class v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->$VALUES:[Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    return-object v0
.end method


# virtual methods
.method public getMetricNameKey()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->metricNameKey:I

    return v0
.end method
