.class public final enum Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
.super Ljava/lang/Enum;
.source "GoalMissionInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/GoalMissionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpecificTracker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum AQUA_AEROBICS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum ARCHERY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum BADMITON:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum BASEBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum BASKETBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum BICYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum BILLIARDS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum BOWLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum BOXING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum DANCE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum DIVING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum FIELD_HOCKEY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum FOOTBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum GENERIC_WALKING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum GOLF:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum HANDBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum HANG_GLIDE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum HIKE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum HORSEBACK:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum HULA_HOOPING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum ICE_HOCKEY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum ICE_SKATING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum IN_LINE_SKATING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum JUDO:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum LEVEL_CYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum NOT_DEFINED:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum PILATES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum PISTOL_SHOOTING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum PRESS_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum PULL_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum PUSH_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum ROCK_CLIMB:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum ROWING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum RUGBY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum RUNNING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SAILING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SIT_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SKI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SKIP_ROPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SNORKELING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SOCCER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SOFTBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SPEED_WALK:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SQUASH:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum STAIRS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum STEEP_CYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum STRETCHING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum SWIM:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum TABLE_TENNIS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum TAEKWONDO:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum TAI_CHI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum TENNIS_DOUBLES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum TENNIS_SINGLES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum VOLLEYBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum WALKING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum WATER_SKI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum WEIGHT_LIFTING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field public static final enum YOGA:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field spTrackerKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 96
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "NOT_DEFINED"

    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->NOT_DEFINED:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 97
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "AQUA_AEROBICS"

    invoke-direct {v4, v5, v8, v8}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->AQUA_AEROBICS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 98
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "BADMITON"

    invoke-direct {v4, v5, v9, v9}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BADMITON:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 99
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "BASEBALL"

    invoke-direct {v4, v5, v10, v10}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BASEBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 100
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "BASKETBALL"

    invoke-direct {v4, v5, v11, v11}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BASKETBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 101
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "BILLIARDS"

    invoke-direct {v4, v5, v12, v12}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BILLIARDS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 102
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "BOWLING"

    const/4 v6, 0x6

    const/4 v7, 0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BOWLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 103
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "BOXING"

    const/4 v6, 0x7

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BOXING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 104
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "DANCE"

    const/16 v6, 0x8

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->DANCE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 105
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "FOOTBALL"

    const/16 v6, 0x9

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->FOOTBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 106
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "GOLF"

    const/16 v6, 0xa

    const/16 v7, 0xa

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->GOLF:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 107
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "HANDBALL"

    const/16 v6, 0xb

    const/16 v7, 0xb

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HANDBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 108
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "HANG_GLIDE"

    const/16 v6, 0xc

    const/16 v7, 0xc

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HANG_GLIDE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 109
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "HIKE"

    const/16 v6, 0xd

    const/16 v7, 0xd

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HIKE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 110
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "HORSEBACK"

    const/16 v6, 0xe

    const/16 v7, 0xe

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HORSEBACK:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 111
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "HULA_HOOPING"

    const/16 v6, 0xf

    const/16 v7, 0xf

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HULA_HOOPING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 112
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "ICE_SKATING"

    const/16 v6, 0x10

    const/16 v7, 0x10

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ICE_SKATING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 113
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "IN_LINE_SKATING"

    const/16 v6, 0x11

    const/16 v7, 0x11

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->IN_LINE_SKATING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 114
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "JUDO"

    const/16 v6, 0x12

    const/16 v7, 0x12

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->JUDO:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 115
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "LEVEL_CYCLING"

    const/16 v6, 0x13

    const/16 v7, 0x13

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->LEVEL_CYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 116
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "PRESS_UPS"

    const/16 v6, 0x14

    const/16 v7, 0x14

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PRESS_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 117
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "PULL_UPS"

    const/16 v6, 0x15

    const/16 v7, 0x15

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PULL_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 118
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "PUSH_UPS"

    const/16 v6, 0x16

    const/16 v7, 0x16

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PUSH_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 119
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "ROCK_CLIMB"

    const/16 v6, 0x17

    const/16 v7, 0x17

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ROCK_CLIMB:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 120
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SIT_UPS"

    const/16 v6, 0x18

    const/16 v7, 0x18

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SIT_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 121
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SKI"

    const/16 v6, 0x19

    const/16 v7, 0x19

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SKI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 122
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SKIP_ROPE"

    const/16 v6, 0x1a

    const/16 v7, 0x1a

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SKIP_ROPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 123
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SOFTBALL"

    const/16 v6, 0x1b

    const/16 v7, 0x1b

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SOFTBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 124
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SPEED_WALK"

    const/16 v6, 0x1c

    const/16 v7, 0x1c

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SPEED_WALK:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 125
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SQUASH"

    const/16 v6, 0x1d

    const/16 v7, 0x1d

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SQUASH:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 126
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "STAIRS"

    const/16 v6, 0x1e

    const/16 v7, 0x1e

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->STAIRS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 127
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "STEEP_CYCLING"

    const/16 v6, 0x1f

    const/16 v7, 0x1f

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->STEEP_CYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 128
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SWIM"

    const/16 v6, 0x20

    const/16 v7, 0x20

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SWIM:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 129
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "TABLE_TENNIS"

    const/16 v6, 0x21

    const/16 v7, 0x21

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TABLE_TENNIS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 130
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "TAEKWONDO"

    const/16 v6, 0x22

    const/16 v7, 0x22

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TAEKWONDO:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 131
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "TAI_CHI"

    const/16 v6, 0x23

    const/16 v7, 0x23

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TAI_CHI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 132
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "TENNIS_DOUBLES"

    const/16 v6, 0x24

    const/16 v7, 0x24

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TENNIS_DOUBLES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 133
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "TENNIS_SINGLES"

    const/16 v6, 0x25

    const/16 v7, 0x25

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TENNIS_SINGLES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 134
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "VOLLEYBALL"

    const/16 v6, 0x26

    const/16 v7, 0x26

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->VOLLEYBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 135
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "WATER_SKI"

    const/16 v6, 0x27

    const/16 v7, 0x27

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->WATER_SKI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 136
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "YOGA"

    const/16 v6, 0x28

    const/16 v7, 0x28

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->YOGA:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 137
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "WALKING"

    const/16 v6, 0x29

    const/16 v7, 0x29

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->WALKING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 138
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "RUNNING"

    const/16 v6, 0x2a

    const/16 v7, 0x2a

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->RUNNING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 139
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "PILATES"

    const/16 v6, 0x2b

    const/16 v7, 0x2b

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PILATES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 140
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "STRETCHING"

    const/16 v6, 0x2c

    const/16 v7, 0x2c

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->STRETCHING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 141
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "PISTOL_SHOOTING"

    const/16 v6, 0x2d

    const/16 v7, 0x2d

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PISTOL_SHOOTING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 142
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "ARCHERY"

    const/16 v6, 0x2e

    const/16 v7, 0x2e

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ARCHERY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 143
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "FIELD_HOCKEY"

    const/16 v6, 0x2f

    const/16 v7, 0x2f

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->FIELD_HOCKEY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 144
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "ICE_HOCKEY"

    const/16 v6, 0x30

    const/16 v7, 0x30

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ICE_HOCKEY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 145
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "RUGBY"

    const/16 v6, 0x31

    const/16 v7, 0x31

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->RUGBY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 146
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SOCCER"

    const/16 v6, 0x32

    const/16 v7, 0x32

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SOCCER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 147
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "WEIGHT_LIFTING"

    const/16 v6, 0x33

    const/16 v7, 0x33

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->WEIGHT_LIFTING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 148
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "ROWING"

    const/16 v6, 0x34

    const/16 v7, 0x34

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ROWING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 149
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SAILING"

    const/16 v6, 0x35

    const/16 v7, 0x35

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SAILING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 150
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "DIVING"

    const/16 v6, 0x36

    const/16 v7, 0x36

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->DIVING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 151
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "SNORKELING"

    const/16 v6, 0x37

    const/16 v7, 0x37

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SNORKELING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 152
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "BICYCLING"

    const/16 v6, 0x38

    const/16 v7, 0x38

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BICYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 153
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const-string v5, "GENERIC_WALKING"

    const/16 v6, 0x39

    const/16 v7, 0x39

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->GENERIC_WALKING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 95
    const/16 v4, 0x3a

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->NOT_DEFINED:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->AQUA_AEROBICS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BADMITON:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BASEBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BASKETBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BILLIARDS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BOWLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BOXING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->DANCE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->FOOTBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->GOLF:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HANDBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HANG_GLIDE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HIKE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0xe

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HORSEBACK:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0xf

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->HULA_HOOPING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x10

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ICE_SKATING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x11

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->IN_LINE_SKATING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x12

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->JUDO:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x13

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->LEVEL_CYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x14

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PRESS_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x15

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PULL_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x16

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PUSH_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x17

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ROCK_CLIMB:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x18

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SIT_UPS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x19

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SKI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x1a

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SKIP_ROPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x1b

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SOFTBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x1c

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SPEED_WALK:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x1d

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SQUASH:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x1e

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->STAIRS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x1f

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->STEEP_CYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x20

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SWIM:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x21

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TABLE_TENNIS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x22

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TAEKWONDO:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x23

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TAI_CHI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x24

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TENNIS_DOUBLES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x25

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->TENNIS_SINGLES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x26

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->VOLLEYBALL:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x27

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->WATER_SKI:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x28

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->YOGA:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x29

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->WALKING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x2a

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->RUNNING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x2b

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PILATES:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x2c

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->STRETCHING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x2d

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->PISTOL_SHOOTING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x2e

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ARCHERY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x2f

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->FIELD_HOCKEY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x30

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ICE_HOCKEY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x31

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->RUGBY:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x32

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SOCCER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x33

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->WEIGHT_LIFTING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x34

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->ROWING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x35

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SAILING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x36

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->DIVING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x37

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->SNORKELING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x38

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->BICYCLING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    const/16 v5, 0x39

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->GENERIC_WALKING:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->$VALUES:[Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 170
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->codeValueMap:Ljava/util/HashMap;

    .line 172
    invoke-static {}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->values()[Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 173
    .local v3, "type":Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    sget-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->getIntValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 175
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "spTrackerKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 160
    iput p3, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->spTrackerKey:I

    .line 161
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    .locals 2
    .param p0, "statusKey"    # I

    .prologue
    .line 179
    sget-object v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 95
    const-class v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->$VALUES:[Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->spTrackerKey:I

    return v0
.end method
