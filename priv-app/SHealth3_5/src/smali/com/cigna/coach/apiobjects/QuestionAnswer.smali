.class public Lcom/cigna/coach/apiobjects/QuestionAnswer;
.super Ljava/lang/Object;
.source "QuestionAnswer.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private answer:Lcom/cigna/coach/apiobjects/Answer;

.field private questionId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswer;->questionId:I

    .line 45
    return-void
.end method


# virtual methods
.method public getAnswer()Lcom/cigna/coach/apiobjects/Answer;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswer;->answer:Lcom/cigna/coach/apiobjects/Answer;

    return-object v0
.end method

.method public getQuestionId()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/cigna/coach/apiobjects/QuestionAnswer;->questionId:I

    return v0
.end method

.method public setAnswer(Lcom/cigna/coach/apiobjects/Answer;)V
    .locals 0
    .param p1, "answer"    # Lcom/cigna/coach/apiobjects/Answer;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/QuestionAnswer;->answer:Lcom/cigna/coach/apiobjects/Answer;

    .line 66
    return-void
.end method

.method public setQuestionId(I)V
    .locals 0
    .param p1, "questionId"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/cigna/coach/apiobjects/QuestionAnswer;->questionId:I

    .line 85
    return-void
.end method
