.class public Lcom/cigna/coach/apiobjects/GoalMissionInfo;
.super Ljava/lang/Object;
.source "GoalMissionInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;,
        Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    }
.end annotation


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private currentMissionStatus:Ljava/lang/String;

.field private frequencyDefault:Ljava/lang/String;

.field private frequencyDefaultId:I

.field private frequencyMax:I

.field private frequencyMin:I

.field private goalId:I

.field private isTrackerMission:Z

.field private mission:Ljava/lang/String;

.field private missionCompletedDate:Ljava/util/Calendar;

.field private missionDescription:Ljava/lang/String;

.field private missionId:I

.field private missionImage:Ljava/lang/String;

.field private missionSeqId:I

.field private specificTracker:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

.field private trackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field private whatToDo:Ljava/lang/String;

.field private whatToDoImage:Ljava/lang/String;

.field private whyDoIt:Ljava/lang/String;

.field private whyDoItImage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->isTrackerMission:Z

    .line 221
    sget-object v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->NOT_TRACKER_TYPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->trackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 224
    sget-object v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->NOT_DEFINED:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->specificTracker:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 248
    return-void
.end method


# virtual methods
.method public getCurrentMissionStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->currentMissionStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getFrequencyDefault()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyDefault:Ljava/lang/String;

    return-object v0
.end method

.method public getFrequencyDefaultId()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyDefaultId:I

    return v0
.end method

.method public getFrequencyMax()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyMax:I

    return v0
.end method

.method public getFrequencyMin()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyMin:I

    return v0
.end method

.method public getGoalId()I
    .locals 1

    .prologue
    .line 336
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->goalId:I

    return v0
.end method

.method public getMission()Ljava/lang/String;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->mission:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionCompletedDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionCompletedDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getMissionDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionId()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionId:I

    return v0
.end method

.method public getMissionImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionImage:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionSeqId()I
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionSeqId:I

    return v0
.end method

.method public getSpecificTracker()Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->specificTracker:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    return-object v0
.end method

.method public getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->trackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    return-object v0
.end method

.method public getWhatToDo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whatToDo:Ljava/lang/String;

    return-object v0
.end method

.method public getWhatToDoImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whatToDoImage:Ljava/lang/String;

    return-object v0
.end method

.method public getWhyDoIt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whyDoIt:Ljava/lang/String;

    return-object v0
.end method

.method public getWhyDoItImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whyDoItImage:Ljava/lang/String;

    return-object v0
.end method

.method public isTrackerMission()Z
    .locals 1

    .prologue
    .line 570
    iget-boolean v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->isTrackerMission:Z

    return v0
.end method

.method public setCurrentMissionStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentMissionStatus"    # Ljava/lang/String;

    .prologue
    .line 599
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->currentMissionStatus:Ljava/lang/String;

    .line 600
    return-void
.end method

.method public setFrequencyDefault(Ljava/lang/String;)V
    .locals 0
    .param p1, "frequencyDefault"    # Ljava/lang/String;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyDefault:Ljava/lang/String;

    .line 268
    return-void
.end method

.method public setFrequencyDefaultId(I)V
    .locals 0
    .param p1, "frequencyDefaultId"    # I

    .prologue
    .line 287
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyDefaultId:I

    .line 288
    return-void
.end method

.method public setFrequencyMax(I)V
    .locals 0
    .param p1, "frequencyMax"    # I

    .prologue
    .line 307
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyMax:I

    .line 308
    return-void
.end method

.method public setFrequencyMin(I)V
    .locals 0
    .param p1, "frequencyMin"    # I

    .prologue
    .line 327
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->frequencyMin:I

    .line 328
    return-void
.end method

.method public setGoalId(I)V
    .locals 0
    .param p1, "goalId"    # I

    .prologue
    .line 346
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->goalId:I

    .line 347
    return-void
.end method

.method public setMission(Ljava/lang/String;)V
    .locals 0
    .param p1, "mission"    # Ljava/lang/String;

    .prologue
    .line 405
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->mission:Ljava/lang/String;

    .line 406
    return-void
.end method

.method public setMissionCompletedDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "missionCompletedDate"    # Ljava/util/Calendar;

    .prologue
    .line 619
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionCompletedDate:Ljava/util/Calendar;

    .line 620
    return-void
.end method

.method public setMissionDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionDescription"    # Ljava/lang/String;

    .prologue
    .line 423
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionDescription:Ljava/lang/String;

    .line 424
    return-void
.end method

.method public setMissionId(I)V
    .locals 0
    .param p1, "missionId"    # I

    .prologue
    .line 386
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionId:I

    .line 387
    return-void
.end method

.method public setMissionImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionImage"    # Ljava/lang/String;

    .prologue
    .line 441
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionImage:Ljava/lang/String;

    .line 442
    return-void
.end method

.method public setMissionSeqId(I)V
    .locals 0
    .param p1, "missionSeqId"    # I

    .prologue
    .line 366
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->missionSeqId:I

    .line 367
    return-void
.end method

.method public setSpecificTracker(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)V
    .locals 0
    .param p1, "specificTracker"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .prologue
    .line 480
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->specificTracker:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    .line 481
    return-void
.end method

.method protected setTrackerMission(Z)V
    .locals 0
    .param p1, "isTrackerMission"    # Z

    .prologue
    .line 580
    iput-boolean p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->isTrackerMission:Z

    .line 581
    return-void
.end method

.method public setTrackerType(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)V
    .locals 0
    .param p1, "trackerType"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .prologue
    .line 461
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->trackerType:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 462
    return-void
.end method

.method public setWhatToDo(Ljava/lang/String;)V
    .locals 0
    .param p1, "whatToDo"    # Ljava/lang/String;

    .prologue
    .line 500
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whatToDo:Ljava/lang/String;

    .line 501
    return-void
.end method

.method public setWhatToDoImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "whatToDoImage"    # Ljava/lang/String;

    .prologue
    .line 520
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whatToDoImage:Ljava/lang/String;

    .line 521
    return-void
.end method

.method public setWhyDoIt(Ljava/lang/String;)V
    .locals 0
    .param p1, "whyDoIt"    # Ljava/lang/String;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whyDoIt:Ljava/lang/String;

    .line 541
    return-void
.end method

.method public setWhyDoItImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "whyDoItImage"    # Ljava/lang/String;

    .prologue
    .line 560
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->whyDoItImage:Ljava/lang/String;

    .line 561
    return-void
.end method
