.class public Lcom/cigna/coach/apiobjects/HealthLibraryCategory;
.super Ljava/lang/Object;
.source "HealthLibraryCategory.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field protected categoryId:I

.field protected description:Ljava/lang/String;

.field protected image:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategoryId()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;->categoryId:I

    return v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/HealthLibraryCategory;->image:Ljava/lang/String;

    return-object v0
.end method
