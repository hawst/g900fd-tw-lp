.class public Lcom/cigna/coach/apiobjects/Scores;
.super Ljava/lang/Object;
.source "Scores.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private categoryInfo:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private outOfMessage:Ljava/lang/String;

.field private overallScore:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/cigna/coach/apiobjects/Scores;->overallScore:I

    .line 51
    return-void
.end method


# virtual methods
.method public getCategoryInfo()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Scores;->categoryInfo:Ljava/util/Hashtable;

    return-object v0
.end method

.method public getOutOfMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Scores;->outOfMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getOverallScore()I
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/cigna/coach/apiobjects/Scores;->overallScore:I

    return v0
.end method

.method public setCategoryInfo(Ljava/util/Hashtable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p1, "categoryInfo":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Scores;->categoryInfo:Ljava/util/Hashtable;

    .line 105
    return-void
.end method

.method public setCategoryScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V
    .locals 2
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p2, "score"    # I

    .prologue
    .line 114
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Scores;->categoryInfo:Ljava/util/Hashtable;

    if-nez v0, :cond_0

    .line 115
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/Scores;->categoryInfo:Ljava/util/Hashtable;

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Scores;->categoryInfo:Ljava/util/Hashtable;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    return-void
.end method

.method public setOutOfMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "outOfMessage"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Scores;->outOfMessage:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setOverallScore(I)V
    .locals 0
    .param p1, "overallScore"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/cigna/coach/apiobjects/Scores;->overallScore:I

    .line 87
    return-void
.end method
