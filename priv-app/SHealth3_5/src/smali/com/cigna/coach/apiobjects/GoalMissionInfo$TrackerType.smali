.class public final enum Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
.super Ljava/lang/Enum;
.source "GoalMissionInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/apiobjects/GoalMissionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrackerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum EXERCISE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum FOOD:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum INVALID:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum NOT_TRACKER_TYPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum PEDOMETER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum SLEEP:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum STRESS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field public static final enum WEIGHT:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field trackerType:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 55
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "INVALID"

    const/4 v6, -0x1

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->INVALID:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 56
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "NOT_TRACKER_TYPE"

    invoke-direct {v4, v5, v9, v8}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->NOT_TRACKER_TYPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 57
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "EXERCISE"

    invoke-direct {v4, v5, v10, v9}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->EXERCISE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 58
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "FOOD"

    invoke-direct {v4, v5, v11, v10}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->FOOD:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 59
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "SLEEP"

    invoke-direct {v4, v5, v12, v11}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->SLEEP:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 60
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "STRESS"

    const/4 v6, 0x5

    invoke-direct {v4, v5, v6, v12}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->STRESS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 61
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "PEDOMETER"

    const/4 v6, 0x6

    const/4 v7, 0x5

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->PEDOMETER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 62
    new-instance v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    const-string v5, "WEIGHT"

    const/4 v6, 0x7

    const/4 v7, 0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->WEIGHT:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 54
    const/16 v4, 0x8

    new-array v4, v4, [Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->INVALID:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->NOT_TRACKER_TYPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->EXERCISE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->FOOD:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->SLEEP:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v5, v4, v12

    const/4 v5, 0x5

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->STRESS:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->PEDOMETER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->WEIGHT:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->$VALUES:[Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    .line 75
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->codeValueMap:Ljava/util/HashMap;

    .line 77
    invoke-static {}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->values()[Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 78
    .local v3, "type":Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    sget-object v4, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->getTrackerType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    .end local v3    # "type":Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "tType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    iput p3, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->trackerType:I

    .line 71
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    .locals 2
    .param p0, "trackerTypeKey"    # I

    .prologue
    .line 89
    sget-object v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    const-class v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->$VALUES:[Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    invoke-virtual {v0}, [Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    return-object v0
.end method


# virtual methods
.method public getTrackerType()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->trackerType:I

    return v0
.end method
