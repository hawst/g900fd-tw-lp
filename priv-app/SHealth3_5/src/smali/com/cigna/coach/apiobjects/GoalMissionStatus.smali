.class public Lcom/cigna/coach/apiobjects/GoalMissionStatus;
.super Ljava/lang/Object;
.source "GoalMissionStatus.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private badgeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation
.end field

.field private goal:Ljava/lang/String;

.field private goalFrequency:Ljava/lang/String;

.field private goalId:I

.field private goalImage:Ljava/lang/String;

.field private goalProgressStatus:Ljava/lang/String;

.field private goalProgressStatusDescription:Ljava/lang/String;

.field private goalProgressStatusImage:Ljava/lang/String;

.field private goalProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

.field private goalStatusAccolade:Ljava/lang/String;

.field private goalStatusTransition:Ljava/lang/String;

.field private mission:Ljava/lang/String;

.field private missionActivityCompletedTotalCount:I

.field private missionActivityTotalCount:I

.field private missionId:I

.field private missionProgressDetail:Ljava/lang/String;

.field private missionProgressStatus:Ljava/lang/String;

.field private missionProgressStatusDescription:Ljava/lang/String;

.field private missionProgressStatusImage:Ljava/lang/String;

.field private missionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field private missionRepeatable:Z

.field private missionStatusAccolade:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    return-void
.end method


# virtual methods
.method public getBadgeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->badgeList:Ljava/util/List;

    return-object v0
.end method

.method public getGoal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goal:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalFrequency()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalFrequency:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalId()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalId:I

    return v0
.end method

.method public getGoalImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalImage:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalProgressStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalProgressStatusDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatusDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalProgressStatusImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatusImage:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    return-object v0
.end method

.method public getGoalStatusAccolade()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalStatusAccolade:Ljava/lang/String;

    return-object v0
.end method

.method public getGoalStatusTransition()Ljava/lang/String;
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalStatusTransition:Ljava/lang/String;

    return-object v0
.end method

.method public getMission()Ljava/lang/String;
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->mission:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionActivityCompletedTotalCount()I
    .locals 1

    .prologue
    .line 366
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionActivityCompletedTotalCount:I

    return v0
.end method

.method public getMissionActivityTotalCount()I
    .locals 1

    .prologue
    .line 346
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionActivityTotalCount:I

    return v0
.end method

.method public getMissionId()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionId:I

    return v0
.end method

.method public getMissionProgressDetail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressDetail:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionProgressStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionProgressStatusDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatusDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionProgressStatusImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatusImage:Ljava/lang/String;

    return-object v0
.end method

.method public getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    return-object v0
.end method

.method public getMissionStatusAccolade()Ljava/lang/String;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionStatusAccolade:Ljava/lang/String;

    return-object v0
.end method

.method public isMissionRepeatable()Z
    .locals 1

    .prologue
    .line 517
    iget-boolean v0, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionRepeatable:Z

    return v0
.end method

.method public setBadgeList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "badgeList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->badgeList:Ljava/util/List;

    .line 147
    return-void
.end method

.method public setGoal(Ljava/lang/String;)V
    .locals 0
    .param p1, "goal"    # Ljava/lang/String;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goal:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public setGoalFrequency(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalFrequency"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalFrequency:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public setGoalId(I)V
    .locals 0
    .param p1, "goalId"    # I

    .prologue
    .line 203
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalId:I

    .line 204
    return-void
.end method

.method public setGoalImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalImage"    # Ljava/lang/String;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalImage:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public setGoalProgressStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalProgressStatus"    # Ljava/lang/String;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatus:Ljava/lang/String;

    .line 244
    return-void
.end method

.method public setGoalProgressStatusDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalProgressStatusDescription"    # Ljava/lang/String;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatusDescription:Ljava/lang/String;

    .line 263
    return-void
.end method

.method public setGoalProgressStatusImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalProgressStatusImage"    # Ljava/lang/String;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatusImage:Ljava/lang/String;

    .line 282
    return-void
.end method

.method public setGoalProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;)V
    .locals 0
    .param p1, "goalProgressStatusType"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    .prologue
    .line 300
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    .line 301
    return-void
.end method

.method public setGoalStatusAccolade(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalStatusAccolade"    # Ljava/lang/String;

    .prologue
    .line 319
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalStatusAccolade:Ljava/lang/String;

    .line 320
    return-void
.end method

.method public setGoalStatusTransition(Ljava/lang/String;)V
    .locals 0
    .param p1, "goalStatusTransition"    # Ljava/lang/String;

    .prologue
    .line 337
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->goalStatusTransition:Ljava/lang/String;

    .line 338
    return-void
.end method

.method public setMission(Ljava/lang/String;)V
    .locals 0
    .param p1, "mission"    # Ljava/lang/String;

    .prologue
    .line 397
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->mission:Ljava/lang/String;

    .line 398
    return-void
.end method

.method public setMissionActivityCompletedTotalCount(I)V
    .locals 0
    .param p1, "missionActivityCompletedTotalCount"    # I

    .prologue
    .line 377
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionActivityCompletedTotalCount:I

    .line 378
    return-void
.end method

.method public setMissionActivityTotalCount(I)V
    .locals 0
    .param p1, "missionActivityTotalCount"    # I

    .prologue
    .line 356
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionActivityTotalCount:I

    .line 357
    return-void
.end method

.method public setMissionId(I)V
    .locals 0
    .param p1, "missionId"    # I

    .prologue
    .line 417
    iput p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionId:I

    .line 418
    return-void
.end method

.method public setMissionProgressDetail(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressDetail"    # Ljava/lang/String;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressDetail:Ljava/lang/String;

    .line 471
    return-void
.end method

.method public setMissionProgressStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressStatus"    # Ljava/lang/String;

    .prologue
    .line 437
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatus:Ljava/lang/String;

    .line 438
    return-void
.end method

.method public setMissionProgressStatusDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressStatusDescription"    # Ljava/lang/String;

    .prologue
    .line 456
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatusDescription:Ljava/lang/String;

    .line 457
    return-void
.end method

.method public setMissionProgressStatusImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionProgressStatusImage"    # Ljava/lang/String;

    .prologue
    .line 489
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatusImage:Ljava/lang/String;

    .line 490
    return-void
.end method

.method public setMissionProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V
    .locals 0
    .param p1, "missionProgressStatusType"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .prologue
    .line 507
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionProgressStatusType:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 508
    return-void
.end method

.method public setMissionRepeatable(Z)V
    .locals 0
    .param p1, "missionRepeatable"    # Z

    .prologue
    .line 527
    iput-boolean p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionRepeatable:Z

    .line 528
    return-void
.end method

.method public setMissionStatusAccolade(Ljava/lang/String;)V
    .locals 0
    .param p1, "missionStatusAccolade"    # Ljava/lang/String;

    .prologue
    .line 545
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/GoalMissionStatus;->missionStatusAccolade:Ljava/lang/String;

    .line 546
    return-void
.end method
