.class public Lcom/cigna/coach/apiobjects/Notification;
.super Ljava/lang/Object;
.source "Notification.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    }
.end annotation


# static fields
.field public static final DEEP_LINK_EXTRA_NAME_DESTINATION:Ljava/lang/String; = "EXTRA_NAME_DESTINATION"

.field public static final DEEP_LINK_GOAL_ID:Ljava/lang/String; = "EXTRA_NAME_GOAL_ID"

.field public static final DEEP_LINK_GOAL_SEQ_ID:Ljava/lang/String; = "EXTRA_NAME_GOAL_SEQUENCE_ID"

.field public static final DEEP_LINK_INTENT_ACTION:Ljava/lang/String; = "INTENT_ACTION"

.field public static final DEEP_LINK_MISSION_ID:Ljava/lang/String; = "EXTRA_NAME_MISSION_ID"

.field public static final DEEP_LINK_MISSION_SEQ_ID:Ljava/lang/String; = "EXTRA_NAME_MISSION_SEQUENCE_ID"

.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private deepLinkInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private feature:Ljava/lang/String;

.field private image:Ljava/lang/String;

.field private languageCode:Ljava/lang/String;

.field private messageLine1:Ljava/lang/String;

.field private messageLine2:Ljava/lang/String;

.field private notificationDate:Ljava/util/Calendar;

.field private notificationType:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

.field private title:Ljava/lang/String;

.field private uiType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    const-string v0, "en"

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->languageCode:Ljava/lang/String;

    .line 147
    const-string v0, "03"

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->uiType:Ljava/lang/String;

    .line 148
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->notificationDate:Ljava/util/Calendar;

    .line 149
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "languageCode"    # Ljava/lang/String;
    .param p2, "uiType"    # Ljava/lang/String;

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->languageCode:Ljava/lang/String;

    .line 158
    iput-object p2, p0, Lcom/cigna/coach/apiobjects/Notification;->uiType:Ljava/lang/String;

    .line 159
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->notificationDate:Ljava/util/Calendar;

    .line 160
    return-void
.end method


# virtual methods
.method public getDeepLinkInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->deepLinkInfo:Ljava/util/Map;

    return-object v0
.end method

.method public getFeature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->feature:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->image:Ljava/lang/String;

    return-object v0
.end method

.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->languageCode:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageLine1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->messageLine1:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageLine2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->messageLine2:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->notificationDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->notificationType:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getUiType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/cigna/coach/apiobjects/Notification;->uiType:Ljava/lang/String;

    return-object v0
.end method

.method public setDeepLinkInfo(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338
    .local p1, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->deepLinkInfo:Ljava/util/Map;

    .line 339
    return-void
.end method

.method public setFeature(Ljava/lang/String;)V
    .locals 0
    .param p1, "feature"    # Ljava/lang/String;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->feature:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "image"    # Ljava/lang/String;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->image:Ljava/lang/String;

    .line 220
    return-void
.end method

.method public setLanguageCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 358
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->languageCode:Ljava/lang/String;

    .line 359
    return-void
.end method

.method public setMessageLine1(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageLine1"    # Ljava/lang/String;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->messageLine1:Ljava/lang/String;

    .line 260
    return-void
.end method

.method public setMessageLine2(Ljava/lang/String;)V
    .locals 0
    .param p1, "messageLine2"    # Ljava/lang/String;

    .prologue
    .line 279
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->messageLine2:Ljava/lang/String;

    .line 280
    return-void
.end method

.method public setNotificationDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "notificationDate"    # Ljava/util/Calendar;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->notificationDate:Ljava/util/Calendar;

    .line 300
    return-void
.end method

.method public setNotificationType(Lcom/cigna/coach/apiobjects/Notification$NotificationType;)V
    .locals 0
    .param p1, "notificationType"    # Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->notificationType:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 319
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->title:Ljava/lang/String;

    .line 240
    return-void
.end method

.method public setUiType(Ljava/lang/String;)V
    .locals 0
    .param p1, "uiType"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/cigna/coach/apiobjects/Notification;->uiType:Ljava/lang/String;

    .line 180
    return-void
.end method
