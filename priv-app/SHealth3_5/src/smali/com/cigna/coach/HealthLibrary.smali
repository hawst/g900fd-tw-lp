.class public Lcom/cigna/coach/HealthLibrary;
.super Ljava/lang/Object;
.source "HealthLibrary.java"

# interfaces
.implements Lcom/cigna/coach/interfaces/IHealthLibrary;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/cigna/coach/HealthLibrary;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    .line 66
    return-void
.end method


# virtual methods
.method public addFavorite(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "articleId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 326
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 327
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "addFavorite : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v0, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 331
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v0}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 332
    .local v2, "healthLibraryDbManager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    const/4 v3, 0x0

    .line 335
    .local v3, "returnVal":Z
    :try_start_0
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/HealthLibraryDbManager;->addUserFavoriteArticle(Ljava/lang/String;I)Z

    move-result v3

    .line 336
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 342
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 343
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "addFavorite : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_1
    return v3

    .line 337
    :catch_0
    move-exception v1

    .line 338
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in addFavorite"

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 339
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in addFavorite"

    invoke-direct {v4, v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 341
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 342
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 343
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "addFavorite : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getArticle(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "articleId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 74
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 75
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getArticle : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v2, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 79
    .local v2, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v3, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v3, v2}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 80
    .local v3, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    const/4 v1, 0x0

    .line 83
    .local v1, "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    const/4 v4, -0x1

    :try_start_0
    invoke-virtual {v3, p1, p2, v4}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getArticle(Ljava/lang/String;II)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    move-result-object v1

    .line 84
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 92
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 93
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getArticle : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_1
    return-object v1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "HealthLibrary::getArticle:Error in getting Article detail"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "HealthLibrary::getArticle:Error in getting Article detail"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 92
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 93
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticle : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getArticle(Ljava/lang/String;II)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "articleId"    # I
    .param p3, "subCategoryId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 107
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 108
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getArticle : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_0
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v2, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 112
    .local v2, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v3, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v3, v2}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 113
    .local v3, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    const/4 v1, 0x0

    .line 116
    .local v1, "healthLibraryArticle":Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    :try_start_0
    invoke-virtual {v3, p1, p2, p3}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getArticle(Ljava/lang/String;II)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;

    move-result-object v1

    .line 117
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 125
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 126
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getArticle : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_1
    return-object v1

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "HealthLibrary::getArticle:Error in getting Article detail"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "HealthLibrary::getArticle:Error in getting Article detail"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 125
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 126
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticle : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getArticlesForSubCategory(Ljava/lang/String;II)Ljava/util/List;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryId"    # I
    .param p3, "subCategoryId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 141
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 142
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getArticlesForSubCategory : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 146
    .local v1, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 147
    .local v2, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 150
    .local v3, "response":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    :try_start_0
    invoke-virtual {v2, p1, p2, p3}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getArticlesForSubCategory(Ljava/lang/String;II)Ljava/util/List;

    move-result-object v3

    .line 151
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 157
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 158
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getArticlesForSubCategory : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_1
    return-object v3

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getArticlesForSubCategory"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 154
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in getArticlesForSubCategory"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 157
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 158
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticlesForSubCategory : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getCategories(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryCategory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 172
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 173
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCategories : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 177
    .local v1, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 178
    .local v2, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 181
    .local v3, "response":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryCategory;>;"
    :try_start_0
    invoke-virtual {v2}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getCategories()Ljava/util/List;

    move-result-object v3

    .line 182
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 188
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 189
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCategories : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_1
    return-object v3

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getCategories"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 185
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in getCategories"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 188
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 189
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCategories : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getFavoritesByCategory(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 202
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 203
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getFavoritesByCategory : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 207
    .local v1, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 208
    .local v2, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 211
    .local v3, "response":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getFavoritesByCategory(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 212
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 218
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 219
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getFavoritesByCategory : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_1
    return-object v3

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getFavoritesByCategory"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 215
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in getFavoritesByCategory"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 218
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 219
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFavoritesByCategory : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getFavoritesByTitle(Ljava/lang/String;Z)Ljava/util/List;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "sortAscending"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 233
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 234
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getFavoritesByTitle : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 238
    .local v1, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 239
    .local v2, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 242
    .local v3, "response":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    :try_start_0
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getFavoritesByTitle(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    .line 243
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 244
    new-instance v4, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;

    invoke-direct {v4, p2}, Lcom/cigna/coach/HealthLibrary$ArticleTitleComparator;-><init>(Z)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 250
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 251
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getFavoritesByTitle : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_1
    return-object v3

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getFavoritesByTitle"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 247
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in getFavoritesByTitle"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 250
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 251
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFavoritesByTitle : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getFeaturedArticles(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 265
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 266
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getFeaturedArticles : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 270
    .local v1, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 271
    .local v2, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 274
    .local v3, "response":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getFeaturedArticles(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 275
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 281
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 282
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getFeaturedArticles : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_1
    return-object v3

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getFeaturedArticles"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 278
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in getFeaturedArticles"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 281
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 282
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFeaturedArticles : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getSubCategories(Ljava/lang/String;I)Ljava/util/List;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 296
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 297
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getSubCategories : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 301
    .local v1, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 302
    .local v2, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 305
    .local v3, "response":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;>;"
    :try_start_0
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getSubCategories(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    .line 306
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 312
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 313
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getSubCategories : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    :cond_1
    return-object v3

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getSubCategories"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 309
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in getSubCategories"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 312
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 313
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getSubCategories : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public removeFavorite(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "articleId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 357
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 358
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "removeFavorite : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v0, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 362
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v0}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 363
    .local v2, "healthLibraryDbManager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    const/4 v3, 0x0

    .line 366
    .local v3, "returnVal":Z
    :try_start_0
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/HealthLibraryDbManager;->removeUserFavoriteArticle(Ljava/lang/String;I)Z

    move-result v3

    .line 367
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 372
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 373
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 374
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "removeFavorite : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_1
    return v3

    .line 368
    :catch_0
    move-exception v1

    .line 369
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in removeFavorite"

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 370
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in removeFavorite"

    invoke-direct {v4, v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 372
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 373
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 374
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "removeFavorite : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public searchArticles(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "searchText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 388
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 389
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "searchArticles : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/HealthLibrary;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 393
    .local v1, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-direct {v2, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 394
    .local v2, "manager":Lcom/cigna/coach/db/HealthLibraryDbManager;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 397
    .local v3, "response":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    :try_start_0
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getArticlesByKeyword(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 398
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v4, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v4, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 404
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 405
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "searchArticles : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_1
    return-object v3

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in searchArticles"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 401
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in searchArticles"

    invoke-direct {v4, v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 403
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 404
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 405
    sget-object v5, Lcom/cigna/coach/HealthLibrary;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "searchArticles : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method
