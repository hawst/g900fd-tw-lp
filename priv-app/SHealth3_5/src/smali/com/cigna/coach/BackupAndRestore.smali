.class public Lcom/cigna/coach/BackupAndRestore;
.super Ljava/lang/Object;
.source "BackupAndRestore.java"

# interfaces
.implements Lcom/cigna/coach/interfaces/IBackupAndRestore;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/BackupAndRestore$1;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/cigna/coach/BackupAndRestore;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/BackupAndRestore;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/cigna/coach/BackupAndRestore;->context:Landroid/content/Context;

    .line 39
    return-void
.end method


# virtual methods
.method public startOperation(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;)V
    .locals 4
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "listener"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 43
    sget-object v1, Lcom/cigna/coach/BackupAndRestore;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    sget-object v1, Lcom/cigna/coach/BackupAndRestore;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startOperation : START : RequestType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    iget-object v1, p0, Lcom/cigna/coach/BackupAndRestore;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;-><init>(Landroid/content/Context;)V

    .line 47
    .local v0, "journalHelper":Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    sget-object v1, Lcom/cigna/coach/BackupAndRestore$1;->$SwitchMap$com$cigna$coach$interfaces$IBackupAndRestore$RequestType:[I

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 62
    :goto_0
    sget-object v1, Lcom/cigna/coach/BackupAndRestore;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    sget-object v1, Lcom/cigna/coach/BackupAndRestore;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "startOperation : END"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_1
    return-void

    .line 49
    :pswitch_0
    new-instance v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;

    invoke-direct {v1, p2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;-><init>(Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;)V

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->backup(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    goto :goto_0

    .line 53
    :pswitch_1
    new-instance v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;

    invoke-direct {v1, p2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;-><init>(Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;)V

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->restore(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    goto :goto_0

    .line 57
    :pswitch_2
    new-instance v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;

    invoke-direct {v1, p2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;-><init>(Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;)V

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->erase(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
