.class public Lcom/cigna/coach/Widget;
.super Ljava/lang/Object;
.source "Widget.java"

# interfaces
.implements Lcom/cigna/coach/interfaces/IWidget;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/cigna/coach/Widget;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/cigna/coach/Widget;->context:Landroid/content/Context;

    .line 49
    return-void
.end method

.method private displayWidgetMessage(Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)V
    .locals 10
    .param p1, "widgteInfo"    # Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    .prologue
    .line 152
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 154
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;

    move-object v6, v0

    .line 155
    .local v6, "widgetData":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Widget Title :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getTitleText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\nWidget Message :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getMessageText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\nWidget Type :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v9

    invoke-virtual {v9}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->name()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\nWidget Title ContentId :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->getTitleContentId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\nWidget Message ContentId :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->getMessageContentId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Widget Title Intent : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->getTitleIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->getTitleIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 161
    .local v5, "titleExtras":Landroid/os/Bundle;
    if-eqz v5, :cond_1

    .line 162
    invoke-virtual {v5}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 163
    .local v3, "kString":Ljava/lang/String;
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Title Intent extra : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " val: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 177
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "kString":Ljava/lang/String;
    .end local v5    # "titleExtras":Landroid/os/Bundle;
    .end local v6    # "widgetData":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    :catch_0
    move-exception v1

    .line 178
    .local v1, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 179
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception displayWidgetMessage: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    return-void

    .line 168
    .restart local v5    # "titleExtras":Landroid/os/Bundle;
    .restart local v6    # "widgetData":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    :cond_1
    :try_start_1
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Widget Message Intent : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->getMessageIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->getMessageIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 170
    .local v4, "messageExtras":Landroid/os/Bundle;
    if-eqz v4, :cond_0

    .line 171
    invoke-virtual {v4}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 172
    .restart local v3    # "kString":Ljava/lang/String;
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Message Intent extra : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    sget-object v7, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " val: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "widgetInfoType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 192
    sget-object v0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {p0, p1, v0, p2}, Lcom/cigna/coach/Widget;->dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 193
    return-void
.end method

.method public dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .param p3, "widgetInfoType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 123
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 124
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "dismissWidgetInfo : START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_0
    sget-object v3, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    if-ne p3, v3, :cond_2

    .line 149
    :cond_1
    :goto_0
    return-void

    .line 129
    :cond_2
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v3, p0, Lcom/cigna/coach/Widget;->context:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V

    .line 132
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v2, Lcom/cigna/coach/utils/WidgetHelper;

    invoke-direct {v2, v0}, Lcom/cigna/coach/utils/WidgetHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 133
    .local v2, "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    invoke-virtual {v2, p1, p3}, Lcom/cigna/coach/utils/WidgetHelper;->dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 134
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V

    .line 135
    iget-object v3, p0, Lcom/cigna/coach/Widget;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    if-eqz v0, :cond_3

    .line 143
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 144
    :cond_3
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getWidgetInfo : END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    .end local v2    # "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    :catch_0
    move-exception v1

    .line 138
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 139
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Exception dismissWidgetNotification : END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    :cond_4
    if-eqz v0, :cond_5

    .line 143
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 144
    :cond_5
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 145
    sget-object v3, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getWidgetInfo : END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 142
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_6

    .line 143
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 144
    :cond_6
    sget-object v4, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 145
    sget-object v4, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getWidgetInfo : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v3
.end method

.method public getWidgetInfo(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 187
    sget-object v0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/Widget;->getWidgetInfo(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;

    move-result-object v0

    return-object v0
.end method

.method public getWidgetInfo(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 65
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 66
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWidgetInfo(\""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") : START"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    iget-object v5, p0, Lcom/cigna/coach/Widget;->context:Landroid/content/Context;

    sget-object v6, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/SharedPrefUtils;->getCoachSettings(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 70
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 71
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getWidgetInfo : COACH_NOT_STARTED"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    iget-object v5, p0, Lcom/cigna/coach/Widget;->context:Landroid/content/Context;

    invoke-static {v5, p1, p2}, Lcom/cigna/coach/utils/WidgetHelper;->populateWidgetInfo_COACH_NOT_STARTED(Landroid/content/Context;Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/AndroidWidgetData;

    move-result-object v0

    .line 74
    .local v0, "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 75
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getWidgetInfo : END COACH_NOT_STARTED"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_2
    invoke-direct {p0, v0}, Lcom/cigna/coach/Widget;->displayWidgetMessage(Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)V

    .line 101
    :cond_3
    :goto_0
    return-object v0

    .line 81
    .end local v0    # "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    :cond_4
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v5, p0, Lcom/cigna/coach/Widget;->context:Landroid/content/Context;

    invoke-direct {v2, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V

    .line 82
    .local v2, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v1, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v1, v2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 85
    .local v1, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    :try_start_0
    new-instance v4, Lcom/cigna/coach/utils/WidgetHelper;

    invoke-direct {v4, v2}, Lcom/cigna/coach/utils/WidgetHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 86
    .local v4, "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    invoke-virtual {v4, p1, p2}, Lcom/cigna/coach/utils/WidgetHelper;->getWidgetInfo(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/AndroidWidgetData;

    move-result-object v0

    .line 87
    .restart local v0    # "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    invoke-virtual {v1, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 88
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V

    .line 89
    invoke-direct {p0, v0}, Lcom/cigna/coach/Widget;->displayWidgetMessage(Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 100
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 101
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getWidgetInfo : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 91
    .end local v0    # "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    .end local v4    # "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    :catch_0
    move-exception v3

    .line 92
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 93
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWidgetInfo : Error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; Falling back on COACH_NOT_STARTED"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_5
    iget-object v5, p0, Lcom/cigna/coach/Widget;->context:Landroid/content/Context;

    invoke-static {v5, p1, p2}, Lcom/cigna/coach/utils/WidgetHelper;->populateWidgetInfo_COACH_NOT_STARTED(Landroid/content/Context;Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 99
    .restart local v0    # "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 100
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 101
    sget-object v5, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getWidgetInfo : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 99
    .end local v0    # "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 100
    sget-object v6, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 101
    sget-object v6, Lcom/cigna/coach/Widget;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getWidgetInfo : END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v5
.end method
