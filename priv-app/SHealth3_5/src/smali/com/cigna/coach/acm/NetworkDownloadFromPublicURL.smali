.class Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;
.super Ljava/lang/Thread;
.source "NetworkDownloadFromPublicURL.java"

# interfaces
.implements Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL$1;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field protected mConnection:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field protected final mTask:Lcom/cigna/coach/acm/ACMTask;

.field private toBeDownloadedFileSize:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/cigna/coach/acm/ACMTask;)V
    .locals 2
    .param p1, "connection"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "task"    # Lcom/cigna/coach/acm/ACMTask;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->toBeDownloadedFileSize:J

    .line 27
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ACMTask can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_1
    iput-object p1, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mConnection:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 30
    iput-object p2, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    .line 31
    return-void
.end method


# virtual methods
.method protected download()J
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 47
    new-instance v7, Ljava/io/File;

    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getFilepathForDownload()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .local v7, "downloadFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v9

    .line 51
    .local v9, "parent":Ljava/io/File;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    :cond_0
    new-instance v0, Lcom/cigna/coach/exceptions/CoachException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t create dir: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 56
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "physicalFileName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; already exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_2
    const/4 v8, 0x0

    .line 62
    .local v8, "outputStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    .end local v8    # "outputStream":Ljava/io/OutputStream;
    .local v4, "outputStream":Ljava/io/OutputStream;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URL to be downloaded = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_3
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mConnection:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getTaskReferenceId()I

    move-result v1

    iget-object v2, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getFilepathForDownload()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v2, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->downloadPublicUrl(ILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener;Ljava/lang/String;Ljava/io/OutputStream;Ljava/lang/Object;Ljava/util/ArrayList;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v10

    .line 69
    .local v10, "requestId":J
    invoke-static {v4}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    return-wide v10

    .end local v4    # "outputStream":Ljava/io/OutputStream;
    .end local v10    # "requestId":J
    .restart local v8    # "outputStream":Ljava/io/OutputStream;
    :catchall_0
    move-exception v0

    move-object v4, v8

    .end local v8    # "outputStream":Ljava/io/OutputStream;
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :goto_0
    invoke-static {v4}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public onDownload(JILcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;JLcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/util/HashMap;)V
    .locals 5
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "status"    # Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;
    .param p5, "size"    # J
    .param p7, "e"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p8, "tag"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;",
            "J",
            "Lcom/sec/android/service/health/connectionmanager2/NetException;",
            "Ljava/lang/Object;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p9, "headervalues":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL$1;->$SwitchMap$com$sec$android$service$health$connectionmanager2$IOnDownloadListener$Status:[I

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/IOnDownloadListener$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 122
    :cond_0
    :goto_0
    return-void

    .line 77
    :pswitch_0
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STARTED content to be downloaded size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_1
    iput-wide p5, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->toBeDownloadedFileSize:J

    goto :goto_0

    .line 84
    :pswitch_1
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ONGOING content downloaded size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    :pswitch_2
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DONE content downloaded size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    :cond_2
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 94
    iget-wide v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->toBeDownloadedFileSize:J

    cmp-long v0, v0, p5

    if-nez v0, :cond_3

    .line 95
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    int-to-long v2, p3

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onFinish(Lcom/cigna/coach/acm/ACMTask;JLorg/json/JSONObject;)V

    goto/16 :goto_0

    .line 97
    :cond_3
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_CANCELLED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-interface {v0, v1, v2}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto/16 :goto_0

    .line 100
    :cond_4
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Received a null callback.. cannot proceed"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    :pswitch_3
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 106
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "STOPPED content downloaded size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_5
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 109
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_CANCELLED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-interface {v0, v1, v2}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 110
    if-eqz p7, :cond_0

    invoke-virtual {p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getException()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 111
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Exception Received:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p7}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 116
    :cond_6
    sget-object v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Received a null callback.. cannot proceed"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public run()V
    .locals 4

    .prologue
    .line 36
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->download()J
    :try_end_0
    .catch Lcom/cigna/coach/acm/ACMManager$ACMError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 44
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Lcom/cigna/coach/acm/ACMManager$ACMError;
    sget-object v1, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error occured during Downlaod: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMManager$ACMError;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMManager$ACMError;->getErrorCode()Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0

    .line 40
    .end local v0    # "e":Lcom/cigna/coach/acm/ACMManager$ACMError;
    :catch_1
    move-exception v0

    .line 41
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown error occured during Downlaod: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->mTask:Lcom/cigna/coach/acm/ACMTask;

    sget-object v3, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-interface {v1, v2, v3}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0
.end method
