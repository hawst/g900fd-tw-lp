.class public Lcom/cigna/coach/acm/SamsungAccountManager;
.super Ljava/lang/Object;
.source "SamsungAccountManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;
    }
.end annotation


# static fields
.field protected static final ADD_SAMSUNG_ACCOUNT:Ljava/lang/String; = "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

.field public static final REQUEST_CODE:I = 0x3e7

.field private static final TAG:Ljava/lang/String;

.field private static instance:Lcom/cigna/coach/acm/SamsungAccountManager;


# instance fields
.field private mAccessToken:Ljava/lang/String;

.field private mClientId:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mMCC:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/cigna/coach/acm/SamsungAccountManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/cigna/coach/acm/SamsungAccountManager;->instance:Lcom/cigna/coach/acm/SamsungAccountManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mClientId:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mAccessToken:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mUserId:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mMCC:Ljava/lang/String;

    .line 28
    iput-object p1, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mContext:Landroid/content/Context;

    .line 29
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/acm/SamsungAccountManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mAccessToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/acm/SamsungAccountManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mUserId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/acm/SamsungAccountManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mClientId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/acm/SamsungAccountManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mMCC:Ljava/lang/String;

    return-object p1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/cigna/coach/acm/SamsungAccountManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    sget-object v0, Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;

    const-string v1, "Getting samsumg account manager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    sget-object v0, Lcom/cigna/coach/acm/SamsungAccountManager;->instance:Lcom/cigna/coach/acm/SamsungAccountManager;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/cigna/coach/acm/SamsungAccountManager;

    invoke-direct {v0, p0}, Lcom/cigna/coach/acm/SamsungAccountManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/cigna/coach/acm/SamsungAccountManager;->instance:Lcom/cigna/coach/acm/SamsungAccountManager;

    .line 36
    :cond_0
    sget-object v0, Lcom/cigna/coach/acm/SamsungAccountManager;->instance:Lcom/cigna/coach/acm/SamsungAccountManager;

    return-object v0
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mAccessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getClientId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mClientId:Ljava/lang/String;

    return-object v0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mMCC:Ljava/lang/String;

    return-object v0
.end method

.method public getSamsungAccountInformation(Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)Z
    .locals 6
    .param p1, "listener"    # Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    .prologue
    .line 42
    :try_start_0
    sget-object v1, Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;

    const-string v2, "Reading account information"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mContext:Landroid/content/Context;

    const-string v3, "1y90e30264"

    const-string v4, "80E7ECD9D301CB7888C73703639302E5"

    new-instance v5, Lcom/cigna/coach/acm/SamsungAccountManager$1;

    invoke-direct {v5, p0, p1}, Lcom/cigna/coach/acm/SamsungAccountManager$1;-><init>(Lcom/cigna/coach/acm/SamsungAccountManager;Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)V

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForUserToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 79
    :catch_0
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method public isDeviceSignInSamsungAccount()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->isDeviceSignInSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public renewExpiredSamsungToken(Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)V
    .locals 2
    .param p1, "listener"    # Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    .prologue
    .line 111
    invoke-static {}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->getInstance()Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;

    move-result-object v0

    new-instance v1, Lcom/cigna/coach/acm/SamsungAccountManager$2;

    invoke-direct {v1, p0, p1}, Lcom/cigna/coach/acm/SamsungAccountManager$2;-><init>(Lcom/cigna/coach/acm/SamsungAccountManager;Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/service/health/samsungaccount/SamsungAccountUserTokenManager;->sendRequestForNewUserToken(Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;)V

    .line 146
    return-void
.end method
