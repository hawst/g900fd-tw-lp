.class public final enum Lcom/cigna/coach/acm/ACMTask$ACMTaskType;
.super Ljava/lang/Enum;
.source "ACMTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/acm/ACMTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ACMTaskType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/acm/ACMTask$ACMTaskType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

.field public static final enum API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

.field public static final enum DOWNLOAD:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-instance v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    const-string v1, "DOWNLOAD"

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->DOWNLOAD:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    new-instance v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    const-string v1, "API"

    invoke-direct {v0, v1, v3}, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    sget-object v1, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->DOWNLOAD:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->$VALUES:[Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/acm/ACMTask$ACMTaskType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/acm/ACMTask$ACMTaskType;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->$VALUES:[Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    invoke-virtual {v0}, [Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    return-object v0
.end method
