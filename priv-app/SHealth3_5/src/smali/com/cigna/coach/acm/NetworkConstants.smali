.class public final Lcom/cigna/coach/acm/NetworkConstants;
.super Ljava/lang/Object;
.source "NetworkConstants.java"


# static fields
.field public static final API_GET_COUNT:Ljava/lang/String;

.field public static final API_GET_ENGINE_DB_URL:Ljava/lang/String;

.field public static final API_GET_LATEST_ENGINE_DB_VERSION:Ljava/lang/String;

.field public static final API_GET_USER_DATA:Ljava/lang/String;

.field public static final API_SET_USER_DATA:Ljava/lang/String;

.field public static final CIGNA_CONTEXT:Ljava/lang/String; = "cigna"

.field public static final CN_PRODUCTION_SERVER:Ljava/lang/String; = "cn-svc.samsungshealth.com"

.field public static final ERROR_CODE_TOKEN_EXPIRED:Ljava/lang/String; = "LIC_4104"

.field public static final EU_PRODUCTION_SERVER:Ljava/lang/String; = "eu-svc.samsungshealth.com"

.field public static final KR_PRODUCTION_SERVER:Ljava/lang/String; = "kr-svc.samsungshealth.com"

.field public static final RESPONSE_AUTH_FAILED:I = 0x1

.field public static final RESPONSE_OK:I = -0x1

.field public static final RESPONSE_OTHER_ERROR:I = 0x6

.field public static final RESPONSE_RUNTIME_ERROR:I = 0x4

.field public static final RESPONSE_SUCCESS:Ljava/lang/String; = "200"

.field public static final RESPONSE_TIMEOUT_ERROR:I = 0x5

.field public static final RESPONSE_UUID_NOT_FOUND:I = 0x2

.field public static final RESPONSE_VALIDATION_ERROR:I = 0x3

.field public static SERVER_DOMAIN:Ljava/lang/String; = null

.field public static final SERVER_PORT:I = 0x1bb

.field public static final STAGING_SERVER:Ljava/lang/String; = "sht30-stg2-usev-shealth30-extelb-871651078.us-east-1.elb.amazonaws.com"

.field public static final US_PRODUCTION_SERVER:Ljava/lang/String; = "svc.samsungshealth.com"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5
    const-string/jumbo v0, "svc.samsungshealth.com"

    sput-object v0, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    .line 20
    const-string v0, "getLatestEngineDBList"

    invoke-static {v0}, Lcom/cigna/coach/acm/NetworkConstants;->getUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/NetworkConstants;->API_GET_LATEST_ENGINE_DB_VERSION:Ljava/lang/String;

    .line 21
    const-string v0, "getEngineDBLanguage"

    invoke-static {v0}, Lcom/cigna/coach/acm/NetworkConstants;->getUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/NetworkConstants;->API_GET_ENGINE_DB_URL:Ljava/lang/String;

    .line 22
    const-string v0, "getUserData"

    invoke-static {v0}, Lcom/cigna/coach/acm/NetworkConstants;->getUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/NetworkConstants;->API_GET_USER_DATA:Ljava/lang/String;

    .line 23
    const-string v0, "getCount"

    invoke-static {v0}, Lcom/cigna/coach/acm/NetworkConstants;->getUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/NetworkConstants;->API_GET_COUNT:Ljava/lang/String;

    .line 24
    const-string/jumbo v0, "setUserData"

    invoke-static {v0}, Lcom/cigna/coach/acm/NetworkConstants;->getUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/NetworkConstants;->API_SET_USER_DATA:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "api"    # Ljava/lang/String;

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cigna/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
