.class Lcom/cigna/coach/acm/ACMManager$1;
.super Ljava/lang/Object;
.source "ACMManager.java"

# interfaces
.implements Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/coach/acm/ACMManager;-><init>(Landroid/content/Context;Lcom/cigna/coach/acm/ACMTask;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/coach/acm/ACMManager;

.field final synthetic val$task:Lcom/cigna/coach/acm/ACMTask;


# direct methods
.method constructor <init>(Lcom/cigna/coach/acm/ACMManager;Lcom/cigna/coach/acm/ACMTask;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/cigna/coach/acm/ACMManager$1;->this$0:Lcom/cigna/coach/acm/ACMManager;

    iput-object p2, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 4
    .param p1, "status"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 65
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    if-eq p1, v1, :cond_0

    .line 66
    # getter for: Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/ACMManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "User Not logged in Samsung Account"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    invoke-interface {v1, v2, p1}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 88
    :goto_0
    return-void

    .line 69
    :cond_0
    # getter for: Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/ACMManager;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    # getter for: Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/ACMManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "User Samsung Account login success"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/shealth/common/utils/ContextHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/CommonUtils;->serverUrls(Landroid/content/Context;)V

    .line 74
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager$1;->this$0:Lcom/cigna/coach/acm/ACMManager;

    # invokes: Lcom/cigna/coach/acm/ACMManager;->initConnMan()V
    invoke-static {v1}, Lcom/cigna/coach/acm/ACMManager;->access$100(Lcom/cigna/coach/acm/ACMManager;)V

    .line 75
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager$1;->this$0:Lcom/cigna/coach/acm/ACMManager;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMManager;->execute()V
    :try_end_0
    .catch Lcom/cigna/coach/acm/ACMManager$ACMError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Lcom/cigna/coach/acm/ACMManager$ACMError;
    # getter for: Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/ACMManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Aborting ACM execution"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMManager$ACMError;->getErrorCode()Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0

    .line 79
    .end local v0    # "e":Lcom/cigna/coach/acm/ACMManager$ACMError;
    :catch_1
    move-exception v0

    .line 81
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/ACMManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Aborting ACM execution"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager$1;->val$task:Lcom/cigna/coach/acm/ACMTask;

    sget-object v3, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-interface {v1, v2, v3}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0

    .line 86
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    # getter for: Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/ACMManager;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Received a null callback.. cannot proceed"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
