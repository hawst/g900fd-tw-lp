.class public Lcom/cigna/coach/acm/NetworkPlaceRequest;
.super Ljava/lang/Thread;
.source "NetworkPlaceRequest.java"

# interfaces
.implements Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;
.implements Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static simulateTokenExpired:Z


# instance fields
.field protected mConnection:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private final mTask:Lcom/cigna/coach/acm/ACMTask;

.field protected samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    sput-boolean v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->simulateTokenExpired:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/cigna/coach/acm/SamsungAccountManager;Lcom/cigna/coach/acm/ACMTask;)V
    .locals 2
    .param p1, "connection"    # Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;
    .param p2, "samsungAccountManager"    # Lcom/cigna/coach/acm/SamsungAccountManager;
    .param p3, "task"    # Lcom/cigna/coach/acm/ACMTask;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 27
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "ACMTask can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 28
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_1
    iput-object p1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mConnection:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 30
    iput-object p2, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    .line 31
    iput-object p3, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    .line 32
    return-void
.end method

.method private doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 2
    .param p1, "e"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-interface {v0, v1, p1}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 45
    :goto_0
    return-void

    .line 43
    :cond_0
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Received a null callback.. cannot proceed"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doFinishCallback(ILorg/json/JSONObject;)V
    .locals 4
    .param p1, "privateId"    # I
    .param p2, "jsonObj"    # Lorg/json/JSONObject;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    int-to-long v2, p1

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/cigna/coach/acm/IACMTaskCompleteListener;->onFinish(Lcom/cigna/coach/acm/ACMTask;JLorg/json/JSONObject;)V

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Received a null callback.. cannot proceed"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private printHeader(Ljava/util/HashMap;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p1, "header":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;[Ljava/lang/String;>;"
    sget-object v9, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Printing headers"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    if-eqz p1, :cond_1

    .line 71
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    .line 72
    .local v4, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 73
    .local v3, "key":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    .line 74
    .local v8, "values":[Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .local v6, "valStr":Ljava/lang/StringBuilder;
    const-string v9, "      "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    const-string v9, ":"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    if-eqz v8, :cond_0

    array-length v9, v8

    if-lez v9, :cond_0

    .line 79
    move-object v0, v8

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v5, :cond_0

    aget-object v7, v0, v2

    .line 80
    .local v7, "value":Ljava/lang/String;
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v9, "   "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 84
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "value":Ljava/lang/String;
    :cond_0
    sget-object v9, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 87
    .end local v3    # "key":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v4    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;[Ljava/lang/String;>;>;"
    .end local v6    # "valStr":Ljava/lang/StringBuilder;
    .end local v8    # "values":[Ljava/lang/String;
    :cond_1
    sget-object v9, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Header is null"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_2
    return-void
.end method

.method public static simulateTokenExpired()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    sput-boolean v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->simulateTokenExpired:Z

    .line 37
    return-void
.end method


# virtual methods
.method public onExceptionReceived(JILcom/sec/android/service/health/connectionmanager2/NetException;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "ex"    # Lcom/sec/android/service/health/connectionmanager2/NetException;
    .param p5, "CLASS"    # Ljava/lang/Object;
    .param p6, "reRequest"    # Ljava/lang/Object;

    .prologue
    .line 171
    sget-object v2, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception received:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-virtual {p4}, Lcom/sec/android/service/health/connectionmanager2/NetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 174
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v2, "code"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    .local v0, "code":Ljava/lang/String;
    const-string v2, "LIC_4104"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 176
    sget-object v2, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 177
    sget-object v2, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Renewing token"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    iget-object v2, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    invoke-virtual {v2, p0}, Lcom/cigna/coach/acm/SamsungAccountManager;->renewExpiredSamsungToken(Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    .end local v0    # "code":Ljava/lang/String;
    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v2

    .line 185
    :cond_1
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {p0, v2}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0
.end method

.method public onFinish(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 3
    .param p1, "e"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 159
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Token renewal finished with error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_0
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    if-ne p1, v0, :cond_1

    .line 163
    invoke-virtual {p0}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->send()Z

    .line 167
    :goto_0
    return-void

    .line 165
    :cond_1
    invoke-direct {p0, p1}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0
.end method

.method public onRequestCancelled(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "CLASS"    # Ljava/lang/Object;
    .param p5, "reRequest"    # Ljava/lang/Object;

    .prologue
    .line 190
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Request cancelled"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_CANCELLED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {p0, v0}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 192
    return-void
.end method

.method public onResponseReceived(JILjava/lang/Object;Ljava/lang/Object;)V
    .locals 8
    .param p1, "requestId"    # J
    .param p3, "privateId"    # I
    .param p4, "result"    # Ljava/lang/Object;
    .param p5, "CLASS"    # Ljava/lang/Object;

    .prologue
    .line 107
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 108
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Response received"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_0
    const/4 v4, 0x0

    .line 112
    .local v4, "jsonString":Ljava/lang/String;
    const/4 v0, 0x0

    .line 113
    .local v0, "code":Ljava/lang/String;
    const/4 v2, 0x0

    .line 115
    .local v2, "jsonObj":Lorg/json/JSONObject;
    instance-of v5, p4, Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 116
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 117
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Result is string"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v4, p4

    .line 120
    check-cast v4, Ljava/lang/String;

    .line 122
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 123
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5, v4}, Lcom/cigna/coach/utils/Log;->logJSON(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_2
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    .local v3, "jsonObj":Lorg/json/JSONObject;
    :try_start_1
    const-string v5, "code"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    sget-boolean v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->simulateTokenExpired:Z

    if-eqz v5, :cond_3

    .line 129
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Simulating token expired (code LIC_4104)"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v0, "LIC_4104"

    .line 131
    const/4 v5, 0x0

    sput-boolean v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->simulateTokenExpired:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    move-object v2, v3

    .line 139
    .end local v3    # "jsonObj":Lorg/json/JSONObject;
    .restart local v2    # "jsonObj":Lorg/json/JSONObject;
    :cond_4
    :goto_0
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 140
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Code is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_5
    if-nez v0, :cond_6

    .line 144
    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {p0, v5}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 152
    :goto_1
    return-void

    .line 133
    :catch_0
    move-exception v1

    .line 134
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    sget-object v5, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v6, " ==== onResponseReceived exception!!"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {p0, v5}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0

    .line 145
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    const-string v5, "LIC_4104"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 146
    iget-object v5, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    invoke-virtual {v5, p0}, Lcom/cigna/coach/acm/SamsungAccountManager;->renewExpiredSamsungToken(Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)V

    goto :goto_1

    .line 147
    :cond_7
    const-string v5, "200"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    if-eqz v2, :cond_8

    .line 148
    invoke-direct {p0, p3, v2}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doFinishCallback(ILorg/json/JSONObject;)V

    goto :goto_1

    .line 150
    :cond_8
    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {p0, v5}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_1

    .line 133
    .end local v2    # "jsonObj":Lorg/json/JSONObject;
    .restart local v3    # "jsonObj":Lorg/json/JSONObject;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "jsonObj":Lorg/json/JSONObject;
    .restart local v2    # "jsonObj":Lorg/json/JSONObject;
    goto :goto_2
.end method

.method public run()V
    .locals 4

    .prologue
    .line 58
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->send()Z
    :try_end_0
    .catch Lcom/cigna/coach/acm/ACMManager$ACMError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 66
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Lcom/cigna/coach/acm/ACMManager$ACMError;
    sget-object v1, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error occured during placeRequest: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMManager$ACMError;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMManager$ACMError;->getErrorCode()Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0

    .line 62
    .end local v0    # "e":Lcom/cigna/coach/acm/ACMManager$ACMError;
    :catch_1
    move-exception v0

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown error occured during placeRequest: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {p0, v1}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->doErrorCallback(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    goto :goto_0
.end method

.method protected send()Z
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 92
    new-instance v9, Lcom/cigna/coach/acm/NetworkHeaderCreator;

    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/SamsungAccountManager;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/SamsungAccountManager;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v9, v0, v1}, Lcom/cigna/coach/acm/NetworkHeaderCreator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .local v9, "header":Lcom/cigna/coach/acm/NetworkHeaderCreator;
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Sending request"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-direct {p0, v9}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->printHeader(Ljava/util/HashMap;)V

    .line 97
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "samsungAccountManager. AccessToken : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/SamsungAccountManager;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    sget-object v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->CLASS_NAME:Ljava/lang/String;

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getJsonData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->logJSON(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mConnection:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getTaskReferenceId()I

    move-result v2

    sget-object v3, Lcom/sec/android/service/health/connectionmanager2/MethodType;->POST:Lcom/sec/android/service/health/connectionmanager2/MethodType;

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/cigna/coach/acm/NetworkPlaceRequest;->mTask:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v1}, Lcom/cigna/coach/acm/ACMTask;->getJsonData()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v7, p0

    move-object v8, v5

    invoke-virtual/range {v0 .. v9}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->placeRequest(Ljava/lang/Object;ILcom/sec/android/service/health/connectionmanager2/MethodType;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/RequestParam;Ljava/lang/String;Lcom/sec/android/service/health/connectionmanager2/IOnResponseListener;Ljava/lang/Object;Ljava/util/HashMap;)J

    move-result-wide v10

    .line 102
    .local v10, "requestId":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
