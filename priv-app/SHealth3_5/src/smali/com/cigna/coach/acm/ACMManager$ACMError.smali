.class public Lcom/cigna/coach/acm/ACMManager$ACMError;
.super Ljava/lang/RuntimeException;
.source "ACMManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/acm/ACMManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ACMError"
.end annotation


# instance fields
.field private code:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "code"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 42
    iput-object p2, p0, Lcom/cigna/coach/acm/ACMManager$ACMError;->code:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 43
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 0
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .param p3, "code"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 47
    iput-object p3, p0, Lcom/cigna/coach/acm/ACMManager$ACMError;->code:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 48
    return-void
.end method


# virtual methods
.method public getErrorCode()Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager$ACMError;->code:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    return-object v0
.end method
