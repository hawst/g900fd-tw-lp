.class public Lcom/cigna/coach/acm/ACMTask;
.super Ljava/lang/Object;
.source "ACMTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/acm/ACMTask$ACMTaskType;
    }
.end annotation


# instance fields
.field callback:Lcom/cigna/coach/acm/IACMTaskCompleteListener;

.field filepathForDownload:Ljava/lang/String;

.field jsonData:Ljava/lang/String;

.field taskReferenceId:I

.field taskType:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

.field url:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILcom/cigna/coach/acm/ACMTask$ACMTaskType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/acm/IACMTaskCompleteListener;)V
    .locals 1
    .param p1, "taskReferenceId"    # I
    .param p2, "taskType"    # Lcom/cigna/coach/acm/ACMTask$ACMTaskType;
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "jsonBodyData"    # Ljava/lang/String;
    .param p5, "filepathForDownload"    # Ljava/lang/String;
    .param p6, "callback"    # Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lcom/cigna/coach/acm/ACMTask;->taskReferenceId:I

    .line 37
    iput p1, p0, Lcom/cigna/coach/acm/ACMTask;->taskReferenceId:I

    .line 38
    iput-object p2, p0, Lcom/cigna/coach/acm/ACMTask;->taskType:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    .line 39
    iput-object p3, p0, Lcom/cigna/coach/acm/ACMTask;->url:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/cigna/coach/acm/ACMTask;->jsonData:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lcom/cigna/coach/acm/ACMTask;->filepathForDownload:Ljava/lang/String;

    .line 42
    iput-object p6, p0, Lcom/cigna/coach/acm/ACMTask;->callback:Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    .line 43
    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    new-instance v0, Lcom/cigna/coach/acm/ACMManager;

    invoke-direct {v0, p1, p0}, Lcom/cigna/coach/acm/ACMManager;-><init>(Landroid/content/Context;Lcom/cigna/coach/acm/ACMTask;)V

    .line 71
    return-void
.end method

.method public getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMTask;->callback:Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    return-object v0
.end method

.method public getFilepathForDownload()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMTask;->filepathForDownload:Ljava/lang/String;

    return-object v0
.end method

.method public getJsonData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMTask;->jsonData:Ljava/lang/String;

    return-object v0
.end method

.method public getTaskReferenceId()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/cigna/coach/acm/ACMTask;->taskReferenceId:I

    return v0
.end method

.method public getTaskType()Lcom/cigna/coach/acm/ACMTask$ACMTaskType;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMTask;->taskType:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMTask;->url:Ljava/lang/String;

    return-object v0
.end method
