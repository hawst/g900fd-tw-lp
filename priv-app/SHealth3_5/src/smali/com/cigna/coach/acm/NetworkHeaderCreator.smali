.class Lcom/cigna/coach/acm/NetworkHeaderCreator;
.super Ljava/util/HashMap;
.source "NetworkHeaderCreator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "[",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 10
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 13
    const-string/jumbo v1, "v2"

    .line 14
    .local v1, "ver":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bearer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 15
    .local v0, "auth":Ljava/lang/String;
    const-string v2, "accept"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "application/json"

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/cigna/coach/acm/NetworkHeaderCreator;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    const-string/jumbo v2, "x-osp-appId"

    new-array v3, v6, [Ljava/lang/String;

    aput-object p2, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/cigna/coach/acm/NetworkHeaderCreator;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    const-string/jumbo v2, "x-osp-version"

    new-array v3, v6, [Ljava/lang/String;

    aput-object v1, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/cigna/coach/acm/NetworkHeaderCreator;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    const-string v2, "Authorization"

    new-array v3, v6, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/cigna/coach/acm/NetworkHeaderCreator;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    const-string v2, "Content-Type"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "application/json"

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/cigna/coach/acm/NetworkHeaderCreator;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    return-void
.end method
