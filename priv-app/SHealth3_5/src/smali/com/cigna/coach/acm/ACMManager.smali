.class Lcom/cigna/coach/acm/ACMManager;
.super Ljava/lang/Object;
.source "ACMManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/acm/ACMManager$ACMError;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

.field private samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

.field private task:Lcom/cigna/coach/acm/ACMTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/cigna/coach/acm/ACMManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/cigna/coach/acm/ACMTask;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "task"    # Lcom/cigna/coach/acm/ACMTask;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    .line 60
    iput-object p2, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    .line 61
    invoke-static {p1}, Lcom/cigna/coach/acm/SamsungAccountManager;->getInstance(Landroid/content/Context;)Lcom/cigna/coach/acm/SamsungAccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    .line 62
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    new-instance v1, Lcom/cigna/coach/acm/ACMManager$1;

    invoke-direct {v1, p0, p2}, Lcom/cigna/coach/acm/ACMManager$1;-><init>(Lcom/cigna/coach/acm/ACMManager;Lcom/cigna/coach/acm/ACMTask;)V

    invoke-virtual {v0, v1}, Lcom/cigna/coach/acm/SamsungAccountManager;->getSamsungAccountInformation(Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)Z

    .line 91
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/cigna/coach/acm/ACMManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/acm/ACMManager;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/cigna/coach/acm/ACMManager;->initConnMan()V

    return-void
.end method

.method private download()V
    .locals 3

    .prologue
    .line 155
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Starting download task"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_0
    new-instance v0, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;

    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager;->connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/cigna/coach/acm/ACMTask;)V

    .line 159
    .local v0, "req":Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;
    invoke-virtual {v0}, Lcom/cigna/coach/acm/NetworkDownloadFromPublicURL;->start()V

    .line 160
    return-void
.end method

.method private initConnMan()V
    .locals 5

    .prologue
    .line 94
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Initializing connection manager"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    :cond_0
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->createInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 98
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManagerFactory;->getInstance(Ljava/lang/String;)Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/cigna/coach/acm/ACMManager;->connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    .line 100
    :try_start_0
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server name:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Server port:443"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager;->connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->initConnectionManager(ZLjava/lang/String;Ljava/lang/String;)Z

    .line 106
    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager;->connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    sget-object v2, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    const/16 v3, 0x1bb

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;->setAddress(Ljava/lang/String;I)Z
    :try_end_0
    .catch Lcom/sec/android/service/health/connectionmanager2/NetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Connection manager initialized"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_2
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Lcom/sec/android/service/health/connectionmanager2/NetException;
    new-instance v1, Lcom/cigna/coach/acm/ACMManager$ACMError;

    const-string v2, "Error while initializing connection manager"

    sget-object v3, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {v1, v2, v0, v3}, Lcom/cigna/coach/acm/ACMManager$ACMError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    throw v1
.end method

.method private invokeApi()V
    .locals 4

    .prologue
    .line 169
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    sget-object v1, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invoking api:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v3}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    new-instance v0, Lcom/cigna/coach/acm/NetworkPlaceRequest;

    iget-object v1, p0, Lcom/cigna/coach/acm/ACMManager;->connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager;->samsungAccountManager:Lcom/cigna/coach/acm/SamsungAccountManager;

    iget-object v3, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/acm/NetworkPlaceRequest;-><init>(Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;Lcom/cigna/coach/acm/SamsungAccountManager;Lcom/cigna/coach/acm/ACMTask;)V

    .line 173
    .local v0, "req":Lcom/cigna/coach/acm/NetworkPlaceRequest;
    invoke-virtual {v0}, Lcom/cigna/coach/acm/NetworkPlaceRequest;->start()V

    .line 175
    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 3

    .prologue
    .line 116
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Executing call"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getTaskType()Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-eqz v0, :cond_5

    .line 120
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getTaskType()Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->DOWNLOAD:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getFilepathForDownload()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 122
    invoke-direct {p0}, Lcom/cigna/coach/acm/ACMManager;->download()V

    .line 149
    :cond_1
    :goto_0
    return-void

    .line 124
    :cond_2
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Not enough parameters for download task"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getFilepathForDownload()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Callback:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/cigna/coach/acm/ACMManager$ACMError;

    const-string v1, "Not enough parameters for download task"

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_INVALID_SYNC_TYPE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/acm/ACMManager$ACMError;-><init>(Ljava/lang/String;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    throw v0

    .line 130
    :cond_3
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getTaskType()Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 131
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 132
    invoke-direct {p0}, Lcom/cigna/coach/acm/ACMManager;->invokeApi()V

    goto/16 :goto_0

    .line 134
    :cond_4
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Not enough parameters for api task"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URL:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Callback:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v2}, Lcom/cigna/coach/acm/ACMTask;->getCallback()Lcom/cigna/coach/acm/IACMTaskCompleteListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    new-instance v0, Lcom/cigna/coach/acm/ACMManager$ACMError;

    const-string v1, "Not enough parameters for api task"

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_INVALID_SYNC_TYPE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/acm/ACMManager$ACMError;-><init>(Ljava/lang/String;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    throw v0

    .line 141
    :cond_5
    sget-object v0, Lcom/cigna/coach/acm/ACMManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Task type is not specified or connection manager is null"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->task:Lcom/cigna/coach/acm/ACMTask;

    invoke-virtual {v0}, Lcom/cigna/coach/acm/ACMTask;->getTaskType()Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    move-result-object v0

    if-nez v0, :cond_6

    .line 143
    new-instance v0, Lcom/cigna/coach/acm/ACMManager$ACMError;

    const-string v1, "Task type is not specified"

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_INVALID_SYNC_TYPE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/acm/ACMManager$ACMError;-><init>(Ljava/lang/String;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    throw v0

    .line 145
    :cond_6
    iget-object v0, p0, Lcom/cigna/coach/acm/ACMManager;->connectionManager:Lcom/sec/android/service/health/connectionmanager2/ConnectionManager;

    if-nez v0, :cond_1

    .line 146
    new-instance v0, Lcom/cigna/coach/acm/ACMManager$ACMError;

    const-string v1, "connection manager is null"

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK_NOT_AVAILABLE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/acm/ACMManager$ACMError;-><init>(Ljava/lang/String;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    throw v0
.end method
