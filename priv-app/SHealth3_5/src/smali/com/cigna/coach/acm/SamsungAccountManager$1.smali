.class Lcom/cigna/coach/acm/SamsungAccountManager$1;
.super Ljava/lang/Object;
.source "SamsungAccountManager.java"

# interfaces
.implements Lcom/sec/android/service/health/samsungaccount/ISamsungUserTokenListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/coach/acm/SamsungAccountManager;->getSamsungAccountInformation(Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/coach/acm/SamsungAccountManager;

.field final synthetic val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;


# direct methods
.method constructor <init>(Lcom/cigna/coach/acm/SamsungAccountManager;Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->this$0:Lcom/cigna/coach/acm/SamsungAccountManager;

    iput-object p2, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "userToken"    # Ljava/lang/String;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "mcc"    # Ljava/lang/String;

    .prologue
    .line 48
    # getter for: Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Reading account information success"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    # getter for: Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->this$0:Lcom/cigna/coach/acm/SamsungAccountManager;

    # setter for: Lcom/cigna/coach/acm/SamsungAccountManager;->mAccessToken:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$102(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->this$0:Lcom/cigna/coach/acm/SamsungAccountManager;

    # setter for: Lcom/cigna/coach/acm/SamsungAccountManager;->mUserId:Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$202(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->this$0:Lcom/cigna/coach/acm/SamsungAccountManager;

    const-string v1, "1y90e30264"

    # setter for: Lcom/cigna/coach/acm/SamsungAccountManager;->mClientId:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$302(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->this$0:Lcom/cigna/coach/acm/SamsungAccountManager;

    # setter for: Lcom/cigna/coach/acm/SamsungAccountManager;->mMCC:Ljava/lang/String;
    invoke-static {v0, p3}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$402(Lcom/cigna/coach/acm/SamsungAccountManager;Ljava/lang/String;)Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-interface {v0, v1}, Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;->onFinish(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 58
    :cond_0
    return-void
.end method

.method public setFailureMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "errMsg"    # Ljava/lang/String;

    .prologue
    .line 63
    # getter for: Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reading account information failed:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-interface {v0, v1}, Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;->onFinish(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 67
    :cond_0
    return-void
.end method

.method public setNetworkFailure()V
    .locals 2

    .prologue
    .line 72
    # getter for: Lcom/cigna/coach/acm/SamsungAccountManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/acm/SamsungAccountManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Reading account information network failure"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/cigna/coach/acm/SamsungAccountManager$1;->val$listener:Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;

    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-interface {v0, v1}, Lcom/cigna/coach/acm/SamsungAccountManager$OnFinish;->onFinish(Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V

    .line 76
    :cond_0
    return-void
.end method
