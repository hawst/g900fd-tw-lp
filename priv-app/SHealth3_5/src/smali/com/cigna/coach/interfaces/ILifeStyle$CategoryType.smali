.class public final enum Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
.super Ljava/lang/Enum;
.source "ILifeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/ILifeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CategoryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field public static final enum NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field public static final enum PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field public static final enum SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field public static final enum STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field public static final enum WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field categoryTypeKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 78
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const-string v5, "PHYSICAL_ACTIVITY"

    const/16 v6, 0x64

    invoke-direct {v4, v5, v7, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 79
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const-string v5, "NUTRITION"

    const/16 v6, 0x65

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 80
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const-string v5, "SLEEP"

    const/16 v6, 0x66

    invoke-direct {v4, v5, v9, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 81
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const-string v5, "STRESS"

    const/16 v6, 0x67

    invoke-direct {v4, v5, v10, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 82
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const-string v5, "WEIGHT"

    const/16 v6, 0x68

    invoke-direct {v4, v5, v11, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 77
    const/4 v4, 0x5

    new-array v4, v4, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    aput-object v5, v4, v11

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 94
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->codeValueMap:Ljava/util/HashMap;

    .line 96
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 97
    .local v3, "type":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 99
    .end local v3    # "type":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "categoryTypeKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput p3, p0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->categoryTypeKey:I

    .line 90
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 2
    .param p0, "categoryTypeKey"    # I

    .prologue
    .line 108
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    const-class v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method


# virtual methods
.method public getCategoryTypeKey()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->categoryTypeKey:I

    return v0
.end method
