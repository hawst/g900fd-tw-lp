.class public final enum Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
.super Ljava/lang/Enum;
.source "IGoalsMissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IGoalsMissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GoalStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field public static final enum ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field public static final enum AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field public static final enum COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field public static final enum DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field public static final enum LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field public static final enum REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field public static final enum USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field statusKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 64
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const-string v5, "AVAILABLE"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 65
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const-string v5, "ACTIVE"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 66
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const-string v5, "COMPLETED"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 67
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const-string v5, "USER_CANCELED"

    invoke-direct {v4, v5, v10, v11}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 68
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const-string v5, "LSR_CANCELLED"

    invoke-direct {v4, v5, v11, v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 69
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const-string v5, "DO_NOT_LIKE"

    const/4 v6, 0x6

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 70
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const-string v5, "REMOVE"

    const/4 v6, 0x6

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 63
    const/4 v4, 0x7

    new-array v4, v4, [Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 87
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->codeValueMap:Ljava/util/HashMap;

    .line 89
    invoke-static {}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->values()[Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 90
    .local v3, "type":Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 92
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "sKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput p3, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->statusKey:I

    .line 78
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .locals 2
    .param p0, "statusKey"    # I

    .prologue
    .line 96
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 63
    const-class v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    return-object v0
.end method


# virtual methods
.method public getStatusKey()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->statusKey:I

    return v0
.end method
