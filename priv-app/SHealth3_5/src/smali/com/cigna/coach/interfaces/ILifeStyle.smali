.class public interface abstract Lcom/cigna/coach/interfaces/ILifeStyle;
.super Ljava/lang/Object;
.source "ILifeStyle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/interfaces/ILifeStyle$AccountChangeAction;,
        Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;,
        Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;,
        Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;,
        Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;,
        Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    }
.end annotation


# virtual methods
.method public abstract clearAllUserInformation()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract clearUserSelections()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getBadgeCount(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/BadgeInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getCategoryIdMsg(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CategoryInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getCoachMsg(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getCoachMsgs(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getQuestionAnswersByCat(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getScoreBackgroundImage(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getScores(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Ljava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/Scores;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getScoresHistory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/ScoreInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract handleUserAccountChange(Lcom/cigna/coach/interfaces/ILifeStyle$AccountChangeAction;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract initializeDatabase()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract setAnswersByCategory(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CategoryGroupQA;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CategoryGroupQA;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract setUserMetrics(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract updateUserId(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method
