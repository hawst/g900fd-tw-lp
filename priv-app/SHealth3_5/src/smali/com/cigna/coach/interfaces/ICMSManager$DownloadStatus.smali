.class public final enum Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;
.super Ljava/lang/Enum;
.source "ICMSManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/ICMSManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DownloadStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

.field public static final enum COMPLETED:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

.field public static final enum DOWNLOADING:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

.field public static final enum ERROR:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

.field public static final enum NONE:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->NONE:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    new-instance v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v3}, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->DOWNLOADING:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    new-instance v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v4}, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->COMPLETED:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    new-instance v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    const-string v1, "ERROR"

    invoke-direct {v0, v1, v5}, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->ERROR:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    sget-object v1, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->NONE:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->DOWNLOADING:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->COMPLETED:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->ERROR:Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->$VALUES:[Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->$VALUES:[Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;

    return-object v0
.end method
