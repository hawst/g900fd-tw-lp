.class public final enum Lcom/cigna/coach/interfaces/IWidget$WidgetType;
.super Ljava/lang/Enum;
.source "IWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WidgetType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IWidget$WidgetType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IWidget$WidgetType;

.field public static final enum COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

.field public static final enum SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IWidget$WidgetType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field widgetType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 52
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    const-string v5, "COACH_WIDGET"

    invoke-direct {v4, v5, v7, v6}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    const-string v5, "SHEALTH_WIDGET"

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .line 51
    new-array v4, v8, [Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    sget-object v5, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    aput-object v5, v4, v6

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->$VALUES:[Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .line 64
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->codeValueMap:Ljava/util/HashMap;

    .line 66
    invoke-static {}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->values()[Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 67
    .local v3, "type":Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    sget-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->getWidgetType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "widgetType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 59
    iput p3, p0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->widgetType:I

    .line 60
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .locals 2
    .param p0, "widgetTypekey"    # I

    .prologue
    .line 78
    sget-object v0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->$VALUES:[Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IWidget$WidgetType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    return-object v0
.end method


# virtual methods
.method public getWidgetType()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->widgetType:I

    return v0
.end method
