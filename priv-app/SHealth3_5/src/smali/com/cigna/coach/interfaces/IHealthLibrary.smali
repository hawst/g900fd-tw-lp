.class public interface abstract Lcom/cigna/coach/interfaces/IHealthLibrary;
.super Ljava/lang/Object;
.source "IHealthLibrary.java"


# virtual methods
.method public abstract addFavorite(Ljava/lang/String;I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getArticle(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getArticle(Ljava/lang/String;II)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getArticlesForSubCategory(Ljava/lang/String;II)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getCategories(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryCategory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getFavoritesByCategory(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getFavoritesByTitle(Ljava/lang/String;Z)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getFeaturedArticles(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getSubCategories(Ljava/lang/String;I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract removeFavorite(Ljava/lang/String;I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract searchArticles(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method
