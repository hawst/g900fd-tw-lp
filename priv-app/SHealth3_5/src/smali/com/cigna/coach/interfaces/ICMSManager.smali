.class public interface abstract Lcom/cigna/coach/interfaces/ICMSManager;
.super Ljava/lang/Object;
.source "ICMSManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/interfaces/ICMSManager$ICMSDownloadStatusCallback;,
        Lcom/cigna/coach/interfaces/ICMSManager$DownloadStatus;
    }
.end annotation


# virtual methods
.method public abstract downloadContentFile(Lcom/cigna/coach/interfaces/ICMSManager$ICMSDownloadStatusCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract isCoachSupportedLanguage(Ljava/util/Locale;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method
