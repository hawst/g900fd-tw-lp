.class public final enum Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
.super Ljava/lang/Enum;
.source "IGoalsMissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IGoalsMissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MissionStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field public static final enum USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field statusKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 102
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "AVAILABLE"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 103
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "ACTIVE"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 104
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "COMPLETED"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 105
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "USER_CANCELED"

    invoke-direct {v4, v5, v10, v11}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 106
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "LSR_CANCELLED"

    invoke-direct {v4, v5, v11, v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 107
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "DO_NOT_LIKE"

    const/4 v6, 0x6

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 108
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "REMOVE"

    const/4 v6, 0x6

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 109
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const-string v5, "FAILED"

    const/4 v6, 0x7

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 101
    const/16 v4, 0x8

    new-array v4, v4, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 126
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->codeValueMap:Ljava/util/HashMap;

    .line 128
    invoke-static {}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->values()[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 129
    .local v3, "type":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 131
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "sKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput p3, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->statusKey:I

    .line 117
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .locals 2
    .param p0, "statusKey"    # I

    .prologue
    .line 135
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 101
    const-class v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    return-object v0
.end method


# virtual methods
.method public getStatusKey()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->statusKey:I

    return v0
.end method
