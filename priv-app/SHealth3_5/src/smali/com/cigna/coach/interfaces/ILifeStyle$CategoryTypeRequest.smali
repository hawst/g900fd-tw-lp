.class public final enum Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
.super Ljava/lang/Enum;
.source "ILifeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/ILifeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CategoryTypeRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field public static final enum ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field public static final enum NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field public static final enum PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field public static final enum SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field public static final enum STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

.field public static final enum WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 156
    new-instance v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 157
    new-instance v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const-string v1, "NUTRITION"

    invoke-direct {v0, v1, v4}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 158
    new-instance v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const-string v1, "PHYSICAL_ACTIVITY"

    invoke-direct {v0, v1, v5}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 159
    new-instance v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const-string v1, "WEIGHT"

    invoke-direct {v0, v1, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 160
    new-instance v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const-string v1, "STRESS"

    invoke-direct {v0, v1, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 161
    new-instance v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    const-string v1, "SLEEP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .line 155
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    aput-object v1, v0, v6

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 155
    const-class v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .locals 1

    .prologue
    .line 155
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    return-object v0
.end method
