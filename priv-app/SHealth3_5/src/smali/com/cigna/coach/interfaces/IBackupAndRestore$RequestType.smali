.class public final enum Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
.super Ljava/lang/Enum;
.source "IBackupAndRestore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IBackupAndRestore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

.field public static final enum REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

.field public static final enum REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

.field public static final enum REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field requestType:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 113
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-string v5, "REQUEST_FOR_BACKUP"

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-string v5, "REQUEST_FOR_RESTORE"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-string v5, "REQUEST_FOR_ERASE"

    invoke-direct {v4, v5, v7, v9}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .line 112
    new-array v4, v9, [Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    aput-object v5, v4, v7

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->$VALUES:[Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .line 128
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->codeValueMap:Ljava/util/HashMap;

    .line 130
    invoke-static {}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->values()[Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 131
    .local v3, "type":Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 133
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "reqIdType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 120
    iput p3, p0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->requestType:I

    .line 122
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .locals 2
    .param p0, "requestType"    # I

    .prologue
    .line 142
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 112
    const-class v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->$VALUES:[Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    return-object v0
.end method


# virtual methods
.method public getRequestType()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->requestType:I

    return v0
.end method
