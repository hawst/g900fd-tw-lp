.class public final enum Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
.super Ljava/lang/Enum;
.source "ILifeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/ILifeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnswerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

.field public static final enum REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

.field public static final enum VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field answerType:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 166
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    const-string v5, "REFERENCE"

    invoke-direct {v4, v5, v6, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    .line 167
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    const-string v5, "VALUE"

    invoke-direct {v4, v5, v7, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    .line 165
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    aput-object v5, v4, v7

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    .line 179
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->codeValueMap:Ljava/util/HashMap;

    .line 181
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 182
    .local v3, "type":Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->getAnswerTypeKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 184
    .end local v3    # "type":Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "answerType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 174
    iput p3, p0, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->answerType:I

    .line 175
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    .locals 2
    .param p0, "scoreType"    # I

    .prologue
    .line 193
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 165
    const-class v0, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    return-object v0
.end method


# virtual methods
.method public getAnswerTypeKey()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->answerType:I

    return v0
.end method
