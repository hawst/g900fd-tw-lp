.class public interface abstract Lcom/cigna/coach/interfaces/IWidget;
.super Ljava/lang/Object;
.source "IWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;,
        Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    }
.end annotation


# virtual methods
.method public abstract dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method

.method public abstract getWidgetInfo(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getWidgetInfo(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation
.end method
