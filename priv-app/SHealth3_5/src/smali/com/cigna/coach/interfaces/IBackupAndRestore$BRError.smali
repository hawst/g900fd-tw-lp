.class public final enum Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
.super Ljava/lang/Enum;
.source "IBackupAndRestore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IBackupAndRestore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BRError"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_ALREADY_REQUESTED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_BUSY:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_CANCELLED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_DB_OPERATION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_INVALID_SYNC_TYPE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NETWORK:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NETWORK_NOT_AVAILABLE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NOT_RESPONDING:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NO_BACKUP_DATA:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NO_PERMISSION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NO_SAMSUNG_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_NO_SHEALTH_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_PROFILE_NOT_EXIST:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_SAMSUNG_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field public static final enum ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field taskErrorKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 43
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NONE"

    invoke-direct {v4, v5, v8, v8}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 45
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_BUSY"

    invoke-direct {v4, v5, v9, v9}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_BUSY:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 47
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NO_SAMSUNG_ACCOUNT"

    invoke-direct {v4, v5, v10, v10}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_SAMSUNG_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 49
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NETWORK_NOT_AVAILABLE"

    invoke-direct {v4, v5, v11, v11}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK_NOT_AVAILABLE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 51
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NETWORK"

    invoke-direct {v4, v5, v12, v12}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 53
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_SERVER"

    const/4 v6, 0x5

    const/4 v7, 0x5

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 55
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_INVALID_SYNC_TYPE"

    const/4 v6, 0x6

    const/4 v7, 0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_INVALID_SYNC_TYPE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 57
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NOT_RESPONDING"

    const/4 v6, 0x7

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NOT_RESPONDING:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 59
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_PROFILE_NOT_EXIST"

    const/16 v6, 0x8

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_PROFILE_NOT_EXIST:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 60
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_CANCELLED"

    const/16 v6, 0x9

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_CANCELLED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 61
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_ALREADY_REQUESTED"

    const/16 v6, 0xa

    const/16 v7, 0xa

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_ALREADY_REQUESTED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 62
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_SAMSUNG_ACCOUNT"

    const/16 v6, 0xb

    const/16 v7, 0xb

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SAMSUNG_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 63
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NO_PERMISSION"

    const/16 v6, 0xc

    const/16 v7, 0xc

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_PERMISSION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 65
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_DB_OPERATION"

    const/16 v6, 0xd

    const/16 v7, 0xd

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_DB_OPERATION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 67
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NO_BACKUP_DATA"

    const/16 v6, 0xe

    const/16 v7, 0xe

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_BACKUP_DATA:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 68
    new-instance v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "ERROR_NO_SHEALTH_ACCOUNT"

    const/16 v6, 0xf

    const/16 v7, 0xf

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_SHEALTH_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 41
    const/16 v4, 0x10

    new-array v4, v4, [Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_BUSY:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_SAMSUNG_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK_NOT_AVAILABLE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NETWORK:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v5, v4, v12

    const/4 v5, 0x5

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_INVALID_SYNC_TYPE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NOT_RESPONDING:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_PROFILE_NOT_EXIST:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_CANCELLED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_ALREADY_REQUESTED:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SAMSUNG_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_PERMISSION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_DB_OPERATION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0xe

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_BACKUP_DATA:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    const/16 v5, 0xf

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_SHEALTH_ACCOUNT:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->$VALUES:[Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 80
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->codeValueMap:Ljava/util/HashMap;

    .line 82
    invoke-static {}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->values()[Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 83
    .local v3, "type":Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->getBRErrorKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "taskErrorKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 75
    iput p3, p0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->taskErrorKey:I

    .line 76
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .locals 3
    .param p0, "taskErrorKey"    # I

    .prologue
    .line 94
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 95
    .local v0, "error":Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    if-nez v0, :cond_0

    .line 96
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_SERVER:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .line 97
    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const-class v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->$VALUES:[Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    return-object v0
.end method


# virtual methods
.method public getBRErrorKey()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->taskErrorKey:I

    return v0
.end method
