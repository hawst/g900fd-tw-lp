.class public final enum Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
.super Ljava/lang/Enum;
.source "ILifeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/ILifeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SubcategoryType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum ADDING_MEANING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum CARDIOVASCULAR:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum COPING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum EATING_HABITS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum ETOH:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum FATS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum FRUITS_VEGETABLES:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum SALTS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum STRETCHING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum SUGAR:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum WEIGHTS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field public static final enum WHOLE_GRAINS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field subcategoryTypeKey_:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 114
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "FRUITS_VEGETABLES"

    const/16 v6, 0xc8

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->FRUITS_VEGETABLES:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 115
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "FATS"

    const/16 v6, 0xc9

    invoke-direct {v4, v5, v9, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->FATS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 116
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "WHOLE_GRAINS"

    const/16 v6, 0xca

    invoke-direct {v4, v5, v10, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->WHOLE_GRAINS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 117
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "SALTS"

    const/16 v6, 0xcb

    invoke-direct {v4, v5, v11, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->SALTS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 118
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "SUGAR"

    const/16 v6, 0xcc

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->SUGAR:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 119
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "ETOH"

    const/4 v6, 0x5

    const/16 v7, 0xcd

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->ETOH:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 120
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "EATING_HABITS"

    const/4 v6, 0x6

    const/16 v7, 0xce

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->EATING_HABITS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 121
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "CARDIOVASCULAR"

    const/4 v6, 0x7

    const/16 v7, 0xcf

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->CARDIOVASCULAR:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 122
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "STRETCHING"

    const/16 v6, 0x8

    const/16 v7, 0xd0

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->STRETCHING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 123
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "WEIGHTS"

    const/16 v6, 0x9

    const/16 v7, 0xd1

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->WEIGHTS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 124
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "WEIGHT"

    const/16 v6, 0xa

    const/16 v7, 0xd2

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 125
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "SLEEP"

    const/16 v6, 0xb

    const/16 v7, 0xd3

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 126
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "COPING"

    const/16 v6, 0xc

    const/16 v7, 0xd4

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->COPING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 127
    new-instance v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    const-string v5, "ADDING_MEANING"

    const/16 v6, 0xd

    const/16 v7, 0xd5

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->ADDING_MEANING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 113
    const/16 v4, 0xe

    new-array v4, v4, [Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->FRUITS_VEGETABLES:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->FATS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->WHOLE_GRAINS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->SALTS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->SUGAR:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v5, v4, v12

    const/4 v5, 0x5

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->ETOH:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->EATING_HABITS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->CARDIOVASCULAR:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->STRETCHING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->WEIGHTS:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->COPING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->ADDING_MEANING:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 141
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->codeValueMap:Ljava/util/HashMap;

    .line 143
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 144
    .local v3, "type":Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->getSubcategoryTypeKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    .end local v3    # "type":Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "subcategoryTypeKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 132
    iput p3, p0, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->subcategoryTypeKey_:I

    .line 133
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    .locals 2
    .param p0, "subcategoryTypeKey"    # I

    .prologue
    .line 150
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 113
    const-class v0, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->$VALUES:[Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    return-object v0
.end method


# virtual methods
.method public getSubcategoryTypeKey()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->subcategoryTypeKey_:I

    return v0
.end method
