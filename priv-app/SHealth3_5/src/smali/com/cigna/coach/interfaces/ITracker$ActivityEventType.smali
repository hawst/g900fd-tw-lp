.class public final enum Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;
.super Ljava/lang/Enum;
.source "ITracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/ITracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityEventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

.field public static final enum DAILY:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

.field public static final enum DATE_CHANGE:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

.field public static final enum STARTUP:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;


# instance fields
.field activityEventType:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 58
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    const-string v1, "DAILY"

    invoke-direct {v0, v1, v4, v2}, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DAILY:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    .line 59
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    const-string v1, "STARTUP"

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->STARTUP:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    .line 60
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    const-string v1, "DATE_CHANGE"

    invoke-direct {v0, v1, v3, v5}, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DATE_CHANGE:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    .line 57
    new-array v0, v5, [Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DAILY:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->STARTUP:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DATE_CHANGE:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->$VALUES:[Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "aeType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->activityEventType:I

    .line 68
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->$VALUES:[Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    return-object v0
.end method
