.class public final enum Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
.super Ljava/lang/Enum;
.source "IGoalsMissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IGoalsMissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MissionFailedReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

.field public static final enum FORGOT:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

.field public static final enum NONE_OF_THESE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

.field public static final enum TOO_BUSY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

.field public static final enum TOO_EASY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

.field public static final enum TOO_HARD:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field reasonKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 141
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    const-string v5, "TOO_BUSY"

    invoke-direct {v4, v5, v11, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_BUSY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .line 142
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    const-string v5, "FORGOT"

    invoke-direct {v4, v5, v7, v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->FORGOT:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .line 143
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    const-string v5, "TOO_HARD"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_HARD:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .line 144
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    const-string v5, "TOO_EASY"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_EASY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .line 145
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    const-string v5, "NONE_OF_THESE"

    const/4 v6, 0x5

    invoke-direct {v4, v5, v10, v6}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->NONE_OF_THESE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .line 140
    const/4 v4, 0x5

    new-array v4, v4, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_BUSY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->FORGOT:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_HARD:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->TOO_EASY:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->NONE_OF_THESE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    aput-object v5, v4, v10

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .line 162
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->codeValueMap:Ljava/util/HashMap;

    .line 164
    invoke-static {}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->values()[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 165
    .local v3, "type":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->getReasonKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 167
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 152
    iput p3, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->reasonKey:I

    .line 153
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    .locals 2
    .param p0, "reasonKey"    # I

    .prologue
    .line 171
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 140
    const-class v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    return-object v0
.end method


# virtual methods
.method public getReasonKey()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->reasonKey:I

    return v0
.end method
