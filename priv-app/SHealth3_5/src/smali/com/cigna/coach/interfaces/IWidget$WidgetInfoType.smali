.class public final enum Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
.super Ljava/lang/Enum;
.source "IWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WidgetInfoType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum CATCH_ALL:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum GOAL_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum GOAL_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum MISSION_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum MISSION_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum NO_GOALS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field public static final enum NO_MISSIONS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field widgetInfoType:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 112
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "COACH_NOT_LAUNCHED"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 113
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "NO_GOALS_ENROLLED"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_GOALS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 114
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "NO_MISSIONS_ENROLLED"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_MISSIONS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 115
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "GOAL_COMPLETED"

    invoke-direct {v4, v5, v10, v11}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 116
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "MISSION_COMPLETED"

    invoke-direct {v4, v5, v11, v12}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 117
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "GOAL_STATUS_NOTIFICATION"

    const/4 v6, 0x6

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 118
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "MISSION_STATUS_NOTIFICATION"

    const/4 v6, 0x6

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 119
    new-instance v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const-string v5, "CATCH_ALL"

    const/4 v6, 0x7

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->CATCH_ALL:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 111
    const/16 v4, 0x8

    new-array v4, v4, [Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_GOALS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_MISSIONS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->CATCH_ALL:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->$VALUES:[Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 131
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->codeValueMap:Ljava/util/HashMap;

    .line 133
    invoke-static {}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->values()[Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 134
    .local v3, "type":Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    sget-object v4, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->getWidgetInfoType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "wType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    iput p3, p0, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->widgetInfoType:I

    .line 127
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .locals 2
    .param p0, "widgetInfoTypeKey"    # I

    .prologue
    .line 145
    sget-object v0, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 111
    const-class v0, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->$VALUES:[Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    return-object v0
.end method


# virtual methods
.method public getWidgetInfoType()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->widgetInfoType:I

    return v0
.end method
