.class public final enum Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
.super Ljava/lang/Enum;
.source "ITracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/ITracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrackerEventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

.field public static final enum EXERCISE:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

.field public static final enum FOOD:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

.field public static final enum PEDOMETER_DATA:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

.field public static final enum SLEEP:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

.field public static final enum WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;


# instance fields
.field trackerEventType:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 41
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    const-string v1, "EXERCISE"

    invoke-direct {v0, v1, v7, v3}, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->EXERCISE:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 42
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    const-string v1, "PEDOMETER_DATA"

    invoke-direct {v0, v1, v3, v4}, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->PEDOMETER_DATA:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 43
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    const-string v1, "FOOD"

    invoke-direct {v0, v1, v4, v5}, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->FOOD:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 44
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    const-string v1, "SLEEP"

    invoke-direct {v0, v1, v5, v6}, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->SLEEP:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 45
    new-instance v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    const-string v1, "WEIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    .line 40
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->EXERCISE:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->PEDOMETER_DATA:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->FOOD:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->SLEEP:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->$VALUES:[Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "teType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->trackerEventType:I

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->$VALUES:[Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    return-object v0
.end method
