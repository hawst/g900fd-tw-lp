.class public final enum Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
.super Ljava/lang/Enum;
.source "IGoalsMissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/interfaces/IGoalsMissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MissionProgressStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field public static final enum COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field public static final enum NOT_PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field public static final enum PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field statusKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 210
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    const-string v5, "PROGRESSED"

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 211
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    const-string v5, "NOT_PROGRESSED"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->NOT_PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 212
    new-instance v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    const-string v5, "COMPLETED"

    invoke-direct {v4, v5, v7, v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 209
    new-array v4, v9, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->NOT_PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    aput-object v5, v4, v7

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 229
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->codeValueMap:Ljava/util/HashMap;

    .line 231
    invoke-static {}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->values()[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 232
    .local v3, "type":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->getStatusKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 234
    .end local v3    # "type":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "sKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 218
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 219
    iput p3, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->statusKey:I

    .line 220
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    .locals 2
    .param p0, "statusKey"    # I

    .prologue
    .line 238
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 209
    const-class v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    .locals 1

    .prologue
    .line 209
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->$VALUES:[Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v0}, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    return-object v0
.end method


# virtual methods
.method public getStatusKey()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->statusKey:I

    return v0
.end method
