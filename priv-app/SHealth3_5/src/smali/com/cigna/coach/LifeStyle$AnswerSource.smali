.class public final enum Lcom/cigna/coach/LifeStyle$AnswerSource;
.super Ljava/lang/Enum;
.source "LifeStyle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/LifeStyle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AnswerSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/LifeStyle$AnswerSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/LifeStyle$AnswerSource;

.field public static final enum COACH:Lcom/cigna/coach/LifeStyle$AnswerSource;

.field public static final enum USER:Lcom/cigna/coach/LifeStyle$AnswerSource;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/LifeStyle$AnswerSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field answerSource:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 132
    new-instance v4, Lcom/cigna/coach/LifeStyle$AnswerSource;

    const-string v5, "USER"

    invoke-direct {v4, v5, v6, v6}, Lcom/cigna/coach/LifeStyle$AnswerSource;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/LifeStyle$AnswerSource;->USER:Lcom/cigna/coach/LifeStyle$AnswerSource;

    new-instance v4, Lcom/cigna/coach/LifeStyle$AnswerSource;

    const-string v5, "COACH"

    invoke-direct {v4, v5, v7, v7}, Lcom/cigna/coach/LifeStyle$AnswerSource;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/LifeStyle$AnswerSource;->COACH:Lcom/cigna/coach/LifeStyle$AnswerSource;

    .line 131
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/cigna/coach/LifeStyle$AnswerSource;

    sget-object v5, Lcom/cigna/coach/LifeStyle$AnswerSource;->USER:Lcom/cigna/coach/LifeStyle$AnswerSource;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/LifeStyle$AnswerSource;->COACH:Lcom/cigna/coach/LifeStyle$AnswerSource;

    aput-object v5, v4, v7

    sput-object v4, Lcom/cigna/coach/LifeStyle$AnswerSource;->$VALUES:[Lcom/cigna/coach/LifeStyle$AnswerSource;

    .line 149
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/LifeStyle$AnswerSource;->codeValueMap:Ljava/util/HashMap;

    .line 151
    invoke-static {}, Lcom/cigna/coach/LifeStyle$AnswerSource;->values()[Lcom/cigna/coach/LifeStyle$AnswerSource;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/LifeStyle$AnswerSource;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 152
    .local v3, "type":Lcom/cigna/coach/LifeStyle$AnswerSource;
    sget-object v4, Lcom/cigna/coach/LifeStyle$AnswerSource;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/LifeStyle$AnswerSource;->getAnswerSourceKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 154
    .end local v3    # "type":Lcom/cigna/coach/LifeStyle$AnswerSource;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "answerSource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 139
    iput p3, p0, Lcom/cigna/coach/LifeStyle$AnswerSource;->answerSource:I

    .line 140
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/LifeStyle$AnswerSource;
    .locals 2
    .param p0, "sourceType"    # I

    .prologue
    .line 158
    sget-object v0, Lcom/cigna/coach/LifeStyle$AnswerSource;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/LifeStyle$AnswerSource;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/LifeStyle$AnswerSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 131
    const-class v0, Lcom/cigna/coach/LifeStyle$AnswerSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/LifeStyle$AnswerSource;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/LifeStyle$AnswerSource;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/cigna/coach/LifeStyle$AnswerSource;->$VALUES:[Lcom/cigna/coach/LifeStyle$AnswerSource;

    invoke-virtual {v0}, [Lcom/cigna/coach/LifeStyle$AnswerSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/LifeStyle$AnswerSource;

    return-object v0
.end method


# virtual methods
.method public getAnswerSourceKey()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/cigna/coach/LifeStyle$AnswerSource;->answerSource:I

    return v0
.end method
