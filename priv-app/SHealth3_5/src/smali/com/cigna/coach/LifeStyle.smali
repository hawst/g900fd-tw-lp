.class public Lcom/cigna/coach/LifeStyle;
.super Ljava/lang/Object;
.source "LifeStyle.java"

# interfaces
.implements Lcom/cigna/coach/interfaces/ILifeStyle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/LifeStyle$1;,
        Lcom/cigna/coach/LifeStyle$AnswerSource;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field static categoryInfo:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CategoryInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    const-class v0, Lcom/cigna/coach/LifeStyle;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    .line 128
    const/4 v0, 0x0

    sput-object v0, Lcom/cigna/coach/LifeStyle;->categoryInfo:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context should not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iput-object p1, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    .line 167
    return-void
.end method

.method private getCategoryCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/cigna/coach/db/CoachSQLiteHelper;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 3
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p3, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 415
    sget-object v1, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    sget-object v1, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getCategoryCoachMessage"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v0, p3}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 419
    .local v0, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    invoke-virtual {v0, p1, p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCategorySpecificCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public clearAllUserInformation()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 178
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "clearAllUserInformation : START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v3, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-direct {v0, v3, v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 183
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v2, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v2, v0}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 184
    .local v2, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual {v2}, Lcom/cigna/coach/utils/UserIdUtils;->resetAllUserEntries()V

    .line 185
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V

    .line 187
    iget-object v3, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 192
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 193
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "clearUserSelections : END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_1
    return-void

    .line 188
    .end local v2    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catch_0
    move-exception v1

    .line 189
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    const-string v4, "Error in clearUserSelections."

    invoke-direct {v3, v4, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 192
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 193
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "clearUserSelections : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v3
.end method

.method public clearUserSelections()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 208
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "clearUserSelections : START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v3, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-direct {v0, v3, v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 213
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v2, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v2, v0}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 214
    .local v2, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual {v2}, Lcom/cigna/coach/utils/UserIdUtils;->resetUserEntries()V

    .line 215
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V

    .line 217
    iget-object v3, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 222
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 223
    sget-object v3, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "clearUserSelections : END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_1
    return-void

    .line 218
    .end local v2    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catch_0
    move-exception v1

    .line 219
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    const-string v4, "Error in clearUserSelections."

    invoke-direct {v3, v4, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 222
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 223
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "clearUserSelections : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v3
.end method

.method public getBadgeCount(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/BadgeInfo;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 279
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 280
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getBadgeCount : START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_0
    new-instance v4, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v9, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-direct {v4, v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V

    .line 284
    .local v4, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v3, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v3, v4}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 287
    .local v3, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    :try_start_0
    new-instance v5, Lcom/cigna/coach/db/BadgeDBManager;

    invoke-direct {v5, v4}, Lcom/cigna/coach/db/BadgeDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 289
    .local v5, "dbManager":Lcom/cigna/coach/db/BadgeDBManager;
    invoke-virtual {v5}, Lcom/cigna/coach/db/BadgeDBManager;->getCountOfAllBadges()I

    move-result v0

    .line 290
    .local v0, "availableCount":I
    invoke-virtual {v5, p1}, Lcom/cigna/coach/db/BadgeDBManager;->getEarnedBadgesForUser(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 291
    .local v7, "earnedBadges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/Badge;

    .line 292
    .local v1, "badge":Lcom/cigna/coach/apiobjects/Badge;
    invoke-virtual {v3, v1}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 302
    .end local v0    # "availableCount":I
    .end local v1    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    .end local v5    # "dbManager":Lcom/cigna/coach/db/BadgeDBManager;
    .end local v7    # "earnedBadges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v8    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v6

    .line 303
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    const-string v10, "Unable to retrieve badge information."

    invoke-direct {v9, v10, v6}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 305
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 306
    sget-object v10, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 307
    sget-object v10, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getBadgeCount : END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    throw v9

    .line 295
    .restart local v0    # "availableCount":I
    .restart local v5    # "dbManager":Lcom/cigna/coach/db/BadgeDBManager;
    .restart local v7    # "earnedBadges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v8    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_2
    new-instance v2, Lcom/cigna/coach/apiobjects/BadgeInfo;

    invoke-direct {v2}, Lcom/cigna/coach/apiobjects/BadgeInfo;-><init>()V

    .line 296
    .local v2, "badgeInfo":Lcom/cigna/coach/apiobjects/BadgeInfo;
    invoke-virtual {v2, v0}, Lcom/cigna/coach/apiobjects/BadgeInfo;->setAvailableBadges(I)V

    .line 297
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/cigna/coach/apiobjects/BadgeInfo;->setEarnedBadges(I)V

    .line 298
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v9

    sub-int v9, v0, v9

    invoke-virtual {v2, v9}, Lcom/cigna/coach/apiobjects/BadgeInfo;->setRemainingBadges(I)V

    .line 299
    invoke-virtual {v2, v7}, Lcom/cigna/coach/apiobjects/BadgeInfo;->setBadges(Ljava/util/List;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 305
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 306
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 307
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getBadgeCount : END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-object v2
.end method

.method public getCategoryIdMsg(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Ljava/util/List;
    .locals 7
    .param p1, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CategoryInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 482
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 483
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCategoryIdMsg : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V

    .line 487
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/utils/LifeStyleHelper;

    invoke-direct {v2, v0}, Lcom/cigna/coach/utils/LifeStyleHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 488
    .local v2, "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    const/4 v3, 0x0

    .line 490
    .local v3, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/cigna/coach/utils/LifeStyleHelper;->getCategoryIdMsg(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 494
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 495
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 496
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCategoryIdMsg : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    :cond_1
    return-object v3

    .line 491
    :catch_0
    move-exception v1

    .line 492
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error in getCategoryIdMsg."

    invoke-direct {v4, v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 495
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 496
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCategoryIdMsg : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getCoachMsg(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "coachSpecifcMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 325
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 326
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getCoachMsg : START : coachSpecifcMessageType= "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_0
    new-instance v4, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v7, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-direct {v4, v7, v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 330
    .local v4, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v3, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v3, v4}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 331
    .local v3, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    const/4 v1, 0x0

    .line 334
    .local v1, "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :try_start_0
    sget-object v7, Lcom/cigna/coach/LifeStyle$1;->$SwitchMap$com$cigna$coach$apiobjects$CoachMessage$CoachSpecificMessageType:[I

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessageType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 386
    new-instance v7, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Invalid CoachSpecifcMessageType value."

    invoke-direct {v7, v8}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 394
    :catch_0
    move-exception v5

    .line 395
    .local v5, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_1
    new-instance v7, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Unable to get Coaches Corner message."

    invoke-direct {v7, v8, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 397
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    :goto_1
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 398
    sget-object v8, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 399
    sget-object v8, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getCoachMsg : END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    throw v7

    .line 336
    :pswitch_0
    :try_start_2
    new-instance v6, Lcom/cigna/coach/db/UserMetricsDBManager;

    invoke-direct {v6, v4}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 337
    .local v6, "metricsDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    new-instance v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 338
    .end local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .local v2, "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    const/4 v7, 0x1

    :try_start_3
    invoke-virtual {v6, v7}, Lcom/cigna/coach/db/UserMetricsDBManager;->getHasSeenWelcomeScreen(I)Z

    move-result v7

    if-nez v7, :cond_4

    .line 339
    const/16 v7, 0x3e9

    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 343
    :goto_2
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lcom/cigna/coach/db/UserMetricsDBManager;->setHasSeenWelcomeScreen(IZ)V

    .line 344
    sget-object v7, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v2

    .line 389
    .end local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .end local v6    # "metricsDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    .restart local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :goto_3
    if-eqz v1, :cond_2

    .line 390
    :try_start_4
    invoke-virtual {v3, v1}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 392
    :cond_2
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 397
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 398
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 399
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getCoachMsg : END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_3
    return-object v1

    .line 341
    .end local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v6    # "metricsDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :cond_4
    const/16 v7, 0xe96

    :try_start_5
    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 394
    .end local v6    # "metricsDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :catch_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    goto :goto_0

    .line 348
    :pswitch_1
    :try_start_6
    new-instance v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 349
    .end local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    const/16 v7, 0xf8a

    :try_start_7
    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 350
    sget-object v7, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v1, v2

    .line 351
    .end local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    goto :goto_3

    .line 354
    :pswitch_2
    :try_start_8
    sget-object v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, p1, v7, v4}, Lcom/cigna/coach/LifeStyle;->getCategoryCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/cigna/coach/db/CoachSQLiteHelper;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-object v1, v0

    .line 355
    goto :goto_3

    .line 358
    :pswitch_3
    sget-object v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, p1, v7, v4}, Lcom/cigna/coach/LifeStyle;->getCategoryCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/cigna/coach/db/CoachSQLiteHelper;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-object v1, v0

    .line 359
    goto :goto_3

    .line 362
    :pswitch_4
    sget-object v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, p1, v7, v4}, Lcom/cigna/coach/LifeStyle;->getCategoryCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/cigna/coach/db/CoachSQLiteHelper;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-object v1, v0

    .line 363
    goto :goto_3

    .line 366
    :pswitch_5
    sget-object v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, p1, v7, v4}, Lcom/cigna/coach/LifeStyle;->getCategoryCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/cigna/coach/db/CoachSQLiteHelper;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-object v1, v0

    .line 367
    goto :goto_3

    .line 370
    :pswitch_6
    sget-object v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-direct {p0, p1, v7, v4}, Lcom/cigna/coach/LifeStyle;->getCategoryCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Lcom/cigna/coach/db/CoachSQLiteHelper;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-object v1, v0

    .line 371
    goto :goto_3

    .line 374
    :pswitch_7
    new-instance v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 375
    .end local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    const/16 v7, 0x3e9

    :try_start_9
    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 376
    sget-object v7, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-object v1, v2

    .line 377
    .end local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    goto :goto_3

    .line 380
    :pswitch_8
    :try_start_a
    new-instance v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 381
    .end local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    const/16 v7, 0xe96

    :try_start_b
    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 382
    sget-object v7, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v2, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-object v1, v2

    .line 383
    .end local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    goto/16 :goto_3

    .line 397
    .end local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :catchall_1
    move-exception v7

    move-object v1, v2

    .end local v2    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .restart local v1    # "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    goto/16 :goto_1

    .line 334
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getCoachMsgs(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;)Ljava/util/List;
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "coachSpecifcMessagesType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 433
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 434
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCoachMsgs : START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_0
    const/4 v0, 0x0

    .line 438
    .local v0, "coachList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v3, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v5, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-direct {v3, v5, v6}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 439
    .local v3, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v1, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v1, v3}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 440
    .local v1, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    new-instance v2, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v2, v3}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 443
    .local v2, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    :try_start_0
    sget-object v5, Lcom/cigna/coach/LifeStyle$1;->$SwitchMap$com$cigna$coach$apiobjects$CoachMessage$CoachSpecificMessagesType:[I

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachSpecificMessagesType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 449
    new-instance v5, Lcom/cigna/coach/exceptions/CoachException;

    const-string v6, "Invalid CoachSpecifcMessagesType value."

    invoke-direct {v5, v6}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 457
    :catch_0
    move-exception v4

    .line 458
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v5, Lcom/cigna/coach/exceptions/CoachException;

    const-string v6, "Unable to get Coach messages."

    invoke-direct {v5, v6, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 461
    sget-object v6, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 462
    sget-object v6, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getCoachMsgs : END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    throw v5

    .line 445
    :pswitch_0
    :try_start_2
    invoke-virtual {v1, p1}, Lcom/cigna/coach/utils/CoachMessageHelper;->getReassesCoachMessage(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 452
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 453
    invoke-virtual {v2, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 455
    :cond_2
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 460
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 461
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 462
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCoachMsgs : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_3
    return-object v0

    .line 443
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getQuestionAnswersByCat(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 517
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 518
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getQuestionAnswersByCat("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ") : START"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :cond_0
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v9, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-direct {v2, v9, v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 522
    .local v2, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    const/4 v0, 0x0

    .line 524
    .local v0, "categoryGroupQAs":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    :try_start_0
    new-instance v5, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-direct {v5, v2}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 525
    .local v5, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    invoke-virtual {v5, p1, p2}, Lcom/cigna/coach/db/LifeStyleDBManager;->getLifeStyleAssesmentQA(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/CategoryGroupQAs;

    move-result-object v0

    .line 526
    new-instance v8, Lcom/cigna/coach/utils/TrackerHelper;

    iget-object v9, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-direct {v8, v2, v9}, Lcom/cigna/coach/utils/TrackerHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;Landroid/content/Context;)V

    .line 527
    .local v8, "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    iget-object v9, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-virtual {v8, v9, v0}, Lcom/cigna/coach/utils/TrackerHelper;->setTrackerSuggestedAnswers(Landroid/content/Context;Lcom/cigna/coach/apiobjects/CategoryGroupQAs;)V

    .line 528
    if-eqz v0, :cond_2

    .line 529
    new-instance v1, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v1, v2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 530
    .local v1, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;->values()Ljava/util/Collection;

    move-result-object v7

    .line 531
    .local v7, "qasKeys":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;"
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 532
    .local v6, "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    invoke-virtual {v1, v6}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 536
    .end local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v6    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .end local v7    # "qasKeys":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;"
    .end local v8    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    :catch_0
    move-exception v3

    .line 537
    .local v3, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    const-string v10, "Error in getQuestionAnswersByCat."

    invoke-direct {v9, v10, v3}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 539
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 540
    sget-object v10, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 541
    sget-object v10, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getQuestionAnswersByCat : END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    throw v9

    .line 535
    .restart local v5    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .restart local v8    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    :cond_2
    :try_start_2
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 539
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 540
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 541
    sget-object v9, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getQuestionAnswersByCat : END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    :cond_3
    return-object v0
.end method

.method public getScoreBackgroundImage(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 557
    sget-object v6, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 558
    sget-object v6, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getScoreBackgroundImage : START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_0
    new-instance v3, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v6, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    const/4 v7, 0x1

    invoke-direct {v3, v6, v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 562
    .local v3, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    const/4 v0, 0x0

    .line 564
    .local v0, "backgroundImage":Ljava/lang/String;
    :try_start_0
    new-instance v5, Lcom/cigna/coach/utils/LifeStyleHelper;

    invoke-direct {v5, v3}, Lcom/cigna/coach/utils/LifeStyleHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 565
    .local v5, "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    invoke-virtual {v5, p1}, Lcom/cigna/coach/utils/LifeStyleHelper;->getScoreBackgroundImage(Ljava/lang/String;)I

    move-result v1

    .line 567
    .local v1, "backgroundImageContentId":I
    new-instance v2, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v2, v3}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 568
    .local v2, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    const/4 v6, 0x0

    invoke-virtual {v2, v1, v6}, Lcom/cigna/coach/utils/ContentHelper;->getContentTextForCurrentLocale(ILcom/cigna/coach/utils/ContextData;)Ljava/lang/String;

    move-result-object v0

    .line 570
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 574
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 575
    sget-object v6, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 576
    sget-object v6, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getScoreBackgroundImage : END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    :cond_1
    return-object v0

    .line 571
    .end local v1    # "backgroundImageContentId":I
    .end local v2    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v5    # "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    :catch_0
    move-exception v4

    .line 572
    .local v4, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v6, Lcom/cigna/coach/exceptions/CoachException;

    const-string v7, "Error in getScoreBackgroundImage."

    invoke-direct {v6, v7, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 575
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 576
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getScoreBackgroundImage : END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v6
.end method

.method public getScores(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;Ljava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/Scores;
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .param p3, "asOfdateTime"    # Ljava/util/Calendar;
    .param p4, "returnCategoryScoreMsg"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 599
    sget-object v11, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 600
    sget-object v11, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getScores : START"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_0
    new-instance v4, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v11, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-direct {v4, v11}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V

    .line 605
    .local v4, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    const/4 v9, 0x0

    .line 607
    .local v9, "returnValue":Lcom/cigna/coach/dataobjects/ScoresData;
    :try_start_0
    new-instance v8, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-direct {v8, v4}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 609
    .local v8, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    move-object/from16 v0, p3

    invoke-virtual {v8, p1, v0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserOverallScore(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/ScoresData;

    move-result-object v9

    .line 612
    new-instance v7, Lcom/cigna/coach/utils/LifeStyleHelper;

    invoke-direct {v7, v4}, Lcom/cigna/coach/utils/LifeStyleHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 613
    .local v7, "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/cigna/coach/utils/LifeStyleHelper;->getCategoryIdMsg(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Ljava/util/List;

    move-result-object v2

    .line 614
    .local v2, "cats":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_2

    .line 616
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/CategoryInfo;

    .line 618
    .local v1, "cat":Lcom/cigna/coach/apiobjects/CategoryInfo;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CategoryInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v11

    move-object/from16 v0, p3

    invoke-virtual {v8, v11, p1, v0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserScoreForCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/util/Calendar;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 620
    .local v10, "scoreForCategory":I
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CategoryInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v11

    invoke-virtual {v9, v11, v10}, Lcom/cigna/coach/dataobjects/ScoresData;->setCategoryScore(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 628
    .end local v1    # "cat":Lcom/cigna/coach/apiobjects/CategoryInfo;
    .end local v2    # "cats":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    .end local v8    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v10    # "scoreForCategory":I
    :catch_0
    move-exception v5

    .line 629
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v11, Lcom/cigna/coach/exceptions/CoachException;

    const-string v12, "Error in getScores."

    invoke-direct {v11, v12, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 631
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v11

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 632
    sget-object v12, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 633
    sget-object v12, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getScores : END"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    throw v11

    .line 624
    .restart local v2    # "cats":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .restart local v7    # "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    .restart local v8    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    :cond_2
    :try_start_2
    new-instance v3, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v3, v4}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 625
    .local v3, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v3, v9}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 627
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 631
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 632
    sget-object v11, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 633
    sget-object v11, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getScores : END"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    :cond_3
    return-object v9
.end method

.method public getScoresHistory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 22
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "scoreGroupingType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/ScoreInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 652
    sget-object v19, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 653
    sget-object v19, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v20, "getScoresHistory : START"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    :cond_0
    new-instance v10, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v10, v0, v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 659
    .local v10, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v18, Lcom/cigna/coach/utils/ScoringHelper;

    move-object/from16 v0, v18

    invoke-direct {v0, v10}, Lcom/cigna/coach/utils/ScoringHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 660
    .local v18, "scoringHelper":Lcom/cigna/coach/utils/ScoringHelper;
    new-instance v15, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v15}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V

    .line 662
    .local v15, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/ScoringHelper;->getUserLifeStyleScoreHistory(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/ScoreInfoData;

    move-result-object v17

    .line 663
    .local v17, "scoreHistory":Lcom/cigna/coach/dataobjects/ScoreInfoData;
    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->summarize(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V

    .line 664
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 666
    new-instance v7, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v7, v10}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 667
    .local v7, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v15}, Lcom/cigna/coach/utils/CoachMessageHelper;->setGetScoresMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 670
    new-instance v8, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v8, v10}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 671
    .local v8, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 674
    new-instance v13, Lcom/cigna/coach/utils/LifeStyleHelper;

    invoke-direct {v13, v10}, Lcom/cigna/coach/utils/LifeStyleHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 675
    .local v13, "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    new-instance v14, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-direct {v14, v10}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 677
    .local v14, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    sget-object v19, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/cigna/coach/utils/LifeStyleHelper;->getCategoryIdMsg(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Ljava/util/List;

    move-result-object v5

    .line 678
    .local v5, "cats":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 680
    .local v6, "catsWithNoScore":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v5, :cond_3

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v19

    if-lez v19, :cond_3

    .line 682
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/CategoryInfo;

    .line 683
    .local v3, "cat":Lcom/cigna/coach/apiobjects/CategoryInfo;
    move-object v0, v3

    check-cast v0, Lcom/cigna/coach/dataobjects/CategoryInfoData;

    move-object v4, v0

    .line 685
    .local v4, "catd":Lcom/cigna/coach/dataobjects/CategoryInfoData;
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/CategoryInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-virtual {v14, v0, v1, v2}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserScoreForCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/util/Calendar;)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 686
    .local v16, "scoreForCategory":I
    if-gez v16, :cond_1

    .line 687
    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/CategoryInfoData;->getContentId()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 703
    .end local v3    # "cat":Lcom/cigna/coach/apiobjects/CategoryInfo;
    .end local v4    # "catd":Lcom/cigna/coach/dataobjects/CategoryInfoData;
    .end local v5    # "cats":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .end local v6    # "catsWithNoScore":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v7    # "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    .end local v8    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v13    # "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    .end local v14    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v15    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    .end local v16    # "scoreForCategory":I
    .end local v17    # "scoreHistory":Lcom/cigna/coach/dataobjects/ScoreInfoData;
    .end local v18    # "scoringHelper":Lcom/cigna/coach/utils/ScoringHelper;
    :catch_0
    move-exception v11

    .line 704
    .local v11, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v19, Lcom/cigna/coach/exceptions/CoachException;

    const-string v20, "Error in getScoresHistory."

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v11}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 706
    .end local v11    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v19

    invoke-virtual {v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 707
    sget-object v20, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 708
    sget-object v20, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "getScoresHistory : END"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v19

    .line 692
    .restart local v5    # "cats":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .restart local v6    # "catsWithNoScore":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v7    # "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    .restart local v8    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v13    # "lifeStyleHelper":Lcom/cigna/coach/utils/LifeStyleHelper;
    .restart local v14    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .restart local v15    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    .restart local v17    # "scoreHistory":Lcom/cigna/coach/dataobjects/ScoreInfoData;
    .restart local v18    # "scoringHelper":Lcom/cigna/coach/utils/ScoringHelper;
    :cond_3
    :try_start_2
    new-instance v9, Lcom/cigna/coach/utils/ContextData;

    invoke-direct {v9}, Lcom/cigna/coach/utils/ContextData;-><init>()V

    .line 696
    .local v9, "contextData":Lcom/cigna/coach/utils/ContextData;
    sget-object v19, Lcom/cigna/coach/utils/ContextData$ContextDataType;->CURRENT_SCORE_MESSAGE:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getCurrentScoreContentId()I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V

    .line 697
    sget-object v19, Lcom/cigna/coach/utils/ContextData$ContextDataType;->CATEGORY_LIST:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    move-object/from16 v0, v19

    invoke-virtual {v9, v0, v6}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;Ljava/util/Collection;)V

    .line 699
    invoke-virtual {v8, v15, v9}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;)V

    .line 701
    invoke-virtual {v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 706
    invoke-virtual {v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 707
    sget-object v19, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 708
    sget-object v19, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v20, "getScoresHistory : END"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-object v15
.end method

.method public handleUserAccountChange(Lcom/cigna/coach/interfaces/ILifeStyle$AccountChangeAction;)V
    .locals 1
    .param p1, "action"    # Lcom/cigna/coach/interfaces/ILifeStyle$AccountChangeAction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 890
    if-eqz p1, :cond_0

    .line 891
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$AccountChangeAction;->ACCOUNT_REMOVED:Lcom/cigna/coach/interfaces/ILifeStyle$AccountChangeAction;

    if-ne p1, v0, :cond_0

    .line 893
    invoke-virtual {p0}, Lcom/cigna/coach/LifeStyle;->clearAllUserInformation()V

    .line 897
    :cond_0
    return-void
.end method

.method public initializeDatabase()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 722
    sget-object v1, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 723
    sget-object v1, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "initializeDatabase : START"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    :cond_0
    :try_start_0
    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {p0, v1}, Lcom/cigna/coach/LifeStyle;->getCategoryIdMsg(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 735
    sget-object v1, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 736
    sget-object v1, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "initializeDatabase : END"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :cond_1
    return-void

    .line 730
    :catch_0
    move-exception v0

    .line 732
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v1, Lcom/cigna/coach/exceptions/CoachException;

    const-string v2, "Error in initializeDatabase."

    invoke-direct {v1, v2, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 735
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 736
    sget-object v2, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "initializeDatabase : END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v1
.end method

.method public setAnswersByCategory(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CategoryGroupQA;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 26
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryGroupQA"    # Lcom/cigna/coach/apiobjects/CategoryGroupQA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CategoryGroupQA;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 760
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 761
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "setAnswersByCategory : START"

    invoke-static {v4, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    :cond_0
    new-instance v18, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    const/4 v7, 0x1

    move-object/from16 v0, v18

    invoke-direct {v0, v4, v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 765
    .local v18, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v9, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v9}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V

    .line 766
    .local v9, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;>;"
    new-instance v16, Lcom/cigna/coach/utils/CoachMessageHelper;

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 767
    .local v16, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v14

    .line 771
    .local v14, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    :try_start_0
    new-instance v22, Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 774
    .local v22, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    new-instance v3, Lcom/cigna/coach/utils/ScoringHelper;

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Lcom/cigna/coach/utils/ScoringHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 776
    .local v3, "scorer":Lcom/cigna/coach/utils/ScoringHelper;
    if-eqz p2, :cond_3

    .line 777
    const/4 v11, 0x0

    .line 779
    .local v11, "isAnyUnalignedGoalCancelled":Z
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CategoryGroupQA;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 780
    .local v20, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 782
    .local v5, "categoryInAnswer":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 784
    .local v6, "answersForCategory":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    sget-object v4, Lcom/cigna/coach/LifeStyle$AnswerSource;->USER:Lcom/cigna/coach/LifeStyle$AnswerSource;

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6, v4}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentAnswers(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/LifeStyle$AnswerSource;)V

    .line 786
    const/4 v7, -0x1

    sget-object v8, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/cigna/coach/utils/ScoringHelper;->computeScoreForCategoryBasedOnUserResponse(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List;ILcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)Z

    move-result v4

    or-int/2addr v11, v4

    .line 789
    goto :goto_0

    .line 792
    .end local v5    # "categoryInAnswer":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v6    # "answersForCategory":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    .end local v20    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;>;"
    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/cigna/coach/utils/ScoringHelper;->computeOverallScore(Ljava/lang/String;)I

    move-result v25

    .line 793
    .local v25, "overAllScore":I
    if-ltz v25, :cond_2

    .line 795
    sget-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    const/4 v7, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    move/from16 v2, v25

    invoke-virtual {v0, v1, v4, v7, v2}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V

    .line 796
    move/from16 v0, v25

    invoke-virtual {v14, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendScoreChangedBroadcast(I)V

    .line 799
    :cond_2
    new-instance v13, Lcom/cigna/coach/utils/BadgeAwarder;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-direct {v13, v0, v1}, Lcom/cigna/coach/utils/BadgeAwarder;-><init>(Ljava/lang/String;Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 800
    .local v13, "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    invoke-virtual {v13}, Lcom/cigna/coach/utils/BadgeAwarder;->awardBadges()Ljava/util/List;

    move-result-object v12

    .line 801
    .local v12, "badgesEarned":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    invoke-virtual {v9, v12}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 803
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CategoryGroupQA;->keySet()Ljava/util/Set;

    move-result-object v15

    .line 804
    .local v15, "categoryTypeKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v15}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .local v10, "categoryTypes":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    move-object/from16 v7, v16

    move-object/from16 v8, p1

    .line 805
    invoke-virtual/range {v7 .. v12}, Lcom/cigna/coach/utils/CoachMessageHelper;->setQuestionAnswerMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;Ljava/util/List;ZLjava/util/List;)V

    .line 811
    .end local v10    # "categoryTypes":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .end local v11    # "isAnyUnalignedGoalCancelled":Z
    .end local v12    # "badgesEarned":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v13    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .end local v15    # "categoryTypeKeys":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .end local v21    # "i$":Ljava/util/Iterator;
    .end local v25    # "overAllScore":I
    :cond_3
    new-instance v23, Lcom/cigna/coach/utils/NotificationHelper;

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/NotificationHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 812
    .local v23, "nh":Lcom/cigna/coach/utils/NotificationHelper;
    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachResponseToNotifications(Lcom/cigna/coach/apiobjects/CoachResponse;)Ljava/util/List;

    move-result-object v24

    .line 814
    .local v24, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    new-instance v17, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct/range {v17 .. v18}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 815
    .local v17, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 816
    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 818
    if-eqz v24, :cond_7

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_7

    .line 819
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 820
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "processing notification List"

    invoke-static {v4, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 822
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/NotificationEngine;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->sendNotifications(Ljava/util/List;)Z

    .line 829
    :cond_5
    :goto_1
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 834
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 835
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 836
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "setAnswersByCategory : END"

    invoke-static {v4, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    :cond_6
    return-object v9

    .line 824
    :cond_7
    :try_start_1
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 825
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "No notifications to send"

    invoke-static {v4, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 830
    .end local v3    # "scorer":Lcom/cigna/coach/utils/ScoringHelper;
    .end local v17    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v22    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v23    # "nh":Lcom/cigna/coach/utils/NotificationHelper;
    .end local v24    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :catch_0
    move-exception v19

    .line 831
    .local v19, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Error while saving answers"

    move-object/from16 v0, v19

    invoke-static {v4, v7, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 832
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v7, "Error while saving answer."

    move-object/from16 v0, v19

    invoke-direct {v4, v7, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 834
    .end local v19    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 835
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 836
    sget-object v7, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "setAnswersByCategory : END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v4
.end method

.method public setUserMetrics(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics;)Z
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "userMetrics"    # Lcom/cigna/coach/apiobjects/UserMetrics;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 240
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 241
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "setUserMetrics : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    invoke-direct {v0, v4, v6}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 246
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v2, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-direct {v2, v0}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 249
    .local v2, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    new-instance v3, Lcom/cigna/coach/dataobjects/UserInfo;

    invoke-direct {v3}, Lcom/cigna/coach/dataobjects/UserInfo;-><init>()V

    .line 250
    .local v3, "user":Lcom/cigna/coach/dataobjects/UserInfo;
    invoke-virtual {v3, p1}, Lcom/cigna/coach/dataobjects/UserInfo;->setUserId(Ljava/lang/String;)V

    .line 251
    invoke-virtual {v3, p2}, Lcom/cigna/coach/dataobjects/UserInfo;->populateFromUserMetrics(Lcom/cigna/coach/apiobjects/UserMetrics;)V

    .line 253
    invoke-virtual {v2, v3}, Lcom/cigna/coach/db/LifeStyleDBManager;->createOrUpdateUserInformation(Lcom/cigna/coach/dataobjects/UserInfo;)Z

    .line 255
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 261
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 262
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "setUserMetrics : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return v6

    .line 257
    .end local v2    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v3    # "user":Lcom/cigna/coach/dataobjects/UserInfo;
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Unable to set user metrics."

    invoke-direct {v4, v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 261
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 262
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "setUserMetrics : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public updateUserId(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "currentUserId"    # Ljava/lang/String;
    .param p2, "newUserId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 856
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 857
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserId : START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    :cond_0
    const/4 v2, 0x0

    .line 862
    .local v2, "success":Z
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/LifeStyle;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v0, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 864
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v3, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v3, v0}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 865
    .local v3, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual {v3, p1, p2}, Lcom/cigna/coach/utils/UserIdUtils;->updateUserName(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 866
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 871
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 872
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 873
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserId : END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    :cond_1
    return v2

    .line 867
    .end local v3    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :catch_0
    move-exception v1

    .line 868
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error while trying to update user id"

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 869
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error while trying to update user id"

    invoke-direct {v4, v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 871
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 872
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 873
    sget-object v5, Lcom/cigna/coach/LifeStyle;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserId : END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method
