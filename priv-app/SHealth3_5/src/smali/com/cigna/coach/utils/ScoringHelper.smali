.class public Lcom/cigna/coach/utils/ScoringHelper;
.super Ljava/lang/Object;
.source "ScoringHelper.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/ScoringHelper$1;,
        Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field static final MODERATE_ACTIVITY_QUESTION_GROUP:I = 0x8

.field static final NUMBER_OF_DAYS_TILL_REASSESSMENT:I = 0xb4

.field static final NUMBER_OF_MONTHS_TILL_REASSESSMENT:F = 6.0f

.field private static final categoryWeight:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final goalSelectExpression:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final questionGroupScoringExpression:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final questionGroupWeight:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

.field private evaluator:Lcom/cigna/coach/utils/ExpressionEvaluator;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0x32

    .line 67
    const-class v0, Lcom/cigna/coach/utils/ScoringHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper;->categoryWeight:Ljava/util/Map;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    .line 90
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->categoryWeight:Ljava/util/Map;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->categoryWeight:Ljava/util/Map;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->categoryWeight:Ljava/util/Map;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->categoryWeight:Ljava/util/Map;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->categoryWeight:Ljava/util/Map;

    sget-object v1, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==1? 5 : (answerId==2? 25 : ( answerId==3? 90 : (answerId==4? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==5? 5 : (answerId==6? 25 : ( answerId==7? 100 : (answerId==8? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==9? 100 : (answerId==10? 90 : ( answerId==11? 25 : (answerId==12? 5  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==13? 5 : (answerId==14? 25 : ( answerId==15? 90 : (answerId==16? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==17? 5 : (answerId==18? 25 : ( answerId==19? 100 : (answerId==20? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==21? 100 : (answerId==22? 90 : ( answerId==23? (gender==\"male\" ? 40 : 5) : (answerId==24? (gender==\"male\" ? 5 : 1) : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==25? 100 : (answerId==26? 100 : ( answerId==27? 50 : (answerId==28? 5  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==29? 5 : (answerId==30? 60 : ( answerId==31? 80 : (answerId==32? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==33? 25 : (answerId==34? 80 : ( answerId==35? 100 : (answerId==36? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==37? 5 : (answerId==38? 80 : ( answerId==39? 100 : (answerId==40? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==41? 25 : (answerId==42? 90 : ( answerId==43? 100 : (answerId==44? 100  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "( bmi<18.5? (  100* (1 - ($math.pow((18.5 - bmi),1.87) * 0.15))        ) : ( bmi>=18.5 && bmi < 25 ? 100 : (  100* (1 - ($math.pow((bmi-25),1.87) * 0.15))    ) )  ) "

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==47? 25 : (answerId==48? 100 : ( answerId==49? 90 : (answerId==50? 70  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==51? 100 : (answerId==52? 100 : ( answerId==53? 15 : (answerId==54? 5  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==55? 100 : (answerId==56? 90 : ( answerId==57? 15 : (answerId==58? 5  : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId== 1?  1 : (answerId== 2?  2 : ( answerId== 3?  3 : (answerId== 4?  4 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId== 5?  5 : (answerId== 6?  6 : ( answerId== 7?  7 : (answerId== 8?  8 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId== 9?  9 : (answerId==10? 10 : ( answerId==11? 11 : (answerId==12? 12 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==13? 13 : (answerId==14? 14 : ( answerId==15? 15 : (answerId==16? 16 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==17? 17 : (answerId==18? 18 : ( answerId==19? 19 : (answerId==20? 20 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==21?  0 : (answerId==22?  0 : (answerId==23? (gender==\"male\" ? 0 : 23) : (answerId==24? 24 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==25? 25 : (answerId==26? 26 : ( answerId==27? 27 : (answerId==28? 28 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==29? 29 : (answerId==30? 30 : ( answerId==31? 31 : (answerId==32? 32 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==33? 33 : (answerId==34? 34 : ( answerId==35? 35 : (answerId==36? 36 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==37? 37 : (answerId==38? 38 : ( answerId==39? 39 : (answerId==40? 40 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==41? 41 : (answerId==42? 42 : ( answerId==43? 43 : (answerId==44? 44 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "48"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==47? 49 : (answerId==48? 50 : ( answerId==49? 51 : (answerId==50? 52 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==51? 53 : (answerId==52? 54 : ( answerId==53? 55 : (answerId==54? 56 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "(answerId==55? 57 : (answerId==56? 58 : ( answerId==57? 59 : (answerId==58? 60 : 0) )) )"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {}, Lcom/cigna/coach/utils/ExpressionEvaluator;->getInstance()Lcom/cigna/coach/utils/ExpressionEvaluator;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/utils/ScoringHelper;->evaluator:Lcom/cigna/coach/utils/ExpressionEvaluator;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 152
    iput-object p1, p0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 153
    return-void
.end method

.method public static addScoreVariances(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 782
    .local p0, "lssList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 783
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "addScoreVariances: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 786
    :cond_0
    const/4 v1, 0x0

    .line 787
    .local v1, "lastLifeStyleScore":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 788
    .local v2, "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreType()Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    if-ne v3, v4, :cond_3

    .line 789
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 790
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "LIFESTYLE_OVERALL found: score:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    :cond_2
    move-object v1, v2

    .line 795
    :cond_3
    if-nez v1, :cond_4

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->setVarianceValue(I)V

    .line 798
    :goto_1
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 799
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "    Variance Value for score "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getVarianceValue()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 796
    :cond_4
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v3

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->setVarianceValue(I)V

    goto :goto_1

    .line 803
    .end local v2    # "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    :cond_5
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 804
    sget-object v3, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "addScoreVariances: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    :cond_6
    return-void
.end method

.method public static calculateBMI(DD)D
    .locals 8
    .param p0, "height"    # D
    .param p2, "weight"    # D

    .prologue
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    .line 232
    const-wide/16 v2, 0x0

    cmpg-double v2, p0, v2

    if-gtz v2, :cond_0

    .line 233
    const-wide/high16 p0, 0x3ff0000000000000L    # 1.0

    .line 235
    :cond_0
    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double v2, p2, v2

    div-double v4, p0, v6

    div-double v6, p0, v6

    mul-double/2addr v4, v6

    div-double v0, v2, v4

    .line 236
    .local v0, "bmi":D
    return-wide v0
.end method

.method private cancelUnalignedGoal(Ljava/lang/String;I)V
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "existingGoalId"    # I

    .prologue
    .line 560
    new-instance v2, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v7, p0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v2, v7}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 561
    .local v2, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    new-instance v5, Lcom/cigna/coach/db/MissionDBManager;

    iget-object v7, p0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v5, v7}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 562
    .local v5, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    iget-object v7, p0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v0

    .line 563
    .local v0, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalDetails(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v1

    .line 565
    .local v1, "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 566
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Existing goals need to be cancelled"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Getting all non completed missions for this goal"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    :cond_0
    invoke-virtual {v5, p1, p2}, Lcom/cigna/coach/db/MissionDBManager;->getNonCompleteUserMissionsForGoal(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v6

    .line 573
    .local v6, "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    if-eqz v6, :cond_4

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_4

    .line 574
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 575
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Need to cancel "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " missions"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    :cond_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 578
    .local v4, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 579
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cancelling missionid:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", seq num:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    :cond_2
    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v7

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v8

    sget-object v9, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v5, p1, v7, v8, v9}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMissionStatus(Ljava/lang/String;IILcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    goto :goto_0

    .line 585
    .end local v4    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_3
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 586
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "All active missions cancelled"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 591
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Cancelling goal"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    :cond_5
    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v2, p1, p2, v7}, Lcom/cigna/coach/db/GoalDBManager;->updateUserGoalStatus(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)I

    .line 596
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 597
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Sending goal cancelled broadcast"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    :cond_6
    invoke-virtual {v0, v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendGoalCancelledBroadcast(Lcom/cigna/coach/dataobjects/UserGoalData;)V

    .line 601
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 602
    sget-object v7, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Goal cancelled"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_7
    return-void
.end method

.method private isTimeToReassess(Lcom/cigna/coach/dataobjects/ScoreInfoData;)Z
    .locals 5
    .param p1, "scoreInfoData"    # Lcom/cigna/coach/dataobjects/ScoreInfoData;

    .prologue
    const/4 v2, 0x1

    .line 767
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getLastAssessmentDate()Ljava/util/Calendar;

    move-result-object v0

    .line 768
    .local v0, "lastAssessmentDate":Ljava/util/Calendar;
    if-nez v0, :cond_1

    .line 771
    :cond_0
    :goto_0
    return v2

    .line 770
    :cond_1
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v1

    .line 771
    .local v1, "now":Ljava/util/Calendar;
    invoke-static {v0, v1}, Lcom/cigna/coach/utils/CalendarUtil;->monthsAndDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)F

    move-result v3

    const/high16 v4, 0x40c00000    # 6.0f

    cmpl-float v3, v3, v4

    if-gez v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private shouldIgnoreModerateActivity(Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    const/4 v6, 0x0

    .line 608
    const/4 v2, -0x1

    .line 609
    .local v2, "moderateActivityAnswerId":I
    const/4 v4, -0x1

    .line 612
    .local v4, "vigorousActivityAnswerId":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    .line 613
    .local v3, "qa":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 614
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerId()I

    move-result v0

    .line 615
    .local v0, "answerId":I
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v5

    const/16 v7, 0x8

    if-ne v5, v7, :cond_1

    move v2, v0

    .line 616
    :cond_1
    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v5

    const/16 v7, 0x9

    if-ne v5, v7, :cond_0

    move v4, v0

    goto :goto_0

    .line 620
    .end local v0    # "answerId":I
    .end local v3    # "qa":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :cond_2
    if-gtz v2, :cond_3

    move v5, v6

    .line 624
    :goto_1
    return v5

    .line 621
    :cond_3
    if-gtz v4, :cond_4

    move v5, v6

    goto :goto_1

    .line 622
    :cond_4
    const/16 v5, 0x20

    if-ne v2, v5, :cond_5

    move v5, v6

    goto :goto_1

    .line 623
    :cond_5
    const/16 v5, 0x21

    if-ne v4, v5, :cond_6

    move v5, v6

    goto :goto_1

    .line 624
    :cond_6
    const/4 v5, 0x1

    goto :goto_1
.end method

.method public static summarizeScoresHistoryList(Ljava/util/List;Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Ljava/util/List;
    .locals 19
    .param p1, "scoreGroupingType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation

    .prologue
    .line 662
    .local p0, "lssList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 663
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "summarizeScoresHistoryList("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "): START"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    :cond_0
    if-nez p0, :cond_2

    .line 667
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 668
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v17, "Scores list is null"

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "summarizeScoresHistoryList("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "): EARLY END"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    .end local p0    # "lssList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    :cond_1
    :goto_0
    return-object p0

    .line 674
    .restart local p0    # "lssList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    :cond_2
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 676
    .local v12, "returnLssList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$ScoreGroupingType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;->ordinal()I

    move-result v17

    aget v16, v16, v17

    packed-switch v16, :pswitch_data_0

    .line 726
    :cond_3
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 727
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v17, "Raw scores list:"

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 729
    .local v13, "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "    Score on "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " was "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 678
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v13    # "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    :pswitch_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v14

    .line 679
    .local v14, "size":I
    const-wide/16 v2, -0x1

    .line 681
    .local v2, "day":J
    add-int/lit8 v4, v14, -0x1

    .local v4, "i":I
    :goto_2
    if-ltz v4, :cond_3

    .line 682
    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 683
    .local v6, "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 684
    .local v7, "lifestyleDay":J
    cmp-long v16, v2, v7

    if-eqz v16, :cond_4

    .line 685
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v12, v0, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 686
    move-wide v2, v7

    .line 681
    :cond_4
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 692
    .end local v2    # "day":J
    .end local v4    # "i":I
    .end local v6    # "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    .end local v7    # "lifestyleDay":J
    .end local v14    # "size":I
    :pswitch_1
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v14

    .line 693
    .restart local v14    # "size":I
    const/4 v11, -0x1

    .line 694
    .local v11, "month":I
    const/4 v15, -0x1

    .line 696
    .local v15, "year":I
    add-int/lit8 v4, v14, -0x1

    .restart local v4    # "i":I
    :goto_3
    if-ltz v4, :cond_3

    .line 697
    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 698
    .restart local v6    # "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v16

    const/16 v17, 0x2

    invoke-virtual/range {v16 .. v17}, Ljava/util/Calendar;->get(I)I

    move-result v9

    .line 699
    .local v9, "lifestyleMonth":I
    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v16

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 700
    .local v10, "lifestyleYear":I
    if-ne v11, v9, :cond_5

    if-eq v10, v15, :cond_6

    .line 701
    :cond_5
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v12, v0, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 702
    move v15, v10

    .line 703
    move v11, v9

    .line 696
    :cond_6
    add-int/lit8 v4, v4, -0x1

    goto :goto_3

    .line 709
    .end local v4    # "i":I
    .end local v6    # "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    .end local v9    # "lifestyleMonth":I
    .end local v10    # "lifestyleYear":I
    .end local v11    # "month":I
    .end local v14    # "size":I
    .end local v15    # "year":I
    :pswitch_2
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v14

    .line 710
    .restart local v14    # "size":I
    const/4 v15, -0x1

    .line 712
    .restart local v15    # "year":I
    add-int/lit8 v4, v14, -0x1

    .restart local v4    # "i":I
    :goto_4
    if-ltz v4, :cond_3

    .line 713
    move-object/from16 v0, p0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 714
    .restart local v6    # "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v16

    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Ljava/util/Calendar;->get(I)I

    move-result v10

    .line 715
    .restart local v10    # "lifestyleYear":I
    if-eq v10, v15, :cond_7

    .line 716
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v12, v0, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 717
    move v15, v10

    .line 712
    :cond_7
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .line 732
    .end local v4    # "i":I
    .end local v6    # "lifeStyleScores":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    .end local v10    # "lifestyleYear":I
    .end local v14    # "size":I
    .end local v15    # "year":I
    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_8
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v17, "Summarized scores list:"

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 734
    .restart local v13    # "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "    Score on "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " was "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 737
    .end local v13    # "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    :cond_9
    sget-object v16, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "summarizeScoresHistoryList("

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "): END"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_a
    move-object/from16 p0, v12

    .line 739
    goto/16 :goto_0

    .line 676
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public computeOverallScore(Ljava/lang/String;)I
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 165
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 166
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "Computing overall score"

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    const/4 v11, -0x1

    .line 169
    .local v11, "overAllScore":I
    const/4 v8, 0x0

    .line 170
    .local v8, "maxPossibleScore":I
    const/4 v10, 0x0

    .line 171
    .local v10, "numCatsWithPositiveScore":I
    new-instance v7, Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v7, v14}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 173
    .local v7, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    invoke-virtual {v7}, Lcom/cigna/coach/db/LifeStyleDBManager;->getAllCategories()Ljava/util/List;

    move-result-object v5

    .line 175
    .local v5, "cats":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 176
    .local v3, "categoryScores":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/CategoryInfo;

    .line 177
    .local v1, "cat":Lcom/cigna/coach/apiobjects/CategoryInfo;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CategoryInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v7, v14, v0, v15}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserScoreForCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/util/Calendar;)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 178
    .local v12, "scoreForCategory":I
    if-ltz v12, :cond_1

    .line 179
    add-int/lit8 v10, v10, 0x1

    .line 181
    :cond_1
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CategoryInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v14

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-interface {v3, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 188
    .end local v1    # "cat":Lcom/cigna/coach/apiobjects/CategoryInfo;
    .end local v12    # "scoreForCategory":I
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v14

    if-ne v10, v14, :cond_9

    .line 189
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v9

    .line 190
    .local v9, "numCategories":I
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 191
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Num categories :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_3
    add-int/lit8 v11, v11, 0x1

    .line 197
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 198
    .local v2, "categories":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 199
    .local v4, "categoryType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 200
    .restart local v12    # "scoreForCategory":I
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 201
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Category score for "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " is "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    :cond_4
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->categoryWeight:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 204
    .local v13, "weightForCategory":I
    mul-int v14, v12, v13

    add-int/2addr v11, v14

    .line 205
    mul-int/lit8 v14, v13, 0x64

    add-int/2addr v8, v14

    .line 206
    goto :goto_1

    .line 208
    .end local v4    # "categoryType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    .end local v12    # "scoreForCategory":I
    .end local v13    # "weightForCategory":I
    :cond_5
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 209
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Overall score before conversion is:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Max possible score is:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_6
    if-lez v8, :cond_8

    .line 213
    mul-int/lit8 v14, v11, 0x64

    div-int v11, v14, v8

    .line 218
    :goto_2
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 219
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Converted overall score is:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    .end local v2    # "categories":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;>;"
    .end local v9    # "numCategories":I
    :cond_7
    :goto_3
    return v11

    .line 215
    .restart local v2    # "categories":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;>;"
    .restart local v9    # "numCategories":I
    :cond_8
    const/4 v11, 0x0

    goto :goto_2

    .line 222
    .end local v2    # "categories":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;>;"
    .end local v9    # "numCategories":I
    :cond_9
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 223
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Only "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " categories has score"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    sget-object v14, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "Overall score is -1"

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public computeScoreForCategoryBasedOnUserResponse(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List;ILcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)Z
    .locals 52
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p4, "restrictGoalCalculationQGrpId"    # I
    .param p5, "scoreType"    # Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;",
            ">;I",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 265
    .local p3, "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    sget-object v48, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, p2

    move-object/from16 v1, v48

    if-ne v0, v1, :cond_1

    .line 266
    new-instance v48, Lcom/cigna/coach/utils/WeightChangeListener;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v49, v0

    invoke-direct/range {v48 .. v49}, Lcom/cigna/coach/utils/WeightChangeListener;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    move-object/from16 v0, v48

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->updateWeightGoalAndScore(Ljava/lang/String;Ljava/util/List;)Z

    move-result v25

    .line 556
    :cond_0
    :goto_0
    return v25

    .line 269
    :cond_1
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_2

    .line 270
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "============================================================"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Processing score for category: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_2
    const/4 v8, 0x0

    .line 274
    .local v8, "categoryScore":I
    const/16 v25, 0x0

    .line 275
    .local v25, "isAnyUnalignedGoalCancelled":Z
    const/16 v27, 0x0

    .line 276
    .local v27, "maxPossibleScore":I
    const/16 v37, -0x1

    .line 278
    .local v37, "returnScore":I
    new-instance v26, Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v48, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 279
    .local v26, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    new-instance v16, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v48, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 281
    .local v16, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    new-instance v44, Lcom/cigna/coach/db/UserMetricsDBManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v48, v0

    move-object/from16 v0, v44

    move-object/from16 v1, v48

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 282
    .local v44, "userMetricsDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    new-instance v9, Lcom/cigna/coach/db/CountryExceptionsDBManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    invoke-direct {v9, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 284
    .local v9, "countryExceptionsDbManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    move-object/from16 v0, v44

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetrics(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics;

    move-result-object v43

    .line 287
    .local v43, "userMetrics":Lcom/cigna/coach/apiobjects/UserMetrics;
    if-nez p2, :cond_3

    .line 288
    new-instance v48, Lcom/cigna/coach/exceptions/CoachException;

    const-string v49, "Error: CategoryType is null, score cannot be calculated"

    invoke-direct/range {v48 .. v49}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v48

    .line 290
    :cond_3
    if-eqz p3, :cond_2e

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v48

    if-lez v48, :cond_2e

    .line 292
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v28

    .line 293
    .local v28, "numQuestionGroups":I
    new-instance v42, Ljava/util/ArrayList;

    move-object/from16 v0, v42

    move/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 295
    .local v42, "userGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserGoalData;>;"
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 296
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Processing "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, " question groups"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_4
    invoke-virtual/range {v43 .. v43}, Lcom/cigna/coach/apiobjects/UserMetrics;->getGender()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v15

    .line 300
    .local v15, "gender":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/ScoringHelper;->shouldIgnoreModerateActivity(Ljava/util/List;)Z

    move-result v39

    .line 302
    .local v39, "shouldIgnoreModerateActivity":Z
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_5
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v48

    if-eqz v48, :cond_25

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    .line 303
    .local v34, "qaGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    if-eqz v34, :cond_5

    .line 304
    invoke-virtual/range {v34 .. v34}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v31

    .line 305
    .local v31, "qGrpKey":I
    const/16 v48, -0x1

    move/from16 v0, v31

    move/from16 v1, v48

    if-le v0, v1, :cond_5

    .line 306
    const/16 v32, 0x0

    .line 309
    .local v32, "qGrpScore":I
    invoke-virtual/range {v34 .. v34}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v35

    .line 310
    .local v35, "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    const/4 v5, -0x1

    .line 311
    .local v5, "answerKey":I
    const/16 v30, -0x1

    .line 312
    .local v30, "qGrpAnswerValue":I
    if-eqz v35, :cond_d

    invoke-interface/range {v35 .. v35}, Ljava/util/List;->size()I

    move-result v48

    if-lez v48, :cond_d

    const/16 v48, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    invoke-virtual/range {v48 .. v48}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v48

    if-eqz v48, :cond_d

    .line 313
    const/16 v48, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v48

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    invoke-virtual/range {v48 .. v48}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerId()I

    move-result v5

    .line 314
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_6

    .line 315
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "----------------------------------------------"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Processing question group id: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_6
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupScoringExpression:Ljava/util/Map;

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v49

    invoke-interface/range {v48 .. v49}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/String;

    .line 319
    .local v38, "scoreExpression":Ljava/lang/String;
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_7

    .line 320
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Scoring expression is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    :cond_7
    new-instance v45, Ljava/util/HashMap;

    invoke-direct/range {v45 .. v45}, Ljava/util/HashMap;-><init>()V

    .line 324
    .local v45, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v48, "gender"

    invoke-virtual {v15}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->toString()Ljava/lang/String;

    move-result-object v49

    sget-object v50, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_8

    .line 326
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Gender is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual {v15}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->toString()Ljava/lang/String;

    move-result-object v50

    sget-object v51, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v50 .. v51}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_8
    packed-switch v31, :pswitch_data_0

    .line 359
    move/from16 v30, v5

    .line 360
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_9

    .line 361
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Answer key is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_9
    const-string v48, "answerId"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ScoringHelper;->evaluator:Lcom/cigna/coach/utils/ExpressionEvaluator;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    move-object/from16 v1, v38

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/ExpressionEvaluator;->evaluate(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v11

    .line 369
    .local v11, "evalResult":Ljava/lang/Object;
    instance-of v0, v11, Ljava/lang/Integer;

    move/from16 v48, v0

    if-eqz v48, :cond_14

    .line 370
    check-cast v11, Ljava/lang/Integer;

    .end local v11    # "evalResult":Ljava/lang/Object;
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v32

    .line 377
    :goto_3
    if-gez v32, :cond_a

    .line 378
    const/16 v32, 0x0

    .line 381
    :cond_a
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_b

    .line 382
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Question group score is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    :cond_b
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->questionGroupWeight:Ljava/util/Map;

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v49

    invoke-interface/range {v48 .. v49}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Ljava/lang/Integer;

    invoke-virtual/range {v48 .. v48}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 386
    .local v19, "groupWeight":I
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_c

    .line 387
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Question group weight is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_c
    const/16 v48, 0x8

    move/from16 v0, v31

    move/from16 v1, v48

    if-ne v0, v1, :cond_16

    const/16 v48, 0x1

    move/from16 v0, v39

    move/from16 v1, v48

    if-ne v0, v1, :cond_16

    .line 392
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_d

    .line 393
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "Should ignore moderate activity"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    .end local v19    # "groupWeight":I
    .end local v38    # "scoreExpression":Ljava/lang/String;
    .end local v45    # "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_d
    :goto_4
    const/16 v48, -0x1

    move/from16 v0, p4

    move/from16 v1, v48

    if-eq v0, v1, :cond_e

    const/16 v48, -0x1

    move/from16 v0, p4

    move/from16 v1, v48

    if-le v0, v1, :cond_5

    move/from16 v0, v31

    move/from16 v1, p4

    if-ne v0, v1, :cond_5

    .line 405
    :cond_e
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_f

    .line 406
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "Getting ready to pick a goal:"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_f
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->goalSelectExpression:Ljava/util/Map;

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v49

    invoke-interface/range {v48 .. v49}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 410
    .local v17, "goalExpression":Ljava/lang/String;
    new-instance v45, Ljava/util/HashMap;

    invoke-direct/range {v45 .. v45}, Ljava/util/HashMap;-><init>()V

    .line 411
    .restart local v45    # "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v48, "country"

    invoke-virtual/range {v43 .. v43}, Lcom/cigna/coach/apiobjects/UserMetrics;->getCountry()Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    const-string v48, "dateOfBirth"

    invoke-virtual/range {v43 .. v43}, Lcom/cigna/coach/apiobjects/UserMetrics;->getDateOfBirth()Ljava/util/Calendar;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    const-string v48, "gender"

    invoke-virtual/range {v43 .. v43}, Lcom/cigna/coach/apiobjects/UserMetrics;->getGender()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->toString()Ljava/lang/String;

    move-result-object v49

    sget-object v50, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    const-string v48, "height"

    invoke-virtual/range {v43 .. v43}, Lcom/cigna/coach/apiobjects/UserMetrics;->getHeight()F

    move-result v49

    invoke-static/range {v49 .. v49}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    const-string/jumbo v48, "maritalStatus"

    invoke-virtual/range {v43 .. v43}, Lcom/cigna/coach/apiobjects/UserMetrics;->getMaritalStatus()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->toString()Ljava/lang/String;

    move-result-object v49

    sget-object v50, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual/range {v49 .. v50}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    const-string/jumbo v48, "weight"

    invoke-virtual/range {v43 .. v43}, Lcom/cigna/coach/apiobjects/UserMetrics;->getWeight()F

    move-result v49

    invoke-static/range {v49 .. v49}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    const-string v48, "answerId"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    const-string/jumbo v48, "value"

    invoke-static/range {v30 .. v30}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_17

    .line 422
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Question group:"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Goal expression is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-interface/range {v45 .. v45}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v48

    invoke-interface/range {v48 .. v48}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .local v23, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v48

    if-eqz v48, :cond_17

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 425
    .local v10, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v49, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Goal "

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v48

    check-cast v48, Ljava/lang/String;

    move-object/from16 v0, v50

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    const-string v50, " is: "

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v50

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 332
    .end local v10    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v17    # "goalExpression":Ljava/lang/String;
    .end local v23    # "i$":Ljava/util/Iterator;
    .restart local v38    # "scoreExpression":Ljava/lang/String;
    :pswitch_0
    const-wide/16 v20, 0x0

    .line 333
    .local v20, "height":D
    const-wide/16 v46, 0x0

    .line 334
    .local v46, "weight":D
    invoke-interface/range {v35 .. v35}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    .restart local v23    # "i$":Ljava/util/Iterator;
    :cond_10
    :goto_6
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v48

    if-eqz v48, :cond_12

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    .line 335
    .local v33, "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    invoke-virtual/range {v33 .. v33}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getQuestionId()I

    move-result v48

    const/16 v49, 0xc

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_11

    .line 336
    invoke-virtual/range {v33 .. v33}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v20

    .line 338
    :cond_11
    invoke-virtual/range {v33 .. v33}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getQuestionId()I

    move-result v48

    const/16 v49, 0xd

    move/from16 v0, v48

    move/from16 v1, v49

    if-ne v0, v1, :cond_10

    .line 339
    invoke-virtual/range {v33 .. v33}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v48

    invoke-static/range {v48 .. v48}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v46

    goto :goto_6

    .line 343
    .end local v33    # "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    :cond_12
    move-wide/from16 v0, v20

    move-wide/from16 v2, v46

    invoke-static {v0, v1, v2, v3}, Lcom/cigna/coach/utils/ScoringHelper;->calculateBMI(DD)D

    move-result-wide v6

    .line 345
    .local v6, "bmi":D
    const-wide/high16 v48, 0x4059000000000000L    # 100.0

    mul-double v48, v48, v6

    invoke-static/range {v48 .. v49}, Ljava/lang/Math;->round(D)J

    move-result-wide v48

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v30, v0

    .line 346
    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v48, v0

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setHeight(Ljava/lang/String;F)V

    .line 347
    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v48, v0

    move-object/from16 v0, v44

    move-object/from16 v1, p1

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setWeight(Ljava/lang/String;F)V

    .line 348
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_13

    .line 349
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Height  is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Weight  is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move-wide/from16 v1, v46

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "BMI is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_13
    const-string v48, "bmi"

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v49

    move-object/from16 v0, v45

    move-object/from16 v1, v48

    move-object/from16 v2, v49

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 371
    .end local v6    # "bmi":D
    .end local v20    # "height":D
    .end local v23    # "i$":Ljava/util/Iterator;
    .end local v46    # "weight":D
    .restart local v11    # "evalResult":Ljava/lang/Object;
    :cond_14
    instance-of v0, v11, Ljava/lang/Double;

    move/from16 v48, v0

    if-eqz v48, :cond_15

    .line 372
    check-cast v11, Ljava/lang/Double;

    .end local v11    # "evalResult":Ljava/lang/Object;
    invoke-virtual {v11}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v48

    invoke-static/range {v48 .. v49}, Ljava/lang/Math;->round(D)J

    move-result-wide v48

    move-wide/from16 v0, v48

    long-to-int v0, v0

    move/from16 v32, v0

    goto/16 :goto_3

    .line 374
    .restart local v11    # "evalResult":Ljava/lang/Object;
    :cond_15
    const/16 v32, 0x0

    goto/16 :goto_3

    .line 397
    .end local v11    # "evalResult":Ljava/lang/Object;
    .restart local v19    # "groupWeight":I
    :cond_16
    mul-int v48, v32, v19

    add-int v8, v8, v48

    .line 398
    mul-int/lit8 v48, v19, 0x64

    add-int v27, v27, v48

    goto/16 :goto_4

    .line 429
    .end local v19    # "groupWeight":I
    .end local v38    # "scoreExpression":Ljava/lang/String;
    .restart local v17    # "goalExpression":Ljava/lang/String;
    :cond_17
    const/16 v40, 0x0

    .line 430
    .local v40, "userGoal":Lcom/cigna/coach/apiobjects/GoalInfo;
    const/16 v18, 0x0

    .line 432
    .local v18, "goalId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ScoringHelper;->evaluator:Lcom/cigna/coach/utils/ExpressionEvaluator;

    move-object/from16 v48, v0

    move-object/from16 v0, v48

    move-object/from16 v1, v17

    move-object/from16 v2, v45

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/ExpressionEvaluator;->evaluate(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v11

    .line 433
    .restart local v11    # "evalResult":Ljava/lang/Object;
    instance-of v0, v11, Ljava/lang/Integer;

    move/from16 v48, v0

    if-eqz v48, :cond_18

    .line 434
    check-cast v11, Ljava/lang/Integer;

    .end local v11    # "evalResult":Ljava/lang/Object;
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 437
    :cond_18
    const/16 v48, 0x8

    move/from16 v0, v31

    move/from16 v1, v48

    if-ne v0, v1, :cond_1a

    if-eqz v39, :cond_1a

    .line 438
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_19

    .line 439
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Ignoring goal for moderate activity question group:"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    :cond_19
    const/16 v18, 0x0

    .line 444
    :cond_1a
    if-lez v18, :cond_1c

    .line 445
    new-instance v40, Lcom/cigna/coach/apiobjects/GoalInfo;

    .end local v40    # "userGoal":Lcom/cigna/coach/apiobjects/GoalInfo;
    invoke-direct/range {v40 .. v40}, Lcom/cigna/coach/apiobjects/GoalInfo;-><init>()V

    .line 446
    .restart local v40    # "userGoal":Lcom/cigna/coach/apiobjects/GoalInfo;
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1b

    .line 447
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "Building GoalInfo Object"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    :cond_1b
    move-object/from16 v0, v40

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/GoalInfo;->setGoalId(I)V

    .line 450
    new-instance v36, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    invoke-direct/range {v36 .. v36}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;-><init>()V

    .line 451
    .local v36, "questionsAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    move-object/from16 v0, v36

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->setQuestionGroupId(I)V

    .line 452
    move-object/from16 v0, v40

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/GoalInfo;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 453
    move-object/from16 v0, v40

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/GoalInfo;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 456
    .end local v36    # "questionsAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :cond_1c
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1d

    .line 457
    sget-object v49, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Goal picked is:"

    move-object/from16 v0, v48

    move-object/from16 v1, v50

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v50

    if-eqz v40, :cond_22

    new-instance v48, Ljava/lang/StringBuilder;

    invoke-direct/range {v48 .. v48}, Ljava/lang/StringBuilder;-><init>()V

    const-string v51, ""

    move-object/from16 v0, v48

    move-object/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v40 .. v40}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalId()I

    move-result v51

    move-object/from16 v0, v48

    move/from16 v1, v51

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    :goto_7
    move-object/from16 v0, v50

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v48

    invoke-virtual/range {v48 .. v48}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v48

    move-object/from16 v0, v49

    move-object/from16 v1, v48

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_1d
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1e

    .line 463
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "Checking existing goals for qeustion group"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    :cond_1e
    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/GoalDBManager;->getUnalignedUserGoalForQuestionGroup(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v14

    .line 467
    .local v14, "existingUserGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    if-eqz v40, :cond_23

    invoke-virtual/range {v40 .. v40}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalId()I

    move-result v48

    if-lez v48, :cond_23

    const/16 v24, 0x1

    .line 469
    .local v24, "insertNewGoal":Z
    :goto_8
    if-eqz v14, :cond_21

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v48

    const/16 v49, -0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-eq v0, v1, :cond_21

    .line 470
    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v12

    .line 471
    .local v12, "existingGoalId":I
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_1f

    .line 472
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Existing goal for question group is:"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    :cond_1f
    if-eqz v40, :cond_20

    invoke-virtual/range {v40 .. v40}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalId()I

    move-result v48

    move/from16 v0, v48

    if-ne v12, v0, :cond_20

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionGroupScore()I

    move-result v48

    move/from16 v0, v48

    move/from16 v1, v32

    if-eq v0, v1, :cond_24

    .line 475
    :cond_20
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12}, Lcom/cigna/coach/utils/ScoringHelper;->cancelUnalignedGoal(Ljava/lang/String;I)V

    .line 476
    const/16 v25, 0x1

    .line 482
    .end local v12    # "existingGoalId":I
    :cond_21
    :goto_9
    if-eqz v24, :cond_5

    if-eqz v40, :cond_5

    if-eqz v42, :cond_5

    .line 483
    new-instance v41, Lcom/cigna/coach/dataobjects/UserGoalData;

    invoke-direct/range {v41 .. v41}, Lcom/cigna/coach/dataobjects/UserGoalData;-><init>()V

    .line 484
    .local v41, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    invoke-virtual/range {v40 .. v40}, Lcom/cigna/coach/apiobjects/GoalInfo;->getGoalId()I

    move-result v48

    move-object/from16 v0, v41

    move/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalId(I)V

    .line 485
    new-instance v29, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;

    invoke-direct/range {v29 .. v29}, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;-><init>()V

    .line 486
    .local v29, "qGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;
    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;->setQuestionGroupId(I)V

    .line 487
    move-object/from16 v0, v41

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 488
    move-object/from16 v0, v41

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionGroupScore(I)V

    .line 489
    move-object/from16 v0, v41

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 490
    sget-object v48, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, v41

    move-object/from16 v1, v48

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 491
    move-object/from16 v0, v42

    move-object/from16 v1, v41

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 457
    .end local v14    # "existingUserGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v24    # "insertNewGoal":Z
    .end local v29    # "qGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;
    .end local v41    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_22
    const-string v48, "None"

    goto/16 :goto_7

    .line 467
    .restart local v14    # "existingUserGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_23
    const/16 v24, 0x0

    goto/16 :goto_8

    .line 478
    .restart local v12    # "existingGoalId":I
    .restart local v24    # "insertNewGoal":Z
    :cond_24
    const/16 v24, 0x0

    goto :goto_9

    .line 498
    .end local v5    # "answerKey":I
    .end local v12    # "existingGoalId":I
    .end local v14    # "existingUserGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v17    # "goalExpression":Ljava/lang/String;
    .end local v18    # "goalId":I
    .end local v24    # "insertNewGoal":Z
    .end local v30    # "qGrpAnswerValue":I
    .end local v31    # "qGrpKey":I
    .end local v32    # "qGrpScore":I
    .end local v34    # "qaGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    .end local v35    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    .end local v40    # "userGoal":Lcom/cigna/coach/apiobjects/GoalInfo;
    .end local v45    # "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_25
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_26

    .line 499
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "============================================================"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Category score is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Max possible score is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :cond_26
    if-lez v27, :cond_29

    .line 506
    mul-int/lit8 v48, v8, 0x64

    div-int v37, v48, v27

    .line 507
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_27

    .line 508
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Converted category score is: "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_27
    :goto_a
    const/16 v48, -0x1

    move/from16 v0, v37

    move/from16 v1, v48

    if-le v0, v1, :cond_2e

    .line 516
    if-eqz v42, :cond_2c

    .line 517
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_28

    .line 518
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "Setting category score for each goal"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    :cond_28
    invoke-interface/range {v42 .. v42}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :goto_b
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v48

    if-eqz v48, :cond_2a

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/cigna/coach/dataobjects/UserGoalData;

    .line 521
    .local v40, "userGoal":Lcom/cigna/coach/dataobjects/UserGoalData;
    move-object/from16 v0, v40

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryScore(I)V

    goto :goto_b

    .line 511
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v40    # "userGoal":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_29
    const/16 v37, 0x0

    goto :goto_a

    .line 524
    .restart local v22    # "i$":Ljava/util/Iterator;
    :cond_2a
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_2b

    .line 525
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Inserting "

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-interface/range {v42 .. v42}, Ljava/util/List;->size()I

    move-result v50

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    const-string v50, " goals into the database"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :cond_2b
    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move-object/from16 v2, v42

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/GoalDBManager;->createUserGoals(Ljava/lang/String;Ljava/util/List;)V

    .line 530
    .end local v22    # "i$":Ljava/util/Iterator;
    :cond_2c
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_2d

    .line 531
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "Saving the category score in database"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :cond_2d
    move-object/from16 v0, v26

    move-object/from16 v1, p1

    move-object/from16 v2, p5

    move-object/from16 v3, p2

    move/from16 v4, v37

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V

    .line 541
    .end local v15    # "gender":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    .end local v28    # "numQuestionGroups":I
    .end local v39    # "shouldIgnoreModerateActivity":Z
    .end local v42    # "userGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserGoalData;>;"
    :cond_2e
    sget-object v48, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, p2

    move-object/from16 v1, v48

    if-ne v0, v1, :cond_30

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppressAlcoholRelatedContent(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_30

    .line 542
    const/16 v48, 0x6

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move/from16 v2, v48

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/GoalDBManager;->getUnalignedUserGoalForQuestionGroup(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v13

    .line 543
    .local v13, "existingUserGoal":Lcom/cigna/coach/dataobjects/UserGoalData;
    if-eqz v13, :cond_30

    invoke-virtual {v13}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v48

    const/16 v49, -0x1

    move/from16 v0, v48

    move/from16 v1, v49

    if-eq v0, v1, :cond_30

    .line 544
    invoke-virtual {v13}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v12

    .line 545
    .restart local v12    # "existingGoalId":I
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_2f

    .line 546
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v49, Ljava/lang/StringBuilder;

    invoke-direct/range {v49 .. v49}, Ljava/lang/StringBuilder;-><init>()V

    const-string v50, "Existing goal for alcohol question group is:"

    invoke-virtual/range {v49 .. v50}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v49

    move-object/from16 v0, v49

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v49

    invoke-virtual/range {v49 .. v49}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v49

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    :cond_2f
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12}, Lcom/cigna/coach/utils/ScoringHelper;->cancelUnalignedGoal(Ljava/lang/String;I)V

    .line 549
    const/16 v25, 0x1

    .line 553
    .end local v12    # "existingGoalId":I
    .end local v13    # "existingUserGoal":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_30
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v48 .. v48}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v48

    if-eqz v48, :cond_0

    .line 554
    sget-object v48, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v49, "============================================================"

    invoke-static/range {v48 .. v49}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public getUserLifeStyleScoreHistory(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/ScoreInfoData;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 629
    sget-object v4, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 630
    sget-object v4, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getUserLifeStyleScoreHistory: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    :cond_0
    new-instance v2, Lcom/cigna/coach/dataobjects/ScoreInfoData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/ScoreInfoData;-><init>()V

    .line 633
    .local v2, "lifeStyleScoreHistory":Lcom/cigna/coach/dataobjects/ScoreInfoData;
    sget-object v4, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 634
    sget-object v4, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Preparing to run query to getLifeStyleScoreHistory "

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :cond_1
    const/4 v1, 0x0

    .line 637
    .local v1, "lifeStyleOverallScoresList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    new-instance v3, Lcom/cigna/coach/db/LifeStyleDBManager;

    iget-object v4, p0, Lcom/cigna/coach/utils/ScoringHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v3, v4}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 640
    .local v3, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    :try_start_0
    invoke-virtual {v3, p1}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserLifeStyleOverallAndCurrentScoresList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 641
    invoke-static {v1}, Lcom/cigna/coach/utils/ScoringHelper;->addScoreVariances(Ljava/util/List;)V

    .line 642
    invoke-virtual {v2, v1}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->setRawLssList(Ljava/util/List;)V

    .line 643
    invoke-direct {p0, v2}, Lcom/cigna/coach/utils/ScoringHelper;->isTimeToReassess(Lcom/cigna/coach/dataobjects/ScoreInfoData;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->setTimeToReassess(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 648
    sget-object v4, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 649
    sget-object v4, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getUserLifeStyleScoreHistory: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    :cond_2
    return-object v2

    .line 644
    :catch_0
    move-exception v0

    .line 645
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v4, Lcom/cigna/coach/utils/ScoringHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getUserLifeStyleScoreHistory: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 646
    throw v0
.end method
