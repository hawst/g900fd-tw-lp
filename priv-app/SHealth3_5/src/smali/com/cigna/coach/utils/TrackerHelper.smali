.class public Lcom/cigna/coach/utils/TrackerHelper;
.super Ljava/lang/Object;
.source "TrackerHelper.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/TrackerHelper$1;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private final context:Landroid/content/Context;

.field private final dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/cigna/coach/utils/TrackerHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;Landroid/content/Context;)V
    .locals 0
    .param p1, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/cigna/coach/utils/TrackerHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 73
    iput-object p2, p0, Lcom/cigna/coach/utils/TrackerHelper;->context:Landroid/content/Context;

    .line 74
    return-void
.end method

.method private getActivityMapPerDay(Ljava/util/List;J)Ljava/util/Map;
    .locals 14
    .param p2, "startTime"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;J)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 712
    .local p1, "metExerciseTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    if-nez p1, :cond_1

    .line 713
    const/4 v3, 0x0

    .line 744
    :cond_0
    :goto_0
    return-object v3

    .line 715
    :cond_1
    sget-object v10, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 716
    sget-object v10, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getActivityMapPerDay : Start"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    :cond_2
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 721
    .local v3, "metExerciseTrackersMapPerDay":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;>;"
    invoke-static/range {p2 .. p3}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v9

    .line 722
    .local v9, "startTimeCalender":Ljava/util/Calendar;
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide p2

    .line 724
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/MetExerciseTracker;

    .line 725
    .local v2, "metExerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTracker;
    if-eqz v2, :cond_3

    .line 726
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDateInMsec()J

    move-result-wide v7

    .line 727
    .local v7, "startOfTheDayInMillis":J
    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v0

    .line 728
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 730
    sub-long v10, v7, p2

    const-wide/32 v12, 0x5265c00

    div-long v5, v10, v12

    .line 731
    .local v5, "noOfDays":J
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 732
    .local v4, "metExerciseTrackersPerDayList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    if-nez v4, :cond_4

    .line 733
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "metExerciseTrackersPerDayList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 735
    .restart local v4    # "metExerciseTrackersPerDayList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    :cond_4
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 737
    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v3, v10, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 740
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v2    # "metExerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTracker;
    .end local v4    # "metExerciseTrackersPerDayList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    .end local v5    # "noOfDays":J
    .end local v7    # "startOfTheDayInMillis":J
    :cond_5
    sget-object v10, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 741
    sget-object v10, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getActivityMapPerDay : END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAvgActivityPerWeek(Ljava/util/List;DDI[I)F
    .locals 15
    .param p2, "minMetValue"    # D
    .param p4, "maxMetValue"    # D
    .param p6, "minCount"    # I
    .param p7, "exerciseIdArray"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;DDI[I)F"
        }
    .end annotation

    .prologue
    .line 655
    .local p1, "lifeStyleExerciseTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    const/4 v11, 0x0

    .line 656
    .local v11, "sumOfActivityMinutes":I
    const/4 v9, 0x0

    .line 657
    .local v9, "perWeekActivityMinutes":I
    const/4 v8, 0x0

    .line 658
    .local v8, "numberOfActivitiesPerWeek":F
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    if-eqz p7, :cond_1

    .line 659
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "------Expected Samsung Exercise ID\'s-----"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    move-object/from16 v1, p7

    .local v1, "arr$":[I
    array-length v6, v1

    .local v6, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v6, :cond_0

    aget v5, v1, v3

    .line 661
    .local v5, "id":I
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "id:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 663
    .end local v5    # "id":I
    :cond_0
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "-------"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Minimun Duration:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p6

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    .end local v1    # "arr$":[I
    .end local v3    # "i$":I
    .end local v6    # "len$":I
    :cond_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/dataobjects/MetExerciseTracker;

    .line 668
    .local v7, "lifeStyleExerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTracker;
    if-eqz v7, :cond_2

    .line 669
    if-eqz p7, :cond_2

    move-object/from16 v0, p7

    array-length v12, v0

    if-lez v12, :cond_2

    .line 670
    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getExerciseInfoId()I

    move-result v10

    .line 671
    .local v10, "samsungExerciseId":I
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 672
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Tracker Exercise ID : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getExerciseInfoId()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    :cond_3
    move-object/from16 v1, p7

    .restart local v1    # "arr$":[I
    array-length v6, v1

    .restart local v6    # "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_2

    aget v2, v1, v4

    .line 675
    .local v2, "exerciseId":I
    if-ne v2, v10, :cond_5

    .line 676
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 677
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Duration from tracker: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDurationMin()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    :cond_4
    const-wide/high16 v12, -0x4010000000000000L    # -1.0

    cmpl-double v12, p4, v12

    if-nez v12, :cond_6

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getMetValue()F

    move-result v12

    float-to-double v12, v12

    cmpl-double v12, v12, p2

    if-ltz v12, :cond_6

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDurationMin()I

    move-result v12

    move/from16 v0, p6

    if-lt v12, v0, :cond_6

    .line 681
    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDurationMin()I

    move-result v12

    add-int/2addr v11, v12

    .line 682
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 683
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Criteria Met adding Min Count: sumOfActivityMinutes:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    :cond_5
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 685
    :cond_6
    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getMetValue()F

    move-result v12

    float-to-double v12, v12

    cmpl-double v12, v12, p2

    if-lez v12, :cond_5

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getMetValue()F

    move-result v12

    float-to-double v12, v12

    cmpg-double v12, v12, p4

    if-gez v12, :cond_5

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDurationMin()I

    move-result v12

    move/from16 v0, p6

    if-lt v12, v0, :cond_5

    .line 687
    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDurationMin()I

    move-result v12

    add-int/2addr v11, v12

    .line 688
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 689
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Criteria Met adding Min Count: sumOfActivityMinutes:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 697
    .end local v1    # "arr$":[I
    .end local v2    # "exerciseId":I
    .end local v4    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "lifeStyleExerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTracker;
    .end local v10    # "samsungExerciseId":I
    :cond_7
    if-lez v11, :cond_8

    .line 698
    div-int/lit8 v9, v11, 0x2

    .line 700
    :cond_8
    if-lez v9, :cond_9

    .line 701
    int-to-float v12, v9

    const/high16 v13, 0x3f800000    # 1.0f

    mul-float/2addr v12, v13

    move/from16 v0, p6

    int-to-float v13, v0

    div-float v8, v12, v13

    .line 703
    :cond_9
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 704
    sget-object v12, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getAvgActivityPerWeek : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    :cond_a
    return v8
.end method

.method private setSuggestedAnswerInAnswerListforQ8(Ljava/util/List;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "lifeStyleExerciseTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    .local p2, "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    const/4 v12, 0x1

    const/high16 v11, 0x40800000    # 4.0f

    const/high16 v10, 0x40000000    # 2.0f

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 504
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 508
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "setSuggestedAnswerInAnswerListforQ8 : Start"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    :cond_2
    const/16 v6, 0x1e

    new-array v7, v12, [I

    const/4 v0, 0x0

    const/16 v1, 0x4652

    aput v1, v7, v0

    move-object v0, p0

    move-object v1, p1

    move-wide v4, v2

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/utils/TrackerHelper;->getAvgActivityPerWeek(Ljava/util/List;DDI[I)F

    move-result v9

    .line 513
    .local v9, "numberOf30MinActivitiesPerWeek":F
    const/4 v8, 0x0

    .line 515
    .local v8, "answerIndex":I
    const/4 v0, 0x0

    cmpl-float v0, v9, v0

    if-lez v0, :cond_5

    cmpg-float v0, v9, v10

    if-gtz v0, :cond_5

    .line 516
    const/4 v8, 0x1

    .line 522
    :cond_3
    :goto_1
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    invoke-virtual {v0, v12}, Lcom/cigna/coach/apiobjects/Answer;->setPreselected(Z)V

    .line 523
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 524
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Selected Answer Index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    :cond_4
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "setSuggestedAnswerInAnswerListforQ8 : End"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 517
    :cond_5
    cmpl-float v0, v9, v10

    if-lez v0, :cond_6

    cmpg-float v0, v9, v11

    if-gtz v0, :cond_6

    .line 518
    const/4 v8, 0x2

    goto :goto_1

    .line 519
    :cond_6
    cmpl-float v0, v9, v11

    if-ltz v0, :cond_3

    .line 520
    const/4 v8, 0x3

    goto :goto_1
.end method

.method private setSuggestedAnswerInAnswerListforQ9(Ljava/util/List;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "lifeStyleExerciseTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    .local p2, "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    const/high16 v12, 0x40a00000    # 5.0f

    const/high16 v11, 0x40400000    # 3.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 534
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 564
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 537
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "setSuggestedAnswerInAnswerListforQ9 : Start"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    :cond_2
    const/4 v8, -0x1

    .line 541
    .local v8, "answerIndex":I
    const/4 v9, 0x0

    .line 543
    .local v9, "numberOf20MinActivitiesPerWeek":F
    const/16 v6, 0x14

    const/4 v0, 0x3

    new-array v7, v0, [I

    fill-array-data v7, :array_0

    move-object v0, p0

    move-object v1, p1

    move-wide v4, v2

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/utils/TrackerHelper;->getAvgActivityPerWeek(Ljava/util/List;DDI[I)F

    move-result v9

    .line 546
    const/4 v0, 0x0

    cmpl-float v0, v9, v0

    if-ltz v0, :cond_6

    cmpg-float v0, v9, v10

    if-gtz v0, :cond_6

    .line 547
    const/4 v8, 0x0

    .line 556
    :cond_3
    :goto_1
    const/4 v0, -0x1

    if-eq v8, v0, :cond_4

    .line 557
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/Answer;->setPreselected(Z)V

    .line 558
    :cond_4
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 559
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Selected Answer Index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    :cond_5
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    sget-object v0, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "setSuggestedAnswerInAnswerListforQ9 : END"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 548
    :cond_6
    cmpl-float v0, v9, v10

    if-lez v0, :cond_7

    cmpg-float v0, v9, v11

    if-gtz v0, :cond_7

    .line 549
    const/4 v8, 0x1

    goto :goto_1

    .line 550
    :cond_7
    cmpl-float v0, v9, v11

    if-lez v0, :cond_8

    cmpg-float v0, v9, v12

    if-gtz v0, :cond_8

    .line 551
    const/4 v8, 0x2

    goto :goto_1

    .line 553
    :cond_8
    cmpl-float v0, v9, v12

    if-lez v0, :cond_3

    .line 554
    const/4 v8, 0x3

    goto :goto_1

    .line 543
    :array_0
    .array-data 4
        0x4653
        0x4654
        0x4655
    .end array-data
.end method


# virtual methods
.method prepareMissionDetailListFromMetExcerciseTracker(Ljava/util/List;Lcom/cigna/coach/dataobjects/TrackerRuleData;Ljava/util/Map;)Ljava/util/List;
    .locals 20
    .param p2, "trackerRuleData"    # Lcom/cigna/coach/dataobjects/TrackerRuleData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/cigna/coach/dataobjects/TrackerRuleData;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "samsungExcerciseIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local p3, "activityPerDayMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;>;"
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 269
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v18, "processMetExerciseTrackerInfoToMission : Start"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 273
    .local v9, "missionDetailsTobeAdded":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->size()I

    move-result v17

    if-lez v17, :cond_14

    .line 274
    invoke-interface/range {p3 .. p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_14

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 275
    .local v7, "metExerciseTrackerPerDayList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    if-eqz v7, :cond_1

    .line 276
    const/4 v5, 0x0

    .line 277
    .local v5, "isAddMissionDetails":Z
    const/4 v14, 0x0

    .line 278
    .local v14, "totalSteps":I
    const/16 v16, 0x0

    .line 279
    .local v16, "walkingSteps":I
    const/4 v2, 0x0

    .line 280
    .local v2, "duration":I
    const/4 v11, 0x0

    .line 281
    .local v11, "runningSteps":I
    const/4 v15, 0x0

    .line 282
    .local v15, "updownSteps":I
    const-wide/16 v12, 0x0

    .line 284
    .local v12, "timeInMillisec":J
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/dataobjects/MetExerciseTracker;

    .line 285
    .local v6, "metExerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTracker;
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 286
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "metExerciseTracker SamsungExerciseId:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getExerciseInfoId()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_3
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getExerciseInfoId()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 295
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getMetValue()F

    move-result v17

    const/high16 v18, 0x42c80000    # 100.0f

    mul-float v10, v17, v18

    .line 296
    .local v10, "retrievedMetValue":F
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDateInMsec()J

    move-result-wide v12

    .line 298
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getMinMetValue()I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getMaxMetValue()I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getMinMetValue()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpl-float v17, v10, v17

    if-ltz v17, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getMaxMetValue()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpg-float v17, v10, v17

    if-lez v17, :cond_5

    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getMinMetValue()I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getMinMetValue()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    cmpl-float v17, v10, v17

    if-ltz v17, :cond_2

    .line 302
    :cond_5
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 303
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "trackerRuleData.getTrackerValueTypeInd():"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValueTypeInd()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "trackerRuleData.getTrackerValue():"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValue()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "date:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_6
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper$1;->$SwitchMap$com$cigna$coach$dataobjects$GoalMissionData$TrackerValueType:[I

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValueTypeInd()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_0

    goto/16 :goto_1

    .line 310
    :pswitch_0
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 311
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "NO_OF_MINS:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDurationMin()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_7
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getDurationMin()I

    move-result v17

    add-int v2, v2, v17

    .line 314
    goto/16 :goto_1

    .line 316
    :pswitch_1
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 317
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "metExerciseTracker.getTotalSteps():"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getTotalSteps()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    :cond_8
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getTotalSteps()I

    move-result v17

    add-int v14, v14, v17

    .line 320
    goto/16 :goto_1

    .line 322
    :pswitch_2
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 323
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "metExerciseTracker.getRunningSteps():"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getRunningSteps()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_9
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getRunningSteps()I

    move-result v17

    add-int v11, v11, v17

    .line 326
    goto/16 :goto_1

    .line 328
    :pswitch_3
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 329
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "metExerciseTracker.getWalkingSteps():"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getWalkingSteps()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :cond_a
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getWalkingSteps()I

    move-result v17

    add-int v16, v16, v17

    .line 332
    goto/16 :goto_1

    .line 334
    :pswitch_4
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 335
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "metExerciseTracker.getUpdownSteps():"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getUpdownSteps()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_b
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->getUpdownSteps()I

    move-result v17

    add-int v15, v15, v17

    .line 338
    goto/16 :goto_1

    .line 345
    .end local v6    # "metExerciseTracker":Lcom/cigna/coach/dataobjects/MetExerciseTracker;
    .end local v10    # "retrievedMetValue":F
    :cond_c
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper$1;->$SwitchMap$com$cigna$coach$dataobjects$GoalMissionData$TrackerValueType:[I

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValueTypeInd()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_1

    .line 390
    :cond_d
    :goto_2
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v5, v0, :cond_1

    .line 391
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_e

    .line 392
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Adding Mission details to the List"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_e
    new-instance v8, Lcom/cigna/coach/apiobjects/MissionDetails;

    invoke-direct {v8}, Lcom/cigna/coach/apiobjects/MissionDetails;-><init>()V

    .line 395
    .local v8, "missionDetails":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-static {v12, v13}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/cigna/coach/apiobjects/MissionDetails;->setMissionActivityCompletedDate(Ljava/util/Calendar;)V

    .line 396
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/cigna/coach/apiobjects/MissionDetails;->setNoOfActivities(I)V

    .line 397
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/cigna/coach/apiobjects/MissionDetails;->setexerciseIds(Ljava/util/List;)V

    .line 398
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 347
    .end local v8    # "missionDetails":Lcom/cigna/coach/apiobjects/MissionDetails;
    :pswitch_5
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 348
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Sum of Duration:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    :cond_f
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValue()I

    move-result v17

    move/from16 v0, v17

    if-lt v2, v0, :cond_d

    .line 351
    const/4 v5, 0x1

    goto :goto_2

    .line 355
    :pswitch_6
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 356
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "totalSteps:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_10
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValue()I

    move-result v17

    move/from16 v0, v17

    if-lt v14, v0, :cond_d

    .line 359
    const/4 v5, 0x1

    goto/16 :goto_2

    .line 363
    :pswitch_7
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_11

    .line 364
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "runningSteps:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    :cond_11
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValue()I

    move-result v17

    move/from16 v0, v17

    if-lt v11, v0, :cond_d

    .line 367
    const/4 v5, 0x1

    goto/16 :goto_2

    .line 371
    :pswitch_8
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_12

    .line 372
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "walkingSteps: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_12
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValue()I

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_d

    .line 375
    const/4 v5, 0x1

    goto/16 :goto_2

    .line 379
    :pswitch_9
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_13

    .line 380
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "updownSteps: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_13
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerValue()I

    move-result v17

    move/from16 v0, v17

    if-lt v15, v0, :cond_d

    .line 383
    const/4 v5, 0x1

    goto/16 :goto_2

    .line 403
    .end local v2    # "duration":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "isAddMissionDetails":Z
    .end local v7    # "metExerciseTrackerPerDayList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    .end local v11    # "runningSteps":I
    .end local v12    # "timeInMillisec":J
    .end local v14    # "totalSteps":I
    .end local v15    # "updownSteps":I
    .end local v16    # "walkingSteps":I
    :cond_14
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_15

    .line 404
    sget-object v17, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v18, "processMetExerciseTrackerInfoToMission : End"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    :cond_15
    return-object v9

    .line 308
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 345
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public processTrackerMissionForChangeWhileOffline(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "defaultUserId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 748
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 750
    .local v4, "returnNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    invoke-static {}, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->values()[Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v5, v0, v1

    .line 751
    .local v5, "trackerEventType":Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    invoke-virtual {p0, p1, v5}, Lcom/cigna/coach/utils/TrackerHelper;->processTrackerMissionsForChange(Ljava/lang/String;Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)Ljava/util/List;

    move-result-object v3

    .line 752
    .local v3, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    invoke-interface {v4, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 750
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 754
    .end local v3    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v5    # "trackerEventType":Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    :cond_0
    return-object v4
.end method

.method public processTrackerMissionsForChange(Ljava/lang/String;Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)Ljava/util/List;
    .locals 25
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "trackerType"    # Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 77
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v17, "returnNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 80
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v23, "processTrackerMissionsForChange : START"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_0
    new-instance v12, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/TrackerHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 84
    .local v12, "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionsWithTrackerRules(Ljava/lang/String;)Ljava/util/List;

    move-result-object v21

    .line 85
    .local v21, "userMissionDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 86
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Found "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " User active Tracker Missions"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_1
    new-instance v7, Lcom/cigna/coach/utils/GoalMissionHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/TrackerHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Lcom/cigna/coach/utils/GoalMissionHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 90
    .local v7, "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_13

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 91
    .local v20, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 92
    if-nez v20, :cond_10

    .line 93
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v23, "Checking for updates to null mission"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    :cond_3
    :goto_0
    if-eqz v20, :cond_2

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerRuleId()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v22

    if-eqz v22, :cond_2

    .line 102
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 103
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Mission TrackerType:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "TrackerEventType:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_4
    sget-object v22, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->EXERCISE:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_5

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->EXERCISE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_9

    :cond_5
    sget-object v22, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->PEDOMETER_DATA:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_6

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->PEDOMETER:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_9

    :cond_6
    sget-object v22, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->FOOD:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_7

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->FOOD:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_9

    :cond_7
    sget-object v22, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->SLEEP:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_8

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->SLEEP:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_9

    :cond_8
    sget-object v22, Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;->WEIGHT:Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-object/from16 v0, p2

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_2

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->WEIGHT:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-ne v0, v1, :cond_2

    .line 112
    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/TrackerHelper;->runTrackerRulesForMission(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v19

    .line 113
    .local v19, "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lcom/cigna/coach/db/MissionDBManager;->getUserMissionDetailsList(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v10

    .line 114
    .local v10, "manualActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    move-object/from16 v0, v19

    invoke-virtual {v7, v10, v0}, Lcom/cigna/coach/utils/GoalMissionHelper;->mergeManualAndTrackerActivities(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v13

    .line 116
    .local v13, "missionDetailActivityList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v22

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1, v13}, Lcom/cigna/coach/utils/GoalMissionHelper;->processMissionDetailsUpdate(Ljava/lang/String;ILjava/util/List;)Lcom/cigna/coach/dataobjects/GoalMissionStatusData;

    move-result-object v18

    .line 119
    .local v18, "status":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 120
    if-nez v18, :cond_11

    .line 121
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v23, "Status: null"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    :cond_a
    :goto_1
    if-eqz v18, :cond_2

    .line 129
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    invoke-virtual/range {v22 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 130
    .local v6, "goalCompleted":Z
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual/range {v22 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 132
    .local v11, "missionCompleted":Z
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 133
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v23, "processTrackerMissionsForChange : START"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    :cond_b
    if-eqz v11, :cond_c

    .line 137
    new-instance v2, Lcom/cigna/coach/utils/BadgeAwarder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/TrackerHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-direct {v2, v0, v1}, Lcom/cigna/coach/utils/BadgeAwarder;-><init>(Ljava/lang/String;Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 138
    .local v2, "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    invoke-virtual {v2}, Lcom/cigna/coach/utils/BadgeAwarder;->awardBadges()Ljava/util/List;

    move-result-object v3

    .line 139
    .local v3, "badgesEarned":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setBadgeList(Ljava/util/List;)V

    .line 141
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 142
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Badges earned: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    .end local v2    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .end local v3    # "badgesEarned":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :cond_c
    new-instance v15, Lcom/cigna/coach/utils/NotificationHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/TrackerHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v15, v0}, Lcom/cigna/coach/utils/NotificationHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 153
    .local v15, "notificationHelper":Lcom/cigna/coach/utils/NotificationHelper;
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v15, v0, v1, v11, v6}, Lcom/cigna/coach/utils/NotificationHelper;->getSetMessionDataNotifications(Lcom/cigna/coach/dataobjects/UserMissionData;Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object v16

    .line 157
    .local v16, "perMissionNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    new-instance v4, Lcom/cigna/coach/utils/ContentHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/TrackerHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 159
    .local v4, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 161
    new-instance v5, Lcom/cigna/coach/utils/ContextData;

    invoke-direct {v5}, Lcom/cigna/coach/utils/ContextData;-><init>()V

    .line 162
    .local v5, "contextData":Lcom/cigna/coach/utils/ContextData;
    sget-object v22, Lcom/cigna/coach/utils/ContextData$ContextDataType;->GOAL_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalContentId()I

    move-result v23

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V

    .line 163
    sget-object v22, Lcom/cigna/coach/utils/ContextData$ContextDataType;->MISSION_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionContentId()I

    move-result v23

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V

    .line 164
    sget-object v22, Lcom/cigna/coach/utils/ContextData$ContextDataType;->TRACKER_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getTrackerNameContentId()I

    move-result v23

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v5, v0, v1}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V

    .line 166
    move-object/from16 v0, v16

    invoke-virtual {v4, v0, v5}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;)V

    .line 168
    if-eqz v16, :cond_2

    .line 169
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_d

    .line 170
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "adding notifications"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_d
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_e
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/apiobjects/Notification;

    .line 173
    .local v14, "notification":Lcom/cigna/coach/apiobjects/Notification;
    if-eqz v14, :cond_12

    if-eqz v17, :cond_12

    .line 174
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_f

    .line 175
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "adding notification to list "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v14}, Lcom/cigna/coach/apiobjects/Notification;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_f
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 95
    .end local v4    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v5    # "contextData":Lcom/cigna/coach/utils/ContextData;
    .end local v6    # "goalCompleted":Z
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "manualActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v11    # "missionCompleted":Z
    .end local v13    # "missionDetailActivityList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v14    # "notification":Lcom/cigna/coach/apiobjects/Notification;
    .end local v15    # "notificationHelper":Lcom/cigna/coach/utils/NotificationHelper;
    .end local v16    # "perMissionNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v18    # "status":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .end local v19    # "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :cond_10
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Checking for updates to mission "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " (seq id "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerRuleId()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v22

    if-nez v22, :cond_3

    .line 97
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v23, "Mission is not a tracker mission"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 123
    .restart local v10    # "manualActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .restart local v13    # "missionDetailActivityList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .restart local v18    # "status":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .restart local v19    # "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :cond_11
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Mission status: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Goal status: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 179
    .restart local v4    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v5    # "contextData":Lcom/cigna/coach/utils/ContextData;
    .restart local v6    # "goalCompleted":Z
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v11    # "missionCompleted":Z
    .restart local v14    # "notification":Lcom/cigna/coach/apiobjects/Notification;
    .restart local v15    # "notificationHelper":Lcom/cigna/coach/utils/NotificationHelper;
    .restart local v16    # "perMissionNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    :cond_12
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_e

    .line 180
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v23, "No notification to send"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 189
    .end local v4    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v5    # "contextData":Lcom/cigna/coach/utils/ContextData;
    .end local v6    # "goalCompleted":Z
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "manualActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v11    # "missionCompleted":Z
    .end local v13    # "missionDetailActivityList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v14    # "notification":Lcom/cigna/coach/apiobjects/Notification;
    .end local v15    # "notificationHelper":Lcom/cigna/coach/utils/NotificationHelper;
    .end local v16    # "perMissionNotifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v18    # "status":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .end local v19    # "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v20    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_13
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_14

    .line 190
    sget-object v22, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v23, "processTrackerMissionsForChange : END"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_14
    return-object v17
.end method

.method public runTrackerRulesForMission(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;
    .locals 23
    .param p1, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 196
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerRuleId()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    if-ne v8, v9, :cond_2

    .line 197
    :cond_0
    new-instance v19, Ljava/util/ArrayList;

    const/4 v8, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 261
    :cond_1
    :goto_0
    return-object v19

    .line 198
    :cond_2
    const/16 v19, 0x0

    .line 199
    .local v19, "missionDetailActivityList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 200
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Running rules for Mission:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_3
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 209
    .local v4, "startTimeInMillis":J
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/CalendarUtil;->getEndOfDayBefore(J)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSpanInDays()I

    move-result v10

    invoke-static {v10}, Lcom/cigna/coach/utils/CalendarUtil;->fromDaysToMilliSecs(I)J

    move-result-wide v10

    add-long v6, v8, v10

    .line 212
    .local v6, "endTimeInMillis":J
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 213
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Start time:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v10

    invoke-static {v10}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "End time:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v10

    invoke-static {v10}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_4
    new-instance v21, Lcom/cigna/coach/db/TrackerDBManager;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cigna/coach/utils/TrackerHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, v21

    invoke-direct {v0, v8}, Lcom/cigna/coach/db/TrackerDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 218
    .local v21, "trackerDBManager":Lcom/cigna/coach/db/TrackerDBManager;
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerRuleId()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/cigna/coach/db/TrackerDBManager;->getTrackerRuleInfo(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;)Lcom/cigna/coach/dataobjects/TrackerRuleData;

    move-result-object v22

    .line 219
    .local v22, "trackerRuleData":Lcom/cigna/coach/dataobjects/TrackerRuleData;
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSpecificTracker()Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/cigna/coach/db/TrackerDBManager;->getSamsungExerciseIds(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)Ljava/util/List;

    move-result-object v20

    .line 220
    .local v20, "samsungExcerciseIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-nez v20, :cond_6

    .line 221
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 222
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v9, "samsungExcerciseIds is null"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    .end local v4    # "startTimeInMillis":J
    .end local v6    # "endTimeInMillis":J
    .end local v20    # "samsungExcerciseIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v21    # "trackerDBManager":Lcom/cigna/coach/db/TrackerDBManager;
    .end local v22    # "trackerRuleData":Lcom/cigna/coach/dataobjects/TrackerRuleData;
    :cond_5
    :goto_1
    if-nez v19, :cond_1

    new-instance v19, Ljava/util/ArrayList;

    .end local v19    # "missionDetailActivityList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    const/4 v8, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    goto/16 :goto_0

    .line 225
    .restart local v4    # "startTimeInMillis":J
    .restart local v6    # "endTimeInMillis":J
    .restart local v19    # "missionDetailActivityList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .restart local v20    # "samsungExcerciseIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v21    # "trackerDBManager":Lcom/cigna/coach/db/TrackerDBManager;
    .restart local v22    # "trackerRuleData":Lcom/cigna/coach/dataobjects/TrackerRuleData;
    :cond_6
    :try_start_1
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 226
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v9, "samsungExcerciseIds mapped with mission:"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v17

    .line 228
    .local v17, "id":I
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "id:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 256
    .end local v4    # "startTimeInMillis":J
    .end local v6    # "endTimeInMillis":J
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "id":I
    .end local v20    # "samsungExcerciseIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v21    # "trackerDBManager":Lcom/cigna/coach/db/TrackerDBManager;
    .end local v22    # "trackerRuleData":Lcom/cigna/coach/dataobjects/TrackerRuleData;
    :catch_0
    move-exception v15

    .line 257
    .local v15, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in runTrackerRulesForMission: "

    invoke-static {v8, v9, v15}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 231
    .end local v15    # "e":Ljava/lang/Exception;
    .restart local v4    # "startTimeInMillis":J
    .restart local v6    # "endTimeInMillis":J
    .restart local v20    # "samsungExcerciseIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v21    # "trackerDBManager":Lcom/cigna/coach/db/TrackerDBManager;
    .restart local v22    # "trackerRuleData":Lcom/cigna/coach/dataobjects/TrackerRuleData;
    :cond_7
    :try_start_2
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    if-eqz v22, :cond_8

    .line 232
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Fetching data from :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerTable()Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_8
    if-eqz v22, :cond_5

    .line 235
    new-instance v3, Lcom/cigna/mobile/coach/DataProcessor;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cigna/coach/utils/TrackerHelper;->context:Landroid/content/Context;

    invoke-direct {v3, v8}, Lcom/cigna/mobile/coach/DataProcessor;-><init>(Landroid/content/Context;)V

    .line 236
    .local v3, "dataProcessor":Lcom/cigna/mobile/coach/DataProcessor;
    const/16 v18, 0x0

    .line 237
    .local v18, "metExerciseTrackerList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper$1;->$SwitchMap$com$cigna$coach$dataobjects$GoalMissionData$SamsungTrackerTable:[I

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->getTrackerTable()Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    move-result-object v9

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 247
    :goto_3
    if-eqz v18, :cond_5

    .line 248
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v4, v5}, Lcom/cigna/coach/utils/TrackerHelper;->getActivityMapPerDay(Ljava/util/List;J)Ljava/util/Map;

    move-result-object v14

    .line 249
    .local v14, "activityPerDayMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;>;"
    if-eqz v14, :cond_5

    .line 250
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2, v14}, Lcom/cigna/coach/utils/TrackerHelper;->prepareMissionDetailListFromMetExcerciseTracker(Ljava/util/List;Lcom/cigna/coach/dataobjects/TrackerRuleData;Ljava/util/Map;)Ljava/util/List;

    move-result-object v19

    goto/16 :goto_1

    .line 239
    .end local v14    # "activityPerDayMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;>;"
    :pswitch_0
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v3 .. v9}, Lcom/cigna/mobile/coach/DataProcessor;->getMetExerciseTrackerInfo(JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;

    move-result-object v18

    .line 240
    goto :goto_3

    .line 242
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/cigna/coach/utils/TrackerHelper;->context:Landroid/content/Context;

    move-object v8, v3

    move-wide v10, v4

    move-wide v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/mobile/coach/DataProcessor;->getPedoData(Landroid/content/Context;JJ)Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v18

    .line 243
    goto :goto_3

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method setSuggestedAnswerInAnswerListforQ12(Lcom/cigna/coach/apiobjects/UserMetrics;Ljava/util/List;)V
    .locals 3
    .param p1, "userMetrics"    # Lcom/cigna/coach/apiobjects/UserMetrics;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/apiobjects/UserMetrics;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 632
    .local p2, "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 640
    :cond_0
    :goto_0
    return-void

    .line 634
    :cond_1
    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    .line 635
    .local v0, "answer":Lcom/cigna/coach/apiobjects/Answer;
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getHeight()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    if-ne v1, v2, :cond_0

    .line 637
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getHeight()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/Answer;->setAnswer(Ljava/lang/String;)V

    .line 638
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/Answer;->setPreselected(Z)V

    goto :goto_0
.end method

.method setSuggestedAnswerInAnswerListforQ13(Lcom/cigna/coach/apiobjects/UserMetrics;Ljava/util/List;)V
    .locals 3
    .param p1, "userMetrics"    # Lcom/cigna/coach/apiobjects/UserMetrics;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/apiobjects/UserMetrics;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Answer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 644
    .local p2, "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 652
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    const/4 v1, 0x0

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/Answer;

    .line 648
    .local v0, "answer":Lcom/cigna/coach/apiobjects/Answer;
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getWeight()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    if-ne v1, v2, :cond_0

    .line 649
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getWeight()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/Answer;->setAnswer(Ljava/lang/String;)V

    .line 650
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/Answer;->setPreselected(Z)V

    goto :goto_0
.end method

.method public setTrackerSuggestedAnswers(Landroid/content/Context;Lcom/cigna/coach/apiobjects/CategoryGroupQAs;)V
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categoryGroupQAs"    # Lcom/cigna/coach/apiobjects/CategoryGroupQAs;

    .prologue
    .line 411
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 412
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "setTrackerSuggestedAnswers: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->getEndOfDayBefore(J)Ljava/util/Calendar;

    move-result-object v12

    .line 419
    .local v12, "endDate":Ljava/util/Calendar;
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    const-wide/32 v23, 0x48190800

    sub-long v7, v7, v23

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v21

    .line 421
    .local v21, "startDate":Ljava/util/Calendar;
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    .line 422
    .local v5, "endDateInMillis":J
    invoke-virtual/range {v21 .. v21}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 424
    .local v3, "startMillisecs":J
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 425
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "End Date = "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v12}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Start Date = "

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :cond_1
    new-instance v2, Lcom/cigna/mobile/coach/DataProcessor;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/cigna/mobile/coach/DataProcessor;-><init>(Landroid/content/Context;)V

    .line 431
    .local v2, "dataProcessor":Lcom/cigna/mobile/coach/DataProcessor;
    const/16 v16, 0x0

    .line 432
    .local v16, "lifeStyleExerciseTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    const/16 v22, 0x0

    .line 433
    .local v22, "userMetrics":Lcom/cigna/coach/apiobjects/UserMetrics;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;->entrySet()Ljava/util/Set;

    move-result-object v10

    .line 434
    .local v10, "categoryTypeKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;>;"
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_2
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/Map$Entry;

    .line 435
    .local v9, "categoryType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;"
    sget-object v8, Lcom/cigna/coach/utils/TrackerHelper$1;->$SwitchMap$com$cigna$coach$interfaces$ILifeStyle$CategoryType:[I

    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v7}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->ordinal()I

    move-result v7

    aget v7, v8, v7

    packed-switch v7, :pswitch_data_0

    .line 453
    :cond_3
    :goto_1
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/List;

    .line 454
    .local v20, "questionsAnswersGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    if-eqz v20, :cond_2

    .line 455
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;

    .line 456
    .local v19, "questionsAnswersGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
    if-eqz v19, :cond_4

    .line 457
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/cigna/coach/apiobjects/QuestionAnswers;

    .line 458
    .local v18, "questionAnswers":Lcom/cigna/coach/apiobjects/QuestionAnswers;
    if-eqz v18, :cond_5

    .line 459
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getQuestionId()I

    move-result v7

    packed-switch v7, :pswitch_data_1

    :pswitch_0
    goto :goto_2

    .line 462
    :pswitch_1
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getAnswerList()Ljava/util/List;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v7}, Lcom/cigna/coach/utils/TrackerHelper;->setSuggestedAnswerInAnswerListforQ8(Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 493
    .end local v2    # "dataProcessor":Lcom/cigna/mobile/coach/DataProcessor;
    .end local v3    # "startMillisecs":J
    .end local v5    # "endDateInMillis":J
    .end local v9    # "categoryType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;"
    .end local v10    # "categoryTypeKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;>;"
    .end local v12    # "endDate":Ljava/util/Calendar;
    .end local v15    # "i$":Ljava/util/Iterator;
    .end local v16    # "lifeStyleExerciseTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    .end local v18    # "questionAnswers":Lcom/cigna/coach/apiobjects/QuestionAnswers;
    .end local v19    # "questionsAnswersGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
    .end local v20    # "questionsAnswersGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .end local v21    # "startDate":Ljava/util/Calendar;
    .end local v22    # "userMetrics":Lcom/cigna/coach/apiobjects/UserMetrics;
    :catch_0
    move-exception v11

    .line 494
    .local v11, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error in setTrackerSuggestedAnswers: "

    invoke-static {v7, v8, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 496
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_6
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 497
    sget-object v7, Lcom/cigna/coach/utils/TrackerHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "setTrackerSuggestedAnswers: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    :cond_7
    return-void

    .line 437
    .restart local v2    # "dataProcessor":Lcom/cigna/mobile/coach/DataProcessor;
    .restart local v3    # "startMillisecs":J
    .restart local v5    # "endDateInMillis":J
    .restart local v9    # "categoryType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;"
    .restart local v10    # "categoryTypeKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;>;>;"
    .restart local v12    # "endDate":Ljava/util/Calendar;
    .restart local v16    # "lifeStyleExerciseTrackers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;"
    .restart local v21    # "startDate":Ljava/util/Calendar;
    .restart local v22    # "userMetrics":Lcom/cigna/coach/apiobjects/UserMetrics;
    :pswitch_2
    const/4 v7, 0x0

    const/4 v8, 0x0

    :try_start_1
    invoke-virtual/range {v2 .. v8}, Lcom/cigna/mobile/coach/DataProcessor;->getMetExerciseTrackerInfo(JJLcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)Ljava/util/List;

    move-result-object v16

    .line 438
    const/16 v17, 0x0

    .line 439
    .local v17, "perDayActiviltyMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;>;"
    if-eqz v16, :cond_8

    .line 441
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v3, v4}, Lcom/cigna/coach/utils/TrackerHelper;->getActivityMapPerDay(Ljava/util/List;J)Ljava/util/Map;

    move-result-object v17

    .line 443
    :cond_8
    if-eqz v17, :cond_2

    invoke-interface/range {v17 .. v17}, Ljava/util/Map;->size()I

    move-result v7

    const/4 v8, 0x6

    if-ge v7, v8, :cond_3

    goto/16 :goto_0

    .line 448
    .end local v17    # "perDayActiviltyMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/util/List<Lcom/cigna/coach/dataobjects/MetExerciseTracker;>;>;"
    :pswitch_3
    invoke-virtual {v2}, Lcom/cigna/mobile/coach/DataProcessor;->getUserProfileInfo()Lcom/cigna/coach/apiobjects/UserMetrics;

    move-result-object v22

    .line 449
    goto :goto_1

    .line 466
    .restart local v15    # "i$":Ljava/util/Iterator;
    .restart local v18    # "questionAnswers":Lcom/cigna/coach/apiobjects/QuestionAnswers;
    .restart local v19    # "questionsAnswersGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
    .restart local v20    # "questionsAnswersGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    :pswitch_4
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getAnswerList()Ljava/util/List;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v7}, Lcom/cigna/coach/utils/TrackerHelper;->setSuggestedAnswerInAnswerListforQ9(Ljava/util/List;Ljava/util/List;)V

    goto :goto_2

    .line 478
    :pswitch_5
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getAnswerList()Ljava/util/List;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v7}, Lcom/cigna/coach/utils/TrackerHelper;->setSuggestedAnswerInAnswerListforQ12(Lcom/cigna/coach/apiobjects/UserMetrics;Ljava/util/List;)V

    goto :goto_2

    .line 481
    :pswitch_6
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/apiobjects/QuestionAnswers;->getAnswerList()Ljava/util/List;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v7}, Lcom/cigna/coach/utils/TrackerHelper;->setSuggestedAnswerInAnswerListforQ13(Lcom/cigna/coach/apiobjects/UserMetrics;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 435
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 459
    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
