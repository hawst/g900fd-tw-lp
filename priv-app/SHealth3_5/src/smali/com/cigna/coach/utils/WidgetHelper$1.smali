.class synthetic Lcom/cigna/coach/utils/WidgetHelper$1;
.super Ljava/lang/Object;
.source "WidgetHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/WidgetHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 92
    invoke-static {}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->values()[Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    :try_start_0
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->CATCH_ALL:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_STATUS_NOTIFICATION:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_GOALS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_MISSIONS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_0
.end method
