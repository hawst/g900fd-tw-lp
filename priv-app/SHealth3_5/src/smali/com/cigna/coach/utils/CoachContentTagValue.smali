.class public interface abstract annotation Lcom/cigna/coach/utils/CoachContentTagValue;
.super Ljava/lang/Object;
.source "CoachContentTagValue.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lcom/cigna/coach/utils/CoachContentTagValue;
        tagValueType = .enum Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
    .end subannotation
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
    }
.end annotation

.annotation runtime Ljava/lang/annotation/Inherited;
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->METHOD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract tagName()Ljava/lang/String;
.end method

.method public abstract tagValueType()Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
.end method
