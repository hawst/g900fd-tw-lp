.class public final Lcom/cigna/coach/utils/backuprestore/JournalDataParser;
.super Ljava/lang/Object;
.source "JournalDataParser.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static PREFIX_CD_BASE_ID:Ljava/lang/String;

.field private static PREFIX_DB_MODEL_VERSION:Ljava/lang/String;

.field private static userTableFieldMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static userTableFieldMapping:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->userTableFieldMapping:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->userTableFieldMap:Ljava/util/Map;

    .line 55
    const-string v0, "CD_BASE_ID_"

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->PREFIX_CD_BASE_ID:Ljava/lang/String;

    .line 56
    const-string v0, "DB_MODEL_VERS_"

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->PREFIX_DB_MODEL_VERSION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findMarker(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 89
    .local p0, "journalEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 90
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTableName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "__MARKER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 89
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 94
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private static getUserTableFieldMapping(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 299
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->userTableFieldMapping:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 300
    sget v0, Lcom/cigna/coach/R$raw;->usr_table_field_mapper:I

    invoke-static {p0, v0}, Lcom/cigna/coach/utils/CommonUtils;->readLocalResourceAsString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->userTableFieldMapping:Ljava/lang/String;

    .line 302
    :cond_0
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->userTableFieldMapping:Ljava/lang/String;

    return-object v0
.end method

.method private static getUserTableFields(Landroid/content/Context;IILjava/lang/String;)Ljava/util/TreeSet;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "codeBaseId"    # I
    .param p2, "dbModelVersion"    # I
    .param p3, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 261
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "|"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string/jumbo v15, "|"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 262
    .local v10, "lookupKey":Ljava/lang/String;
    sget-object v14, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->userTableFieldMap:Ljava/util/Map;

    invoke-interface {v14, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/TreeSet;

    .line 263
    .local v11, "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    invoke-static {}, Lcom/cigna/coach/utils/ExpressionEvaluator;->getInstance()Lcom/cigna/coach/utils/ExpressionEvaluator;

    move-result-object v4

    .line 265
    .local v4, "evaluator":Lcom/cigna/coach/utils/ExpressionEvaluator;
    if-nez v11, :cond_2

    .line 267
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 268
    .local v13, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v14, "codeBaseId"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->PREFIX_CD_BASE_ID:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v13, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    const-string v14, "dbModelVersion"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->PREFIX_DB_MODEL_VERSION:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v13, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    const-string/jumbo v14, "tableName"

    move-object/from16 v0, p3

    invoke-interface {v13, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->getUserTableFieldMapping(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v4, v14, v15}, Lcom/cigna/coach/utils/ExpressionEvaluator;->evaluate(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v3

    .line 275
    .local v3, "evalResult":Ljava/lang/Object;
    if-eqz v3, :cond_2

    instance-of v14, v3, Ljava/util/Map;

    if-eqz v14, :cond_2

    .line 276
    const-string v14, "lookupMap"

    invoke-interface {v13, v14, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    const-string v14, "lookupMap[codeBaseId][dbModelVersion][tableName]"

    invoke-virtual {v4, v14, v13}, Lcom/cigna/coach/utils/ExpressionEvaluator;->evaluate(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 278
    .local v9, "listOfFields":Ljava/lang/String;
    const/4 v14, 0x0

    new-array v6, v14, [Ljava/lang/String;

    .line 279
    .local v6, "fields":[Ljava/lang/String;
    if-eqz v9, :cond_0

    .line 280
    const-string v14, "\\,"

    invoke-virtual {v9, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 282
    :cond_0
    new-instance v12, Ljava/util/TreeSet;

    invoke-direct {v12}, Ljava/util/TreeSet;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    .end local v11    # "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    .local v12, "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    move-object v1, v6

    .local v1, "arr$":[Ljava/lang/String;
    :try_start_1
    array-length v8, v1

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v5, v1, v7

    .line 284
    .local v5, "field":Ljava/lang/String;
    invoke-virtual {v12, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 283
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 286
    .end local v5    # "field":Ljava/lang/String;
    :cond_1
    sget-object v14, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->userTableFieldMap:Ljava/util/Map;

    invoke-interface {v14, v10, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v11, v12

    .line 295
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v3    # "evalResult":Ljava/lang/Object;
    .end local v6    # "fields":[Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "listOfFields":Ljava/lang/String;
    .end local v12    # "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    .end local v13    # "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v11    # "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    :cond_2
    return-object v11

    .line 289
    .restart local v13    # "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v2

    .line 290
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    sget-object v14, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "Error in getUserTableFields: "

    invoke-static {v14, v15, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 292
    new-instance v14, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v14, v2}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v14

    .line 289
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v11    # "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    .restart local v1    # "arr$":[Ljava/lang/String;
    .restart local v3    # "evalResult":Ljava/lang/Object;
    .restart local v6    # "fields":[Ljava/lang/String;
    .restart local v9    # "listOfFields":Ljava/lang/String;
    .restart local v12    # "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    :catch_1
    move-exception v2

    move-object v11, v12

    .end local v12    # "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    .restart local v11    # "returnSet":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    goto :goto_1
.end method

.method public static group(Ljava/util/List;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;
    .locals 17
    .param p1, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;>;>;"
        }
    .end annotation

    .prologue
    .line 147
    .local p0, "journalEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    sget-object v12, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 148
    sget-object v12, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "Grouping journal entries by code base, db version, table"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 153
    .local v7, "parsedData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;>;"
    const/4 v4, 0x0

    .line 154
    .local v4, "groupedEntries":I
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .line 155
    .local v10, "userTableData":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    sget-object v12, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "groupedEntries":I
    .local v5, "groupedEntries":I
    int-to-double v13, v4

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v13, v15

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v15

    int-to-double v15, v15

    div-double/2addr v13, v15

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13, v14}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 156
    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTableName()Ljava/lang/String;

    move-result-object v9

    .line 157
    .local v9, "tableName":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getCodeBaseId()I

    move-result v1

    .line 158
    .local v1, "codeBaseId":I
    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getDbModelVersion()I

    move-result v3

    .line 160
    .local v3, "dbModelVersion":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 161
    .local v2, "codeBaseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    if-nez v2, :cond_1

    .line 162
    new-instance v2, Ljava/util/HashMap;

    .end local v2    # "codeBaseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 163
    .restart local v2    # "codeBaseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v7, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v2, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map;

    .line 167
    .local v11, "versionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    if-nez v11, :cond_2

    .line 168
    new-instance v11, Ljava/util/HashMap;

    .end local v11    # "versionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 169
    .restart local v11    # "versionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v2, v12, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_2
    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    .line 173
    .local v8, "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    if-nez v8, :cond_3

    .line 174
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 175
    .restart local v8    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    invoke-interface {v11, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    :cond_3
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v4, v5

    .line 179
    .end local v5    # "groupedEntries":I
    .restart local v4    # "groupedEntries":I
    goto :goto_0

    .line 180
    .end local v1    # "codeBaseId":I
    .end local v2    # "codeBaseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .end local v3    # "dbModelVersion":I
    .end local v8    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    .end local v9    # "tableName":Ljava/lang/String;
    .end local v10    # "userTableData":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .end local v11    # "versionMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    :cond_4
    sget-object v12, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p1

    invoke-interface {v0, v12, v13, v14}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 182
    return-object v7
.end method

.method public static parse(Landroid/content/Context;Lorg/json/JSONObject;)Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "journalEntry"    # Lorg/json/JSONObject;

    .prologue
    .line 196
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 197
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Parsing journal entry"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_0
    const/16 v19, 0x0

    .line 203
    .local v19, "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :try_start_0
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 204
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "table name:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v22, "tblNm"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "code base id:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v22, "cdBaseId"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "db model version:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v22, "dbVersn"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "data:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v22, "data"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_1
    const-string/jumbo v9, "seqId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 211
    .local v3, "seqId":J
    const-string v9, "cdBaseId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 212
    .local v5, "codeBaseId":I
    const-string v9, "dbVersn"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 213
    .local v6, "dbModelVersion":I
    const-string/jumbo v9, "tblNm"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 214
    .local v7, "tableName":Ljava/lang/String;
    const-string v9, "key"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 215
    .local v8, "key":Ljava/lang/String;
    const-string v9, "actnCd"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 216
    .local v11, "actionCode":I
    const-string v9, "actnTimestamp"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v20

    .line 218
    .local v20, "transactionTimestamp":J
    new-instance v2, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    invoke-static {v11}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getInstance(I)Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v9

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v10

    invoke-direct/range {v2 .. v10}, Lcom/cigna/coach/dataobjects/UserTableJournalData;-><init>(JIILjava/lang/String;Ljava/lang/String;Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 221
    .end local v19    # "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .local v2, "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :try_start_1
    const-string v9, "__MARKER"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 255
    .end local v2    # "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :cond_2
    :goto_0
    return-object v2

    .line 225
    .restart local v2    # "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :cond_3
    const-string v9, "data"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 227
    .local v12, "data":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-static {v0, v5, v6, v7}, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->getUserTableFields(Landroid/content/Context;IILjava/lang/String;)Ljava/util/TreeSet;

    move-result-object v16

    .line 229
    .local v16, "fieldNames":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    if-eqz v12, :cond_4

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_5

    .line 230
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 232
    :cond_5
    const-string v9, "\\|"

    invoke-virtual {v12, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    .line 233
    .local v17, "fieldValues":[Ljava/lang/String;
    if-eqz v16, :cond_2

    .line 234
    move-object/from16 v0, v17

    array-length v9, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/TreeSet;->size()I

    move-result v10

    if-ne v9, v10, :cond_6

    .line 236
    const/4 v14, 0x0

    .line 237
    .local v14, "fieldCount":I
    invoke-virtual/range {v16 .. v16}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 238
    .local v15, "fieldName":Ljava/lang/String;
    aget-object v9, v17, v14

    invoke-virtual {v2, v15, v9}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 239
    add-int/lit8 v14, v14, 0x1

    .line 240
    goto :goto_1

    .line 243
    .end local v14    # "fieldCount":I
    .end local v15    # "fieldName":Ljava/lang/String;
    .end local v18    # "i$":Ljava/util/Iterator;
    :cond_6
    sget-object v9, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Number of field values does not match columns[fields:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v16 .. v16}, Ljava/util/TreeSet;->size()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v22, ",values:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v22, "]: Data is:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Number of field values does not match columns[fields:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {v16 .. v16}, Ljava/util/TreeSet;->size()I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v22, ",values:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v22, "]: Data is:"

    move-object/from16 v0, v22

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 250
    .end local v12    # "data":Ljava/lang/String;
    .end local v16    # "fieldNames":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/String;>;"
    .end local v17    # "fieldValues":[Ljava/lang/String;
    :catch_0
    move-exception v13

    .line 252
    .end local v3    # "seqId":J
    .end local v5    # "codeBaseId":I
    .end local v6    # "dbModelVersion":I
    .end local v7    # "tableName":Ljava/lang/String;
    .end local v8    # "key":Ljava/lang/String;
    .end local v11    # "actionCode":I
    .end local v20    # "transactionTimestamp":J
    .local v13, "e":Ljava/lang/Exception;
    :goto_2
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v13}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 250
    .end local v2    # "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v19    # "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :catch_1
    move-exception v13

    move-object/from16 v2, v19

    .end local v19    # "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .restart local v2    # "returnObject":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    goto :goto_2
.end method

.method public static parse(Landroid/content/Context;Lorg/json/JSONArray;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/List;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "journalEntries"    # Lorg/json/JSONArray;
    .param p2, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lorg/json/JSONArray;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    .line 72
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Parsing journal entries"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 77
    .local v1, "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 78
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    int-to-double v3, v0

    mul-double/2addr v3, v7

    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v5

    int-to-double v5, v5

    div-double/2addr v3, v5

    invoke-interface {p2, v2, v3, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 79
    if-eqz v1, :cond_1

    .line 80
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->parse(Landroid/content/Context;Lorg/json/JSONObject;)Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_2
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-interface {p2, v2, v7, v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 85
    return-object v1
.end method

.method public static sort(Ljava/util/List;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 116
    .local p0, "journalEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Sorting journal entries by timestamp"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->sortByDate:Ljava/util/Comparator;

    invoke-static {p0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 120
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v2, 0x3fe999999999999aL    # 0.8

    invoke-interface {p1, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 122
    invoke-static {p0}, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->findMarker(Ljava/util/List;)I

    move-result v0

    .local v0, "lastMarker":I
    :goto_0
    if-ltz v0, :cond_1

    .line 123
    const/4 v1, 0x0

    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 122
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 125
    :cond_1
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-interface {p1, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 126
    return-void
.end method
