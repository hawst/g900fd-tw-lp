.class public Lcom/cigna/coach/utils/SharedPrefUtils;
.super Ljava/lang/Object;
.source "SharedPrefUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/SharedPrefUtils$1;,
        Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final COACH_STARTED_SETTING:Ljava/lang/String; = "COACH_STARTED"

.field private static final SAMSUNG_ACCOUNT_NAME:Ljava/lang/String; = "SAMSUNG_ACCOUNT_NAME"

.field private static final SHEALTH_STARTED_SETTING:Ljava/lang/String; = "SHEALTH_STARTED"

.field private static final WIDGET_SCORE_NO_GOAL_DISPLAY_SETTING:Ljava/lang/String; = "WIDGET_SCORE_NO_GOAL_DISPLAY_SETTING"

.field private static final WIDGET_SCORE_NO_MISSION_DISPLAY_SETTING:Ljava/lang/String; = "WIDGET_SCORE_NO_MISSION_DISPLAY_SETTING"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/cigna/coach/utils/SharedPrefUtils;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static getCoachSettings(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .prologue
    const/4 v4, 0x0

    .line 126
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getCoachSettings : START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    const/4 v0, 0x0

    .line 132
    .local v0, "isSet":Z
    const-string/jumbo v2, "settings"

    invoke-virtual {p0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 133
    .local v1, "prefs":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_1

    .line 134
    invoke-static {p1}, Lcom/cigna/coach/utils/SharedPrefUtils;->getSettingStringName(Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 137
    :cond_1
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 138
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getCoachSettings : END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_2
    return v0
.end method

.method public static getCoachSettingsString(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "type"    # Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .prologue
    .line 150
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 151
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getCoachSettingsString : START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_0
    const-string v1, ""

    .line 156
    .local v1, "settingsVal":Ljava/lang/String;
    const-string/jumbo v2, "settings"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 157
    .local v0, "prefs":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {p1}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->name()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 161
    :cond_1
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 162
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getCoachSettingsString : END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_2
    return-object v1
.end method

.method private static getSettingStringName(Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Ljava/lang/String;
    .locals 3
    .param p0, "type"    # Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .prologue
    .line 169
    const-string v0, ""

    .line 171
    .local v0, "settingName":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/utils/SharedPrefUtils$1;->$SwitchMap$com$cigna$coach$utils$SharedPrefUtils$CoachSettingsType:[I

    invoke-virtual {p0}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 189
    :goto_0
    return-object v0

    .line 173
    :pswitch_0
    const-string v0, "COACH_STARTED"

    .line 174
    goto :goto_0

    .line 176
    :pswitch_1
    const-string v0, "SHEALTH_STARTED"

    .line 177
    goto :goto_0

    .line 179
    :pswitch_2
    const-string v0, "WIDGET_SCORE_NO_GOAL_DISPLAY_SETTING"

    .line 180
    goto :goto_0

    .line 182
    :pswitch_3
    const-string v0, "WIDGET_SCORE_NO_MISSION_DISPLAY_SETTING"

    .line 183
    goto :goto_0

    .line 185
    :pswitch_4
    const-string v0, "SAMSUNG_ACCOUNT_NAME"

    goto :goto_0

    .line 171
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static setCoachSetttings(Landroid/content/Context;ZLcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "isStarted"    # Z
    .param p2, "type"    # Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .prologue
    .line 77
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setCoachSetttings : START, isStarted = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    const-string/jumbo v2, "settings"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 82
    .local v1, "prefs":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_1

    .line 83
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 84
    .local v0, "editSharedPrefs":Landroid/content/SharedPreferences$Editor;
    invoke-static {p2}, Lcom/cigna/coach/utils/SharedPrefUtils;->getSettingStringName(Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 85
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 88
    .end local v0    # "editSharedPrefs":Landroid/content/SharedPreferences$Editor;
    :cond_1
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "setCoachSetttings : END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_2
    return-void
.end method

.method public static setCoachSetttingsString(Landroid/content/Context;Ljava/lang/String;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "settingsVal"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .prologue
    .line 101
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "setCoachSetttingsString : START, settings : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    const-string/jumbo v2, "settings"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 106
    .local v1, "prefs":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_1

    .line 107
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 108
    .local v0, "editSharedPrefs":Landroid/content/SharedPreferences$Editor;
    invoke-virtual {p2}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 109
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 112
    .end local v0    # "editSharedPrefs":Landroid/content/SharedPreferences$Editor;
    :cond_1
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 113
    sget-object v2, Lcom/cigna/coach/utils/SharedPrefUtils;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "setCoachSetttingsString : END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_2
    return-void
.end method
