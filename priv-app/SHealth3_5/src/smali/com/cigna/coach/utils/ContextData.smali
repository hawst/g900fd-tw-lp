.class public Lcom/cigna/coach/utils/ContextData;
.super Ljava/lang/Object;
.source "ContextData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/ContextData$ContextDataType;
    }
.end annotation


# instance fields
.field private contentIdsToResolve:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContextData$ContextDataType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private resolvedContent:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContextData$ContextDataType;",
            "Lcom/cigna/coach/dataobjects/ContentInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/utils/ContextData;->contentIdsToResolve:Ljava/util/Map;

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/utils/ContextData;->resolvedContent:Ljava/util/Map;

    .line 26
    return-void
.end method


# virtual methods
.method public addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V
    .locals 2
    .param p1, "dataType"    # Lcom/cigna/coach/utils/ContextData$ContextDataType;
    .param p2, "contentId"    # I

    .prologue
    .line 31
    iget-object v0, p0, Lcom/cigna/coach/utils/ContextData;->contentIdsToResolve:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    return-void
.end method

.method public addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;Ljava/util/Collection;)V
    .locals 1
    .param p1, "dataType"    # Lcom/cigna/coach/utils/ContextData$ContextDataType;
    .param p2, "contentIds"    # Ljava/util/Collection;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/cigna/coach/utils/ContextData;->contentIdsToResolve:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public getContentIdsToResolve()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContextData$ContextDataType;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/cigna/coach/utils/ContextData;->contentIdsToResolve:Ljava/util/Map;

    return-object v0
.end method

.method public getResolvedContent(Lcom/cigna/coach/utils/ContextData$ContextDataType;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;
    .locals 3
    .param p1, "dataType"    # Lcom/cigna/coach/utils/ContextData$ContextDataType;
    .param p2, "locale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .prologue
    .line 44
    iget-object v2, p0, Lcom/cigna/coach/utils/ContextData;->resolvedContent:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 45
    .local v0, "c":Lcom/cigna/coach/dataobjects/ContentInfo;
    const/4 v1, 0x0

    .line 46
    .local v1, "returnValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0, p2}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentForLocale(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v1

    .line 49
    :cond_0
    return-object v1
.end method

.method public getResolvedContents()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContextData$ContextDataType;",
            "Lcom/cigna/coach/dataobjects/ContentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/cigna/coach/utils/ContextData;->resolvedContent:Ljava/util/Map;

    return-object v0
.end method
