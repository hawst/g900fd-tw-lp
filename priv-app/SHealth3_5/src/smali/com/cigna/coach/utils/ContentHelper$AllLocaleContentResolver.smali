.class Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;
.super Ljava/lang/Object;
.source "ContentHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/ContentHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AllLocaleContentResolver"
.end annotation


# instance fields
.field contentId:I

.field contextData:Lcom/cigna/coach/utils/ContextData;

.field resolvedContent:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(ILcom/cigna/coach/utils/ContextData;)V
    .locals 0
    .param p1, "cId"    # I
    .param p2, "contextData"    # Lcom/cigna/coach/utils/ContextData;

    .prologue
    .line 436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 437
    iput-object p2, p0, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;->contextData:Lcom/cigna/coach/utils/ContextData;

    .line 438
    iput p1, p0, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;->contentId:I

    .line 439
    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachMultiLanguageContentId;
        setTextMethod = "setResolvedContent"
    .end annotation

    .prologue
    .line 443
    iget v0, p0, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;->contentId:I

    return v0
.end method

.method public getContextData()Lcom/cigna/coach/utils/ContextData;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;->contextData:Lcom/cigna/coach/utils/ContextData;

    return-object v0
.end method

.method public getResolvedContent()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 447
    iget-object v0, p0, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;->resolvedContent:Ljava/util/Map;

    return-object v0
.end method

.method public setResolvedContent(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 451
    .local p1, "resolvedContent":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;->resolvedContent:Ljava/util/Map;

    .line 452
    return-void
.end method
