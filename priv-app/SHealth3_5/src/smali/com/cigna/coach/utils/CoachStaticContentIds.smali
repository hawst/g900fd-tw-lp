.class public Lcom/cigna/coach/utils/CoachStaticContentIds;
.super Ljava/lang/Object;
.source "CoachStaticContentIds.java"


# static fields
.field public static final ABOUT_CIGNA:I = 0xf8a

.field public static final ABOUT_CIGNA_URL:I = 0x1325

.field public static final BACKGROUND_IMAGE_1:I = 0x118c

.field public static final BACKGROUND_IMAGE_2:I = 0x118d

.field public static final BACKGROUND_IMAGE_3:I = 0x118e

.field public static final BACKGROUND_IMAGE_4:I = 0x118f

.field public static final BACKGROUND_IMAGE_5:I = 0x1190

.field public static final BADGE_BACKGROUND_IMAGE:I = 0x125e

.field public static final COACHES_CORNER_MSG_EXISTING_USER:I = 0xe96

.field public static final COACHES_CORNER_MSG_NEW_USER:I = 0x3e9

.field public static final COACH_NOT_STARTED:I = 0x19c8

.field public static final COMPLETED_GOALS:I = 0xf2e

.field public static final COMPLETED_MISSIONS:I = 0xf2f

.field public static final CURRENT_SCORE_MESSAGE:I = 0xf75

.field public static final FORGOT:I = 0xf01

.field public static final GET_YOUR_LIFESTYLE_SCORE:I = 0x139e

.field public static final GOAL_STATUS_COMPLETED:I = 0xf5c

.field public static final GOAL_STATUS_IN_PROGRESS:I = 0xf30

.field public static final GOAL_STATUS_USER_CANCELLED:I = 0x1015

.field public static final LAST_UPDATE_MESSAGE:I = 0x1016

.field public static final LA_SCORE:I = 0x137d

.field public static final MISSION_FAILED_COACH_MSG:I = 0xf67

.field public static final MISSION_STATUS_CANCELLED:I = 0x1015

.field public static final MISSION_STATUS_COMPLETED:I = 0xf5c

.field public static final MISSION_STATUS_FAILED:I = 0x1191

.field public static final MISSION_STATUS_IN_PROGRESS:I = 0xf30

.field public static final NONE_OF_THESE:I = 0xf03

.field public static final NO_GOALS:I = 0x13c6

.field public static final NO_LA_NO_SCORE_DESC:I = 0xec8

.field public static final NO_LA_NO_SCORE_TITLE:I = 0xf0f

.field public static final NO_MISSIONS:I = 0x13c7

.field public static final OUT_OF_MESSAGE:I = 0xf6f

.field public static final SCORE_VARIANCE_MESSAGE:I = 0xf72

.field public static final SET_GOAL_INSTRUCTIONAL_MESSAGE:I = 0x43e

.field public static final TIP_OF_THE_DAY:I = 0x3e8

.field public static final TOO_BUSY_CAN_CREATE_NEW_MISSION:I = 0xf00

.field public static final TOO_BUSY_CAN_NOT_CREATE_NEW_MISSION:I = 0xf03

.field public static final TOO_EASY:I = 0xf03

.field public static final TOO_HARD:I = 0xf02

.field public static final TRACKER_INFO:I = 0x10c6

.field public static final YOUR_TRACKER_SAYS_LABEL:I = 0xf19


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
