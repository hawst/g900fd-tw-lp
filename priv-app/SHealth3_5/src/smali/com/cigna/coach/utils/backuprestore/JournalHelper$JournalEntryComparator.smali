.class Lcom/cigna/coach/utils/backuprestore/JournalHelper$JournalEntryComparator;
.super Ljava/lang/Object;
.source "JournalHelper.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "JournalEntryComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/dataobjects/JournalEntry;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/dataobjects/JournalEntry;Lcom/cigna/coach/dataobjects/JournalEntry;)I
    .locals 4
    .param p1, "lhs"    # Lcom/cigna/coach/dataobjects/JournalEntry;
    .param p2, "rhs"    # Lcom/cigna/coach/dataobjects/JournalEntry;

    .prologue
    .line 446
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getActionTime()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/JournalEntry;->getActionTime()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v1

    .line 447
    .local v1, "timeCompare":I
    if-nez v1, :cond_0

    .line 448
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getSeqId()I

    move-result v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/JournalEntry;->getSeqId()I

    move-result v3

    sub-int v0, v2, v3

    .line 451
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 442
    check-cast p1, Lcom/cigna/coach/dataobjects/JournalEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/dataobjects/JournalEntry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$JournalEntryComparator;->compare(Lcom/cigna/coach/dataobjects/JournalEntry;Lcom/cigna/coach/dataobjects/JournalEntry;)I

    move-result v0

    return v0
.end method
