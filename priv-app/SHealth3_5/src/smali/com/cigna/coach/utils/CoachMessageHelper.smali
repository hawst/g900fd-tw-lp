.class public Lcom/cigna/coach/utils/CoachMessageHelper;
.super Ljava/lang/Object;
.source "CoachMessageHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/CoachMessageHelper$1;,
        Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field public static final MISSION_EXPIRY_GRACE_PERIOD:I = 0x1

.field private static final NOTIFICATION_TYPES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;


# instance fields
.field private dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

.field private dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 58
    const-class v0, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    const/4 v1, 0x0

    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->OUT_OF_APPLICATION_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->NOTIFICATION_TYPES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-instance v0, Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-direct {v0, p1}, Lcom/cigna/coach/db/CoachMessageDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    .line 77
    iput-object p1, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 78
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private addNotificationsForMissionToResponse(Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V
    .locals 7
    .param p1, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .param p3, "messageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p4, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1129
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_MISSION_DATA:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1130
    .local v2, "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, p3, p4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .line 1131
    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getUserId()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 1134
    .local v6, "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    if-eqz v6, :cond_0

    .line 1135
    invoke-static {p1}, Lcom/cigna/coach/utils/CoachMessageHelper;->createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v6, v0}, Lcom/cigna/coach/utils/CoachMessageHelper;->getIntentFromCoachMessage(Lcom/cigna/coach/dataobjects/CoachMessageData;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setNotificationDeepLinkInfo(Ljava/util/Map;)V

    .line 1137
    :cond_0
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1138
    return-void
.end method

.method private checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 370
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .local p5, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    const/4 v6, -0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/Map;)V

    .line 371
    return-void
.end method

.method private checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/Map;)V
    .locals 37
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p6, "missionContentId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 376
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .local p5, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p7, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p4, :cond_8

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    .line 379
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 380
    .local v8, "outOfApplicationNotificationCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 381
    .local v14, "timelineCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 382
    .local v20, "coachWidgetCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 384
    .local v26, "shealthWidgetCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v30

    .local v30, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v30 .. v30}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 385
    .local v27, "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual/range {v27 .. v27}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->OUT_OF_APPLICATION_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v3, v4, :cond_1

    .line 387
    move-object/from16 v0, v27

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    :cond_1
    invoke-virtual/range {v27 .. v27}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v3, v4, :cond_2

    .line 391
    move-object/from16 v0, v27

    invoke-interface {v14, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_2
    invoke-virtual/range {v27 .. v27}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v3, v4, :cond_3

    .line 394
    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    :cond_3
    invoke-virtual/range {v27 .. v27}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    if-ne v3, v4, :cond_0

    .line 397
    invoke-interface/range {v26 .. v27}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 402
    .end local v27    # "cm":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_4
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 403
    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->OUT_OF_APPLICATION_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v7, p3

    invoke-virtual/range {v3 .. v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v31

    .local v31, "outOfApplicationNotificationCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v32, v31

    .line 405
    check-cast v32, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 406
    .local v32, "outOfApplicationNotificationCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    move-object/from16 v0, v32

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMissionContentId(I)V

    .line 408
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->getIntentFromCoachMessage(Lcom/cigna/coach/dataobjects/CoachMessageData;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    move-object/from16 v0, v32

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setNotificationDeepLinkInfo(Ljava/util/Map;)V

    .line 409
    invoke-virtual/range {p5 .. p5}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v3, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 414
    .end local v31    # "outOfApplicationNotificationCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v32    # "outOfApplicationNotificationCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_5
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 415
    sget-object v12, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v13, p3

    invoke-virtual/range {v9 .. v14}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v35

    .local v35, "timelineCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v36, v35

    .line 417
    check-cast v36, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 418
    .local v36, "timelineCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    move-object/from16 v0, v36

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMissionContentId(I)V

    .line 420
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->getIntentFromCoachMessage(Lcom/cigna/coach/dataobjects/CoachMessageData;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    move-object/from16 v0, v36

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setNotificationDeepLinkInfo(Ljava/util/Map;)V

    .line 421
    invoke-virtual/range {p5 .. p5}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    invoke-virtual {v0, v3, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 425
    .end local v35    # "timelineCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v36    # "timelineCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_6
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_7

    .line 426
    sget-object v18, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v17, p2

    move-object/from16 v19, p3

    invoke-virtual/range {v15 .. v20}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v28

    .local v28, "coachWidetCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v29, v28

    .line 427
    check-cast v29, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 428
    .local v29, "coachWidgetCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    move-object/from16 v0, v29

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMissionContentId(I)V

    .line 429
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->getIntentFromCoachMessage(Lcom/cigna/coach/dataobjects/CoachMessageData;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setNotificationDeepLinkInfo(Ljava/util/Map;)V

    .line 430
    invoke-virtual/range {p5 .. p5}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 434
    .end local v28    # "coachWidetCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v29    # "coachWidgetCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_7
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_8

    .line 435
    sget-object v24, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-object/from16 v21, p0

    move-object/from16 v22, p1

    move-object/from16 v23, p2

    move-object/from16 v25, p3

    invoke-virtual/range {v21 .. v26}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v33

    .local v33, "shealthWidetCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v34, v33

    .line 436
    check-cast v34, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 437
    .local v34, "shealthwidgetCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    move-object/from16 v0, v34

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMissionContentId(I)V

    .line 438
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->getIntentFromCoachMessage(Lcom/cigna/coach/dataobjects/CoachMessageData;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v3

    move-object/from16 v0, v34

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setNotificationDeepLinkInfo(Ljava/util/Map;)V

    .line 439
    invoke-virtual/range {p5 .. p5}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v3, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 443
    .end local v8    # "outOfApplicationNotificationCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v14    # "timelineCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "coachWidgetCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v26    # "shealthWidgetCoachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v30    # "i$":Ljava/util/Iterator;
    .end local v33    # "shealthWidetCoachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v34    # "shealthwidgetCoachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_8
    return-void
.end method

.method private createContextMapFromList(Ljava/util/List;)Ljava/util/Map;
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "allCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 202
    .local v1, "coachMessagesContextBasedMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    .local v0, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object v7, v0

    .line 204
    check-cast v7, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v2

    .line 205
    .local v2, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    if-eqz v2, :cond_0

    move-object v7, v0

    .line 207
    check-cast v7, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMinValue()I

    move-result v4

    .line 208
    .local v4, "minValue":I
    const/4 v6, 0x0

    .line 210
    .local v6, "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 212
    .local v5, "perContextCoachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    if-nez v5, :cond_1

    .line 213
    new-instance v5, Ljava/util/HashMap;

    .end local v5    # "perContextCoachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 215
    .restart local v5    # "perContextCoachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    :cond_1
    const/4 v7, -0x1

    if-eq v4, v7, :cond_3

    .line 216
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    check-cast v6, Ljava/util/List;

    .line 217
    .restart local v6    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-nez v6, :cond_2

    .line 218
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 220
    .restart local v6    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_2
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_3
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    invoke-interface {v1, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 225
    .end local v0    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v2    # "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v4    # "minValue":I
    .end local v5    # "perContextCoachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v6    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_4
    return-object v1
.end method

.method public static createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserGoalData;)Ljava/util/Map;
    .locals 3
    .param p0, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/UserGoalData;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 669
    if-nez p0, :cond_0

    .line 670
    const/4 v0, 0x0

    .line 679
    :goto_0
    return-object v0

    .line 675
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 676
    .local v0, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 677
    const-string v1, "EXTRA_NAME_GOAL_SEQUENCE_ID"

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalSequenceId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/Map;
    .locals 3
    .param p0, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 659
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 660
    .local v0, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "EXTRA_NAME_MISSION_ID"

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 661
    const-string v1, "EXTRA_NAME_MISSION_SEQUENCE_ID"

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 662
    const-string v1, "EXTRA_NAME_GOAL_ID"

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 663
    const-string v1, "EXTRA_NAME_GOAL_SEQUENCE_ID"

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalSequenceId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 665
    return-object v0
.end method

.method private getIntentFromCoachMessage(Lcom/cigna/coach/dataobjects/CoachMessageData;Ljava/util/Map;)Ljava/util/Map;
    .locals 4
    .param p1, "coachMessageData"    # Lcom/cigna/coach/dataobjects/CoachMessageData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 446
    .local p2, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez p1, :cond_0

    .line 447
    const/4 v1, 0x0

    .line 461
    :goto_0
    return-object v1

    .line 448
    :cond_0
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentAction()Ljava/lang/String;

    move-result-object v0

    .line 450
    .local v0, "intentAction":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 453
    if-nez p2, :cond_1

    .line 454
    new-instance p2, Ljava/util/HashMap;

    .end local p2    # "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p2}, Ljava/util/HashMap;-><init>()V

    .line 456
    .restart local p2    # "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_1
    const-string v1, "EXTRA_NAME_DESTINATION"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    sget-object v1, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 458
    sget-object v1, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deepLinkInfo getIntentAction"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v1, p2

    .line 461
    goto :goto_0
.end method

.method private getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p3, "minValue"    # I
    .param p4, "checkUserProgressContextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p5, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p6, "today"    # Ljava/util/Calendar;

    .prologue
    .line 344
    iget-object v1, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object v2, p1

    move-object v3, p4

    move-object v4, p2

    move-object/from16 v5, p5

    move v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/cigna/coach/db/CoachMessageDBManager;->getUserCoachMessageLastDeliveredTime(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)J

    move-result-wide v7

    .line 347
    .local v7, "deliveredTime":J
    const/4 v10, 0x0

    .line 348
    .local v10, "lastMsgDeliveredTime":Ljava/util/Calendar;
    const/4 v9, 0x0

    .line 349
    .local v9, "lastDeliveredMsgDaysBetween":I
    const-wide/16 v1, 0x0

    cmp-long v1, v7, v1

    if-eqz v1, :cond_0

    .line 350
    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v10

    .line 351
    move-object/from16 v0, p6

    invoke-static {v10, v0}, Lcom/cigna/coach/utils/CalendarUtil;->absDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v9

    .line 353
    :cond_0
    return v9
.end method

.method private getSingleContextCoachMessageMap(Ljava/util/List;)Ljava/util/Map;
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "singleContextCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 232
    .local v3, "perContextCoachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    .local v0, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object v5, v0

    .line 233
    check-cast v5, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMinValue()I

    move-result v2

    .line 234
    .local v2, "minValue":I
    const/4 v4, 0x0

    .line 236
    .local v4, "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const/4 v5, -0x1

    if-eq v2, v5, :cond_1

    .line 237
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    check-cast v4, Ljava/util/List;

    .line 238
    .restart local v4    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-nez v4, :cond_0

    .line 239
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 241
    .restart local v4    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_0
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 246
    .end local v0    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v2    # "minValue":I
    .end local v4    # "perMinvalueContextBasedList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_2
    return-object v3
.end method

.method private getTimelineBadgeMessages(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1227
    .local p3, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1228
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz p3, :cond_2

    .line 1229
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/Badge;

    .line 1230
    .local v6, "badge":Lcom/cigna/coach/apiobjects/Badge;
    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1231
    .local v2, "timelineCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v3, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1232
    .local v3, "badgeContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    .line 1235
    .local v4, "badgeReferenceType":Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v5

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;I)Ljava/util/List;

    move-result-object v9

    .line 1239
    .local v9, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v9, :cond_0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1240
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1241
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1243
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "More than one message found for badge: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", contextType: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1250
    .end local v2    # "timelineCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v3    # "badgeContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v4    # "badgeReferenceType":Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;
    .end local v6    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_2
    return-object v7
.end method


# virtual methods
.method protected addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V
    .locals 4
    .param p2, "coachMessage"    # Lcom/cigna/coach/apiobjects/CoachMessage;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1448
    .local p1, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz p1, :cond_1

    .line 1449
    if-eqz p2, :cond_1

    .line 1450
    sget-object v1, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1451
    instance-of v1, p2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    if-eqz v1, :cond_0

    move-object v0, p2

    .line 1452
    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 1453
    .local v0, "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v1, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adding coach message to list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1456
    .end local v0    # "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_0
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1459
    :cond_1
    return-void
.end method

.method protected addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V
    .locals 3
    .param p2, "coachMessageData"    # Lcom/cigna/coach/dataobjects/CoachMessageData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/CoachMessageData;",
            ">;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1462
    .local p1, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/CoachMessageData;>;"
    if-eqz p1, :cond_1

    .line 1463
    if-eqz p2, :cond_1

    .line 1464
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1465
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding coach message to list: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1467
    :cond_0
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1470
    :cond_1
    return-void
.end method

.method public getCatchAllMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "messageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .prologue
    .line 807
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 808
    .local v2, "getUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    move-object v3, p2

    .line 809
    .local v3, "widgetNotificationCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CATCH_ALL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 810
    .local v4, "catchAllContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 811
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    return-object v0
.end method

.method public getCategorySpecificCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 26
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 1302
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1303
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getCategorySpecificCoachMessage : START, categoryType ="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1305
    :cond_0
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_QUESTION_ANSWER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1306
    .local v5, "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CATEGORY_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1307
    .local v6, "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v7, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CATEGORY_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1309
    .local v7, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    const/4 v8, 0x0

    .line 1310
    .local v8, "categoryScore":I
    sget-object v4, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1312
    new-instance v21, Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1313
    .local v21, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    const/16 v4, 0xc

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v9, v10}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserResponseForQuestionGroup(Ljava/lang/String;ILjava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v23

    .line 1314
    .local v23, "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    const-wide/16 v24, 0x0

    .local v24, "weight":D
    const-wide/16 v18, 0x0

    .line 1315
    .local v18, "height":D
    if-eqz v23, :cond_3

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1316
    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    .line 1317
    .local v22, "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1319
    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getQuestionId()I

    move-result v4

    const/16 v9, 0xc

    if-ne v4, v9, :cond_2

    .line 1320
    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v18

    goto :goto_0

    .line 1321
    :cond_2
    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getQuestionId()I

    move-result v4

    const/16 v9, 0xd

    if-ne v4, v9, :cond_1

    .line 1322
    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v24

    goto :goto_0

    .line 1326
    .end local v20    # "i$":Ljava/util/Iterator;
    .end local v22    # "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    :cond_3
    move-wide/from16 v0, v18

    move-wide/from16 v2, v24

    invoke-static {v0, v1, v2, v3}, Lcom/cigna/coach/utils/ScoringHelper;->calculateBMI(DD)D

    move-result-wide v15

    .line 1327
    .local v15, "bmi":D
    const-wide/high16 v9, 0x4059000000000000L    # 100.0

    mul-double/2addr v9, v15

    invoke-static {v9, v10}, Ljava/lang/Math;->round(D)J

    move-result-wide v9

    long-to-int v8, v9

    .line 1328
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1329
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "WEIGHT, bmi: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-wide v0, v15

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    .end local v15    # "bmi":D
    .end local v18    # "height":D
    .end local v21    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .end local v23    # "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    .end local v24    # "weight":D
    :cond_4
    :goto_1
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1336
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "WEIGHT, category score for coach messages: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1339
    :cond_5
    const/4 v14, 0x0

    .line 1340
    .local v14, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const/4 v4, -0x1

    if-eq v8, v4, :cond_6

    .line 1341
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v9

    invoke-virtual/range {v4 .. v9}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValuesAndRefId(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;II)Ljava/util/List;

    move-result-object v14

    :cond_6
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v5

    move-object v12, v6

    move-object v13, v7

    .line 1344
    invoke-virtual/range {v9 .. v14}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v17

    .line 1346
    .local v17, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1347
    sget-object v4, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getCategorySpecificCoachMessage : END"

    invoke-static {v4, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1349
    :cond_7
    return-object v17

    .line 1333
    .end local v14    # "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v17    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Lcom/cigna/coach/db/CoachMessageDBManager;->getLatestCategoryScore(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I

    move-result v8

    goto :goto_1
.end method

.method protected getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p3, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p4, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;)",
            "Lcom/cigna/coach/apiobjects/CoachMessage;"
        }
    .end annotation

    .prologue
    .line 1485
    .local p5, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const/4 v13, 0x0

    .line 1487
    .local v13, "returnCoachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    if-eqz p5, :cond_2

    .line 1488
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v11

    .line 1489
    .local v11, "coachMsgListSize":I
    if-lez v11, :cond_2

    .line 1490
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMinValue()I

    move-result v6

    .line 1491
    .local v6, "minValue":I
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v7

    .line 1492
    .local v7, "maxValue":I
    const/4 v1, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMinMaxRefValue()I

    move-result v8

    .line 1493
    .local v8, "minMaxRefValue":I
    iget-object v1, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v8}, Lcom/cigna/coach/db/CoachMessageDBManager;->getUserCoachMessageActivityCount(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;III)I

    move-result v9

    .line 1494
    .local v9, "activityCount":I
    rem-int v12, v9, v11

    .line 1495
    .local v12, "messageIndex":I
    sget-object v1, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1496
    sget-object v1, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCoachMessageFromList total messages = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " selected coach message index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    :cond_0
    move-object/from16 v0, p5

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 1499
    .local v10, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    if-eqz v10, :cond_2

    .line 1501
    sget-object v1, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1502
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "coachMessageID = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object v1, v10

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1505
    :cond_1
    new-instance v13, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local v13    # "returnCoachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-direct {v13}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V

    .line 1506
    .restart local v13    # "returnCoachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-virtual {v13, v10}, Lcom/cigna/coach/dataobjects/CoachMessageData;->copy(Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1510
    .end local v6    # "minValue":I
    .end local v7    # "maxValue":I
    .end local v8    # "minMaxRefValue":I
    .end local v9    # "activityCount":I
    .end local v10    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v11    # "coachMsgListSize":I
    .end local v12    # "messageIndex":I
    :cond_2
    return-object v13
.end method

.method public getNoGoalsEnrolledMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "messageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .prologue
    .line 827
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 828
    .local v2, "getUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    move-object v3, p2

    .line 829
    .local v3, "widgetNotificationCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ZERO_ACTIVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 830
    .local v4, "noGoalContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 831
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    return-object v0
.end method

.method public getNoMissionsEnrolledMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "messageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .prologue
    .line 817
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 818
    .local v2, "getUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    move-object v3, p2

    .line 819
    .local v3, "widgetNotificationCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ZERO_ACTIVE_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 820
    .local v4, "noMissionContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 821
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    return-object v0
.end method

.method public getReassesCoachMessage(Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 749
    sget-object v8, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 750
    sget-object v8, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getReassesCoachMessage : START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 754
    .local v6, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v3, Lcom/cigna/coach/db/LifeStyleDBManager;

    iget-object v8, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v3, v8}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 755
    .local v3, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    new-instance v1, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v8, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v1, v8}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 757
    .local v1, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    const/4 v8, 0x0

    invoke-virtual {v3, p1, v8}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserOverallScore(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/ScoresData;

    move-result-object v5

    .line 758
    .local v5, "overAllScore":Lcom/cigna/coach/dataobjects/ScoresData;
    if-eqz v5, :cond_3

    .line 759
    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/ScoresData;->getAssesmentTimestamp()Ljava/util/Calendar;

    move-result-object v2

    .line 760
    .local v2, "lastReassesDate":Ljava/util/Calendar;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    .line 761
    .local v7, "today":Ljava/util/Calendar;
    const/4 v0, 0x0

    .line 763
    .local v0, "daysFromLastReasses":F
    if-eqz v2, :cond_1

    .line 765
    invoke-static {v2, v7}, Lcom/cigna/coach/utils/CalendarUtil;->monthsAndDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)F

    move-result v0

    .line 767
    :cond_1
    sget-object v8, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 768
    sget-object v8, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "daysFromLastReasses : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    :cond_2
    const/high16 v8, 0x42c80000    # 100.0f

    mul-float/2addr v8, v0

    float-to-int v8, v8

    invoke-virtual {p0, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getReassesWarningMessage(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v8

    invoke-virtual {p0, v6, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 772
    invoke-virtual {v1, p1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalsCount(Ljava/lang/String;)I

    move-result v4

    .line 773
    .local v4, "numActiveGoals":I
    if-lez v4, :cond_3

    .line 774
    invoke-virtual {p0, p1}, Lcom/cigna/coach/utils/CoachMessageHelper;->getReassesWithGoalsWarningMessage(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v8

    invoke-virtual {p0, v6, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 777
    .end local v0    # "daysFromLastReasses":F
    .end local v2    # "lastReassesDate":Ljava/util/Calendar;
    .end local v4    # "numActiveGoals":I
    .end local v7    # "today":Ljava/util/Calendar;
    :cond_3
    sget-object v8, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 778
    sget-object v8, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getReassesCoachMessage : END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    :cond_4
    return-object v6
.end method

.method public getReassesWarningMessage(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "daysFromLastReasses"    # I

    .prologue
    .line 786
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 787
    .local v2, "getScoresContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 788
    .local v3, "reassessWarningMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS_WARNING:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 789
    .local v4, "reassessWarningContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4, p2}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 791
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    return-object v0
.end method

.method public getReassesWithGoalsWarningMessage(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 797
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 798
    .local v2, "getScoresContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_WITH_GOALS_WARNING:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 799
    .local v3, "reassessWithGoalsWarningMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS_WARNING:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 800
    .local v4, "reassessWarningContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 801
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    return-object v0
.end method

.method isAddMessagesToList(IIIII)Z
    .locals 5
    .param p1, "actualValue"    # I
    .param p2, "minValue"    # I
    .param p3, "maxValue"    # I
    .param p4, "repeatInterval"    # I
    .param p5, "lastDeliveredMsgDaysBetween"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 357
    if-ge p1, p2, :cond_1

    .line 364
    :cond_0
    :goto_0
    return v2

    .line 358
    :cond_1
    if-gt p1, p3, :cond_0

    .line 359
    if-nez p5, :cond_2

    move v2, v1

    goto :goto_0

    .line 360
    :cond_2
    if-lez p4, :cond_0

    .line 363
    sub-int v0, p1, p5

    .line 364
    .local v0, "lastDelivered":I
    sub-int v3, p1, p2

    div-int/2addr v3, p4

    sub-int v4, v0, p2

    div-int/2addr v4, p4

    if-le v3, v4, :cond_3

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method protected orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1407
    .local p1, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    if-eqz p1, :cond_2

    .line 1408
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v3

    .line 1409
    .local v3, "coachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v3, :cond_2

    .line 1411
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1412
    .local v2, "coachMessageDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/CoachMessageData;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 1413
    .local v0, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local v0    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1417
    :cond_0
    new-instance v7, Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;

    invoke-direct {v7}, Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;-><init>()V

    invoke-static {v2, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1420
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1421
    .local v6, "sortedCoachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 1422
    .local v1, "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1431
    .end local v1    # "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_1
    :try_start_0
    invoke-virtual {p1, v6}, Lcom/cigna/coach/apiobjects/CoachResponse;->setCoachMsgList(Ljava/util/List;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1438
    .end local v2    # "coachMessageDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/CoachMessageData;>;"
    .end local v3    # "coachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "sortedCoachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_2
    :goto_2
    return-void

    .line 1432
    .restart local v2    # "coachMessageDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/CoachMessageData;>;"
    .restart local v3    # "coachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "sortedCoachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :catch_0
    move-exception v4

    .line 1434
    .local v4, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v7, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error attempting to setCoachMsgList on CoachResponse"

    invoke-static {v7, v8, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public setCheckUserProgressMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V
    .locals 27
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 90
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    if-eqz p2, :cond_4

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getNumberOfScores(Ljava/lang/String;)I

    move-result v5

    .line 95
    .local v5, "numberOfScores":I
    sget-object v15, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 96
    .local v15, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p3

    invoke-virtual {v2, v15, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Ljava/util/List;

    move-result-object v23

    .line 98
    .local v23, "noContextFilterCoachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->createContextMapFromList(Ljava/util/List;)Ljava/util/Map;

    move-result-object v17

    .line 100
    .local v17, "coachMessagesContextBasedMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getUserCreateDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v26

    .line 102
    .local v26, "userCreateDate":Ljava/util/Calendar;
    if-nez v26, :cond_0

    .line 103
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_NOT_STARTED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v16

    .line 104
    .local v16, "coachMessagesContextBasedKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 105
    .local v7, "eachMinValue":I
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_NOT_STARTED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .local v6, "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    .line 107
    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_USER_NOT_STARTED_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_0

    .line 111
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v7    # "eachMinValue":I
    .end local v16    # "coachMessagesContextBasedKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v21    # "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface/range {v17 .. v17}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v18

    .line 112
    .local v18, "coachMessageskeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;>;"
    invoke-interface/range {v18 .. v18}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_1
    :goto_1
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Map$Entry;

    .line 113
    .local v19, "contextFilterType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/util/Map;

    .line 114
    .local v25, "perContextTypeCoachMessagesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v25 .. v25}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v24

    .line 115
    .local v24, "perContextTypeCoachMessagesKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;"
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contextFilterType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_2
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper$1;->$SwitchMap$com$cigna$coach$dataobjects$CoachMessageData$ContextFilterType:[I

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 120
    :pswitch_0
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 121
    .local v20, "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 123
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_NO_GOALS_SET_fromDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_2

    .line 129
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_1
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 130
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 132
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_ASSESSMENT_NOT_COMPLETE_fromDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_3

    .line 137
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_2
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 138
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 140
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_SIX_MONTH_LIFESTYLE_REASSESS(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_4

    .line 145
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 146
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 147
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_NO_GOAL_PROGRESS_fromDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_5

    .line 152
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_4
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 153
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 154
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_NO_MISSION_PROGRESS(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_6

    .line 159
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_5
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 160
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 161
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_NO_QUESTIONS_ANSWERED_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_7

    .line 166
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_6
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 167
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 168
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const/4 v14, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v14}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_PRE_AND_POST_MISSION_EXPIRED_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;IZ)V

    goto :goto_8

    .line 173
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_7
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_9
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 174
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 175
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_USER_NOT_STARTED_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_9

    .line 180
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    :pswitch_8
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .restart local v22    # "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/Map$Entry;

    .line 181
    .restart local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 182
    .restart local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move v11, v5

    move-object v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages_UPDATE_YOUR_WEIGHT_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V

    goto :goto_a

    .line 195
    .end local v6    # "perContextCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v18    # "coachMessageskeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;>;"
    .end local v19    # "contextFilterType":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;"
    .end local v20    # "eachMinValue":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v24    # "perContextTypeCoachMessagesKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;"
    .end local v25    # "perContextTypeCoachMessagesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 197
    .end local v5    # "numberOfScores":I
    .end local v15    # "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v17    # "coachMessagesContextBasedMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;>;"
    .end local v23    # "noContextFilterCoachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v26    # "userCreateDate":Ljava/util/Calendar;
    :cond_4
    return-void

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method setCheckUserProgressMessages_ASSESSMENT_NOT_COMPLETE_fromDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 23
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 309
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 313
    .local v5, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ASSESSMENT_NOT_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 315
    .local v6, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    sget-object v2, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/cigna/coach/db/CoachMessageDBManager;->hasScoreType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)Z

    move-result v21

    .line 317
    .local v21, "hasALifeStyleOverallScore":Z
    if-nez v21, :cond_0

    .line 318
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    sget-object v2, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/cigna/coach/db/CoachMessageDBManager;->hasScoreType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)Z

    move-result v20

    .line 322
    .local v20, "hasACategoryScore":Z
    if-eqz v20, :cond_0

    .line 323
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    .line 324
    .local v7, "today":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getLatestLifeStyleScoreDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v22

    .line 325
    .local v22, "maxScoreDate":Ljava/util/Calendar;
    const/4 v9, -0x1

    .line 326
    .local v9, "absDaysBetween":I
    if-eqz v22, :cond_2

    .line 327
    move-object/from16 v0, v22

    invoke-static {v7, v0}, Lcom/cigna/coach/utils/CalendarUtil;->absDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v9

    .line 329
    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v12

    .line 330
    .local v12, "repeatInterval":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v11

    .line 332
    .local v11, "maxValue":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v4, p5

    invoke-direct/range {v1 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v13

    .local v13, "lastDeliveredMsgDaysBetween":I
    move-object/from16 v8, p0

    move/from16 v10, p5

    .line 334
    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v18, p4

    move-object/from16 v19, p2

    .line 335
    invoke-direct/range {v14 .. v19}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    goto/16 :goto_0
.end method

.method setCheckUserProgressMessages_NO_GOALS_SET_fromDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 27
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 491
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 492
    .local v6, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v7, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOALS_SET:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 495
    .local v7, "noGoalsContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    const/4 v2, 0x1

    move/from16 v0, p3

    if-lt v0, v2, :cond_3

    if-eqz p4, :cond_3

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_3

    .line 497
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    .line 498
    .local v8, "today":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getMaxScoreDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v23

    .line 499
    .local v23, "maxScoreDate":Ljava/util/Calendar;
    const/4 v10, -0x1

    .line 500
    .local v10, "absDaysBetween":I
    if-eqz v23, :cond_0

    .line 501
    move-object/from16 v0, v23

    invoke-static {v8, v0}, Lcom/cigna/coach/utils/CalendarUtil;->absDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v10

    .line 503
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v13

    .line 504
    .local v13, "repeatInterval":I
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v12

    .line 506
    .local v12, "maxValue":I
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v4

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v5, p5

    invoke-direct/range {v2 .. v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v14

    .local v14, "lastDeliveredMsgDaysBetween":I
    move-object/from16 v9, p0

    move/from16 v11, p5

    .line 509
    invoke-virtual/range {v9 .. v14}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 510
    new-instance v21, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 512
    .local v21, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalsCount(Ljava/lang/String;)I

    move-result v25

    .line 513
    .local v25, "numberOfActiveGoals":I
    if-nez v25, :cond_3

    .line 515
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getMinScore(Ljava/lang/String;)I

    move-result v24

    .line 516
    .local v24, "minScore":I
    const/16 v22, 0x0

    .line 517
    .local v22, "hasOkOrPoorScore":Z
    const/4 v2, -0x1

    move/from16 v0, v24

    if-eq v0, v2, :cond_2

    .line 518
    invoke-static/range {v24 .. v24}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v26

    .line 519
    .local v26, "scoreRating":Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    sget-object v2, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->MEDIUM:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->LOW:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 520
    :cond_1
    const/16 v22, 0x1

    .line 523
    .end local v26    # "scoreRating":Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    :cond_2
    if-eqz v22, :cond_3

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v17, v6

    move-object/from16 v18, v7

    move-object/from16 v19, p4

    move-object/from16 v20, p2

    .line 524
    invoke-direct/range {v15 .. v20}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 530
    .end local v8    # "today":Ljava/util/Calendar;
    .end local v10    # "absDaysBetween":I
    .end local v12    # "maxValue":I
    .end local v13    # "repeatInterval":I
    .end local v14    # "lastDeliveredMsgDaysBetween":I
    .end local v21    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v22    # "hasOkOrPoorScore":Z
    .end local v23    # "maxScoreDate":Ljava/util/Calendar;
    .end local v24    # "minScore":I
    .end local v25    # "numberOfActiveGoals":I
    :cond_3
    return-void
.end method

.method setCheckUserProgressMessages_NO_GOAL_PROGRESS_fromDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 20
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 292
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 293
    .local v5, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOAL_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 294
    .local v6, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 295
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->noOfDaysPassedSinceLastMissionActivity(Ljava/lang/String;)I

    move-result v9

    .line 296
    .local v9, "absDaysBetween":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v12

    .line 297
    .local v12, "repeatInterval":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v11

    .line 299
    .local v11, "maxValue":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    .line 300
    .local v7, "today":Ljava/util/Calendar;
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v4, p5

    invoke-direct/range {v1 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v13

    .local v13, "lastDeliveredMsgDaysBetween":I
    move-object/from16 v8, p0

    move/from16 v10, p5

    .line 301
    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v18, p4

    move-object/from16 v19, p2

    .line 302
    invoke-direct/range {v14 .. v19}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 305
    .end local v7    # "today":Ljava/util/Calendar;
    .end local v9    # "absDaysBetween":I
    .end local v11    # "maxValue":I
    .end local v12    # "repeatInterval":I
    .end local v13    # "lastDeliveredMsgDaysBetween":I
    :cond_0
    return-void
.end method

.method setCheckUserProgressMessages_NO_MISSION_PROGRESS(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 18
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 252
    .local v4, "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_MISSION_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 253
    .local v5, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    new-instance v13, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v13, v2}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 254
    .local v13, "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    const/4 v15, 0x0

    .line 255
    .local v15, "msdContentId":I
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v10

    .line 256
    .local v10, "coachMessageListSize":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    if-ge v11, v10, :cond_1

    .line 257
    move-object/from16 v0, p4

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 258
    .local v16, "ss":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageContentId()I

    move-result v15

    .line 259
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Coach Message: Content ID "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageContentId()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "MIN VALUE : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " RPT : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " MSG TYPE"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 264
    .end local v16    # "ss":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_1
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-virtual {v13, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->getAllMissionsWithMissionDataNotReportedWithinTimeSpanFreqencyGracePeriod(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v14

    .line 266
    .local v14, "missionDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    if-eqz v14, :cond_8

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 267
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 268
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NO_MISSION_PROGRESS foundMissions: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    :cond_2
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 272
    .local v17, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_5

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v2

    const/4 v3, 0x3

    if-gt v2, v3, :cond_5

    const/16 v2, 0xe37

    if-eq v15, v2, :cond_4

    const/16 v2, 0x19cd

    if-ne v15, v2, :cond_5

    .line 273
    :cond_4
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 274
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "For \"You have got this\" (day 3 message), mission frequency should be between 4 and 7 "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 276
    :cond_5
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v2

    const/4 v3, 0x1

    if-lt v2, v3, :cond_7

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v2

    const/4 v3, 0x4

    if-gt v2, v3, :cond_7

    const/16 v2, 0xe38

    if-eq v15, v2, :cond_6

    const/16 v2, 0x19ce

    if-ne v15, v2, :cond_7

    .line 277
    :cond_6
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 278
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "For \"How is it going\" (day 4 message), mission frequency should be between 5 and 7 "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 281
    :cond_7
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getContentId()I

    move-result v8

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/CoachMessageHelper;->createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/Map;

    move-result-object v9

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v6, p4

    move-object/from16 v7, p2

    invoke-direct/range {v2 .. v9}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/Map;)V

    goto :goto_1

    .line 288
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v17    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_8
    return-void
.end method

.method setCheckUserProgressMessages_NO_QUESTIONS_ANSWERED_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 21
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 534
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 535
    .local v5, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_QUESTIONS_ANSWERED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 537
    .local v6, "noQuestionsAnsweredContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    if-eqz p4, :cond_1

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 538
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getUserCreateDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v20

    .line 539
    .local v20, "userCreateDate":Ljava/util/Calendar;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    .line 540
    .local v7, "today":Ljava/util/Calendar;
    const/4 v9, -0x1

    .line 541
    .local v9, "daysBetweenTodayAndUserCreateDate":I
    if-eqz v20, :cond_0

    .line 542
    move-object/from16 v0, v20

    invoke-static {v0, v7}, Lcom/cigna/coach/utils/CalendarUtil;->absDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v9

    .line 543
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v12

    .line 544
    .local v12, "repeatInterval":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v11

    .line 545
    .local v11, "maxValue":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v4, p5

    invoke-direct/range {v1 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v13

    .line 548
    .local v13, "lastDeliveredMsgDaysBetween":I
    if-nez p3, :cond_1

    move-object/from16 v8, p0

    move/from16 v10, p5

    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v18, p4

    move-object/from16 v19, p2

    .line 549
    invoke-direct/range {v14 .. v19}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 553
    .end local v7    # "today":Ljava/util/Calendar;
    .end local v9    # "daysBetweenTodayAndUserCreateDate":I
    .end local v11    # "maxValue":I
    .end local v12    # "repeatInterval":I
    .end local v13    # "lastDeliveredMsgDaysBetween":I
    .end local v20    # "userCreateDate":Ljava/util/Calendar;
    :cond_1
    return-void
.end method

.method setCheckUserProgressMessages_PRE_AND_POST_MISSION_EXPIRED_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;IZ)V
    .locals 26
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .param p6, "isCheckForPreExpiry"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 612
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v10, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 613
    .local v10, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v11, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->PRE_AND_POST_MISSION_EXPIRY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 614
    .local v11, "noQuestionsAnsweredContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    if-eqz p4, :cond_4

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 615
    new-instance v20, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 617
    .local v20, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    const/4 v2, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/GoalDBManager;->getAllActiveUserGoalsAndMissions(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v17

    .line 618
    .local v17, "activeGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v6

    .line 619
    .local v6, "repeatInterval":I
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v5

    .line 620
    .local v5, "maxValue":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v24

    .line 622
    .local v24, "now":Ljava/util/Calendar;
    const/4 v7, 0x0

    .line 625
    .local v7, "lastDeliveredMsgDaysBetween":I
    if-eqz v17, :cond_4

    .line 626
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :cond_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 627
    .local v16, "activeGoal":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v19

    .line 628
    .local v19, "activeMissions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .local v18, "activeMission":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    move-object/from16 v25, v18

    .line 629
    check-cast v25, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 630
    .local v25, "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-virtual/range {v25 .. v25}, Lcom/cigna/coach/dataobjects/UserMissionData;->getContentId()I

    move-result v14

    .line 633
    .local v14, "missionContentId":I
    invoke-virtual/range {v25 .. v25}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v23

    .line 634
    .local v23, "missionStartDate":Ljava/util/Calendar;
    const/4 v3, -0x1

    .line 635
    .local v3, "absNoOfDaysPassedAfterMissionStartDate":I
    invoke-static/range {v23 .. v24}, Lcom/cigna/coach/utils/CalendarUtil;->absDateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v3

    .line 636
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 637
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No of Days passed after mission start date:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " missionContentId : "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    :cond_2
    const/4 v2, -0x1

    if-eq v3, v2, :cond_1

    move-object/from16 v2, p0

    move/from16 v4, p5

    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 642
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 643
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Mission Content id:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    :cond_3
    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/CoachMessageHelper;->createDeepLinkInfo(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/Map;

    move-result-object v15

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v12, p4

    move-object/from16 v13, p2

    invoke-direct/range {v8 .. v15}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/Map;)V

    goto/16 :goto_0

    .line 652
    .end local v3    # "absNoOfDaysPassedAfterMissionStartDate":I
    .end local v5    # "maxValue":I
    .end local v6    # "repeatInterval":I
    .end local v7    # "lastDeliveredMsgDaysBetween":I
    .end local v14    # "missionContentId":I
    .end local v16    # "activeGoal":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v17    # "activeGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v18    # "activeMission":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .end local v19    # "activeMissions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v20    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v22    # "i$":Ljava/util/Iterator;
    .end local v23    # "missionStartDate":Ljava/util/Calendar;
    .end local v24    # "now":Ljava/util/Calendar;
    .end local v25    # "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_4
    return-void
.end method

.method setCheckUserProgressMessages_SIX_MONTH_LIFESTYLE_REASSESS(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 466
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 467
    .local v2, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->SIX_MONTH_LIFESTYLE_REASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 469
    .local v3, "sixMonthLifestyleReassessContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    .line 470
    .local v8, "today":Ljava/util/Calendar;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, p1}, Lcom/cigna/coach/db/CoachMessageDBManager;->getLatestLifeStyleScoreDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v7

    .line 474
    .local v7, "maxScoreDate":Ljava/util/Calendar;
    if-eqz v7, :cond_1

    .line 476
    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->monthsAndDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)F

    move-result v6

    .line 477
    .local v6, "absMonthsBetween":F
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Last assesment day: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "today: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v8}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "absDaysBetween last assesment"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :cond_0
    const/high16 v0, 0x40c00000    # 6.0f

    cmpl-float v0, v6, v0

    if-nez v0, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    move-object v5, p2

    .line 483
    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 487
    .end local v6    # "absMonthsBetween":F
    :cond_1
    return-void
.end method

.method setCheckUserProgressMessages_UPDATE_YOUR_WEIGHT_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 24
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 583
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v22, Lcom/cigna/coach/utils/WeightChangeListener;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/WeightChangeListener;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 585
    .local v22, "weightHelper":Lcom/cigna/coach/utils/WeightChangeListener;
    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/WeightChangeListener;->isWeightGoalActive(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 587
    :cond_1
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 588
    .local v6, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v7, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->UPDATE_YOUR_WEIGHT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 590
    .local v7, "updateYourWeightContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    .line 591
    .local v8, "now":Ljava/util/Calendar;
    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v8}, Lcom/cigna/coach/utils/WeightChangeListener;->weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;

    move-result-object v23

    .line 592
    .local v23, "weightTracker":Lcom/cigna/coach/dataobjects/WeightTracker;
    if-eqz v23, :cond_0

    .line 594
    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/dataobjects/WeightTracker;->getDateInMsec()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v21

    .line 595
    .local v21, "lastWeightUpdate":Ljava/util/Calendar;
    move-object/from16 v0, v21

    invoke-static {v0, v8}, Lcom/cigna/coach/utils/CalendarUtil;->absDateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v10

    .line 596
    .local v10, "daysSinceWeightUpdate":I
    if-eqz p4, :cond_0

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 597
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v13

    .line 598
    .local v13, "repeatInterval":I
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v12

    .line 599
    .local v12, "maxValue":I
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v4

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v5, p5

    invoke-direct/range {v2 .. v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v14

    .local v14, "lastDeliveredMsgDaysBetween":I
    move-object/from16 v9, p0

    move/from16 v11, p5

    .line 602
    invoke-virtual/range {v9 .. v14}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v17, v6

    move-object/from16 v18, v7

    move-object/from16 v19, p4

    move-object/from16 v20, p2

    .line 603
    invoke-direct/range {v15 .. v20}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    goto :goto_0
.end method

.method setCheckUserProgressMessages_USER_NOT_STARTED_inDays(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ILjava/util/List;I)V
    .locals 22
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "numberOfScores"    # I
    .param p5, "minValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 557
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p4, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 558
    .local v5, "checkUserProgressContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_NOT_STARTED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 560
    .local v6, "userNotStartedContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    sget-object v2, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/cigna/coach/db/CoachMessageDBManager;->hasScoreType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)Z

    move-result v20

    .line 562
    .local v20, "hasACategoryScore":Z
    if-nez v20, :cond_1

    if-eqz p4, :cond_1

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 563
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getUserCreateDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v21

    .line 564
    .local v21, "userCreateDate":Ljava/util/Calendar;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    .line 565
    .local v7, "today":Ljava/util/Calendar;
    const/4 v9, -0x1

    .line 566
    .local v9, "daysBetweenTodayAndUserCreateDate":I
    if-eqz v21, :cond_0

    .line 567
    move-object/from16 v0, v21

    invoke-static {v0, v7}, Lcom/cigna/coach/utils/CalendarUtil;->absDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v9

    .line 568
    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v12

    .line 569
    .local v12, "repeatInterval":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v11

    .line 570
    .local v11, "maxValue":I
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/CoachMessage;

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/CoachMessage;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v4, p5

    invoke-direct/range {v1 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v13

    .local v13, "lastDeliveredMsgDaysBetween":I
    move-object/from16 v8, p0

    move/from16 v10, p5

    .line 573
    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v18, p4

    move-object/from16 v19, p2

    .line 574
    invoke-direct/range {v14 .. v19}, Lcom/cigna/coach/utils/CoachMessageHelper;->checkAndAddMessagesToList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 578
    .end local v7    # "today":Ljava/util/Calendar;
    .end local v9    # "daysBetweenTodayAndUserCreateDate":I
    .end local v11    # "maxValue":I
    .end local v12    # "repeatInterval":I
    .end local v13    # "lastDeliveredMsgDaysBetween":I
    .end local v21    # "userCreateDate":Ljava/util/Calendar;
    :cond_1
    return-void
.end method

.method public setGetGoalsMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 837
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    if-eqz p2, :cond_3

    .line 838
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 839
    .local v4, "retrieveGoalsContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 845
    .local v5, "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    new-instance v15, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v15, v2}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 847
    .local v15, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalsCount(Ljava/lang/String;)I

    move-result v16

    .line 848
    .local v16, "numberOfActiveGoals":I
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 849
    sget-object v2, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "numberOfActiveGoals = "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    :cond_0
    if-nez v16, :cond_1

    .line 852
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOALS_LEFT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 853
    .local v6, "noGoalsLeftContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v2, v4, v5, v6}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v7

    .local v7, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 855
    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v14

    .line 858
    .local v14, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 865
    .end local v6    # "noGoalsLeftContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v14    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 868
    sget-object v12, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->DEFAULT_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 869
    .local v12, "defaultMessageContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v2, v4, v5, v12}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v7

    .restart local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v4

    move-object v11, v5

    move-object v13, v7

    .line 871
    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v14

    .line 874
    .restart local v14    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 881
    .end local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v12    # "defaultMessageContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v14    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_2
    invoke-virtual/range {p0 .. p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->setRetrieveGoalsGoalSpecificMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 884
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 887
    .end local v4    # "retrieveGoalsContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v5    # "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v15    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v16    # "numberOfActiveGoals":I
    :cond_3
    return-void
.end method

.method public setGetGoalsMessagesWhenAddingGoal(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1095
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1096
    .local v2, "retrieveGoalsContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1097
    .local v3, "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_ADDS_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1099
    .local v4, "userAddsGoalContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 1101
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v6

    .line 1104
    .local v6, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1105
    invoke-virtual {p0, p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1106
    return-void
.end method

.method public setGetGoalsMessagesWhenDeletingGoal(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1075
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1076
    .local v2, "retrieveGoalsContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1077
    .local v3, "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_DELETES_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1078
    .local v4, "userDeletesGoalContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    new-instance v7, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v7, v0}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1080
    .local v7, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    invoke-virtual {v7, p1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalsCount(Ljava/lang/String;)I

    move-result v8

    .line 1081
    .local v8, "numberOfActiveRemainingGoals":I
    if-lez v8, :cond_0

    .line 1082
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 1084
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v6

    .line 1087
    .local v6, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1088
    invoke-virtual {p0, p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1090
    .end local v5    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v6    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_0
    return-void
.end method

.method public setGetGoalsMessagesWhenDeletingMission(Ljava/lang/String;ILcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1111
    .local p3, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1112
    .local v2, "retrieveGoalsContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1113
    .local v3, "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_DELETES_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1115
    .local v4, "userDeletesMissionContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, p1, p2}, Lcom/cigna/coach/db/CoachMessageDBManager;->getNumberOfActiveMissions(Ljava/lang/String;I)I

    move-result v7

    .line 1116
    .local v7, "numberOfActiveRemainingMissions":I
    if-nez v7, :cond_0

    .line 1117
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 1119
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v6

    .line 1122
    .local v6, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {p3}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1123
    invoke-virtual {p0, p3}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1125
    .end local v5    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v6    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_0
    return-void
.end method

.method public setGetScoresMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 22
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/ScoreInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 685
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/ScoreInfo;>;"
    if-eqz p2, :cond_0

    .line 687
    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 688
    .local v5, "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 689
    .local v4, "getScoresContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    const/16 v19, 0x0

    .line 691
    .local v19, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/cigna/coach/dataobjects/ScoreInfoData;

    .line 692
    .local v20, "scoreInfoData":Lcom/cigna/coach/dataobjects/ScoreInfoData;
    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getLssList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v21

    .line 695
    .local v21, "summarizedScoreCount":I
    if-nez v21, :cond_1

    .line 697
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->OVERALL_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 698
    .local v6, "overallScoreContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getCurrentScore()I

    move-result v3

    invoke-virtual {v2, v4, v5, v6, v3}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;

    move-result-object v7

    .local v7, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 700
    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v19

    .line 726
    .end local v6    # "overallScoreContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 729
    const/4 v2, 0x1

    move/from16 v0, v21

    if-ne v0, v2, :cond_0

    .line 731
    sget-object v11, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 732
    .local v11, "reassessMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v12, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 733
    .local v12, "reassessContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v11, v12, v3}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;

    move-result-object v7

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v4

    move-object v13, v7

    .line 735
    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v19

    .line 737
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 740
    .end local v4    # "getScoresContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v5    # "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v11    # "reassessMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v12    # "reassessContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v19    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v20    # "scoreInfoData":Lcom/cigna/coach/dataobjects/ScoreInfoData;
    .end local v21    # "summarizedScoreCount":I
    :cond_0
    return-void

    .line 702
    .restart local v4    # "getScoresContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .restart local v5    # "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .restart local v19    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .restart local v20    # "scoreInfoData":Lcom/cigna/coach/dataobjects/ScoreInfoData;
    .restart local v21    # "summarizedScoreCount":I
    :cond_1
    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->isTimeToReassess()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 704
    sget-object v11, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->REASSESS_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 705
    .restart local v11    # "reassessMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v12, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 706
    .restart local v12    # "reassessContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    const/16 v3, 0xb5

    invoke-virtual {v2, v4, v11, v12, v3}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;

    move-result-object v7

    .restart local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v4

    move-object v13, v7

    .line 708
    invoke-virtual/range {v8 .. v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v19

    .line 710
    goto :goto_0

    .end local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v11    # "reassessMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v12    # "reassessContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    :cond_2
    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getVarianceValue()I

    move-result v2

    if-lez v2, :cond_3

    .line 712
    sget-object v15, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->HISTORICAL_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 713
    .local v15, "historicalScoresContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v17, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CURRENT_SCORE_HIGHER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 714
    .local v17, "higherScoreContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, v17

    invoke-virtual {v2, v15, v5, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v7

    .restart local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move-object/from16 v16, v5

    move-object/from16 v18, v7

    .line 716
    invoke-virtual/range {v13 .. v18}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v19

    .line 718
    goto :goto_0

    .line 720
    .end local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v15    # "historicalScoresContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v17    # "higherScoreContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    :cond_3
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->OVERALL_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 721
    .restart local v6    # "overallScoreContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getCurrentScore()I

    move-result v3

    invoke-virtual {v2, v4, v5, v6, v3}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;

    move-result-object v7

    .restart local v7    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    .line 723
    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v19

    goto/16 :goto_0
.end method

.method public setGetSuggestedGoalsMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1356
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    if-eqz p2, :cond_0

    .line 1358
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SUGGESTED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1359
    .local v2, "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1360
    .local v3, "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1362
    .local v4, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    new-instance v7, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v7, v0}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1364
    .local v7, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    invoke-virtual {v7, p1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalsCount(Ljava/lang/String;)I

    move-result v8

    .line 1365
    .local v8, "noOfActiveUserGoals":I
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4, v8}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;

    move-result-object v5

    .local v5, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v0, p0

    move-object v1, p1

    .line 1368
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v6

    .line 1370
    .local v6, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1371
    invoke-virtual {p0, p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1373
    .end local v2    # "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v3    # "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v4    # "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v5    # "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v6    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v7    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v8    # "noOfActiveUserGoals":I
    :cond_0
    return-void
.end method

.method public setGetSuggestedMissionsMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1378
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    if-eqz p2, :cond_0

    .line 1380
    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SUGGESTED_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1381
    .local v2, "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1382
    .local v3, "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE_MISSION_COUNT_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1383
    .local v4, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    const/4 v5, 0x0

    .line 1384
    .local v5, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    new-instance v7, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v7, v0}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1385
    .local v7, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    invoke-virtual {v7, p1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserMissionsCount(Ljava/lang/String;)I

    move-result v8

    .line 1386
    .local v8, "noOfActiveUserMissions":I
    if-nez v8, :cond_1

    .line 1387
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE_MISSION_COUNT_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1388
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    .line 1394
    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v6

    .line 1395
    .local v6, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1396
    invoke-virtual {p0, p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1398
    .end local v2    # "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v3    # "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v4    # "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v5    # "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v6    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    .end local v7    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v8    # "noOfActiveUserMissions":I
    :cond_0
    return-void

    .line 1390
    .restart local v2    # "contextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .restart local v3    # "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .restart local v4    # "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .restart local v5    # "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .restart local v7    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .restart local v8    # "noOfActiveUserMissions":I
    :cond_1
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE_MISSION_COUNT_GREATER_THAN_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1391
    iget-object v0, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v0, v2, v3, v4, v8}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;

    move-result-object v5

    goto :goto_0
.end method

.method public setQuestionAnswerMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;Ljava/util/List;ZLjava/util/List;)V
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p4, "unalignedGoalsCancelled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            ">;Z",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1258
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p3, "categoryTypes":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    .local p5, "badgesEarned":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    if-eqz p2, :cond_3

    .line 1260
    sget-object v3, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_QUESTION_ANSWER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1262
    .local v3, "setQuestionAnswerContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    if-eqz p4, :cond_0

    .line 1263
    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1264
    .local v4, "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1265
    .local v5, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v1, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v1, v3, v4, v5}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v6

    .local v6, "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v1, p0

    move-object v2, p1

    .line 1267
    invoke-virtual/range {v1 .. v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v8

    .line 1270
    .local v8, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1274
    .end local v4    # "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v5    # "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v6    # "coachMessageList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v8    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_0
    if-eqz p3, :cond_1

    .line 1275
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 1277
    .local v7, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, p1, v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCategorySpecificCoachMessage(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    goto :goto_0

    .line 1281
    .end local v7    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v9    # "i$":Ljava/util/Iterator;
    :cond_1
    if-eqz p5, :cond_2

    .line 1282
    move-object/from16 v0, p5

    invoke-direct {p0, p1, v3, v0}, Lcom/cigna/coach/utils/CoachMessageHelper;->getTimelineBadgeMessages(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    .line 1283
    .local v11, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v11, :cond_2

    .line 1284
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1288
    .end local v11    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_2
    new-instance v10, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v10}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V

    .line 1289
    .local v10, "instrMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v1, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->INSTRUCTIONAL_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v10, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 1290
    const/16 v1, 0x43e

    invoke-virtual {v10, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 1291
    const/4 v1, -0x1

    invoke-virtual {v10, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageDescriptionContentId(I)V

    .line 1292
    const/4 v1, -0x1

    invoke-virtual {v10, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageImageContentId(I)V

    .line 1293
    const/4 v1, 0x1

    invoke-virtual {v10, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageDisplayRank(I)V

    .line 1294
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1297
    invoke-virtual {p0, p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1299
    .end local v3    # "setQuestionAnswerContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v10    # "instrMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_3
    return-void
.end method

.method setRetrieveGoalsGoalSpecificMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 890
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    if-eqz p2, :cond_1

    .line 891
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 892
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addMessageToListIfNotNull addMessageToListIfNotNull  = GOAL_MISSIONS_ZERO     "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 895
    :cond_0
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 897
    .local v1, "goalInfoMissionsInfos":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    if-eqz v1, :cond_1

    .line 898
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 899
    .local v0, "goalInfoMissionsInfo":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/utils/CoachMessageHelper;->setRetrieveGoalsGoalSpecificMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;)V

    goto :goto_0

    .line 904
    .end local v0    # "goalInfoMissionsInfo":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v1    # "goalInfoMissionsInfos":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method setRetrieveGoalsGoalSpecificMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;)V
    .locals 59
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalInfoMissionsInfo"    # Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .prologue
    .line 907
    if-eqz p2, :cond_0

    .line 909
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalId()I

    move-result v3

    const/16 v4, 0x30

    if-ne v3, v4, :cond_1

    .line 910
    new-instance v58, Lcom/cigna/coach/utils/WeightChangeListener;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, v58

    invoke-direct {v0, v3}, Lcom/cigna/coach/utils/WeightChangeListener;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .local v58, "weightChangeListener":Lcom/cigna/coach/utils/WeightChangeListener;
    move-object/from16 v3, p2

    .line 911
    check-cast v3, Lcom/cigna/coach/dataobjects/UserGoalData;

    move-object/from16 v0, v58

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/cigna/coach/utils/WeightChangeListener;->getMessageForWeightGoal(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->setCoachMessage(Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1070
    .end local v58    # "weightChangeListener":Lcom/cigna/coach/utils/WeightChangeListener;
    :cond_0
    :goto_0
    return-void

    .line 920
    :cond_1
    new-instance v50, Ljava/util/ArrayList;

    invoke-direct/range {v50 .. v50}, Ljava/util/ArrayList;-><init>()V

    .line 921
    .local v50, "goalSpecificCoachMessageDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/CoachMessageData;>;"
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 922
    .local v5, "retrieveGoalsContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    sget-object v6, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .local v6, "goalSpecificMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    move-object/from16 v3, p2

    .line 924
    check-cast v3, Lcom/cigna/coach/dataobjects/UserGoalData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalSequenceId()I

    move-result v49

    .line 925
    .local v49, "goalSeqId":I
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 926
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "goalSeqId = goalSeqId   "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    :cond_2
    const/16 v55, 0x0

    .line 930
    .local v55, "noMissionFor7or14Or21Days":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v3, v0, v1}, Lcom/cigna/coach/db/CoachMessageDBManager;->noOfDaysPassedSinceLastMissionActivityForAGoal(Ljava/lang/String;I)I

    move-result v37

    .line 931
    .local v37, "absDaysBetweenForMissionActivity":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v3, v0, v1}, Lcom/cigna/coach/db/CoachMessageDBManager;->checkActionMissionsAvailable(Ljava/lang/String;I)Z

    move-result v52

    .line 932
    .local v52, "isActionMissionExists":Z
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v15

    .line 936
    .local v15, "today":Ljava/util/Calendar;
    if-eqz p2, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_4

    const/4 v3, -0x1

    move/from16 v0, v37

    if-ne v0, v3, :cond_4

    .line 938
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 939
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "ContextFilterType = GOAL_MISSIONS_ZERO"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    :cond_3
    sget-object v7, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_MISSIONS_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 943
    .local v7, "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v3, v5, v6, v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v8

    .local v8, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v3, p0

    move-object/from16 v4, p1

    .line 946
    invoke-virtual/range {v3 .. v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v46

    .local v46, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v3, v46

    .line 948
    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMinValue()I

    move-result v12

    .local v12, "minValue":I
    move-object/from16 v3, v46

    .line 949
    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v20

    .local v20, "repeatInterval":I
    move-object/from16 v3, v46

    .line 950
    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v19

    .local v19, "maxValue":I
    move-object/from16 v3, p2

    .line 951
    check-cast v3, Lcom/cigna/coach/dataobjects/UserGoalData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v56

    .line 954
    .local v56, "startDate":Ljava/util/Calendar;
    move-object/from16 v0, v56

    invoke-static {v15, v0}, Lcom/cigna/coach/utils/CalendarUtil;->absDateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v17

    .local v17, "absDaysBetween":I
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v6

    move-object v13, v5

    move-object v14, v7

    .line 955
    invoke-direct/range {v9 .. v15}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v21

    .local v21, "lastDeliveredMsgDaysBetween":I
    move-object/from16 v16, p0

    move/from16 v18, v12

    .line 957
    invoke-virtual/range {v16 .. v21}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 958
    check-cast v46, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local v46    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 959
    const/16 v55, 0x1

    .line 965
    .end local v7    # "contextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v8    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v12    # "minValue":I
    .end local v17    # "absDaysBetween":I
    .end local v19    # "maxValue":I
    .end local v20    # "repeatInterval":I
    .end local v21    # "lastDeliveredMsgDaysBetween":I
    .end local v56    # "startDate":Ljava/util/Calendar;
    :cond_4
    if-nez v52, :cond_7

    if-nez v55, :cond_7

    .line 967
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v3, v0, v1}, Lcom/cigna/coach/db/CoachMessageDBManager;->getUserGoalStartDate(Ljava/lang/String;I)Ljava/util/Calendar;

    move-result-object v57

    .line 969
    .local v57, "userGoalStartDate":Ljava/util/Calendar;
    if-eqz v57, :cond_6

    .line 970
    sget-object v14, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->DAYS_NO_ACTION_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 971
    .local v14, "daysNoActionMissionContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v3, v5, v6, v14}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v8

    .line 973
    .restart local v8    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v8, :cond_6

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 974
    move-object/from16 v0, v57

    invoke-static {v15, v0}, Lcom/cigna/coach/utils/CalendarUtil;->absDateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v23

    .line 975
    .local v23, "absDaysForActionMission":I
    const/4 v3, 0x0

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v20

    .line 976
    .restart local v20    # "repeatInterval":I
    const/4 v3, 0x0

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v19

    .line 978
    .restart local v19    # "maxValue":I
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getSingleContextCoachMessageMap(Ljava/util/List;)Ljava/util/Map;

    move-result-object v47

    .line 979
    .local v47, "coachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v47 .. v47}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v48

    .line 980
    .local v48, "coachMsgKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface/range {v48 .. v48}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v51

    .local v51, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 981
    .restart local v12    # "minValue":I
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v47

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/util/List;

    .local v29, "minValueBasedCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v6

    move-object v13, v5

    .line 983
    invoke-direct/range {v9 .. v15}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v21

    .restart local v21    # "lastDeliveredMsgDaysBetween":I
    move-object/from16 v22, p0

    move/from16 v24, v12

    move/from16 v25, v19

    move/from16 v26, v20

    move/from16 v27, v21

    .line 985
    invoke-virtual/range {v22 .. v27}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v24, p0

    move-object/from16 v25, p1

    move-object/from16 v26, v5

    move-object/from16 v27, v6

    move-object/from16 v28, v14

    .line 986
    invoke-virtual/range {v24 .. v29}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v46

    .line 988
    .restart local v46    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    check-cast v46, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local v46    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 989
    const/16 v55, 0x1

    goto :goto_1

    .line 994
    .end local v8    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v12    # "minValue":I
    .end local v14    # "daysNoActionMissionContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v19    # "maxValue":I
    .end local v20    # "repeatInterval":I
    .end local v21    # "lastDeliveredMsgDaysBetween":I
    .end local v23    # "absDaysForActionMission":I
    .end local v29    # "minValueBasedCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v47    # "coachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v48    # "coachMsgKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v51    # "i$":Ljava/util/Iterator;
    :cond_6
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 995
    const-string v3, "CLASS_NAME"

    const-string v4, "isActionMissionExists == true"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1000
    .end local v57    # "userGoalStartDate":Ljava/util/Calendar;
    :cond_7
    const/4 v3, -0x1

    move/from16 v0, v37

    if-eq v0, v3, :cond_c

    .line 1002
    const/16 v44, 0x0

    .line 1003
    .local v44, "activeMission":Z
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v51

    .restart local v51    # "i$":Ljava/util/Iterator;
    :cond_8
    :goto_2
    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v54

    check-cast v54, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .local v54, "missionDetails":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    move-object/from16 v53, v54

    .line 1004
    check-cast v53, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 1005
    .local v53, "missionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1006
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Current Mission Status : "

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v53 .. v53}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    :cond_9
    invoke-virtual/range {v53 .. v53}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v3

    if-eqz v3, :cond_8

    invoke-virtual/range {v53 .. v53}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v3, v4}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1009
    const/16 v44, 0x1

    goto :goto_2

    .line 1012
    .end local v53    # "missionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    .end local v54    # "missionDetails":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    :cond_a
    if-nez v44, :cond_c

    .line 1014
    sget-object v35, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOAL_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1015
    .local v35, "noGoalProgressContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, v35

    invoke-virtual {v3, v5, v6, v0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v8

    .line 1017
    .restart local v8    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getSingleContextCoachMessageMap(Ljava/util/List;)Ljava/util/Map;

    move-result-object v47

    .line 1018
    .restart local v47    # "coachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    invoke-interface/range {v47 .. v47}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v48

    .line 1019
    .restart local v48    # "coachMsgKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface/range {v48 .. v48}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v51

    :cond_b
    :goto_3
    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface/range {v51 .. v51}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 1020
    .restart local v12    # "minValue":I
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v47

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Ljava/util/List;

    .line 1021
    .restart local v29    # "minValueBasedCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v29, :cond_b

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 1022
    const/4 v3, 0x0

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v20

    .line 1023
    .restart local v20    # "repeatInterval":I
    const/4 v3, 0x0

    move-object/from16 v0, v29

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v19

    .restart local v19    # "maxValue":I
    move-object/from16 v30, p0

    move-object/from16 v31, p1

    move-object/from16 v32, v6

    move/from16 v33, v12

    move-object/from16 v34, v5

    move-object/from16 v36, v15

    .line 1024
    invoke-direct/range {v30 .. v36}, Lcom/cigna/coach/utils/CoachMessageHelper;->getLastDeliveredMsgDaysBetween(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/Calendar;)I

    move-result v21

    .restart local v21    # "lastDeliveredMsgDaysBetween":I
    move-object/from16 v36, p0

    move/from16 v38, v12

    move/from16 v39, v19

    move/from16 v40, v20

    move/from16 v41, v21

    .line 1026
    invoke-virtual/range {v36 .. v41}, Lcom/cigna/coach/utils/CoachMessageHelper;->isAddMessagesToList(IIIII)Z

    move-result v3

    if-eqz v3, :cond_b

    move-object/from16 v31, p0

    move-object/from16 v32, p1

    move-object/from16 v33, v5

    move-object/from16 v34, v6

    move-object/from16 v36, v29

    .line 1027
    invoke-virtual/range {v31 .. v36}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v46

    .line 1029
    .restart local v46    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    check-cast v46, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local v46    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 1030
    const/16 v55, 0x1

    goto :goto_3

    .line 1039
    .end local v8    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v12    # "minValue":I
    .end local v19    # "maxValue":I
    .end local v20    # "repeatInterval":I
    .end local v21    # "lastDeliveredMsgDaysBetween":I
    .end local v29    # "minValueBasedCoachMessagesList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v35    # "noGoalProgressContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v44    # "activeMission":Z
    .end local v47    # "coachMessageMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;>;"
    .end local v48    # "coachMsgKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v51    # "i$":Ljava/util/Iterator;
    :cond_c
    if-nez v55, :cond_e

    .line 1042
    sget-object v42, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_ON_TRACK:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1044
    .local v42, "goalOnTrackContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v45

    .line 1045
    .local v45, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    if-eqz v45, :cond_e

    .line 1046
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1047
    sget-object v3, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "ContextFilterType = GOAL_ON_TRACK"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, v42

    move-object/from16 v1, v45

    invoke-virtual {v3, v5, v6, v0, v1}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesForGoalOnTrackWithCategory(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/util/List;

    move-result-object v8

    .restart local v8    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object/from16 v38, p0

    move-object/from16 v39, p1

    move-object/from16 v40, v5

    move-object/from16 v41, v6

    move-object/from16 v43, v8

    .line 1052
    invoke-virtual/range {v38 .. v43}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v46

    .line 1054
    .restart local v46    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    check-cast v46, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local v46    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    move-object/from16 v0, p0

    move-object/from16 v1, v50

    move-object/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 1064
    .end local v8    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v42    # "goalOnTrackContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v45    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_e
    invoke-interface/range {v50 .. v50}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 1065
    new-instance v3, Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;

    invoke-direct {v3}, Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;-><init>()V

    move-object/from16 v0, v50

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1066
    const/4 v3, 0x0

    move-object/from16 v0, v50

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/apiobjects/CoachMessage;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->setCoachMessage(Lcom/cigna/coach/apiobjects/CoachMessage;)V

    goto/16 :goto_0
.end method

.method public setSetMissionDataMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ZZLjava/util/List;)V
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "goalCompleted"    # Z
    .param p4, "missionCompleted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;ZZ",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1190
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p5, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    if-eqz p2, :cond_3

    .line 1191
    sget-object v3, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_MISSION_DATA:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1194
    .local v3, "setMissionDataContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    if-eqz p3, :cond_0

    .line 1195
    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1196
    .local v4, "timelineCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1197
    .local v5, "goalCompleteContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v1, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v1, v3, v4, v5}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v6

    .local v6, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v1, p0

    move-object v2, p1

    .line 1199
    invoke-virtual/range {v1 .. v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v13

    .line 1201
    .local v13, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1, v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1205
    .end local v4    # "timelineCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v5    # "goalCompleteContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v6    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v13    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_0
    if-eqz p4, :cond_1

    .line 1206
    sget-object v10, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 1207
    .local v10, "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v11, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->MISSION_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 1208
    .local v11, "goalCompletedContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    iget-object v1, p0, Lcom/cigna/coach/utils/CoachMessageHelper;->dbManager:Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-virtual {v1, v3, v10, v11}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v6

    .restart local v6    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    move-object v7, p0

    move-object v8, p1

    move-object v9, v3

    move-object v12, v6

    .line 1210
    invoke-virtual/range {v7 .. v12}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v13

    .line 1212
    .restart local v13    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1, v13}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/apiobjects/CoachMessage;)V

    .line 1217
    .end local v6    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v10    # "coachMessageCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v11    # "goalCompletedContextFilterType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .end local v13    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    :cond_1
    move-object/from16 v0, p5

    invoke-direct {p0, p1, v3, v0}, Lcom/cigna/coach/utils/CoachMessageHelper;->getTimelineBadgeMessages(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 1218
    .restart local v6    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v6, :cond_2

    .line 1219
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1222
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1224
    .end local v3    # "setMissionDataContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .end local v6    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_3
    return-void
.end method

.method public setSetMissionDataTrackerMessages(Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/apiobjects/CoachResponse;Ljava/util/List;ZZ)V
    .locals 9
    .param p1, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .param p4, "missionCompleted"    # Z
    .param p5, "goalCompleted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 1152
    .local p2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    .local p3, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    sget-object v6, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1153
    sget-object v6, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "setSetMissionDataTrackerMessages : START, missionCompleted ="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " goalCompleted: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    :cond_0
    if-eqz p2, :cond_6

    .line 1156
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_MISSION_DATA:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 1158
    .local v4, "setMissionDataContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    if-eqz p3, :cond_1

    .line 1159
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getUserId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6, v4, p3}, Lcom/cigna/coach/utils/CoachMessageHelper;->getTimelineBadgeMessages(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 1160
    .local v5, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz v5, :cond_1

    .line 1161
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1165
    .end local v5    # "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    :cond_1
    sget-object v0, Lcom/cigna/coach/utils/CoachMessageHelper;->NOTIFICATION_TYPES:[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .local v0, "arr$":[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_5

    aget-object v3, v0, v1

    .line 1166
    .local v3, "messageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v6, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1167
    sget-object v6, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Adding message for type:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    :cond_2
    if-eqz p4, :cond_3

    .line 1170
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TRACKER_MISSION_COMPLETED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-direct {p0, p1, p2, v3, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addNotificationsForMissionToResponse(Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V

    .line 1172
    :cond_3
    if-eqz p5, :cond_4

    .line 1173
    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-direct {p0, p1, p2, v3, v6}, Lcom/cigna/coach/utils/CoachMessageHelper;->addNotificationsForMissionToResponse(Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V

    .line 1165
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1176
    .end local v3    # "messageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    :cond_5
    invoke-virtual {p0, p2}, Lcom/cigna/coach/utils/CoachMessageHelper;->orderCoachMessages(Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 1177
    sget-object v6, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1178
    sget-object v6, Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "setSetMissionDataTrackerMessages : END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181
    .end local v0    # "arr$":[Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "setMissionDataContextType":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    :cond_6
    return-void
.end method
