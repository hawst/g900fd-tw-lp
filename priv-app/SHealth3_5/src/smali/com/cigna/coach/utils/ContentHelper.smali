.class public Lcom/cigna/coach/utils/ContentHelper;
.super Ljava/lang/Object;
.source "ContentHelper.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/ContentHelper$1;,
        Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;,
        Lcom/cigna/coach/utils/ContentHelper$ContentResolver;,
        Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;,
        Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field public static articleBaseUrl:Ljava/lang/String; = null

.field private static final articleBaseUrlContentId:I = 0x1330

.field public static final articleBaseUrlFallback:Ljava/lang/String; = "http://img.samsungshealth.com/Cigna/Articles/"

.field public static final articleBaseUrlForChina:Ljava/lang/String; = "http://cn-img.samsungshealth.com/Cigna/Articles/"

.field private static final contentInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/ContentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static fromCache:D = 0.0

.field private static fromDb:D = 0.0

.field private static final getMethodMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "[",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;>;"
        }
    .end annotation
.end field

.field public static isLatinSpanish:Z = false

.field public static final mccArgentina:I = 0x2d2

.field public static final mccChile:I = 0x2da

.field public static final mccColombia:I = 0x2dc

.field private static final setMethodMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cacheContent:Z

.field private dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

.field tagPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const-class v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ContentHelper;->contentInfoMap:Ljava/util/Map;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ContentHelper;->getMethodMap:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ContentHelper;->setMethodMap:Ljava/util/Map;

    .line 60
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    .line 61
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sput-wide v0, Lcom/cigna/coach/utils/ContentHelper;->fromDb:D

    .line 66
    const-string v0, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v0, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    sput-boolean v0, Lcom/cigna/coach/utils/ContentHelper;->isLatinSpanish:Z

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, "\\$\\{[a-zA-Z_]+\\}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/utils/ContentHelper;->tagPattern:Ljava/util/regex/Pattern;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cigna/coach/utils/ContentHelper;->cacheContent:Z

    .line 375
    iput-object p1, p0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 376
    return-void
.end method

.method private fillContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V
    .locals 30
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .param p3, "requestedLocale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 680
    if-eqz p1, :cond_14

    .line 681
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 682
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "------------------------------------------------------"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Resolving contents in class:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :cond_0
    new-instance v16, Ljava/util/HashSet;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashSet;-><init>()V

    .line 686
    .local v16, "contentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    .line 688
    .local v21, "contentSetInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;>;"
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 689
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Getting all methods that has any coach annotations"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/cigna/coach/utils/ContentHelper;->getContentIdAndTagMethods(Ljava/lang/Class;)[Ljava/util/Map;

    move-result-object v20

    .line 693
    .local v20, "contentRelatedMethodMaps":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    const/4 v3, 0x0

    aget-object v15, v20, v3

    .line 694
    .local v15, "contentIdMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    const/4 v3, 0x1

    aget-object v8, v20, v3

    .line 695
    .local v8, "tagNameMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    const/4 v3, 0x2

    aget-object v19, v20, v3

    .line 697
    .local v19, "contentObjectMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    if-eqz v15, :cond_11

    invoke-interface {v15}, Ljava/util/Map;->size()I

    move-result v3

    if-lez v3, :cond_11

    .line 698
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 699
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v15}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " methods found"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    :cond_2
    invoke-interface {v15}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v14

    .line 702
    .local v14, "contentIdMethodEntrys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;>;"
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .local v24, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    .line 703
    .local v13, "contentIdMethodEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/reflect/Method;

    .line 704
    .local v26, "method":Ljava/lang/reflect/Method;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 705
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invoking method :"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 708
    :cond_4
    const/4 v3, 0x0

    :try_start_0
    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    .line 709
    .local v11, "contentId":Ljava/lang/Integer;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 710
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got content id:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    :cond_5
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_3

    .line 713
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 714
    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;

    move-object/from16 v0, v26

    invoke-direct {v4, v11, v0}, Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;-><init>(Ljava/lang/Integer;Ljava/lang/reflect/Method;)V

    move-object/from16 v0, v21

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 716
    .end local v11    # "contentId":Ljava/lang/Integer;
    :catch_0
    move-exception v22

    .line 717
    .local v22, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invocation of "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " failed"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    const-string v4, "Error while fillContent."

    move-object/from16 v0, v22

    invoke-direct {v3, v4, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 721
    .end local v13    # "contentIdMethodEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v22    # "e":Ljava/lang/Exception;
    .end local v26    # "method":Ljava/lang/reflect/Method;
    :cond_6
    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_10

    .line 722
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 723
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Resolving "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " content ids to description"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    :cond_7
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/ContentHelper;->getContentInfo(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v18

    .line 726
    .local v18, "contentInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 727
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Now setting content description to object"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 729
    :cond_8
    if-eqz v18, :cond_c

    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->size()I

    move-result v3

    if-lez v3, :cond_c

    .line 730
    invoke-interface/range {v21 .. v21}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    .line 731
    .local v10, "contentGetMethodNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;>;>;"
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_1
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/util/Map$Entry;

    .line 732
    .local v23, "getMethodName":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;>;"
    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;

    .line 733
    .local v5, "cInfo":Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;
    iget-object v0, v5, Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;->contentId:Ljava/lang/Integer;

    move-object/from16 v17, v0

    .line 734
    .local v17, "contentInfoId":Ljava/lang/Integer;
    iget-object v12, v5, Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;->method:Ljava/lang/reflect/Method;

    .line 736
    .local v12, "contentIdMethod":Ljava/lang/reflect/Method;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 737
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Getting content info for content id:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    :cond_9
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 742
    .local v7, "contentInfoAllLocale":Lcom/cigna/coach/dataobjects/ContentInfo;
    if-eqz v7, :cond_a

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v6, p3

    move-object/from16 v9, p2

    .line 743
    invoke-direct/range {v3 .. v9}, Lcom/cigna/coach/utils/ContentHelper;->setContentText(Ljava/lang/Object;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;Lcom/cigna/coach/utils/ContentHelper$LocaleType;Lcom/cigna/coach/dataobjects/ContentInfo;Ljava/util/Map;Lcom/cigna/coach/utils/ContextData;)V

    goto :goto_1

    .line 746
    :cond_a
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 747
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Content info is null"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    :cond_b
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Content id is missing in database:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 755
    .end local v5    # "cInfo":Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;
    .end local v7    # "contentInfoAllLocale":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v10    # "contentGetMethodNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;>;>;"
    .end local v12    # "contentIdMethod":Ljava/lang/reflect/Method;
    .end local v17    # "contentInfoId":Ljava/lang/Integer;
    .end local v23    # "getMethodName":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;>;"
    :cond_c
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 756
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "No content info was found in database"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    .end local v14    # "contentIdMethodEntrys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;>;"
    .end local v18    # "contentInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    .end local v24    # "i$":Ljava/util/Iterator;
    :cond_d
    :goto_2
    if-eqz v19, :cond_12

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->size()I

    move-result v3

    if-lez v3, :cond_12

    .line 772
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 773
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Found "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " coach content objects "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    :cond_e
    invoke-interface/range {v19 .. v19}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v28

    .line 776
    .local v28, "methodNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;>;"
    invoke-interface/range {v28 .. v28}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .restart local v24    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/util/Map$Entry;

    .line 777
    .local v27, "methodNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/reflect/Method;

    .line 778
    .local v25, "m":Ljava/lang/reflect/Method;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 779
    sget-object v4, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invoking content object method:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface/range {v27 .. v27}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 782
    :cond_f
    const/4 v3, 0x0

    :try_start_1
    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v29

    .line 783
    .local v29, "o":Ljava/lang/Object;
    move-object/from16 v0, p0

    move-object/from16 v1, v29

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 784
    .end local v29    # "o":Ljava/lang/Object;
    :catch_1
    move-exception v22

    .line 785
    .restart local v22    # "e":Ljava/lang/Exception;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invocation of "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v25 .. v25}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " failed"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 761
    .end local v22    # "e":Ljava/lang/Exception;
    .end local v25    # "m":Ljava/lang/reflect/Method;
    .end local v27    # "methodNameEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v28    # "methodNameEntrySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;>;"
    .restart local v14    # "contentIdMethodEntrys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;>;"
    :cond_10
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "No content ids to resolve"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 765
    .end local v14    # "contentIdMethodEntrys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/reflect/Method;>;>;"
    .end local v24    # "i$":Ljava/util/Iterator;
    :cond_11
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 766
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "No methods found that has CoachContentId annotation"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 790
    :cond_12
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 791
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "No coach content object found"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    .end local v8    # "tagNameMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v15    # "contentIdMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v16    # "contentIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v19    # "contentObjectMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v20    # "contentRelatedMethodMaps":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v21    # "contentSetInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;>;"
    :cond_13
    :goto_4
    return-void

    .line 796
    :cond_14
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 797
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Incoming object is null"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method private getContentIdAndTagMethods(Ljava/lang/Class;)[Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)[",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1132
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v8, 0x0

    .line 1133
    .local v8, "returnValue":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->getMethodMap:Ljava/util/Map;

    invoke-interface {v10, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    .line 1135
    .local v6, "isInCache":Z
    if-nez v6, :cond_9

    .line 1136
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1137
    .local v2, "contentIdMethods":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 1138
    .local v9, "tagGetterMethods":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1140
    .local v3, "contentObjectMethods":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    move-object v1, p1

    .line 1141
    .local v1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_0
    const-class v10, Ljava/lang/Object;

    if-eq v1, v10, :cond_8

    .line 1142
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v10

    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-direct {v0, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1143
    .local v0, "allMethods":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/reflect/Method;

    .line 1144
    .local v7, "method":Ljava/lang/reflect/Method;
    const-class v10, Lcom/cigna/coach/utils/CoachContentId;

    invoke-virtual {v7, v10}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v10

    if-nez v10, :cond_1

    const-class v10, Lcom/cigna/coach/utils/CoachMultiLanguageContentId;

    invoke-virtual {v7, v10}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1145
    :cond_1
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1146
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Adding content id method:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    :cond_2
    invoke-virtual {v7}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v2, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1150
    :cond_3
    const-class v10, Lcom/cigna/coach/utils/CoachContentTagValue;

    invoke-virtual {v7, v10}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1151
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1152
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Adding tag parameter method :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    :cond_4
    const-class v10, Lcom/cigna/coach/utils/CoachContentTagValue;

    invoke-virtual {v7, v10}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/utils/CoachContentTagValue;

    .line 1155
    .local v4, "contentTagInfo":Lcom/cigna/coach/utils/CoachContentTagValue;
    invoke-interface {v4}, Lcom/cigna/coach/utils/CoachContentTagValue;->tagName()Ljava/lang/String;

    move-result-object v10

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1157
    .end local v4    # "contentTagInfo":Lcom/cigna/coach/utils/CoachContentTagValue;
    :cond_5
    const-class v10, Lcom/cigna/coach/utils/CoachContentObject;

    invoke-virtual {v7, v10}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1158
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1159
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Adding content object method :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1161
    :cond_6
    invoke-virtual {v7}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 1164
    .end local v7    # "method":Ljava/lang/reflect/Method;
    :cond_7
    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    .line 1165
    goto/16 :goto_0

    .line 1166
    .end local v0    # "allMethods":Ljava/util/List;, "Ljava/util/List<Ljava/lang/reflect/Method;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_8
    const/4 v10, 0x3

    new-array v8, v10, [Ljava/util/Map;

    .end local v8    # "returnValue":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    const/4 v10, 0x0

    aput-object v2, v8, v10

    const/4 v10, 0x1

    aput-object v9, v8, v10

    const/4 v10, 0x2

    aput-object v3, v8, v10

    .line 1167
    .restart local v8    # "returnValue":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->getMethodMap:Ljava/util/Map;

    invoke-interface {v10, p1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1171
    .end local v1    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "contentIdMethods":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v3    # "contentObjectMethods":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    .end local v9    # "tagGetterMethods":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    :goto_2
    return-object v8

    .line 1169
    :cond_9
    sget-object v10, Lcom/cigna/coach/utils/ContentHelper;->getMethodMap:Ljava/util/Map;

    invoke-interface {v10, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .end local v8    # "returnValue":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    check-cast v8, [Ljava/util/Map;

    .restart local v8    # "returnValue":[Ljava/util/Map;, "[Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    goto :goto_2
.end method

.method private getContentInfo(I)Lcom/cigna/coach/dataobjects/ContentInfo;
    .locals 13
    .param p1, "contentId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    .line 1181
    sget-object v7, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1182
    sget-object v7, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getContentInfo "

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    :cond_0
    const/4 v6, 0x0

    .line 1186
    .local v6, "returnContentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    const/4 v3, 0x0

    .line 1187
    .local v3, "inCache":Z
    const/4 v0, 0x0

    .line 1188
    .local v0, "cidSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-boolean v7, p0, Lcom/cigna/coach/utils/ContentHelper;->cacheContent:Z

    if-eqz v7, :cond_1

    .line 1189
    sget-object v7, Lcom/cigna/coach/utils/ContentHelper;->contentInfoMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "returnContentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    check-cast v6, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 1190
    .restart local v6    # "returnContentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    if-eqz v6, :cond_5

    .line 1191
    const/4 v3, 0x1

    .line 1192
    sget-wide v7, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    add-double/2addr v7, v9

    sput-wide v7, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    .line 1199
    :cond_1
    :goto_0
    if-nez v3, :cond_3

    if-eqz v0, :cond_3

    .line 1200
    new-instance v1, Lcom/cigna/coach/db/ContentInfoDBManager;

    iget-object v7, p0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v1, v7}, Lcom/cigna/coach/db/ContentInfoDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1201
    .local v1, "contentDBManager":Lcom/cigna/coach/db/ContentInfoDBManager;
    invoke-virtual {v1, v0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getContentInfo(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v2

    .line 1202
    .local v2, "contentFromDBMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v7

    if-lez v7, :cond_3

    .line 1203
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "returnContentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    check-cast v6, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 1204
    .restart local v6    # "returnContentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    iget-boolean v7, p0, Lcom/cigna/coach/utils/ContentHelper;->cacheContent:Z

    if-eqz v7, :cond_2

    .line 1205
    sget-object v7, Lcom/cigna/coach/utils/ContentHelper;->contentInfoMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1207
    :cond_2
    sget-wide v7, Lcom/cigna/coach/utils/ContentHelper;->fromDb:D

    add-double/2addr v7, v9

    sput-wide v7, Lcom/cigna/coach/utils/ContentHelper;->fromDb:D

    .line 1211
    .end local v1    # "contentDBManager":Lcom/cigna/coach/db/ContentInfoDBManager;
    .end local v2    # "contentFromDBMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    :cond_3
    sget-object v7, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1212
    sget-wide v7, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    sget-wide v9, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    sget-wide v11, Lcom/cigna/coach/utils/ContentHelper;->fromDb:D

    add-double/2addr v9, v11

    div-double/2addr v7, v9

    const-wide/high16 v9, 0x4059000000000000L    # 100.0

    mul-double v4, v7, v9

    .line 1213
    .local v4, "percHit":D
    sget-object v7, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cache hit:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    .end local v4    # "percHit":D
    :cond_4
    return-object v6

    .line 1194
    :cond_5
    new-instance v0, Ljava/util/HashSet;

    .end local v0    # "cidSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1195
    .restart local v0    # "cidSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getContentInfo(Ljava/util/Collection;)Ljava/util/Map;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/ContentInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 1225
    .local p1, "contentIdArray":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    sget-object v13, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1226
    sget-object v13, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "getContentInfo "

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1229
    :cond_0
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 1231
    .local v11, "returnContentInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1233
    .local v8, "notInCacheIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_1

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v13

    if-nez v13, :cond_2

    :cond_1
    move-object v12, v11

    .line 1272
    .end local v11    # "returnContentInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    .local v12, "returnContentInfoMap":Ljava/lang/Object;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    :goto_0
    return-object v12

    .line 1236
    .end local v12    # "returnContentInfoMap":Ljava/lang/Object;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    .restart local v11    # "returnContentInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/cigna/coach/utils/ContentHelper;->cacheContent:Z

    if-eqz v13, :cond_4

    .line 1237
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1238
    .local v5, "contentId":I
    sget-object v13, Lcom/cigna/coach/utils/ContentHelper;->contentInfoMap:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 1239
    .local v6, "contentInfoAllLocales":Lcom/cigna/coach/dataobjects/ContentInfo;
    if-eqz v6, :cond_3

    .line 1240
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v11, v13, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1241
    sget-wide v13, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    add-double/2addr v13, v15

    sput-wide v13, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    goto :goto_1

    .line 1243
    :cond_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v8, v13}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1247
    .end local v5    # "contentId":I
    .end local v6    # "contentInfoAllLocales":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_4
    move-object/from16 v8, p1

    .line 1250
    :cond_5
    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v13

    if-lez v13, :cond_7

    .line 1251
    new-instance v3, Lcom/cigna/coach/db/ContentInfoDBManager;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v3, v13}, Lcom/cigna/coach/db/ContentInfoDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1252
    .local v3, "contentDBManager":Lcom/cigna/coach/db/ContentInfoDBManager;
    invoke-virtual {v3, v8}, Lcom/cigna/coach/db/ContentInfoDBManager;->getContentInfo(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v4

    .line 1254
    .local v4, "contentFromDBMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/cigna/coach/utils/ContentHelper;->cacheContent:Z

    if-eqz v13, :cond_6

    .line 1255
    if-eqz v4, :cond_7

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v13

    if-lez v13, :cond_7

    .line 1256
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    .line 1257
    .local v2, "cIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 1258
    .local v1, "cId":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v13

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    invoke-interface {v4, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    invoke-interface {v11, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1259
    sget-object v13, Lcom/cigna/coach/utils/ContentHelper;->contentInfoMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v4, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    invoke-interface {v13, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1260
    sget-wide v13, Lcom/cigna/coach/utils/ContentHelper;->fromDb:D

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    add-double/2addr v13, v15

    sput-wide v13, Lcom/cigna/coach/utils/ContentHelper;->fromDb:D

    goto :goto_2

    .line 1264
    .end local v1    # "cId":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    .end local v2    # "cIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_6
    move-object v11, v4

    .line 1268
    .end local v3    # "contentDBManager":Lcom/cigna/coach/db/ContentInfoDBManager;
    .end local v4    # "contentFromDBMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    :cond_7
    sget-object v13, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 1269
    sget-wide v13, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    sget-wide v15, Lcom/cigna/coach/utils/ContentHelper;->fromCache:D

    sget-wide v17, Lcom/cigna/coach/utils/ContentHelper;->fromDb:D

    add-double v15, v15, v17

    div-double/2addr v13, v15

    const-wide/high16 v15, 0x4059000000000000L    # 100.0

    mul-double v9, v13, v15

    .line 1270
    .local v9, "percHit":D
    sget-object v13, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Cache hit:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {v9, v10}, Ljava/lang/Math;->round(D)J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "%"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v9    # "percHit":D
    :cond_8
    move-object v12, v11

    .line 1272
    .restart local v12    # "returnContentInfoMap":Ljava/lang/Object;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    goto/16 :goto_0
.end method

.method public static getContentInfoFromCache(I)Lcom/cigna/coach/dataobjects/ContentInfo;
    .locals 3
    .param p0, "contentId"    # I

    .prologue
    .line 461
    sget-object v1, Lcom/cigna/coach/utils/ContentHelper;->contentInfoMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 462
    .local v0, "cInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    if-nez v0, :cond_0

    .line 463
    new-instance v0, Lcom/cigna/coach/dataobjects/ContentInfo;

    .end local v0    # "cInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    invoke-direct {v0, p0}, Lcom/cigna/coach/dataobjects/ContentInfo;-><init>(I)V

    .line 464
    .restart local v0    # "cInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    sget-object v1, Lcom/cigna/coach/utils/ContentHelper;->contentInfoMap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    :cond_0
    return-object v0
.end method

.method private getContentStringForIdAndLocale(Lcom/cigna/coach/dataobjects/ContentInfo;Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/Object;Ljava/util/Map;Lcom/cigna/coach/utils/ContextData;)Ljava/lang/String;
    .locals 7
    .param p1, "contentInfoAllLocale"    # Lcom/cigna/coach/dataobjects/ContentInfo;
    .param p2, "locale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .param p3, "obj"    # Ljava/lang/Object;
    .param p5, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/ContentInfo;",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;",
            "Lcom/cigna/coach/utils/ContextData;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 1021
    .local p4, "tagMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    invoke-virtual {p1, p2}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentForLocale(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v1

    .line 1022
    .local v1, "contentString":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 1023
    const-string v1, ""

    :cond_0
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 1026
    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/ContentHelper;->replacePlaceholdersInText(Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/Object;Ljava/util/Map;Lcom/cigna/coach/utils/ContextData;)Ljava/lang/String;

    move-result-object v6

    .line 1027
    .local v6, "finalString":Ljava/lang/String;
    return-object v6
.end method

.method private replacePlaceholdersInText(Ljava/lang/String;Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/Object;Ljava/util/Map;Lcom/cigna/coach/utils/ContextData;)Ljava/lang/String;
    .locals 24
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "locale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .param p3, "obj"    # Ljava/lang/Object;
    .param p5, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;",
            "Lcom/cigna/coach/utils/ContextData;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 887
    .local p4, "tagMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    if-eqz p1, :cond_13

    .line 888
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/ContentHelper;->tagPattern:Ljava/util/regex/Pattern;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v13

    .line 889
    .local v13, "matcher":Ljava/util/regex/Matcher;
    :goto_0
    invoke-virtual {v13}, Ljava/util/regex/Matcher;->find()Z

    move-result v20

    if-eqz v20, :cond_14

    .line 890
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v14

    .line 891
    .local v14, "tag":Ljava/lang/String;
    const/16 v20, 0x2

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v20

    new-instance v21, Ljava/util/Locale;

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getLanguageCode()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getCountryCode()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v21 .. v23}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v17

    .line 893
    .local v17, "tagName":Ljava/lang/String;
    const/16 v18, 0x0

    .line 894
    .local v18, "tagValue":Ljava/lang/Object;
    const-string v19, ""

    .line 896
    .local v19, "tagValueText":Ljava/lang/String;
    const-string v20, "${NEW_LINE}"

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 897
    const-string v19, "\n"

    .line 994
    .end local v18    # "tagValue":Ljava/lang/Object;
    :cond_0
    :goto_1
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 995
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Replacing "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " with value "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " for locale:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    :cond_1
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v14, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 999
    goto/16 :goto_0

    .line 898
    .restart local v18    # "tagValue":Ljava/lang/Object;
    :cond_2
    const-string v20, "${ARTICLE_URL}"

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 899
    const/16 v20, 0x1330

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/ContentHelper;->getContentTextForCurrentLocale(ILcom/cigna/coach/utils/ContextData;)Ljava/lang/String;

    move-result-object v3

    .line 900
    .local v3, "articleUrl":Ljava/lang/String;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_3

    .line 901
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v19

    goto :goto_1

    .line 903
    :cond_3
    sget-object v19, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    goto :goto_1

    .line 906
    .end local v3    # "articleUrl":Ljava/lang/String;
    :cond_4
    if-eqz p4, :cond_b

    .line 907
    move-object/from16 v0, p4

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/reflect/Method;

    .line 908
    .local v16, "tagMethod":Ljava/lang/reflect/Method;
    if-eqz v16, :cond_a

    .line 909
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 910
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Getting value of tag "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " for locale:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " from "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v16 .. v16}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    :cond_5
    :try_start_0
    const-class v20, Lcom/cigna/coach/utils/CoachContentTagValue;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v15

    check-cast v15, Lcom/cigna/coach/utils/CoachContentTagValue;

    .line 916
    .local v15, "tagAnnotation":Lcom/cigna/coach/utils/CoachContentTagValue;
    invoke-interface {v15}, Lcom/cigna/coach/utils/CoachContentTagValue;->tagValueType()Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    move-result-object v20

    sget-object v21, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    invoke-virtual/range {v20 .. v21}, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 917
    const/16 v20, 0x0

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v18

    .line 941
    .end local v15    # "tagAnnotation":Lcom/cigna/coach/utils/CoachContentTagValue;
    .end local v16    # "tagMethod":Ljava/lang/reflect/Method;
    .end local v18    # "tagValue":Ljava/lang/Object;
    :cond_6
    :goto_2
    if-nez v18, :cond_d

    .line 942
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 943
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Looking for tagname in context data"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    :cond_7
    if-eqz p5, :cond_c

    .line 946
    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/ContextData$ContextDataType;->valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/ContextData$ContextDataType;

    move-result-object v20

    move-object/from16 v0, p5

    move-object/from16 v1, v20

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/ContextData;->getResolvedContent(Lcom/cigna/coach/utils/ContextData$ContextDataType;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v18

    .line 947
    .local v18, "tagValue":Ljava/lang/String;
    if-nez v18, :cond_15

    .line 949
    invoke-virtual/range {p5 .. p5}, Lcom/cigna/coach/utils/ContextData;->getContentIdsToResolve()Ljava/util/Map;

    move-result-object v20

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/ContextData$ContextDataType;->valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/ContextData$ContextDataType;

    move-result-object v21

    invoke-interface/range {v20 .. v21}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 951
    .local v6, "contentIdsToResolve":Ljava/lang/Object;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v6, v1}, Lcom/cigna/coach/utils/ContentHelper;->resolveContentIdObjectToString(Ljava/lang/Object;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v5, v18

    .line 960
    .end local v6    # "contentIdsToResolve":Ljava/lang/Object;
    .end local v18    # "tagValue":Ljava/lang/String;
    :goto_3
    if-eqz v5, :cond_0

    .line 961
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->isArray()Z

    move-result v20

    if-eqz v20, :cond_f

    .line 962
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 963
    .local v4, "b":Ljava/lang/StringBuffer;
    const/4 v8, 0x1

    .line 964
    .local v8, "first":Z
    invoke-static {v5}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v12

    .line 965
    .local v12, "len":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_4
    if-ge v9, v12, :cond_e

    .line 966
    invoke-static {v5, v9}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v11

    .line 967
    .local v11, "item":Ljava/lang/Object;
    if-nez v8, :cond_8

    .line 968
    const-string v20, ", "

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 970
    :cond_8
    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 971
    const/4 v8, 0x0

    .line 965
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 919
    .end local v4    # "b":Ljava/lang/StringBuffer;
    .end local v8    # "first":Z
    .end local v9    # "i":I
    .end local v11    # "item":Ljava/lang/Object;
    .end local v12    # "len":I
    .restart local v15    # "tagAnnotation":Lcom/cigna/coach/utils/CoachContentTagValue;
    .restart local v16    # "tagMethod":Ljava/lang/reflect/Method;
    .local v18, "tagValue":Ljava/lang/Object;
    :cond_9
    const/16 v20, 0x0

    :try_start_1
    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 920
    .restart local v6    # "contentIdsToResolve":Ljava/lang/Object;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v6, v1}, Lcom/cigna/coach/utils/ContentHelper;->resolveContentIdObjectToString(Ljava/lang/Object;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v18

    .local v18, "tagValue":Ljava/lang/String;
    goto/16 :goto_2

    .line 922
    .end local v6    # "contentIdsToResolve":Ljava/lang/Object;
    .end local v15    # "tagAnnotation":Lcom/cigna/coach/utils/CoachContentTagValue;
    .local v18, "tagValue":Ljava/lang/Object;
    :catch_0
    move-exception v7

    .line 923
    .local v7, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Failed to invoke "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 925
    .end local v7    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v7

    .line 926
    .local v7, "e":Ljava/lang/IllegalAccessException;
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Failed to invoke "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 931
    .end local v7    # "e":Ljava/lang/IllegalAccessException;
    :cond_a
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 932
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "No tag method found for tag name."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 936
    .end local v16    # "tagMethod":Ljava/lang/reflect/Method;
    :cond_b
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 937
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Tag map is empty"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 954
    .end local v18    # "tagValue":Ljava/lang/Object;
    :cond_c
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 955
    sget-object v20, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Context data is empty"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    move-object/from16 v5, v18

    goto/16 :goto_3

    .line 973
    .restart local v4    # "b":Ljava/lang/StringBuffer;
    .restart local v8    # "first":Z
    .restart local v9    # "i":I
    .restart local v12    # "len":I
    :cond_e
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    .line 974
    goto/16 :goto_1

    .end local v4    # "b":Ljava/lang/StringBuffer;
    .end local v8    # "first":Z
    .end local v9    # "i":I
    .end local v12    # "len":I
    :cond_f
    instance-of v0, v5, Ljava/util/Collection;

    move/from16 v20, v0

    if-eqz v20, :cond_12

    .line 975
    const/4 v8, 0x1

    .line 976
    .restart local v8    # "first":Z
    check-cast v5, Ljava/util/Collection;

    .line 977
    .local v5, "c":Ljava/util/Collection;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 978
    .restart local v4    # "b":Ljava/lang/StringBuffer;
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_11

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    .line 979
    .restart local v11    # "item":Ljava/lang/Object;
    if-nez v8, :cond_10

    .line 980
    const-string v20, ", "

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 982
    :cond_10
    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 983
    const/4 v8, 0x0

    .line 984
    goto :goto_5

    .line 985
    .end local v11    # "item":Ljava/lang/Object;
    :cond_11
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v19

    .line 987
    goto/16 :goto_1

    .line 988
    .end local v4    # "b":Ljava/lang/StringBuffer;
    .end local v5    # "c":Ljava/util/Collection;
    .end local v8    # "first":Z
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_12
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_1

    .line 1001
    .end local v13    # "matcher":Ljava/util/regex/Matcher;
    .end local v14    # "tag":Ljava/lang/String;
    .end local v17    # "tagName":Ljava/lang/String;
    .end local v19    # "tagValueText":Ljava/lang/String;
    :cond_13
    const-string p1, ""

    .line 1003
    :cond_14
    return-object p1

    .restart local v13    # "matcher":Ljava/util/regex/Matcher;
    .restart local v14    # "tag":Ljava/lang/String;
    .restart local v17    # "tagName":Ljava/lang/String;
    .local v18, "tagValue":Ljava/lang/String;
    .restart local v19    # "tagValueText":Ljava/lang/String;
    :cond_15
    move-object/from16 v5, v18

    goto/16 :goto_3
.end method

.method private resolveContentIdObjectToString(Ljava/lang/Object;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;
    .locals 12
    .param p1, "contentIdsToResolve"    # Ljava/lang/Object;
    .param p2, "locale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 811
    const/4 v8, 0x0

    .line 812
    .local v8, "tagValue":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 813
    instance-of v9, p1, Ljava/lang/Integer;

    if-eqz v9, :cond_4

    .line 814
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "contentIdsToResolve":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 815
    .local v4, "contentIdToResolve":I
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 816
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Tagname need to be resolved from content id:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 819
    :cond_0
    invoke-direct {p0, v4}, Lcom/cigna/coach/utils/ContentHelper;->getContentInfo(I)Lcom/cigna/coach/dataobjects/ContentInfo;

    move-result-object v0

    .line 820
    .local v0, "c":Lcom/cigna/coach/dataobjects/ContentInfo;
    if-eqz v0, :cond_3

    .line 821
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 822
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Content id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " resolved "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    :cond_1
    invoke-virtual {v0, p2}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentForLocale(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v8

    .line 825
    if-nez v8, :cond_2

    .line 826
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Locale:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " specific string nout found for content id:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    .end local v0    # "c":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v4    # "contentIdToResolve":I
    :cond_2
    :goto_0
    return-object v8

    .line 830
    .restart local v0    # "c":Lcom/cigna/coach/dataobjects/ContentInfo;
    .restart local v4    # "contentIdToResolve":I
    :cond_3
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 831
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Content id "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " not found in db"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 835
    .end local v0    # "c":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v4    # "contentIdToResolve":I
    .restart local p1    # "contentIdsToResolve":Ljava/lang/Object;
    :cond_4
    instance-of v9, p1, Ljava/util/Collection;

    if-eqz v9, :cond_8

    .line 836
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 837
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Content ids to resolve is a collection "

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v2, p1

    .line 840
    check-cast v2, Ljava/util/Collection;

    .line 841
    .local v2, "cIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    invoke-direct {p0, v2}, Lcom/cigna/coach/utils/ContentHelper;->getContentInfo(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v5

    .line 842
    .local v5, "contents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v9

    if-lez v9, :cond_2

    .line 844
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, ""

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 845
    .local v7, "sbconCatTagValue":Ljava/lang/StringBuilder;
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 846
    .local v1, "cId":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 847
    .local v3, "cInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-lez v9, :cond_6

    .line 849
    const-string v9, ", "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 852
    :cond_6
    invoke-virtual {v3, p2}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentForLocale(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 854
    .end local v1    # "cId":I
    .end local v3    # "cInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    :cond_7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_0

    .line 858
    .end local v2    # "cIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    .end local v5    # "contents":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "sbconCatTagValue":Ljava/lang/StringBuilder;
    :cond_8
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 859
    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "contentIdsToResolve is not of type integer or collection of integers"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private setContentText(Ljava/lang/Object;Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;Lcom/cigna/coach/utils/ContentHelper$LocaleType;Lcom/cigna/coach/dataobjects/ContentInfo;Ljava/util/Map;Lcom/cigna/coach/utils/ContextData;)V
    .locals 28
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "contentSetInfo"    # Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;
    .param p3, "requestedLocale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .param p4, "contentInfoAllLocale"    # Lcom/cigna/coach/dataobjects/ContentInfo;
    .param p6, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Lcom/cigna/coach/dataobjects/ContentInfo;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Method;",
            ">;",
            "Lcom/cigna/coach/utils/ContextData;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 1043
    .local p5, "tagMethodMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/reflect/Method;>;"
    if-eqz p3, :cond_1

    .line 1045
    :goto_0
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;->method:Ljava/lang/reflect/Method;

    move-object/from16 v24, v0

    .line 1046
    .local v24, "setMethod":Ljava/lang/reflect/Method;
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/cigna/coach/utils/ContentHelper$ContentSetInfo;->contentId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 1048
    .local v15, "contentId":I
    const-class v3, Lcom/cigna/coach/utils/CoachContentId;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/utils/CoachContentId;

    .line 1049
    .local v14, "contentAnnotation":Lcom/cigna/coach/utils/CoachContentId;
    const-class v3, Lcom/cigna/coach/utils/CoachMultiLanguageContentId;

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v21

    check-cast v21, Lcom/cigna/coach/utils/CoachMultiLanguageContentId;

    .line 1051
    .local v21, "multiLanguageAnnotation":Lcom/cigna/coach/utils/CoachMultiLanguageContentId;
    if-eqz v14, :cond_2

    invoke-interface {v14}, Lcom/cigna/coach/utils/CoachContentId;->setTextMethod()Ljava/lang/String;

    move-result-object v26

    .line 1052
    .local v26, "setMethodName":Ljava/lang/String;
    :goto_1
    if-eqz v21, :cond_4

    const/4 v12, 0x1

    .line 1054
    .local v12, "allLanguages":Z
    :goto_2
    if-eqz v26, :cond_d

    const-string/jumbo v3, "none"

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 1056
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1057
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting content using "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " with all language flag :"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Requested locale is:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1061
    :cond_0
    new-instance v23, Lcom/cigna/coach/dataobjects/ContentInfo;

    move-object/from16 v0, v23

    invoke-direct {v0, v15}, Lcom/cigna/coach/dataobjects/ContentInfo;-><init>(I)V

    .line 1063
    .local v23, "setContentValues":Lcom/cigna/coach/dataobjects/ContentInfo;
    if-eqz v12, :cond_5

    .line 1064
    invoke-static {}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->values()[Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v22

    .line 1065
    .local v22, "neededLocaleTypes":[Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    move-object/from16 v13, v22

    .local v13, "arr$":[Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    array-length v0, v13

    move/from16 v20, v0

    .local v20, "len$":I
    const/16 v18, 0x0

    .local v18, "i$":I
    :goto_3
    move/from16 v0, v18

    move/from16 v1, v20

    if-ge v0, v1, :cond_6

    aget-object v5, v13, v18

    .local v5, "locale":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    move-object/from16 v3, p0

    move-object/from16 v4, p4

    move-object/from16 v6, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    .line 1066
    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/ContentHelper;->getContentStringForIdAndLocale(Lcom/cigna/coach/dataobjects/ContentInfo;Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/Object;Ljava/util/Map;Lcom/cigna/coach/utils/ContextData;)Ljava/lang/String;

    move-result-object v17

    .line 1068
    .local v17, "finalString":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-virtual {v0, v5, v1}, Lcom/cigna/coach/dataobjects/ContentInfo;->addContent(Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;)V

    .line 1065
    add-int/lit8 v18, v18, 0x1

    goto :goto_3

    .line 1043
    .end local v5    # "locale":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .end local v12    # "allLanguages":Z
    .end local v13    # "arr$":[Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .end local v14    # "contentAnnotation":Lcom/cigna/coach/utils/CoachContentId;
    .end local v15    # "contentId":I
    .end local v17    # "finalString":Ljava/lang/String;
    .end local v18    # "i$":I
    .end local v20    # "len$":I
    .end local v21    # "multiLanguageAnnotation":Lcom/cigna/coach/utils/CoachMultiLanguageContentId;
    .end local v22    # "neededLocaleTypes":[Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .end local v23    # "setContentValues":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v24    # "setMethod":Ljava/lang/reflect/Method;
    .end local v26    # "setMethodName":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/utils/ContentHelper;->getLocaleFromContext()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object p3

    goto/16 :goto_0

    .line 1051
    .restart local v14    # "contentAnnotation":Lcom/cigna/coach/utils/CoachContentId;
    .restart local v15    # "contentId":I
    .restart local v21    # "multiLanguageAnnotation":Lcom/cigna/coach/utils/CoachMultiLanguageContentId;
    .restart local v24    # "setMethod":Ljava/lang/reflect/Method;
    :cond_2
    if-eqz v21, :cond_3

    invoke-interface/range {v21 .. v21}, Lcom/cigna/coach/utils/CoachMultiLanguageContentId;->setTextMethod()Ljava/lang/String;

    move-result-object v26

    goto/16 :goto_1

    :cond_3
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 1052
    .restart local v26    # "setMethodName":Ljava/lang/String;
    :cond_4
    const/4 v12, 0x0

    goto/16 :goto_2

    .restart local v12    # "allLanguages":Z
    .restart local v23    # "setContentValues":Lcom/cigna/coach/dataobjects/ContentInfo;
    :cond_5
    move-object/from16 v6, p0

    move-object/from16 v7, p4

    move-object/from16 v8, p3

    move-object/from16 v9, p1

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    .line 1071
    invoke-direct/range {v6 .. v11}, Lcom/cigna/coach/utils/ContentHelper;->getContentStringForIdAndLocale(Lcom/cigna/coach/dataobjects/ContentInfo;Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/Object;Ljava/util/Map;Lcom/cigna/coach/utils/ContextData;)Ljava/lang/String;

    move-result-object v17

    .line 1073
    .restart local v17    # "finalString":Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/ContentInfo;->setRequestedLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V

    .line 1074
    move-object/from16 v0, v23

    move-object/from16 v1, p3

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/dataobjects/ContentInfo;->addContent(Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;)V

    .line 1078
    .end local v17    # "finalString":Ljava/lang/String;
    :cond_6
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    .line 1079
    .local v25, "setMethodKey":Ljava/lang/String;
    const/16 v27, 0x0

    .line 1080
    .local v27, "setter":Ljava/lang/reflect/Method;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->setMethodMap:Ljava/util/Map;

    move-object/from16 v0, v25

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v19

    .line 1082
    .local v19, "isSetMethodCache":Z
    if-eqz v19, :cond_8

    .line 1083
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->setMethodMap:Ljava/util/Map;

    move-object/from16 v0, v25

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v27

    .end local v27    # "setter":Ljava/lang/reflect/Method;
    check-cast v27, Ljava/lang/reflect/Method;

    .line 1093
    .restart local v27    # "setter":Ljava/lang/reflect/Method;
    :goto_4
    if-eqz v27, :cond_c

    .line 1094
    if-eqz v12, :cond_a

    .line 1095
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentMap()Ljava/util/Map;

    move-result-object v6

    aput-object v6, v3, v4

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1122
    .end local v19    # "isSetMethodCache":Z
    .end local v23    # "setContentValues":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v25    # "setMethodKey":Ljava/lang/String;
    .end local v27    # "setter":Ljava/lang/reflect/Method;
    :cond_7
    :goto_5
    return-void

    .line 1085
    .restart local v19    # "isSetMethodCache":Z
    .restart local v23    # "setContentValues":Lcom/cigna/coach/dataobjects/ContentInfo;
    .restart local v25    # "setMethodKey":Ljava/lang/String;
    .restart local v27    # "setter":Ljava/lang/reflect/Method;
    :cond_8
    if-eqz v12, :cond_9

    .line 1086
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/util/Map;

    aput-object v7, v4, v6

    move-object/from16 v0, v26

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v27

    .line 1091
    :goto_6
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->setMethodMap:Ljava/util/Map;

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_4

    .line 1106
    .end local v19    # "isSetMethodCache":Z
    .end local v25    # "setMethodKey":Ljava/lang/String;
    .end local v27    # "setter":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v16

    .line 1107
    .local v16, "e":Ljava/lang/NoSuchMethodException;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not find "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1088
    .end local v16    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v19    # "isSetMethodCache":Z
    .restart local v25    # "setMethodKey":Ljava/lang/String;
    .restart local v27    # "setter":Ljava/lang/reflect/Method;
    :cond_9
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v4, v6

    move-object/from16 v0, v26

    invoke-virtual {v3, v0, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v27

    goto :goto_6

    .line 1097
    :cond_a
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1098
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Setting content text:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentForRequestedLocale()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1100
    :cond_b
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentForRequestedLocale()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_5

    .line 1109
    .end local v19    # "isSetMethodCache":Z
    .end local v25    # "setMethodKey":Ljava/lang/String;
    .end local v27    # "setter":Ljava/lang/reflect/Method;
    :catch_1
    move-exception v16

    .line 1110
    .local v16, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to invoke "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1103
    .end local v16    # "e":Ljava/lang/reflect/InvocationTargetException;
    .restart local v19    # "isSetMethodCache":Z
    .restart local v25    # "setMethodKey":Ljava/lang/String;
    .restart local v27    # "setter":Ljava/lang/reflect/Method;
    :cond_c
    :try_start_2
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not find "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " in class:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_5

    .line 1112
    .end local v19    # "isSetMethodCache":Z
    .end local v25    # "setMethodKey":Ljava/lang/String;
    .end local v27    # "setter":Ljava/lang/reflect/Method;
    :catch_2
    move-exception v16

    .line 1113
    .local v16, "e":Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to invoke "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1118
    .end local v16    # "e":Ljava/lang/IllegalAccessException;
    .end local v23    # "setContentValues":Lcom/cigna/coach/dataobjects/ContentInfo;
    :cond_d
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1119
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "No set method defined. Ignoring"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5
.end method


# virtual methods
.method public checkAndLoadContent()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 529
    new-instance v0, Lcom/cigna/coach/db/ContentInfoDBManager;

    iget-object v5, p0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v5}, Lcom/cigna/coach/db/ContentInfoDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 530
    .local v0, "cInfoDbMgr":Lcom/cigna/coach/db/ContentInfoDBManager;
    invoke-virtual {p0}, Lcom/cigna/coach/utils/ContentHelper;->getLocaleFromContext()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v2

    .line 531
    .local v2, "currentContextLocaleType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 532
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Context locale type is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    :cond_0
    invoke-virtual {v0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getCurrentLocaleTypeLoaded()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v4

    .line 536
    .local v4, "dbContentLocaleType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 537
    if-nez v4, :cond_5

    .line 538
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Database locale type is null"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_1
    :goto_0
    if-eqz v4, :cond_2

    invoke-virtual {v2, v4}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 546
    :cond_2
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 547
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Invoking CMS helper to get content"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_3
    new-instance v1, Lcom/cigna/coach/utils/CMSHelper;

    iget-object v5, p0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Lcom/cigna/coach/utils/CMSHelper;-><init>(Landroid/content/Context;)V

    .line 551
    .local v1, "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    invoke-virtual {v1, v2}, Lcom/cigna/coach/utils/CMSHelper;->retrieveContentForLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)[Ljava/lang/String;

    move-result-object v3

    .line 553
    .local v3, "dataForLocale":[Ljava/lang/String;
    if-eqz v3, :cond_6

    array-length v5, v3

    if-lez v5, :cond_6

    .line 554
    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, Lcom/cigna/coach/utils/ContentHelper;->loadContent([Ljava/lang/String;Z)V

    .line 559
    .end local v1    # "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    .end local v3    # "dataForLocale":[Ljava/lang/String;
    :cond_4
    :goto_1
    return-void

    .line 540
    :cond_5
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Database locale type is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 556
    .restart local v1    # "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    .restart local v3    # "dataForLocale":[Ljava/lang/String;
    :cond_6
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "No content returned from CMS"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getContentTextForAvailableLocales(ILcom/cigna/coach/utils/ContextData;)Ljava/util/Map;
    .locals 2
    .param p1, "contentId"    # I
    .param p2, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/cigna/coach/utils/ContextData;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 519
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;

    invoke-direct {v0, p1, p2}, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;-><init>(ILcom/cigna/coach/utils/ContextData;)V

    .line 520
    .local v0, "cr":Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;
    invoke-virtual {p0, v0, p2}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;)V

    .line 521
    invoke-virtual {v0}, Lcom/cigna/coach/utils/ContentHelper$AllLocaleContentResolver;->getResolvedContent()Ljava/util/Map;

    move-result-object v1

    return-object v1
.end method

.method public getContentTextForCurrentLocale(ILcom/cigna/coach/utils/ContextData;)Ljava/lang/String;
    .locals 1
    .param p1, "contentId"    # I
    .param p2, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 493
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/cigna/coach/utils/ContentHelper;->getContentTextForLocale(ILcom/cigna/coach/utils/ContentHelper$LocaleType;Lcom/cigna/coach/utils/ContextData;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentTextForLocale(ILcom/cigna/coach/utils/ContentHelper$LocaleType;Lcom/cigna/coach/utils/ContextData;)Ljava/lang/String;
    .locals 2
    .param p1, "contentId"    # I
    .param p2, "locale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .param p3, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 505
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;

    invoke-direct {v0, p1, p3}, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;-><init>(ILcom/cigna/coach/utils/ContextData;)V

    .line 506
    .local v0, "cr":Lcom/cigna/coach/utils/ContentHelper$ContentResolver;
    invoke-virtual {p0, v0, p3, p2}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V

    .line 507
    invoke-virtual {v0}, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;->getResolvedContent()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getLocaleFromContext()Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 4

    .prologue
    .line 475
    iget-object v3, p0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 476
    .local v0, "ctx":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v1, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 477
    .local v1, "locale":Ljava/util/Locale;
    invoke-static {v1}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(Ljava/util/Locale;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v2

    .line 478
    .local v2, "returnType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    if-nez v2, :cond_0

    .line 479
    invoke-static {}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getDefault()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v2

    .line 482
    :cond_0
    return-object v2
.end method

.method public isCurrentLocaleType(Ljava/lang/String;)Z
    .locals 8
    .param p1, "archiveFileName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v7, -0x1

    .line 569
    if-eqz p1, :cond_0

    .line 570
    const-string v6, "_"

    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v6

    add-int/lit8 v3, v6, 0x1

    .line 571
    .local v3, "indexOfLangCode":I
    if-le v3, v7, :cond_0

    .line 572
    const-string v6, "_"

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v6

    add-int/lit8 v2, v6, 0x1

    .line 573
    .local v2, "indexOfCountryCode":I
    if-le v2, v7, :cond_0

    .line 574
    add-int/lit8 v6, v3, 0x2

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 575
    .local v4, "lang":Ljava/lang/String;
    add-int/lit8 v6, v2, 0x2

    invoke-virtual {p1, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 576
    .local v0, "country":Ljava/lang/String;
    new-instance v6, Ljava/util/Locale;

    invoke-direct {v6, v4, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(Ljava/util/Locale;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v1

    .line 577
    .local v1, "fileLocaleType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cigna/coach/utils/ContentHelper;->getLocaleFromContext()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 578
    const/4 v5, 0x1

    .line 583
    .end local v0    # "country":Ljava/lang/String;
    .end local v1    # "fileLocaleType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .end local v2    # "indexOfCountryCode":I
    .end local v3    # "indexOfLangCode":I
    .end local v4    # "lang":Ljava/lang/String;
    :cond_0
    return v5
.end method

.method public loadContent([Ljava/lang/String;Z)V
    .locals 3
    .param p1, "data"    # [Ljava/lang/String;
    .param p2, "multiLanguage"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 596
    sget-object v1, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 597
    if-eqz p2, :cond_2

    .line 598
    sget-object v1, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Loading new set of multi language content"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_0
    :goto_0
    new-instance v0, Lcom/cigna/coach/db/ContentInfoDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/ContentHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/ContentInfoDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 604
    .local v0, "dbm":Lcom/cigna/coach/db/ContentInfoDBManager;
    if-eqz p2, :cond_3

    .line 605
    invoke-virtual {v0}, Lcom/cigna/coach/db/ContentInfoDBManager;->deleteMultiLanguageContent()V

    .line 610
    :goto_1
    invoke-virtual {v0, p1, p2}, Lcom/cigna/coach/db/ContentInfoDBManager;->insertContent([Ljava/lang/String;Z)V

    .line 612
    sget-object v1, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 613
    sget-object v1, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Loading complete"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    :cond_1
    return-void

    .line 600
    .end local v0    # "dbm":Lcom/cigna/coach/db/ContentInfoDBManager;
    :cond_2
    sget-object v1, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Loading new set of language specific content"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 607
    .restart local v0    # "dbm":Lcom/cigna/coach/db/ContentInfoDBManager;
    :cond_3
    invoke-virtual {v0}, Lcom/cigna/coach/db/ContentInfoDBManager;->deleteNonMultiLanguageContent()V

    goto :goto_1
.end method

.method public setContent(Ljava/lang/Object;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 625
    invoke-virtual {p0, p1, v0, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V

    .line 626
    return-void
.end method

.method public setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;)V
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 637
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V

    .line 638
    return-void
.end method

.method public setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;
    .param p2, "contextData"    # Lcom/cigna/coach/utils/ContextData;
    .param p3, "requestedLocale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/cigna/coach/utils/ContentHelper;->checkAndLoadContent()V

    .line 652
    instance-of v3, p1, Ljava/util/List;

    if-eqz v3, :cond_1

    .line 653
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 654
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Incoming object is a list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v1, p1

    .line 656
    check-cast v1, Ljava/util/List;

    .line 657
    .local v1, "l":Ljava/util/List;
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 658
    .local v2, "o":Ljava/lang/Object;
    invoke-direct {p0, v2, p2, p3}, Lcom/cigna/coach/utils/ContentHelper;->fillContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V

    goto :goto_0

    .line 661
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "l":Ljava/util/List;
    .end local v2    # "o":Ljava/lang/Object;
    :cond_1
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 662
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Incoming object is an instance of object"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/cigna/coach/utils/ContentHelper;->fillContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V

    .line 666
    :cond_3
    return-void
.end method
