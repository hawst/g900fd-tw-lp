.class public Lcom/cigna/coach/utils/WidgetHelper;
.super Ljava/lang/Object;
.source "WidgetHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/WidgetHelper$1;
    }
.end annotation


# static fields
.field private static final CATCH_ALL_UNIQUE_KEY:Ljava/lang/String; = "CATCH_ALL"

.field private static final CIGNA_LOGO_URI:Ljava/lang/String; = "drawable/cignalogo"

.field private static final CLASS_NAME:Ljava/lang/String;

.field public static final COACH_LAUNCH_APP_WIDGET_ACTIVITY:Ljava/lang/String; = "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

.field public static final COACH_WIDGET_ACTIVITY_ACTION:Ljava/lang/String; = "com.sec.shealth.action.COACH"

.field private static final GOAL_COMPLETED_UNIQUE_KEY:Ljava/lang/String; = "GOAL_COMPLETED"

.field private static GOAL_UNIQUE_KEY_PREFIX:Ljava/lang/String; = null

.field private static final MISSION_COMPLETED_UNIQUE_KEY:Ljava/lang/String; = "MISSION_COMPLETED"

.field private static MISSION_UNIQUE_KEY_PREFIX:Ljava/lang/String; = null

.field private static final NO_GOALS_ENROLLED_UNIQUE_KEY:Ljava/lang/String; = "NO_GOALS_ENROLLED"

.field private static final NO_MISSIONS_ENROLLED_UNIQUE_KEY:Ljava/lang/String; = "NO_MISSIONS_ENROLLED"


# instance fields
.field dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "MISSION"

    sput-object v0, Lcom/cigna/coach/utils/WidgetHelper;->MISSION_UNIQUE_KEY_PREFIX:Ljava/lang/String;

    .line 45
    const-string v0, "GOAL"

    sput-object v0, Lcom/cigna/coach/utils/WidgetHelper;->GOAL_UNIQUE_KEY_PREFIX:Ljava/lang/String;

    .line 47
    const-class v0, Lcom/cigna/coach/utils/WidgetHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 51
    iput-object p1, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 52
    return-void
.end method

.method private static createIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "widgetInfoType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .prologue
    const/4 v3, 0x1

    .line 83
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createIntent: START : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    const-string v0, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const-string/jumbo v0, "widgetActivityPackage"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const-string/jumbo v0, "widgetActivityAction"

    const-string v1, "com.sec.shealth.action.COACH"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string v0, "launchWidget"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 90
    const v0, 0x10008000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 92
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 114
    :goto_0
    :pswitch_0
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createIntent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_1
    return-object p1

    .line 94
    :pswitch_1
    const-string v0, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 97
    :pswitch_2
    const-string v0, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private dismissNotificatiosByKey(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "uniqueKey"    # Ljava/lang/String;

    .prologue
    .line 420
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 421
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----dismissNotificatiosByKey("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : START-----"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/WidgetNotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 425
    .local v0, "widgetNotificationDBManager":Lcom/cigna/coach/db/WidgetNotificationDBManager;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->removeNotification(Ljava/lang/String;Ljava/util/List;)V

    .line 426
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 427
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----dismissNotificatiosByKey("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : END-----"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_1
    return-void
.end method

.method private goalUniqueKey(Lcom/cigna/coach/dataobjects/UserGoalData;)Ljava/lang/String;
    .locals 3
    .param p1, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    .line 416
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalSequenceId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/cigna/coach/utils/WidgetHelper;->goalUniqueKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private goalUniqueKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "goalId"    # Ljava/lang/String;
    .param p2, "goalSeqId"    # Ljava/lang/String;

    .prologue
    .line 407
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----goalUniqueKey("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") : START-----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoalUniqueKey Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/utils/WidgetHelper;->MISSION_UNIQUE_KEY_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->GOAL_UNIQUE_KEY_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private missionUniqueKey(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Ljava/lang/String;
    .locals 3
    .param p1, "mission"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo;

    .prologue
    .line 403
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionSeqId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/cigna/coach/utils/WidgetHelper;->missionUniqueKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private missionUniqueKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "missionId"    # Ljava/lang/String;
    .param p2, "missionSeqId"    # Ljava/lang/String;

    .prologue
    .line 395
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-----missionUniqueKey("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") : START-----"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    sget-object v0, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MissionUnique Key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/cigna/coach/utils/WidgetHelper;->MISSION_UNIQUE_KEY_PREFIX:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->MISSION_UNIQUE_KEY_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static populateWidgetInfo_COACH_NOT_STARTED(Landroid/content/Context;Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 56
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "populateWidgetInfo_COACH_NOT_STARTED: widgetType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_0
    new-instance v0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;-><init>()V

    .line 60
    .local v0, "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    invoke-virtual {v0, p2}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setWidgetType(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V

    .line 61
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 62
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setLifeStyleScore(I)V

    .line 63
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    if-ne p2, v1, :cond_3

    .line 64
    const/16 v1, 0xf0f

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleContentId(I)V

    .line 65
    sget v1, Lcom/cigna/coach/R$string;->coach_not_started_title_message:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleText(Ljava/lang/String;)V

    .line 66
    const/16 v1, 0xec8

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setMessageTextContentId(I)V

    .line 67
    sget v1, Lcom/cigna/coach/R$string;->coach_not_started_desc_message:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setMessageText(Ljava/lang/String;)V

    .line 73
    :cond_1
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    sget-object v2, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-static {p0, v1, v2}, Lcom/cigna/coach/utils/WidgetHelper;->createIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleIntent(Landroid/content/Intent;)V

    .line 74
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    sget-object v2, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->COACH_NOT_LAUNCHED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-static {p0, v1, v2}, Lcom/cigna/coach/utils/WidgetHelper;->createIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setMessageIntent(Landroid/content/Intent;)V

    .line 75
    const-string v1, "drawable/cignalogo"

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setCignaLogoImageURI(Ljava/lang/String;)V

    .line 76
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "populateWidgetInfo_COACH_NOT_STARTED: END: widgetType:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    :cond_2
    return-object v0

    .line 68
    :cond_3
    sget-object v1, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    if-ne p2, v1, :cond_1

    .line 69
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleText(Ljava/lang/String;)V

    .line 70
    const/16 v1, 0x19c8

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setMessageTextContentId(I)V

    .line 71
    sget v1, Lcom/cigna/coach/R$string;->coach_shealth_not_started_message:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setMessageText(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public cleanupWidgetNotifications(Ljava/lang/String;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 318
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----cleanupWidgetNotifications("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : START-----"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/WidgetNotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 323
    .local v0, "widgetNotificationDBManager":Lcom/cigna/coach/db/WidgetNotificationDBManager;
    invoke-virtual {v0, p1}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->removeAllNotifications(Ljava/lang/String;)V

    .line 324
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 325
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----cleanupWidgetNotifications("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : END-----"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_1
    return-void
.end method

.method public dismissGoalNotifications(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    .line 437
    invoke-direct {p0, p2}, Lcom/cigna/coach/utils/WidgetHelper;->goalUniqueKey(Lcom/cigna/coach/dataobjects/UserGoalData;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/cigna/coach/utils/WidgetHelper;->dismissNotificatiosByKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    return-void
.end method

.method public dismissMissionNotifications(Ljava/lang/String;Lcom/cigna/coach/apiobjects/GoalMissionInfo;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "mission"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo;

    .prologue
    .line 433
    invoke-direct {p0, p2}, Lcom/cigna/coach/utils/WidgetHelper;->missionUniqueKey(Lcom/cigna/coach/apiobjects/GoalMissionInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/cigna/coach/utils/WidgetHelper;->dismissNotificatiosByKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    return-void
.end method

.method public dismissWidgetNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "type"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .prologue
    .line 294
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----dismissWidgetNotification("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : START-----"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/WidgetNotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 299
    .local v0, "widgetNotificationDBManager":Lcom/cigna/coach/db/WidgetNotificationDBManager;
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 311
    :goto_0
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 312
    sget-object v1, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "-----dismissWidgetNotification("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : END-----"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_1
    return-void

    .line 305
    :pswitch_0
    invoke-virtual {v0, p1}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->removeNotification(Ljava/lang/String;)V

    goto :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public getWidgetInfo(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 123
    new-instance v2, Lcom/cigna/coach/db/LifeStyleDBManager;

    iget-object v9, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v2, v9}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 124
    .local v2, "lfDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    invoke-virtual {v2, p1}, Lcom/cigna/coach/db/LifeStyleDBManager;->getNoOfAssessmentScores(Ljava/lang/String;)I

    move-result v3

    .line 126
    .local v3, "noOfScores":I
    sget-object v9, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 127
    sget-object v9, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "noOfScores: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_0
    if-gtz v3, :cond_2

    .line 131
    iget-object v9, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9, p1, p2}, Lcom/cigna/coach/utils/WidgetHelper;->populateWidgetInfo_COACH_NOT_STARTED(Landroid/content/Context;Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/AndroidWidgetData;

    move-result-object v0

    .line 172
    :cond_1
    :goto_0
    return-object v0

    .line 135
    :cond_2
    invoke-virtual {p0, p1, v3}, Lcom/cigna/coach/utils/WidgetHelper;->populateWidgetNotifications(Ljava/lang/String;I)V

    .line 137
    new-instance v0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;-><init>()V

    .line 138
    .local v0, "androidWidgetInfo":Lcom/cigna/coach/dataobjects/AndroidWidgetData;
    invoke-virtual {v0, p2}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setWidgetType(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V

    .line 139
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v9

    invoke-virtual {v2, p1, v9}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserOverallScore(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/ScoresData;

    move-result-object v5

    .line 140
    .local v5, "soresData":Lcom/cigna/coach/dataobjects/ScoresData;
    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/ScoresData;->getOverallScore()I

    move-result v4

    .line 142
    .local v4, "overallScore":I
    const-string v9, "drawable/cignalogo"

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setCignaLogoImageURI(Ljava/lang/String;)V

    .line 143
    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setLifeStyleScore(I)V

    .line 145
    sget-object v9, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 146
    sget-object v9, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "overallScore: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_3
    new-instance v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;

    iget-object v9, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v7, v9}, Lcom/cigna/coach/db/WidgetNotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 149
    .local v7, "widgetNotificationDBManager":Lcom/cigna/coach/db/WidgetNotificationDBManager;
    invoke-virtual {v7, p1, p2}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/WidgetNotificationData;

    move-result-object v8

    .line 152
    .local v8, "widgetNotificationData":Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    if-eqz v8, :cond_1

    .line 153
    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 154
    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getCoachMessageData()Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-result-object v1

    .line 156
    .local v1, "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 157
    .local v6, "titleIntent":Landroid/content/Intent;
    if-lez v4, :cond_4

    .line 158
    const/16 v9, 0x137d

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleContentId(I)V

    .line 159
    iget-object v9, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v10

    invoke-static {v9, v6, v10}, Lcom/cigna/coach/utils/WidgetHelper;->createIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleIntent(Landroid/content/Intent;)V

    .line 166
    :goto_1
    if-eqz v1, :cond_1

    .line 167
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDescriptionContentId()I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setMessageTextContentId(I)V

    .line 168
    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getIntent()Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setMessageIntent(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 161
    :cond_4
    const/16 v9, 0x139e

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleContentId(I)V

    .line 162
    iget-object v9, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v10

    invoke-static {v9, v6, v10}, Lcom/cigna/coach/utils/WidgetHelper;->createIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->setTitleIntent(Landroid/content/Intent;)V

    .line 163
    const-string v9, "EXTRA_NAME_FORCED_CIGNA_SCORE_SCREEN"

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method

.method public populateWidgetNotifications(Ljava/lang/String;I)V
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "noOfScores"    # I

    .prologue
    .line 331
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 332
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "-----populateWidgetNotifications("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") : START-----"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :cond_0
    new-instance v3, Lcom/cigna/coach/db/MissionDBManager;

    iget-object v8, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v3, v8}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 337
    .local v3, "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    new-instance v1, Lcom/cigna/coach/utils/CoachMessageHelper;

    iget-object v8, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 338
    .local v1, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    new-instance v2, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v8, p0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v2, v8}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 343
    .local v2, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 344
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "noOfScores: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_1
    invoke-virtual {v2, p1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalsCount(Ljava/lang/String;)I

    move-result v4

    .line 348
    .local v4, "noOfActiveGoals":I
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 349
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "noOfActiveGoals: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :cond_2
    invoke-virtual {v3, p1}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionsCount(Ljava/lang/String;)I

    move-result v5

    .line 353
    .local v5, "noOfActiveMissions":I
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 354
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "noOfActiveMissions: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 358
    .local v6, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    if-lez p2, :cond_5

    if-nez v4, :cond_5

    .line 359
    sget-object v8, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v1, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getNoGoalsEnrolledMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 360
    .local v0, "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v8, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v1, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getNoGoalsEnrolledMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 362
    .local v7, "shealthMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-static {v0}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 363
    invoke-static {v7}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    invoke-virtual {p0, p1}, Lcom/cigna/coach/utils/WidgetHelper;->cleanupWidgetNotifications(Ljava/lang/String;)V

    .line 365
    sget-object v8, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_GOALS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {p0, p1, v6, v8}, Lcom/cigna/coach/utils/WidgetHelper;->setWidgetNotifications(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 386
    :goto_0
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 387
    sget-object v8, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "-----populateWidgetNotifications("

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ") : END-----"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_4
    return-void

    .line 366
    .end local v0    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .end local v7    # "shealthMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_5
    if-lez p2, :cond_6

    if-lez v4, :cond_6

    if-nez v5, :cond_6

    .line 367
    sget-object v8, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v1, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getNoMissionsEnrolledMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 368
    .restart local v0    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v8, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v1, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getNoMissionsEnrolledMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 370
    .restart local v7    # "shealthMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-static {v0}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    invoke-static {v7}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    invoke-virtual {p0, p1}, Lcom/cigna/coach/utils/WidgetHelper;->cleanupWidgetNotifications(Ljava/lang/String;)V

    .line 373
    sget-object v8, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->NO_MISSIONS_ENROLLED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {p0, p1, v6, v8}, Lcom/cigna/coach/utils/WidgetHelper;->setWidgetNotifications(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    goto :goto_0

    .line 375
    .end local v0    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .end local v7    # "shealthMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :cond_6
    const-string v8, "NO_GOALS_ENROLLED"

    invoke-direct {p0, p1, v8}, Lcom/cigna/coach/utils/WidgetHelper;->dismissNotificatiosByKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const-string v8, "NO_MISSIONS_ENROLLED"

    invoke-direct {p0, p1, v8}, Lcom/cigna/coach/utils/WidgetHelper;->dismissNotificatiosByKey(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    sget-object v8, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v1, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCatchAllMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 379
    .restart local v0    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v8, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v1, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCatchAllMessage(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 381
    .restart local v7    # "shealthMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-static {v0}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    invoke-static {v7}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    sget-object v8, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->CATCH_ALL:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    invoke-virtual {p0, p1, v6, v8}, Lcom/cigna/coach/utils/WidgetHelper;->setWidgetNotifications(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    goto/16 :goto_0
.end method

.method public setWidgetNotifications(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .locals 20
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "widgetInfoType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;",
            "Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 178
    .local p2, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 179
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "setWidgetNotifications : START widgetInfoType: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_0
    new-instance v16, Ljava/util/ArrayList;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v17

    invoke-direct/range {v16 .. v17}, Ljava/util/ArrayList;-><init>(I)V

    .line 182
    .local v16, "widgetNotificationDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/WidgetNotificationData;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v13

    .line 184
    .local v13, "userKey":I
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 185
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Converting "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " NotificationDatas to WidgetNotificationDatas"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_1
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_b

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/cigna/coach/apiobjects/Notification;

    .line 188
    .local v10, "notification":Lcom/cigna/coach/apiobjects/Notification;
    if-eqz v10, :cond_2

    move-object v11, v10

    .line 191
    check-cast v11, Lcom/cigna/coach/dataobjects/NotificationData;

    .line 192
    .local v11, "notificationData":Lcom/cigna/coach/dataobjects/NotificationData;
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 193
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Converting notification: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getCoachMessageId()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "; type: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_3
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v17

    sget-object v18, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->COACH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_4

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v17

    sget-object v18, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->SHEALTH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_4

    .line 199
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 200
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Not a widget notification; skipping"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 205
    :cond_4
    new-instance v15, Lcom/cigna/coach/dataobjects/WidgetNotificationData;

    invoke-direct {v15}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;-><init>()V

    .line 207
    .local v15, "widgetNotificationData":Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    new-instance v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V

    .line 208
    .local v3, "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-virtual {v15, v3}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setCoachMessageData(Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 209
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getCoachMessageId()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setCoachMessageId(I)V

    .line 210
    invoke-virtual/range {p3 .. p3}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->getWidgetInfoType()I

    move-result v17

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setDisplayRank(I)V

    .line 211
    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 213
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v17

    sget-object v18, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->COACH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_5

    .line 214
    sget-object v17, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->COACH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetType(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V

    .line 215
    :cond_5
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationType()Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-result-object v17

    sget-object v18, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->SHEALTH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 216
    sget-object v17, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->SHEALTH_WIDGET:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetType(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V

    .line 218
    :cond_6
    invoke-virtual {v15, v13}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUserId(I)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v17

    const-string v18, "com.sec.shealth.broadcast.LAUNCH_APP_WIDGET_ACTIVITY"

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getDeepLinkInfo()Ljava/util/Map;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/mobile/coach/notification/NotificationEngine;->createDeepLinkIntent(Ljava/lang/String;Ljava/util/Map;)Landroid/content/Intent;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/cigna/coach/utils/WidgetHelper;->createIntent(Landroid/content/Context;Landroid/content/Intent;Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)Landroid/content/Intent;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setIntent(Landroid/content/Intent;)V

    .line 222
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 223
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "widgetInfoType : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " , WidgetType: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getWidgetType()Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_7
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper$1;->$SwitchMap$com$cigna$coach$interfaces$IWidget$WidgetInfoType:[I

    invoke-virtual/range {p3 .. p3}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_0

    .line 276
    :cond_8
    :goto_1
    :pswitch_0
    const/16 v17, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-interface {v0, v1, v15}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 228
    :pswitch_1
    const-string v17, "GOAL_COMPLETED"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    .line 229
    sget-object v17, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    goto :goto_1

    .line 232
    :pswitch_2
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 233
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "coachMessageData.getContextFilterType() : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_9
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v17

    sget-object v18, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 237
    const-string v17, "GOAL_COMPLETED"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    .line 238
    sget-object v17, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    goto :goto_1

    .line 240
    :cond_a
    const-string v17, "MISSION_COMPLETED"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    .line 241
    sget-object v17, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->MISSION_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    goto :goto_1

    .line 245
    :pswitch_3
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getDeepLinkInfo()Ljava/util/Map;

    move-result-object v4

    .line 247
    .local v4, "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v4, :cond_8

    .line 248
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "EXTRA_NAME_GOAL_ID"

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 249
    .local v5, "goalId":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "EXTRA_NAME_GOAL_SEQUENCE_ID"

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 250
    .local v6, "goalSeqId":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/cigna/coach/utils/WidgetHelper;->goalUniqueKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 255
    .end local v4    # "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5    # "goalId":Ljava/lang/String;
    .end local v6    # "goalSeqId":Ljava/lang/String;
    :pswitch_4
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/NotificationData;->getDeepLinkInfo()Ljava/util/Map;

    move-result-object v4

    .line 257
    .restart local v4    # "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v4, :cond_8

    .line 258
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "EXTRA_NAME_MISSION_ID"

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 259
    .local v8, "missionId":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "EXTRA_NAME_MISSION_SEQUENCE_ID"

    move-object/from16 v0, v18

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 260
    .local v9, "missionSeqId":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/cigna/coach/utils/WidgetHelper;->missionUniqueKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 265
    .end local v4    # "deepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8    # "missionId":Ljava/lang/String;
    .end local v9    # "missionSeqId":Ljava/lang/String;
    :pswitch_5
    const-string v17, "NO_GOALS_ENROLLED"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 268
    :pswitch_6
    const-string v17, "NO_MISSIONS_ENROLLED"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 271
    :pswitch_7
    const-string v17, "CATCH_ALL"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 279
    .end local v3    # "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .end local v10    # "notification":Lcom/cigna/coach/apiobjects/Notification;
    .end local v11    # "notificationData":Lcom/cigna/coach/dataobjects/NotificationData;
    .end local v15    # "widgetNotificationData":Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    :cond_b
    new-instance v14, Lcom/cigna/coach/db/WidgetNotificationDBManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/WidgetHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v14, v0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 280
    .local v14, "widgetNotificationDBManager":Lcom/cigna/coach/db/WidgetNotificationDBManager;
    sget-object v17, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->GOAL_COMPLETED:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    if-ne v0, v1, :cond_c

    .line 283
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 284
    .local v12, "uniqueKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v17, "MISSION_COMPLETED"

    move-object/from16 v0, v17

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v12}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->removeNotification(Ljava/lang/String;Ljava/util/List;)V

    .line 287
    .end local v12    # "uniqueKeyList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_c
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v14, v0, v1}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->setNotifications(Ljava/lang/String;Ljava/util/List;)V

    .line 288
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_d

    .line 289
    sget-object v17, Lcom/cigna/coach/utils/WidgetHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v19, "setWidgetNotifications : END widgetInfoType: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    :cond_d
    return-void

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
