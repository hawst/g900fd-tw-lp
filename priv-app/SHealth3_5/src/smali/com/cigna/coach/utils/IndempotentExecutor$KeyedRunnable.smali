.class public Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;
.super Ljava/lang/Object;
.source "IndempotentExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/IndempotentExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KeyedRunnable"
.end annotation


# instance fields
.field protected final key:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/Object;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;->key:Ljava/lang/Object;

    .line 43
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "otherObject"    # Ljava/lang/Object;

    .prologue
    .line 60
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 64
    :goto_0
    return v1

    .line 61
    :cond_0
    instance-of v1, p1, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 63
    check-cast v0, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;

    .line 64
    .local v0, "other":Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;
    invoke-virtual {p0}, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;->key()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0}, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;->key()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;->key()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public key()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;->key:Ljava/lang/Object;

    return-object v0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;->key()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
