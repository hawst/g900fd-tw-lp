.class final Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator$1;
.super Ljava/lang/Object;
.source "UserJournalDataAggregator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/dataobjects/UserTableJournalData;Lcom/cigna/coach/dataobjects/UserTableJournalData;)I
    .locals 5
    .param p1, "lhs"    # Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .param p2, "rhs"    # Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .prologue
    .line 62
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTransactionTimeStamp()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTransactionTimeStamp()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->compareIfNotNull(Ljava/lang/Comparable;Ljava/lang/Object;)I

    move-result v0

    .line 63
    .local v0, "ans":I
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getSeqId()J

    move-result-wide v1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getSeqId()J

    move-result-wide v3

    sub-long/2addr v1, v3

    long-to-int v0, v1

    .line 64
    :cond_0
    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 60
    check-cast p1, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator$1;->compare(Lcom/cigna/coach/dataobjects/UserTableJournalData;Lcom/cigna/coach/dataobjects/UserTableJournalData;)I

    move-result v0

    return v0
.end method
