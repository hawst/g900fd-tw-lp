.class public Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;
.super Ljava/lang/Object;
.source "JournalHelper.java"

# interfaces
.implements Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PartialActivityListener"
.end annotation


# instance fields
.field private max:D

.field private min:D

.field private parent:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V
    .locals 0
    .param p1, "parent"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .param p2, "min"    # D
    .param p4, "max"    # D

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->parent:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 185
    iput-wide p2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->min:D

    .line 186
    iput-wide p4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->max:D

    .line 187
    return-void
.end method


# virtual methods
.method public onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    .locals 1
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "isSuccess"    # Z
    .param p3, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->parent:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method public onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    .locals 7
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "fraction"    # D

    .prologue
    .line 191
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->parent:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    iget-wide v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->min:D

    iget-wide v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->max:D

    iget-wide v5, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->min:D

    sub-double/2addr v3, v5

    mul-double/2addr v3, p2

    add-double/2addr v1, v3

    invoke-interface {v0, p1, v1, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 192
    return-void
.end method

.method public onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
    .locals 1
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;->parent:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    invoke-interface {v0, p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V

    .line 197
    return-void
.end method
