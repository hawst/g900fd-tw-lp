.class public Lcom/cigna/coach/utils/CMSHelper;
.super Ljava/lang/Object;
.source "CMSHelper.java"


# static fields
.field private static final ALL_LANGUAGES_CONTENT_ZIP_FILE_NAME:Ljava/lang/String; = "CoachContent_All_Lang.zip"

.field private static final BUFFER_SIZE:I = 0x2000

.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final CONTENT_FILE_NAME:Ljava/lang/String; = "coachcontent"

.field private static final CSV_DELIMITER:Ljava/lang/String; = ";"

.field private static final DIGEST_ALGORITHM:Ljava/lang/String; = "MD5"

.field private static final LOCAL_ZIP_FILES_PATH:Ljava/lang/String; = "CignaCoachContent"

.field private static final MASTER_TABLES_ZIP_FILE_NAME:Ljava/lang/String; = "coachreferencetables.zip"

.field private static final NEXT_LINE_DELIMITER:Ljava/lang/String; = "\n"


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Lcom/cigna/coach/utils/CMSHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    .line 93
    return-void
.end method

.method static cleanStringForContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "inString"    # Ljava/lang/String;

    .prologue
    .line 273
    const-string/jumbo v0, "\ufffd"

    const-string v1, "\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 274
    const-string v0, "\'\'\'"

    const-string v1, "\'"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 275
    const-string/jumbo v0, "\u2012"

    const-string v1, "-"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 276
    const-string/jumbo v0, "\u2013"

    const-string v1, "-"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 277
    const-string/jumbo v0, "\u2014"

    const-string v1, "-"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 278
    const-string/jumbo v0, "\u2015"

    const-string v1, "-"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 279
    const-string v0, "\\|([a-z][a-z])\\|([A-Z][A-Z])\\|"

    const-string/jumbo v1, "|\"$1\"|\"$2\"|"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 282
    return-object p0
.end method

.method private copyRawFileToLocalPath(ILjava/lang/String;)V
    .locals 12
    .param p1, "rid"    # I
    .param p2, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 558
    sget-object v9, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 559
    sget-object v9, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "copyRawFileToLocalPath: START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    :cond_0
    iget-object v9, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-static {v9, p2}, Lcom/cigna/coach/utils/CMSHelper;->getLocalZipFileAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 563
    .local v2, "filePath":Ljava/lang/String;
    sget-object v9, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 564
    sget-object v9, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "copyRawFileToLocalPath: filePath = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    :cond_1
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 570
    .local v8, "zipFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    .line 572
    .local v7, "parent":Ljava/io/File;
    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    move-result v9

    if-nez v9, :cond_2

    .line 573
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Couldn\'t create dir: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 576
    :cond_2
    const/4 v4, 0x0

    .line 577
    .local v4, "myRawResource":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 579
    .local v5, "out":Ljava/io/OutputStream;
    :try_start_0
    iget-object v9, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v4

    .line 580
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582
    .end local v5    # "out":Ljava/io/OutputStream;
    .local v6, "out":Ljava/io/OutputStream;
    const/16 v9, 0x2000

    :try_start_1
    new-array v0, v9, [B

    .line 584
    .local v0, "buf":[B
    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .local v3, "len":I
    if-lez v3, :cond_3

    .line 585
    const/4 v9, 0x0

    invoke-virtual {v6, v0, v9, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 587
    .end local v0    # "buf":[B
    .end local v3    # "len":I
    :catch_0
    move-exception v1

    move-object v5, v6

    .line 588
    .end local v6    # "out":Ljava/io/OutputStream;
    .local v1, "e":Ljava/io/IOException;
    .restart local v5    # "out":Ljava/io/OutputStream;
    :goto_1
    :try_start_2
    sget-object v9, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "copyRawFileToLocalPath: ERROR: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v9, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 591
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    :goto_2
    invoke-static {v5}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    .line 592
    invoke-static {v4}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    throw v9

    .line 591
    .end local v5    # "out":Ljava/io/OutputStream;
    .restart local v0    # "buf":[B
    .restart local v3    # "len":I
    .restart local v6    # "out":Ljava/io/OutputStream;
    :cond_3
    invoke-static {v6}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    .line 592
    invoke-static {v4}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    .line 595
    sget-object v9, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 596
    sget-object v9, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "copyRawFileToLocalPath : END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    :cond_4
    return-void

    .line 591
    .end local v0    # "buf":[B
    .end local v3    # "len":I
    :catchall_1
    move-exception v9

    move-object v5, v6

    .end local v6    # "out":Ljava/io/OutputStream;
    .restart local v5    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 587
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static earlyCoachSQLiteHelper(Landroid/content/Context;ZLandroid/database/sqlite/SQLiteDatabase;)Lcom/cigna/coach/db/CoachSQLiteHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "writable"    # Z
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 84
    if-nez p2, :cond_0

    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, p0, p1}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 85
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/CMSHelper$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p2}, Lcom/cigna/coach/utils/CMSHelper$1;-><init>(Landroid/content/Context;ZLandroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public static formatFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "zipFileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 210
    move-object v0, p0

    .line 211
    .local v0, "returnValue":Ljava/lang/String;
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-le v1, v2, :cond_0

    .line 212
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 214
    :cond_0
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-le v1, v2, :cond_1

    .line 215
    const/4 v1, 0x0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 218
    :cond_1
    return-object v0
.end method

.method private getDataFromLocalZipFile(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 336
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/cigna/coach/utils/CMSHelper;->getLocalZipFileAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/cigna/coach/utils/CMSHelper;->getDataFromZipFile(Ljava/io/File;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static getDataFromZipFile(Ljava/io/File;)Ljava/util/Map;
    .locals 7
    .param p0, "actualFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 340
    const/4 v2, 0x0

    .line 342
    .local v2, "inputStream":Ljava/io/InputStream;
    :try_start_0
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 343
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Trying to read :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 347
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .local v3, "inputStream":Ljava/io/InputStream;
    :try_start_1
    invoke-static {v3}, Lcom/cigna/coach/utils/CMSHelper;->uncompressZipFileContent(Ljava/io/InputStream;)Ljava/util/Map;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 362
    .local v1, "eachZipFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-static {v3}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    move-object v2, v3

    .end local v1    # "eachZipFileMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    :goto_0
    return-object v1

    .line 353
    :cond_1
    const/4 v1, 0x0

    .line 362
    invoke-static {v2}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    goto :goto_0

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_2
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "An error occured while unZipDefaultZipFile"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error is:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v4, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 362
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :goto_2
    invoke-static {v2}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    throw v4

    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    goto :goto_2

    .line 357
    .end local v2    # "inputStream":Ljava/io/InputStream;
    .restart local v3    # "inputStream":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/InputStream;
    .restart local v2    # "inputStream":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private getFileNameForLocale(Ljava/util/Locale;)Ljava/lang/String;
    .locals 2
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 643
    const/4 v0, 0x0

    .line 644
    .local v0, "lt":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    if-nez p1, :cond_0

    .line 646
    invoke-static {}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getDefault()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v0

    .line 651
    :goto_0
    invoke-static {v0}, Lcom/cigna/coach/utils/CMSHelper;->getFileNameForLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 649
    :cond_0
    invoke-static {p1}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(Ljava/util/Locale;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v0

    goto :goto_0
.end method

.method public static getFileNameForLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;
    .locals 4
    .param p0, "lt"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getLanguageCode()Ljava/lang/String;

    move-result-object v1

    .line 638
    .local v1, "languagecode":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 639
    .local v0, "countryCode":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "coachcontent_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".zip"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getLocalZipFileAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 601
    new-instance v0, Landroid/content/ContextWrapper;

    invoke-direct {v0, p0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 604
    .local v0, "contextWrapper":Landroid/content/ContextWrapper;
    if-eqz p1, :cond_0

    .line 605
    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 606
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 607
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "CignaCoachContent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 612
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return-object v2

    :cond_0
    const-string v2, ""

    goto :goto_0
.end method

.method private loadFileContentToDB(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 8
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 160
    iget-object v5, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-static {v5, v6, p1}, Lcom/cigna/coach/utils/CMSHelper;->earlyCoachSQLiteHelper(Landroid/content/Context;ZLandroid/database/sqlite/SQLiteDatabase;)Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v0

    .line 162
    .local v0, "coachSQLiteHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    invoke-direct {p0, p2}, Lcom/cigna/coach/utils/CMSHelper;->getDataFromLocalZipFile(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    .line 164
    .local v4, "fileContentMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    .line 166
    const-string v5, "coachcontent"

    invoke-virtual {p2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 167
    new-instance v1, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v1, v0}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 169
    .local v1, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 170
    .local v2, "data":[Ljava/lang/String;
    const-string v5, "CoachContent_All_Lang.zip"

    invoke-virtual {p2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 171
    const/4 v5, 0x1

    invoke-virtual {v1, v2, v5}, Lcom/cigna/coach/utils/ContentHelper;->loadContent([Ljava/lang/String;Z)V

    .line 180
    .end local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v2    # "data":[Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 188
    return-void

    .line 172
    .restart local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v2    # "data":[Ljava/lang/String;
    :cond_1
    :try_start_1
    invoke-virtual {v1, p2}, Lcom/cigna/coach/utils/ContentHelper;->isCurrentLocaleType(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 173
    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Lcom/cigna/coach/utils/ContentHelper;->loadContent([Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 181
    .end local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v2    # "data":[Ljava/lang/String;
    .end local v4    # "fileContentMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 182
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v5, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "An error occured while loadFileContentToDB"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v5, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    new-instance v5, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v5, v3}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 186
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v5

    .line 176
    .restart local v4    # "fileContentMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    :cond_2
    :try_start_3
    invoke-virtual {v0, v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->loadData(Ljava/util/Map;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private rawZipFileIds()Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 391
    sget-object v6, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 392
    sget-object v6, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "rawZipFileIds: START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 395
    .local v0, "ans":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const-class v6, Lcom/cigna/coach/R$raw;

    invoke-virtual {v6}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 398
    .local v3, "fields":[Ljava/lang/reflect/Field;
    const/4 v1, 0x0

    .local v1, "count":I
    :goto_0
    :try_start_0
    array-length v6, v3

    if-ge v1, v6, :cond_1

    .line 399
    aget-object v6, v3, v1

    aget-object v7, v3, v1

    invoke-virtual {v6, v7}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    .line 400
    .local v5, "rid":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v7, v3, v1

    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".zip"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 401
    .local v4, "filename":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 403
    .end local v4    # "filename":Ljava/lang/String;
    .end local v5    # "rid":I
    :catch_0
    move-exception v2

    .line 404
    .local v2, "e":Ljava/lang/IllegalAccessException;
    sget-object v6, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "rawZipFileIds: ERROR: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    new-instance v6, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v6, v2}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 408
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :cond_1
    sget-object v6, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 409
    sget-object v6, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "rawZipFileIds: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_2
    return-object v0
.end method

.method private static uncompressZipFileContent(Ljava/io/InputStream;)Ljava/util/Map;
    .locals 13
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 222
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 223
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v11, "uncompressText"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :cond_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 227
    .local v7, "unzippedContentMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    :try_start_0
    new-instance v9, Ljava/util/zip/ZipInputStream;

    invoke-direct {v9, p0}, Ljava/util/zip/ZipInputStream;-><init>(Ljava/io/InputStream;)V

    .line 231
    .local v9, "zipInputStream":Ljava/util/zip/ZipInputStream;
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v8

    .local v8, "zipEntry":Ljava/util/zip/ZipEntry;
    :goto_0
    if-eqz v8, :cond_6

    .line 232
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 233
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Extracting: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "..."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_1
    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_5

    .line 236
    invoke-virtual {v8}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    .line 237
    .local v6, "tableName":Ljava/lang/String;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 239
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    const/16 v10, 0x2000

    new-array v3, v10, [B

    .line 241
    .local v3, "data":[B
    :goto_1
    const/4 v10, 0x0

    array-length v11, v3

    invoke-virtual {v9, v3, v10, v11}, Ljava/util/zip/ZipInputStream;->read([BII)I

    move-result v2

    .local v2, "bytesRead":I
    const/4 v10, -0x1

    if-eq v2, v10, :cond_2

    .line 242
    const/4 v10, 0x0

    invoke-virtual {v0, v3, v10, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 264
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "bytesRead":I
    .end local v3    # "data":[B
    .end local v6    # "tableName":Ljava/lang/String;
    .end local v8    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v9    # "zipInputStream":Ljava/util/zip/ZipInputStream;
    :catch_0
    move-exception v4

    .line 265
    .local v4, "e":Ljava/lang/Exception;
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "An error occured while unzipping the content"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error is:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    new-instance v10, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v10, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    .line 244
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "bytesRead":I
    .restart local v3    # "data":[B
    .restart local v6    # "tableName":Ljava/lang/String;
    .restart local v8    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v9    # "zipInputStream":Ljava/util/zip/ZipInputStream;
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 245
    .local v1, "bytes":[B
    new-instance v5, Ljava/lang/String;

    const-string v10, "UTF-8"

    invoke-direct {v5, v1, v10}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 247
    .local v5, "eachFileString":Ljava/lang/String;
    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    .line 248
    invoke-static {v6}, Lcom/cigna/coach/utils/CMSHelper;->formatFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 249
    const-string v10, "dbcreate"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 250
    const-string v10, ";"

    invoke-virtual {v5, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v6, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "bytes":[B
    .end local v2    # "bytesRead":I
    .end local v3    # "data":[B
    .end local v5    # "eachFileString":Ljava/lang/String;
    .end local v6    # "tableName":Ljava/lang/String;
    :cond_3
    :goto_2
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->getNextEntry()Ljava/util/zip/ZipEntry;

    move-result-object v8

    goto/16 :goto_0

    .line 252
    .restart local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "bytes":[B
    .restart local v2    # "bytesRead":I
    .restart local v3    # "data":[B
    .restart local v5    # "eachFileString":Ljava/lang/String;
    .restart local v6    # "tableName":Ljava/lang/String;
    :cond_4
    invoke-static {v5}, Lcom/cigna/coach/utils/CMSHelper;->cleanStringForContent(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    invoke-interface {v7, v6, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 256
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "bytes":[B
    .end local v2    # "bytesRead":I
    .end local v3    # "data":[B
    .end local v5    # "eachFileString":Ljava/lang/String;
    .end local v6    # "tableName":Ljava/lang/String;
    :cond_5
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 257
    sget-object v10, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Directory in the zip file. Ignoring"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 261
    :cond_6
    invoke-virtual {v9}, Ljava/util/zip/ZipInputStream;->close()V

    .line 262
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 269
    return-object v7
.end method

.method private updateVersionTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/cigna/coach/dataobjects/VersionTableData;)V
    .locals 4
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "data"    # Lcom/cigna/coach/dataobjects/VersionTableData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 488
    sget-object v2, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 489
    sget-object v2, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "updateVersionTableIfDbExists: START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :cond_0
    iget-object v2, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3, p1}, Lcom/cigna/coach/utils/CMSHelper;->earlyCoachSQLiteHelper(Landroid/content/Context;ZLandroid/database/sqlite/SQLiteDatabase;)Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v1

    .line 493
    .local v1, "coachSQLiteHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v0, Lcom/cigna/coach/db/CMSDBManager;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/CMSDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 496
    .local v0, "cmsDbManager":Lcom/cigna/coach/db/CMSDBManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, p2, v2}, Lcom/cigna/coach/db/CMSDBManager;->updateVersionTable(Lcom/cigna/coach/dataobjects/VersionTableData;I)V

    .line 497
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 499
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 502
    sget-object v2, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 503
    sget-object v2, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "updateVersionTableIfDbExists: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :cond_1
    return-void

    .line 499
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v2
.end method


# virtual methods
.method public checkAndCopyDefaultZipFilesToLocalPath(ZLandroid/database/sqlite/SQLiteDatabase;)Z
    .locals 11
    .param p1, "dbExists"    # Z
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 513
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 514
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "checkAndCopyDefaultZipFilesToLocalPath: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    :cond_0
    const/4 v0, 0x0

    .line 518
    .local v0, "anyChangesMade":Z
    invoke-virtual {p0}, Lcom/cigna/coach/utils/CMSHelper;->getRawZipFileVersions()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v7

    .line 519
    .local v7, "versionValues":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/cigna/coach/dataobjects/VersionTableData;>;"
    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/dataobjects/VersionTableData;

    .line 520
    .local v6, "rawFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v1

    .line 521
    .local v1, "filename":Ljava/lang/String;
    iget-object v8, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-static {v8, v1}, Lcom/cigna/coach/utils/CMSHelper;->getLocalZipFileAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 524
    .local v4, "localFilePath":Ljava/lang/String;
    if-eqz p1, :cond_6

    .line 525
    invoke-virtual {p0, p2, v1}, Lcom/cigna/coach/utils/CMSHelper;->getTableVersion(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/VersionTableData;

    move-result-object v3

    .line 526
    .local v3, "localFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    invoke-virtual {v6, v3}, Lcom/cigna/coach/dataobjects/VersionTableData;->compareTo(Lcom/cigna/coach/dataobjects/VersionTableData;)I

    move-result v8

    if-lez v8, :cond_5

    const/4 v5, 0x1

    .line 527
    .local v5, "needsUpdate":Z
    :goto_1
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 528
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkAndCopyDefaultZipFilesToLocalPath: local file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; version: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    .end local v3    # "localFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    :cond_2
    :goto_2
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 534
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkAndCopyDefaultZipFilesToLocalPath: raw file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; resource: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/VersionTableData;->getResourceId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; version: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    if-nez p1, :cond_7

    .line 536
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkAndCopyDefaultZipFilesToLocalPath: DB is not created. Updating "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    :cond_3
    :goto_3
    if-eqz v5, :cond_1

    .line 544
    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/VersionTableData;->getResourceId()I

    move-result v8

    invoke-direct {p0, v8, v1}, Lcom/cigna/coach/utils/CMSHelper;->copyRawFileToLocalPath(ILjava/lang/String;)V

    .line 545
    if-eqz p1, :cond_4

    invoke-direct {p0, p2, v6}, Lcom/cigna/coach/utils/CMSHelper;->updateVersionTable(Landroid/database/sqlite/SQLiteDatabase;Lcom/cigna/coach/dataobjects/VersionTableData;)V

    .line 546
    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 526
    .end local v5    # "needsUpdate":Z
    .restart local v3    # "localFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_1

    .line 531
    .end local v3    # "localFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    :cond_6
    const/4 v5, 0x1

    .restart local v5    # "needsUpdate":Z
    goto :goto_2

    .line 537
    :cond_7
    if-eqz v5, :cond_8

    .line 538
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkAndCopyDefaultZipFilesToLocalPath: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is out of date; updating"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 540
    :cond_8
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkAndCopyDefaultZipFilesToLocalPath: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is up to date; skipping"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 550
    .end local v1    # "filename":Ljava/lang/String;
    .end local v4    # "localFilePath":Ljava/lang/String;
    .end local v5    # "needsUpdate":Z
    .end local v6    # "rawFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    :cond_9
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 551
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "checkAndCopyDefaultZipFilesToLocalPath: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :cond_a
    return v0
.end method

.method public downloadCompressedFileFromSCMS(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 205
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChecksum([B)Ljava/lang/String;
    .locals 6
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 296
    if-nez p1, :cond_0

    .line 297
    const/4 v2, 0x0

    .line 309
    :goto_0
    return-object v2

    .line 299
    :cond_0
    const/4 v2, 0x0

    .line 301
    .local v2, "returnValue":Ljava/lang/String;
    :try_start_0
    const-string v3, "MD5"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 302
    .local v0, "complete":Ljava/security/MessageDigest;
    const/4 v3, 0x0

    array-length v4, p1

    invoke-virtual {v0, p1, v3, v4}, Ljava/security/MessageDigest;->update([BII)V

    .line 303
    new-instance v3, Ljava/math/BigInteger;

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/math/BigInteger;-><init>(I[B)V

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 304
    .end local v0    # "complete":Ljava/security/MessageDigest;
    :catch_0
    move-exception v1

    .line 305
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while getting the file check sum"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    sget-object v3, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3
.end method

.method public getRawZipFileVersions()Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/dataobjects/VersionTableData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 416
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 417
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getRawZipFileVersions: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_0
    invoke-direct {p0}, Lcom/cigna/coach/utils/CMSHelper;->rawZipFileIds()Ljava/util/Map;

    move-result-object v4

    .line 421
    .local v4, "fileIds":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 423
    .local v3, "fileDatas":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/cigna/coach/dataobjects/VersionTableData;>;"
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 424
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "found "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " raw zip files"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_1
    const/4 v6, 0x0

    .line 429
    .local v6, "versionsFile":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/InputStreamReader;

    iget-object v9, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/cigna/coach/R$raw;->versions:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v9

    const-string v10, "UTF-8"

    invoke-direct {v8, v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v7, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    .end local v6    # "versionsFile":Ljava/io/BufferedReader;
    .local v7, "versionsFile":Ljava/io/BufferedReader;
    :cond_2
    :goto_0
    :try_start_1
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, "line":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 433
    const-string v8, ","

    invoke-virtual {v5, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 434
    .local v1, "fields":[Ljava/lang/String;
    new-instance v2, Lcom/cigna/coach/dataobjects/VersionTableData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;-><init>()V

    .line 435
    .local v2, "fileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    const/4 v8, 0x0

    aget-object v8, v1, v8

    invoke-virtual {v2, v8}, Lcom/cigna/coach/dataobjects/VersionTableData;->setTableName(Ljava/lang/String;)V

    .line 436
    const/4 v8, 0x1

    aget-object v8, v1, v8

    invoke-virtual {v2, v8}, Lcom/cigna/coach/dataobjects/VersionTableData;->setVersionCode(Ljava/lang/String;)V

    .line 437
    const/4 v8, 0x2

    aget-object v8, v1, v8

    invoke-virtual {v2, v8}, Lcom/cigna/coach/dataobjects/VersionTableData;->setChksum(Ljava/lang/String;)V

    .line 438
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 442
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/cigna/coach/dataobjects/VersionTableData;->setResourceId(I)V

    .line 443
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 445
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "    "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": version: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; checksum: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;->getChksum()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; resource: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/VersionTableData;->getResourceId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_0

    .line 452
    .end local v1    # "fields":[Ljava/lang/String;
    .end local v2    # "fileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    .end local v5    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v6, v7

    .line 453
    .end local v7    # "versionsFile":Ljava/io/BufferedReader;
    .local v0, "e":Ljava/io/IOException;
    .restart local v6    # "versionsFile":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getRawZipFileVersions: ERROR: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    new-instance v8, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v8, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 456
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_2
    invoke-static {v6}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    throw v8

    .end local v6    # "versionsFile":Ljava/io/BufferedReader;
    .restart local v5    # "line":Ljava/lang/String;
    .restart local v7    # "versionsFile":Ljava/io/BufferedReader;
    :cond_3
    invoke-static {v7}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    .line 459
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 460
    sget-object v8, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getRawZipFileVersions: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    :cond_4
    return-object v3

    .line 456
    .end local v5    # "line":Ljava/lang/String;
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "versionsFile":Ljava/io/BufferedReader;
    .restart local v6    # "versionsFile":Ljava/io/BufferedReader;
    goto :goto_2

    .line 452
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public getTableVersion(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/VersionTableData;
    .locals 8
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 367
    iget-object v5, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-static {v5, p2}, Lcom/cigna/coach/utils/CMSHelper;->getLocalZipFileAbsolutePath(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 368
    .local v3, "filePath":Ljava/lang/String;
    sget-object v5, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 369
    sget-object v5, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getTableVersion: filePath = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 375
    .local v4, "zipFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 376
    invoke-static {p2}, Lcom/cigna/coach/dataobjects/VersionTableData;->nullVersion(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/VersionTableData;

    move-result-object v0

    .line 387
    :goto_0
    return-object v0

    .line 378
    :cond_1
    iget-object v5, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    const/4 v6, 0x0

    invoke-static {v5, v6, p1}, Lcom/cigna/coach/utils/CMSHelper;->earlyCoachSQLiteHelper(Landroid/content/Context;ZLandroid/database/sqlite/SQLiteDatabase;)Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v2

    .line 379
    .local v2, "coachSQLiteHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v1, Lcom/cigna/coach/db/CMSDBManager;

    invoke-direct {v1, v2}, Lcom/cigna/coach/db/CMSDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 382
    .local v1, "cmsDbManager":Lcom/cigna/coach/db/CMSDBManager;
    :try_start_0
    invoke-virtual {v1, p2}, Lcom/cigna/coach/db/CMSDBManager;->getTableVersion(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/VersionTableData;

    move-result-object v0

    .line 383
    .local v0, "ans":Lcom/cigna/coach/dataobjects/VersionTableData;
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto :goto_0

    .end local v0    # "ans":Lcom/cigna/coach/dataobjects/VersionTableData;
    :catchall_0
    move-exception v5

    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v5
.end method

.method public initializeVersionsTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 466
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 467
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "initializeVersionsTable: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    :cond_0
    iget-object v4, p0, Lcom/cigna/coach/utils/CMSHelper;->context:Landroid/content/Context;

    invoke-static {v4, v6, p1}, Lcom/cigna/coach/utils/CMSHelper;->earlyCoachSQLiteHelper(Landroid/content/Context;ZLandroid/database/sqlite/SQLiteDatabase;)Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v1

    .line 471
    .local v1, "coachSQLiteHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v0, Lcom/cigna/coach/db/CMSDBManager;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/CMSDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 474
    .local v0, "cmsDbManager":Lcom/cigna/coach/db/CMSDBManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/utils/CMSHelper;->getRawZipFileVersions()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/VersionTableData;

    .line 475
    .local v3, "rawFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Lcom/cigna/coach/db/CMSDBManager;->updateVersionTable(Lcom/cigna/coach/dataobjects/VersionTableData;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 479
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "rawFileData":Lcom/cigna/coach/dataobjects/VersionTableData;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v4

    .line 477
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 479
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 482
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 483
    sget-object v4, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "initializeVersionsTable: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_2
    return-void
.end method

.method isFileIntegrityCheckPassed([BLjava/lang/String;)Z
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "actualChksum"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 286
    invoke-virtual {p0, p1}, Lcom/cigna/coach/utils/CMSHelper;->getChecksum([B)Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, "downloadedChksum":Ljava/lang/String;
    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    const/4 v1, 0x1

    .line 291
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public retrieveAllLanguageContent()[Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 317
    const-string v1, "CoachContent_All_Lang.zip"

    invoke-direct {p0, v1}, Lcom/cigna/coach/utils/CMSHelper;->getDataFromLocalZipFile(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 318
    .local v0, "contenMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    if-nez v0, :cond_0

    .line 319
    const/4 v1, 0x0

    .line 321
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public retrieveAllMasterTables()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 313
    const-string v0, "coachreferencetables.zip"

    invoke-direct {p0, v0}, Lcom/cigna/coach/utils/CMSHelper;->getDataFromLocalZipFile(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public retrieveContentForLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)[Ljava/lang/String;
    .locals 3
    .param p1, "localeType"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 325
    if-nez p1, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-object v2

    .line 327
    :cond_1
    invoke-static {p1}, Lcom/cigna/coach/utils/CMSHelper;->getFileNameForLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v1

    .line 328
    .local v1, "fileNameForGivenLocale":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/cigna/coach/utils/CMSHelper;->getDataFromLocalZipFile(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 329
    .local v0, "contenMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 332
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    goto :goto_0
.end method

.method public updateLanguageContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V
    .locals 1
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 632
    invoke-direct {p0, p2}, Lcom/cigna/coach/utils/CMSHelper;->getFileNameForLocale(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 633
    .local v0, "fileNameForGivenLocale":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/cigna/coach/utils/CMSHelper;->loadFileContentToDB(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 634
    return-void
.end method

.method public updateLanguageContent(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V
    .locals 4
    .param p1, "localeType"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .prologue
    .line 621
    invoke-static {p1}, Lcom/cigna/coach/utils/CMSHelper;->getFileNameForLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v1

    .line 624
    .local v1, "fileNameForGivenLocale":Ljava/lang/String;
    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v2, v1}, Lcom/cigna/coach/utils/CMSHelper;->loadFileContentToDB(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    :goto_0
    return-void

    .line 625
    :catch_0
    move-exception v0

    .line 628
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v2, Lcom/cigna/coach/utils/CMSHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateLanguageContent(Ljava/util/Locale;)V
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 617
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/cigna/coach/utils/CMSHelper;->updateLanguageContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    .line 618
    return-void
.end method
