.class public Lcom/cigna/coach/utils/RuleBasedAggregator;
.super Ljava/lang/Object;
.source "RuleBasedAggregator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private aggregateCyclesCompleted:I

.field private aggregationLimit:I

.field private final dateFormat:Ljava/text/SimpleDateFormat;

.field private dayLastAggregateComplete:Ljava/util/Calendar;

.field private dayOfLastPush:Ljava/util/Calendar;

.field private entries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;",
            "Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;",
            ">;"
        }
    .end annotation
.end field

.field private entrySequence:I

.field private graceDays:I

.field private maxDays:I

.field private minDays:I

.field private missionSequenceMap:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private sweepWindow:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/cigna/coach/utils/RuleBasedAggregator;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MM-dd-yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;

    .line 49
    iput v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->minDays:I

    .line 50
    const/16 v0, 0xa

    iput v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->maxDays:I

    .line 51
    const/16 v0, 0xf

    iput v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->graceDays:I

    .line 52
    const/4 v0, 0x7

    iput v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->sweepWindow:I

    .line 53
    iput v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregationLimit:I

    .line 54
    iput v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entrySequence:I

    .line 55
    iput v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    .line 57
    iput-object v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;

    .line 58
    iput-object v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entries:Ljava/util/Map;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->missionSequenceMap:Ljava/util/Map;

    .line 67
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/cigna/coach/utils/RuleBasedAggregator;)I
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->sweepWindow:I

    return v0
.end method

.method static synthetic access$1000(Lcom/cigna/coach/utils/RuleBasedAggregator;)I
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->graceDays:I

    return v0
.end method

.method static synthetic access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$202(Lcom/cigna/coach/utils/RuleBasedAggregator;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;
    .param p1, "x1"    # Ljava/util/Calendar;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$300(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/text/SimpleDateFormat;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;

    return-object v0
.end method

.method static synthetic access$400(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->missionSequenceMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$500(Lcom/cigna/coach/utils/RuleBasedAggregator;)I
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregationLimit:I

    return v0
.end method

.method static synthetic access$600(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$602(Lcom/cigna/coach/utils/RuleBasedAggregator;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;
    .param p1, "x1"    # Ljava/util/Calendar;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$700(Lcom/cigna/coach/utils/RuleBasedAggregator;)I
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    return v0
.end method

.method static synthetic access$702(Lcom/cigna/coach/utils/RuleBasedAggregator;I)I
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;
    .param p1, "x1"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    return p1
.end method

.method static synthetic access$708(Lcom/cigna/coach/utils/RuleBasedAggregator;)I
    .locals 2
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    return v0
.end method

.method static synthetic access$800(Lcom/cigna/coach/utils/RuleBasedAggregator;)I
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->minDays:I

    return v0
.end method

.method static synthetic access$900(Lcom/cigna/coach/utils/RuleBasedAggregator;)I
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;

    .prologue
    .line 45
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->maxDays:I

    return v0
.end method

.method private add(Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 5
    .param p1, "mission"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    .line 341
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionStage()Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v2, v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 343
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getPrimarySubcategory()Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    move-result-object v1

    .line 344
    .local v1, "subCategory":Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    sget-object v2, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 345
    sget-object v2, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Adding a mission of subcategory:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    :cond_0
    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entries:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;

    .line 349
    .local v0, "bucket":Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;
    if-nez v0, :cond_1

    .line 350
    new-instance v0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;

    .end local v0    # "bucket":Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;
    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;-><init>(Lcom/cigna/coach/utils/RuleBasedAggregator;Lcom/cigna/coach/utils/RuleBasedAggregator$1;)V

    .line 351
    .restart local v0    # "bucket":Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;
    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entries:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    :cond_1
    # invokes: Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->push(Lcom/cigna/coach/dataobjects/UserMissionData;)V
    invoke-static {v0, p1}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->access$1200(Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;Lcom/cigna/coach/dataobjects/UserMissionData;)V

    .line 360
    .end local v0    # "bucket":Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;
    .end local v1    # "subCategory":Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    :cond_2
    :goto_0
    return-void

    .line 356
    :cond_3
    sget-object v2, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 357
    sget-object v2, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Mission is not an action mission or it is not in completed status"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public aggregate(Ljava/util/List;)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 281
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 282
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "*****************************************"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Starting aggregating"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :cond_0
    iput v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entrySequence:I

    .line 287
    iput v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    .line 288
    iput-object v6, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;

    .line 289
    iput-object v6, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;

    .line 290
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entries:Ljava/util/Map;

    .line 291
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->missionSequenceMap:Ljava/util/Map;

    .line 293
    if-eqz p1, :cond_3

    .line 294
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 295
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Number of missions:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_1
    new-instance v3, Lcom/cigna/coach/utils/RuleBasedAggregator$1;

    invoke-direct {v3, p0}, Lcom/cigna/coach/utils/RuleBasedAggregator$1;-><init>(Lcom/cigna/coach/utils/RuleBasedAggregator;)V

    invoke-static {p1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 312
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .local v1, "m":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    move-object v2, v1

    .line 313
    check-cast v2, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 314
    .local v2, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 315
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "........................."

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entrySequence:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entrySequence:I

    .line 318
    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->missionSequenceMap:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->entrySequence:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    :cond_2
    invoke-direct {p0, v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->add(Lcom/cigna/coach/dataobjects/UserMissionData;)V

    goto :goto_0

    .line 323
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "m":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .end local v2    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_3
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 324
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Incoming list for aggregation is null"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_4
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 328
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "-----------------------------------------"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Num cycles completed:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    sget-object v4, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Last mission counted date:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;

    iget-object v6, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    sget-object v3, Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "*****************************************"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_5
    return-void

    .line 330
    :cond_6
    const-string/jumbo v3, "null"

    goto :goto_1
.end method

.method public getAggregateCyclesCompleted()I
    .locals 1

    .prologue
    .line 364
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I

    return v0
.end method

.method public getDayLastAggregateComplete()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;

    return-object v0
.end method

.method public setAggregationLimit(I)V
    .locals 0
    .param p1, "aggregationLimit"    # I

    .prologue
    .line 384
    iput p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregationLimit:I

    .line 385
    return-void
.end method

.method public setGraceDays(I)V
    .locals 0
    .param p1, "graceDays"    # I

    .prologue
    .line 380
    iput p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->graceDays:I

    .line 381
    return-void
.end method

.method public setMaxDays(I)V
    .locals 0
    .param p1, "maxDays"    # I

    .prologue
    .line 376
    iput p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->maxDays:I

    .line 377
    return-void
.end method

.method public setMinDays(I)V
    .locals 0
    .param p1, "minDays"    # I

    .prologue
    .line 372
    iput p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->minDays:I

    .line 373
    return-void
.end method

.method public setSweepWindow(I)V
    .locals 0
    .param p1, "sweepWindow"    # I

    .prologue
    .line 388
    iput p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator;->sweepWindow:I

    .line 389
    return-void
.end method
