.class public Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.super Ljava/lang/Object;
.source "JournalHelper.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale",
        "SimpleDateFormat"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;,
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$JournalEntryComparator;,
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;,
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;,
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;,
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;,
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;,
        Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    }
.end annotation


# static fields
.field public static final DEFAULT_USER_NAME_WHILE_RESTORE:Ljava/lang/String; = "_default"

.field public static final JOURNAL_TABLE_NAME:Ljava/lang/String; = "usrActyJrnlList"

.field public static final KEY_ACTN_CD:Ljava/lang/String; = "actnCd"

.field public static final KEY_ACTN_TIMESTMP:Ljava/lang/String; = "actnTimestamp"

.field public static final KEY_CD_BASE_ID:Ljava/lang/String; = "cdBaseId"

.field public static final KEY_DATA:Ljava/lang/String; = "data"

.field public static final KEY_DATA_SET:Ljava/lang/String; = "dataSet"

.field public static final KEY_DB_VERSN:Ljava/lang/String; = "dbVersn"

.field public static final KEY_DEVICE_ID:Ljava/lang/String; = "deviceId"

.field public static final KEY_KEY:Ljava/lang/String; = "key"

.field public static final KEY_RESPONSE_CODE:Ljava/lang/String; = "code"

.field public static final KEY_RESPONSE_MESSAGE:Ljava/lang/String; = "message"

.field public static final KEY_SEQ_ID:Ljava/lang/String; = "seqId"

.field public static final KEY_TBL_NM:Ljava/lang/String; = "tblNm"

.field public static final KEY_TIMESTAMP:Ljava/lang/String; = "trsTs"

.field public static final KEY_TRANSINDICATOR:Ljava/lang/String; = "transIndicator"

.field public static final KEY_TYPE_CODE:Ljava/lang/String; = "trsTyCd"

.field public static final KEY_USERNAME:Ljava/lang/String; = "usrIdTxt"

.field public static final KEY_USER_ID:Ljava/lang/String; = "usrId"

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final MARKER_TABLE:Ljava/lang/String; = "__MARKER"

.field public static final NULL:Ljava/lang/String; = "_NULL_"

.field private static USER_TABLES:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final backupApi:Ljava/lang/String; = "/cigna/setUserData"

.field public static final nullActivityListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

.field private static final restoreApi:Ljava/lang/String; = "/cigna/getUserData"


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    const-class v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    .line 99
    sput-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->USER_TABLES:Ljava/util/List;

    .line 125
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;-><init>(Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;)V

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->nullActivityListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object p1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    .line 213
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/cigna/coach/utils/backuprestore/JournalHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;ZLcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .param p4, "x4"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->sendBackup(Ljava/lang/String;ZLcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;)Ljava/util/Properties;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->loadProperties(Ljava/lang/String;)Ljava/util/Properties;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Lcom/cigna/coach/db/JournalDBManager;Ljava/util/Properties;)V
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper;
    .param p1, "x1"    # Lcom/cigna/coach/db/JournalDBManager;
    .param p2, "x2"    # Ljava/util/Properties;

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->deleteUserTableEntiries(Lcom/cigna/coach/db/JournalDBManager;Ljava/util/Properties;)V

    return-void
.end method

.method private backup(Ljava/lang/String;)Z
    .locals 2
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 506
    const/4 v0, 0x0

    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->nullActivityListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    invoke-direct {p0, p1, v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->startBackup(Ljava/lang/String;ZLcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v0

    return v0
.end method

.method private chooseDatasetToRestore(Ljava/util/Map;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;
    .locals 7
    .param p2, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;>;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 924
    .local p1, "dataSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 925
    :cond_0
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 926
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Picking null dataset from null or empty array"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    :cond_1
    const/4 v4, 0x0

    .line 946
    :goto_0
    return-object v4

    .line 931
    :cond_2
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 932
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Picking one dataset from array of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    :cond_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 936
    .local v3, "sortableDatasets":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;>;"
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 937
    .local v0, "dataset":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    new-instance v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    invoke-direct {v6, v4, v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 939
    .end local v0    # "dataset":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    :cond_4
    invoke-static {v3}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;

    .line 941
    .local v2, "largestDataset":Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 942
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Selected devide id: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getDeviceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v5, "========================="

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 946
    :cond_5
    invoke-virtual {v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getDataset()Ljava/util/Map;

    move-result-object v4

    goto/16 :goto_0
.end method

.method private static concatenateDbFieldsForTriggerData(Ljava/util/Set;Ljava/lang/String;C)Ljava/lang/String;
    .locals 6
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "delimiter"    # C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "C)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1430
    .local p0, "fields":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v4, ""

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1431
    .local v2, "sb":Ljava/lang/StringBuffer;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "||\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'||"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1432
    .local v3, "seperator":Ljava/lang/String;
    if-eqz p0, :cond_1

    .line 1433
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1434
    .local v0, "field":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 1435
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1437
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "COALESCE("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_NULL_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\')"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 1442
    .end local v0    # "field":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private deleteUserTableEntiries(Lcom/cigna/coach/db/JournalDBManager;Ljava/util/Properties;)V
    .locals 6
    .param p1, "dbManager"    # Lcom/cigna/coach/db/JournalDBManager;
    .param p2, "restoreOrder"    # Ljava/util/Properties;

    .prologue
    .line 1175
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1176
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Wiping user tables in preparation for restore..."

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1181
    .local v0, "deleteOrder":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/util/Properties;->stringPropertyNames()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1182
    .local v2, "table":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-interface {v0, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_0

    .line 1185
    .end local v2    # "table":Ljava/lang/String;
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1186
    .restart local v2    # "table":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1187
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting all rows from table:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1189
    :cond_2
    invoke-virtual {p1, v2}, Lcom/cigna/coach/db/JournalDBManager;->deleteUserTableEntries(Ljava/lang/String;)I

    goto :goto_1

    .line 1193
    .end local v2    # "table":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method public static dropJournalTriggers(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 1202
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1203
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v17, "Dropping journal triggers"

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1206
    .local v10, "startTime":J
    const/4 v15, 0x0

    .line 1208
    .local v15, "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-static/range {p0 .. p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->getUserTables(Landroid/content/Context;)Ljava/util/List;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v15

    .line 1213
    :goto_0
    const-string v3, " DROP TRIGGER IF EXISTS %1$s_AI "

    .line 1215
    .local v3, "dropInsertTrigger":Ljava/lang/String;
    const-string v5, " DROP TRIGGER IF EXISTS %1$s_AU "

    .line 1217
    .local v5, "dropUpdateTrigger":Ljava/lang/String;
    const-string v1, " DROP TRIGGER IF EXISTS %1$s_BD1 "

    .line 1219
    .local v1, "dropDeleteTrigger":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/cigna/coach/db/JournalDBManager;->getJournalTableDetails(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)Ljava/util/Set;

    move-result-object v13

    .line 1220
    .local v13, "tableInfoSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/db/DbTableInfo;>;"
    if-eqz v13, :cond_5

    invoke-interface {v13}, Ljava/util/Set;->size()I

    move-result v16

    if-lez v16, :cond_5

    .line 1221
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cigna/coach/db/DbTableInfo;

    .line 1222
    .local v12, "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_1

    .line 1223
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Dropping in triggers for table:"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v12}, Lcom/cigna/coach/db/DbTableInfo;->getTableName()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    :cond_1
    invoke-virtual {v12}, Lcom/cigna/coach/db/DbTableInfo;->getTableName()Ljava/lang/String;

    move-result-object v14

    .line 1227
    .local v14, "tableName":Ljava/lang/String;
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v14, v16, v17

    move-object/from16 v0, v16

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1228
    .local v4, "dropInsertTriggerQuery":Ljava/lang/String;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 1229
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    :cond_2
    :try_start_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1237
    :goto_2
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v14, v16, v17

    move-object/from16 v0, v16

    invoke-static {v5, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1238
    .local v6, "dropUpdateTriggerQuery":Ljava/lang/String;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 1239
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    :cond_3
    :try_start_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1247
    :goto_3
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v14, v16, v17

    move-object/from16 v0, v16

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1248
    .local v2, "dropDeleteTriggerQuery":Ljava/lang/String;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 1249
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-static {v0, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    :cond_4
    :try_start_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1

    .line 1253
    :catch_0
    move-exception v7

    .line 1254
    .local v7, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Dropping Delete triggers throws error "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1209
    .end local v1    # "dropDeleteTrigger":Ljava/lang/String;
    .end local v2    # "dropDeleteTriggerQuery":Ljava/lang/String;
    .end local v3    # "dropInsertTrigger":Ljava/lang/String;
    .end local v4    # "dropInsertTriggerQuery":Ljava/lang/String;
    .end local v5    # "dropUpdateTrigger":Ljava/lang/String;
    .end local v6    # "dropUpdateTriggerQuery":Ljava/lang/String;
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v12    # "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    .end local v13    # "tableInfoSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/db/DbTableInfo;>;"
    .end local v14    # "tableName":Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 1210
    .local v8, "e1":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v8}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1233
    .end local v8    # "e1":Lcom/cigna/coach/exceptions/CoachException;
    .restart local v1    # "dropDeleteTrigger":Ljava/lang/String;
    .restart local v3    # "dropInsertTrigger":Ljava/lang/String;
    .restart local v4    # "dropInsertTriggerQuery":Ljava/lang/String;
    .restart local v5    # "dropUpdateTrigger":Ljava/lang/String;
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v12    # "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    .restart local v13    # "tableInfoSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/db/DbTableInfo;>;"
    .restart local v14    # "tableName":Ljava/lang/String;
    :catch_2
    move-exception v7

    .line 1234
    .restart local v7    # "e":Landroid/database/sqlite/SQLiteException;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Dropping Insert triggers throws error "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1243
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v6    # "dropUpdateTriggerQuery":Ljava/lang/String;
    :catch_3
    move-exception v7

    .line 1244
    .restart local v7    # "e":Landroid/database/sqlite/SQLiteException;
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Dropping Update triggers throws error "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1259
    .end local v4    # "dropInsertTriggerQuery":Ljava/lang/String;
    .end local v6    # "dropUpdateTriggerQuery":Ljava/lang/String;
    .end local v7    # "e":Landroid/database/sqlite/SQLiteException;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v12    # "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    .end local v14    # "tableName":Ljava/lang/String;
    :cond_5
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 1260
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v17, "No table info returned"

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    :cond_6
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v16 .. v16}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 1264
    sget-object v16, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Triggers dropped in "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v10

    invoke-virtual/range {v17 .. v19}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " ms"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    :cond_7
    return-void
.end method

.method public static getTablesToReset(Landroid/content/Context;)Ljava/util/List;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 1475
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->getUserTables(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1476
    .local v0, "ans":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "WIDGT_NOTFTN"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1477
    const-string v1, "NOTFTN_QUEUE_BLOB"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1478
    const-string v1, "NOTFTN_STE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1479
    return-object v0
.end method

.method public static declared-synchronized getUserTables(Landroid/content/Context;)Ljava/util/List;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 1447
    const-class v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    monitor-enter v7

    :try_start_0
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->USER_TABLES:Ljava/util/List;

    if-nez v6, :cond_1

    .line 1448
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    sput-object v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->USER_TABLES:Ljava/util/List;

    .line 1449
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const-string v8, "backup_restore_sequence"

    const-string/jumbo v9, "raw"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v8, v9, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    .line 1451
    .local v1, "resourceId":I
    const/4 v2, 0x0

    .line 1453
    .local v2, "stream":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 1454
    new-instance v5, Lcom/cigna/coach/utils/EntryOrderedProperties;

    invoke-direct {v5}, Lcom/cigna/coach/utils/EntryOrderedProperties;-><init>()V

    .line 1455
    .local v5, "userTables":Lcom/cigna/coach/utils/EntryOrderedProperties;
    invoke-virtual {v5, v2}, Lcom/cigna/coach/utils/EntryOrderedProperties;->load(Ljava/io/InputStream;)V

    .line 1457
    invoke-virtual {v5}, Lcom/cigna/coach/utils/EntryOrderedProperties;->keys()Ljava/util/Enumeration;

    move-result-object v4

    .line 1458
    .local v4, "tables":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1459
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1460
    .local v3, "table":Ljava/lang/String;
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->USER_TABLES:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1463
    .end local v3    # "table":Ljava/lang/String;
    .end local v4    # "tables":Ljava/util/Enumeration;
    .end local v5    # "userTables":Lcom/cigna/coach/utils/EntryOrderedProperties;
    :catch_0
    move-exception v0

    .line 1464
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v6, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Failed to read field mapping from \'%s\'."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "backup_restore_sequence"

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v8, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1467
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    :try_start_3
    invoke-static {v2}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1447
    .end local v1    # "resourceId":I
    .end local v2    # "stream":Ljava/io/InputStream;
    :catchall_1
    move-exception v6

    monitor-exit v7

    throw v6

    .line 1462
    .restart local v1    # "resourceId":I
    .restart local v2    # "stream":Ljava/io/InputStream;
    .restart local v4    # "tables":Ljava/util/Enumeration;
    .restart local v5    # "userTables":Lcom/cigna/coach/utils/EntryOrderedProperties;
    :cond_0
    :try_start_4
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->USER_TABLES:Ljava/util/List;

    invoke-static {v6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    sput-object v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->USER_TABLES:Ljava/util/List;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1467
    :try_start_5
    invoke-static {v2}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    .line 1471
    .end local v1    # "resourceId":I
    .end local v2    # "stream":Ljava/io/InputStream;
    .end local v4    # "tables":Ljava/util/Enumeration;
    .end local v5    # "userTables":Lcom/cigna/coach/utils/EntryOrderedProperties;
    :cond_1
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->USER_TABLES:Ljava/util/List;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    monitor-exit v7

    return-object v6
.end method

.method private loadProperties(Ljava/lang/String;)Ljava/util/Properties;
    .locals 8
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1493
    iget-object v4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string/jumbo v5, "raw"

    iget-object v6, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, p1, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 1494
    .local v2, "resourceId":I
    new-instance v1, Ljava/util/Properties;

    invoke-direct {v1}, Ljava/util/Properties;-><init>()V

    .line 1496
    .local v1, "properties":Ljava/util/Properties;
    const/4 v3, 0x0

    .line 1499
    .local v3, "stream":Ljava/io/InputStream;
    :try_start_0
    iget-object v4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 1506
    :try_start_1
    invoke-virtual {v1, v3}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1511
    if-eqz v3, :cond_0

    .line 1512
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 1516
    :cond_0
    return-object v1

    .line 1500
    :catch_0
    move-exception v0

    .line 1502
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    :try_start_2
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Property file \'%s\' not found."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1511
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    :catchall_0
    move-exception v4

    if-eqz v3, :cond_1

    .line 1512
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v4

    .line 1507
    :catch_1
    move-exception v0

    .line 1508
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    new-instance v4, Ljava/lang/Exception;

    const-string v5, "Failed to read properties from \'%s\'."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private parseRestoreResponse(Ljava/lang/String;Lorg/json/JSONObject;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;
    .locals 31
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "dump"    # Lorg/json/JSONObject;
    .param p3, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/json/JSONObject;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 958
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 959
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Parsing data received from server"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    :cond_0
    const/16 v22, 0x0

    .line 965
    .local v22, "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    :try_start_0
    const-string v3, "code"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 966
    .local v10, "code":Ljava/lang/String;
    const-string/jumbo v3, "message"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 967
    .local v20, "message":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 968
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Top level value CODE:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Top level value MESSAGE:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    :cond_1
    const-string v3, "dataSet"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v25

    .line 974
    .local v25, "unparsedDataSets":Lorg/json/JSONArray;
    if-eqz v25, :cond_2

    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 976
    :cond_2
    const/16 v23, 0x0

    .line 1036
    :goto_0
    return-object v23

    .line 979
    :cond_3
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 980
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Number of data sets:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONArray;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    :cond_4
    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 985
    .end local v22    # "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .local v23, "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    :try_start_1
    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONArray;->length()I

    move-result v3

    move/from16 v0, v16

    if-ge v0, v3, :cond_a

    .line 986
    new-instance v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    move/from16 v0, v16

    int-to-double v3, v0

    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONArray;->length()I

    move-result v5

    int-to-double v5, v5

    div-double v4, v3, v5

    add-int/lit8 v3, v16, 0x1

    int-to-double v6, v3

    invoke-virtual/range {v25 .. v25}, Lorg/json/JSONArray;->length()I

    move-result v3

    int-to-double v0, v3

    move-wide/from16 v29, v0

    div-double v6, v6, v29

    move-object/from16 v3, p3

    invoke-direct/range {v2 .. v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    .line 989
    .local v2, "datasetListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 990
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Parsing dataset :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 994
    :cond_5
    move-object/from16 v0, v25

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v24

    .line 996
    .local v24, "unparsedDataSet":Lorg/json/JSONObject;
    const-string v3, "deviceId"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 997
    .local v12, "deviceId":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 998
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Adding data for deviceId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    :cond_6
    const-string/jumbo v3, "usrActyJrnlList"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v19

    .line 1002
    .local v19, "journalEntries":Lorg/json/JSONArray;
    sget-object v3, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v4, 0x3fb999999999999aL    # 0.1

    invoke-interface {v2, v3, v4, v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 1005
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    move-object/from16 v29, v0

    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v5, 0x3fb999999999999aL    # 0.1

    const-wide/high16 v7, 0x3fd0000000000000L    # 0.25

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-static {v0, v1, v3}, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->parse(Landroid/content/Context;Lorg/json/JSONArray;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/List;

    move-result-object v21

    .line 1007
    .local v21, "parsedRecords":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v5, 0x3fd0000000000000L    # 0.25

    const-wide v7, 0x3fd999999999999aL    # 0.4

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, v21

    invoke-static {v0, v3}, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->sort(Ljava/util/List;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V

    .line 1008
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v5, 0x3fd999999999999aL    # 0.4

    const-wide v7, 0x3fe199999999999aL    # 0.55

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, v21

    invoke-static {v0, v3}, Lcom/cigna/coach/utils/backuprestore/JournalDataParser;->group(Ljava/util/List;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;

    move-result-object v15

    .line 1010
    .local v15, "groupedRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;>;"
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v5, 0x3fe199999999999aL    # 0.55

    const-wide v7, 0x3fe6666666666666L    # 0.7

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    invoke-static {v15, v3}, Lcom/cigna/coach/utils/backuprestore/UserJournalDataUpgrader;->upgradeToCurrentVersion(Ljava/util/Map;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;

    move-result-object v26

    .line 1012
    .local v26, "upgradedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v5, 0x3fe6666666666666L    # 0.7

    const-wide v7, 0x3feccccccccccccdL    # 0.9

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, v26

    invoke-static {v0, v3}, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->aggregate(Ljava/util/Map;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;

    move-result-object v11

    .line 1014
    .local v11, "consolidatedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1015
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Adding data into map for deviceId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "and data: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1017
    :cond_7
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1018
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v4, "printing the consolidated records by table name--"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v9

    .line 1020
    .local v9, "TableNameKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_8
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    .line 1021
    .local v14, "eachKey":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "key:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1022
    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Ljava/util/List;

    .line 1023
    .local v28, "userTableJournalDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .line 1024
    .local v27, "userTableJournalData":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->printUserTableJournalData(Lcom/cigna/coach/dataobjects/UserTableJournalData;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 1031
    .end local v2    # "datasetListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .end local v9    # "TableNameKeys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .end local v11    # "consolidatedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    .end local v12    # "deviceId":Ljava/lang/String;
    .end local v14    # "eachKey":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    .end local v15    # "groupedRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;>;"
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v19    # "journalEntries":Lorg/json/JSONArray;
    .end local v21    # "parsedRecords":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    .end local v24    # "unparsedDataSet":Lorg/json/JSONObject;
    .end local v26    # "upgradedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    .end local v27    # "userTableJournalData":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .end local v28    # "userTableJournalDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    :catch_0
    move-exception v13

    move-object/from16 v22, v23

    .line 1032
    .end local v10    # "code":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v20    # "message":Ljava/lang/String;
    .end local v23    # "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .end local v25    # "unparsedDataSets":Lorg/json/JSONArray;
    .local v13, "e":Lorg/json/JSONException;
    .restart local v22    # "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    :goto_3
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Unable to extract top-level values from restore dump: %s."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v13}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    throw v13

    .line 1028
    .end local v13    # "e":Lorg/json/JSONException;
    .end local v22    # "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .restart local v2    # "datasetListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .restart local v10    # "code":Ljava/lang/String;
    .restart local v11    # "consolidatedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    .restart local v12    # "deviceId":Ljava/lang/String;
    .restart local v15    # "groupedRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;>;"
    .restart local v16    # "i":I
    .restart local v19    # "journalEntries":Lorg/json/JSONArray;
    .restart local v20    # "message":Ljava/lang/String;
    .restart local v21    # "parsedRecords":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    .restart local v23    # "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .restart local v24    # "unparsedDataSet":Lorg/json/JSONObject;
    .restart local v25    # "unparsedDataSets":Lorg/json/JSONArray;
    .restart local v26    # "upgradedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    :cond_9
    :try_start_2
    move-object/from16 v0, v23

    invoke-interface {v0, v12, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1029
    sget-object v3, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-interface {v2, v3, v4, v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 985
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .end local v2    # "datasetListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .end local v11    # "consolidatedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    .end local v12    # "deviceId":Ljava/lang/String;
    .end local v15    # "groupedRecords":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;>;"
    .end local v19    # "journalEntries":Lorg/json/JSONArray;
    .end local v21    # "parsedRecords":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    .end local v24    # "unparsedDataSet":Lorg/json/JSONObject;
    .end local v26    # "upgradedRecordsByTableName":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    :cond_a
    move-object/from16 v22, v23

    .line 1036
    .end local v23    # "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .restart local v22    # "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    goto/16 :goto_0

    .line 1031
    .end local v10    # "code":Ljava/lang/String;
    .end local v16    # "i":I
    .end local v20    # "message":Ljava/lang/String;
    .end local v25    # "unparsedDataSets":Lorg/json/JSONArray;
    :catch_1
    move-exception v13

    goto :goto_3
.end method

.method private printUserTableJournalData(Lcom/cigna/coach/dataobjects/UserTableJournalData;)V
    .locals 4
    .param p1, "userTableJournalData"    # Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .prologue
    .line 1041
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1042
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTableName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "USR_API_CALL_LOG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1043
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v1, "--------"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1044
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEY_SEQ_ID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getSeqId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEY_CD_BASE_ID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getCodeBaseId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEY_DB_VERSN:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getDbModelVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEY_TBL_NM:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEY_ACTN_TIMESTMP:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTransactionTimeStamp()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEY_ACTN_CD:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getActionTypeCode()Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    sget-object v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEY_KEY:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1053
    :cond_0
    return-void
.end method

.method private sendBackup(Ljava/lang/String;ZLcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 20
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "isBeforeRestore"    # Z
    .param p3, "backupType"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .param p4, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 575
    sget-object v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 576
    sget-object v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Starting backup process: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    sget-object v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Before restore flag:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    :cond_0
    if-eqz p2, :cond_4

    .line 582
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/16 v5, 0x0

    const-wide/high16 v7, 0x3fe0000000000000L    # 0.5

    move-object/from16 v4, p4

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    .line 583
    .local v3, "backupListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    new-instance v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    move-object/from16 v5, p4

    invoke-direct/range {v4 .. v9}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    .line 589
    .local v4, "restoreListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    :goto_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v7}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v14

    .line 591
    .local v14, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    sget-object v7, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v8, 0x3fa999999999999aL    # 0.05

    invoke-interface {v3, v7, v8, v9}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 593
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 595
    .local v12, "backupStartTime":J
    :try_start_0
    new-instance v5, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v7, 0x3fa999999999999aL    # 0.05

    const-wide v9, 0x3fd999999999999aL    # 0.4

    move-object v6, v3

    invoke-direct/range {v5 .. v10}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2, v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->serializeBackupRequest(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Lorg/json/JSONObject;

    move-result-object v18

    .line 598
    .local v18, "serializedData":Lorg/json/JSONObject;
    sget-object v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 599
    sget-object v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v8, "Sending json data to server"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_1
    new-instance v17, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    const/4 v8, 0x1

    move-object/from16 v0, v17

    invoke-direct {v0, v7, v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    .local v17, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v19, Lcom/cigna/coach/db/UserMetricsDBManager;

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 606
    .local v19, "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    sget-object v7, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v7}, Lcom/cigna/coach/db/UserMetricsDBManager;->setBackupType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;)V

    .line 607
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609
    :try_start_2
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 612
    new-instance v15, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;

    new-instance v5, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v7, 0x3fd999999999999aL    # 0.4

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    move-object v6, v3

    invoke-direct/range {v5 .. v10}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object v6, v15

    move-object/from16 v7, p0

    move-wide v8, v12

    move-object/from16 v10, p1

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper;JLjava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V

    .line 617
    .local v15, "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    sget-object v7, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v7}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result v6

    .line 620
    .local v6, "requestId":I
    if-eqz v18, :cond_2

    .line 621
    :try_start_3
    new-instance v5, Lcom/cigna/coach/acm/ACMTask;

    sget-object v7, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    sget-object v8, Lcom/cigna/coach/acm/NetworkConstants;->API_SET_USER_DATA:Ljava/lang/String;

    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    move-object v11, v15

    invoke-direct/range {v5 .. v11}, Lcom/cigna/coach/acm/ACMTask;-><init>(ILcom/cigna/coach/acm/ACMTask$ACMTaskType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/acm/IACMTaskCompleteListener;)V

    .line 623
    .local v5, "task":Lcom/cigna/coach/acm/ACMTask;
    sget-object v7, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v8, 0x3fd999999999999aL    # 0.4

    invoke-interface {v3, v7, v8, v9}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 624
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-virtual {v5, v7}, Lcom/cigna/coach/acm/ACMTask;->execute(Landroid/content/Context;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 630
    .end local v5    # "task":Lcom/cigna/coach/acm/ACMTask;
    :cond_2
    if-eqz p2, :cond_3

    .line 631
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->startRestore(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    .line 642
    :cond_3
    const/4 v7, 0x1

    .end local v6    # "requestId":I
    .end local v15    # "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    .end local v17    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v18    # "serializedData":Lorg/json/JSONObject;
    .end local v19    # "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :goto_1
    return v7

    .line 585
    .end local v3    # "backupListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .end local v4    # "restoreListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .end local v12    # "backupStartTime":J
    .end local v14    # "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    :cond_4
    move-object/from16 v3, p4

    .line 586
    .restart local v3    # "backupListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    const/4 v4, 0x0

    .restart local v4    # "restoreListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    goto/16 :goto_0

    .line 609
    .restart local v12    # "backupStartTime":J
    .restart local v14    # "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    .restart local v17    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v18    # "serializedData":Lorg/json/JSONObject;
    :catchall_0
    move-exception v7

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v7
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 633
    .end local v17    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v18    # "serializedData":Lorg/json/JSONObject;
    :catch_0
    move-exception v16

    .line 635
    .local v16, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupFailedBroadcast()V

    .line 636
    sget-object v7, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v8, 0x0

    sget-object v9, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_DB_OPERATION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v3, v7, v8, v9, v10}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 637
    sget-object v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v8, "An error occured while startBackup"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    sget-object v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    const/4 v7, 0x0

    goto :goto_1

    .line 626
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v6    # "requestId":I
    .restart local v15    # "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    .restart local v17    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v18    # "serializedData":Lorg/json/JSONObject;
    .restart local v19    # "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :catch_1
    move-exception v16

    .line 627
    .restart local v16    # "e":Ljava/lang/Exception;
    :try_start_5
    new-instance v7, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Error while creating remote connection"

    move-object/from16 v0, v16

    invoke-direct {v7, v8, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
.end method

.method private serializeJournalRow(Lcom/cigna/coach/dataobjects/JournalEntry;)Lorg/json/JSONObject;
    .locals 5
    .param p1, "journalEntry"    # Lcom/cigna/coach/dataobjects/JournalEntry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 813
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 814
    .local v0, "row":Lorg/json/JSONObject;
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 818
    :cond_0
    const-string/jumbo v1, "trsTyCd"

    sget-object v2, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 819
    const-string/jumbo v1, "trsTs"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getActionTime()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 822
    const-string v1, "cdBaseId"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getCodeBaseId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 823
    const-string v1, "dbVersn"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getDbVersion()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 824
    const-string/jumbo v1, "tblNm"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getTableName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 825
    const-string v1, "actnTimestamp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getActionTime()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 826
    const-string v1, "actnCd"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getActionType()Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 827
    const-string v1, "key"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 828
    const-string v1, "data"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 830
    return-object v0
.end method

.method public static setJournalTriggers(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1284
    .local p2, "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1285
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v19, "Setting up journal triggers"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    .line 1305
    .local v11, "startTime":J
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " CREATE TRIGGER %1$s_AI  AFTER INSERT ON %1$s  WHEN ( (SELECT JRNL_ON FROM USR_ACTY_JRNL_SWITCH WHERE TBL_NM=\'%1$s\') > 0)  BEGIN  \tDELETE FROM USR_ACTY_JRNL WHERE USR_ID=new.USR_ID AND KEY = (%2$s) AND TBL_NM=\'%1$s\' AND ACTN_CD = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ";"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " \tINSERT INTO USR_ACTY_JRNL(CD_BASE_ID, DB_VERSN, USR_ID, TBL_NM, ACTN_CD, KEY, DATA) "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\t\tSELECT CD_BASE_ID , DB_VERSN, new.USR_ID, \'%1$s\', "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", (%2$s), (%3$s) FROM DB_VERSN LIMIT 1;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " END;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1319
    .local v7, "insertTrigger":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " CREATE TRIGGER %1$s_AU  AFTER UPDATE ON %1$s  WHEN ( (SELECT JRNL_ON FROM USR_ACTY_JRNL_SWITCH WHERE TBL_NM=\'%1$s\') > 0)  BEGIN  \tDELETE FROM USR_ACTY_JRNL WHERE USR_ID=new.USR_ID AND KEY = (%2$s) AND TBL_NM=\'%1$s\' AND ACTN_CD IN ("

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->UPDATE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ","

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->DELETE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ");"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " \tINSERT INTO USR_ACTY_JRNL(CD_BASE_ID, DB_VERSN, USR_ID, TBL_NM, ACTN_CD, KEY, DATA) "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\t\tSELECT CD_BASE_ID ,DB_VERSN ,new.USR_ID, \'%1$s\', "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->UPDATE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", (%2$s), (%3$s) FROM DB_VERSN LIMIT 1;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " END;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 1355
    .local v16, "updateTrigger":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " CREATE TRIGGER %1$s_BD1  BEFORE DELETE ON %1$s  WHEN ( (SELECT JRNL_ON FROM USR_ACTY_JRNL_SWITCH WHERE TBL_NM=\'%1$s\') > 0)  BEGIN  \tINSERT INTO USR_ACTY_JRNL(CD_BASE_ID, DB_VERSN, USR_ID, TBL_NM, ACTN_CD, KEY, DATA) \t\tSELECT CD_BASE_ID , DB_VERSN , old.USR_ID, \'%1$s\', "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->DELETE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", (%2$s), (%3$s) FROM DB_VERSN LIMIT 1;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " \tUPDATE USR_ACTY_JRNL SET ACTN_CD="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->TEMP:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " WHERE USR_ID=old.USR_ID AND KEY = (%2$s) AND TBL_NM=\'%1$s\' AND "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\t\t(SELECT COUNT(*) FROM USR_ACTY_JRNL WHERE USR_ID=old.USR_ID AND KEY = (%2$s) AND TBL_NM=\'%1$s\' AND ACTN_CD ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") !=0 ;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " \tUPDATE USR_ACTY_JRNL SET ACTN_CD="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->TEMP:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " WHERE USR_ID=old.USR_ID AND KEY = (%2$s) AND ACTN_CD="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->UPDATE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " AND TBL_NM=\'%1$s\' AND "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\t\t(SELECT COUNT(*) FROM USR_ACTY_JRNL WHERE USR_ID=old.USR_ID AND KEY = (%2$s) AND TBL_NM=\'%1$s\' AND ACTN_CD ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->UPDATE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ") !=0 ;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " \tDELETE FROM USR_ACTY_JRNL WHERE USR_ID=old.USR_ID AND KEY = (%2$s) AND TBL_NM=\'%1$s\' AND ACTN_CD ="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->TEMP:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ";"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " END;"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1383
    .local v4, "deleteTrigger":Ljava/lang/String;
    invoke-static/range {p1 .. p2}, Lcom/cigna/coach/db/JournalDBManager;->getJournalTableDetails(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)Ljava/util/Set;

    move-result-object v14

    .line 1384
    .local v14, "tableInfoSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/db/DbTableInfo;>;"
    if-eqz v14, :cond_5

    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v18

    if-lez v18, :cond_5

    .line 1385
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/cigna/coach/db/DbTableInfo;

    .line 1386
    .local v13, "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 1387
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Setting up triggers for table:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v13}, Lcom/cigna/coach/db/DbTableInfo;->getTableName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1389
    :cond_1
    invoke-virtual {v13}, Lcom/cigna/coach/db/DbTableInfo;->getTableName()Ljava/lang/String;

    move-result-object v15

    .line 1390
    .local v15, "tableName":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/cigna/coach/db/DbTableInfo;->getPrimaryKeys()Ljava/util/Set;

    move-result-object v18

    const-string/jumbo v19, "new."

    const/16 v20, 0x7c

    invoke-static/range {v18 .. v20}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->concatenateDbFieldsForTriggerData(Ljava/util/Set;Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v10

    .line 1392
    .local v10, "pkCombinationUpdateAndInsert":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/cigna/coach/db/DbTableInfo;->getAllFields()Ljava/util/Set;

    move-result-object v18

    const-string/jumbo v19, "new."

    const/16 v20, 0x7c

    invoke-static/range {v18 .. v20}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->concatenateDbFieldsForTriggerData(Ljava/util/Set;Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v3

    .line 1394
    .local v3, "allFieldCombinationUpdateAndInsert":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/cigna/coach/db/DbTableInfo;->getPrimaryKeys()Ljava/util/Set;

    move-result-object v18

    const-string/jumbo v19, "old."

    const/16 v20, 0x7c

    invoke-static/range {v18 .. v20}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->concatenateDbFieldsForTriggerData(Ljava/util/Set;Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v9

    .line 1395
    .local v9, "pkCombinationDelete":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/cigna/coach/db/DbTableInfo;->getAllFields()Ljava/util/Set;

    move-result-object v18

    const-string/jumbo v19, "old."

    const/16 v20, 0x7c

    invoke-static/range {v18 .. v20}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->concatenateDbFieldsForTriggerData(Ljava/util/Set;Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v2

    .line 1397
    .local v2, "allFieldCombinationDelete":Ljava/lang/String;
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v15, v18, v19

    const/16 v19, 0x1

    aput-object v10, v18, v19

    const/16 v19, 0x2

    aput-object v3, v18, v19

    move-object/from16 v0, v18

    invoke-static {v7, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 1399
    .local v8, "insertTriggerQuery":Ljava/lang/String;
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1400
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1404
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v15, v18, v19

    const/16 v19, 0x1

    aput-object v10, v18, v19

    const/16 v19, 0x2

    aput-object v3, v18, v19

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 1406
    .local v17, "updateTriggerQuery":Ljava/lang/String;
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 1407
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1409
    :cond_3
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1411
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    aput-object v15, v18, v19

    const/16 v19, 0x1

    aput-object v9, v18, v19

    const/16 v19, 0x2

    aput-object v2, v18, v19

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1413
    .local v5, "deleteTriggerQuery":Ljava/lang/String;
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1414
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1420
    .end local v2    # "allFieldCombinationDelete":Ljava/lang/String;
    .end local v3    # "allFieldCombinationUpdateAndInsert":Ljava/lang/String;
    .end local v5    # "deleteTriggerQuery":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "insertTriggerQuery":Ljava/lang/String;
    .end local v9    # "pkCombinationDelete":Ljava/lang/String;
    .end local v10    # "pkCombinationUpdateAndInsert":Ljava/lang/String;
    .end local v13    # "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    .end local v15    # "tableName":Ljava/lang/String;
    .end local v17    # "updateTriggerQuery":Ljava/lang/String;
    :cond_5
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 1421
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v19, "No table info returned"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    :cond_6
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 1425
    sget-object v18, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Triggers set in "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v11

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ms"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1427
    :cond_7
    return-void
.end method

.method private startBackup(Ljava/lang/String;ZLcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 14
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "isBeforeRestore"    # Z
    .param p3, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 510
    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v9

    .line 511
    .local v9, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {v9}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupStartedBroadcast()V

    .line 512
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V

    .line 513
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v3, 0x3fa999999999999aL    # 0.05

    move-object/from16 v0, p3

    invoke-interface {v0, v2, v3, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 515
    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    sget-object v3, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/SharedPrefUtils;->getCoachSettings(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 516
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 517
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "startBackup : COACH_NOT_STARTED"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_0
    invoke-virtual {v9}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupFailedBroadcast()V

    .line 520
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v3, 0x0

    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_BACKUP_DATA:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "coach not started"

    move-object/from16 v0, p3

    invoke-interface {v0, v2, v3, v4, v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 521
    const/4 v2, 0x0

    .line 568
    :goto_0
    return v2

    .line 524
    :cond_1
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 525
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Determining backup type"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :cond_2
    sget-object v8, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .line 530
    .local v8, "backupType":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    const/4 v11, 0x0

    .line 532
    .local v11, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v12, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v12, v2, v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 533
    .end local v11    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v12, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v13, Lcom/cigna/coach/db/UserMetricsDBManager;

    invoke-direct {v13, v12}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 534
    .local v13, "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    invoke-virtual {v13, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getBackupType(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v8

    .line 537
    if-eqz v12, :cond_3

    .line 538
    invoke-virtual {v12}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 540
    :cond_3
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v3, 0x3fb999999999999aL    # 0.1

    move-object/from16 v0, p3

    invoke-interface {v0, v2, v3, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 542
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 543
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DB says backup type is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :cond_4
    sget-object v2, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    if-ne v8, v2, :cond_7

    .line 547
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 548
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Skipping remote row count request"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_5
    new-instance v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v3, 0x3fb999999999999aL    # 0.1

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move/from16 v0, p2

    invoke-direct {p0, p1, v0, v8, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->sendBackup(Ljava/lang/String;ZLcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v2

    goto :goto_0

    .line 537
    .end local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v13    # "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    .restart local v11    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_0
    move-exception v2

    :goto_1
    if-eqz v11, :cond_6

    .line 538
    invoke-virtual {v11}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_6
    throw v2

    .line 555
    .end local v11    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v13    # "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :cond_7
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 556
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Requesting row count from server"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    :cond_8
    new-instance v7, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;

    new-instance v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v3, 0x3fb999999999999aL    # 0.1

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move/from16 v0, p2

    invoke-direct {v7, p0, p1, v0, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;ZLcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V

    .line 562
    .local v7, "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    :try_start_2
    new-instance v1, Lcom/cigna/coach/acm/ACMTask;

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v2

    sget-object v3, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    sget-object v4, Lcom/cigna/coach/acm/NetworkConstants;->API_GET_COUNT:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "{\"deviceId\": \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v6}, Lcom/cigna/coach/utils/CommonUtils;->deviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\"}"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct/range {v1 .. v7}, Lcom/cigna/coach/acm/ACMTask;-><init>(ILcom/cigna/coach/acm/ACMTask$ACMTaskType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/acm/IACMTaskCompleteListener;)V

    .line 564
    .local v1, "task":Lcom/cigna/coach/acm/ACMTask;
    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/cigna/coach/acm/ACMTask;->execute(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 565
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 566
    .end local v1    # "task":Lcom/cigna/coach/acm/ACMTask;
    :catch_0
    move-exception v10

    .line 567
    .local v10, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Error while requesting remote row count"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 537
    .end local v7    # "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v13    # "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :catchall_1
    move-exception v2

    move-object v11, v12

    .end local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v11    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_1
.end method

.method private startErase(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 13
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 667
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 668
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Starting erase process"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    :cond_0
    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v7

    .line 672
    .local v7, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {v7}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendEraseStartedBroadcast()V

    .line 674
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-interface {p2, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V

    .line 675
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v3, 0x3fa999999999999aL    # 0.05

    invoke-interface {p2, v2, v3, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 677
    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    sget-object v3, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/SharedPrefUtils;->getCoachSettings(Landroid/content/Context;Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 678
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 679
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string/jumbo v3, "startBackup : COACH_NOT_STARTED"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    :cond_1
    invoke-virtual {v7}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendEraseFailedBroadcast()V

    .line 682
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v3, 0x0

    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_BACKUP_DATA:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const-string v5, "coach not started"

    invoke-interface {p2, v2, v3, v4, v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 683
    const/4 v2, 0x0

    .line 720
    :goto_0
    return v2

    .line 686
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 689
    .local v10, "eraseStartTime":J
    :try_start_0
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v2, 0x3fa999999999999aL    # 0.05

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->serializeEraseRequest(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Lorg/json/JSONObject;

    move-result-object v12

    .line 692
    .local v12, "serializedData":Lorg/json/JSONObject;
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 693
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Sending json data to server"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    :cond_3
    new-instance v8, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;

    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object v1, v8

    move-object v2, p0

    move-wide v3, v10

    move-object v5, p1

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper;JLjava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V

    .line 699
    .local v8, "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 702
    .local v1, "requestId":I
    if-eqz v12, :cond_4

    .line 703
    :try_start_1
    new-instance v0, Lcom/cigna/coach/acm/ACMTask;

    sget-object v2, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    sget-object v3, Lcom/cigna/coach/acm/NetworkConstants;->API_SET_USER_DATA:Ljava/lang/String;

    invoke-virtual {v12}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    move-object v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/cigna/coach/acm/ACMTask;-><init>(ILcom/cigna/coach/acm/ACMTask$ACMTaskType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/acm/IACMTaskCompleteListener;)V

    .line 705
    .local v0, "task":Lcom/cigna/coach/acm/ACMTask;
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    invoke-interface {p2, v2, v3, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 706
    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/cigna/coach/acm/ACMTask;->execute(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 720
    .end local v0    # "task":Lcom/cigna/coach/acm/ACMTask;
    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    .line 708
    :catch_0
    move-exception v9

    .line 709
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v2, Lcom/cigna/coach/exceptions/CoachException;

    const-string v3, "Error while creating remote connection"

    invoke-direct {v2, v3, v9}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 711
    .end local v1    # "requestId":I
    .end local v8    # "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v12    # "serializedData":Lorg/json/JSONObject;
    :catch_1
    move-exception v9

    .line 713
    .restart local v9    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendEraseFailedBroadcast()V

    .line 714
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v3, 0x0

    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_DB_OPERATION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v2, v3, v4, v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 715
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "An error occured while startBackup"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method private startRestore(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 9
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    const-wide v2, 0x3fb999999999999aL    # 0.1

    .line 457
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 458
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Startng restore request"

    invoke-static {v1, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_0
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v7

    .line 461
    .local v7, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {v7}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendRestoreStartedBroadcast()V

    .line 463
    if-nez p2, :cond_1

    .line 464
    sget-object p2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->nullActivityListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 466
    :cond_1
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-interface {p2, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V

    .line 467
    new-instance v6, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;

    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    invoke-direct {v6, p0, p1, v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V

    .line 470
    .local v6, "callback":Lcom/cigna/coach/acm/IACMTaskCompleteListener;
    :try_start_0
    new-instance v0, Lcom/cigna/coach/acm/ACMTask;

    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v1

    sget-object v2, Lcom/cigna/coach/acm/ACMTask$ACMTaskType;->API:Lcom/cigna/coach/acm/ACMTask$ACMTaskType;

    sget-object v3, Lcom/cigna/coach/acm/NetworkConstants;->API_GET_USER_DATA:Ljava/lang/String;

    const-string/jumbo v4, "{\"startDate\":\"0000000000000\", \"endDate\":\"9999999999999\"}"

    const-string v5, ""

    invoke-direct/range {v0 .. v6}, Lcom/cigna/coach/acm/ACMTask;-><init>(ILcom/cigna/coach/acm/ACMTask$ACMTaskType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/cigna/coach/acm/IACMTaskCompleteListener;)V

    .line 472
    .local v0, "task":Lcom/cigna/coach/acm/ACMTask;
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v2, 0x3fb999999999999aL    # 0.1

    invoke-interface {p2, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 473
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/acm/ACMTask;->execute(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    const/4 v1, 0x1

    .line 477
    .end local v0    # "task":Lcom/cigna/coach/acm/ACMTask;
    :goto_0
    return v1

    .line 475
    :catch_0
    move-exception v8

    .line 476
    .local v8, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Error while creating remote connection"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public applyRestoreResponse(Ljava/lang/String;Lorg/json/JSONObject;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 22
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "dump"    # Lorg/json/JSONObject;
    .param p3, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 1068
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1069
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Restore data from server:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    :cond_0
    new-instance v13, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-direct {v13, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 1075
    .local v13, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v11, Lcom/cigna/coach/db/JournalDBManager;

    invoke-direct {v11, v13}, Lcom/cigna/coach/db/JournalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1077
    .local v11, "dbManager":Lcom/cigna/coach/db/JournalDBManager;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v9

    .line 1081
    .local v9, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    if-nez p3, :cond_1

    .line 1082
    :try_start_0
    sget-object p3, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->nullActivityListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 1085
    :cond_1
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/16 v5, 0x0

    const-wide v7, 0x3fd3333333333333L    # 0.3

    move-object/from16 v4, p3

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->parseRestoreResponse(Ljava/lang/String;Lorg/json/JSONObject;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;

    move-result-object v10

    .line 1087
    .local v10, "dataSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    if-eqz v10, :cond_9

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v4

    if-lez v4, :cond_9

    .line 1089
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v5, 0x3fd3333333333333L    # 0.3

    const-wide v7, 0x3fd999999999999aL    # 0.4

    move-object/from16 v4, p3

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->chooseDatasetToRestore(Ljava/util/Map;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;

    move-result-object v17

    .line 1091
    .local v17, "selectedDataset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    if-nez v17, :cond_3

    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "failed to select a dataset"

    invoke-direct {v4, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1150
    .end local v10    # "dataSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .end local v17    # "selectedDataset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    :catch_0
    move-exception v12

    .line 1151
    .local v12, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendRestoreFailedBroadcast()V

    .line 1152
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_DB_OPERATION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5, v6, v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 1153
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v5, "An error occured while applyRestoreResponse"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1154
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1155
    const/4 v4, 0x0

    .line 1157
    if-eqz v13, :cond_2

    .line 1158
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 1170
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_2
    :goto_0
    return v4

    .line 1094
    .restart local v10    # "dataSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .restart local v17    # "selectedDataset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    :cond_3
    :try_start_2
    invoke-virtual {v11}, Lcom/cigna/coach/db/JournalDBManager;->stopJournalling()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1097
    :try_start_3
    const-string v4, "backup_restore_sequence"

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->loadProperties(Ljava/lang/String;)Ljava/util/Properties;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v16

    .line 1103
    .local v16, "restoreOrder":Ljava/util/Properties;
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v11, v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->deleteUserTableEntiries(Lcom/cigna/coach/db/JournalDBManager;Ljava/util/Properties;)V

    .line 1105
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTimeInMillis()J

    move-result-wide v4

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v4, v5}, Lcom/cigna/coach/db/JournalDBManager;->deleteAllJournalEntriesForUser(Ljava/lang/String;J)V

    .line 1106
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5, v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 1108
    new-instance v3, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    const-wide v7, 0x3feccccccccccccdL    # 0.9

    move-object/from16 v4, p3

    invoke-direct/range {v3 .. v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    .line 1109
    .local v3, "tableListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    const/16 v19, 0x0

    .line 1111
    .local v19, "tablesProcessed":I
    invoke-virtual/range {v16 .. v16}, Ljava/util/Properties;->stringPropertyNames()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 1112
    .local v18, "table":Ljava/lang/String;
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    int-to-double v5, v0

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v5, v7

    invoke-virtual/range {v16 .. v16}, Ljava/util/Properties;->stringPropertyNames()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v7

    int-to-double v7, v7

    div-double/2addr v5, v7

    invoke-interface {v3, v4, v5, v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 1113
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1114
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Retrieving records for table:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    :cond_5
    const-string v4, "USR_API_CALL_LOG"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1117
    invoke-interface/range {v17 .. v18}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/List;

    .line 1118
    .local v15, "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    if-eqz v15, :cond_7

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_7

    .line 1119
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v11, v0, v1, v15}, Lcom/cigna/coach/db/JournalDBManager;->restoreTable(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1157
    .end local v3    # "tableListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .end local v10    # "dataSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    .end local v16    # "restoreOrder":Ljava/util/Properties;
    .end local v17    # "selectedDataset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    .end local v18    # "table":Ljava/lang/String;
    .end local v19    # "tablesProcessed":I
    :catchall_0
    move-exception v4

    if-eqz v13, :cond_6

    .line 1158
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_6
    throw v4

    .line 1098
    .restart local v10    # "dataSets":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    .restart local v17    # "selectedDataset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    :catch_1
    move-exception v12

    .line 1099
    .restart local v12    # "e":Ljava/lang/Exception;
    :try_start_5
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Unable to load the restore ordering from file \'%s\'."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "backup_restore_sequence"

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v12}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 1121
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v3    # "tableListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v15    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    .restart local v16    # "restoreOrder":Ljava/util/Properties;
    .restart local v18    # "table":Ljava/lang/String;
    .restart local v19    # "tablesProcessed":I
    :cond_7
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1122
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v5, "No records to inset"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1126
    .end local v15    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    :cond_8
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1127
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Skipping API call log table"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1133
    .end local v3    # "tableListener":Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v16    # "restoreOrder":Ljava/util/Properties;
    .end local v17    # "selectedDataset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    .end local v18    # "table":Ljava/lang/String;
    .end local v19    # "tablesProcessed":I
    :cond_9
    invoke-static {}, Lcom/cigna/coach/db/UserDBManager;->clearCache()V

    .line 1135
    new-instance v21, Lcom/cigna/coach/db/UserMetricsDBManager;

    move-object/from16 v0, v21

    invoke-direct {v0, v13}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 1136
    .local v21, "userMetricDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    const/4 v4, 0x1

    sget-object v5, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Lcom/cigna/coach/db/UserMetricsDBManager;->setBackupType(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;)V

    .line 1138
    invoke-virtual {v11}, Lcom/cigna/coach/db/JournalDBManager;->resumeJournalling()V

    .line 1139
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v5, 0x3fee666666666666L    # 0.95

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5, v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 1141
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V

    .line 1142
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5, v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 1144
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1145
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v5, "Restore complete"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    :cond_a
    invoke-virtual {v9}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendRestoreCompletedBroadcast()V

    .line 1149
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v5, 0x1

    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v4, v5, v6, v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1157
    if-eqz v13, :cond_b

    .line 1158
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 1163
    :cond_b
    :try_start_6
    new-instance v20, Lcom/cigna/coach/Tracker;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Lcom/cigna/coach/Tracker;-><init>(Landroid/content/Context;)V

    .line 1164
    .local v20, "tracker":Lcom/cigna/coach/Tracker;
    sget-object v4, Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;->DAILY:Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lcom/cigna/coach/Tracker;->checkForInactivity(Lcom/cigna/coach/interfaces/ITracker$ActivityEventType;)Ljava/util/List;
    :try_end_6
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_6 .. :try_end_6} :catch_2

    .line 1170
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1165
    .end local v20    # "tracker":Lcom/cigna/coach/Tracker;
    :catch_2
    move-exception v12

    .line 1166
    .local v12, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v4, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error in checkForInactivity after restore: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v12}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1167
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-direct {v4, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public backup(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 4
    .param p1, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 489
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V

    .line 490
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v2, v0}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 491
    .local v2, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual {v2}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v1

    .line 492
    .local v1, "userName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 495
    :cond_0
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3, p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->startBackup(Ljava/lang/String;ZLcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v3

    return v3
.end method

.method public erase(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 4
    .param p1, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 659
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V

    .line 660
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v2, v0}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 661
    .local v2, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual {v2}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v1

    .line 662
    .local v1, "userName":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 663
    invoke-direct {p0, v1, p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->startErase(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v3

    return v3
.end method

.method public restore(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    .line 655
    const-string v0, "_default"

    invoke-direct {p0, v0, p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->startRestore(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    move-result v0

    return v0
.end method

.method public serializeBackupRequest(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Lorg/json/JSONObject;
    .locals 18
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "backupType"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .param p3, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 734
    const/4 v12, 0x0

    .line 736
    .local v12, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v2, 0x3fa999999999999aL    # 0.05

    move-object/from16 v0, p3

    invoke-interface {v0, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 738
    new-instance v13, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {v13, v1, v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 739
    .end local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v13, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v8, Lcom/cigna/coach/db/JournalDBManager;

    invoke-direct {v8, v13}, Lcom/cigna/coach/db/JournalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 740
    .local v8, "dbManager":Lcom/cigna/coach/db/JournalDBManager;
    const/4 v11, 0x0

    .line 741
    .local v11, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/JournalEntry;>;"
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    move-object/from16 v0, p2

    if-ne v0, v1, :cond_2

    .line 742
    invoke-virtual {v8}, Lcom/cigna/coach/db/JournalDBManager;->getDbVersion()Lcom/cigna/coach/dataobjects/DbVersion;

    move-result-object v9

    .line 743
    .local v9, "dbVersion":Lcom/cigna/coach/dataobjects/DbVersion;
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v16

    .line 744
    .local v16, "userId":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->getUserTables(Landroid/content/Context;)Ljava/util/List;

    move-result-object v15

    .line 745
    .local v15, "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/DbVersion;->getCodeBaseId()I

    move-result v7

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/DbVersion;->getDbVersion()I

    move-result v17

    new-instance v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v3, 0x3fc999999999999aL    # 0.2

    const-wide v5, 0x3feccccccccccccdL    # 0.9

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object v2, v8

    move-object v3, v15

    move/from16 v4, v16

    move v5, v7

    move/from16 v6, v17

    move-object v7, v1

    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/db/JournalDBManager;->generateAllJournalEntriesFromUserTables(Ljava/util/List;IIILcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/List;

    move-result-object v11

    .line 752
    .end local v9    # "dbVersion":Lcom/cigna/coach/dataobjects/DbVersion;
    .end local v15    # "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v16    # "userId":I
    :goto_0
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 753
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " journal entries form db"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 756
    :cond_0
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v2, 0x3feccccccccccccdL    # 0.9

    move-object/from16 v0, p3

    invoke-interface {v0, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 757
    const-string v1, "N"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v11}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->serializeSendUserDataRequest(Ljava/lang/String;Ljava/util/List;)Lorg/json/JSONObject;

    move-result-object v14

    .line 758
    .local v14, "jsonResult":Lorg/json/JSONObject;
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p3

    invoke-interface {v0, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 767
    if-eqz v13, :cond_1

    .line 768
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_1
    return-object v14

    .line 749
    .end local v14    # "jsonResult":Lorg/json/JSONObject;
    :cond_2
    :try_start_2
    new-instance v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide v3, 0x3fc999999999999aL    # 0.2

    const-wide v5, 0x3feccccccccccccdL    # 0.9

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v1}, Lcom/cigna/coach/db/JournalDBManager;->getAllJournalEntriesForUser(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v11

    goto :goto_0

    .line 762
    .end local v8    # "dbManager":Lcom/cigna/coach/db/JournalDBManager;
    .end local v11    # "entries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/JournalEntry;>;"
    .end local v13    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_0
    move-exception v10

    .line 763
    .local v10, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v2, "An error occured while serializeBackupRequest"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 764
    sget-object v1, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    new-instance v1, Lcom/cigna/coach/exceptions/CoachException;

    const-string v2, "Error while serializeBackupRequest."

    invoke-direct {v1, v2, v10}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 767
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    :goto_2
    if-eqz v12, :cond_3

    .line 768
    invoke-virtual {v12}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_3
    throw v1

    .line 767
    .end local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v13    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_1
    move-exception v1

    move-object v12, v13

    .end local v13    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_2

    .line 762
    .end local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v13    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_1
    move-exception v10

    move-object v12, v13

    .end local v13    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v12    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_1
.end method

.method public serializeEraseRequest(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Lorg/json/JSONObject;
    .locals 5
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 774
    :try_start_0
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v3, 0x3fb999999999999aL    # 0.1

    invoke-interface {p2, v2, v3, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 775
    const-string v2, "Y"

    sget-object v3, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-virtual {p0, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->serializeSendUserDataRequest(Ljava/lang/String;Ljava/util/List;)Lorg/json/JSONObject;

    move-result-object v1

    .line 776
    .local v1, "jsonResult":Lorg/json/JSONObject;
    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-interface {p2, v2, v3, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 777
    return-object v1

    .line 778
    .end local v1    # "jsonResult":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 779
    .local v0, "e":Lorg/json/JSONException;
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    const-string v3, "An error occured while serializeEraseRequest"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    sget-object v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    new-instance v2, Lcom/cigna/coach/exceptions/CoachException;

    const-string v3, "Error while serializeEraseRequest."

    invoke-direct {v2, v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public serializeSendUserDataRequest(Ljava/lang/String;Ljava/util/List;)Lorg/json/JSONObject;
    .locals 6
    .param p1, "transIndicator"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/JournalEntry;",
            ">;)",
            "Lorg/json/JSONObject;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 786
    .local p2, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/JournalEntry;>;"
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 788
    .local v3, "jsonResult":Lorg/json/JSONObject;
    const-string v4, "deviceId"

    iget-object v5, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;

    invoke-static {v5}, Lcom/cigna/coach/utils/CommonUtils;->deviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 789
    const-string/jumbo v4, "transIndicator"

    invoke-virtual {v3, v4, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 790
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 791
    .local v2, "jrnlEntryList":Lorg/json/JSONArray;
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/JournalEntry;

    .line 792
    .local v0, "entry":Lcom/cigna/coach/dataobjects/JournalEntry;
    invoke-direct {p0, v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->serializeJournalRow(Lcom/cigna/coach/dataobjects/JournalEntry;)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    .line 795
    .end local v0    # "entry":Lcom/cigna/coach/dataobjects/JournalEntry;
    :cond_0
    const-string/jumbo v4, "usrActyJrnlList"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 801
    return-object v3
.end method
