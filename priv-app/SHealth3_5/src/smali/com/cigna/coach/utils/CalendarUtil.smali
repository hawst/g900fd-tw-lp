.class public Lcom/cigna/coach/utils/CalendarUtil;
.super Ljava/lang/Object;
.source "CalendarUtil.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SimpleDateFormat"
    }
.end annotation


# static fields
.field public static final DAYS_IN_WEEK:I = 0x7

.field public static final DEFAULT_PATTERN:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

.field public static final MILLISECONDS_IN_DAY:J = 0x5265c00L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static absDateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 1
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 106
    invoke-static {p0, p1}, Lcom/cigna/coach/utils/CalendarUtil;->dateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public static absDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 1
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 82
    invoke-static {p0, p1}, Lcom/cigna/coach/utils/CalendarUtil;->daysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public static absMonthsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 1
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 127
    invoke-static {p0, p1}, Lcom/cigna/coach/utils/CalendarUtil;->monthsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public static absYearsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 1
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 215
    invoke-static {p0, p1}, Lcom/cigna/coach/utils/CalendarUtil;->yearsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    return v0
.end method

.method public static compareFor6MonthsApart(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 2
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 152
    invoke-static {p0}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p0

    .line 153
    invoke-static {p1}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    .line 155
    const/4 v0, 0x2

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 156
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    .line 158
    invoke-virtual {p0, p1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    return v0
.end method

.method public static createTodayCalendar()Ljava/util/Calendar;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    return-object v0
.end method

.method public static dateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 11
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 119
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v3

    .line 120
    .local v3, "startDay":Ljava/util/Calendar;
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v0

    .line 121
    .local v0, "endDay":Ljava/util/Calendar;
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v8

    int-to-long v8, v8

    add-long v4, v6, v8

    .line 122
    .local v4, "startMS":J
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v8

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v8

    int-to-long v8, v8

    add-long v1, v6, v8

    .line 123
    .local v1, "endMS":J
    sub-long v6, v1, v4

    const-wide/32 v8, 0x5265c00

    div-long/2addr v6, v8

    long-to-int v6, v6

    return v6
.end method

.method public static daysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 10
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 94
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 95
    .local v4, "milliseconds1":J
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    .line 99
    .local v6, "milliseconds2":J
    cmp-long v8, v6, v4

    if-ltz v8, :cond_0

    sub-long v0, v6, v4

    .line 100
    .local v0, "diff":J
    :goto_0
    const-wide/32 v8, 0x5265c00

    div-long v2, v0, v8

    .line 102
    .local v2, "diffDays":J
    cmp-long v8, v6, v4

    if-ltz v8, :cond_1

    .end local v2    # "diffDays":J
    :goto_1
    long-to-int v8, v2

    return v8

    .line 99
    .end local v0    # "diff":J
    :cond_0
    sub-long v0, v4, v6

    goto :goto_0

    .line 102
    .restart local v0    # "diff":J
    .restart local v2    # "diffDays":J
    :cond_1
    neg-long v2, v2

    goto :goto_1
.end method

.method public static duplicateCalendar(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 2
    .param p0, "in"    # Ljava/util/Calendar;

    .prologue
    .line 382
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 383
    .local v0, "out":Ljava/util/Calendar;
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 384
    return-object v0
.end method

.method public static formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 313
    const/4 v1, 0x0

    .line 314
    .local v1, "returnVal":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 315
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 316
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 319
    .end local v0    # "dateFormat":Ljava/text/SimpleDateFormat;
    :cond_0
    return-object v1
.end method

.method public static formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;
    .param p1, "pattern"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 349
    const/4 v1, 0x0

    .line 350
    .local v1, "returnVal":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 351
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 352
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 354
    .end local v0    # "dateFormat":Ljava/text/SimpleDateFormat;
    :cond_0
    return-object v1
.end method

.method public static fromCalendarToMilliSecs(Ljava/util/Calendar;)J
    .locals 2
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 57
    if-nez p0, :cond_0

    .line 58
    const-wide/16 v0, 0x0

    .line 60
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static fromDaysToMilliSecs(I)J
    .locals 4
    .param p0, "days"    # I

    .prologue
    .line 68
    int-to-long v0, p0

    const-wide/32 v2, 0x5265c00

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static fromMilliSecToCalendar(J)Ljava/util/Calendar;
    .locals 2
    .param p0, "milliseconds"    # J

    .prologue
    .line 50
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 51
    .local v0, "calendar":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p0, p1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 53
    return-object v0
.end method

.method public static getEndOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 4
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    const/16 v3, 0x3b

    .line 234
    invoke-static {p0}, Lcom/cigna/coach/utils/CalendarUtil;->toDefaultTimeZone(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    .line 235
    .local v0, "c":Ljava/util/Calendar;
    const/16 v1, 0xb

    const/16 v2, 0x17

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 236
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 237
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 238
    const/16 v1, 0xe

    const/16 v2, 0x3e7

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 239
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    .line 240
    return-object v0
.end method

.method public static getEndOfDayBefore(J)Ljava/util/Calendar;
    .locals 3
    .param p0, "dateTime"    # J
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 289
    invoke-static {p0, p1}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v0

    .line 290
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 291
    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->getEndOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v1

    return-object v1
.end method

.method public static getEndOfDayBefore(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 276
    invoke-static {p0}, Lcom/cigna/coach/utils/CalendarUtil;->toDefaultTimeZone(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    .line 277
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x6

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 278
    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->getEndOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v1

    return-object v1
.end method

.method public static getStartOfDay(J)Ljava/util/Calendar;
    .locals 1
    .param p0, "dateTime"    # J

    .prologue
    .line 266
    invoke-static {p0, p1}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public static getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    const/4 v2, 0x0

    .line 250
    invoke-static {p0}, Lcom/cigna/coach/utils/CalendarUtil;->toDefaultTimeZone(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    .line 251
    .local v0, "c":Ljava/util/Calendar;
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 252
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 253
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 254
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 255
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    .line 256
    return-object v0
.end method

.method public static getStartOfDayAfter(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 3
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 300
    invoke-virtual {p0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 301
    .local v0, "c":Ljava/util/Calendar;
    const/4 v1, 0x6

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 302
    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v1

    return-object v1
.end method

.method public static getStartOfToday()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 372
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public static getStartOfTodayInMilliSec()J
    .locals 2

    .prologue
    .line 363
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static monthsAndDaysBetween(Ljava/util/Calendar;Ljava/util/Calendar;)F
    .locals 2
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    .line 136
    const/4 v1, 0x0

    .line 137
    .local v1, "monthNDays":F
    invoke-static {p0, p1}, Lcom/cigna/coach/utils/CalendarUtil;->compareFor6MonthsApart(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v0

    .line 139
    .local v0, "compare":I
    if-gez v0, :cond_0

    .line 140
    const v1, 0x40c051ec    # 6.01f

    .line 147
    :goto_0
    return v1

    .line 141
    :cond_0
    if-lez v0, :cond_1

    .line 142
    const v1, 0x40bf5c29    # 5.98f

    goto :goto_0

    .line 144
    :cond_1
    const/high16 v1, 0x40c00000    # 6.0f

    goto :goto_0
.end method

.method public static monthsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 4
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 131
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int v0, v1, v2

    .line 132
    .local v0, "yearDiff":I
    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int/2addr v1, v2

    mul-int/lit8 v2, v0, 0xc

    add-int/2addr v1, v2

    return v1
.end method

.method public static parseCalendarFromString(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 5
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 329
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string/jumbo v4, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ"

    invoke-direct {v1, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 331
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 332
    .local v0, "date":Ljava/util/Date;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v3

    .line 333
    .local v3, "result":Ljava/util/Calendar;
    invoke-virtual {v3, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    .end local v0    # "date":Ljava/util/Date;
    .end local v3    # "result":Ljava/util/Calendar;
    :goto_0
    return-object v3

    .line 335
    :catch_0
    move-exception v2

    .line 336
    .local v2, "e":Ljava/text/ParseException;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static toDefaultTimeZone(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 2
    .param p0, "calendar"    # Ljava/util/Calendar;

    .prologue
    .line 64
    invoke-static {p0}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public static yearsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I
    .locals 5
    .param p0, "startCalendar"    # Ljava/util/Calendar;
    .param p1, "endCalendar"    # Ljava/util/Calendar;

    .prologue
    const/4 v4, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 219
    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sub-int v0, v1, v2

    .line 220
    .local v0, "diff":I
    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-le v1, v2, :cond_1

    .line 222
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 224
    :cond_1
    return v0
.end method
