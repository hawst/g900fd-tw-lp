.class public final enum Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
.super Ljava/lang/Enum;
.source "CoachContentTagValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/CoachContentTagValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TagValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

.field public static final enum CONTENT:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

.field public static final enum CONTENT_ID:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 26
    new-instance v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    const-string v1, "CONTENT_ID"

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT_ID:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    new-instance v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    const-string v1, "CONTENT"

    invoke-direct {v0, v1, v3}, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    sget-object v1, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT_ID:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->$VALUES:[Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->$VALUES:[Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    invoke-virtual {v0}, [Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;

    return-object v0
.end method
