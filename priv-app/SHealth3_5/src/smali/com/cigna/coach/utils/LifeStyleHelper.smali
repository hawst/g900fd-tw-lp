.class public Lcom/cigna/coach/utils/LifeStyleHelper;
.super Ljava/lang/Object;
.source "LifeStyleHelper.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field static categoryInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CategoryInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/cigna/coach/utils/LifeStyleHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/cigna/coach/utils/LifeStyleHelper;->categoryInfo:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/utils/LifeStyleHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 37
    iput-object p1, p0, Lcom/cigna/coach/utils/LifeStyleHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 38
    return-void
.end method


# virtual methods
.method public getCategoryIdMsg(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Ljava/util/List;
    .locals 8
    .param p1, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CategoryInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 41
    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 42
    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getCategoryIdMsg: START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :cond_0
    const/4 v4, 0x0

    .line 49
    .local v4, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    :try_start_0
    new-instance v3, Lcom/cigna/coach/db/LifeStyleDBManager;

    iget-object v6, p0, Lcom/cigna/coach/utils/LifeStyleHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v3, v6}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 50
    .local v3, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->categoryInfo:Ljava/util/List;

    if-nez v6, :cond_1

    .line 52
    invoke-virtual {v3}, Lcom/cigna/coach/db/LifeStyleDBManager;->getAllCategories()Ljava/util/List;

    move-result-object v6

    sput-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->categoryInfo:Ljava/util/List;

    .line 55
    :cond_1
    new-instance v1, Lcom/cigna/coach/utils/ContentHelper;

    iget-object v6, p0, Lcom/cigna/coach/utils/LifeStyleHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v1, v6}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 56
    .local v1, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->categoryInfo:Ljava/util/List;

    invoke-virtual {v1, v6}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 59
    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {p1, v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 60
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    .end local v4    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .local v5, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    :try_start_1
    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->categoryInfo:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CategoryInfo;

    .line 62
    .local v0, "catInfo":Lcom/cigna/coach/apiobjects/CategoryInfo;
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CategoryInfo;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 63
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v0    # "catInfo":Lcom/cigna/coach/apiobjects/CategoryInfo;
    :cond_3
    move-object v4, v5

    .line 75
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .restart local v4    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    :goto_0
    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 76
    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getCategoryIdMsg: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_4
    return-object v4

    .line 71
    :cond_5
    :try_start_2
    new-instance v5, Ljava/util/ArrayList;

    sget-object v6, Lcom/cigna/coach/utils/LifeStyleHelper;->categoryInfo:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .end local v4    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .restart local v5    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    move-object v4, v5

    .end local v5    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .restart local v4    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    goto :goto_0

    .line 73
    .end local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v3    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    :catchall_0
    move-exception v6

    :goto_1
    throw v6

    .end local v4    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .restart local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v3    # "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    .restart local v5    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    .restart local v4    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    goto :goto_1
.end method

.method public getScoreBackgroundImage(Ljava/lang/String;)I
    .locals 19
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 82
    const/4 v13, -0x1

    .line 84
    .local v13, "backgroundImageContentId":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v17

    .line 86
    .local v17, "nowInMillis":J
    const-wide/32 v8, 0x240c8400

    sub-long v8, v17, v8

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 87
    .local v3, "last7DaysStartDate":J
    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/CalendarUtil;->getEndOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    .line 89
    .local v5, "last7DaysEndDate":J
    new-instance v1, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/LifeStyleHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v1, v2}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 90
    .local v1, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    new-instance v7, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/utils/LifeStyleHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v7, v2}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .local v7, "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    move-object/from16 v2, p1

    .line 92
    invoke-virtual/range {v1 .. v6}, Lcom/cigna/coach/db/GoalDBManager;->getCompletedGoalsCountBetweenDays(Ljava/lang/String;JJ)I

    move-result v15

    .line 93
    .local v15, "noOfGoalsCompletedInLast7Days":I
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "noOfGoalsCompletedInLast7Days = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "last7DaysStartDate = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v9

    invoke-static {v9}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "last7DaysEndDate = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v9

    invoke-static {v9}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    if-lez v15, :cond_2

    .line 102
    const/16 v13, 0x1190

    .line 136
    :goto_0
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Background content id= "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_1
    return v13

    .line 106
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalsCount(Ljava/lang/String;)I

    move-result v14

    .line 107
    .local v14, "noOfActiveGoals":I
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "noOfActiveGoals = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_3
    if-lez v14, :cond_7

    move-object/from16 v8, p1

    move-wide v9, v3

    move-wide v11, v5

    .line 112
    invoke-virtual/range {v7 .. v12}, Lcom/cigna/coach/db/MissionDBManager;->getCompletedMissionsCountBetweenDays(Ljava/lang/String;JJ)I

    move-result v16

    .line 113
    .local v16, "noOfMissionsCompletedInLast7Days":I
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 114
    sget-object v2, Lcom/cigna/coach/utils/LifeStyleHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "noOfMissionsCompletedInLast7Days = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_4
    const/4 v2, 0x3

    move/from16 v0, v16

    if-lt v0, v2, :cond_5

    .line 119
    const/16 v13, 0x118f

    goto :goto_0

    .line 121
    :cond_5
    const/4 v2, 0x1

    move/from16 v0, v16

    if-lt v0, v2, :cond_6

    .line 123
    const/16 v13, 0x118e

    goto/16 :goto_0

    .line 127
    :cond_6
    const/16 v13, 0x118d

    goto/16 :goto_0

    .line 132
    .end local v16    # "noOfMissionsCompletedInLast7Days":I
    :cond_7
    const/16 v13, 0x118c

    goto/16 :goto_0
.end method
