.class public final enum Lcom/cigna/coach/utils/BadgeType;
.super Ljava/lang/Enum;
.source "BadgeType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/utils/BadgeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_FIFTEENTH:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_FIRST:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_INCREASE_CATEGORY_SCORE:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_ONE_PER_CATEGORY:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_ONE_PER_EXERCISE_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_ONE_PER_FOOD_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_THREE_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_THREE_FOOD:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_THREE_SLEEP:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_THREE_STRESS:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum GOAL_THREE_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum LIFESTYLE_COMPLETE_ALL:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum LIFESTYLE_GREEN_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum LIFESTYLE_GREEN_FOOD:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum LIFESTYLE_GREEN_SLEEP:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum LIFESTYLE_GREEN_STRESS:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum LIFESTYLE_GREEN_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum LIFESTYLE_INCREASE_SCORE:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_COMPLETE_AFTER_FAILING:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_FIFTEEN_ACTION:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_FIFTY_ACTION:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_FIRST:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_FIVE_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_FIVE_PREPARE:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_FIVE_REFLECT:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_FOUR_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_SECOND_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_SEVEN_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_SIX_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

.field public static final enum MISSION_THIRD_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;


# instance fields
.field private final badgeId_:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 20
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "LIFESTYLE_GREEN_EXERCISE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

    .line 21
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "LIFESTYLE_GREEN_FOOD"

    invoke-direct {v0, v1, v4, v5}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_FOOD:Lcom/cigna/coach/utils/BadgeType;

    .line 22
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "LIFESTYLE_GREEN_SLEEP"

    invoke-direct {v0, v1, v5, v6}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_SLEEP:Lcom/cigna/coach/utils/BadgeType;

    .line 23
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "LIFESTYLE_GREEN_STRESS"

    invoke-direct {v0, v1, v6, v7}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_STRESS:Lcom/cigna/coach/utils/BadgeType;

    .line 24
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "LIFESTYLE_GREEN_WEIGHT"

    invoke-direct {v0, v1, v7, v8}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

    .line 25
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "LIFESTYLE_COMPLETE_ALL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_COMPLETE_ALL:Lcom/cigna/coach/utils/BadgeType;

    .line 26
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "LIFESTYLE_INCREASE_SCORE"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_INCREASE_SCORE:Lcom/cigna/coach/utils/BadgeType;

    .line 29
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_FIRST"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_FIRST:Lcom/cigna/coach/utils/BadgeType;

    .line 30
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_ONE_PER_CATEGORY"

    const/16 v2, 0x8

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_CATEGORY:Lcom/cigna/coach/utils/BadgeType;

    .line 31
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_ONE_PER_FOOD_SUBCATEGORY"

    const/16 v2, 0x9

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_FOOD_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

    .line 32
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_ONE_PER_EXERCISE_SUBCATEGORY"

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_EXERCISE_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

    .line 33
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_THREE_EXERCISE"

    const/16 v2, 0xb

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

    .line 34
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_THREE_FOOD"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_FOOD:Lcom/cigna/coach/utils/BadgeType;

    .line 35
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_THREE_SLEEP"

    const/16 v2, 0xd

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_SLEEP:Lcom/cigna/coach/utils/BadgeType;

    .line 36
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_THREE_STRESS"

    const/16 v2, 0xe

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_STRESS:Lcom/cigna/coach/utils/BadgeType;

    .line 37
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_THREE_WEIGHT"

    const/16 v2, 0xf

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

    .line 38
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_FIFTEENTH"

    const/16 v2, 0x10

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_FIFTEENTH:Lcom/cigna/coach/utils/BadgeType;

    .line 39
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "GOAL_INCREASE_CATEGORY_SCORE"

    const/16 v2, 0x11

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->GOAL_INCREASE_CATEGORY_SCORE:Lcom/cigna/coach/utils/BadgeType;

    .line 42
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_FIRST"

    const/16 v2, 0x12

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIRST:Lcom/cigna/coach/utils/BadgeType;

    .line 43
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_SECOND_THIS_WEEK"

    const/16 v2, 0x13

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_SECOND_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;

    .line 44
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_THIRD_THIS_WEEK"

    const/16 v2, 0x14

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_THIRD_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;

    .line 45
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_FOUR_PER_WEEK"

    const/16 v2, 0x15

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_FOUR_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    .line 46
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_FIVE_PER_WEEK"

    const/16 v2, 0x16

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    .line 47
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_SIX_PER_WEEK"

    const/16 v2, 0x17

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_SIX_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    .line 48
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_SEVEN_PER_WEEK"

    const/16 v2, 0x18

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_SEVEN_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    .line 49
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_COMPLETE_AFTER_FAILING"

    const/16 v2, 0x19

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_COMPLETE_AFTER_FAILING:Lcom/cigna/coach/utils/BadgeType;

    .line 50
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_FIVE_PREPARE"

    const/16 v2, 0x1a

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_PREPARE:Lcom/cigna/coach/utils/BadgeType;

    .line 51
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_FIVE_REFLECT"

    const/16 v2, 0x1b

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_REFLECT:Lcom/cigna/coach/utils/BadgeType;

    .line 52
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_FIFTEEN_ACTION"

    const/16 v2, 0x1c

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIFTEEN_ACTION:Lcom/cigna/coach/utils/BadgeType;

    .line 53
    new-instance v0, Lcom/cigna/coach/utils/BadgeType;

    const-string v1, "MISSION_FIFTY_ACTION"

    const/16 v2, 0x1d

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/BadgeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIFTY_ACTION:Lcom/cigna/coach/utils/BadgeType;

    .line 18
    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/cigna/coach/utils/BadgeType;

    const/4 v1, 0x0

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_FOOD:Lcom/cigna/coach/utils/BadgeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_SLEEP:Lcom/cigna/coach/utils/BadgeType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_STRESS:Lcom/cigna/coach/utils/BadgeType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_COMPLETE_ALL:Lcom/cigna/coach/utils/BadgeType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_INCREASE_SCORE:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_FIRST:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_CATEGORY:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_FOOD_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_EXERCISE_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_FOOD:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_SLEEP:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_STRESS:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_FIFTEENTH:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->GOAL_INCREASE_CATEGORY_SCORE:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIRST:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_SECOND_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_THIRD_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_FOUR_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_SIX_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_SEVEN_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_COMPLETE_AFTER_FAILING:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_PREPARE:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_REFLECT:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIFTEEN_ACTION:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIFTY_ACTION:Lcom/cigna/coach/utils/BadgeType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/coach/utils/BadgeType;->$VALUES:[Lcom/cigna/coach/utils/BadgeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "badgeId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, Lcom/cigna/coach/utils/BadgeType;->badgeId_:I

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/BadgeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/cigna/coach/utils/BadgeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/BadgeType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/utils/BadgeType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/cigna/coach/utils/BadgeType;->$VALUES:[Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual {v0}, [Lcom/cigna/coach/utils/BadgeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/utils/BadgeType;

    return-object v0
.end method


# virtual methods
.method public getBadgeId()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/cigna/coach/utils/BadgeType;->badgeId_:I

    return v0
.end method
