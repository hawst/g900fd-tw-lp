.class public Lcom/cigna/coach/utils/BadgeAwarder;
.super Ljava/lang/Object;
.source "BadgeAwarder.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private final badgeDbManager_:Lcom/cigna/coach/db/BadgeDBManager;

.field private final contentHelper_:Lcom/cigna/coach/utils/ContentHelper;

.field private final goalDbManager_:Lcom/cigna/coach/db/GoalDBManager;

.field private final lifestyleDbManager_:Lcom/cigna/coach/db/LifeStyleDBManager;

.field private final missionDbManager_:Lcom/cigna/coach/db/MissionDBManager;

.field private final userId_:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/cigna/coach/utils/BadgeAwarder;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/BadgeAwarder;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/cigna/coach/db/BadgeDBManager;

    invoke-direct {v0, p2}, Lcom/cigna/coach/db/BadgeDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/BadgeAwarder;->badgeDbManager_:Lcom/cigna/coach/db/BadgeDBManager;

    .line 61
    new-instance v0, Lcom/cigna/coach/db/GoalDBManager;

    invoke-direct {v0, p2}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/BadgeAwarder;->goalDbManager_:Lcom/cigna/coach/db/GoalDBManager;

    .line 62
    new-instance v0, Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {v0, p2}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/BadgeAwarder;->missionDbManager_:Lcom/cigna/coach/db/MissionDBManager;

    .line 63
    new-instance v0, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-direct {v0, p2}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/BadgeAwarder;->lifestyleDbManager_:Lcom/cigna/coach/db/LifeStyleDBManager;

    .line 64
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v0, p2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/BadgeAwarder;->contentHelper_:Lcom/cigna/coach/utils/ContentHelper;

    .line 65
    return-void
.end method

.method private getEligibleBadgeIds()Ljava/util/List;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 111
    new-instance v17, Ljava/util/LinkedList;

    invoke-direct/range {v17 .. v17}, Ljava/util/LinkedList;-><init>()V

    .line 116
    .local v17, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->lifestyleDbManager_:Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/LifeStyleDBManager;->hasEverImprovedBetweenConsecutiveAssessments(Ljava/lang/String;)Z

    move-result v6

    .line 117
    .local v6, "everImproved":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->lifestyleDbManager_:Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/LifeStyleDBManager;->getPerCategoryBestEverScores(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v16

    .line 118
    .local v16, "perCategoryScores":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 119
    .local v7, "exerciseScore":I
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 120
    .local v8, "foodScore":I
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 121
    .local v18, "sleepScore":I
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 122
    .local v19, "stressScore":I
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v20

    .line 124
    .local v20, "weightScore":I
    invoke-static {v7}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 125
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_0
    invoke-static {v8}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_1

    .line 129
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_FOOD:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_1
    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 133
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_SLEEP:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_2
    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 137
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_STRESS:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_3
    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 141
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_GREEN_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    :cond_4
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v7, v0, :cond_5

    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v8, v0, :cond_5

    const/16 v21, -0x1

    move/from16 v0, v18

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    const/16 v21, -0x1

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    .line 149
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_COMPLETE_ALL:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    :cond_5
    if-eqz v6, :cond_6

    .line 153
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->LIFESTYLE_INCREASE_SCORE:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->lifestyleDbManager_:Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/LifeStyleDBManager;->hasImprovedScoreRatingByCompletingGoals(Ljava/lang/String;)Z

    move-result v12

    .line 160
    .local v12, "hasImprovedScoreByCompletingGaols":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->goalDbManager_:Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/GoalDBManager;->getCompletedGoalCount(Ljava/lang/String;)I

    move-result v3

    .line 161
    .local v3, "completedGoalCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->goalDbManager_:Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/GoalDBManager;->getCategoriesWithCompletedGoalsCount(Ljava/lang/String;)I

    move-result v2

    .line 162
    .local v2, "categoriesWithCompletedGoalsCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->goalDbManager_:Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/GoalDBManager;->getSubcategoriesWithCompletedGoalCountPerCategory(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v10

    .line 163
    .local v10, "goalSubcategoryWithCompletedCount":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->lifestyleDbManager_:Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/db/LifeStyleDBManager;->getNumberOfSubcategoriesPerCategory()Ljava/util/Map;

    move-result-object v9

    .line 164
    .local v9, "goalCategorySubcategoryCount":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->goalDbManager_:Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/GoalDBManager;->getCompletedGoalCountPerCategory(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v11

    .line 166
    .local v11, "goalsCompletedPerCategory":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    const/16 v21, 0x1

    move/from16 v0, v21

    if-lt v3, v0, :cond_7

    .line 167
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_FIRST:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    :cond_7
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v21

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    if-lt v2, v0, :cond_8

    .line 171
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_CATEGORY:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_8
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    sget-object v22, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v22

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 175
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_FOOD_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    :cond_9
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v21

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    sget-object v22, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v22

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 179
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_ONE_PER_EXERCISE_SUBCATEGORY:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    :cond_a
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->PHYSICAL_ACTIVITY:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v21

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_b

    .line 183
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_EXERCISE:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    :cond_b
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v21

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_c

    .line 187
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_FOOD:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    :cond_c
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->SLEEP:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v21

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_d

    .line 191
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_SLEEP:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_d
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->STRESS:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v21

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_e

    .line 195
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_STRESS:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_e
    sget-object v21, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, v21

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_f

    .line 199
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_THREE_WEIGHT:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_f
    const/16 v21, 0xf

    move/from16 v0, v21

    if-lt v3, v0, :cond_10

    .line 203
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_FIFTEENTH:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    :cond_10
    if-eqz v12, :cond_11

    .line 207
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->GOAL_INCREASE_CATEGORY_SCORE:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->missionDbManager_:Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/MissionDBManager;->getCompletedMissionCount(Ljava/lang/String;)I

    move-result v4

    .line 214
    .local v4, "completedMissionCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->missionDbManager_:Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    const/16 v23, 0x7

    invoke-virtual/range {v21 .. v23}, Lcom/cigna/coach/db/MissionDBManager;->getRecentlyCompletedMissionCount(Ljava/lang/String;I)I

    move-result v5

    .line 215
    .local v5, "completedMissionsThisWeek":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->missionDbManager_:Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/MissionDBManager;->getMissionsCompletedAfterFailureCount(Ljava/lang/String;)I

    move-result v13

    .line 216
    .local v13, "missionCompletedAfterFailureCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->missionDbManager_:Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/MissionDBManager;->getFrequenciesOfCompletedMissions(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v14

    .line 217
    .local v14, "missionFrequencies":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->missionDbManager_:Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/db/MissionDBManager;->getCompletedMissionCountPerStage(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v15

    .line 219
    .local v15, "missionsCompletedPerStage":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;Ljava/lang/Integer;>;"
    const/16 v21, 0x1

    move/from16 v0, v21

    if-lt v4, v0, :cond_12

    .line 220
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIRST:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    :cond_12
    const/16 v21, 0x2

    move/from16 v0, v21

    if-lt v5, v0, :cond_13

    .line 224
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_SECOND_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    :cond_13
    const/16 v21, 0x3

    move/from16 v0, v21

    if-lt v5, v0, :cond_14

    .line 228
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_THIRD_THIS_WEEK:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_14
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    if-lez v21, :cond_15

    .line 232
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_FOUR_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    :cond_15
    const/16 v21, 0x5

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    if-lez v21, :cond_16

    .line 236
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    :cond_16
    const/16 v21, 0x6

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    if-lez v21, :cond_17

    .line 240
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_SIX_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    :cond_17
    const/16 v21, 0x7

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    if-lez v21, :cond_18

    .line 244
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_SEVEN_PER_WEEK:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    :cond_18
    if-lez v13, :cond_19

    .line 248
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_COMPLETE_AFTER_FAILING:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    :cond_19
    sget-object v21, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->PREPARE:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x5

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1a

    .line 252
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_PREPARE:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    :cond_1a
    sget-object v21, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->REFLECT:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x5

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1b

    .line 256
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIVE_REFLECT:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    :cond_1b
    sget-object v21, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0xf

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1c

    .line 260
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIFTEEN_ACTION:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    :cond_1c
    sget-object v21, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/Integer;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Integer;->intValue()I

    move-result v21

    const/16 v22, 0x32

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_1d

    .line 264
    sget-object v21, Lcom/cigna/coach/utils/BadgeType;->MISSION_FIFTY_ACTION:Lcom/cigna/coach/utils/BadgeType;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/utils/BadgeType;->getBadgeId()I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    :cond_1d
    return-object v17
.end method


# virtual methods
.method public awardBadges()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {p0}, Lcom/cigna/coach/utils/BadgeAwarder;->getEligibleBadgeIds()Ljava/util/List;

    move-result-object v9

    invoke-direct {v5, v9}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 77
    .local v5, "eligible":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 78
    .local v4, "earned":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v9, p0, Lcom/cigna/coach/utils/BadgeAwarder;->badgeDbManager_:Lcom/cigna/coach/db/BadgeDBManager;

    iget-object v10, p0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/cigna/coach/db/BadgeDBManager;->getEarnedBadgesForUser(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/Badge;

    .line 79
    .local v1, "badge":Lcom/cigna/coach/apiobjects/Badge;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    .end local v1    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    :cond_0
    invoke-interface {v5, v4}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 84
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    .line 85
    .local v8, "now":Ljava/util/Calendar;
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2, v5}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 86
    .local v2, "badgeIdsToSave":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v9, p0, Lcom/cigna/coach/utils/BadgeAwarder;->badgeDbManager_:Lcom/cigna/coach/db/BadgeDBManager;

    iget-object v10, p0, Lcom/cigna/coach/utils/BadgeAwarder;->userId_:Ljava/lang/String;

    invoke-virtual {v9, v10, v2, v8}, Lcom/cigna/coach/db/BadgeDBManager;->saveBadgesToUser(Ljava/lang/String;Ljava/util/List;Ljava/util/Calendar;)Ljava/util/List;

    move-result-object v0

    .line 89
    .local v0, "awarded":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v10, p0, Lcom/cigna/coach/utils/BadgeAwarder;->badgeDbManager_:Lcom/cigna/coach/db/BadgeDBManager;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/Integer;

    invoke-interface {v0, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/Integer;

    invoke-virtual {v10, v9}, Lcom/cigna/coach/db/BadgeDBManager;->getBadgesById([Ljava/lang/Integer;)Ljava/util/List;

    move-result-object v6

    .line 90
    .local v6, "filled":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/Badge;

    .line 91
    .restart local v1    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    invoke-virtual {v1, v8}, Lcom/cigna/coach/apiobjects/Badge;->setBadgeEarnedDate(Ljava/util/Calendar;)V

    .line 93
    :try_start_0
    iget-object v9, p0, Lcom/cigna/coach/utils/BadgeAwarder;->contentHelper_:Lcom/cigna/coach/utils/ContentHelper;

    invoke-virtual {v9, v1}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 94
    :catch_0
    move-exception v3

    .line 95
    .local v3, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v9, Lcom/cigna/coach/utils/BadgeAwarder;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Unable to set content for badge %d during awarding."

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/Badge;->getBadgeId()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 99
    .end local v1    # "badge":Lcom/cigna/coach/apiobjects/Badge;
    .end local v3    # "e":Lcom/cigna/coach/exceptions/CoachException;
    :cond_1
    return-object v6
.end method
