.class public Lcom/cigna/coach/utils/ExpressionEvaluator;
.super Lorg/apache/commons/jexl2/JexlEngine;
.source "ExpressionEvaluator.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final instance:Lcom/cigna/coach/utils/ExpressionEvaluator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/cigna/coach/utils/ExpressionEvaluator;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/cigna/coach/utils/ExpressionEvaluator;

    invoke-direct {v0}, Lcom/cigna/coach/utils/ExpressionEvaluator;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/ExpressionEvaluator;->instance:Lcom/cigna/coach/utils/ExpressionEvaluator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/apache/commons/jexl2/JexlEngine;-><init>()V

    .line 32
    return-void
.end method

.method public static getInstance()Lcom/cigna/coach/utils/ExpressionEvaluator;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/cigna/coach/utils/ExpressionEvaluator;->instance:Lcom/cigna/coach/utils/ExpressionEvaluator;

    return-object v0
.end method


# virtual methods
.method public evaluate(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/Object;
    .locals 11
    .param p1, "expr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 39
    .local p2, "values":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 40
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Preparing to evaluate the expression:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    :cond_0
    const/4 v6, 0x0

    .line 44
    .local v6, "result":Ljava/lang/Object;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/cigna/coach/utils/ExpressionEvaluator;->createExpression(Ljava/lang/String;)Lorg/apache/commons/jexl2/Expression;

    move-result-object v1

    .line 45
    .local v1, "expression":Lorg/apache/commons/jexl2/Expression;
    new-instance v3, Lorg/apache/commons/jexl2/MapContext;

    invoke-direct {v3}, Lorg/apache/commons/jexl2/MapContext;-><init>()V

    .line 46
    .local v3, "jc":Lorg/apache/commons/jexl2/JexlContext;
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 47
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Setting user supplied values"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    :cond_1
    if-eqz p2, :cond_3

    .line 50
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    .line 51
    .local v5, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 52
    .local v4, "key":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    .line 53
    .local v7, "value":Ljava/lang/Object;
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 54
    sget-object v9, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, "->"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    :cond_2
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-interface {v3, v8, v7}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    .end local v1    # "expression":Lorg/apache/commons/jexl2/Expression;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "jc":Lorg/apache/commons/jexl2/JexlContext;
    .end local v4    # "key":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v6    # "result":Ljava/lang/Object;
    .end local v7    # "value":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Exception while evaluating expression:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    new-instance v8, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v8, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 60
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "expression":Lorg/apache/commons/jexl2/Expression;
    .restart local v3    # "jc":Lorg/apache/commons/jexl2/JexlContext;
    .restart local v6    # "result":Ljava/lang/Object;
    :cond_3
    :try_start_1
    const-string v8, "$math"

    const-class v9, Ljava/lang/Math;

    invoke-interface {v3, v8, v9}, Lorg/apache/commons/jexl2/JexlContext;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 62
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Calculating result"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_4
    invoke-interface {v1, v3}, Lorg/apache/commons/jexl2/Expression;->evaluate(Lorg/apache/commons/jexl2/JexlContext;)Ljava/lang/Object;

    move-result-object v6

    .line 65
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    if-eqz v6, :cond_5

    .line 66
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Result is:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    sget-object v8, Lcom/cigna/coach/utils/ExpressionEvaluator;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Evaluation complete"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 75
    :cond_5
    return-object v6
.end method
