.class public Lcom/cigna/coach/utils/WeightChangeListener;
.super Ljava/lang/Object;
.source "WeightChangeListener.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field public static final ENABLED:Z = true

.field private static final GRAMS_TO_POUNDS:F = 0.0022046f

.field static final GetUserAssessmentDateForCategory_SELECT:Ljava/lang/String; = " SELECT   A.LFSTYL_ASSMT_TIMESTMP  FROM LFSTYL_ASSMT_SCORE A WHERE USR_ID = ? AND    (SCORE_TY_IND = ? OR SCORE_TY_IND = ?) AND    SCORE_TY_ID = ? AND    LFSTYL_ASSMT_TIMESTMP <= ? ORDER BY A.LFSTYL_ASSMT_TIMESTMP DESC LIMIT 1"

.field private static final HIGH_BMI:F = 25.0f

.field private static final LOW_BMI:F = 18.5f

.field private static final POUNDS_TO_GRAMS:F = 453.59702f

.field private static final TEN_PERCENT_GOAL_CONTENT_ID:I = 0x179f

.field private static final TEN_POUND_GOAL_CONTENT_ID:I = 0x18ac

.field public static final WEIGHT_GOAL:I = 0x30

.field private static final WEIGHT_QUESTION_GROUP:I = 0xc

.field private static final WEIGHT_THRESHOLD_IN_GRAMS:F = 4535.97f

.field private static final WEIGHT_THRESHOLD_IN_POUNDS:I = 0xa


# instance fields
.field private final dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/cigna/coach/utils/WeightChangeListener;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 2
    .param p1, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SQLiteHelper object can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_0
    iput-object p1, p0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 67
    return-void
.end method

.method private bmi(Lcom/cigna/coach/dataobjects/WeightTracker;)F
    .locals 3
    .param p1, "tracker"    # Lcom/cigna/coach/dataobjects/WeightTracker;

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 460
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WeightTracker;->getWeight()F

    move-result v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WeightTracker;->getHeight()F

    move-result v1

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WeightTracker;->getHeight()F

    move-result v1

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method private score(Lcom/cigna/coach/dataobjects/WeightTracker;)I
    .locals 10
    .param p1, "tracker"    # Lcom/cigna/coach/dataobjects/WeightTracker;

    .prologue
    const/high16 v8, 0x435c0000    # 220.0f

    const/high16 v7, 0x43390000    # 185.0f

    const/high16 v6, 0x43170000    # 151.0f

    .line 464
    invoke-direct {p0, p1}, Lcom/cigna/coach/utils/WeightChangeListener;->bmi(Lcom/cigna/coach/dataobjects/WeightTracker;)F

    move-result v4

    const/high16 v5, 0x41200000    # 10.0f

    mul-float v0, v4, v5

    .line 467
    .local v0, "bmi10":F
    cmpg-float v4, v0, v6

    if-gtz v4, :cond_3

    const-wide/high16 v2, 0x403f000000000000L    # 31.0

    .line 474
    .local v2, "score":D
    :goto_0
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v2

    double-to-int v1, v4

    .line 475
    .local v1, "roundScore":I
    if-gez v1, :cond_0

    const/4 v1, 0x0

    .line 477
    :cond_0
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 478
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "score(): tracker: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; bmi*10: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; raw score: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; score: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    :cond_1
    const/16 v4, 0x64

    if-le v1, v4, :cond_2

    const/16 v1, 0x64

    .line 481
    :cond_2
    return v1

    .line 468
    .end local v1    # "roundScore":I
    .end local v2    # "score":D
    :cond_3
    cmpg-float v4, v0, v7

    if-gez v4, :cond_4

    const-wide/high16 v4, 0x403f000000000000L    # 31.0

    sub-float v6, v0, v6

    float-to-double v6, v6

    const-wide v8, 0x3ff2666666666666L    # 1.15

    mul-double/2addr v6, v8

    add-double v2, v4, v6

    .restart local v2    # "score":D
    goto :goto_0

    .line 469
    .end local v2    # "score":D
    :cond_4
    const/high16 v4, 0x43480000    # 200.0f

    cmpg-float v4, v0, v4

    if-gez v4, :cond_5

    const/high16 v4, 0x428e0000    # 71.0f

    sub-float v5, v0, v7

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v2, v4

    .restart local v2    # "score":D
    goto :goto_0

    .line 470
    .end local v2    # "score":D
    :cond_5
    cmpg-float v4, v0, v8

    if-gtz v4, :cond_6

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    .restart local v2    # "score":D
    goto :goto_0

    .line 471
    .end local v2    # "score":D
    :cond_6
    const/high16 v4, 0x437a0000    # 250.0f

    cmpg-float v4, v0, v4

    if-gtz v4, :cond_7

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    sub-float v6, v0, v8

    float-to-double v6, v6

    const-wide v8, 0x3feeb851eb851eb8L    # 0.96

    mul-double/2addr v6, v8

    sub-double v2, v4, v6

    .restart local v2    # "score":D
    goto/16 :goto_0

    .line 472
    .end local v2    # "score":D
    :cond_7
    const-wide v4, 0x4051800000000000L    # 70.0

    const/high16 v6, 0x437b0000    # 251.0f

    sub-float v6, v0, v6

    float-to-double v6, v6

    const-wide v8, 0x3fea4dd2f1a9fbe7L    # 0.822

    mul-double/2addr v6, v8

    sub-double v2, v4, v6

    .restart local v2    # "score":D
    goto/16 :goto_0
.end method

.method private stopGoal(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "existingGoalId"    # I
    .param p3, "endStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .prologue
    .line 139
    new-instance v2, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v7, p0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v2, v7}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 140
    .local v2, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    new-instance v5, Lcom/cigna/coach/db/MissionDBManager;

    iget-object v7, p0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v5, v7}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 141
    .local v5, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    iget-object v7, p0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v0

    .line 142
    .local v0, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalDetails(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v1

    .line 144
    .local v1, "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 145
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Existing goals need to be stopped"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Getting all non completed missions for this goal"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    invoke-virtual {v5, p1, p2}, Lcom/cigna/coach/db/MissionDBManager;->getNonCompleteUserMissionsForGoal(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v6

    .line 151
    .local v6, "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    if-eqz v6, :cond_4

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_4

    .line 152
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 153
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Need to cancel "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " missions"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_1
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 156
    .local v4, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 157
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cancelling missionid:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", seq num:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_2
    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v7

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v8

    sget-object v9, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v5, p1, v7, v8, v9}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMissionStatus(Ljava/lang/String;IILcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    goto :goto_0

    .line 162
    .end local v4    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_3
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 163
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "All active missions cancelled"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_4
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 168
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Stopping goal"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_5
    invoke-virtual {v2, p1, p2, p3}, Lcom/cigna/coach/db/GoalDBManager;->updateUserGoalStatus(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)I

    .line 172
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 173
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Sending broadcast"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_6
    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p3, v7, :cond_8

    .line 176
    invoke-virtual {v0, v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendGoalCompletedBroadcast(Lcom/cigna/coach/dataobjects/UserGoalData;)V

    .line 181
    :goto_1
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 182
    sget-object v7, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Goal stopped"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_7
    return-void

    .line 178
    :cond_8
    invoke-virtual {v0, v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendGoalCancelledBroadcast(Lcom/cigna/coach/dataobjects/UserGoalData;)V

    goto :goto_1
.end method

.method private stopStartWeightGoal(Ljava/lang/String;Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;Lcom/cigna/coach/dataobjects/WeightTracker;)V
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "endStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .param p3, "currentWeight"    # Lcom/cigna/coach/dataobjects/WeightTracker;

    .prologue
    const/4 v5, 0x1

    const/16 v9, 0x30

    .line 485
    sget-object v6, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 486
    sget-object v6, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "stopStartWeightGoal("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "): START"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v6, p0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v6}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 495
    .local v0, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    if-eqz p3, :cond_3

    invoke-direct {p0, p3}, Lcom/cigna/coach/utils/WeightChangeListener;->bmi(Lcom/cigna/coach/dataobjects/WeightTracker;)F

    move-result v6

    const/high16 v7, 0x41c80000    # 25.0f

    cmpl-float v6, v6, v7

    if-lez v6, :cond_3

    move v1, v5

    .line 496
    .local v1, "needsGoal":Z
    :goto_0
    sget-object v6, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 497
    sget-object v6, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "stopStartWeightGoal(): needsGoal: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_1
    if-eqz v1, :cond_4

    invoke-virtual {v0, p1, v9}, Lcom/cigna/coach/db/GoalDBManager;->getUserGoalStatus(Ljava/lang/String;I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne v6, v7, :cond_4

    .line 501
    sget-object v5, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 502
    sget-object v5, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "stopStartWeightGoal(): END: goal 48 is already available; no need to stop and restart. "

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :cond_2
    :goto_1
    return-void

    .line 495
    .end local v1    # "needsGoal":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 509
    .restart local v1    # "needsGoal":Z
    :cond_4
    invoke-direct {p0, p1, v9, p2}, Lcom/cigna/coach/utils/WeightChangeListener;->stopGoal(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 512
    if-eqz v1, :cond_6

    .line 513
    sget-object v6, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 514
    sget-object v6, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "stopStartWeightGoal(): creating new goal"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    :cond_5
    new-instance v3, Lcom/cigna/coach/dataobjects/UserGoalData;

    invoke-direct {v3}, Lcom/cigna/coach/dataobjects/UserGoalData;-><init>()V

    .line 517
    .local v3, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    invoke-virtual {v3, v9}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalId(I)V

    .line 518
    new-instance v2, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;-><init>()V

    .line 519
    .local v2, "qGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;
    const/16 v6, 0xc

    invoke-virtual {v2, v6}, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;->setQuestionGroupId(I)V

    .line 520
    invoke-virtual {v3, v2}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 521
    invoke-direct {p0, p3}, Lcom/cigna/coach/utils/WeightChangeListener;->score(Lcom/cigna/coach/dataobjects/WeightTracker;)I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionGroupScore(I)V

    .line 522
    sget-object v6, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    invoke-virtual {v3, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 523
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 524
    invoke-direct {p0, p3}, Lcom/cigna/coach/utils/WeightChangeListener;->score(Lcom/cigna/coach/dataobjects/WeightTracker;)I

    move-result v6

    invoke-virtual {v3, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryScore(I)V

    .line 526
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 527
    .local v4, "userGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserGoalData;>;"
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    invoke-virtual {v0, p1, v4}, Lcom/cigna/coach/db/GoalDBManager;->createUserGoals(Ljava/lang/String;Ljava/util/List;)V

    .line 531
    .end local v2    # "qGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;
    .end local v3    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v4    # "userGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserGoalData;>;"
    :cond_6
    sget-object v5, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 532
    sget-object v5, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "stopStartWeightGoal(): END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private updateWeightGoalAndScore(Ljava/lang/String;Lcom/cigna/coach/dataobjects/WeightTracker;ZZ)V
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "currentWeight"    # Lcom/cigna/coach/dataobjects/WeightTracker;
    .param p3, "goalEnrollment"    # Z
    .param p4, "reassessment"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 547
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 548
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v15, "updateWeightGoalAndScore(): START"

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "updateWeightGoalAndScore(): userId: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "updateWeightGoalAndScore(): currentWeight: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "updateWeightGoalAndScore(): goalEnrollment: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "updateWeightGoalAndScore(): reassessment: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p4

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    :cond_0
    new-instance v4, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v4, v14}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 571
    .local v4, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    new-instance v7, Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v7, v14}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 572
    .local v7, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    new-instance v10, Lcom/cigna/coach/utils/ScoringHelper;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v10, v14}, Lcom/cigna/coach/utils/ScoringHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 573
    .local v10, "scorer":Lcom/cigna/coach/utils/ScoringHelper;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v14}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v3

    .line 575
    .local v3, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-direct/range {p0 .. p1}, Lcom/cigna/coach/utils/WeightChangeListener;->weightGoalStartDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v5

    .line 576
    .local v5, "goalStartDate":Ljava/util/Calendar;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v5}, Lcom/cigna/coach/utils/WeightChangeListener;->weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;

    move-result-object v11

    .line 577
    .local v11, "startWeight":Lcom/cigna/coach/dataobjects/WeightTracker;
    move/from16 v9, p3

    .line 579
    .local v9, "recomputeScore":Z
    if-nez p2, :cond_5

    .line 580
    const/4 v9, 0x0

    .line 608
    :cond_1
    :goto_0
    const/16 v14, 0x30

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v14}, Lcom/cigna/coach/db/GoalDBManager;->getUserGoalStatus(Ljava/lang/String;I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v14

    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-eq v14, v15, :cond_2

    .line 609
    sget-object v14, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v14, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->stopStartWeightGoal(Ljava/lang/String;Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;Lcom/cigna/coach/dataobjects/WeightTracker;)V

    .line 612
    :cond_2
    if-eqz v9, :cond_4

    .line 613
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 614
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "Saving new score"

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_3
    sget-object v14, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    sget-object v15, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->WEIGHT:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/WeightChangeListener;->score(Lcom/cigna/coach/dataobjects/WeightTracker;)I

    move-result v16

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v7, v0, v14, v15, v1}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V

    .line 617
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/cigna/coach/utils/ScoringHelper;->computeOverallScore(Ljava/lang/String;)I

    move-result v8

    .line 618
    .local v8, "overAllScore":I
    if-ltz v8, :cond_4

    .line 620
    sget-object v14, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v14, v15, v8}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V

    .line 621
    invoke-virtual {v3, v8}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendScoreChangedBroadcast(I)V

    .line 624
    .end local v8    # "overAllScore":I
    :cond_4
    return-void

    .line 581
    :cond_5
    if-eqz v11, :cond_a

    .line 583
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/WeightTracker;->getHeight()F

    move-result v14

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/WeightTracker;->getHeight()F

    move-result v15

    sub-float v6, v14, v15

    .line 584
    .local v6, "heightChange":F
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/WeightTracker;->getWeight()F

    move-result v14

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/WeightTracker;->getWeight()F

    move-result v15

    sub-float v12, v14, v15

    .line 585
    .local v12, "weightChange":F
    const v14, 0x458dbfc3

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/WeightTracker;->getWeight()F

    move-result v15

    const v16, 0x3dcccccd    # 0.1f

    mul-float v15, v15, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->min(FF)F

    move-result v14

    float-to-int v13, v14

    .line 590
    .local v13, "weightThreshold":I
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 591
    sget-object v14, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v16, "weightThreshold :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", weightChange : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    :cond_6
    const/4 v14, 0x0

    cmpl-float v14, v6, v14

    if-nez v14, :cond_7

    const/4 v14, 0x1

    move/from16 v0, p4

    if-ne v0, v14, :cond_8

    const/4 v14, 0x0

    cmpl-float v14, v12, v14

    if-eqz v14, :cond_8

    .line 594
    :cond_7
    sget-object v14, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v14, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->stopStartWeightGoal(Ljava/lang/String;Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;Lcom/cigna/coach/dataobjects/WeightTracker;)V

    .line 595
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 596
    :cond_8
    neg-int v14, v13

    int-to-float v14, v14

    cmpg-float v14, v12, v14

    if-gtz v14, :cond_9

    .line 597
    sget-object v14, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v14, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->stopStartWeightGoal(Ljava/lang/String;Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;Lcom/cigna/coach/dataobjects/WeightTracker;)V

    .line 598
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 599
    :cond_9
    int-to-float v14, v13

    cmpl-float v14, v12, v14

    if-ltz v14, :cond_1

    .line 600
    sget-object v14, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v14, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->stopStartWeightGoal(Ljava/lang/String;Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;Lcom/cigna/coach/dataobjects/WeightTracker;)V

    .line 601
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 604
    .end local v6    # "heightChange":F
    .end local v12    # "weightChange":F
    .end local v13    # "weightThreshold":I
    :cond_a
    const/4 v9, 0x1

    goto/16 :goto_0
.end method

.method private weightGoal(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 373
    new-instance v0, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    const/16 v1, 0x30

    invoke-virtual {v0, p1, v1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalDetails(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v0

    return-object v0
.end method

.method private weightGoalStartDate(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 377
    invoke-direct {p0, p1}, Lcom/cigna/coach/utils/WeightChangeListener;->weightGoal(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v0

    .line 378
    .local v0, "weightGoal":Lcom/cigna/coach/dataobjects/UserGoalData;
    if-nez v0, :cond_0

    .line 379
    const/4 v1, 0x0

    .line 380
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v1

    goto :goto_0
.end method

.method private weightLossGoalInPounds(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)F
    .locals 7
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    .line 187
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 188
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "weightLossGoalInPounds: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_0
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-eq v4, v5, :cond_1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-eq v4, v5, :cond_1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne v4, v5, :cond_5

    .line 196
    :cond_1
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 198
    .local v0, "goalStartDate":Ljava/util/Calendar;
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 199
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "weightLossGoalInPounds: goal status is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; looking up current weight"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_2
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/utils/WeightChangeListener;->weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;

    move-result-object v1

    .line 210
    .local v1, "startWeight":Lcom/cigna/coach/dataobjects/WeightTracker;
    if-nez v1, :cond_6

    .line 211
    const/high16 v3, 0x41200000    # 10.0f

    .line 212
    .local v3, "weightLossGoal":F
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 213
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "weightLossGoalInPounds: No weight data as of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using default weight loss goal of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " lbs"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_3
    :goto_1
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 228
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "weightLossGoalInPounds: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_4
    return v3

    .line 202
    .end local v0    # "goalStartDate":Ljava/util/Calendar;
    .end local v1    # "startWeight":Lcom/cigna/coach/dataobjects/WeightTracker;
    .end local v3    # "weightLossGoal":F
    :cond_5
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v0

    .line 203
    .restart local v0    # "goalStartDate":Ljava/util/Calendar;
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 204
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "weightLossGoalInPounds: goal status is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; looking up weight as of goal start date"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    .restart local v1    # "startWeight":Lcom/cigna/coach/dataobjects/WeightTracker;
    :cond_6
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/WeightTracker;->getWeight()F

    move-result v4

    const v5, 0x3b107b0d    # 0.0022046f

    mul-float v2, v4, v5

    .line 219
    .local v2, "startWeightInPounds":F
    const/high16 v4, 0x41200000    # 10.0f

    const v5, 0x3dcccccd    # 0.1f

    mul-float/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 220
    .restart local v3    # "weightLossGoal":F
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 221
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "weightLossGoalInPounds: Weight as of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; using weight loss goal of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " lbs"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public getMessageForWeightGoal(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)Lcom/cigna/coach/dataobjects/CoachMessageData;
    .locals 21
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    .line 256
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 257
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_0
    new-instance v2, Lcom/cigna/coach/utils/CoachMessageHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v2, v3}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 261
    .local v2, "helper":Lcom/cigna/coach/utils/CoachMessageHelper;
    new-instance v13, Lcom/cigna/coach/db/CoachMessageDBManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v13, v3}, Lcom/cigna/coach/db/CoachMessageDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 262
    .local v13, "dbManager":Lcom/cigna/coach/db/CoachMessageDBManager;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 264
    .local v15, "messageCandidates":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/CoachMessageData;>;"
    if-nez p2, :cond_1

    .line 265
    const/4 v3, 0x0

    .line 368
    :goto_0
    return-object v3

    .line 267
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v3

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->dateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v3

    div-int/lit8 v9, v3, 0x7

    .line 269
    .local v9, "currentWeek":I
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getWeightLossMaxDurationInWeeks()I

    move-result v14

    .line 270
    .local v14, "finalWeek":I
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getWeightLossMinDurationInWeeks()I

    move-result v17

    .line 272
    .local v17, "midpointWeek":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/cigna/coach/utils/WeightChangeListener;->weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;

    move-result-object v12

    .line 273
    .local v12, "currentWeightTracker":Lcom/cigna/coach/dataobjects/WeightTracker;
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/cigna/coach/utils/WeightChangeListener;->weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;

    move-result-object v19

    .line 274
    .local v19, "startWeightTracker":Lcom/cigna/coach/dataobjects/WeightTracker;
    if-eqz v12, :cond_2

    if-nez v19, :cond_3

    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 276
    :cond_3
    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/WeightTracker;->getWeight()F

    move-result v3

    const v4, 0x3b107b0d    # 0.0022046f

    mul-float v11, v3, v4

    .line 277
    .local v11, "currentWeight":F
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/WeightTracker;->getWeight()F

    move-result v3

    const v4, 0x3b107b0d    # 0.0022046f

    mul-float v18, v3, v4

    .line 278
    .local v18, "startWeight":F
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getWeightLossGoalInPounds()I

    move-result v3

    int-to-float v3, v3

    sub-float v20, v18, v3

    .line 279
    .local v20, "targetWeight":F
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getWeightLossGoalInPounds()I

    move-result v3

    int-to-float v3, v3

    move/from16 v0, v17

    int-to-float v4, v0

    mul-float/2addr v3, v4

    int-to-float v4, v14

    div-float/2addr v3, v4

    sub-float v16, v18, v3

    .line 280
    .local v16, "midpointTargetWeight":F
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getWeightLossGoalInPounds()I

    move-result v3

    int-to-float v3, v3

    int-to-float v4, v9

    mul-float/2addr v3, v4

    int-to-float v4, v14

    div-float/2addr v3, v4

    sub-float v10, v18, v3

    .line 282
    .local v10, "currentWeekTargetWeight":F
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 283
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageForWeightGoal: current weight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " lbs\nstart date: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-static {v5}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " weight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " lbs\ncurrent week: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " target weight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " lbs\nmidpoint week: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " target weight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " lbs\nfinal week: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " target weight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " lbs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_4
    sget-object v3, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_ON_TRACK:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v6

    invoke-virtual {v13, v3, v4, v5, v6}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessagesForGoalOnTrackWithCategory(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/util/List;

    move-result-object v7

    .line 296
    .local v7, "tempCoachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_ON_TRACK:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v8

    check-cast v8, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 303
    .local v8, "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 304
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: On Track"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    if-nez v8, :cond_b

    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: Canditate message is null"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :cond_5
    :goto_1
    invoke-virtual {v2, v15, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 311
    move/from16 v0, v17

    if-lt v9, v0, :cond_7

    cmpl-float v3, v11, v10

    if-lez v3, :cond_7

    .line 312
    sget-object v3, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_LOWER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-virtual {v13, v3, v4, v5}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v7

    .line 317
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_LOWER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v8

    .end local v8    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    check-cast v8, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 324
    .restart local v8    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 325
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: Behind schedule at midpoint"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    if-nez v8, :cond_c

    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: Canditate message is null"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :cond_6
    :goto_2
    invoke-virtual {v2, v15, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 333
    :cond_7
    if-lt v9, v14, :cond_9

    cmpl-float v3, v11, v20

    if-lez v3, :cond_9

    .line 334
    sget-object v3, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_UPPER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-virtual {v13, v3, v4, v5}, Lcom/cigna/coach/db/CoachMessageDBManager;->getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;

    move-result-object v7

    .line 339
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    sget-object v5, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->GOAL_SPECIFIC_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_UPPER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->getCoachMessageFromList(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Ljava/util/List;)Lcom/cigna/coach/apiobjects/CoachMessage;

    move-result-object v8

    .end local v8    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    check-cast v8, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 346
    .restart local v8    # "coachMessage":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 347
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: Behind schedule at end"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    if-nez v8, :cond_d

    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: Canditate message is null"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :cond_8
    :goto_3
    invoke-virtual {v2, v15, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->addMessageToListIfNotNull(Ljava/util/List;Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 358
    :cond_9
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_e

    .line 359
    new-instance v3, Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;

    invoke-direct {v3}, Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;-><init>()V

    invoke-static {v15, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 360
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 361
    sget-object v4, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageForWeightGoal: END; returning message "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v3, 0x0

    invoke-interface {v15, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_a
    const/4 v3, 0x0

    invoke-interface {v15, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    goto/16 :goto_0

    .line 306
    :cond_b
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageForWeightGoal: Adding candidate message "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", display order "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 327
    :cond_c
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageForWeightGoal: Adding candidate message "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", display order "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 349
    :cond_d
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageForWeightGoal: Adding candidate message "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", display order "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 365
    :cond_e
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 366
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getMessageForWeightGoal: END; returning null"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method public isWeightGoalActive(Ljava/lang/String;)Z
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 384
    invoke-direct {p0, p1}, Lcom/cigna/coach/utils/WeightChangeListener;->weightGoal(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lastCategoryScoreDateBefore(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 16
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p3, "dateTime"    # Ljava/util/Calendar;

    .prologue
    .line 79
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 80
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "lastCategoryAssessmentDateBefore: START"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Preparing to run query to get last assessment date for category:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_0
    const/4 v7, 0x0

    .line 85
    .local v7, "returnValue":Ljava/util/Calendar;
    if-nez p3, :cond_2

    .line 86
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 87
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "No datetime was provided, defaulting to current time"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_1
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object p3

    .line 92
    :cond_2
    invoke-virtual/range {p3 .. p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    .line 93
    .local v5, "refTimeInMilliSecs":J
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 94
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Reference time being used is: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static/range {p3 .. p3}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Reference time in secs: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_3
    sget-object v10, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v8

    .line 99
    .local v8, "scoretypeKey":I
    sget-object v10, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v10}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v4

    .line 100
    .local v4, "goalscoretypeKey":I
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v2

    .line 102
    .local v2, "categoryKey":I
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 103
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "User Id is: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Score Type is: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Category Type is: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_4
    const/4 v1, 0x0

    .line 110
    .local v1, "c2":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v9

    .line 111
    .local v9, "userKey":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    const-string v11, " SELECT   A.LFSTYL_ASSMT_TIMESTMP  FROM LFSTYL_ASSMT_SCORE A WHERE USR_ID = ? AND    (SCORE_TY_IND = ? OR SCORE_TY_IND = ?) AND    SCORE_TY_ID = ? AND    LFSTYL_ASSMT_TIMESTMP <= ? ORDER BY A.LFSTYL_ASSMT_TIMESTMP DESC LIMIT 1"

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 114
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 115
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v10

    if-nez v10, :cond_8

    .line 116
    const/4 v10, 0x0

    invoke-interface {v1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    int-to-long v10, v10

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v7

    .line 117
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 118
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Latest assessment date in DB is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v7}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_5
    :goto_0
    if-eqz v1, :cond_6

    .line 130
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_6
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 132
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "lastCategoryAssessmentDateBefore: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_7
    return-object v7

    .line 121
    :cond_8
    :try_start_1
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 122
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "No entries for this category before the reference timestamp"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 125
    .end local v9    # "userKey":I
    :catch_0
    move-exception v3

    .line 126
    .local v3, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v10, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Error in lastCategoryAssessmentDateBefore: "

    invoke-static {v10, v11, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 127
    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 129
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v10

    if-eqz v1, :cond_9

    .line 130
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 131
    :cond_9
    sget-object v11, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 132
    sget-object v11, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "lastCategoryAssessmentDateBefore: END"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    throw v10
.end method

.method public populateWeightGoalNameFields(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    .line 234
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v1

    const/16 v2, 0x30

    if-eq v1, v2, :cond_0

    .line 253
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/cigna/coach/utils/WeightChangeListener;->weightLossGoalInPounds(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)F

    move-result v0

    .line 239
    .local v0, "weightLossGoal":F
    sget-object v1, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 240
    sget-object v1, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "populateWeightGoalNameFields(): Weight loss goal "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; setting weight loss goal to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " lbs"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :cond_1
    const/high16 v1, 0x41200000    # 10.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_2

    .line 245
    const/16 v1, 0x18ac

    invoke-virtual {p2, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setContentId(I)V

    .line 249
    :goto_1
    float-to-int v1, v0

    invoke-virtual {p2, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setWeightLossGoalInPounds(I)V

    .line 250
    const v1, 0x43e2cc6b

    mul-float/2addr v1, v0

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {p2, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setWeightLossGoalInKilograms(I)V

    .line 251
    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    invoke-virtual {p2, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setWeightLossMinDurationInWeeks(I)V

    .line 252
    float-to-int v1, v0

    invoke-virtual {p2, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setWeightLossMaxDurationInWeeks(I)V

    goto :goto_0

    .line 247
    :cond_2
    const/16 v1, 0x179f

    invoke-virtual {p2, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setContentId(I)V

    goto :goto_1
.end method

.method public declared-synchronized updateWeightGoalAndScore(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalEnrollment"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 627
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "updateWeightGoalAndScore1(): START"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_0
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/utils/WeightChangeListener;->weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/cigna/coach/utils/WeightChangeListener;->updateWeightGoalAndScore(Ljava/lang/String;Lcom/cigna/coach/dataobjects/WeightTracker;ZZ)V

    .line 631
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "updateWeightGoalAndScore1(): END"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 634
    :cond_1
    monitor-exit p0

    return-void

    .line 627
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public updateWeightGoalAndScore(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .local p2, "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    const/4 v2, 0x1

    .line 637
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "updateWeightGoalAndScore2(): START"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    :cond_0
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/utils/WeightChangeListener;->weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->updateWeightGoalAndScore(Ljava/lang/String;Lcom/cigna/coach/dataobjects/WeightTracker;ZZ)V

    .line 644
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 645
    sget-object v0, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v1, "updateWeightGoalAndScore2(): END"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    :cond_1
    return v2
.end method

.method public weightTrackerAsOf(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/WeightTracker;
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "date"    # Ljava/util/Calendar;

    .prologue
    const/4 v0, 0x0

    .line 388
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 389
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "weightTrackerAsOf("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p2}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "): start"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_0
    if-nez p2, :cond_2

    .line 413
    :cond_1
    :goto_0
    return-object v0

    .line 397
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/cigna/coach/utils/WeightChangeListener;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-static {p2}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v6

    invoke-static {v3, v4, v5, v6, v7}, Lcom/cigna/mobile/coach/model/User;->getWeightData(Landroid/content/Context;JJ)Ljava/util/List;
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 402
    .local v2, "weights":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/WeightTracker;>;"
    if-eqz v2, :cond_3

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 403
    :cond_3
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 404
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "weightTrackerAsOf: No weights available"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 398
    .end local v2    # "weights":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/WeightTracker;>;"
    :catch_0
    move-exception v1

    .line 399
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in weightTrackerAsOf: "

    invoke-static {v3, v4, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 409
    .end local v1    # "e":Lcom/cigna/coach/exceptions/CoachException;
    .restart local v2    # "weights":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/WeightTracker;>;"
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/WeightTracker;

    .line 410
    .local v0, "ans":Lcom/cigna/coach/dataobjects/WeightTracker;
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 411
    sget-object v3, Lcom/cigna/coach/utils/WeightChangeListener;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "weightTrackerAsOf found weight: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
