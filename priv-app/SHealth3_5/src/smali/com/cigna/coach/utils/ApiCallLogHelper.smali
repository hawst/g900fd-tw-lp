.class public Lcom/cigna/coach/utils/ApiCallLogHelper;
.super Ljava/lang/Object;
.source "ApiCallLogHelper.java"


# instance fields
.field private dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/utils/ApiCallLogHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 33
    iput-object p1, p0, Lcom/cigna/coach/utils/ApiCallLogHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 34
    return-void
.end method


# virtual methods
.method public insertApiCallLogEntry(Lcom/cigna/coach/dataobjects/ApiCallLogEntry;)V
    .locals 2
    .param p1, "entry"    # Lcom/cigna/coach/dataobjects/ApiCallLogEntry;

    .prologue
    .line 37
    new-instance v0, Lcom/cigna/coach/db/ApiCallDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/ApiCallLogHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/ApiCallDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 38
    .local v0, "dbm":Lcom/cigna/coach/db/ApiCallDBManager;
    invoke-virtual {v0, p1}, Lcom/cigna/coach/db/ApiCallDBManager;->insertApiCallLogEntry(Lcom/cigna/coach/dataobjects/ApiCallLogEntry;)V

    .line 39
    return-void
.end method
