.class Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;
.super Ljava/lang/Object;
.source "RuleBasedAggregator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/RuleBasedAggregator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Bucket"
.end annotation


# instance fields
.field private aggregateValue:I

.field private bucket:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;


# direct methods
.method private constructor <init>(Lcom/cigna/coach/utils/RuleBasedAggregator;)V
    .locals 1

    .prologue
    .line 67
    iput-object p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    .line 69
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    return-void
.end method

.method synthetic constructor <init>(Lcom/cigna/coach/utils/RuleBasedAggregator;Lcom/cigna/coach/utils/RuleBasedAggregator$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator;
    .param p2, "x1"    # Lcom/cigna/coach/utils/RuleBasedAggregator$1;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;-><init>(Lcom/cigna/coach/utils/RuleBasedAggregator;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;
    .param p1, "x1"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->push(Lcom/cigna/coach/dataobjects/UserMissionData;)V

    return-void
.end method

.method private check()V
    .locals 5

    .prologue
    .line 223
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 224
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Initiating check"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    iget v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregationLimit:I
    invoke-static {v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$500(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v2

    if-lt v1, v2, :cond_b

    .line 227
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 228
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Aggregate limit reached"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->minDays:I
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$800(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 231
    .local v0, "numDaysDifferenceFromLastAggregationComplete":I
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$600(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 232
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$600(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/CalendarUtil;->absDateDifference(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v0

    .line 234
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 235
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Last aggregate completion:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v3}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$300(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/text/SimpleDateFormat;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;
    invoke-static {v4}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$600(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Num days from last aggregate completion:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_2
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->minDays:I
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$800(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v1

    if-gt v0, v1, :cond_5

    .line 241
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 242
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Aggregation limit reached, but too close to previous completion"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :cond_3
    :goto_0
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 266
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Num cycles completed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I
    invoke-static {v3}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$700(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "---------------------------"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    .end local v0    # "numDaysDifferenceFromLastAggregationComplete":I
    :cond_4
    :goto_1
    return-void

    .line 244
    .restart local v0    # "numDaysDifferenceFromLastAggregationComplete":I
    :cond_5
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->minDays:I
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$800(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v1

    if-le v0, v1, :cond_7

    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->maxDays:I
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$900(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v1

    if-gt v0, v1, :cond_7

    .line 246
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 247
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "This aggreagtion set can be considered complete"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_6
    invoke-direct {p0}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->cycle()V

    goto :goto_0

    .line 250
    :cond_7
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->maxDays:I
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$900(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v1

    if-le v0, v1, :cond_9

    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->graceDays:I
    invoke-static {v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$1000(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v1

    if-gt v0, v1, :cond_9

    .line 252
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 253
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "This aggregation is complete, but cannot be counted because it completed out of range."

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Date will be reset because its within the grace period"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_8
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v2

    # setter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;
    invoke-static {v1, v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$602(Lcom/cigna/coach/utils/RuleBasedAggregator;Ljava/util/Calendar;)Ljava/util/Calendar;

    goto/16 :goto_0

    .line 258
    :cond_9
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 259
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "This aggregation is complete, but exceeded the grace period"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Only credit will be given for this instance"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_a
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v2

    # setter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;
    invoke-static {v1, v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$602(Lcom/cigna/coach/utils/RuleBasedAggregator;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 263
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    const/4 v2, 0x1

    # setter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I
    invoke-static {v1, v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$702(Lcom/cigna/coach/utils/RuleBasedAggregator;I)I

    goto/16 :goto_0

    .line 271
    .end local v0    # "numDaysDifferenceFromLastAggregationComplete":I
    :cond_b
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 272
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Aggregate limit not reached"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private cycle()V
    .locals 8

    .prologue
    .line 176
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 177
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Removing entries contributed to the aggregation"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_0
    iget v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    .line 181
    .local v4, "startAggregateValue":I
    const/4 v1, 0x1

    .line 185
    .local v1, "firstCycle":Z
    const/4 v2, 0x0

    .line 186
    .local v2, "removedMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v5, ""

    invoke-direct {v0, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 188
    .local v0, "consideredGroup":Ljava/lang/StringBuffer;
    :cond_1
    if-eqz v1, :cond_6

    .line 189
    invoke-direct {p0}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->popFromLast()Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v2

    .line 194
    :goto_0
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 195
    iget-object v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->missionSequenceMap:Ljava/util/Map;
    invoke-static {v5}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$400(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 196
    .local v3, "sequence":I
    if-nez v1, :cond_2

    .line 197
    const/16 v5, 0x2c

    invoke-virtual {v0, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 199
    :cond_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 202
    .end local v3    # "sequence":I
    :cond_3
    const/4 v1, 0x0

    .line 203
    iget v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    sub-int v5, v4, v5

    iget-object v6, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregationLimit:I
    invoke-static {v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$500(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v6

    if-lt v5, v6, :cond_1

    .line 205
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 206
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Entries removed are ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_4
    iget-object v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    iget-object v6, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v6

    # setter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayLastAggregateComplete:Ljava/util/Calendar;
    invoke-static {v5, v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$602(Lcom/cigna/coach/utils/RuleBasedAggregator;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 210
    iget-object v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # operator++ for: Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregateCyclesCompleted:I
    invoke-static {v5}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$708(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    .line 212
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 213
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Signal complete"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_5
    return-void

    .line 191
    :cond_6
    invoke-direct {p0}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->popFromBegin()Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v2

    goto/16 :goto_0
.end method

.method private popFromBegin()Lcom/cigna/coach/dataobjects/UserMissionData;
    .locals 5

    .prologue
    .line 151
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Popping one object from beginning of queue"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 156
    .local v0, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-eqz v0, :cond_2

    .line 157
    iget v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    .line 158
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    .line 159
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 160
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removing mission with completion date:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v3}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$300(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reducing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " days from aggregate value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New aggregate value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_1
    :goto_0
    return-object v0

    .line 165
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    goto :goto_0
.end method

.method private popFromLast()Lcom/cigna/coach/dataobjects/UserMissionData;
    .locals 5

    .prologue
    .line 126
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Popping one object from end of queue"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 131
    .local v0, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-eqz v0, :cond_2

    .line 132
    iget v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    .line 133
    iget-object v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v1}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    .line 134
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 135
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removing mission with completion date:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v3}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$300(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reducing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " days from aggregate value"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New aggregate value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_1
    :goto_0
    return-object v0

    .line 140
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    goto :goto_0
.end method

.method private push(Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 4
    .param p1, "mission"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    .line 105
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Adding an object to deque"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    .line 109
    iget v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    .line 110
    iget-object v0, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v1

    # setter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v0, v1}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$202(Lcom/cigna/coach/utils/RuleBasedAggregator;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 112
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Day of last push:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$300(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/text/SimpleDateFormat;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v3}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " days to aggregate value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New aggregate value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_1
    invoke-direct {p0}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->sweep()V

    .line 118
    invoke-direct {p0}, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->check()V

    .line 119
    return-void
.end method

.method private sweep()V
    .locals 8

    .prologue
    .line 77
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 78
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Running sweep for a window of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->sweepWindow:I
    invoke-static {v4}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$100(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " starting from:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v4}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$300(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/text/SimpleDateFormat;

    move-result-object v4

    iget-object v5, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_0
    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dayOfLastPush:Ljava/util/Calendar;
    invoke-static {v2}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$200(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    iget-object v6, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->sweepWindow:I
    invoke-static {v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$100(Lcom/cigna/coach/utils/RuleBasedAggregator;)I

    move-result v6

    int-to-long v6, v6

    mul-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v0

    .line 85
    .local v0, "expirationDate":Ljava/util/Calendar;
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->peekFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 86
    iget-object v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->bucket:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 87
    .local v1, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    iget v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    .line 88
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 89
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Removing mission with completion date:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->dateFormat:Ljava/text/SimpleDateFormat;
    invoke-static {v4}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$300(Lcom/cigna/coach/utils/RuleBasedAggregator;)Ljava/text/SimpleDateFormat;

    move-result-object v4

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Reducing "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " days from aggregate value"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "New aggregate value:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$Bucket;->aggregateValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 94
    .end local v1    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_2
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 95
    # getter for: Lcom/cigna/coach/utils/RuleBasedAggregator;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/RuleBasedAggregator;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Sweep complete"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :cond_3
    return-void
.end method
