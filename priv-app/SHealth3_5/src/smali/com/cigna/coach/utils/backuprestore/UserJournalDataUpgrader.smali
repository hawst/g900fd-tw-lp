.class public final Lcom/cigna/coach/utils/backuprestore/UserJournalDataUpgrader;
.super Ljava/lang/Object;
.source "UserJournalDataUpgrader.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static upgradeToCurrentVersion(Ljava/util/Map;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;
    .locals 7
    .param p1, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;>;>;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;>;"
    const/4 v1, 0x4

    .line 59
    .local v1, "currentDBModelVersion":I
    const/4 v0, 0x1

    .line 61
    .local v0, "currentCodeBaseId":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 62
    .local v3, "upgradedVersionList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    if-nez v3, :cond_0

    .line 63
    new-instance v3, Ljava/util/HashMap;

    .end local v3    # "upgradedVersionList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 66
    .restart local v3    # "upgradedVersionList":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;>;"
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 67
    .local v2, "upgradedTables":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    if-nez v2, :cond_1

    .line 68
    new-instance v2, Ljava/util/HashMap;

    .end local v2    # "upgradedTables":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 70
    .restart local v2    # "upgradedTables":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    :cond_1
    sget-object v4, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    invoke-interface {p1, v4, v5, v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 72
    return-object v2
.end method
