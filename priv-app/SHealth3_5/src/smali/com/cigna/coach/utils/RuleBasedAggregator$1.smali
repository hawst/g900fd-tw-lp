.class Lcom/cigna/coach/utils/RuleBasedAggregator$1;
.super Ljava/lang/Object;
.source "RuleBasedAggregator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregate(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;


# direct methods
.method constructor <init>(Lcom/cigna/coach/utils/RuleBasedAggregator;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/cigna/coach/utils/RuleBasedAggregator$1;->this$0:Lcom/cigna/coach/utils/RuleBasedAggregator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;)I
    .locals 2
    .param p1, "mission1"    # Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .param p2, "mission2"    # Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .prologue
    .line 301
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v0

    if-nez v0, :cond_0

    .line 302
    const/4 v0, -0x1

    .line 306
    :goto_0
    return v0

    .line 303
    :cond_0
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v0

    if-nez v0, :cond_1

    .line 304
    const/4 v0, 0x1

    goto :goto_0

    .line 306
    :cond_1
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 297
    check-cast p1, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/utils/RuleBasedAggregator$1;->compare(Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;)I

    move-result v0

    return v0
.end method
