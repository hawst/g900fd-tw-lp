.class public Lcom/cigna/coach/utils/IndempotentExecutor;
.super Ljava/lang/Object;
.source "IndempotentExecutor.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/IndempotentExecutor$NullKeyedRunnable;,
        Lcom/cigna/coach/utils/IndempotentExecutor$KeyedRunnable;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private cycleTime:J

.field private isTimerScheduled:Z

.field private taskSet:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private timer:Ljava/util/concurrent/ScheduledExecutorService;

.field timerRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/cigna/coach/utils/IndempotentExecutor;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 107
    const-wide/16 v0, 0x1388

    invoke-direct {p0, v0, v1}, Lcom/cigna/coach/utils/IndempotentExecutor;-><init>(J)V

    .line 108
    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .param p1, "cycleTime"    # J

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;

    .line 94
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->timer:Ljava/util/concurrent/ScheduledExecutorService;

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->isTimerScheduled:Z

    .line 138
    new-instance v0, Lcom/cigna/coach/utils/IndempotentExecutor$1;

    invoke-direct {v0, p0}, Lcom/cigna/coach/utils/IndempotentExecutor$1;-><init>(Lcom/cigna/coach/utils/IndempotentExecutor;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->timerRunnable:Ljava/lang/Runnable;

    .line 103
    iput-wide p1, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->cycleTime:J

    .line 104
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/cigna/coach/utils/IndempotentExecutor;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/utils/IndempotentExecutor;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$202(Lcom/cigna/coach/utils/IndempotentExecutor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/utils/IndempotentExecutor;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->isTimerScheduled:Z

    return p1
.end method

.method private scheduleTimerIfNeeded(J)V
    .locals 5
    .param p1, "delay"    # J

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->isTimerScheduled:Z

    if-eqz v0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->timer:Ljava/util/concurrent/ScheduledExecutorService;

    iget-object v1, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->timerRunnable:Ljava/lang/Runnable;

    iget-wide v2, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->cycleTime:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->isTimerScheduled:Z

    .line 133
    sget-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    sget-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Timer started="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->isTimerScheduled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "timer scheduled for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public execute(Ljava/lang/Runnable;)V
    .locals 4
    .param p1, "task"    # Ljava/lang/Runnable;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;

    monitor-enter v1

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 114
    sget-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Postponing task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;

    invoke-interface {v0, p1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    :goto_0
    iget-wide v2, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->cycleTime:J

    invoke-direct {p0, v2, v3}, Lcom/cigna/coach/utils/IndempotentExecutor;->scheduleTimerIfNeeded(J)V

    .line 126
    monitor-exit v1

    .line 127
    return-void

    .line 119
    :cond_1
    sget-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    sget-object v0, Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Running task immediately: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_2
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 123
    iget-object v0, p0, Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;

    new-instance v2, Lcom/cigna/coach/utils/IndempotentExecutor$NullKeyedRunnable;

    invoke-direct {v2, p1}, Lcom/cigna/coach/utils/IndempotentExecutor$NullKeyedRunnable;-><init>(Ljava/lang/Runnable;)V

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
