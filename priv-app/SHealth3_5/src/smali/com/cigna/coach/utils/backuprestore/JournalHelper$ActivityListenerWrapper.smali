.class public Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;
.super Ljava/lang/Object;
.source "JournalHelper.java"

# interfaces
.implements Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ActivityListenerWrapper"
.end annotation


# instance fields
.field private parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;)V
    .locals 0
    .param p1, "parent"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;->parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    .line 143
    return-void
.end method


# virtual methods
.method public onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
    .locals 3
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "isSuccess"    # Z
    .param p3, "taskError"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 165
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RequestType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , & onFinished, & isSuccess: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " & taskError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " & errorMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;->parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;->parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 171
    :cond_1
    return-void
.end method

.method public onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    .locals 6
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;
    .param p2, "fraction"    # D

    .prologue
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    .line 147
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RequestType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , & onProgress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-double v2, p2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;->parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;->parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    mul-double v1, p2, v4

    double-to-int v1, v1

    invoke-interface {v0, p1, v1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;I)V

    .line 152
    :cond_1
    return-void
.end method

.method public onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
    .locals 3
    .param p1, "requestType"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    .prologue
    .line 156
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RequestType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , & onStarted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;->parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$ActivityListenerWrapper;->parent:Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;

    invoke-interface {v0, p1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$IActivityListener;->onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V

    .line 161
    :cond_1
    return-void
.end method
