.class public final Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;
.super Ljava/lang/Object;
.source "UserJournalDataAggregator.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field public static final sortByDate:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;"
        }
    .end annotation
.end field

.field public static final sortByKey:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    .line 60
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator$1;

    invoke-direct {v0}, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator$1;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->sortByDate:Ljava/util/Comparator;

    .line 79
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator$2;

    invoke-direct {v0}, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator$2;-><init>()V

    sput-object v0, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->sortByKey:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static aggregate(Ljava/util/Map;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/Map;
    .locals 13
    .param p1, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .local p0, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    .line 97
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 98
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Aggregating user journal data records"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 102
    .local v2, "returnData":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    if-eqz p0, :cond_2

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 103
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 104
    .local v4, "tables":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 105
    .local v5, "tablesAggregated":I
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 106
    .local v3, "table":Ljava/lang/String;
    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    int-to-double v7, v5

    mul-double/2addr v7, v11

    invoke-interface {p0}, Ljava/util/Map;->size()I

    move-result v9

    int-to-double v9, v9

    div-double/2addr v7, v9

    invoke-interface {p1, v6, v7, v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 108
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 109
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Reading  data for table:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_1
    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 112
    .local v1, "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    invoke-static {v1}, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->aggregateRows(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v2, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 115
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    .end local v3    # "table":Ljava/lang/String;
    .end local v4    # "tables":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "tablesAggregated":I
    :cond_2
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 116
    sget-object v6, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "No records to aggregate"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_3
    sget-object v6, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-interface {p1, v6, v11, v12}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 121
    return-object v2
.end method

.method private static aggregateRows(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    .local p0, "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    const/4 v4, 0x0

    .line 135
    .local v4, "returnData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    if-eqz p0, :cond_4

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 136
    sget-object v5, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 137
    sget-object v5, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Processing "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " records"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "returnData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .restart local v4    # "returnData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    sget-object v5, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->sortByKey:Ljava/util/Comparator;

    invoke-static {p0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 144
    const/4 v5, 0x0

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .line 145
    .local v3, "recordToAdd":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    if-eqz v3, :cond_3

    .line 146
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "prevKey":Ljava/lang/String;
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .line 151
    .local v2, "record":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 152
    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 153
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getActionTypeCode()Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->DELETE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual {v5, v6}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 154
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    :cond_1
    move-object v3, v2

    .line 159
    goto :goto_0

    .line 160
    .end local v2    # "record":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :cond_2
    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getActionTypeCode()Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->DELETE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual {v5, v6}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 161
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "prevKey":Ljava/lang/String;
    .end local v3    # "recordToAdd":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :cond_3
    :goto_1
    return-object v4

    .line 166
    :cond_4
    sget-object v5, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 167
    sget-object v5, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "No records to process"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static compareIfNotNull(Ljava/lang/Comparable;Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Comparable",
            "<TT;>;TT;)I"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "lhs":Ljava/lang/Comparable;, "Ljava/lang/Comparable<TT;>;"
    .local p1, "rhs":Ljava/lang/Object;, "TT;"
    if-nez p0, :cond_1

    .line 52
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 56
    :goto_0
    return v0

    .line 53
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 55
    :cond_1
    if-nez p1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 56
    :cond_2
    invoke-interface {p0, p1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method
