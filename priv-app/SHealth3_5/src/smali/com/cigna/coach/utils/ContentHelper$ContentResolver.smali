.class Lcom/cigna/coach/utils/ContentHelper$ContentResolver;
.super Ljava/lang/Object;
.source "ContentHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/ContentHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContentResolver"
.end annotation


# instance fields
.field contentId:I

.field contextData:Lcom/cigna/coach/utils/ContextData;

.field resolvedContent:Ljava/lang/String;


# direct methods
.method constructor <init>(ILcom/cigna/coach/utils/ContextData;)V
    .locals 0
    .param p1, "cId"    # I
    .param p2, "contextData"    # Lcom/cigna/coach/utils/ContextData;

    .prologue
    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    iput-object p2, p0, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;->contextData:Lcom/cigna/coach/utils/ContextData;

    .line 405
    iput p1, p0, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;->contentId:I

    .line 406
    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setResolvedContent"
    .end annotation

    .prologue
    .line 410
    iget v0, p0, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;->contentId:I

    return v0
.end method

.method public getContextData()Lcom/cigna/coach/utils/ContextData;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;->contextData:Lcom/cigna/coach/utils/ContextData;

    return-object v0
.end method

.method public getResolvedContent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;->resolvedContent:Ljava/lang/String;

    return-object v0
.end method

.method public setResolvedContent(Ljava/lang/String;)V
    .locals 0
    .param p1, "resolvedContent"    # Ljava/lang/String;

    .prologue
    .line 418
    iput-object p1, p0, Lcom/cigna/coach/utils/ContentHelper$ContentResolver;->resolvedContent:Ljava/lang/String;

    .line 420
    return-void
.end method
