.class public Lcom/cigna/coach/utils/GoalMissionHelper;
.super Ljava/lang/Object;
.source "GoalMissionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/GoalMissionHelper$2;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private final dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/cigna/coach/utils/GoalMissionHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 2
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "SQLiteHelper object can not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    iput-object p1, p0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/cigna/coach/utils/GoalMissionHelper;Lcom/cigna/coach/apiobjects/MissionDetails;)J
    .locals 2
    .param p0, "x0"    # Lcom/cigna/coach/utils/GoalMissionHelper;
    .param p1, "x1"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/cigna/coach/utils/GoalMissionHelper;->activityDate(Lcom/cigna/coach/apiobjects/MissionDetails;)J

    move-result-wide v0

    return-wide v0
.end method

.method private activityDate(Lcom/cigna/coach/apiobjects/MissionDetails;)J
    .locals 4
    .param p1, "activity"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 106
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v0

    .line 107
    .local v0, "timestamp":J
    invoke-static {v0, v1}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v2

    return-wide v2
.end method

.method private activityToString(Lcom/cigna/coach/apiobjects/MissionDetails;)Ljava/lang/String;
    .locals 3
    .param p1, "activity"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .local v0, "ans":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81
    const-string v2, " activities"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    instance-of v2, p1, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 84
    check-cast v1, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    .line 85
    .local v1, "data":Lcom/cigna/coach/dataobjects/MissionDetailsData;
    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getActivitySource()Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 87
    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 90
    .end local v1    # "data":Lcom/cigna/coach/dataobjects/MissionDetailsData;
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private isTrackerActivity(Lcom/cigna/coach/apiobjects/MissionDetails;)Z
    .locals 4
    .param p1, "activity"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    const/4 v1, 0x0

    .line 111
    instance-of v2, p1, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    if-nez v2, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 112
    check-cast v0, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    .line 113
    .local v0, "data":Lcom/cigna/coach/dataobjects/MissionDetailsData;
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getActivitySource()Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->TRACKER:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private logActivities(Ljava/lang/String;Ljava/util/List;)V
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p2, "activities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    if-nez p2, :cond_1

    .line 95
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_0
    return-void

    .line 99
    :cond_1
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " contains "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " activities"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 101
    .local v0, "activity":Lcom/cigna/coach/apiobjects/MissionDetails;
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "    "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/cigna/coach/utils/GoalMissionHelper;->activityToString(Lcom/cigna/coach/apiobjects/MissionDetails;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processGoalCompletion(Ljava/lang/String;ILcom/cigna/coach/dataobjects/GoalMissionStatusData;)Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "returnStatus"    # Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 381
    sget-object v6, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 382
    sget-object v6, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "================================================================================"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    sget-object v6, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Processing goal completion"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    sget-object v6, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Fetching user goal details from database"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_0
    new-instance v2, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v6, p0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v2, v6}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 388
    .local v2, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    invoke-virtual {v2, p1, p2}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalDetails(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v1

    .line 390
    .local v1, "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    if-nez v1, :cond_1

    .line 429
    :goto_0
    return-object p3

    .line 394
    :cond_1
    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->NOT_COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    .line 396
    .local v3, "goalProgressStatus":Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;
    new-instance v0, Lcom/cigna/coach/utils/RuleBasedAggregator;

    invoke-direct {v0}, Lcom/cigna/coach/utils/RuleBasedAggregator;-><init>()V

    .line 397
    .local v0, "aggregator":Lcom/cigna/coach/utils/RuleBasedAggregator;
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getMinMissionFrequencyNumber()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->setAggregationLimit(I)V

    .line 398
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getNumMinDaysBetweenMissions()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->setMinDays(I)V

    .line 399
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getNumMaxDaysBetweenMissions()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->setMaxDays(I)V

    .line 400
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getMaxGracePeriodDays()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->setGraceDays(I)V

    .line 401
    const/4 v6, 0x7

    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->setSweepWindow(I)V

    .line 403
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/RuleBasedAggregator;->aggregate(Ljava/util/List;)V

    .line 405
    invoke-virtual {v0}, Lcom/cigna/coach/utils/RuleBasedAggregator;->getAggregateCyclesCompleted()I

    move-result v5

    .line 406
    .local v5, "numMissionsCompleted":I
    invoke-virtual {v0}, Lcom/cigna/coach/utils/RuleBasedAggregator;->getDayLastAggregateComplete()Ljava/util/Calendar;

    move-result-object v4

    .line 408
    .local v4, "lastMissionCompletionCountedDate":Ljava/util/Calendar;
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getNumMissionsToComplete()I

    move-result v6

    if-lt v5, v6, :cond_3

    .line 409
    sget-object v6, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 410
    sget-object v6, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Setting goal status to completed"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_2
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v1, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 413
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCompletedDate(Ljava/util/Calendar;)V

    .line 414
    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    .line 419
    :goto_1
    invoke-virtual {v1, v5}, Lcom/cigna/coach/dataobjects/UserGoalData;->setNumMissionsCompleted(I)V

    .line 420
    invoke-virtual {v1, v4}, Lcom/cigna/coach/dataobjects/UserGoalData;->setLastMissionCountedDate(Ljava/util/Calendar;)V

    .line 421
    invoke-direct {p0, p1, v1}, Lcom/cigna/coach/utils/GoalMissionHelper;->updateUserGoal(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)V

    .line 423
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getContentId()I

    move-result v6

    invoke-virtual {p3, v6}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setGoalContentId(I)V

    .line 424
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getImageContentId()I

    move-result v6

    invoke-virtual {p3, v6}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setGoalImageContentId(I)V

    .line 425
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getFrequencyContentId()I

    move-result v6

    invoke-virtual {p3, v6}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setGoalFrequencyContentId(I)V

    .line 426
    invoke-virtual {p3, v3}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setGoalProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;)V

    .line 427
    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v6}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setGoalProgressStatus(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 416
    :cond_3
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v1, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    goto :goto_1
.end method

.method private updateUserGoal(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)V
    .locals 20
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 471
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 472
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Preparing to update the goal in database"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :cond_0
    new-instance v11, Lcom/cigna/coach/db/LifeStyleDBManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v11, v3}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 476
    .local v11, "lsDbManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    new-instance v9, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v9, v3}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 477
    .local v9, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    new-instance v13, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v13, v3}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 478
    .local v13, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v8

    .line 479
    .local v8, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v9, v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->updateUserGoal(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)I

    .line 482
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v3

    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3, v6}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    .line 484
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 485
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Sending goal completed broadcast"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_1
    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendGoalCompletedBroadcast(Lcom/cigna/coach/dataobjects/UserGoalData;)V

    .line 489
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 490
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting all non completed missions for this goal"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v3}, Lcom/cigna/coach/db/MissionDBManager;->getNonCompleteUserMissionsForGoal(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v14

    .line 494
    .local v14, "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    if-eqz v14, :cond_6

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 495
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 496
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Need to cancel "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " missions"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_3
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 499
    .local v12, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 500
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cancelling missionid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", seq num:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_4
    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v3

    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v6

    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v3, v6, v7}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMissionStatus(Ljava/lang/String;IILcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    goto :goto_0

    .line 506
    .end local v12    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_5
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 507
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "All active missions cancelled"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_6
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 512
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Goal completed... Getting next optimal answer"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v17

    .line 516
    .local v17, "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v3, v6}, Lcom/cigna/coach/db/LifeStyleDBManager;->getNextOptimalAnswerForUser(Ljava/lang/String;II)Lcom/cigna/coach/dataobjects/QuestionAnswerData;

    move-result-object v15

    .line 517
    .local v15, "nextAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    if-eqz v15, :cond_10

    .line 518
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 519
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Next optimal answer is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v15}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 522
    :cond_8
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 523
    .local v18, "qaList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    move-object/from16 v0, v18

    invoke-interface {v0, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524
    invoke-virtual/range {v17 .. v18}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->setQuestionAnswer(Ljava/util/List;)V

    .line 525
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 526
    .local v19, "qagList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 529
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Saving the answer as coach response"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    :cond_9
    sget-object v3, Lcom/cigna/coach/LifeStyle$AnswerSource;->COACH:Lcom/cigna/coach/LifeStyle$AnswerSource;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v11, v0, v1, v3}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentAnswers(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/LifeStyle$AnswerSource;)V

    .line 533
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 534
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting category for question group"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :cond_a
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v3

    invoke-virtual {v11, v3}, Lcom/cigna/coach/db/LifeStyleDBManager;->getCategoryOfQuestionGroup(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v4

    .line 537
    .local v4, "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    if-nez v4, :cond_b

    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "no such question group id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 539
    :cond_b
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 540
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Category is :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting all latest responses for category"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    :cond_c
    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v11, v0, v4, v3, v6}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserLifeStyleAssesmentResponses(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/Calendar;Z)Ljava/util/List;

    move-result-object v5

    .line 546
    .local v5, "userResponses":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 547
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Recomputing the new category score and getting the next goal"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    :cond_d
    new-instance v2, Lcom/cigna/coach/utils/ScoringHelper;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v2, v3}, Lcom/cigna/coach/utils/ScoringHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 551
    .local v2, "scorer":Lcom/cigna/coach/utils/ScoringHelper;
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v6

    sget-object v7, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/cigna/coach/utils/ScoringHelper;->computeScoreForCategoryBasedOnUserResponse(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/List;ILcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)Z

    .line 553
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 554
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Recomputing the new overall score and getting the next goal"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :cond_e
    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/cigna/coach/utils/ScoringHelper;->computeOverallScore(Ljava/lang/String;)I

    move-result v16

    .line 558
    .local v16, "overAllScore":I
    if-ltz v16, :cond_f

    .line 560
    sget-object v3, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    const/4 v6, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v11, v0, v3, v6, v1}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V

    .line 561
    move/from16 v0, v16

    invoke-virtual {v8, v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendScoreChangedBroadcast(I)V

    .line 576
    .end local v2    # "scorer":Lcom/cigna/coach/utils/ScoringHelper;
    .end local v4    # "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v5    # "userResponses":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    .end local v14    # "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .end local v15    # "nextAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v16    # "overAllScore":I
    .end local v17    # "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    .end local v18    # "qaList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    .end local v19    # "qagList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    :cond_f
    :goto_1
    return-void

    .line 565
    .restart local v14    # "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .restart local v15    # "nextAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .restart local v17    # "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :cond_10
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 566
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "No next optimal answer. Nothing else to do"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 570
    .end local v14    # "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .end local v15    # "nextAnswer":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v17    # "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :cond_11
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 571
    sget-object v3, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Goal not completed.. carry on"

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private updateUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "mission"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 444
    sget-object v1, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 445
    sget-object v1, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Preparing to update the user mission in database"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/MissionDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 449
    .local v0, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/cigna/coach/db/MissionDBManager;->deleteUserMissionData(Ljava/lang/String;II)I

    .line 451
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionDetails()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/cigna/coach/db/MissionDBManager;->createUserMissionData(Ljava/lang/String;IILjava/util/List;)I

    .line 453
    invoke-virtual {v0, p1, p2}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)I

    .line 455
    return-void
.end method


# virtual methods
.method public checkForMissionExpiry(Ljava/lang/String;)V
    .locals 18
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 685
    new-instance v6, Lcom/cigna/coach/db/GoalDBManager;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v6, v15}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 687
    .local v6, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v6, v0, v15}, Lcom/cigna/coach/db/GoalDBManager;->getAllActiveUserGoalsAndMissions(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v3

    .line 689
    .local v3, "activeGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    if-eqz v3, :cond_4

    .line 690
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;

    .line 691
    .local v2, "activeGoal":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v5

    .line 692
    .local v5, "activeMissions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .local v4, "activeMission":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    move-object v14, v4

    .line 693
    check-cast v14, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 694
    .local v14, "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v12

    .line 696
    .local v12, "missionSequenceId":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v13

    .line 697
    .local v13, "now":Ljava/util/Calendar;
    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/UserMissionData;->getExpirationDate()Ljava/util/Calendar;

    move-result-object v11

    .line 700
    .local v11, "missionExpirationDate":Ljava/util/Calendar;
    sget-object v15, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v15}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 701
    sget-object v15, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Now:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v13}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    sget-object v15, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "MissionExpirationDate:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v11}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :cond_2
    invoke-virtual {v13, v11}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v15

    if-ltz v15, :cond_1

    .line 707
    sget-object v15, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v15}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 708
    sget-object v15, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Mission Expired: mission ID"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_3
    new-instance v9, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v9, v15}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 712
    .local v9, "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    invoke-virtual {v9, v12}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMissionWithFailedStatus(I)I

    .line 713
    invoke-virtual {v9, v12}, Lcom/cigna/coach/db/MissionDBManager;->getUserMissionDetails(I)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v10

    .line 714
    .local v10, "missionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-eqz v10, :cond_1

    .line 715
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14, v10}, Lcom/cigna/coach/utils/GoalMissionHelper;->sendMissionUpdateBroadcasts(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/dataobjects/UserMissionData;)V

    goto/16 :goto_0

    .line 720
    .end local v2    # "activeGoal":Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
    .end local v4    # "activeMission":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .end local v5    # "activeMissions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    .end local v10    # "missionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    .end local v11    # "missionExpirationDate":Ljava/util/Calendar;
    .end local v12    # "missionSequenceId":I
    .end local v13    # "now":Ljava/util/Calendar;
    .end local v14    # "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_4
    return-void
.end method

.method public getUserAvailableGoalsCount(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)I
    .locals 3
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 762
    new-instance v1, Lcom/cigna/coach/db/GoalDBManager;

    iget-object v2, p0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v1, v2}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 763
    .local v1, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    sget-object v2, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {p2, v2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 764
    invoke-virtual {v1, p1}, Lcom/cigna/coach/db/GoalDBManager;->getAvailableGoalCount(Ljava/lang/String;)I

    move-result v2

    .line 767
    :goto_0
    return v2

    .line 766
    :cond_0
    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .line 767
    .local v0, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {v1, p1, v0}, Lcom/cigna/coach/db/GoalDBManager;->getAvailableGoalCountByCategory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I

    move-result v2

    goto :goto_0
.end method

.method public mergeManualAndTrackerActivities(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "manualActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .local p2, "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    sget-object v10, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 119
    sget-object v10, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v11, "mergeManualAndTrackerActivities(): START"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const-string v10, "Manual activities"

    invoke-direct {p0, v10, p1}, Lcom/cigna/coach/utils/GoalMissionHelper;->logActivities(Ljava/lang/String;Ljava/util/List;)V

    .line 121
    const-string v10, "Tracker activities"

    invoke-direct {p0, v10, p2}, Lcom/cigna/coach/utils/GoalMissionHelper;->logActivities(Ljava/lang/String;Ljava/util/List;)V

    .line 124
    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/GoalMissionHelper$1;

    invoke-direct {v0, p0}, Lcom/cigna/coach/utils/GoalMissionHelper$1;-><init>(Lcom/cigna/coach/utils/GoalMissionHelper;)V

    .line 130
    .local v0, "dateComparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 131
    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 133
    sget-object v10, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 134
    const-string v10, "Manual activities (sorted)"

    invoke-direct {p0, v10, p1}, Lcom/cigna/coach/utils/GoalMissionHelper;->logActivities(Ljava/lang/String;Ljava/util/List;)V

    .line 135
    const-string v10, "Tracker activities (sorted)"

    invoke-direct {p0, v10, p2}, Lcom/cigna/coach/utils/GoalMissionHelper;->logActivities(Ljava/lang/String;Ljava/util/List;)V

    .line 138
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 140
    .local v5, "mergedActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    const/4 v4, 0x0

    .line 141
    .local v4, "manualIndex":I
    const/4 v9, 0x0

    .line 143
    .local v9, "trackerIndex":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_5

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_5

    .line 144
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 145
    .local v1, "manualActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 147
    .local v6, "trackerActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-direct {p0, v1}, Lcom/cigna/coach/utils/GoalMissionHelper;->activityDate(Lcom/cigna/coach/apiobjects/MissionDetails;)J

    move-result-wide v2

    .line 148
    .local v2, "manualDate":J
    invoke-direct {p0, v6}, Lcom/cigna/coach/utils/GoalMissionHelper;->activityDate(Lcom/cigna/coach/apiobjects/MissionDetails;)J

    move-result-wide v7

    .line 150
    .local v7, "trackerDate":J
    cmp-long v10, v2, v7

    if-gez v10, :cond_3

    .line 151
    invoke-direct {p0, v1}, Lcom/cigna/coach/utils/GoalMissionHelper;->isTrackerActivity(Lcom/cigna/coach/apiobjects/MissionDetails;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 152
    new-instance v10, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    sget-object v11, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->MANUAL:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    invoke-direct {v10, v1, v11}, Lcom/cigna/coach/dataobjects/MissionDetailsData;-><init>(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 155
    :cond_3
    cmp-long v10, v2, v7

    if-lez v10, :cond_4

    .line 156
    new-instance v10, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    sget-object v11, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->TRACKER:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    invoke-direct {v10, v6, v11}, Lcom/cigna/coach/dataobjects/MissionDetailsData;-><init>(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 159
    :cond_4
    new-instance v10, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    sget-object v11, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->MANUAL:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    invoke-direct {v10, v1, v11}, Lcom/cigna/coach/dataobjects/MissionDetailsData;-><init>(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    add-int/lit8 v9, v9, 0x1

    .line 161
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 165
    .end local v1    # "manualActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    .end local v2    # "manualDate":J
    .end local v6    # "trackerActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    .end local v7    # "trackerDate":J
    :cond_5
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_6

    .line 166
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 167
    .restart local v1    # "manualActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    new-instance v10, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    sget-object v11, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->MANUAL:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    invoke-direct {v10, v1, v11}, Lcom/cigna/coach/dataobjects/MissionDetailsData;-><init>(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v4, v4, 0x1

    .line 169
    goto :goto_1

    .line 171
    .end local v1    # "manualActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    :cond_6
    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_7

    .line 172
    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 173
    .restart local v6    # "trackerActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    new-instance v10, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    sget-object v11, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->TRACKER:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    invoke-direct {v10, v6, v11}, Lcom/cigna/coach/dataobjects/MissionDetailsData;-><init>(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    add-int/lit8 v9, v9, 0x1

    .line 175
    goto :goto_2

    .line 177
    .end local v6    # "trackerActivity":Lcom/cigna/coach/apiobjects/MissionDetails;
    :cond_7
    sget-object v10, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 178
    const-string v10, "Merged activities"

    invoke-direct {p0, v10, v5}, Lcom/cigna/coach/utils/GoalMissionHelper;->logActivities(Ljava/lang/String;Ljava/util/List;)V

    .line 179
    sget-object v10, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v11, "mergeManualAndTrackerActivities(): END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_8
    return-object v5
.end method

.method public processMissionDetailsUpdate(Ljava/lang/String;ILjava/util/List;)Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .locals 24
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)",
            "Lcom/cigna/coach/dataobjects/GoalMissionStatusData;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 203
    .local p3, "newMissionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    new-instance v12, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-direct {v12, v0}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 204
    .local v12, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    new-instance v19, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;

    invoke-direct/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;-><init>()V

    .line 205
    .local v19, "returnStatus":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionId(I)V

    .line 206
    sget-object v21, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->NOT_COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setGoalProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;)V

    .line 207
    sget-object v21, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->NOT_PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V

    .line 209
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 210
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "================================================================================"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_0
    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->NOT_PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 215
    .local v15, "missionProgressStatus":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;
    move/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v12, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v11

    .line 216
    .local v11, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    move/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v12, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v17

    .line 217
    .local v17, "oldMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-eqz v11, :cond_8

    .line 218
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setGoalId(I)V

    .line 219
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getContentId()I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionContentId(I)V

    .line 226
    :cond_1
    :goto_0
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 227
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Processing mission details update"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_2
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 233
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Fetching existing user mission details from database"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_3
    if-eqz v11, :cond_16

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_16

    .line 238
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 239
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Analysing the current data"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Mission id:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " for user:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getUserId()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_4
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getDailyTarget()I

    move-result v5

    .line 244
    .local v5, "dailyTarget":I
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v20

    .line 246
    .local v20, "userSelectedFrequency":I
    const/16 v18, 0x0

    .line 247
    .local v18, "prevNumDaysCountedToMission":I
    const/4 v4, 0x0

    .line 250
    .local v4, "currNumDaysCountedToMission":I
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionDetails()Ljava/util/List;

    move-result-object v14

    .line 251
    .local v14, "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    if-eqz v14, :cond_a

    .line 252
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 253
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Previously there are "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " mission details"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_5
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 257
    .local v13, "missionDetail":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v6

    .line 258
    .local v6, "date":Ljava/util/Calendar;
    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string/jumbo v21, "yyyy-MM-dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v9, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 259
    .local v9, "fmt":Ljava/text/SimpleDateFormat;
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 260
    .local v7, "dateFormatted":Ljava/lang/String;
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 261
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Previously, date "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " has "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " activities"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_7
    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v21

    move/from16 v0, v21

    if-lt v0, v5, :cond_6

    .line 265
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 221
    .end local v4    # "currNumDaysCountedToMission":I
    .end local v5    # "dailyTarget":I
    .end local v6    # "date":Ljava/util/Calendar;
    .end local v7    # "dateFormatted":Ljava/lang/String;
    .end local v9    # "fmt":Ljava/text/SimpleDateFormat;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v13    # "missionDetail":Lcom/cigna/coach/apiobjects/MissionDetails;
    .end local v14    # "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v18    # "prevNumDaysCountedToMission":I
    .end local v20    # "userSelectedFrequency":I
    :cond_8
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 222
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "No user mission found for id:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 269
    .restart local v4    # "currNumDaysCountedToMission":I
    .restart local v5    # "dailyTarget":I
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v14    # "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .restart local v18    # "prevNumDaysCountedToMission":I
    .restart local v20    # "userSelectedFrequency":I
    :cond_9
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 270
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Before update "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " days are counted toward mission completion"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_a
    const/4 v4, 0x0

    .line 275
    if-eqz p3, :cond_11

    .line 277
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_b
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_d

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 279
    .local v16, "newMissionDetail":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v6

    .line 280
    .restart local v6    # "date":Ljava/util/Calendar;
    new-instance v9, Ljava/text/SimpleDateFormat;

    const-string/jumbo v21, "yyyy-MM-dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-direct {v9, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 281
    .restart local v9    # "fmt":Ljava/text/SimpleDateFormat;
    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 283
    .restart local v7    # "dateFormatted":Ljava/lang/String;
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 284
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Currently, date "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " has "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " activities"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_c
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v21

    move/from16 v0, v21

    if-lt v0, v5, :cond_b

    .line 288
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 293
    .end local v6    # "date":Ljava/util/Calendar;
    .end local v7    # "dateFormatted":Ljava/lang/String;
    .end local v9    # "fmt":Ljava/text/SimpleDateFormat;
    .end local v16    # "newMissionDetail":Lcom/cigna/coach/apiobjects/MissionDetails;
    :cond_d
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 294
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "After update "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " days are counted toward mission completion"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "-----------------------------------------------------------"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_e
    move-object/from16 v0, p3

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionDetails(Ljava/util/List;)V

    .line 301
    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Lcom/cigna/coach/db/MissionDBManager;->getMissionActivityCompletedCount(Ljava/util/List;)I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionActivityCompletedTotalCount(I)V

    .line 308
    .end local v10    # "i$":Ljava/util/Iterator;
    :goto_3
    move/from16 v0, v18

    if-gt v4, v0, :cond_12

    .line 309
    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->NOT_PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    .line 325
    :goto_4
    invoke-virtual/range {v19 .. v20}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionActivityTotalCount(I)V

    .line 326
    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionProgressStatusType(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;)V

    .line 327
    invoke-virtual {v15}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionProgressStatus(Ljava/lang/String;)V

    .line 328
    sub-int v21, v4, v18

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setNumActivitiesCurrentlyUpdated(I)V

    .line 329
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerNameContentId()I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setTrackerNameContentId(I)V

    .line 331
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionDetails()Ljava/util/List;

    move-result-object v8

    .line 332
    .local v8, "details":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    invoke-virtual {v12, v8}, Lcom/cigna/coach/db/MissionDBManager;->getMissionActivityCompletedCount(Ljava/util/List;)I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionActivityCompletedTotalCount(I)V

    .line 334
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/cigna/coach/utils/GoalMissionHelper;->updateUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)V

    .line 336
    if-eqz v17, :cond_f

    .line 337
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2, v11}, Lcom/cigna/coach/utils/GoalMissionHelper;->sendMissionUpdateBroadcasts(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/dataobjects/UserMissionData;)V

    .line 341
    :cond_f
    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/utils/GoalMissionHelper;->processGoalCompletion(Ljava/lang/String;ILcom/cigna/coach/dataobjects/GoalMissionStatusData;)Lcom/cigna/coach/dataobjects/GoalMissionStatusData;

    move-result-object v19

    .line 344
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_15

    sget-object v21, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    invoke-virtual {v11}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionStage()Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_15

    const/16 v21, 0x1

    :goto_5
    move-object/from16 v0, v19

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setMissionRepeatable(Z)V

    .line 361
    .end local v4    # "currNumDaysCountedToMission":I
    .end local v5    # "dailyTarget":I
    .end local v8    # "details":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v14    # "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v18    # "prevNumDaysCountedToMission":I
    .end local v20    # "userSelectedFrequency":I
    :goto_6
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 362
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "================================================================================"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_10
    return-object v19

    .line 304
    .restart local v4    # "currNumDaysCountedToMission":I
    .restart local v5    # "dailyTarget":I
    .restart local v14    # "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .restart local v18    # "prevNumDaysCountedToMission":I
    .restart local v20    # "userSelectedFrequency":I
    :cond_11
    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionDetails(Ljava/util/List;)V

    .line 305
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionActivityCompletedTotalCount(I)V

    goto/16 :goto_3

    .line 311
    :cond_12
    move/from16 v0, v20

    if-lt v4, v0, :cond_14

    .line 312
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_13

    .line 313
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Now the mission is complete"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Updating mission status to complete with flag \'Not displayed to user\'"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_13
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionCompletedDate(Ljava/util/Calendar;)V

    .line 317
    sget-object v21, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setCurrentStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    .line 318
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDisplayedToUser(Z)V

    .line 319
    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    goto/16 :goto_4

    .line 321
    :cond_14
    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    goto/16 :goto_4

    .line 344
    .restart local v8    # "details":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :cond_15
    const/16 v21, 0x0

    goto :goto_5

    .line 352
    .end local v4    # "currNumDaysCountedToMission":I
    .end local v5    # "dailyTarget":I
    .end local v8    # "details":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v14    # "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v18    # "prevNumDaysCountedToMission":I
    .end local v20    # "userSelectedFrequency":I
    :cond_16
    sget-object v21, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Error: Unexpected update on a non active mission.."

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method

.method public sendMissionUpdateBroadcasts(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "oldMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .param p3, "newMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    .line 723
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v2

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 759
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 725
    :cond_1
    new-instance v1, Lcom/cigna/coach/utils/WidgetHelper;

    iget-object v2, p0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v1, v2}, Lcom/cigna/coach/utils/WidgetHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 726
    .local v1, "widgetHelper":Lcom/cigna/coach/utils/WidgetHelper;
    iget-object v2, p0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v0

    .line 728
    .local v0, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne v2, v3, :cond_0

    .line 729
    invoke-virtual {v1, p1, p3}, Lcom/cigna/coach/utils/WidgetHelper;->dismissMissionNotifications(Ljava/lang/String;Lcom/cigna/coach/apiobjects/GoalMissionInfo;)V

    .line 731
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper$2;->$SwitchMap$com$cigna$coach$interfaces$IGoalsMissions$MissionStatus:[I

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 733
    :pswitch_1
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 734
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Sending mission completed broadcast"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_2
    invoke-virtual {v0, p3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendMissionCompletedBroadcast(Lcom/cigna/coach/dataobjects/UserMissionData;)V

    goto :goto_0

    .line 739
    :pswitch_2
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 740
    sget-object v2, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Sending mission failed broadcast"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    :cond_3
    invoke-virtual {v0, p3}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendMissionFailedBroadcast(Lcom/cigna/coach/dataobjects/UserMissionData;)V

    goto :goto_0

    .line 731
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setUserMission(Ljava/lang/String;IILcom/cigna/coach/apiobjects/MissionOptions;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 19
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "missionId"    # I
    .param p4, "missionOptions"    # Lcom/cigna/coach/apiobjects/MissionOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Lcom/cigna/coach/apiobjects/MissionOptions;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 582
    new-instance v11, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v11, v4}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 584
    .local v11, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    new-instance v3, Lcom/cigna/coach/utils/CoachMessageHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v3, v4}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 586
    .local v3, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    new-instance v5, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v5}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V

    .line 589
    .local v5, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    if-eqz p4, :cond_6

    .line 590
    :try_start_0
    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getMissionStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v15

    .line 591
    .local v15, "status":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    move/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v11, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v14

    .line 592
    .local v14, "oldUserMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    move/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v11, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v17

    .line 594
    .local v17, "userMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-eqz v17, :cond_7

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v4

    const/4 v8, -0x1

    if-le v4, v8, :cond_7

    .line 596
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/cigna/coach/dataobjects/UserMissionData;->setCurrentStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    .line 597
    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getSelectedFrequency()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->setSelectedFrequencyDays(I)V

    .line 598
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 599
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Selected frequency is:"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getSelectedFrequency()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v11, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)I

    .line 602
    if-eqz v14, :cond_1

    .line 603
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v14, v2}, Lcom/cigna/coach/utils/GoalMissionHelper;->sendMissionUpdateBroadcasts(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/dataobjects/UserMissionData;)V

    .line 637
    :cond_1
    :goto_0
    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getMissionStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 638
    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getMissionStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v4

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v4, v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 640
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v1, v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->setGetGoalsMessagesWhenDeletingMission(Ljava/lang/String;ILcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 644
    :cond_2
    const/4 v6, 0x0

    .line 645
    .local v6, "goalCompleted":Z
    const/4 v7, 0x0

    .line 646
    .local v7, "missionCompleted":Z
    const/4 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->setSetMissionDataMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ZZLjava/util/List;)V

    .line 648
    .end local v6    # "goalCompleted":Z
    .end local v7    # "missionCompleted":Z
    :cond_3
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 649
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Mission Status:"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :cond_4
    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v15, v4}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 655
    move/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v11, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v17

    .line 656
    if-eqz v17, :cond_6

    .line 657
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 658
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Mission Tracker Type:"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    :cond_5
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v4

    sget-object v8, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->NOT_TRACKER_TYPE:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    if-eq v4, v8, :cond_6

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v4

    sget-object v8, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->INVALID:Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    if-eq v4, v8, :cond_6

    .line 662
    new-instance v16, Lcom/cigna/coach/utils/TrackerHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v8

    move-object/from16 v0, v16

    invoke-direct {v0, v4, v8}, Lcom/cigna/coach/utils/TrackerHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;Landroid/content/Context;)V

    .line 663
    .local v16, "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getTrackerType()Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/mobile/coach/DataProcessor;->getTrackerEventType(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;

    move-result-object v4

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4}, Lcom/cigna/coach/utils/TrackerHelper;->processTrackerMissionsForChange(Ljava/lang/String;Lcom/cigna/coach/interfaces/ITracker$TrackerEventType;)Ljava/util/List;

    move-result-object v13

    .line 665
    .local v13, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 666
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v18, "setUserMission: Notifications sent: "

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    .end local v13    # "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    .end local v14    # "oldUserMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    .end local v15    # "status":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .end local v16    # "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    .end local v17    # "userMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_6
    new-instance v9, Lcom/cigna/coach/utils/ContentHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/utils/GoalMissionHelper;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v9, v4}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 674
    .local v9, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v9, v5}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 681
    return-object v5

    .line 606
    .end local v9    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v14    # "oldUserMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    .restart local v15    # "status":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .restart local v17    # "userMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_7
    const/4 v12, 0x0

    .line 607
    .local v12, "missionStartDate":Ljava/util/Calendar;
    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getMissionStartDate()Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 608
    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getMissionStartDate()Ljava/util/Calendar;

    move-result-object v12

    .line 612
    :goto_1
    new-instance v17, Lcom/cigna/coach/dataobjects/UserMissionData;

    .end local v17    # "userMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-direct/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;-><init>()V

    .line 613
    .restart local v17    # "userMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->setUserId(Ljava/lang/String;)V

    .line 614
    move-object/from16 v0, v17

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->setGoalId(I)V

    .line 615
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDisplayedToUser(Z)V

    .line 616
    move-object/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionId(I)V

    .line 617
    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getSelectedFrequency()I

    move-result v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->setSelectedFrequencyDays(I)V

    .line 618
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 619
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Selected frequency is:"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/apiobjects/MissionOptions;->getSelectedFrequency()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    :cond_8
    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v15, v4}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 622
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->setStartDate(Ljava/util/Calendar;)V

    .line 626
    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/cigna/coach/dataobjects/UserMissionData;->setCurrentStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    .line 627
    if-eqz v12, :cond_9

    .line 630
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/cigna/coach/dataobjects/UserMissionData;->setStartDate(Ljava/util/Calendar;)V

    .line 631
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 632
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Setting mission start date: "

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v12}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/Date;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_9
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v11, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->createUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 676
    .end local v12    # "missionStartDate":Ljava/util/Calendar;
    .end local v14    # "oldUserMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    .end local v15    # "status":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .end local v17    # "userMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v10

    .line 677
    .local v10, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/cigna/coach/utils/GoalMissionHelper;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error while setUserMission"

    invoke-static {v4, v8, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 678
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v4, v10}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 610
    .end local v10    # "e":Ljava/lang/Exception;
    .restart local v12    # "missionStartDate":Ljava/util/Calendar;
    .restart local v14    # "oldUserMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    .restart local v15    # "status":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .restart local v17    # "userMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_a
    :try_start_1
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v12

    goto/16 :goto_1

    .line 624
    :cond_b
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionCompletedDate(Ljava/util/Calendar;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
