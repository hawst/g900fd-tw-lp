.class Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;
.super Ljava/lang/Object;
.source "JournalHelper.java"

# interfaces
.implements Lcom/cigna/coach/acm/IACMTaskCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackupCompleteCallback"
.end annotation


# instance fields
.field backupStartTime:J

.field broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

.field finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

.field final synthetic this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

.field uploadListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

.field userName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper;JLjava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V
    .locals 8
    .param p2, "backupStartTime"    # J
    .param p4, "userName"    # Ljava/lang/String;
    .param p5, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    const-wide v4, 0x3feccccccccccccdL    # 0.9

    .line 314
    iput-object p1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    .line 315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->backupStartTime:J

    .line 309
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->userName:Ljava/lang/String;

    .line 310
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$100(Lcom/cigna/coach/utils/backuprestore/JournalHelper;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    .line 316
    iput-wide p2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->backupStartTime:J

    .line 317
    iput-object p4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->userName:Ljava/lang/String;

    .line 318
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/16 v2, 0x0

    move-object v1, p5

    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->uploadListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 319
    new-instance v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-object v3, p5

    invoke-direct/range {v2 .. v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    iput-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 320
    return-void
.end method


# virtual methods
.method public onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 5
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "error"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    const/4 v4, 0x0

    .line 419
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Backup request completed with error..Details below"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Backup request error >>apiId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Backup request error >>isSuccess:false"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Backup request error >>refId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/acm/ACMTask;->getTaskReferenceId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Backup request error >>error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    invoke-virtual {p1}, Lcom/cigna/coach/acm/ACMTask;->getTaskReferenceId()I

    move-result v0

    .line 426
    .local v0, "requestId":I
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 427
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Backup failed"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_0
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-virtual {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupFailedBroadcast()V

    .line 431
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v4, p2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 439
    :cond_1
    :goto_0
    return-void

    .line 433
    :cond_2
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 434
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 435
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Erase failed"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_3
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-virtual {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendEraseFailedBroadcast()V

    .line 437
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v4, p2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFinish(Lcom/cigna/coach/acm/ACMTask;JLorg/json/JSONObject;)V
    .locals 15
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "requestId"    # J
    .param p4, "resultData"    # Lorg/json/JSONObject;

    .prologue
    .line 333
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v11, "onFinish : BackupCompleteCallback"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    move-wide/from16 v0, p2

    long-to-int v11, v0

    invoke-static {v11}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v11

    const-wide/16 v12, 0x0

    invoke-interface {v10, v11, v12, v13}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 337
    new-instance v5, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;
    invoke-static {v10}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$100(Lcom/cigna/coach/utils/backuprestore/JournalHelper;)Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x1

    invoke-direct {v5, v10, v11}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 339
    .local v5, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    new-instance v3, Lcom/cigna/coach/db/JournalDBManager;

    invoke-direct {v3, v5}, Lcom/cigna/coach/db/JournalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 340
    .local v3, "dbm":Lcom/cigna/coach/db/JournalDBManager;
    new-instance v2, Lcom/cigna/coach/db/ApiCallDBManager;

    invoke-direct {v2, v5}, Lcom/cigna/coach/db/ApiCallDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 342
    .local v2, "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    invoke-virtual {v3}, Lcom/cigna/coach/db/JournalDBManager;->stopJournalling()V

    .line 343
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Deleting all journal entries"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->userName:Ljava/lang/String;

    iget-wide v11, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->backupStartTime:J

    invoke-virtual {v3, v10, v11, v12}, Lcom/cigna/coach/db/JournalDBManager;->deleteAllJournalEntriesForUser(Ljava/lang/String;J)V

    .line 346
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    move-wide/from16 v0, p2

    long-to-int v11, v0

    invoke-static {v11}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v11

    const-wide v12, 0x3fd3333333333333L    # 0.3

    invoke-interface {v10, v11, v12, v13}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 348
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->userName:Ljava/lang/String;

    invoke-virtual {v2, v10}, Lcom/cigna/coach/db/ApiCallDBManager;->deleteAllApiCallEntriesForUser(Ljava/lang/String;)V

    .line 349
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    move-wide/from16 v0, p2

    long-to-int v11, v0

    invoke-static {v11}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v11

    const-wide v12, 0x3feccccccccccccdL    # 0.9

    invoke-interface {v10, v11, v12, v13}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 351
    sget-object v10, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, p2, v10

    if-nez v10, :cond_3

    .line 352
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Backup Block"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    new-instance v8, Lcom/cigna/coach/db/UserMetricsDBManager;

    invoke-direct {v8, v5}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 355
    .local v8, "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    new-instance v9, Lcom/cigna/coach/utils/UserIdUtils;

    invoke-direct {v9, v5}, Lcom/cigna/coach/utils/UserIdUtils;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 356
    .local v9, "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    invoke-virtual {v9}, Lcom/cigna/coach/utils/UserIdUtils;->getDefaultUserId()Ljava/lang/String;

    move-result-object v7

    .line 357
    .local v7, "userIdTxt":Ljava/lang/String;
    sget-object v10, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->INCREMENTAL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    invoke-virtual {v8, v7, v10}, Lcom/cigna/coach/db/UserMetricsDBManager;->setBackupType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;)V

    .line 359
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 360
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Deleting all journal entries and call log complete"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    .end local v7    # "userIdTxt":Ljava/lang/String;
    .end local v8    # "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    .end local v9    # "userUtils":Lcom/cigna/coach/utils/UserIdUtils;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Lcom/cigna/coach/db/JournalDBManager;->resumeJournalling()V

    .line 382
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    move-wide/from16 v0, p2

    long-to-int v11, v0

    invoke-static {v11}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v11

    const-wide v12, 0x3fee666666666666L    # 0.95

    invoke-interface {v10, v11, v12, v13}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 384
    sget-object v10, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, p2, v10

    if-nez v10, :cond_7

    .line 385
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 386
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Backup complete"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_1
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-virtual {v10}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupCompletedBroadcast()V

    .line 389
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v11, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v12, 0x1

    sget-object v13, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const/4 v14, 0x0

    invoke-interface {v10, v11, v12, v13, v14}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 400
    :goto_1
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V

    .line 401
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    move-wide/from16 v0, p2

    long-to-int v11, v0

    invoke-static {v11}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v11

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    invoke-interface {v10, v11, v12, v13}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    if-eqz v5, :cond_2

    .line 413
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 415
    .end local v2    # "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    .end local v3    # "dbm":Lcom/cigna/coach/db/JournalDBManager;
    :cond_2
    :goto_2
    return-void

    .line 362
    .restart local v2    # "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    .restart local v3    # "dbm":Lcom/cigna/coach/db/JournalDBManager;
    :cond_3
    :try_start_1
    sget-object v10, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, p2, v10

    if-nez v10, :cond_6

    .line 363
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Erase Block"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 366
    :try_start_2
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    const-string v11, "backup_restore_sequence"

    # invokes: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->loadProperties(Ljava/lang/String;)Ljava/util/Properties;
    invoke-static {v10, v11}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$300(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;)Ljava/util/Properties;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    .line 371
    .local v6, "restoreOrder":Ljava/util/Properties;
    :try_start_3
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    # invokes: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->deleteUserTableEntiries(Lcom/cigna/coach/db/JournalDBManager;Ljava/util/Properties;)V
    invoke-static {v10, v3, v6}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$400(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Lcom/cigna/coach/db/JournalDBManager;Ljava/util/Properties;)V

    .line 373
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 374
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Deleting all journal entries,user table entries and call log complete"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 402
    .end local v2    # "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    .end local v3    # "dbm":Lcom/cigna/coach/db/JournalDBManager;
    .end local v6    # "restoreOrder":Ljava/util/Properties;
    :catch_0
    move-exception v4

    .line 403
    .local v4, "e":Ljava/lang/Exception;
    :try_start_4
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "An error occured while BackupCompleteCallback.onFinish"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error is:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    move-wide/from16 v0, p2

    long-to-int v11, v0

    invoke-static {v11}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v11

    const/4 v12, 0x0

    sget-object v13, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_DB_OPERATION:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v10, v11, v12, v13, v14}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 406
    sget-object v10, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, p2, v10

    if-nez v10, :cond_a

    .line 407
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-virtual {v10}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupFailedBroadcast()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 412
    :cond_4
    :goto_3
    if-eqz v5, :cond_2

    .line 413
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto/16 :goto_2

    .line 367
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v2    # "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    .restart local v3    # "dbm":Lcom/cigna/coach/db/JournalDBManager;
    :catch_1
    move-exception v4

    .line 368
    .restart local v4    # "e":Ljava/lang/Exception;
    :try_start_5
    new-instance v10, Ljava/lang/Exception;

    const-string v11, "Unable to load the restore ordering from file \'%s\'."

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const-string v14, "backup_restore_sequence"

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 412
    .end local v2    # "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    .end local v3    # "dbm":Lcom/cigna/coach/db/JournalDBManager;
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v10

    if-eqz v5, :cond_5

    .line 413
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_5
    throw v10

    .line 377
    .restart local v2    # "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    .restart local v3    # "dbm":Lcom/cigna/coach/db/JournalDBManager;
    :cond_6
    :try_start_6
    new-instance v10, Ljava/lang/Exception;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unexpected requestId: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p2

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v10

    .line 390
    :cond_7
    sget-object v10, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, p2, v10

    if-nez v10, :cond_9

    .line 391
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 392
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v10

    const-string v11, "Erase complete"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_8
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-virtual {v10}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendEraseCompletedBroadcast()V

    .line 395
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->finishListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v11, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v12, 0x1

    sget-object v13, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NONE:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    const/4 v14, 0x0

    invoke-interface {v10, v11, v12, v13, v14}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 397
    :cond_9
    new-instance v10, Ljava/lang/Exception;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unexpected requestId: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-wide/from16 v0, p2

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v10
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 408
    .end local v2    # "apiDbm":Lcom/cigna/coach/db/ApiCallDBManager;
    .end local v3    # "dbm":Lcom/cigna/coach/db/JournalDBManager;
    .restart local v4    # "e":Ljava/lang/Exception;
    :cond_a
    :try_start_7
    sget-object v10, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_ERASE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getRequestType()I

    move-result v10

    int-to-long v10, v10

    cmp-long v10, p2, v10

    if-nez v10, :cond_4

    .line 409
    iget-object v10, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->broadcastManager:Lcom/cigna/mobile/coach/notification/BroadcastManager;

    invoke-virtual {v10}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendEraseFailedBroadcast()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3
.end method

.method public onProgress(Lcom/cigna/coach/acm/ACMTask;JD)V
    .locals 2
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "requestId"    # J
    .param p4, "progress"    # D

    .prologue
    .line 324
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$BackupCompleteCallback;->uploadListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    long-to-int v1, p2

    invoke-static {v1}, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->getInstance(I)Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    move-result-object v1

    invoke-interface {v0, v1, p4, p5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 325
    return-void
.end method
