.class public Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;
.super Ljava/lang/Object;
.source "CoachMessageHelper.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/CoachMessageHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CoachMessageRankComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/dataobjects/CoachMessageData;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x6fff1b1b76e428fbL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1522
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/dataobjects/CoachMessageData;Lcom/cigna/coach/dataobjects/CoachMessageData;)I
    .locals 4
    .param p1, "coachMessageData1"    # Lcom/cigna/coach/dataobjects/CoachMessageData;
    .param p2, "coachMessageData2"    # Lcom/cigna/coach/dataobjects/CoachMessageData;

    .prologue
    const/4 v3, -0x1

    .line 1531
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v2

    if-le v2, v3, :cond_0

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v2

    if-le v2, v3, :cond_0

    .line 1532
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v3

    sub-int/2addr v2, v3

    mul-int/lit8 v2, v2, -0x1

    return v2

    .line 1534
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error comparing CoachMessageData ranks: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1541
    .local v1, "msg":Ljava/lang/String;
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 1542
    .local v0, "e":Ljava/lang/RuntimeException;
    # getter for: Lcom/cigna/coach/utils/CoachMessageHelper;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/CoachMessageHelper;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1543
    throw v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 1522
    check-cast p1, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/utils/CoachMessageHelper$CoachMessageRankComparator;->compare(Lcom/cigna/coach/dataobjects/CoachMessageData;Lcom/cigna/coach/dataobjects/CoachMessageData;)I

    move-result v0

    return v0
.end method
