.class public final enum Lcom/cigna/coach/utils/ContentHelper$LocaleType;
.super Ljava/lang/Enum;
.source "ContentHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/ContentHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LocaleType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum ARABIC:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum BAHASA_MALAYSIA:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum CHINESE_HONGKONG:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum DUTCH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum EUROPEAN_SPANISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum FRENCH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum FRENCH_CANADIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum GERMAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum HEBREW:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum HINDI:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum INDONESIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum ITALIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum JAPANESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum KOREAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum LATIN_PORTUGUESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum LATIN_SPANISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum POLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum RUSSIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum SIMPLIFIED_CHINESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum SPANISH_FOR_USA:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum SWEDISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum THAI:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum TRADITIONAL_CHINESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum TURKISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum UK_ENGLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field public static final enum US_ENGLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

.field private static final codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            ">;"
        }
    .end annotation
.end field

.field public static final localeValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/Locale;",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field countryCode:Ljava/lang/String;

.field defaultOnDevice:Z

.field key:I

.field languageCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 81
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "US_ENGLISH"

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7, v9}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->US_ENGLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 82
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "KOREAN"

    invoke-direct {v5, v6, v9, v10}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->KOREAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 83
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "EUROPEAN_SPANISH"

    invoke-direct {v5, v6, v10, v11}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->EUROPEAN_SPANISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 84
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "SPANISH_FOR_USA"

    invoke-direct {v5, v6, v11, v12}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->SPANISH_FOR_USA:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 85
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "ARABIC"

    invoke-direct {v5, v6, v12, v13}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->ARABIC:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 86
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "UK_ENGLISH"

    const/4 v7, 0x6

    invoke-direct {v5, v6, v13, v7}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->UK_ENGLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 87
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "GERMAN"

    const/4 v7, 0x6

    const/4 v8, 0x7

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->GERMAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 88
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "JAPANESE"

    const/4 v7, 0x7

    const/16 v8, 0x8

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->JAPANESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 89
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "ITALIAN"

    const/16 v7, 0x8

    const/16 v8, 0x9

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->ITALIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 90
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "FRENCH"

    const/16 v7, 0x9

    const/16 v8, 0xa

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->FRENCH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 91
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "SWEDISH"

    const/16 v7, 0xa

    const/16 v8, 0xb

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->SWEDISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 92
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "LATIN_PORTUGUESE"

    const/16 v7, 0xb

    const/16 v8, 0xc

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->LATIN_PORTUGUESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 93
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "FRENCH_CANADIAN"

    const/16 v7, 0xc

    const/16 v8, 0xd

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->FRENCH_CANADIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 94
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "DUTCH"

    const/16 v7, 0xd

    const/16 v8, 0xe

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->DUTCH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 95
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "TURKISH"

    const/16 v7, 0xe

    const/16 v8, 0xf

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->TURKISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 96
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "RUSSIAN"

    const/16 v7, 0xf

    const/16 v8, 0x10

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->RUSSIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 97
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "BAHASA_MALAYSIA"

    const/16 v7, 0x10

    const/16 v8, 0x11

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->BAHASA_MALAYSIA:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 98
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "LATIN_SPANISH"

    const/16 v7, 0x11

    const/16 v8, 0x12

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->LATIN_SPANISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 99
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "THAI"

    const/16 v7, 0x12

    const/16 v8, 0x13

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->THAI:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 100
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "INDONESIAN"

    const/16 v7, 0x13

    const/16 v8, 0x14

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->INDONESIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 101
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "HEBREW"

    const/16 v7, 0x14

    const/16 v8, 0x15

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->HEBREW:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 103
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "SIMPLIFIED_CHINESE"

    const/16 v7, 0x15

    const/16 v8, 0x17

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->SIMPLIFIED_CHINESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 104
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "POLISH"

    const/16 v7, 0x16

    const/16 v8, 0x18

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->POLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 106
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "TRADITIONAL_CHINESE"

    const/16 v7, 0x17

    const/16 v8, 0x1a

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->TRADITIONAL_CHINESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 107
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "HINDI"

    const/16 v7, 0x18

    const/16 v8, 0x1b

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->HINDI:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 108
    new-instance v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const-string v6, "CHINESE_HONGKONG"

    const/16 v7, 0x19

    const/16 v8, 0x1c

    invoke-direct {v5, v6, v7, v8}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->CHINESE_HONGKONG:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 80
    const/16 v5, 0x1a

    new-array v5, v5, [Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    const/4 v6, 0x0

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->US_ENGLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    sget-object v6, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->KOREAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v6, v5, v9

    sget-object v6, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->EUROPEAN_SPANISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v6, v5, v10

    sget-object v6, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->SPANISH_FOR_USA:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v6, v5, v11

    sget-object v6, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->ARABIC:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v6, v5, v12

    sget-object v6, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->UK_ENGLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v6, v5, v13

    const/4 v6, 0x6

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->GERMAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/4 v6, 0x7

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->JAPANESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x8

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->ITALIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x9

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->FRENCH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0xa

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->SWEDISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0xb

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->LATIN_PORTUGUESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0xc

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->FRENCH_CANADIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0xd

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->DUTCH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0xe

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->TURKISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0xf

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->RUSSIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x10

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->BAHASA_MALAYSIA:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x11

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->LATIN_SPANISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x12

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->THAI:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x13

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->INDONESIAN:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x14

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->HEBREW:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x15

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->SIMPLIFIED_CHINESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x16

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->POLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x17

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->TRADITIONAL_CHINESE:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x18

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->HINDI:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    const/16 v6, 0x19

    sget-object v7, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->CHINESE_HONGKONG:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    aput-object v7, v5, v6

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->$VALUES:[Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 151
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->codeValueMap:Ljava/util/HashMap;

    .line 152
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    .line 154
    invoke-static {}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->values()[Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 155
    .local v4, "type":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getKey()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const/4 v2, 0x0

    .line 157
    .local v2, "l":Ljava/util/Locale;
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$1;->$SwitchMap$com$cigna$coach$utils$ContentHelper$LocaleType:[I

    invoke-virtual {v4}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 154
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 159
    :pswitch_0
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "en"

    const-string v6, "US"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "en"

    const-string v6, "US"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "CA"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "PA"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "MY"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 168
    :pswitch_1
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "ko"

    const-string v6, "KR"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "ko"

    const-string v6, "KR"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 174
    :pswitch_2
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "es"

    const-string v6, "ES"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "es"

    const-string v6, "ES"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 180
    :pswitch_3
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "es"

    const-string v6, "US"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "es"

    const-string v6, "US"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "es"

    const-string v8, "MX"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "es"

    const-string v8, "PA"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 188
    :pswitch_4
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "ar"

    const-string v6, "AE"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "ar"

    const-string v6, "AE"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "ar"

    const-string v8, "IL"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 195
    :pswitch_5
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "en"

    const-string v6, "GB"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "en"

    const-string v6, "GB"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "AU"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "NZ"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "IN"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "PH"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "SG"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "ZA"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "en"

    const-string v8, "PH"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 208
    :pswitch_6
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "de"

    const-string v6, "DE"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "de"

    const-string v6, "DE"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "de"

    const-string v8, "AT"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "de"

    const-string v8, "CH"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 216
    :pswitch_7
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "ja"

    const-string v6, "JP"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "ja"

    const-string v6, "JP"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 222
    :pswitch_8
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "it"

    const-string v6, "IT"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "it"

    const-string v6, "IT"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 228
    :pswitch_9
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "fr"

    const-string v6, "FR"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "fr"

    const-string v6, "FR"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "fr"

    const-string v8, "CH"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 235
    :pswitch_a
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "sv"

    const-string v6, "SE"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "sv"

    const-string v6, "SE"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 241
    :pswitch_b
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "pt"

    const-string v6, "BR"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "pt"

    const-string v6, "BR"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 247
    :pswitch_c
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "fr"

    const-string v6, "CA"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "fr"

    const-string v6, "CA"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 253
    :pswitch_d
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "nl"

    const-string v6, "NL"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "nl"

    const-string v6, "NL"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 259
    :pswitch_e
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "tr"

    const-string v6, "TR"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "tr"

    const-string v6, "TR"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 265
    :pswitch_f
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "ru"

    const-string v6, "RU"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "ru"

    const-string v6, "RU"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 271
    :pswitch_10
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "ms"

    const-string v6, "MY"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "ms"

    const-string v6, "MY"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string/jumbo v7, "mx"

    const-string v8, "MY"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 278
    :pswitch_11
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "es"

    const-string v6, "CO"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "es"

    const-string v6, "CO"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "es"

    const-string v8, "AR"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "es"

    const-string v8, "CL"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 286
    :pswitch_12
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "th"

    const-string v6, "TH"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "th"

    const-string v6, "TH"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 292
    :pswitch_13
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "in"

    const-string v6, "ID"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "in"

    const-string v6, "ID"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 298
    :pswitch_14
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "he"

    const-string v6, "IL"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "he"

    const-string v6, "IL"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string v7, "iw"

    const-string v8, "IL"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 311
    :pswitch_15
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "zh"

    const-string v6, "CN"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "zh"

    const-string v6, "CN"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    new-instance v6, Ljava/util/Locale;

    const-string/jumbo v7, "zh"

    const-string v8, "MY"

    invoke-direct {v6, v7, v8}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 318
    :pswitch_16
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "pl"

    const-string v6, "PL"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "pl"

    const-string v6, "PL"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 330
    :pswitch_17
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "zh"

    const-string v6, "TW"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "zh"

    const-string v6, "TW"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 336
    :pswitch_18
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string v5, "hi"

    const-string v6, "IN"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    .restart local v2    # "l":Ljava/util/Locale;
    const-string v5, "hi"

    const-string v6, "IN"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 342
    :pswitch_19
    new-instance v2, Ljava/util/Locale;

    .end local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "zh"

    const-string v6, "HK"

    invoke-direct {v2, v5, v6}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    .restart local v2    # "l":Ljava/util/Locale;
    const-string/jumbo v5, "zh"

    const-string v6, "HK"

    invoke-direct {v4, v5, v6}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    sget-object v5, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 352
    .end local v2    # "l":Ljava/util/Locale;
    .end local v4    # "type":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    :cond_0
    return-void

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3, "sKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->defaultOnDevice:Z

    .line 116
    iput p3, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->key:I

    .line 117
    return-void
.end method

.method public static getDefault()Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->US_ENGLISH:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    return-object v0
.end method

.method public static getInstance(I)Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 2
    .param p0, "key"    # I

    .prologue
    .line 355
    sget-object v0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    return-object v0
.end method

.method public static getInstance(Ljava/util/Locale;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 5
    .param p0, "l"    # Ljava/util/Locale;

    .prologue
    .line 359
    new-instance v1, Ljava/util/Locale;

    const-string v3, "es"

    const-string v4, "US"

    invoke-direct {v1, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    .local v1, "esUS":Ljava/util/Locale;
    new-instance v0, Ljava/util/Locale;

    const-string v3, "es"

    const-string v4, "CO"

    invoke-direct {v0, v3, v4}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    .local v0, "esCO":Ljava/util/Locale;
    invoke-virtual {p0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-boolean v3, Lcom/cigna/coach/utils/ContentHelper;->isLatinSpanish:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 362
    move-object p0, v0

    .line 364
    :cond_0
    sget-object v3, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->localeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 365
    .local v2, "returnValue":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    if-nez v2, :cond_1

    .line 366
    invoke-static {}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getDefault()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v2

    .line 368
    :cond_1
    return-object v2
.end method

.method private setCountryCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->countryCode:Ljava/lang/String;

    .line 142
    return-void
.end method

.method private setLanguageAndCountryCode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "languageCode"    # Ljava/lang/String;
    .param p2, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->languageCode:Ljava/lang/String;

    .line 146
    iput-object p2, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->countryCode:Ljava/lang/String;

    .line 147
    return-void
.end method

.method private setLanguageCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "languageCode"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->languageCode:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 80
    const-class v0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->$VALUES:[Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    invoke-virtual {v0}, [Lcom/cigna/coach/utils/ContentHelper$LocaleType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    return-object v0
.end method


# virtual methods
.method public getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->key:I

    return v0
.end method

.method public getLanguageCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->languageCode:Ljava/lang/String;

    return-object v0
.end method
