.class public final enum Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
.super Ljava/lang/Enum;
.source "SharedPrefUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/SharedPrefUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoachSettingsType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

.field public static final enum COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

.field public static final enum SAMSUNG_ACCOUNT_NAME:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

.field public static final enum SHEALTH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

.field public static final enum WIDGET_SCORE_NO_GOAL_DISPLAY_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

.field public static final enum WIDGET_SCORE_NO_MISSION_DISPLAY_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field settingsType:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 34
    new-instance v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    const-string v5, "SHEALTH_STARTED_SETTING"

    invoke-direct {v4, v5, v11, v7}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->SHEALTH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .line 35
    new-instance v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    const-string v5, "COACH_STARTED_SETTING"

    invoke-direct {v4, v5, v7, v8}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .line 36
    new-instance v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    const-string v5, "WIDGET_SCORE_NO_GOAL_DISPLAY_SETTING"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->WIDGET_SCORE_NO_GOAL_DISPLAY_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .line 37
    new-instance v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    const-string v5, "WIDGET_SCORE_NO_MISSION_DISPLAY_SETTING"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->WIDGET_SCORE_NO_MISSION_DISPLAY_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .line 38
    new-instance v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    const-string v5, "SAMSUNG_ACCOUNT_NAME"

    const/4 v6, 0x5

    invoke-direct {v4, v5, v10, v6}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->SAMSUNG_ACCOUNT_NAME:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .line 33
    const/4 v4, 0x5

    new-array v4, v4, [Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    sget-object v5, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->SHEALTH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->WIDGET_SCORE_NO_GOAL_DISPLAY_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->WIDGET_SCORE_NO_MISSION_DISPLAY_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->SAMSUNG_ACCOUNT_NAME:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    aput-object v5, v4, v10

    sput-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->$VALUES:[Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    .line 50
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->codeValueMap:Ljava/util/HashMap;

    .line 52
    invoke-static {}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->values()[Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 53
    .local v3, "type":Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
    sget-object v4, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->getSettingsType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 55
    .end local v3    # "type":Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "settingsType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput p3, p0, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->settingsType:I

    .line 46
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
    .locals 2
    .param p0, "settingsTypeKey"    # I

    .prologue
    .line 64
    sget-object v0, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->$VALUES:[Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-virtual {v0}, [Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    return-object v0
.end method


# virtual methods
.method public getSettingsType()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->settingsType:I

    return v0
.end method
