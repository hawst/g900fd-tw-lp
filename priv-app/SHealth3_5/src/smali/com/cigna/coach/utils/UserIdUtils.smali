.class public Lcom/cigna/coach/utils/UserIdUtils;
.super Ljava/lang/Object;
.source "UserIdUtils.java"


# instance fields
.field private dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/utils/UserIdUtils;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 45
    iput-object p1, p0, Lcom/cigna/coach/utils/UserIdUtils;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 46
    return-void
.end method

.method public static getDefaultUserId(Lcom/cigna/coach/db/CoachSQLiteHelper;)Ljava/lang/String;
    .locals 2
    .param p0, "dbHelper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 89
    new-instance v0, Lcom/cigna/coach/db/UserDBManager;

    invoke-direct {v0, p0}, Lcom/cigna/coach/db/UserDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 90
    .local v0, "userDBM":Lcom/cigna/coach/db/UserDBManager;
    invoke-virtual {v0}, Lcom/cigna/coach/db/UserDBManager;->getDefaultUserId()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 1
    .param p0, "sqlDatabase"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/cigna/coach/db/UserDBManager;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getDefaultUserId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/cigna/coach/db/UserDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/UserIdUtils;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/UserDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 80
    .local v0, "userDBM":Lcom/cigna/coach/db/UserDBManager;
    invoke-virtual {v0}, Lcom/cigna/coach/db/UserDBManager;->getDefaultUserId()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getUserName(I)Ljava/lang/String;
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 69
    new-instance v0, Lcom/cigna/coach/db/UserDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/UserIdUtils;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/UserDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 70
    .local v0, "userDBM":Lcom/cigna/coach/db/UserDBManager;
    invoke-virtual {v0, p1}, Lcom/cigna/coach/db/UserDBManager;->getUserName(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public resetAllUserEntries()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 109
    new-instance v0, Lcom/cigna/coach/db/UserDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/UserIdUtils;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/UserDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 110
    .local v0, "userDBM":Lcom/cigna/coach/db/UserDBManager;
    invoke-virtual {v0}, Lcom/cigna/coach/db/UserDBManager;->resetAllUserEntries()V

    .line 111
    return-void
.end method

.method public resetUserEntries()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 118
    new-instance v0, Lcom/cigna/coach/db/UserDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/UserIdUtils;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/UserDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 119
    .local v0, "userDBM":Lcom/cigna/coach/db/UserDBManager;
    invoke-virtual {v0}, Lcom/cigna/coach/db/UserDBManager;->resetUserEntries()V

    .line 120
    return-void
.end method

.method public updateUserName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "currentUserName"    # Ljava/lang/String;
    .param p2, "newUserName"    # Ljava/lang/String;

    .prologue
    .line 100
    new-instance v0, Lcom/cigna/coach/db/UserDBManager;

    iget-object v1, p0, Lcom/cigna/coach/utils/UserIdUtils;->dbHelper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/UserDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 101
    .local v0, "userDBM":Lcom/cigna/coach/db/UserDBManager;
    invoke-virtual {v0, p1, p2}, Lcom/cigna/coach/db/UserDBManager;->updateUserName(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method
