.class public Lcom/cigna/coach/utils/CommonUtils;
.super Ljava/lang/Object;
.source "CommonUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/CommonUtils$1;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/cigna/coach/utils/CommonUtils;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    return-void
.end method

.method public static closeSafely(Ljava/io/Closeable;)V
    .locals 3
    .param p0, "object"    # Ljava/io/Closeable;

    .prologue
    .line 43
    if-eqz p0, :cond_0

    .line 45
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static defineServerURLsbasedOnRegionGroup(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x3

    .line 103
    :try_start_0
    const-string v1, ""

    .line 104
    .local v1, "mcc":Ljava/lang/String;
    const-string/jumbo v6, "phone"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 105
    .local v5, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v3

    .line 106
    .local v3, "networkOperator":Ljava/lang/String;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v7, :cond_3

    .line 107
    const-string v6, "RegionGroup"

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 108
    const-string v6, "RegionGroup"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "networkOperator"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_0
    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 111
    const-string v6, "RegionGroup"

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 112
    const-string v6, "RegionGroup"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "mcc code"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    if-eqz v1, :cond_3

    .line 115
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 116
    .local v2, "mccVal":I
    const/16 v6, 0x2d2

    if-eq v2, v6, :cond_2

    const/16 v6, 0x2dc

    if-eq v2, v6, :cond_2

    const/16 v6, 0x2da

    if-ne v2, v6, :cond_3

    .line 117
    :cond_2
    const/4 v6, 0x1

    sput-boolean v6, Lcom/cigna/coach/utils/ContentHelper;->isLatinSpanish:Z

    .line 121
    .end local v2    # "mccVal":I
    :cond_3
    invoke-static {v1}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->getRegionGroup(Ljava/lang/String;)Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    move-result-object v4

    .line 122
    .local v4, "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    const-string v6, "RegionGroup"

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 123
    const-string v6, "RegionGroup"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "region:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_4
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils$1;->$SwitchMap$com$sec$android$service$health$cp$common$HttpConnectionConstants$RegionGroup:[I

    invoke-virtual {v4}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 143
    const-string v6, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v6, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 144
    const-string/jumbo v6, "svc.samsungshealth.com"

    sput-object v6, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :goto_0
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 152
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "articleBaseUrl:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_5
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 155
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SERVER_DOMAIN:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    .end local v1    # "mcc":Ljava/lang/String;
    .end local v3    # "networkOperator":Ljava/lang/String;
    .end local v4    # "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_6
    :goto_1
    return-void

    .line 127
    .restart local v1    # "mcc":Ljava/lang/String;
    .restart local v3    # "networkOperator":Ljava/lang/String;
    .restart local v4    # "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .restart local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :pswitch_0
    :try_start_1
    const-string v6, "http://cn-img.samsungshealth.com/Cigna/Articles/"

    sput-object v6, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 128
    const-string v6, "cn-svc.samsungshealth.com"

    sput-object v6, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 147
    .end local v1    # "mcc":Ljava/lang/String;
    .end local v3    # "networkOperator":Ljava/lang/String;
    .end local v4    # "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v6, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 149
    const-string/jumbo v6, "svc.samsungshealth.com"

    sput-object v6, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 151
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 152
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "articleBaseUrl:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_7
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 155
    sget-object v6, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SERVER_DOMAIN:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 131
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "mcc":Ljava/lang/String;
    .restart local v3    # "networkOperator":Ljava/lang/String;
    .restart local v4    # "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .restart local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :pswitch_1
    :try_start_3
    const-string v6, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v6, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 132
    const-string v6, "eu-svc.samsungshealth.com"

    sput-object v6, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 151
    .end local v1    # "mcc":Ljava/lang/String;
    .end local v3    # "networkOperator":Ljava/lang/String;
    .end local v4    # "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :catchall_0
    move-exception v6

    sget-object v7, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 152
    sget-object v7, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "articleBaseUrl:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_8
    sget-object v7, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 155
    sget-object v7, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SERVER_DOMAIN:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    throw v6

    .line 135
    .restart local v1    # "mcc":Ljava/lang/String;
    .restart local v3    # "networkOperator":Ljava/lang/String;
    .restart local v4    # "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    .restart local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :pswitch_2
    :try_start_4
    const-string v6, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v6, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 136
    const-string v6, "kr-svc.samsungshealth.com"

    sput-object v6, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    goto/16 :goto_0

    .line 139
    :pswitch_3
    const-string v6, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v6, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 140
    const-string/jumbo v6, "svc.samsungshealth.com"

    sput-object v6, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static deviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    const-string/jumbo v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 97
    .local v1, "telephonyManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "deviceId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .end local v0    # "deviceId":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "deviceId":Ljava/lang/String;
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static isOnline(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 81
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 82
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    sget-object v2, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    sget-object v2, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    const-string v3, " isOnline : True"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    const/4 v2, 0x1

    .line 91
    :goto_0
    return v2

    .line 88
    :cond_1
    sget-object v2, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    sget-object v2, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    const-string v3, " isOnline : False"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static readLocalResourceAsString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 53
    sget-object v5, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 54
    sget-object v5, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Reading resource with id:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 58
    .local v4, "returnData":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 61
    .local v1, "file":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v6

    const-string v7, "UTF-8"

    invoke-direct {v5, v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v2, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    .end local v1    # "file":Ljava/io/BufferedReader;
    .local v2, "file":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "line":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 64
    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 66
    .end local v3    # "line":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 67
    .end local v2    # "file":Ljava/io/BufferedReader;
    .local v0, "e":Ljava/io/IOException;
    .restart local v1    # "file":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    sget-object v5, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "readLocalResourceAsString: ERROR: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    new-instance v5, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v5, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 70
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-static {v1}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    throw v5

    .end local v1    # "file":Ljava/io/BufferedReader;
    .restart local v2    # "file":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Lcom/cigna/coach/utils/CommonUtils;->closeSafely(Ljava/io/Closeable;)V

    .line 73
    sget-object v5, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 74
    sget-object v5, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "readLocalResourceAsString: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 70
    .end local v3    # "line":Ljava/lang/String;
    :catchall_1
    move-exception v5

    move-object v1, v2

    .end local v2    # "file":Ljava/io/BufferedReader;
    .restart local v1    # "file":Ljava/io/BufferedReader;
    goto :goto_2

    .line 66
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public static serverUrls(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    invoke-static {p0}, Lcom/cigna/coach/acm/SamsungAccountManager;->getInstance(Landroid/content/Context;)Lcom/cigna/coach/acm/SamsungAccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cigna/coach/acm/SamsungAccountManager;->getMCC()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants;->getRegionGroup(Ljava/lang/String;)Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;

    move-result-object v0

    .line 166
    .local v0, "region":Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;
    const-string v1, "RegionGroup"

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    const-string v1, "RegionGroup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Access token Mcc, region:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    sget-object v1, Lcom/cigna/coach/utils/CommonUtils$1;->$SwitchMap$com$sec$android$service$health$cp$common$HttpConnectionConstants$RegionGroup:[I

    invoke-virtual {v0}, Lcom/sec/android/service/health/cp/common/HttpConnectionConstants$RegionGroup;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 187
    const-string v1, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v1, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 188
    const-string/jumbo v1, "svc.samsungshealth.com"

    sput-object v1, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    .line 191
    :goto_0
    sget-object v1, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    sget-object v1, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Access token Mcc, articleBaseUrl:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_1
    sget-object v1, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    sget-object v1, Lcom/cigna/coach/utils/CommonUtils;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Access token Mcc, SERVER_DOMAIN:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    :cond_2
    return-void

    .line 171
    :pswitch_0
    const-string v1, "http://cn-img.samsungshealth.com/Cigna/Articles/"

    sput-object v1, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 172
    const-string v1, "cn-svc.samsungshealth.com"

    sput-object v1, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    goto :goto_0

    .line 175
    :pswitch_1
    const-string v1, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v1, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 176
    const-string v1, "eu-svc.samsungshealth.com"

    sput-object v1, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    goto :goto_0

    .line 179
    :pswitch_2
    const-string v1, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v1, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 180
    const-string v1, "kr-svc.samsungshealth.com"

    sput-object v1, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    goto :goto_0

    .line 183
    :pswitch_3
    const-string v1, "http://img.samsungshealth.com/Cigna/Articles/"

    sput-object v1, Lcom/cigna/coach/utils/ContentHelper;->articleBaseUrl:Ljava/lang/String;

    .line 184
    const-string/jumbo v1, "svc.samsungshealth.com"

    sput-object v1, Lcom/cigna/coach/acm/NetworkConstants;->SERVER_DOMAIN:Ljava/lang/String;

    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
