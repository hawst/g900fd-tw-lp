.class Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;
.super Ljava/lang/Object;
.source "JournalHelper.java"

# interfaces
.implements Lcom/cigna/coach/acm/IACMTaskCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RestoreCompleteCallback"
.end annotation


# instance fields
.field downloadListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

.field restoreListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

.field final synthetic this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

.field userName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V
    .locals 8
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    const-wide v4, 0x3fe3333333333333L    # 0.6

    .line 219
    iput-object p1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->userName:Ljava/lang/String;

    .line 221
    iput-object p2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->userName:Ljava/lang/String;

    .line 222
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/16 v2, 0x0

    move-object v1, p3

    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->downloadListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 223
    new-instance v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-object v3, p3

    invoke-direct/range {v2 .. v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    iput-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->restoreListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 224
    return-void
.end method


# virtual methods
.method public onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 5
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "error"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 239
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Restore request completed with error..Details below"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restore request error >>apiId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Restore request error >>isSuccess:false"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restore request error >>refId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/acm/ACMTask;->getTaskReferenceId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Restore request error >>error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$100(Lcom/cigna/coach/utils/backuprestore/JournalHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v0

    .line 245
    .local v0, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendRestoreFailedBroadcast()V

    .line 247
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->restoreListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, p2, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 248
    return-void
.end method

.method public onFinish(Lcom/cigna/coach/acm/ACMTask;JLorg/json/JSONObject;)V
    .locals 4
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "requestId"    # J
    .param p4, "resultData"    # Lorg/json/JSONObject;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->restoreListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 234
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->userName:Ljava/lang/String;

    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->restoreListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    invoke-virtual {v0, v1, p4, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->applyRestoreResponse(Ljava/lang/String;Lorg/json/JSONObject;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    .line 235
    return-void
.end method

.method public onProgress(Lcom/cigna/coach/acm/ACMTask;JD)V
    .locals 2
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "requestId"    # J
    .param p4, "progress"    # D

    .prologue
    .line 228
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$RestoreCompleteCallback;->downloadListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_RESTORE:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-interface {v0, v1, p4, p5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 229
    return-void
.end method
