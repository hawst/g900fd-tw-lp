.class Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;
.super Ljava/lang/Object;
.source "JournalHelper.java"

# interfaces
.implements Lcom/cigna/coach/acm/IACMTaskCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCountCompleteCallback"
.end annotation


# instance fields
.field backupListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

.field isBeforeRestore:Z

.field queryListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

.field final synthetic this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

.field userName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;ZLcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)V
    .locals 8
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "isBeforeRestore"    # Z
    .param p4, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .prologue
    const-wide v4, 0x3fd3333333333333L    # 0.3

    .line 257
    iput-object p1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    const-string v0, ""

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->userName:Ljava/lang/String;

    .line 259
    iput-object p2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->userName:Ljava/lang/String;

    .line 260
    iput-boolean p3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->isBeforeRestore:Z

    .line 261
    new-instance v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/16 v2, 0x0

    move-object v1, p4

    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->queryListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 262
    new-instance v2, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-object v3, p4

    invoke-direct/range {v2 .. v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$PartialActivityListener;-><init>(Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;DD)V

    iput-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->backupListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    .line 263
    return-void
.end method


# virtual methods
.method public onError(Lcom/cigna/coach/acm/ACMTask;Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;)V
    .locals 5
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "error"    # Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    .prologue
    .line 294
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Backup request completed with error..Details below"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Backup request error >>apiId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/acm/ACMTask;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Backup request error >>isSuccess:false"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Backup request error >>refId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/acm/ACMTask;->getTaskReferenceId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Backup request error >>error:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;
    invoke-static {v1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$100(Lcom/cigna/coach/utils/backuprestore/JournalHelper;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v0

    .line 300
    .local v0, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    invoke-virtual {v0}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupFailedBroadcast()V

    .line 302
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->backupListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v2, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v3, 0x0

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->name()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, p2, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method public onFinish(Lcom/cigna/coach/acm/ACMTask;JLorg/json/JSONObject;)V
    .locals 9
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "requestId"    # J
    .param p4, "resultData"    # Lorg/json/JSONObject;

    .prologue
    .line 272
    iget-object v4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->backupListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 273
    iget-object v4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->context:Landroid/content/Context;
    invoke-static {v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$100(Lcom/cigna/coach/utils/backuprestore/JournalHelper;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v1

    .line 275
    .local v1, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    :try_start_0
    const-string v4, "count"

    invoke-virtual {p4, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 277
    .local v3, "rowCount":I
    if-nez v3, :cond_1

    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .line 280
    .local v0, "backupType":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    :goto_0
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 281
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "rowCount = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; backupType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_0
    iget-object v4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->this$0:Lcom/cigna/coach/utils/backuprestore/JournalHelper;

    iget-object v5, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->userName:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->isBeforeRestore:Z

    iget-object v7, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->backupListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    # invokes: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->sendBackup(Ljava/lang/String;ZLcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z
    invoke-static {v4, v5, v6, v0, v7}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$200(Lcom/cigna/coach/utils/backuprestore/JournalHelper;Ljava/lang/String;ZLcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Z

    .line 290
    .end local v0    # "backupType":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .end local v3    # "rowCount":I
    :goto_1
    return-void

    .line 278
    .restart local v3    # "rowCount":I
    :cond_1
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->INCREMENTAL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v0    # "backupType":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    goto :goto_0

    .line 284
    .end local v0    # "backupType":Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .end local v3    # "rowCount":I
    :catch_0
    move-exception v2

    .line 285
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendBackupFailedBroadcast()V

    .line 286
    iget-object v4, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->backupListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v5, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const/4 v6, 0x0

    sget-object v7, Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;->ERROR_NO_BACKUP_DATA:Lcom/cigna/coach/interfaces/IBackupAndRestore$BRError;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V

    .line 287
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "An error occured while sendBackup"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onProgress(Lcom/cigna/coach/acm/ACMTask;JD)V
    .locals 2
    .param p1, "task"    # Lcom/cigna/coach/acm/ACMTask;
    .param p2, "requestId"    # J
    .param p4, "progress"    # D

    .prologue
    .line 267
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$GetCountCompleteCallback;->queryListener:Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;

    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    invoke-interface {v0, v1, p4, p5}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 268
    return-void
.end method
