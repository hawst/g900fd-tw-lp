.class public Lcom/cigna/coach/utils/NotificationHelper;
.super Ljava/lang/Object;
.source "NotificationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/utils/NotificationHelper$1;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final NOTIFICATION_FEATURE_CONTENT_ID:I = 0x3e8

.field private static final NOTIFICATION_UI_TYPE:Ljava/lang/String; = "03"


# instance fields
.field private final coachMessageHelper:Lcom/cigna/coach/utils/CoachMessageHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/cigna/coach/utils/NotificationHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v0, p1}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    iput-object v0, p0, Lcom/cigna/coach/utils/NotificationHelper;->coachMessageHelper:Lcom/cigna/coach/utils/CoachMessageHelper;

    .line 38
    return-void
.end method

.method public static mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;
    .locals 6
    .param p0, "coachMessageData"    # Lcom/cigna/coach/dataobjects/CoachMessageData;

    .prologue
    .line 78
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "mapCoachMessageToNotification: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    const/4 v1, 0x0

    .line 82
    .local v1, "notificationData":Lcom/cigna/coach/dataobjects/NotificationData;
    if-eqz p0, :cond_3

    .line 83
    new-instance v1, Lcom/cigna/coach/dataobjects/NotificationData;

    .end local v1    # "notificationData":Lcom/cigna/coach/dataobjects/NotificationData;
    invoke-direct {v1}, Lcom/cigna/coach/dataobjects/NotificationData;-><init>()V

    .line 84
    .restart local v1    # "notificationData":Lcom/cigna/coach/dataobjects/NotificationData;
    if-eqz v1, :cond_2

    .line 85
    const/16 v3, 0x3e8

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setFeatureContentId(I)V

    .line 86
    const-string v3, "03"

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setUiType(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setCoachMessageId(I)V

    .line 89
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getNotificationDeepLinkInfo()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setDeepLinkInfo(Ljava/util/Map;)V

    .line 90
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setContextFilterType(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V

    .line 91
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 92
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "mapCoachMessageToNotification: coachMessageData.getContextFilterType() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v0

    .line 95
    .local v0, "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageContentId()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setTitleContentId(I)V

    .line 96
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDescriptionContentId()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setMessageLine1ContentId(I)V

    .line 97
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageImageContentId()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/cigna/coach/dataobjects/NotificationData;->setImageContentId(I)V

    .line 98
    if-eqz v0, :cond_2

    .line 99
    const/4 v2, 0x0

    .line 101
    .local v2, "notificationType":Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper$1;->$SwitchMap$com$cigna$coach$apiobjects$CoachMessage$CoachMessageType:[I

    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 121
    const/4 v2, 0x0

    .line 126
    :goto_0
    :pswitch_0
    invoke-virtual {v1, v2}, Lcom/cigna/coach/dataobjects/NotificationData;->setNotificationType(Lcom/cigna/coach/apiobjects/Notification$NotificationType;)V

    .line 128
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 129
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "mapCoachMessageToNotification: coachMessageType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "mapCoachMessageToNotification: notificationType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "mapCoachMessageToNotification: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .end local v0    # "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v2    # "notificationType":Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    :cond_2
    :goto_1
    return-object v1

    .line 106
    .restart local v0    # "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .restart local v2    # "notificationType":Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    :pswitch_1
    sget-object v2, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->TIMELINE_NOTIFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 107
    goto :goto_0

    .line 110
    :pswitch_2
    sget-object v2, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->ANDROID_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 111
    goto :goto_0

    .line 114
    :pswitch_3
    sget-object v2, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->COACH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 115
    goto :goto_0

    .line 118
    :pswitch_4
    sget-object v2, Lcom/cigna/coach/apiobjects/Notification$NotificationType;->SHEALTH_WIDGET_NOTFICATION:Lcom/cigna/coach/apiobjects/Notification$NotificationType;

    .line 119
    goto :goto_0

    .line 137
    .end local v0    # "coachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .end local v2    # "notificationType":Lcom/cigna/coach/apiobjects/Notification$NotificationType;
    :cond_3
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 138
    sget-object v3, Lcom/cigna/coach/utils/NotificationHelper;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "mapCoachMessageToNotification: CoachMessage is null. mapping to null notification. END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public getCheckUserProgressNotifications(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v0}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V

    .line 43
    .local v0, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->TIMELINE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 44
    .local v4, "timelineCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v2, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->OUT_OF_APPLICATION_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 45
    .local v2, "outOfApplicationNotificationCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v1, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 46
    .local v1, "coachWidgetCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    sget-object v3, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->SHEALTH_WIDGET_NOTIFICATION:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    .line 47
    .local v3, "shealthWidgetCoachMessageType":Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    iget-object v5, p0, Lcom/cigna/coach/utils/NotificationHelper;->coachMessageHelper:Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-virtual {v5, p1, v0, v4}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 48
    iget-object v5, p0, Lcom/cigna/coach/utils/NotificationHelper;->coachMessageHelper:Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-virtual {v5, p1, v0, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 49
    iget-object v5, p0, Lcom/cigna/coach/utils/NotificationHelper;->coachMessageHelper:Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-virtual {v5, p1, v0, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 50
    iget-object v5, p0, Lcom/cigna/coach/utils/NotificationHelper;->coachMessageHelper:Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-virtual {v5, p1, v0, v3}, Lcom/cigna/coach/utils/CoachMessageHelper;->setCheckUserProgressMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 51
    invoke-virtual {p0, v0}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachResponseToNotifications(Lcom/cigna/coach/apiobjects/CoachResponse;)Ljava/util/List;

    move-result-object v5

    return-object v5
.end method

.method public getSetMessionDataNotifications(Lcom/cigna/coach/dataobjects/UserMissionData;Ljava/util/List;ZZ)Ljava/util/List;
    .locals 6
    .param p1, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .param p3, "missionCompleted"    # Z
    .param p4, "goalCompleted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;ZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    .local p2, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    new-instance v2, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v2}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V

    .line 57
    .local v2, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/cigna/coach/utils/NotificationHelper;->coachMessageHelper:Lcom/cigna/coach/utils/CoachMessageHelper;

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/utils/CoachMessageHelper;->setSetMissionDataTrackerMessages(Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/apiobjects/CoachResponse;Ljava/util/List;ZZ)V

    .line 59
    invoke-virtual {p0, v2}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachResponseToNotifications(Lcom/cigna/coach/apiobjects/CoachResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public mapCoachResponseToNotifications(Lcom/cigna/coach/apiobjects/CoachResponse;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<*>;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    .local p1, "coachResponse":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<*>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v4, "notifications":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    if-eqz p1, :cond_1

    .line 66
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/CoachResponse;->getCoachMsgList()Ljava/util/List;

    move-result-object v1

    .line 67
    .local v1, "coachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/CoachMessage;

    .line 68
    .local v0, "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .end local v0    # "coachMessage":Lcom/cigna/coach/apiobjects/CoachMessage;
    invoke-static {v0}, Lcom/cigna/coach/utils/NotificationHelper;->mapCoachMessageToNotification(Lcom/cigna/coach/dataobjects/CoachMessageData;)Lcom/cigna/coach/apiobjects/Notification;

    move-result-object v3

    .line 69
    .local v3, "notification":Lcom/cigna/coach/apiobjects/Notification;
    if-eqz v3, :cond_0

    .line 70
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    .end local v1    # "coachMessages":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "notification":Lcom/cigna/coach/apiobjects/Notification;
    :cond_1
    return-object v4
.end method
