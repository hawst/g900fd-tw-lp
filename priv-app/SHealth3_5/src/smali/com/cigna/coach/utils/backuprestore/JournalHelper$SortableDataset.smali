.class public Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;
.super Ljava/lang/Object;
.source "JournalHelper.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SortableDataset"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;",
        ">;"
    }
.end annotation


# instance fields
.field private badgeCount:I

.field private final dataset:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;"
        }
    .end annotation
.end field

.field private final deviceId:Ljava/lang/String;

.field private latestEntry:Lcom/cigna/coach/dataobjects/UserTableJournalData;

.field private missionCount:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "deviceId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p2, "dataset":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;>;"
    const/4 v0, -0x1

    .line 840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 836
    iput v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->badgeCount:I

    .line 837
    iput v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->missionCount:I

    .line 838
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->latestEntry:Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .line 841
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "dataset cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 842
    :cond_0
    iput-object p1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->deviceId:Ljava/lang/String;

    .line 843
    iput-object p2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->dataset:Ljava/util/Map;

    .line 844
    return-void
.end method

.method private tableSize(Ljava/lang/String;)I
    .locals 2
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 847
    iget-object v1, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->dataset:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 848
    .local v0, "table":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 849
    :goto_0
    return v1

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public compareTo(Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;)I
    .locals 4
    .param p1, "other"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;

    .prologue
    .line 891
    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getBadgeCount()I

    move-result v1

    invoke-virtual {p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getBadgeCount()I

    move-result v2

    sub-int v0, v1, v2

    .line 892
    .local v0, "ans":I
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getMissionCount()I

    move-result v1

    invoke-virtual {p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getMissionCount()I

    move-result v2

    sub-int v0, v1, v2

    .line 893
    :cond_0
    if-nez v0, :cond_1

    sget-object v1, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->sortByDate:Ljava/util/Comparator;

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 894
    :cond_1
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 833
    check-cast p1, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->compareTo(Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "otherObject"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 898
    if-ne p0, p1, :cond_1

    .line 905
    :cond_0
    :goto_0
    return v1

    .line 900
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    .line 901
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 904
    check-cast v0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;

    .line 905
    .local v0, "other":Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;
    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getBadgeCount()I

    move-result v3

    invoke-virtual {v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getBadgeCount()I

    move-result v4

    if-ne v3, v4, :cond_4

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getMissionCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getMissionCount()I

    move-result v4

    if-ne v3, v4, :cond_4

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTransactionTimeStamp()Ljava/util/Calendar;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTransactionTimeStamp()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTransactionTimeStamp()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getSeqId()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getSeqId()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getBadgeCount()I
    .locals 3

    .prologue
    .line 861
    iget v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->badgeCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 862
    const-string v0, "USR_BADGE"

    invoke-direct {p0, v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->tableSize(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->badgeCount:I

    .line 863
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 864
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Badge Length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->badgeCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Device Id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    :cond_0
    iget v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->badgeCount:I

    return v0
.end method

.method public getDataset()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 857
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->dataset:Ljava/util/Map;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 853
    iget-object v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getLatestEntry()Lcom/cigna/coach/dataobjects/UserTableJournalData;
    .locals 6

    .prologue
    .line 879
    iget-object v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->latestEntry:Lcom/cigna/coach/dataobjects/UserTableJournalData;

    if-nez v3, :cond_1

    .line 880
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->dataset:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 881
    .local v1, "latestEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    iget-object v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->dataset:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 882
    .local v2, "table":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->sortByDate:Ljava/util/Comparator;

    invoke-static {v2, v3}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 884
    .end local v2    # "table":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    :cond_0
    sget-object v3, Lcom/cigna/coach/utils/backuprestore/UserJournalDataAggregator;->sortByDate:Ljava/util/Comparator;

    invoke-static {v1, v3}, Ljava/util/Collections;->max(Ljava/util/Collection;Ljava/util/Comparator;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    iput-object v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->latestEntry:Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .line 885
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Latest entry time : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->latestEntry:Lcom/cigna/coach/dataobjects/UserTableJournalData;

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getTransactionTimeStamp()Ljava/util/Calendar;

    move-result-object v5

    invoke-static {v5}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Device Id : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->deviceId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "latestEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    :cond_1
    iget-object v3, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->latestEntry:Lcom/cigna/coach/dataobjects/UserTableJournalData;

    return-object v3
.end method

.method public getMissionCount()I
    .locals 3

    .prologue
    .line 871
    iget v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->missionCount:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 872
    const-string v0, "USR_MSSN_DATA"

    invoke-direct {p0, v0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->tableSize(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->missionCount:I

    .line 873
    # getter for: Lcom/cigna/coach/utils/backuprestore/JournalHelper;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mission Length : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->missionCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Device Id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    :cond_0
    iget v0, p0, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->missionCount:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 912
    const/4 v0, 0x0

    .line 913
    .local v0, "result":I
    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getBadgeCount()I

    move-result v1

    add-int/lit8 v0, v1, 0x0

    .line 914
    mul-int/lit8 v1, v0, 0x1f

    invoke-virtual {p0}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$SortableDataset;->getMissionCount()I

    move-result v2

    add-int v0, v1, v2

    .line 916
    return v0
.end method
