.class public final enum Lcom/cigna/coach/utils/ContextData$ContextDataType;
.super Ljava/lang/Enum;
.source "ContextData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/ContextData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContextDataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/utils/ContextData$ContextDataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/utils/ContextData$ContextDataType;

.field public static final enum CATEGORY_LIST:Lcom/cigna/coach/utils/ContextData$ContextDataType;

.field public static final enum CURRENT_SCORE_MESSAGE:Lcom/cigna/coach/utils/ContextData$ContextDataType;

.field public static final enum GOAL_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

.field public static final enum MISSION_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

.field public static final enum TRACKER_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

.field public static final enum VARIANCE_FROM_LAST_ASSESSMENT:Lcom/cigna/coach/utils/ContextData$ContextDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    const-string v1, "GOAL_NAME"

    invoke-direct {v0, v1, v3}, Lcom/cigna/coach/utils/ContextData$ContextDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->GOAL_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    new-instance v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    const-string v1, "MISSION_NAME"

    invoke-direct {v0, v1, v4}, Lcom/cigna/coach/utils/ContextData$ContextDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->MISSION_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    new-instance v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    const-string v1, "VARIANCE_FROM_LAST_ASSESSMENT"

    invoke-direct {v0, v1, v5}, Lcom/cigna/coach/utils/ContextData$ContextDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->VARIANCE_FROM_LAST_ASSESSMENT:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    new-instance v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    const-string v1, "CURRENT_SCORE_MESSAGE"

    invoke-direct {v0, v1, v6}, Lcom/cigna/coach/utils/ContextData$ContextDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->CURRENT_SCORE_MESSAGE:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    new-instance v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    const-string v1, "TRACKER_NAME"

    invoke-direct {v0, v1, v7}, Lcom/cigna/coach/utils/ContextData$ContextDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->TRACKER_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    new-instance v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    const-string v1, "CATEGORY_LIST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/ContextData$ContextDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->CATEGORY_LIST:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    .line 26
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/cigna/coach/utils/ContextData$ContextDataType;

    sget-object v1, Lcom/cigna/coach/utils/ContextData$ContextDataType;->GOAL_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cigna/coach/utils/ContextData$ContextDataType;->MISSION_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/utils/ContextData$ContextDataType;->VARIANCE_FROM_LAST_ASSESSMENT:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cigna/coach/utils/ContextData$ContextDataType;->CURRENT_SCORE_MESSAGE:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/cigna/coach/utils/ContextData$ContextDataType;->TRACKER_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/cigna/coach/utils/ContextData$ContextDataType;->CATEGORY_LIST:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->$VALUES:[Lcom/cigna/coach/utils/ContextData$ContextDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/ContextData$ContextDataType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/utils/ContextData$ContextDataType;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/cigna/coach/utils/ContextData$ContextDataType;->$VALUES:[Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-virtual {v0}, [Lcom/cigna/coach/utils/ContextData$ContextDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/utils/ContextData$ContextDataType;

    return-object v0
.end method
