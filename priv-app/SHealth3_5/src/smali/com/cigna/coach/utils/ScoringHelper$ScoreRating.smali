.class public final enum Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
.super Ljava/lang/Enum;
.source "ScoringHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/ScoringHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ScoreRating"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

.field public static final enum HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

.field public static final enum LOW:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

.field public static final enum MEDIUM:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 743
    new-instance v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v2}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    new-instance v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->MEDIUM:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    new-instance v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v4}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->LOW:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    .line 742
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    sget-object v1, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->MEDIUM:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->LOW:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    aput-object v1, v0, v4

    sput-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->$VALUES:[Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 742
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    .locals 1
    .param p0, "score"    # I

    .prologue
    .line 746
    const/16 v0, 0x47

    if-lt p0, v0, :cond_0

    .line 747
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    .line 754
    :goto_0
    return-object v0

    .line 750
    :cond_0
    const/16 v0, 0x1f

    if-lt p0, v0, :cond_1

    .line 751
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->MEDIUM:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    goto :goto_0

    .line 754
    :cond_1
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->LOW:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 742
    const-class v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    .locals 1

    .prologue
    .line 742
    sget-object v0, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->$VALUES:[Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    invoke-virtual {v0}, [Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    return-object v0
.end method
