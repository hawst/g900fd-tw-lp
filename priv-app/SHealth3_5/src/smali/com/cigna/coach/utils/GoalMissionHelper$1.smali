.class Lcom/cigna/coach/utils/GoalMissionHelper$1;
.super Ljava/lang/Object;
.source "GoalMissionHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/coach/utils/GoalMissionHelper;->mergeManualAndTrackerActivities(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/apiobjects/MissionDetails;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/coach/utils/GoalMissionHelper;


# direct methods
.method constructor <init>(Lcom/cigna/coach/utils/GoalMissionHelper;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/cigna/coach/utils/GoalMissionHelper$1;->this$0:Lcom/cigna/coach/utils/GoalMissionHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/apiobjects/MissionDetails;)I
    .locals 4
    .param p1, "lhs"    # Lcom/cigna/coach/apiobjects/MissionDetails;
    .param p2, "rhs"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/cigna/coach/utils/GoalMissionHelper$1;->this$0:Lcom/cigna/coach/utils/GoalMissionHelper;

    # invokes: Lcom/cigna/coach/utils/GoalMissionHelper;->activityDate(Lcom/cigna/coach/apiobjects/MissionDetails;)J
    invoke-static {v0, p1}, Lcom/cigna/coach/utils/GoalMissionHelper;->access$000(Lcom/cigna/coach/utils/GoalMissionHelper;Lcom/cigna/coach/apiobjects/MissionDetails;)J

    move-result-wide v0

    iget-object v2, p0, Lcom/cigna/coach/utils/GoalMissionHelper$1;->this$0:Lcom/cigna/coach/utils/GoalMissionHelper;

    # invokes: Lcom/cigna/coach/utils/GoalMissionHelper;->activityDate(Lcom/cigna/coach/apiobjects/MissionDetails;)J
    invoke-static {v2, p2}, Lcom/cigna/coach/utils/GoalMissionHelper;->access$000(Lcom/cigna/coach/utils/GoalMissionHelper;Lcom/cigna/coach/apiobjects/MissionDetails;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 124
    check-cast p1, Lcom/cigna/coach/apiobjects/MissionDetails;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/apiobjects/MissionDetails;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/utils/GoalMissionHelper$1;->compare(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/apiobjects/MissionDetails;)I

    move-result v0

    return v0
.end method
