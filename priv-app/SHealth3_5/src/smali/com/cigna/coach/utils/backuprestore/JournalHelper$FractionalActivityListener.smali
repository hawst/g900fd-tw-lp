.class public interface abstract Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
.super Ljava/lang/Object;
.source "JournalHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/backuprestore/JournalHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FractionalActivityListener"
.end annotation


# virtual methods
.method public abstract onFinished(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;ZLcom/cigna/coach/interfaces/IBackupAndRestore$BRError;Ljava/lang/String;)V
.end method

.method public abstract onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
.end method

.method public abstract onStarted(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;)V
.end method
