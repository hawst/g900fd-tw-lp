.class Lcom/cigna/coach/utils/IndempotentExecutor$1;
.super Ljava/lang/Object;
.source "IndempotentExecutor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/utils/IndempotentExecutor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/coach/utils/IndempotentExecutor;


# direct methods
.method constructor <init>(Lcom/cigna/coach/utils/IndempotentExecutor;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/cigna/coach/utils/IndempotentExecutor$1;->this$0:Lcom/cigna/coach/utils/IndempotentExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 142
    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Timer is Running : START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_0
    iget-object v2, p0, Lcom/cigna/coach/utils/IndempotentExecutor$1;->this$0:Lcom/cigna/coach/utils/IndempotentExecutor;

    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;
    invoke-static {v2}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$100(Lcom/cigna/coach/utils/IndempotentExecutor;)Ljava/util/Map;

    move-result-object v3

    monitor-enter v3

    .line 146
    :try_start_0
    iget-object v2, p0, Lcom/cigna/coach/utils/IndempotentExecutor$1;->this$0:Lcom/cigna/coach/utils/IndempotentExecutor;

    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;
    invoke-static {v2}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$100(Lcom/cigna/coach/utils/IndempotentExecutor;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 147
    .local v1, "task":Ljava/lang/Runnable;
    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Running task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_1
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 154
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "task":Ljava/lang/Runnable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 152
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/cigna/coach/utils/IndempotentExecutor$1;->this$0:Lcom/cigna/coach/utils/IndempotentExecutor;

    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->taskSet:Ljava/util/Map;
    invoke-static {v2}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$100(Lcom/cigna/coach/utils/IndempotentExecutor;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 153
    iget-object v2, p0, Lcom/cigna/coach/utils/IndempotentExecutor$1;->this$0:Lcom/cigna/coach/utils/IndempotentExecutor;

    const/4 v4, 0x0

    # setter for: Lcom/cigna/coach/utils/IndempotentExecutor;->isTimerScheduled:Z
    invoke-static {v2, v4}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$202(Lcom/cigna/coach/utils/IndempotentExecutor;Z)Z

    .line 154
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$000()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 156
    # getter for: Lcom/cigna/coach/utils/IndempotentExecutor;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/IndempotentExecutor;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Timer is Running : END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :cond_3
    return-void
.end method
