.class public Lcom/cigna/coach/GoalsMissions;
.super Ljava/lang/Object;
.source "GoalsMissions.java"

# interfaces
.implements Lcom/cigna/coach/interfaces/IGoalsMissions;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/GoalsMissions$1;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    const-class v0, Lcom/cigna/coach/GoalsMissions;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    .line 97
    return-void
.end method


# virtual methods
.method public getMissionNotificationFailureMessage()Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 148
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getMissionNotificationFailureMessage: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 153
    .local v1, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v3, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V

    .line 155
    .local v3, "returnValue":Lcom/cigna/coach/dataobjects/CoachMessageData;
    const/16 v4, 0xf67

    :try_start_0
    invoke-virtual {v3, v4}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 156
    sget-object v4, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    invoke-virtual {v3, v4}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 157
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 158
    .local v0, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v0, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 164
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 165
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getMissionNotificationFailureMessage: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_1
    return-object v3

    .line 159
    .end local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    :catch_0
    move-exception v2

    .line 160
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error while getMissionNotificationFailureMessage"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error while getMissionNotificationFailureMessage."

    invoke-direct {v4, v5, v2}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 164
    sget-object v5, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 165
    sget-object v5, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getMissionNotificationFailureMessage: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public getUserAvailableGoalMissions(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionInfo;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 196
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserAvailableGoalMissions: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_0
    new-instance v3, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v8, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v9, 0x1

    invoke-direct {v3, v8, v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 200
    .local v3, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v5, Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {v5, v3}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 201
    .local v5, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    const/4 v6, 0x0

    .line 205
    .local v6, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    :try_start_0
    invoke-virtual {v5, p1, p2}, Lcom/cigna/coach/db/MissionDBManager;->getAvailableUserMissionsForGoal(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    .line 206
    .local v2, "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    new-instance v7, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v7}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .local v7, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    :try_start_1
    invoke-virtual {v7, v2}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 210
    new-instance v0, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v0, v3}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 211
    .local v0, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    invoke-virtual {v0, p1, v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->setGetSuggestedMissionsMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 213
    new-instance v1, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v1, v3}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 214
    .local v1, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v1, v7}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 216
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 221
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 222
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 223
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserAvailableGoalMissions: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :cond_1
    return-object v7

    .line 217
    .end local v0    # "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    .end local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v2    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    :catch_0
    move-exception v4

    .line 218
    .local v4, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error while getUserAvailableGoalMissions"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    new-instance v8, Lcom/cigna/coach/exceptions/CoachException;

    const-string v9, "Error while getUserAvailableGoalMissions."

    invoke-direct {v8, v9, v4}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 221
    .end local v4    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    :goto_1
    invoke-virtual {v3}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 222
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 223
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getUserAvailableGoalMissions: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v8

    .line 221
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v2    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .restart local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    goto :goto_1

    .line 217
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    :catch_1
    move-exception v4

    move-object v6, v7

    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;>;"
    goto :goto_0
.end method

.method public getUserAvailableGoals(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfo;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 181
    sget-object v0, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/GoalsMissions;->getUserAvailableGoals(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v0

    return-object v0
.end method

.method public getUserAvailableGoals(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryTypeRequest"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfo;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 726
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 727
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getUserAvailableGoals: START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    :cond_0
    new-instance v4, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v9, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v10, 0x1

    invoke-direct {v4, v9, v10}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 731
    .local v4, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v6, Lcom/cigna/coach/db/GoalDBManager;

    invoke-direct {v6, v4}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 733
    .local v6, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    const/4 v7, 0x0

    .line 736
    .local v7, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    :try_start_0
    sget-object v9, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    invoke-virtual {p2, v9}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 737
    invoke-virtual {v6, p1}, Lcom/cigna/coach/db/GoalDBManager;->getAvailableUserGoals(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 743
    .local v3, "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    :goto_0
    new-instance v8, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v8}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 744
    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .local v8, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    :try_start_1
    invoke-virtual {v8, v3}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 747
    new-instance v1, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v1, v4}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 748
    .local v1, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    invoke-virtual {v1, p1, v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->setGetSuggestedGoalsMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 750
    new-instance v2, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v2, v4}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 751
    .local v2, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v2, v8}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 753
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 759
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 760
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 761
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getUserAvailableGoals: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :cond_1
    return-object v8

    .line 739
    .end local v1    # "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    .end local v2    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v3    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .end local v8    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    :cond_2
    :try_start_2
    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .line 740
    .local v0, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual {v6, p1, v0}, Lcom/cigna/coach/db/GoalDBManager;->getAvailableUserGoalsForCategory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/util/List;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .restart local v3    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    goto :goto_0

    .line 755
    .end local v0    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v3    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    :catch_0
    move-exception v5

    .line 756
    .local v5, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Error while getUserAvailableGoals"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    const-string v10, "Error while getUserAvailableGoals."

    invoke-direct {v9, v10, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 759
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    :goto_2
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 760
    sget-object v10, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 761
    sget-object v10, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getUserAvailableGoals: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v9

    .line 759
    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v3    # "data":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    .restart local v8    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    :catchall_1
    move-exception v9

    move-object v7, v8

    .end local v8    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    goto :goto_2

    .line 755
    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v8    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    :catch_1
    move-exception v5

    move-object v7, v8

    .end local v8    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    .restart local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;>;"
    goto :goto_1
.end method

.method public getUserCompletedGoalMissions(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 241
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 242
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserCompletedGoalMissions: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_0
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v7, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-direct {v1, v7, v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 246
    .local v1, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v3, Lcom/cigna/coach/db/GoalDBManager;

    invoke-direct {v3, v1}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 247
    .local v3, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    const/4 v5, 0x0

    .line 250
    .local v5, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    :try_start_0
    invoke-virtual {v3, p1}, Lcom/cigna/coach/db/GoalDBManager;->getUserGoalsWithCompletedMissions(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;

    move-result-object v4

    .line 251
    .local v4, "result":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    new-instance v6, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v6}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .local v6, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    :try_start_1
    invoke-virtual {v6, v4}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 258
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v0, v1}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 259
    .local v0, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 261
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 266
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 267
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserCompletedGoalMissions: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :cond_1
    return-object v6

    .line 262
    .end local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v4    # "result":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    :catch_0
    move-exception v2

    .line 263
    .local v2, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error while getUserCompletedGoalMissions"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v7, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Error while getUserCompletedGoalMissions."

    invoke-direct {v7, v8, v2}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 266
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    :goto_1
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 267
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserCompletedGoalMissions: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v7

    .line 266
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .restart local v4    # "result":Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    goto :goto_1

    .line 262
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    :catch_1
    move-exception v2

    move-object v5, v6

    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;>;"
    goto :goto_0
.end method

.method public getUserMissionDetails(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 329
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 330
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserMissionDetails: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    :cond_0
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v7, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-direct {v2, v7, v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 334
    .local v2, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v4, Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {v4, v2}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 336
    .local v4, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    const/4 v5, 0x0

    .line 338
    .local v5, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :try_start_0
    invoke-virtual {v4, p2, p1}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v1

    .line 339
    .local v1, "data":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    new-instance v6, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v6}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .local v6, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :try_start_1
    invoke-virtual {v6, v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 342
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 343
    .local v0, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 345
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 350
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 351
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 352
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserMissionDetails: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    :cond_1
    return-object v6

    .line 346
    .end local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v1    # "data":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :catch_0
    move-exception v3

    .line 347
    .local v3, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error while getUserMissionDetails"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    new-instance v7, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Error while getUserMissionDetails."

    invoke-direct {v7, v8, v3}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 350
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    :goto_1
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 351
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 352
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserMissionDetails: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v7

    .line 350
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v1    # "data":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    goto :goto_1

    .line 346
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :catch_1
    move-exception v3

    move-object v5, v6

    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    goto :goto_0
.end method

.method public getUserMissionHistoryDetails(Ljava/lang/String;I)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionSeqId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 370
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 371
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserMissionHistoryDetails: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_0
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v7, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-direct {v2, v7, v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 375
    .local v2, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v4, Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {v4, v2}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 377
    .local v4, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    const/4 v5, 0x0

    .line 379
    .local v5, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :try_start_0
    invoke-virtual {v4, p2}, Lcom/cigna/coach/db/MissionDBManager;->getUserMissionDetails(I)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v1

    .line 380
    .local v1, "data":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    new-instance v6, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v6}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .local v6, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :try_start_1
    invoke-virtual {v6, v1}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 383
    new-instance v0, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 384
    .local v0, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v0, v6}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 386
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 391
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 392
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 393
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserMissionHistoryDetails: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_1
    return-object v6

    .line 387
    .end local v0    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v1    # "data":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :catch_0
    move-exception v3

    .line 388
    .local v3, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v7, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error while getUserMissionDetails"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    new-instance v7, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Error while getUserMissionDetails."

    invoke-direct {v7, v8, v3}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 391
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    :goto_1
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 392
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 393
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserMissionHistoryDetails: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v7

    .line 391
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v1    # "data":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :catchall_1
    move-exception v7

    move-object v5, v6

    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    goto :goto_1

    .line 387
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :catch_1
    move-exception v3

    move-object v5, v6

    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    goto :goto_0
.end method

.method public getUserSelectedGoalMissions(Ljava/lang/String;Z)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "enableAnimation"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 286
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 287
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserSelectedGoalMissions: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_0
    new-instance v2, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v8, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v9, 0x1

    invoke-direct {v2, v8, v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 291
    .local v2, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v4, Lcom/cigna/coach/db/GoalDBManager;

    invoke-direct {v4, v2}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 292
    .local v4, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    const/4 v6, 0x0

    .line 295
    .local v6, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    :try_start_0
    invoke-virtual {v4, p1, p2}, Lcom/cigna/coach/db/GoalDBManager;->getAllActiveUserGoalsAndMissions(Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v5

    .line 296
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    new-instance v7, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v7}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .local v7, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    :try_start_1
    invoke-virtual {v7, v5}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 300
    new-instance v0, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v0, v2}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 301
    .local v0, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    invoke-virtual {v0, p1, v7}, Lcom/cigna/coach/utils/CoachMessageHelper;->setGetGoalsMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 303
    new-instance v1, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v1, v2}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 304
    .local v1, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v1, v7}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 306
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 311
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 312
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserSelectedGoalMissions: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_1
    return-object v7

    .line 307
    .end local v0    # "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    .end local v1    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v5    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    :catch_0
    move-exception v3

    .line 308
    .local v3, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error while getUserSelectedGoalMissions"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    new-instance v8, Lcom/cigna/coach/exceptions/CoachException;

    const-string v9, "Error while getUserSelectedGoalMissions."

    invoke-direct {v8, v9, v3}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    :goto_1
    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 312
    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    sget-object v9, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getUserSelectedGoalMissions: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v8

    .line 311
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v5    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    :catchall_1
    move-exception v8

    move-object v6, v7

    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    goto :goto_1

    .line 307
    .end local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    :catch_1
    move-exception v3

    move-object v6, v7

    .end local v7    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    .restart local v6    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;>;"
    goto :goto_0
.end method

.method public setMissionFailureReasonCode(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)Lcom/cigna/coach/apiobjects/CoachMessage;
    .locals 25
    .param p1, "missionSequenceId"    # I
    .param p2, "missionFailedReason"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 410
    sget-object v22, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 411
    sget-object v22, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v23, "setMissionFailureReasonCode: START"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_0
    if-nez p2, :cond_1

    .line 416
    new-instance v22, Ljava/lang/IllegalArgumentException;

    const-string v23, "GoalsMissions.setMissionFailureReasonCode - missionFailedReason cannot be null"

    invoke-direct/range {v22 .. v23}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 419
    :cond_1
    new-instance v4, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v4, v0, v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 420
    .local v4, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v18, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V

    .line 422
    .local v18, "returnValue":Lcom/cigna/coach/dataobjects/CoachMessageData;
    :try_start_0
    new-instance v15, Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {v15, v4}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 423
    .local v15, "msnDBManager":Lcom/cigna/coach/db/MissionDBManager;
    new-instance v2, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v2, v4}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 424
    .local v2, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    new-instance v3, Lcom/cigna/coach/utils/ContextData;

    invoke-direct {v3}, Lcom/cigna/coach/utils/ContextData;-><init>()V

    .line 427
    .local v3, "contextData":Lcom/cigna/coach/utils/ContextData;
    move/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v15, v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMissionWithMissionFailedReason(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)I

    .line 429
    const/4 v11, -0x1

    .line 430
    .local v11, "messageContentId":I
    sget-object v22, Lcom/cigna/coach/GoalsMissions$1;->$SwitchMap$com$cigna$coach$interfaces$IGoalsMissions$MissionFailedReason:[I

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_0

    .line 482
    :goto_0
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 483
    sget-object v22, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->COACH_MESSAGE:Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 484
    move-object/from16 v0, v18

    invoke-virtual {v2, v0, v3}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;)V

    .line 486
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    if-eqz v4, :cond_2

    .line 492
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 493
    :cond_2
    sget-object v22, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 494
    sget-object v22, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v23, "setMissionFailureReasonCode: END"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    :cond_3
    return-object v18

    .line 432
    :pswitch_0
    :try_start_1
    move/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/cigna/coach/db/MissionDBManager;->getUserMissionDetails(I)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v17

    .line 433
    .local v17, "oldMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-nez v17, :cond_6

    new-instance v22, Lcom/cigna/coach/exceptions/CoachException;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "No such mission sequence Id: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v22
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 487
    .end local v2    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v3    # "contextData":Lcom/cigna/coach/utils/ContextData;
    .end local v11    # "messageContentId":I
    .end local v15    # "msnDBManager":Lcom/cigna/coach/db/MissionDBManager;
    .end local v17    # "oldMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v5

    .line 488
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    sget-object v22, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v23, "Error while setMissionFailureReasonCode"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    new-instance v22, Lcom/cigna/coach/exceptions/CoachException;

    const-string v23, "Error while setMissionFailureReasonCode."

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v22
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 491
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v22

    if-eqz v4, :cond_4

    .line 492
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 493
    :cond_4
    sget-object v23, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 494
    sget-object v23, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v24, "setMissionFailureReasonCode: END"

    invoke-static/range {v23 .. v24}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v22

    .line 434
    .restart local v2    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .restart local v3    # "contextData":Lcom/cigna/coach/utils/ContextData;
    .restart local v11    # "messageContentId":I
    .restart local v15    # "msnDBManager":Lcom/cigna/coach/db/MissionDBManager;
    .restart local v17    # "oldMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_6
    :try_start_3
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getUserId()Ljava/lang/String;

    move-result-object v21

    .line 435
    .local v21, "userId":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v8

    .line 436
    .local v8, "goalId":I
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v13

    .line 438
    .local v13, "missionId":I
    new-instance v7, Lcom/cigna/coach/db/GoalDBManager;

    invoke-direct {v7, v4}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 439
    .local v7, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    new-instance v12, Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {v12, v4}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 442
    .local v12, "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    move-object/from16 v0, v21

    invoke-virtual {v7, v0, v8}, Lcom/cigna/coach/db/GoalDBManager;->getIsUserGoalActive(Ljava/lang/String;I)Z

    move-result v9

    .line 443
    .local v9, "goalIsActive":Z
    move-object/from16 v0, v21

    invoke-virtual {v12, v0, v13}, Lcom/cigna/coach/db/MissionDBManager;->getIsUserMissionActiveForAnyActiveGoal(Ljava/lang/String;I)Z

    move-result v14

    .line 444
    .local v14, "missionIsActiveWithAnyGoal":Z
    if-eqz v9, :cond_7

    if-nez v14, :cond_7

    .line 445
    const/16 v11, 0xf00

    .line 448
    invoke-virtual {v7, v8}, Lcom/cigna/coach/db/GoalDBManager;->getGoalNameContentIdForGoal(I)I

    move-result v6

    .line 449
    .local v6, "goalContentId":I
    sget-object v22, Lcom/cigna/coach/utils/ContextData$ContextDataType;->GOAL_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0, v6}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V

    .line 452
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v20

    .line 453
    .local v20, "today":Ljava/util/Calendar;
    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDayAfter(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v19

    .line 455
    .local v19, "startOfTomorrow":Ljava/util/Calendar;
    new-instance v16, Lcom/cigna/coach/apiobjects/MissionOptions;

    invoke-direct/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/MissionOptions;-><init>()V

    .line 456
    .local v16, "newMissionOptions":Lcom/cigna/coach/apiobjects/MissionOptions;
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/MissionOptions;->setMissionStartDate(Ljava/util/Calendar;)V

    .line 457
    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/MissionOptions;->setMissionStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    .line 458
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v22

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/MissionOptions;->setSelectedFrequency(I)V

    .line 461
    new-instance v10, Lcom/cigna/coach/utils/GoalMissionHelper;

    invoke-direct {v10, v4}, Lcom/cigna/coach/utils/GoalMissionHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 462
    .local v10, "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v10, v0, v8, v13, v1}, Lcom/cigna/coach/utils/GoalMissionHelper;->setUserMission(Ljava/lang/String;IILcom/cigna/coach/apiobjects/MissionOptions;)Lcom/cigna/coach/apiobjects/CoachResponse;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 465
    .end local v6    # "goalContentId":I
    .end local v10    # "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    .end local v16    # "newMissionOptions":Lcom/cigna/coach/apiobjects/MissionOptions;
    .end local v19    # "startOfTomorrow":Ljava/util/Calendar;
    .end local v20    # "today":Ljava/util/Calendar;
    :cond_7
    const/16 v11, 0xf03

    .line 467
    goto/16 :goto_0

    .line 469
    .end local v7    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v8    # "goalId":I
    .end local v9    # "goalIsActive":Z
    .end local v12    # "missionDBManager":Lcom/cigna/coach/db/MissionDBManager;
    .end local v13    # "missionId":I
    .end local v14    # "missionIsActiveWithAnyGoal":Z
    .end local v17    # "oldMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    .end local v21    # "userId":Ljava/lang/String;
    :pswitch_1
    const/16 v11, 0xf01

    .line 470
    goto/16 :goto_0

    .line 472
    :pswitch_2
    const/16 v11, 0xf02

    .line 473
    goto/16 :goto_0

    .line 475
    :pswitch_3
    const/16 v11, 0xf03

    .line 476
    goto/16 :goto_0

    .line 478
    :pswitch_4
    const/16 v11, 0xf03

    goto/16 :goto_0

    .line 430
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setUserGoalStatus(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 23
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "goalStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 513
    sget-object v20, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 514
    sget-object v20, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v21, "setUserGoalStatus: START"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    :cond_0
    new-instance v7, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v7, v0, v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 518
    .local v7, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v10, Lcom/cigna/coach/db/GoalDBManager;

    invoke-direct {v10, v7}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 519
    .local v10, "goalDbManager":Lcom/cigna/coach/db/GoalDBManager;
    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v4

    .line 520
    .local v4, "broadcastManager":Lcom/cigna/mobile/coach/notification/BroadcastManager;
    const/4 v15, 0x0

    .line 524
    .local v15, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v10, v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalDetails(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v9

    .line 525
    .local v9, "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    sget-object v20, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 526
    new-instance v14, Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {v14, v7}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 527
    .local v14, "msnDBManager":Lcom/cigna/coach/db/MissionDBManager;
    if-eqz v9, :cond_4

    .line 529
    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalMissionDetailInfoList()Ljava/util/List;

    move-result-object v13

    .line 530
    .local v13, "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    if-eqz v13, :cond_3

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v20

    if-lez v20, :cond_3

    .line 531
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;

    .line 532
    .local v12, "mission":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    move-object v0, v12

    check-cast v0, Lcom/cigna/coach/dataobjects/UserMissionData;

    move-object/from16 v19, v0

    .line 533
    .local v19, "usrMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v20

    sget-object v21, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual/range {v20 .. v21}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 534
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v20

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    move-object/from16 v3, v22

    invoke-virtual {v14, v0, v1, v2, v3}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMissionStatus(Ljava/lang/String;IILcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 586
    .end local v9    # "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "mission":Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
    .end local v13    # "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v14    # "msnDBManager":Lcom/cigna/coach/db/MissionDBManager;
    .end local v19    # "usrMission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v8

    .line 587
    .local v8, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_1
    sget-object v20, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Error while setUserGoalStatus"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    new-instance v20, Lcom/cigna/coach/exceptions/CoachException;

    const-string v21, "Error while setUserGoalStatus"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v8}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v20
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 590
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v20

    :goto_2
    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 591
    sget-object v21, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 592
    sget-object v21, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v22, "setUserGoalStatus: END"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v20

    .line 544
    .restart local v9    # "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v13    # "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v14    # "msnDBManager":Lcom/cigna/coach/db/MissionDBManager;
    :cond_3
    :try_start_2
    new-instance v17, Lcom/cigna/coach/dataobjects/UserGoalData;

    invoke-direct/range {v17 .. v17}, Lcom/cigna/coach/dataobjects/UserGoalData;-><init>()V

    .line 545
    .local v17, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalId(I)V

    .line 546
    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 547
    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionGroupScore()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionGroupScore(I)V

    .line 548
    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCategoryType()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 549
    sget-object v20, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 550
    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCategoryScore()I

    move-result v20

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryScore(I)V

    .line 552
    new-instance v18, Ljava/util/ArrayList;

    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 553
    .local v18, "userGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserGoalData;>;"
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->createUserGoals(Ljava/lang/String;Ljava/util/List;)V

    .line 558
    .end local v13    # "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v14    # "msnDBManager":Lcom/cigna/coach/db/MissionDBManager;
    .end local v17    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v18    # "userGoals":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserGoalData;>;"
    :cond_4
    sget-object v20, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_5

    const/16 v20, 0x30

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 559
    new-instance v20, Lcom/cigna/coach/utils/WeightChangeListener;

    move-object/from16 v0, v20

    invoke-direct {v0, v7}, Lcom/cigna/coach/utils/WeightChangeListener;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->updateWeightGoalAndScore(Ljava/lang/String;Z)V

    .line 562
    :cond_5
    move-object/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v10, v0, v1, v2}, Lcom/cigna/coach/db/GoalDBManager;->updateUserGoalStatus(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)I

    .line 563
    new-instance v16, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 565
    .end local v15    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    .local v16, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    :try_start_3
    new-instance v5, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v5, v7}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 568
    .local v5, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    sget-object v20, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 570
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v5, v0, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->setGetGoalsMessagesWhenDeletingGoal(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 572
    sget-object v20, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 573
    sget-object v20, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Sending goal cancelled broadcast"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    :cond_6
    invoke-virtual {v4, v9}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendGoalCancelledBroadcast(Lcom/cigna/coach/dataobjects/UserGoalData;)V

    .line 577
    :cond_7
    sget-object v20, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_8

    .line 579
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v5, v0, v1}, Lcom/cigna/coach/utils/CoachMessageHelper;->setGetGoalsMessagesWhenAddingGoal(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;)V

    .line 582
    :cond_8
    new-instance v6, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v6, v7}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 583
    .local v6, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 585
    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 590
    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 591
    sget-object v20, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 592
    sget-object v20, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v21, "setUserGoalStatus: END"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_9
    return-object v16

    .line 590
    .end local v5    # "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    .end local v6    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    :catchall_1
    move-exception v20

    move-object/from16 v15, v16

    .end local v16    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    .restart local v15    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    goto/16 :goto_2

    .line 586
    .end local v15    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    .restart local v16    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    :catch_1
    move-exception v8

    move-object/from16 v15, v16

    .end local v16    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    .restart local v15    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    goto/16 :goto_1
.end method

.method public setUserMission(Ljava/lang/String;IILcom/cigna/coach/apiobjects/MissionOptions;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "missionId"    # I
    .param p4, "missionOptions"    # Lcom/cigna/coach/apiobjects/MissionOptions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Lcom/cigna/coach/apiobjects/MissionOptions;",
            ")",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 613
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 614
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "setUserMission: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {v0, v4, v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 618
    .local v0, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v2, Lcom/cigna/coach/utils/GoalMissionHelper;

    invoke-direct {v2, v0}, Lcom/cigna/coach/utils/GoalMissionHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 622
    .local v2, "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    :try_start_0
    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/cigna/coach/utils/GoalMissionHelper;->setUserMission(Ljava/lang/String;IILcom/cigna/coach/apiobjects/MissionOptions;)Lcom/cigna/coach/apiobjects/CoachResponse;

    move-result-object v3

    .line 624
    .local v3, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 631
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 632
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "setUserMission: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    :cond_1
    return-object v3

    .line 625
    .end local v3    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Ljava/lang/Void;>;"
    :catch_0
    move-exception v1

    .line 626
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error while setUserMission"

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 627
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v5, "Error while setUserMission."

    invoke-direct {v4, v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 629
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v4
.end method

.method public setUserMissionDetail(Ljava/lang/String;ILjava/util/List;)Lcom/cigna/coach/apiobjects/CoachResponse;
    .locals 24
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)",
            "Lcom/cigna/coach/apiobjects/CoachResponse",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionStatus;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 652
    .local p3, "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 653
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "setUserMissionDetail: START"

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    :cond_0
    new-instance v13, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    const/4 v8, 0x1

    invoke-direct {v13, v4, v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 657
    .local v13, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    new-instance v15, Lcom/cigna/coach/utils/GoalMissionHelper;

    invoke-direct {v15, v13}, Lcom/cigna/coach/utils/GoalMissionHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 658
    .local v15, "goalMissionHelper":Lcom/cigna/coach/utils/GoalMissionHelper;
    new-instance v17, Lcom/cigna/coach/db/MissionDBManager;

    move-object/from16 v0, v17

    invoke-direct {v0, v13}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 659
    .local v17, "missionDbManager":Lcom/cigna/coach/db/MissionDBManager;
    new-instance v21, Lcom/cigna/coach/utils/TrackerHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cigna/coach/GoalsMissions;->context:Landroid/content/Context;

    move-object/from16 v0, v21

    invoke-direct {v0, v13, v4}, Lcom/cigna/coach/utils/TrackerHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;Landroid/content/Context;)V

    .line 660
    .local v21, "trackerHelper":Lcom/cigna/coach/utils/TrackerHelper;
    const/16 v18, 0x0

    .line 663
    .local v18, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    :try_start_0
    move-object/from16 v0, v17

    move/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/MissionDBManager;->getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v22

    .line 664
    .local v22, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-virtual/range {v21 .. v22}, Lcom/cigna/coach/utils/TrackerHelper;->runTrackerRulesForMission(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v20

    .line 665
    .local v20, "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v15, v0, v1}, Lcom/cigna/coach/utils/GoalMissionHelper;->mergeManualAndTrackerActivities(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v16

    .line 667
    .local v16, "mergedMissionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    move-object/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, v16

    invoke-virtual {v15, v0, v1, v2}, Lcom/cigna/coach/utils/GoalMissionHelper;->processMissionDetailsUpdate(Ljava/lang/String;ILjava/util/List;)Lcom/cigna/coach/dataobjects/GoalMissionStatusData;

    move-result-object v19

    .line 670
    .local v19, "status":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    new-instance v5, Lcom/cigna/coach/apiobjects/CoachResponse;

    invoke-direct {v5}, Lcom/cigna/coach/apiobjects/CoachResponse;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 671
    .end local v18    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    .local v5, "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    :try_start_1
    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/cigna/coach/apiobjects/CoachResponse;->setObject(Ljava/lang/Object;)V

    .line 674
    new-instance v3, Lcom/cigna/coach/utils/CoachMessageHelper;

    invoke-direct {v3, v13}, Lcom/cigna/coach/utils/CoachMessageHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 675
    .local v3, "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v4

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    invoke-virtual {v4, v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 676
    .local v6, "goalCompleted":Z
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v4

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v4, v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 678
    .local v7, "missionCompleted":Z
    if-eqz v7, :cond_1

    .line 679
    new-instance v9, Lcom/cigna/coach/utils/BadgeAwarder;

    move-object/from16 v0, p1

    invoke-direct {v9, v0, v13}, Lcom/cigna/coach/utils/BadgeAwarder;-><init>(Ljava/lang/String;Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 680
    .local v9, "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    invoke-virtual {v9}, Lcom/cigna/coach/utils/BadgeAwarder;->awardBadges()Ljava/util/List;

    move-result-object v10

    .line 681
    .local v10, "badgesEarned":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->setBadgeList(Ljava/util/List;)V

    .line 684
    .end local v9    # "badgeAwarder":Lcom/cigna/coach/utils/BadgeAwarder;
    .end local v10    # "badgesEarned":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :cond_1
    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getBadgeList()Ljava/util/List;

    move-result-object v8

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v8}, Lcom/cigna/coach/utils/CoachMessageHelper;->setSetMissionDataMessages(Ljava/lang/String;Lcom/cigna/coach/apiobjects/CoachResponse;ZZLjava/util/List;)V

    .line 686
    new-instance v11, Lcom/cigna/coach/utils/ContentHelper;

    invoke-direct {v11, v13}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 687
    .local v11, "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;)V

    .line 688
    new-instance v12, Lcom/cigna/coach/utils/ContextData;

    invoke-direct {v12}, Lcom/cigna/coach/utils/ContextData;-><init>()V

    .line 689
    .local v12, "contextData":Lcom/cigna/coach/utils/ContextData;
    sget-object v4, Lcom/cigna/coach/utils/ContextData$ContextDataType;->GOAL_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalContentId()I

    move-result v8

    invoke-virtual {v12, v4, v8}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V

    .line 690
    sget-object v4, Lcom/cigna/coach/utils/ContextData$ContextDataType;->MISSION_NAME:Lcom/cigna/coach/utils/ContextData$ContextDataType;

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionContentId()I

    move-result v8

    invoke-virtual {v12, v4, v8}, Lcom/cigna/coach/utils/ContextData;->addContentIdToResolve(Lcom/cigna/coach/utils/ContextData$ContextDataType;I)V

    .line 692
    invoke-virtual {v11, v5, v12}, Lcom/cigna/coach/utils/ContentHelper;->setContent(Ljava/lang/Object;Lcom/cigna/coach/utils/ContextData;)V

    .line 710
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 715
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 716
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 717
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "setUserMissionDetail: END"

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_2
    return-object v5

    .line 711
    .end local v3    # "coachMessageHelper":Lcom/cigna/coach/utils/CoachMessageHelper;
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    .end local v6    # "goalCompleted":Z
    .end local v7    # "missionCompleted":Z
    .end local v11    # "contentHelper":Lcom/cigna/coach/utils/ContentHelper;
    .end local v12    # "contextData":Lcom/cigna/coach/utils/ContextData;
    .end local v16    # "mergedMissionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v19    # "status":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .end local v20    # "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v22    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    .restart local v18    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    :catch_0
    move-exception v14

    move-object/from16 v5, v18

    .line 712
    .end local v18    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    .local v14, "e":Ljava/lang/Exception;
    :goto_0
    :try_start_2
    sget-object v4, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error while setUserMissionDetail"

    invoke-static {v4, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    const-string v8, "Error while setUserMissionDetail."

    invoke-direct {v4, v8, v14}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 715
    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    :goto_1
    invoke-virtual {v13}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 716
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 717
    sget-object v8, Lcom/cigna/coach/GoalsMissions;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v23, "setUserMissionDetail: END"

    move-object/from16 v0, v23

    invoke-static {v8, v0}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v4

    .line 715
    .end local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    .restart local v18    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    :catchall_1
    move-exception v4

    move-object/from16 v5, v18

    .end local v18    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    .restart local v5    # "returnValue":Lcom/cigna/coach/apiobjects/CoachResponse;, "Lcom/cigna/coach/apiobjects/CoachResponse<Lcom/cigna/coach/apiobjects/GoalMissionStatus;>;"
    goto :goto_1

    .line 711
    .restart local v16    # "mergedMissionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .restart local v19    # "status":Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
    .restart local v20    # "trackerActivities":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .restart local v22    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_1
    move-exception v14

    goto :goto_0
.end method
