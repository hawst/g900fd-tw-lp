.class public Lcom/cigna/coach/db/CoachSQLiteHelper;
.super Ljava/lang/Object;
.source "CoachSQLiteHelper.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

.field context:Landroid/content/Context;

.field writable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/CoachSQLiteHelper;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "writable"    # Z

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v1, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->context:Landroid/content/Context;

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->writable:Z

    .line 63
    iput-object v1, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->context:Landroid/content/Context;

    .line 68
    invoke-static {p1}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->getConnectionManager(Landroid/content/Context;)Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    .line 69
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 70
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->writable:Z

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 87
    :cond_0
    return-void
.end method

.method public commit()V
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->writable:Z

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 81
    :cond_0
    return-void
.end method

.method public displayColumnHeaders(Landroid/database/Cursor;)V
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 115
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    .line 117
    .local v2, "columns":[Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, ""

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 119
    .local v3, "dsb":Ljava/lang/StringBuilder;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 121
    .local v1, "column":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const/16 v6, 0x2c

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 119
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 124
    .end local v1    # "column":Ljava/lang/String;
    :cond_0
    sget-object v6, Lcom/cigna/coach/db/CoachSQLiteHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 125
    sget-object v6, Lcom/cigna/coach/db/CoachSQLiteHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_1
    return-void
.end method

.method public displayResultSet(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 136
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "columns":[Ljava/lang/String;
    array-length v0, v1

    .line 139
    .local v0, "colCount":I
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, ""

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 140
    .local v2, "dsb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 142
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const/16 v4, 0x2c

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 140
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 145
    :cond_0
    sget-object v4, Lcom/cigna/coach/db/CoachSQLiteHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 146
    sget-object v4, Lcom/cigna/coach/db/CoachSQLiteHelper;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_1
    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->context:Landroid/content/Context;

    return-object v0
.end method

.method public declared-synchronized getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected loadContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V
    .locals 2
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "data"    # [Ljava/lang/String;
    .param p3, "multiLanguage"    # Z

    .prologue
    .line 105
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->loadContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V

    .line 106
    return-void
.end method

.method public loadData(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSQLiteHelper;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->loadData(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;Z)V

    .line 102
    return-void
.end method
