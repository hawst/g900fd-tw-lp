.class Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;
.super Ljava/lang/Object;
.source "WidgetNotificationDBManager.java"

# interfaces
.implements Lcom/cigna/coach/db/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/WidgetNotificationDBManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WidgetNotificationDataRowMapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/cigna/coach/db/RowMapper",
        "<",
        "Lcom/cigna/coach/dataobjects/WidgetNotificationData;",
        ">;"
    }
.end annotation


# instance fields
.field coachMessageMapper:Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    new-instance v0, Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;

    invoke-direct {v0}, Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;->coachMessageMapper:Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/cigna/coach/db/WidgetNotificationDBManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/cigna/coach/db/WidgetNotificationDBManager$1;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;-><init>()V

    return-void
.end method


# virtual methods
.method public mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    .locals 4
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 196
    new-instance v0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;-><init>()V

    .line 197
    .local v0, "data":Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    const-string v2, "SEQ_ID"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setSeqId(I)V

    .line 198
    const-string v2, "WIDGT_RANK"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setDisplayRank(I)V

    .line 199
    const-string v2, "MSG_TYPE"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->getInstance(I)Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V

    .line 200
    const-string v2, "WIDGT_TYPE"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->getInstance(I)Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setWidgetType(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V

    .line 201
    iget-object v2, p0, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;->coachMessageMapper:Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;

    invoke-virtual {v2, p1}, Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setCoachMessageData(Lcom/cigna/coach/dataobjects/CoachMessageData;)V

    .line 202
    const-string v2, "USR_ID"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUserId(I)V

    .line 203
    const-string v2, "UNIQ_KEY"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setUniqueKey(Ljava/lang/String;)V

    .line 205
    :try_start_0
    const-string v2, "INTNT"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->setIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    return-object v0

    .line 206
    :catch_0
    move-exception v1

    .line 207
    .local v1, "e":Ljava/net/URISyntaxException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public bridge synthetic mapRow(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 191
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/WidgetNotificationData;

    move-result-object v0

    return-object v0
.end method

.method public toRow(Lcom/cigna/coach/dataobjects/WidgetNotificationData;I)Landroid/content/ContentValues;
    .locals 4
    .param p1, "data"    # Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    .param p2, "userKey"    # I

    .prologue
    .line 213
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 215
    .local v0, "insertions":Landroid/content/ContentValues;
    const-string v1, "DISPL_RANK_NUM"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getDisplayRank()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 216
    const-string v1, "MSG_TYPE"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;->getWidgetInfoType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 217
    const-string v1, "WIDGT_TYPE"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getWidgetType()Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->getWidgetType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 218
    const-string v1, "COACH_MSG_ID"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getCoachMessageData()Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 219
    const-string v1, "USR_ID"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 220
    const-string v1, "UNIQ_KEY"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getUniqueKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v1, "INTNT"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    # getter for: Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->access$100()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    # getter for: Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    :cond_0
    return-object v0
.end method
