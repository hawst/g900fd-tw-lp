.class public Lcom/cigna/coach/db/tabledefs/NotificationColumns;
.super Ljava/lang/Object;
.source "NotificationColumns.java"


# static fields
.field public static final KEY_DLVRY_DT:Ljava/lang/String; = "DLVRY_DT"

.field public static final KEY_LAST_STRTUP_DT:Ljava/lang/String; = "LAST_STRTUP_DT"

.field public static final KEY_LAST_TM_CHG_DT:Ljava/lang/String; = "LAST_TM_CHG_DT"

.field public static final KEY_NOTFTN_END_TM:Ljava/lang/String; = "NOTFTN_END_TM"

.field public static final KEY_NOTFTN_OBJ:Ljava/lang/String; = "NOTFTN_OBJ"

.field public static final KEY_NOTFTN_STRT_TM:Ljava/lang/String; = "NOTFTN_STRT_TM"

.field public static final TABLE_NAME_NOTFTN_QUEUE_BLOB:Ljava/lang/String; = "NOTFTN_QUEUE_BLOB"

.field public static final TABLE_NAME_NOTFTN_STE:Ljava/lang/String; = "NOTFTN_STE"

.field protected static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
