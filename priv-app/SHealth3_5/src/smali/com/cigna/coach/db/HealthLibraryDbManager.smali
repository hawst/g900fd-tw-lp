.class public Lcom/cigna/coach/db/HealthLibraryDbManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "HealthLibraryDbManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/HealthLibraryDbManager$1;,
        Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;,
        Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;,
        Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryArticleRowMapper;
    }
.end annotation


# static fields
.field private static final ARTCL_MAPPER:Lcom/cigna/coach/db/RowMapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/cigna/coach/db/RowMapper",
            "<",
            "Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;",
            ">;"
        }
    .end annotation
.end field

.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    const-class v0, Lcom/cigna/coach/db/HealthLibraryDbManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    .line 49
    new-instance v0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryArticleRowMapper;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryArticleRowMapper;-><init>(Lcom/cigna/coach/db/HealthLibraryDbManager$1;)V

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->ARTCL_MAPPER:Lcom/cigna/coach/db/RowMapper;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 58
    return-void
.end method


# virtual methods
.method public final addUserFavoriteArticle(Ljava/lang/String;I)Z
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "articleId"    # I

    .prologue
    .line 599
    sget-object v7, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 600
    sget-object v7, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "addUserFavoriteArticle adding favorite article %d for user %s."

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object p1, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_0
    const-wide/16 v3, -0x1

    .line 606
    .local v3, "rowid":J
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 609
    .local v5, "userKey":I
    const/4 v0, 0x0

    .line 610
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "SELECT COUNT(*) FROM USR_ARTCL WHERE ARTCL_ID=? AND USR_ID=?"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 618
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 619
    const/4 v7, 0x0

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 621
    if-eqz v1, :cond_1

    .line 622
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 626
    :cond_1
    if-lez v0, :cond_3

    .line 627
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "articleId \'%d\' already exists"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 634
    .end local v0    # "count":I
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v5    # "userKey":I
    :catch_0
    move-exception v2

    .line 635
    .local v2, "e":Ljava/lang/RuntimeException;
    sget-object v7, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "addUserFavoriteArticle error"

    invoke-static {v7, v8, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 636
    throw v2

    .line 621
    .end local v2    # "e":Ljava/lang/RuntimeException;
    .restart local v0    # "count":I
    .restart local v1    # "cursor":Landroid/database/Cursor;
    .restart local v5    # "userKey":I
    :catchall_0
    move-exception v7

    if-eqz v1, :cond_2

    .line 622
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v7

    .line 629
    :cond_3
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 630
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "USR_ID"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 631
    const-string v7, "ARTCL_ID"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 632
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "USR_ARTCL"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-wide v3

    .line 639
    sget-object v7, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v8, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 640
    sget-object v8, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "addUserFavoriteArticle %s favorite article %d for user %s."

    const/4 v7, 0x3

    new-array v10, v7, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    cmp-long v7, v3, v12

    if-gez v7, :cond_5

    const-string/jumbo v7, "unable to add"

    :goto_0
    aput-object v7, v10, v11

    const/4 v7, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v7

    const/4 v7, 0x2

    aput-object p1, v10, v7

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    :cond_4
    const-wide/16 v7, -0x1

    cmp-long v7, v3, v7

    if-lez v7, :cond_6

    const/4 v7, 0x1

    :goto_1
    return v7

    .line 640
    :cond_5
    const-string v7, "added"

    goto :goto_0

    .line 645
    :cond_6
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public final getArticle(Ljava/lang/String;II)Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "articleId"    # I
    .param p3, "subCategoryId"    # I

    .prologue
    .line 68
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 69
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getArticle retrieving article \'%d\'."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    const/4 v2, 0x0

    .line 73
    .local v2, "count":I
    const/4 v3, 0x0

    .line 74
    .local v3, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 77
    .local v1, "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 78
    .local v5, "userKey":I
    const/4 v6, -0x1

    if-ne p3, v6, :cond_5

    .line 79
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTICLE_DETAIL_QUERY:Ljava/lang/String;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 94
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    .line 96
    if-lez v2, :cond_1

    .line 97
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 98
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->ARTCL_MAPPER:Lcom/cigna/coach/db/RowMapper;

    invoke-interface {v6, v3}, Lcom/cigna/coach/db/RowMapper;->mapRow(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;

    move-object v1, v0

    .line 100
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v7, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 101
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getArticle Article %d,%d,%d,%d,%d,%d,%d,%d,%b"

    const/16 v8, 0x9

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getTitleId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getImageId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getDetailId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getCategoryId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x5

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getSubCategoryId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x6

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getCatDescId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x7

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getSubCatDescId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/16 v9, 0x8

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->isFavorite()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    :cond_1
    if-eqz v3, :cond_2

    .line 113
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_2
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v7, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    if-eqz v1, :cond_3

    .line 118
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getArticle returning article %d in category %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getCategoryId()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_3
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v7, Lcom/cigna/coach/utils/Log;->WARN:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    if-nez v1, :cond_4

    .line 123
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getArticle article %d NOT FOUND!"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_4
    return-object v1

    .line 86
    :cond_5
    :try_start_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/db/HealthLibraryDBQueries;->NON_PRIMARY_ARTICLE_DETAIL_QUERY:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    goto/16 :goto_0

    .line 108
    .end local v5    # "userKey":I
    :catch_0
    move-exception v4

    .line 109
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error getting Article detail for article : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 110
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 112
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    if-eqz v3, :cond_6

    .line 113
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v6
.end method

.method public final getArticlesByKeyword(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "keyword"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 139
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getArticlesByKeyword retrieving articles for keyword \'%s\'."

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_0
    new-instance v3, Lcom/cigna/coach/db/CountryExceptionsDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v11

    invoke-direct {v3, v11}, Lcom/cigna/coach/db/CountryExceptionsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 143
    .local v3, "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppressAlcoholRelatedContent(Ljava/lang/String;)Z

    move-result v8

    .line 145
    .local v8, "shouldSuppressAlcoholRelatedContent":Z
    new-instance v9, Ljava/lang/StringBuffer;

    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDBQueries;->KWD_ARTICLES_QUERY:Ljava/lang/String;

    invoke-direct {v9, v11}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 146
    .local v9, "sql":Ljava/lang/StringBuffer;
    if-eqz v8, :cond_2

    .line 147
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 148
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getArticlesByKeyword - suppressing alcohol related subcategory: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/16 v13, 0xcd

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    :cond_1
    const-string v11, " AND ARTCL_DTL_VW.ARTCL_ID NOT IN ( SELECT A.ARTCL_ID FROM ARTCL_DTL_VW A WHERE A.SUB_CTGRY_ID = 205 )"

    invoke-virtual {v9, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 153
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .local v7, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    const/4 v5, 0x0

    .line 157
    .local v5, "cursor":Landroid/database/Cursor;
    if-eqz p2, :cond_3

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x1

    if-ge v11, v12, :cond_5

    .line 158
    :cond_3
    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "keyword is null or empty."

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :catch_0
    move-exception v6

    .line 189
    .local v6, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getArticlesByKeyword error"

    invoke-static {v11, v12, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 190
    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    .end local v6    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v11

    if-eqz v5, :cond_4

    .line 193
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v11

    .line 161
    :cond_5
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v10

    .line 162
    .local v10, "userKey":I
    new-instance v2, Lcom/cigna/coach/utils/ContentHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v11

    invoke-direct {v2, v11}, Lcom/cigna/coach/utils/ContentHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 163
    .local v2, "ch":Lcom/cigna/coach/utils/ContentHelper;
    invoke-virtual {v2}, Lcom/cigna/coach/utils/ContentHelper;->getLocaleFromContext()Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v4

    .line 164
    .local v4, "currentLocaleType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 165
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Current localetype is:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x3

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    invoke-virtual {v4}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getKey()I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "*"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 177
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v11

    if-nez v11, :cond_8

    .line 178
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->ARTCL_MAPPER:Lcom/cigna/coach/db/RowMapper;

    invoke-interface {v11, v5}, Lcom/cigna/coach/db/RowMapper;->mapRow(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;

    .line 179
    .local v1, "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v12, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 182
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getArticlesByKeyword Keyword \'%s\' Article %d,%d,%d,%d,%d,%d,%b"

    const/16 v13, 0x8

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    const/4 v14, 0x1

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x2

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getCategoryId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x3

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getSubCategoryId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x4

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getTitleId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x5

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getImageId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x6

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getDetailId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x7

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->isFavorite()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_7
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 192
    .end local v1    # "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    :cond_8
    if-eqz v5, :cond_9

    .line 193
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_9
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v12, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 198
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getArticlesByKeyword returning %d articles for keyword \'%s\'."

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_a
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    if-nez v11, :cond_b

    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v12, Lcom/cigna/coach/utils/Log;->WARN:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 203
    sget-object v11, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getArticlesByKeyword found ZERO articles for keyword \'%s\'!"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p2, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    :cond_b
    return-object v7
.end method

.method public final getArticlesForSubCategory(Ljava/lang/String;II)Ljava/util/List;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryId"    # I
    .param p3, "subCategoryId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 221
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticlesForSubCategory retrieving articles for category/subcategory %d/%d."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 227
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    const/4 v1, 0x0

    .line 230
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    .line 231
    .local v4, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDBQueries;->SUBCATEGORY_ARTICLES_QUERY:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 241
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_3

    .line 242
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->ARTCL_MAPPER:Lcom/cigna/coach/db/RowMapper;

    invoke-interface {v5, v1}, Lcom/cigna/coach/db/RowMapper;->mapRow(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;

    .line 243
    .local v0, "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 246
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticlesForSubCategory category/subcategory %d/%d Article %d,%d,%d,%d,%d,%d,%b"

    const/16 v7, 0x9

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getCategoryId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getSubCategoryId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x5

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getTitleId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x6

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getImageId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x7

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getDetailId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/16 v8, 0x8

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->isFavorite()Z

    move-result v9

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 253
    .end local v0    # "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    .end local v4    # "userKey":I
    :catch_0
    move-exception v2

    .line 254
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticlesForSubCategory error"

    invoke-static {v5, v6, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 255
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    if-eqz v1, :cond_2

    .line 258
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v5

    .line 257
    .restart local v4    # "userKey":I
    :cond_3
    if-eqz v1, :cond_4

    .line 258
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 262
    :cond_4
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 263
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticlesForSubCategory returning %d articles for category/subcategory %d/%d."

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_6

    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->WARN:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 269
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getArticlesForSubCategory found ZERO articles for category/subcategory %d/%d!"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_6
    return-object v3
.end method

.method public final getCategories()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryCategory;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 283
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getCategories entered."

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 288
    .local v11, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryCategory;>;"
    new-instance v10, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;

    invoke-direct {v10, v13, v14, v2}, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;-><init>(III)V

    .line 289
    .local v10, "mapper":Lcom/cigna/coach/db/RowMapper;, "Lcom/cigna/coach/db/RowMapper<Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;>;"
    const/4 v8, 0x0

    .line 292
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "CTGRY"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "CTGRY_ID"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "CNTNT_ID"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "IMG_CNTNT_ID"

    aput-object v4, v2, v3

    const-string v3, "%s=1"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "ACTV_IND"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "RANK_NUM"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 303
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 304
    invoke-interface {v10, v8}, Lcom/cigna/coach/db/RowMapper;->mapRow(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;

    .line 305
    .local v12, "row":Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v1, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 308
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getCategories Category %d,%d,%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->getCategoryId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->getCntntId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {v12}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->getImgCntntId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 313
    .end local v12    # "row":Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    :catch_0
    move-exception v9

    .line 314
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getCategories error"

    invoke-static {v0, v1, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 315
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 317
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_2

    .line 318
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 317
    :cond_3
    if-eqz v8, :cond_4

    .line 318
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 322
    :cond_4
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 323
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getCategories returning %d categories."

    new-array v2, v14, [Ljava/lang/Object;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :cond_5
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v1, Lcom/cigna/coach/utils/Log;->WARN:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 327
    sget-object v0, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getCategories found ZERO categories!"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    :cond_6
    return-object v11
.end method

.method public final getFavoritesByCategory(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 341
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 342
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFavoritesByCategory getting favorite for user \'%s\'."

    new-array v7, v11, [Ljava/lang/Object;

    aput-object p1, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 346
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    const/4 v1, 0x0

    .line 349
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    .line 350
    .local v4, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDBQueries;->FAVES_ARTICLES_BY_CTGRY_QUERY:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 355
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 356
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFavoritesByCategory User \'%s\' Article Count is %d."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_4

    .line 363
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->ARTCL_MAPPER:Lcom/cigna/coach/db/RowMapper;

    invoke-interface {v5, v1}, Lcom/cigna/coach/db/RowMapper;->mapRow(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;

    .line 364
    .local v0, "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 367
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFavoritesByCategory User \'%s\' Article %d,%d,%d,%d,%d,%d"

    const/4 v7, 0x7

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getCategoryId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getSubCategoryId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getTitleId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x5

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getImageId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x6

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getDetailId()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    .end local v0    # "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    .end local v4    # "userKey":I
    :catch_0
    move-exception v2

    .line 374
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFavoritebyCategory error"

    invoke-static {v5, v6, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 375
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 377
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    if-eqz v1, :cond_3

    .line 378
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v5

    .line 377
    .restart local v4    # "userKey":I
    :cond_4
    if-eqz v1, :cond_5

    .line 378
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 382
    :cond_5
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 383
    sget-object v5, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFavoritesByCategory returning %d articles for user \'%s\'."

    new-array v7, v12, [Ljava/lang/Object;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    aput-object p1, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :cond_6
    return-object v3
.end method

.method public final getFavoritesByTitle(Ljava/lang/String;Z)Ljava/util/List;
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "sortAscending"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 398
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v2, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    sget-object v2, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getFavoritesByTitle getting favorite for user \'%s\' with sort %s."

    new-array v4, v7, [Ljava/lang/Object;

    aput-object p1, v4, v5

    if-eqz p2, :cond_2

    const-string v1, "ascending"

    :goto_0
    aput-object v1, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    :cond_0
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getFavoritesByCategory(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 406
    .local v0, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getFavoritesByTitle returning %d articles for user \'%s\'."

    new-array v3, v7, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    aput-object p1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    :cond_1
    return-object v0

    .line 399
    .end local v0    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    :cond_2
    const-string v1, "descending"

    goto :goto_0
.end method

.method public final getFeaturedArticles(Ljava/lang/String;)Ljava/util/List;
    .locals 26
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibraryArticle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 419
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 420
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getFeaturedArticles entered."

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :cond_0
    new-instance v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 424
    .local v7, "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppressAlcoholRelatedContent(Ljava/lang/String;)Z

    move-result v18

    .line 426
    .local v18, "shouldSuppressAlcoholRelatedContent":Z
    new-instance v19, Ljava/lang/StringBuffer;

    const-string v21, "SELECT CTGRY_ARTCL.ARTCL_ID AS C0,CTGRY_ARTCL.CTGRY_ID AS C1 FROM CTGRY_ARTCL JOIN CTGRY ON CTGRY.CTGRY_ID=CTGRY_ARTCL.CTGRY_ID JOIN ARTCL ON ARTCL.ARTCL_ID=CTGRY_ARTCL.ARTCL_ID WHERE PRIM_ARTCL_IND=1 AND ARTCL.ACTV_IND=1 AND CTGRY.ACTV_IND=1"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 427
    .local v19, "sql":Ljava/lang/StringBuffer;
    if-eqz v18, :cond_1

    .line 428
    const-string v21, " AND CTGRY_ARTCL.ARTCL_ID NOT IN ( SELECT A.ARTCL_ID FROM ARTCL_DTL_VW A WHERE A.SUB_CTGRY_ID = 205 )"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 430
    :cond_1
    const-string v21, " ORDER BY CTGRY.RANK_NUM"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 432
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 433
    .local v17, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibraryArticle;>;"
    const/4 v9, 0x0

    .line 436
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v20

    .line 437
    .local v20, "userKey":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 440
    .local v3, "article_keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v21

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 442
    const/16 v8, -0x64

    .line 444
    .local v8, "curr_cat":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 445
    .local v6, "cat_list":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/Integer;>;>;"
    const/4 v13, 0x0

    .line 447
    .local v13, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    if-eqz v9, :cond_6

    .line 448
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v21

    if-nez v21, :cond_5

    .line 449
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 451
    .local v5, "cat":I
    if-eq v5, v8, :cond_2

    .line 452
    new-instance v13, Ljava/util/ArrayList;

    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 453
    .restart local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v6, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    move v8, v5

    .line 457
    :cond_2
    if-eqz v13, :cond_3

    .line 458
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 496
    .end local v3    # "article_keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "cat":I
    .end local v6    # "cat_list":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/Integer;>;>;"
    .end local v8    # "curr_cat":I
    .end local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v20    # "userKey":I
    :catch_0
    move-exception v11

    .line 497
    .local v11, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getFeaturedArticles error"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v1, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 498
    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 500
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v21

    if-eqz v9, :cond_4

    .line 501
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v21

    .line 464
    .restart local v3    # "article_keys":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v6    # "cat_list":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/Integer;>;>;"
    .restart local v8    # "curr_cat":I
    .restart local v13    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v20    # "userKey":I
    :cond_5
    :try_start_2
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 467
    :cond_6
    new-instance v16, Ljava/util/Random;

    invoke-direct/range {v16 .. v16}, Ljava/util/Random;-><init>()V

    .line 469
    .local v16, "random":Ljava/util/Random;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 470
    .local v4, "artlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v21

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 474
    .end local v4    # "artlist":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_7
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v14

    .line 475
    .local v14, "num_articles":I
    const-string v21, "%s AND ARTCL_DTL_VW.ARTCL_ID IN(%s)"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    sget-object v24, Lcom/cigna/coach/db/HealthLibraryDBQueries;->FEAT_ARTICLES_LIST_QUERY_STUB:Ljava/lang/String;

    aput-object v24, v22, v23

    const/16 v23, 0x1

    invoke-static {v14}, Lcom/cigna/coach/db/CoachDBManager;->makePlaceholders(I)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 478
    .local v10, "dtlquery":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 479
    .local v15, "params":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 480
    invoke-interface {v15, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 481
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v22

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v21

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v15, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v21

    check-cast v21, [Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v10, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 485
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v21

    if-nez v21, :cond_9

    .line 486
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->ARTCL_MAPPER:Lcom/cigna/coach/db/RowMapper;

    move-object/from16 v0, v21

    invoke-interface {v0, v9}, Lcom/cigna/coach/db/RowMapper;->mapRow(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;

    .line 487
    .local v2, "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 489
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v22, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 490
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getFeaturedArticles Article %d,%d,%d,%d,%d,%d,%b"

    const/16 v23, 0x7

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getCategoryId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x2

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getSubCategoryId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x3

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getTitleId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x4

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getImageId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x5

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->getDetailId()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x6

    invoke-virtual {v2}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->isFavorite()Z

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_8
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 500
    .end local v2    # "article":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    :cond_9
    if-eqz v9, :cond_a

    .line 501
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 505
    :cond_a
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 506
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getFeaturedArticles returning %d articles."

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_b
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v21

    if-nez v21, :cond_c

    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v22, Lcom/cigna/coach/utils/Log;->WARN:Ljava/lang/String;

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 510
    sget-object v21, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getFeaturedArticles found ZERO articles!"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :cond_c
    return-object v17
.end method

.method public final getSubCategories(Ljava/lang/String;I)Ljava/util/List;
    .locals 16
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 525
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 526
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getSubCategories retrieving subcategories for category \'%d\'."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    :cond_0
    new-instance v9, Lcom/cigna/coach/db/CountryExceptionsDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v1

    invoke-direct {v9, v1}, Lcom/cigna/coach/db/CountryExceptionsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 530
    .local v9, "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppressAlcoholRelatedContent(Ljava/lang/String;)Z

    move-result v15

    .line 532
    .local v15, "shouldSuppressAlcoholRelatedContent":Z
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 533
    .local v13, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;>;"
    new-instance v12, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x2

    const/4 v5, 0x3

    invoke-direct {v12, v1, v2, v3, v5}, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;-><init>(IIII)V

    .line 534
    .local v12, "mapper":Lcom/cigna/coach/db/RowMapper;, "Lcom/cigna/coach/db/RowMapper<Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;>;"
    const/4 v10, 0x0

    .line 536
    .local v10, "cursor":Landroid/database/Cursor;
    const-string v1, "%s=? AND %s=1"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v5, "CTGRY_ID"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "ACTV_IND"

    aput-object v5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 537
    .local v4, "selection":Ljava/lang/String;
    if-eqz v15, :cond_2

    .line 538
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 539
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSubCategories - suppressing alcohol related subcategory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xcd

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "SUB_CTGRY_ID"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0xcd

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 545
    :cond_2
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "SUB_CTGRY"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "CTGRY_ID"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "SUB_CTGRY_ID"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "CNTNT_ID"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    const-string v6, "IMG_CNTNT_ID"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "RANK_NUM"

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 560
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_5

    .line 561
    invoke-interface {v12, v10}, Lcom/cigna/coach/db/RowMapper;->mapRow(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;

    .line 562
    .local v14, "row":Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;
    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 564
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v2, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 565
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getSubCategories Sub Category %d,%d,%d,%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->getCategoryId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x1

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->getSubCategoryId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x2

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->getCntntId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x3

    invoke-virtual {v14}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->getImgCntntId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 570
    .end local v14    # "row":Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;
    :catch_0
    move-exception v11

    .line 571
    .local v11, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getSubCategories error"

    invoke-static {v1, v2, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 572
    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 574
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_4

    .line 575
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1

    .line 574
    :cond_5
    if-eqz v10, :cond_6

    .line 575
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 579
    :cond_6
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v2, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 580
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getSubCategories returning %d subcategories for category %d."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    const/4 v5, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_7
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v2, Lcom/cigna/coach/utils/Log;->WARN:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 585
    sget-object v1, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getSubCategories found ZERO subcategories!"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :cond_8
    return-object v13
.end method

.method public final removeUserFavoriteArticle(Ljava/lang/String;I)Z
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "articleId"    # I

    .prologue
    const/4 v11, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 657
    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 658
    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "removeUserFavoriteArticle removing favorite article %d from user %s."

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v5

    aput-object p1, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    :cond_0
    const/4 v1, 0x0

    .line 665
    .local v1, "rows":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 667
    .local v2, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v6, "USR_ARTCL"

    const-string v7, "USR_ID=? AND ARTCL_ID=?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 678
    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    sget-object v6, Lcom/cigna/coach/utils/Log;->DEBUG:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/cigna/coach/utils/Log;->isLoggable(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 679
    sget-object v6, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "removeUserFavoriteArticle %s favorite article %d for user %s."

    const/4 v3, 0x3

    new-array v8, v3, [Ljava/lang/Object;

    if-ge v1, v4, :cond_2

    const-string/jumbo v3, "unable to remove"

    :goto_0
    aput-object v3, v8, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v4

    aput-object p1, v8, v11

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    :cond_1
    if-lez v1, :cond_3

    move v3, v4

    :goto_1
    return v3

    .line 673
    .end local v2    # "userKey":I
    :catch_0
    move-exception v0

    .line 674
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDbManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "removeUserFavoriteArticle error"

    invoke-static {v3, v4, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 675
    throw v0

    .line 679
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .restart local v2    # "userKey":I
    :cond_2
    const-string/jumbo v3, "removed"

    goto :goto_0

    :cond_3
    move v3, v5

    .line 684
    goto :goto_1
.end method
