.class public final Lcom/cigna/coach/db/tabledefs/ApiCallLogColumns;
.super Ljava/lang/Object;
.source "ApiCallLogColumns.java"


# static fields
.field public static final KEY_API_CALL_ERR_CD:Ljava/lang/String; = "API_CALL_ERR_CD"

.field public static final KEY_API_CALL_RESP_TM:Ljava/lang/String; = "API_CALL_RESP_TM"

.field public static final KEY_API_CALL_SRC_CD:Ljava/lang/String; = "API_CALL_SRC_CD"

.field public static final KEY_API_CALL_TS:Ljava/lang/String; = "API_CALL_TS"

.field public static final KEY_API_CD:Ljava/lang/String; = "API_CD"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "USR_API_CALL_LOG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
