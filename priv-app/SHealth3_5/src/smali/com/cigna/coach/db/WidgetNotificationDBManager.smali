.class public Lcom/cigna/coach/db/WidgetNotificationDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "WidgetNotificationDBManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/WidgetNotificationDBManager$1;,
        Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 24
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private basicDismissAll(I)V
    .locals 7
    .param p1, "userKey"    # I

    .prologue
    .line 175
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "WIDGT_NOTFTN"

    const-string v2, "USR_ID = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 180
    return-void
.end method

.method private basicDismissByKey(ILjava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V
    .locals 7
    .param p1, "userKey"    # I
    .param p2, "uniqueKey"    # Ljava/lang/String;
    .param p3, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "WIDGT_NOTFTN"

    const-string v2, "USR_ID = ? AND UNIQ_KEY = ? AND WIDGT_TYPE = ?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->getWidgetType()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 172
    return-void
.end method

.method private basicDismissLatest(ILcom/cigna/coach/interfaces/IWidget$WidgetType;)V
    .locals 6
    .param p1, "userKey"    # I
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 183
    sget-object v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "basicDismissLatest:  userKey: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "WIDGET TYPE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Query = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "DELETE FROM WIDGT_NOTFTN WHERE SEQ_ID IN (SELECT SEQ_ID FROM WIDGT_NOTFTN WHERE WIDGT_NOTFTN.USR_ID = ? AND WIDGT_NOTFTN.WIDGT_TYPE = ? ORDER BY DISPL_RANK_NUM, WIDGT_NOTFTN.SEQ_ID DESC LIMIT 1)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DELETE FROM WIDGT_NOTFTN WHERE SEQ_ID IN (SELECT SEQ_ID FROM WIDGT_NOTFTN WHERE WIDGT_NOTFTN.USR_ID = ? AND WIDGT_NOTFTN.WIDGT_TYPE = ? ORDER BY DISPL_RANK_NUM, WIDGT_NOTFTN.SEQ_ID DESC LIMIT 1)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->getWidgetType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    return-void
.end method


# virtual methods
.method public getNotification(Ljava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    const/4 v3, 0x0

    .line 33
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 34
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getNotification: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 38
    .local v0, "cursor":Landroid/database/Cursor;
    new-instance v1, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;

    invoke-direct {v1, v3}, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;-><init>(Lcom/cigna/coach/db/WidgetNotificationDBManager$1;)V

    .line 41
    .local v1, "mapper":Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 42
    .local v2, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/WidgetNotificationDBQueries;->GET_LATEST_NOTIFICATION_QUERY:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->getWidgetType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 45
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 47
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_3

    .line 53
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 55
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 56
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getNotification: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-object v3

    .line 51
    :cond_3
    :try_start_1
    invoke-virtual {v1, v0}, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    .line 53
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 55
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 56
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getNotification: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 53
    .end local v2    # "userKey":I
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 55
    :cond_5
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 56
    sget-object v4, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getNotification: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v3
.end method

.method public removeAllNotifications(Ljava/lang/String;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 150
    sget-object v1, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 151
    sget-object v1, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "removeAllNotifications: START"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v0

    .line 155
    .local v0, "userKey":I
    invoke-direct {p0, v0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->basicDismissAll(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    sget-object v1, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    sget-object v1, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "removeAllNotifications: END"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_1
    return-void

    .line 158
    .end local v0    # "userKey":I
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 159
    sget-object v2, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "removeAllNotifications: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v1
.end method

.method public removeNotification(Ljava/lang/String;)V
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 104
    sget-object v5, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 105
    sget-object v5, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "removeLatestNotification: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 109
    .local v3, "userKey":I
    invoke-static {}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->values()[Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    .line 110
    .local v4, "widgetType":Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    invoke-direct {p0, v3, v4}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->basicDismissLatest(ILcom/cigna/coach/interfaces/IWidget$WidgetType;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    .end local v4    # "widgetType":Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    :cond_1
    sget-object v5, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 115
    sget-object v5, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "removeLatestNotification: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_2
    return-void

    .line 114
    .end local v0    # "arr$":[Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "userKey":I
    :catchall_0
    move-exception v5

    sget-object v6, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 115
    sget-object v6, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "removeLatestNotification: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v5
.end method

.method public removeNotification(Ljava/lang/String;Ljava/util/List;)V
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p2, "uniqueIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 127
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "removeNotificationsByKey: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 131
    .local v5, "userKey":I
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 132
    .local v4, "uniqueKey":Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/interfaces/IWidget$WidgetType;->values()[Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v6, v0, v2

    .line 133
    .local v6, "widgetType":Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    invoke-direct {p0, v5, v4, v6}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->basicDismissByKey(ILjava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 138
    .end local v0    # "arr$":[Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "uniqueKey":Ljava/lang/String;
    .end local v6    # "widgetType":Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    :cond_2
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 139
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "removeNotificationsByKey: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    :cond_3
    return-void

    .line 138
    .end local v5    # "userKey":I
    :catchall_0
    move-exception v7

    sget-object v8, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 139
    sget-object v8, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v9, "removeNotificationsByKey: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v7
.end method

.method public setNotifications(Ljava/lang/String;Ljava/util/List;)V
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/WidgetNotificationData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "notificationsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/WidgetNotificationData;>;"
    const/4 v10, 0x0

    .line 67
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 68
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "setNotifications: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "adding "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " notifications to database"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    new-instance v4, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;

    invoke-direct {v4, v10}, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;-><init>(Lcom/cigna/coach/db/WidgetNotificationDBManager$1;)V

    .line 75
    .local v4, "mapper":Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    .line 76
    .local v6, "userKey":I
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/dataobjects/WidgetNotificationData;

    .line 77
    .local v5, "notification":Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getUniqueKey()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->getWidgetType()Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    move-result-object v8

    invoke-direct {p0, v6, v7, v8}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->basicDismissByKey(ILjava/lang/String;Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V

    .line 78
    invoke-virtual {v4, v5, v6}, Lcom/cigna/coach/db/WidgetNotificationDBManager$WidgetNotificationDataRowMapper;->toRow(Lcom/cigna/coach/dataobjects/WidgetNotificationData;I)Landroid/content/ContentValues;

    move-result-object v3

    .line 79
    .local v3, "insertions":Landroid/content/ContentValues;
    invoke-virtual {p0}, Lcom/cigna/coach/db/WidgetNotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "WIDGT_NOTFTN"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 80
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 81
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Inserted row into WIDGT_NOTFTN"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v3}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 83
    .local v0, "column":Ljava/lang/String;
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 90
    .end local v0    # "column":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "insertions":Landroid/content/ContentValues;
    .end local v5    # "notification":Lcom/cigna/coach/dataobjects/WidgetNotificationData;
    .end local v6    # "userKey":I
    :catchall_0
    move-exception v7

    sget-object v8, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 91
    sget-object v8, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v9, "setNotifications: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v7

    .line 90
    .restart local v6    # "userKey":I
    :cond_3
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 91
    sget-object v7, Lcom/cigna/coach/db/WidgetNotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "setNotifications: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_4
    return-void
.end method
