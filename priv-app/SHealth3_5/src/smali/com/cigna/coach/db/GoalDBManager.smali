.class public Lcom/cigna/coach/db/GoalDBManager;
.super Lcom/cigna/coach/db/GoalMissoinDBManager;
.source "GoalDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field protected static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-class v0, Lcom/cigna/coach/db/GoalDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/GoalMissoinDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 88
    return-void
.end method

.method private isActiveUserGoalBehind14(Ljava/lang/String;II)Z
    .locals 14
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "goalSequenceId"    # I

    .prologue
    .line 481
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 482
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind14: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_0
    const/4 v1, 0x0

    .line 485
    .local v1, "behind":Z
    const/4 v3, 0x0

    .line 487
    .local v3, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 490
    .local v7, "userId":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v8

    const/16 v10, 0xe

    invoke-static {v10}, Lcom/cigna/coach/utils/CalendarUtil;->fromDaysToMilliSecs(I)J

    move-result-wide v10

    sub-long v5, v8, v10

    .line 492
    .local v5, "fourteenDaysBehind":J
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "SELECT  COUNT(*) > 0 AS RESULT FROM  USR_GOAL A  WHERE A.USR_ID = ?  AND A.GOAL_ID = ?  AND A.SEQ_ID = ?  AND A.START_DT < ?"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 499
    if-eqz v3, :cond_4

    .line 500
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "RESULT"

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-nez v8, :cond_3

    .line 532
    if-eqz v3, :cond_1

    .line 533
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 534
    :cond_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 535
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind14: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v2, v1

    .line 538
    .end local v1    # "behind":Z
    .local v2, "behind":I
    :goto_0
    return v2

    .line 505
    .end local v2    # "behind":I
    .restart local v1    # "behind":Z
    :cond_3
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 507
    :cond_4
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "SELECT  (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A  INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?,?)  AND B.MSSN_END_DT >= ? "

    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v13}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v13}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x5

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 517
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 518
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Actual Query = SELECT  (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A  INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?,?)  AND B.MSSN_END_DT >= ? "

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    :cond_5
    if-eqz v3, :cond_6

    .line 522
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 523
    const-string v8, "BEHIND"

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_a

    const/4 v1, 0x1

    .line 525
    :cond_6
    :goto_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 526
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "is behind (14 days passed since last mission completion (success or fail)) = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 532
    :cond_7
    if-eqz v3, :cond_8

    .line 533
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 534
    :cond_8
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 535
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind14: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move v2, v1

    .line 538
    .restart local v2    # "behind":I
    goto/16 :goto_0

    .line 523
    .end local v2    # "behind":I
    :cond_a
    const/4 v1, 0x0

    goto :goto_1

    .line 528
    .end local v5    # "fourteenDaysBehind":J
    .end local v7    # "userId":I
    :catch_0
    move-exception v4

    .line 529
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in isActiveUserGoalBehind14: "

    invoke-static {v8, v9, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 530
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 532
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v3, :cond_b

    .line 533
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 534
    :cond_b
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 535
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "isActiveUserGoalBehind14: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v8
.end method

.method private isActiveUserGoalBehind21(Ljava/lang/String;II)Z
    .locals 14
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "goalSequenceId"    # I

    .prologue
    .line 542
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 543
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind21: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_0
    const/4 v1, 0x0

    .line 546
    .local v1, "behind":Z
    const/4 v3, 0x0

    .line 548
    .local v3, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 551
    .local v7, "userId":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v8

    const/16 v10, 0x15

    invoke-static {v10}, Lcom/cigna/coach/utils/CalendarUtil;->fromDaysToMilliSecs(I)J

    move-result-wide v10

    sub-long v5, v8, v10

    .line 553
    .local v5, "twentyOneDaysBehind":J
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "SELECT  COUNT(*) > 0 AS RESULT FROM  USR_GOAL A  WHERE A.USR_ID = ?  AND A.GOAL_ID = ?  AND A.SEQ_ID = ?  AND A.START_DT < ?"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 561
    if-eqz v3, :cond_4

    .line 562
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "RESULT"

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-nez v8, :cond_3

    .line 595
    if-eqz v3, :cond_1

    .line 596
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 597
    :cond_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 598
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind21: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v2, v1

    .line 601
    .end local v1    # "behind":Z
    .local v2, "behind":I
    :goto_0
    return v2

    .line 567
    .end local v2    # "behind":I
    .restart local v1    # "behind":Z
    :cond_3
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 569
    :cond_4
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "SELECT (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  INNER JOIN MSSN C ON (C.MSSN_ID = B.MSSN_ID)  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?,?)  AND (A.START_DT < ?)  AND C.MSSN_STG_ID = ? "

    const/4 v10, 0x7

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v13}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v13}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x5

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x6

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    invoke-virtual {v13}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getIntValue()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 580
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 581
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Actual Query = SELECT (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  INNER JOIN MSSN C ON (C.MSSN_ID = B.MSSN_ID)  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?,?)  AND (A.START_DT < ?)  AND C.MSSN_STG_ID = ? "

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :cond_5
    if-eqz v3, :cond_6

    .line 585
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 586
    const-string v8, "BEHIND"

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_a

    const/4 v1, 0x1

    .line 588
    :cond_6
    :goto_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 589
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "is behind (21 days since goal picked without committing to an Action mission) = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 595
    :cond_7
    if-eqz v3, :cond_8

    .line 596
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 597
    :cond_8
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 598
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind21: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move v2, v1

    .line 601
    .restart local v2    # "behind":I
    goto/16 :goto_0

    .line 586
    .end local v2    # "behind":I
    :cond_a
    const/4 v1, 0x0

    goto :goto_1

    .line 591
    .end local v5    # "twentyOneDaysBehind":J
    .end local v7    # "userId":I
    :catch_0
    move-exception v4

    .line 592
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in isActiveUserGoalBehind21: "

    invoke-static {v8, v9, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 593
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 595
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v3, :cond_b

    .line 596
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 597
    :cond_b
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 598
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "isActiveUserGoalBehind21: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v8
.end method

.method private isActiveUserGoalBehind7(Ljava/lang/String;II)Z
    .locals 14
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "goalSequenceId"    # I

    .prologue
    .line 415
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 416
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind7: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    :cond_0
    const/4 v1, 0x0

    .line 419
    .local v1, "behind":Z
    const/4 v3, 0x0

    .line 421
    .local v3, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 426
    .local v7, "userId":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v8

    const/4 v10, 0x7

    invoke-static {v10}, Lcom/cigna/coach/utils/CalendarUtil;->fromDaysToMilliSecs(I)J

    move-result-wide v10

    sub-long v5, v8, v10

    .line 428
    .local v5, "sevenDaysBehind":J
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "SELECT  COUNT(*) > 0 AS RESULT FROM  USR_GOAL A  WHERE A.USR_ID = ?  AND A.GOAL_ID = ?  AND A.SEQ_ID = ?  AND A.START_DT < ?"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 436
    if-eqz v3, :cond_4

    .line 437
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "RESULT"

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    if-nez v8, :cond_3

    .line 471
    if-eqz v3, :cond_1

    .line 472
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 474
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind7: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move v2, v1

    .line 477
    .end local v1    # "behind":Z
    .local v2, "behind":I
    :goto_0
    return v2

    .line 442
    .end local v2    # "behind":I
    .restart local v1    # "behind":Z
    :cond_3
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 445
    :cond_4
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "SELECT  (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?, ?)  AND (A.START_DT < ?) "

    const/4 v10, 0x6

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v13}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x4

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v13}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x5

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 455
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 456
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Actual Query = SELECT  (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?, ?)  AND (A.START_DT < ?) "

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_5
    if-eqz v3, :cond_6

    .line 460
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 461
    const-string v8, "BEHIND"

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_a

    const/4 v1, 0x1

    .line 463
    :cond_6
    :goto_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 464
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "is behind (7 days from goal selection without committing to any missions) = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 471
    :cond_7
    if-eqz v3, :cond_8

    .line 472
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_8
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 474
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "isActiveUserGoalBehind7: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    move v2, v1

    .line 477
    .restart local v2    # "behind":I
    goto/16 :goto_0

    .line 461
    .end local v2    # "behind":I
    :cond_a
    const/4 v1, 0x0

    goto :goto_1

    .line 467
    .end local v5    # "sevenDaysBehind":J
    .end local v7    # "userId":I
    :catch_0
    move-exception v4

    .line 468
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in isActiveUserGoalBehind7: "

    invoke-static {v8, v9, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 469
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 471
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v3, :cond_b

    .line 472
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_b
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 474
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "isActiveUserGoalBehind7: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v8
.end method


# virtual methods
.method public createUserGoals(Ljava/lang/String;Ljava/util/List;)V
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserGoalData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "goalList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserGoalData;>;"
    const/4 v10, -0x1

    .line 130
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 131
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "createUserGoals: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    :cond_0
    if-eqz p2, :cond_a

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_a

    .line 135
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 136
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Processing "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " goal groups"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    .line 139
    .local v5, "userKey":I
    if-ne v5, v10, :cond_3

    .line 187
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 188
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "createUserGoals: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .end local v5    # "userKey":I
    :cond_2
    :goto_0
    return-void

    .line 142
    .restart local v5    # "userKey":I
    :cond_3
    :try_start_1
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 143
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Preparing to run query to save user responses to db"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    :cond_4
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/UserGoalData;

    .line 146
    .local v1, "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    if-eqz v1, :cond_9

    .line 147
    const/4 v2, -0x1

    .line 148
    .local v2, "goalKey":I
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v2

    .line 149
    if-eq v2, v10, :cond_8

    .line 151
    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v7

    if-le v7, v10, :cond_7

    .line 152
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 153
    .local v6, "values":Landroid/content/ContentValues;
    const-string v7, "GOAL_ID"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 154
    const-string v7, "USR_ID"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 155
    const-string v7, "LFSTYL_ASSMT_QUEST_GRP_ID"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v8

    invoke-virtual {v8}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 156
    const-string v7, "LFSTYL_ASSMT_QUEST_GRP_SCORE"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionGroupScore()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 157
    const-string v7, "LFSTYL_ASSMT_CTRGY_SCORE"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCategoryScore()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 158
    const-string v7, "START_DT"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 159
    const-string v7, "END_DT"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCompletedDate()Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 160
    const-string v7, "STAT_IND"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v8

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 161
    const-string v7, "CMPLTD_MSSN_CNT"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getNumMissionsCompleted()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 162
    const-string v7, "LAST_MSSN_CNTD_DT"

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->getLastMissionCountedDate()Ljava/util/Calendar;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 164
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "USR_GOAL"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v7

    long-to-int v4, v7

    .line 165
    .local v4, "rowId":I
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 166
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Inserting row: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 183
    .end local v1    # "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v2    # "goalKey":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "rowId":I
    .end local v5    # "userKey":I
    .end local v6    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error in createUserGoals: "

    invoke-static {v7, v8, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 185
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 187
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 188
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "createUserGoals: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v7

    .line 169
    .restart local v1    # "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v2    # "goalKey":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "userKey":I
    :cond_7
    :try_start_3
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 170
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error: received null Group Id"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 174
    :cond_8
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error: received null goal key"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 177
    .end local v2    # "goalKey":I
    :cond_9
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error: received a negative goal"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 181
    .end local v1    # "goal":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "userKey":I
    :cond_a
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "No user goals to insert"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 187
    :cond_b
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 188
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "createUserGoals: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getActiveUserGoalDetails(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;
    .locals 18
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I

    .prologue
    .line 1037
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1038
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getActiveUserGoalDetails: START"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    :cond_0
    const/4 v2, 0x0

    .line 1041
    .local v2, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1044
    .local v7, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v12, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v9

    .line 1045
    .local v9, "userKey":I
    const/4 v12, -0x1

    if-ne v9, v12, :cond_3

    .line 1117
    if-eqz v2, :cond_1

    .line 1118
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1119
    :cond_1
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1120
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getActiveUserGoalDetails: END"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v8, v7

    .line 1124
    .end local v7    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .local v8, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :goto_0
    return-object v8

    .line 1048
    .end local v8    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v7    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_3
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    const-string v13, "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID  FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.STAT_IND = ?"

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 1052
    if-nez v2, :cond_6

    .line 1117
    if-eqz v2, :cond_4

    .line 1118
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1119
    :cond_4
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1120
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getActiveUserGoalDetails: END"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v8, v7

    .end local v7    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v8    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    goto :goto_0

    .line 1054
    .end local v8    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v7    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_6
    :try_start_2
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1055
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SELECT_ALL_USER_GOALS_QUERY records = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    :cond_7
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1059
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v12

    if-nez v12, :cond_11

    .line 1061
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserGoalDataFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v7

    .line 1064
    if-eqz v7, :cond_10

    .line 1065
    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v4

    .line 1066
    .local v4, "gloalId":I
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1067
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Goal ID "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1069
    :cond_8
    const/4 v12, -0x1

    if-eq v4, v12, :cond_10

    .line 1070
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1072
    .local v11, "userMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    const/4 v6, 0x0

    .line 1074
    .local v6, "subQuery":Landroid/database/Cursor;
    :try_start_3
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    const-string v13, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.GOAL_ID = ? AND A.USR_ID = ? AND A.GOAL_SEQ_ID = ? "

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalSequenceId()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1076
    if-eqz v6, :cond_f

    .line 1078
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1079
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "SELECT_ALL_USER_MISSIONS_QUERY records = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1081
    :cond_9
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1082
    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v12

    if-nez v12, :cond_f

    .line 1084
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v10

    .line 1086
    .local v10, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-eqz v10, :cond_b

    .line 1087
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1088
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Mission ID"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/cigna/coach/db/GoalDBManager;->getUserMissionDetailsList(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v5

    .line 1092
    .local v5, "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    invoke-virtual {v10, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionDetails(Ljava/util/List;)V

    .line 1093
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/cigna/coach/db/GoalDBManager;->getMissionActivityCompletedCount(Ljava/util/List;)I

    move-result v12

    invoke-virtual {v10, v12}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionActivityCompletedTotalCount(I)V

    .line 1095
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1097
    .end local v5    # "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :cond_b
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 1101
    .end local v10    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v3

    .line 1102
    .local v3, "e":Ljava/lang/RuntimeException;
    :try_start_4
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "Error in getActiveUserGoalDetails subQuery: "

    invoke-static {v12, v13, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1103
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1105
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v12

    if-eqz v6, :cond_c

    .line 1106
    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v12
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1113
    .end local v4    # "gloalId":I
    .end local v6    # "subQuery":Landroid/database/Cursor;
    .end local v9    # "userKey":I
    .end local v11    # "userMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :catch_1
    move-exception v3

    .line 1114
    .restart local v3    # "e":Ljava/lang/RuntimeException;
    :try_start_6
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "Error in getActiveUserGoalDetails: "

    invoke-static {v12, v13, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1115
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1117
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v12

    if-eqz v2, :cond_d

    .line 1118
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1119
    :cond_d
    sget-object v13, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 1120
    sget-object v13, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "getActiveUserGoalDetails: END"

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    throw v12

    .line 1100
    .restart local v4    # "gloalId":I
    .restart local v6    # "subQuery":Landroid/database/Cursor;
    .restart local v9    # "userKey":I
    .restart local v11    # "userMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :cond_f
    :try_start_7
    invoke-virtual {v7, v11}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalMissionDetailInfoList(Ljava/util/List;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1105
    if-eqz v6, :cond_10

    .line 1106
    :try_start_8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1111
    .end local v4    # "gloalId":I
    .end local v6    # "subQuery":Landroid/database/Cursor;
    .end local v11    # "userMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    :cond_10
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_1

    .line 1117
    :cond_11
    if-eqz v2, :cond_12

    .line 1118
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1119
    :cond_12
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 1120
    sget-object v12, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getActiveUserGoalDetails: END"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    move-object v8, v7

    .line 1124
    .end local v7    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v8    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    goto/16 :goto_0
.end method

.method public getActiveUserGoalSequenceId(Ljava/lang/String;I)I
    .locals 8
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "goalId"    # I

    .prologue
    .line 1361
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1362
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getActiveUserGoalSequenceId: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1364
    :cond_0
    const/4 v0, 0x0

    .line 1366
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 1368
    .local v2, "userId":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/db/GoalDBQueries;->ACTIVE_USER_GOAL_SEQUENCE_ID:Ljava/lang/String;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1371
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1372
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 1378
    if-eqz v0, :cond_1

    .line 1379
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1380
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1381
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getActiveUserGoalSequenceId: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    :cond_2
    :goto_0
    return v3

    .line 1378
    :cond_3
    if-eqz v0, :cond_4

    .line 1379
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1380
    :cond_4
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1381
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getActiveUserGoalSequenceId: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1385
    :cond_5
    const/4 v3, -0x1

    goto :goto_0

    .line 1374
    .end local v2    # "userId":I
    :catch_0
    move-exception v1

    .line 1375
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in getActiveUserGoalSequenceId: "

    invoke-static {v3, v4, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1376
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1378
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_6

    .line 1379
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1380
    :cond_6
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1381
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getActiveUserGoalSequenceId: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v3
.end method

.method public getActiveUserGoalsCount(Ljava/lang/String;)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 1389
    const/4 v2, 0x0

    .line 1390
    .local v2, "returnValue":I
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1391
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting all Active User Goals Count: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1395
    :cond_0
    if-nez p1, :cond_2

    .line 1418
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1419
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting all Active User Goals Count: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    :cond_1
    :goto_0
    return v4

    .line 1398
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v3

    .line 1399
    .local v3, "userKey":I
    const/4 v0, 0x0

    .line 1401
    .local v0, "c":Landroid/database/Cursor;
    if-eq v3, v4, :cond_4

    .line 1403
    :try_start_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "SELECT COUNT(*) FROM USR_GOAL WHERE USR_ID =? AND STAT_IND =? "

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1405
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_3

    .line 1406
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1407
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 1413
    :cond_3
    if-eqz v0, :cond_4

    .line 1414
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1418
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1419
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Getting all Active User Goals Count: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move v4, v2

    .line 1423
    goto :goto_0

    .line 1409
    :catch_0
    move-exception v1

    .line 1410
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_3
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error getting number of active goals: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1411
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1413
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_6

    .line 1414
    :try_start_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1418
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v3    # "userKey":I
    :catchall_1
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1419
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting all Active User Goals Count: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v4
.end method

.method public getAllActiveUserGoalsAndMissions(Ljava/lang/String;Z)Ljava/util/List;
    .locals 24
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "shouldBringNonDisplayedCompleted"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 195
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "getAllActiveUserGoalsAndMissions: START"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_0
    const/4 v6, 0x0

    .line 198
    .local v6, "goalsCursor":Landroid/database/Cursor;
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v14, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    new-instance v8, Lcom/cigna/coach/db/MissionDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Lcom/cigna/coach/db/MissionDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 200
    .local v8, "missionDB":Lcom/cigna/coach/db/MissionDBManager;
    new-instance v7, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 202
    .local v7, "lsDBManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v17

    .line 203
    .local v17, "userKey":I
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 204
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getAllActiveUserGoalsAndMissions: userId:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", userKey:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 206
    :cond_1
    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_4

    .line 307
    if-eqz v6, :cond_2

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 309
    :cond_2
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 310
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "getAllActiveUserGoalsAndMissions: END"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_3
    :goto_0
    return-object v14

    .line 209
    :cond_4
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    const-string v19, "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND = ? ORDER BY A.START_DT"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 212
    if-nez v6, :cond_6

    .line 307
    if-eqz v6, :cond_5

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 309
    :cond_5
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 310
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "getAllActiveUserGoalsAndMissions: END"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 214
    :cond_6
    :try_start_2
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_7

    .line 215
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "Executing goals query"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :cond_7
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 218
    const/4 v10, 0x0

    .line 219
    .local v10, "missionsCursor":Landroid/database/Cursor;
    :cond_8
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v18

    if-nez v18, :cond_18

    .line 221
    :try_start_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v6, v1}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserGoalDataFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v15

    .line 222
    .local v15, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v12

    .line 223
    .local v12, "questionGrpId":I
    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v7, v0, v12, v1, v2}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserResponseForQuestionGroup(Ljava/lang/String;ILjava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 224
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 226
    .local v11, "missionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    if-eqz p2, :cond_10

    .line 227
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_9

    .line 228
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "Executing query: SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.GOAL_SEQ_ID = ?  AND (A.STAT_CD = ?  OR (A.STAT_CD = ? AND MSSN_CMPLT_IND = ? )) ORDER BY A.MSSN_START_DT"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    const-string v19, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.GOAL_SEQ_ID = ?  AND (A.STAT_CD = ?  OR (A.STAT_CD = ? AND MSSN_CMPLT_IND = ? )) ORDER BY A.MSSN_START_DT"

    const/16 v20, 0x5

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalSequenceId()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x3

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x4

    const-string v22, "0"

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 242
    :goto_2
    if-eqz v10, :cond_13

    .line 243
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 244
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Executing missions query for goal:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_a
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 247
    :cond_b
    :goto_3
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v18

    if-nez v18, :cond_12

    .line 248
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v16

    .line 249
    .local v16, "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->getUserMissionDetailsList(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v9

    .line 251
    .local v9, "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    if-eqz p2, :cond_c

    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v18

    sget-object v19, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual/range {v18 .. v19}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 253
    new-instance v13, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-direct {v13}, Lcom/cigna/coach/dataobjects/UserMissionData;-><init>()V

    .line 254
    .local v13, "recordToUpdate":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setGoalId(I)V

    .line 255
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionId(I)V

    .line 256
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionSeqId(I)V

    .line 257
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDisplayedToUser(Z)V

    .line 259
    move-object/from16 v0, p1

    invoke-virtual {v8, v0, v13}, Lcom/cigna/coach/db/MissionDBManager;->updateUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)I

    .line 262
    .end local v13    # "recordToUpdate":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_c
    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionDetails(Ljava/util/List;)V

    .line 263
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/cigna/coach/db/GoalDBManager;->getMissionActivityCompletedCount(Ljava/util/List;)I

    move-result v18

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionActivityCompletedTotalCount(I)V

    .line 265
    move-object/from16 v0, v16

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    .line 267
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_b

    .line 268
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Mission ID "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_3

    .line 292
    .end local v9    # "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    .end local v11    # "missionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v12    # "questionGrpId":I
    .end local v15    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v16    # "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v4

    .line 293
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_4
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "Error in getAllActiveUserGoalsAndMissions missionCursor loop: "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 294
    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 296
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v18

    if-eqz v10, :cond_d

    .line 297
    :try_start_5
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v18
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 303
    .end local v10    # "missionsCursor":Landroid/database/Cursor;
    .end local v17    # "userKey":I
    :catch_1
    move-exception v4

    .line 304
    .restart local v4    # "e":Ljava/lang/RuntimeException;
    :try_start_6
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "Error in getAllActiveUserGoalsAndMissions: "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-static {v0, v1, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 305
    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 307
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v18

    if-eqz v6, :cond_e

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 309
    :cond_e
    sget-object v19, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_f

    .line 310
    sget-object v19, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v20, "getAllActiveUserGoalsAndMissions: END"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    throw v18

    .line 235
    .restart local v10    # "missionsCursor":Landroid/database/Cursor;
    .restart local v11    # "missionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v12    # "questionGrpId":I
    .restart local v15    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v17    # "userKey":I
    :cond_10
    :try_start_7
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_11

    .line 236
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "Executing query: SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.GOAL_SEQ_ID = ?  AND A.STAT_CD = ? ORDER BY A.MSSN_START_DT"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v18

    const-string v19, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.GOAL_SEQ_ID = ?  AND A.STAT_CD = ? ORDER BY A.MSSN_START_DT"

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalSequenceId()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x2

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    goto/16 :goto_2

    .line 272
    :cond_12
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_13

    .line 273
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Found "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " available User Missions"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_13
    invoke-virtual {v15, v11}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalMissionDetailInfoList(Ljava/util/List;)V

    .line 279
    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalSequenceId(Ljava/lang/String;I)I

    move-result v5

    .line 281
    .local v5, "goalSequenceId":I
    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/cigna/coach/db/GoalDBManager;->isActiveUserGoalBehind7(Ljava/lang/String;II)Z

    move-result v18

    if-nez v18, :cond_14

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/cigna/coach/db/GoalDBManager;->isActiveUserGoalBehind14(Ljava/lang/String;II)Z

    move-result v18

    if-nez v18, :cond_14

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v5}, Lcom/cigna/coach/db/GoalDBManager;->isActiveUserGoalBehind21(Ljava/lang/String;II)Z

    move-result v18

    if-eqz v18, :cond_16

    :cond_14
    const/4 v3, 0x1

    .line 285
    .local v3, "behind":Z
    :goto_4
    if-nez v3, :cond_17

    const/16 v18, 0x1

    :goto_5
    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalOnTrack(Z)V

    .line 287
    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 289
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_15

    .line 290
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Goal ID "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 296
    :cond_15
    if-eqz v10, :cond_8

    .line 297
    :try_start_8
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 281
    .end local v3    # "behind":Z
    :cond_16
    const/4 v3, 0x0

    goto :goto_4

    .line 285
    .restart local v3    # "behind":Z
    :cond_17
    const/16 v18, 0x0

    goto :goto_5

    .line 300
    .end local v3    # "behind":Z
    .end local v5    # "goalSequenceId":I
    .end local v11    # "missionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v12    # "questionGrpId":I
    .end local v15    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_18
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_19

    .line 301
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Found "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " available User Goals"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 307
    :cond_19
    if-eqz v6, :cond_1a

    .line 308
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 309
    :cond_1a
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v18 .. v18}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 310
    sget-object v18, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v19, "getAllActiveUserGoalsAndMissions: END"

    invoke-static/range {v18 .. v19}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getAvailableGoalCount(Ljava/lang/String;)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 1128
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1129
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getAvailableGoalCount: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1131
    :cond_0
    const/4 v1, 0x0

    .line 1132
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1134
    .local v0, "count":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1135
    .local v3, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBQueries;->AVAILABLE_GOAL_COUNT:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1137
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1138
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1143
    if-eqz v1, :cond_1

    .line 1144
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1145
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1146
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getAvailableGoalCount: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    :cond_2
    return v0

    .line 1139
    .end local v3    # "userKey":I
    :catch_0
    move-exception v2

    .line 1140
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getAvailableGoalCount: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1141
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1143
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 1144
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1145
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1146
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getAvailableGoalCount: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getAvailableGoalCountByCategory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 1154
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1155
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getAvailableGoalCountByCategory: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    :cond_0
    const/4 v1, 0x0

    .line 1158
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1160
    .local v0, "count":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1161
    .local v3, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBQueries;->AVAILABLE_GOAL_SINGLE_CATEGORY_COUNT:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1164
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1165
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1170
    if-eqz v1, :cond_1

    .line 1171
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1172
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1173
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getAvailableGoalCountByCategory: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1177
    :cond_2
    return v0

    .line 1166
    .end local v3    # "userKey":I
    :catch_0
    move-exception v2

    .line 1167
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getAvailableGoalCountByCategory: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1168
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1170
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 1171
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1172
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1173
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getAvailableGoalCountByCategory: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getAvailableUserGoals(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 318
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 319
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAvailableUserGoals: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :cond_0
    const/4 v0, 0x0

    .line 322
    .local v0, "c":Landroid/database/Cursor;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 323
    .local v4, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    new-instance v2, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v7

    invoke-direct {v2, v7}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 325
    .local v2, "lsDBManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    .line 327
    .local v6, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND IN (?,?)  ORDER BY A.STAT_IND, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, E.RANK_NUM, D.RANK_NUM"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 334
    if-nez v0, :cond_3

    .line 356
    if-eqz v0, :cond_1

    .line 357
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 358
    :cond_1
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 359
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAvailableUserGoals: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :cond_2
    :goto_0
    return-object v4

    .line 336
    :cond_3
    :try_start_1
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 337
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Actual Query = SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND IN (?,?)  ORDER BY A.STAT_IND, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, E.RANK_NUM, D.RANK_NUM"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_8

    .line 341
    invoke-virtual {p0, v0, p1}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserGoalDataFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v5

    .line 342
    .local v5, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v3

    .line 343
    .local v3, "questionGrpId":I
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v2, p1, v3, v7, v8}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserResponseForQuestionGroup(Ljava/lang/String;ILjava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 344
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 346
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Goal ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :cond_5
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 352
    .end local v3    # "questionGrpId":I
    .end local v5    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v6    # "userKey":I
    :catch_0
    move-exception v1

    .line 353
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error in getAvailableUserGoals: "

    invoke-static {v7, v8, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 354
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 356
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_6

    .line 357
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 358
    :cond_6
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 359
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getAvailableUserGoals: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v7

    .line 349
    .restart local v6    # "userKey":I
    :cond_8
    :try_start_3
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 350
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Found "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " available User Goals"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 356
    :cond_9
    if-eqz v0, :cond_a

    .line 357
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 358
    :cond_a
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 359
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAvailableUserGoals: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getAvailableUserGoalsForCategory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/util/List;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 366
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 367
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAvailableUserGoalsForCategory: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :cond_0
    const/4 v0, 0x0

    .line 370
    .local v0, "c":Landroid/database/Cursor;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .local v4, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfo;>;"
    new-instance v2, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v7

    invoke-direct {v2, v7}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 373
    .local v2, "lsDBManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    .line 375
    .local v6, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND IN (?,?)  AND C.CTGRY_ID = ?  ORDER BY A.STAT_IND, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, E.RANK_NUM, D.RANK_NUM"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 383
    if-nez v0, :cond_3

    .line 405
    if-eqz v0, :cond_1

    .line 406
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 407
    :cond_1
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 408
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAvailableUserGoalsForCategory: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    :cond_2
    :goto_0
    return-object v4

    .line 385
    :cond_3
    :try_start_1
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 386
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Actual Query = SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND IN (?,?)  AND C.CTGRY_ID = ?  ORDER BY A.STAT_IND, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, E.RANK_NUM, D.RANK_NUM"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_8

    .line 390
    invoke-virtual {p0, v0, p1}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserGoalDataFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v5

    .line 391
    .local v5, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v3

    .line 392
    .local v3, "questionGrpId":I
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v2, p1, v3, v7, v8}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserResponseForQuestionGroup(Ljava/lang/String;ILjava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 393
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 395
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Goal ID "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    :cond_5
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 401
    .end local v3    # "questionGrpId":I
    .end local v5    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v6    # "userKey":I
    :catch_0
    move-exception v1

    .line 402
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error in getAvailableUserGoalsForCategory: "

    invoke-static {v7, v8, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 403
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_6

    .line 406
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 407
    :cond_6
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 408
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getAvailableUserGoalsForCategory: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v7

    .line 398
    .restart local v6    # "userKey":I
    :cond_8
    :try_start_3
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 399
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Found "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " available User Goals"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 405
    :cond_9
    if-eqz v0, :cond_a

    .line 406
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 407
    :cond_a
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 408
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAvailableUserGoalsForCategory: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getCategoriesWithCompletedGoalsCount(Ljava/lang/String;)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 1207
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1208
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCategoriesWithCompletedGoalsCount: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    :cond_0
    const/4 v1, 0x0

    .line 1211
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1213
    .local v0, "count":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1214
    .local v3, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_DISTINCT_CATEGORY_COUNT:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1217
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1218
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1223
    if-eqz v1, :cond_1

    .line 1224
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1225
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1226
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCategoriesWithCompletedGoalsCount: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    :cond_2
    return v0

    .line 1219
    .end local v3    # "userKey":I
    :catch_0
    move-exception v2

    .line 1220
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getCategoriesWithCompletedGoalsCount: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1221
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1223
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 1224
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1225
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1226
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCategoriesWithCompletedGoalsCount: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getCompletedGoalCount(Ljava/lang/String;)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 1181
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1182
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedGoalCount: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    :cond_0
    const/4 v1, 0x0

    .line 1185
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1187
    .local v0, "count":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1188
    .local v3, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_COUNT:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1190
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1191
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1196
    if-eqz v1, :cond_1

    .line 1197
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1198
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1199
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedGoalCount: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    :cond_2
    return v0

    .line 1192
    .end local v3    # "userKey":I
    :catch_0
    move-exception v2

    .line 1193
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getCompletedGoalCount: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1194
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1196
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 1197
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1198
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1199
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCompletedGoalCount: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getCompletedGoalCountByCategory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 1234
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1235
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedGoalCountByCategory: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1237
    :cond_0
    const/4 v1, 0x0

    .line 1238
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1240
    .local v0, "count":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1241
    .local v3, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_SINGLE_CATEGORY_COUNT:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1244
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1245
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1250
    if-eqz v1, :cond_1

    .line 1251
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1252
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1253
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedGoalCountByCategory: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1257
    :cond_2
    return v0

    .line 1246
    .end local v3    # "userKey":I
    :catch_0
    move-exception v2

    .line 1247
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getCompletedGoalCountByCategory: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1248
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1250
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 1251
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1252
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1253
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCompletedGoalCountByCategory: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getCompletedGoalCountBySubcategory(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;)I
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "subcategoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .prologue
    .line 1261
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1262
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedGoalCountBySubcategory: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    :cond_0
    const/4 v1, 0x0

    .line 1265
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1267
    .local v0, "count":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1268
    .local v3, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_SINGLE_SUBCATEGORY_COUNT:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->getSubcategoryTypeKey()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1271
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1272
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1277
    if-eqz v1, :cond_1

    .line 1278
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1279
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1280
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedGoalCountBySubcategory: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1284
    :cond_2
    return v0

    .line 1273
    .end local v3    # "userKey":I
    :catch_0
    move-exception v2

    .line 1274
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getCompletedGoalCountBySubcategory: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1275
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1277
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 1278
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1279
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1280
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCompletedGoalCountBySubcategory: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getCompletedGoalCountPerCategory(Ljava/lang/String;)Ljava/util/Map;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1320
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1321
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getCompletedGoalCountPerCategory: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    :cond_0
    const/4 v2, 0x0

    .line 1324
    .local v2, "cursor":Landroid/database/Cursor;
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1326
    .local v6, "result":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 1328
    .local v7, "userKey":I
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 1329
    .local v1, "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1332
    .end local v1    # "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/db/GoalDBQueries;->GET_COMPLETED_GOAL_COUNT_PER_CATEGORY:Ljava/lang/String;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1334
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1335
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1334
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1337
    .end local v0    # "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "userKey":I
    :catch_0
    move-exception v3

    .line 1338
    .local v3, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in getCompletedGoalCountPerCategory: "

    invoke-static {v8, v9, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1339
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1341
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v2, :cond_2

    .line 1342
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1343
    :cond_2
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1344
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getCompletedGoalCountPerCategory: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v8

    .line 1341
    .restart local v0    # "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v7    # "userKey":I
    :cond_4
    if-eqz v2, :cond_5

    .line 1342
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1343
    :cond_5
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1344
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getCompletedGoalCountPerCategory: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1348
    :cond_6
    return-object v6
.end method

.method public getCompletedGoalsCountBetweenDays(Ljava/lang/String;JJ)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "startDate"    # J
    .param p4, "endDate"    # J

    .prologue
    const/4 v4, -0x1

    .line 1427
    const/4 v2, 0x0

    .line 1428
    .local v2, "returnValue":I
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1429
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Get Completed Goals Count Between Days: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    if-eqz v5, :cond_1

    if-nez p1, :cond_3

    .line 1457
    :cond_1
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1458
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Get Completed Goals Count Between Days: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1462
    :cond_2
    :goto_0
    return v4

    .line 1435
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 1437
    .local v3, "userKey":I
    if-eq v3, v4, :cond_5

    .line 1438
    const/4 v0, 0x0

    .line 1440
    .local v0, "c":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "SELECT COUNT(*) FROM USR_GOAL WHERE USR_ID = ?  AND STAT_IND = ?  AND END_DT >= ?  AND END_DT <= ? "

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1444
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_4

    .line 1445
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1446
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    .line 1452
    :cond_4
    if-eqz v0, :cond_5

    .line 1453
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1457
    .end local v0    # "c":Landroid/database/Cursor;
    :cond_5
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1458
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Get Completed Goals Count Between Days: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v4, v2

    .line 1462
    goto/16 :goto_0

    .line 1448
    .restart local v0    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v1

    .line 1449
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_4
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error getting number of active goals: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1450
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1452
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_7

    .line 1453
    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1457
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v3    # "userKey":I
    :catchall_1
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1458
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Get Completed Goals Count Between Days: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v4
.end method

.method public getGoalNameContentIdForGoal(I)I
    .locals 8
    .param p1, "goalId"    # I

    .prologue
    .line 825
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 826
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getGoalNameContentIdForGoal: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    :cond_0
    const/4 v0, 0x0

    .line 829
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v2, -0x1

    .line 831
    .local v2, "returnVal":I
    :try_start_0
    const-string v3, " SELECT GOAL_CNTNT_ID FROM GOAL WHERE GOAL_ID = ? "

    .line 833
    .local v3, "sql":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v4, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 835
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 836
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_3

    .line 837
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 838
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 840
    .end local v3    # "sql":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 841
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getGoalNameContentIdForGoal: "

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 842
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 844
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_1

    .line 845
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_1
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 847
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getGoalNameContentIdForGoal: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4

    .line 844
    .restart local v3    # "sql":Ljava/lang/String;
    :cond_3
    if-eqz v0, :cond_4

    .line 845
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 847
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getGoalNameContentIdForGoal: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 851
    :cond_5
    return v2
.end method

.method public getIsUserGoalActive(Ljava/lang/String;I)Z
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I

    .prologue
    .line 91
    sget-object v1, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    sget-object v1, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "getIsUserGoalActive START"

    invoke-static {v1, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_0
    const/4 v12, 0x0

    .line 95
    .local v12, "returnVal":Z
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/db/GoalDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v13

    .line 96
    .local v13, "userIdKey":I
    const/4 v10, -0x1

    .line 98
    .local v10, "count":I
    const-string v2, "USR_GOAL"

    .line 99
    .local v2, "table":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v14, "COUNT(*)"

    aput-object v14, v3, v1

    .line 100
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, "USR_ID = ? AND GOAL_ID = ? AND STAT_IND = ? "

    .line 101
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v1

    const/4 v1, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v1

    const/4 v1, 0x2

    sget-object v14, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v14}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v1

    .line 102
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 103
    .local v6, "groupBy":Ljava/lang/String;
    const/4 v7, 0x0

    .line 104
    .local v7, "having":Ljava/lang/String;
    const/4 v8, 0x0

    .line 105
    .local v8, "orderBy":Ljava/lang/String;
    const/4 v9, 0x0

    .line 107
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 108
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 109
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 110
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 111
    if-lez v10, :cond_1

    .line 112
    const/4 v12, 0x1

    .line 121
    :cond_1
    if-eqz v9, :cond_2

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 122
    :cond_2
    sget-object v1, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 123
    sget-object v1, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getIsUserGoalActive END, isUserGoalActive:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", userId:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", goalId:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v1, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_3
    return v12

    .line 116
    :catch_0
    move-exception v11

    .line 117
    .local v11, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "Error getIsUserGoalActive: "

    invoke-static {v1, v14, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 118
    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 122
    :cond_4
    sget-object v14, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 123
    sget-object v14, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getIsUserGoalActive END, isUserGoalActive:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", userId:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", goalId:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v1
.end method

.method protected getLastAssesmentTimeForGoalCategory(II)Ljava/util/Calendar;
    .locals 9
    .param p1, "goalId"    # I
    .param p2, "userKey"    # I

    .prologue
    .line 605
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 606
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getLastAssesmentTimeForCategory: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    :cond_0
    const/4 v2, 0x0

    .line 609
    .local v2, "returnValue":Ljava/util/Calendar;
    const/4 v1, 0x0

    .line 611
    .local v1, "result":Landroid/database/Cursor;
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 612
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getLastAssesmentTimeForCategory: Goal id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", userKey:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 616
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "SELECT  B.ASSMT_UPDT_TS  FROM USR_GOAL A    INNER JOIN USR_LFSTYL_ASSMT_RESP B ON A.LFSTYL_ASSMT_QUEST_GRP_ID = B.LFSTYL_ASSMT_QUEST_GRP_ID \t\tAND A.USR_ID = B.USR_ID AND B.ANS_SRC = ?  WHERE A.GOAL_ID = ? AND A. USR_ID = ?  ORDER BY B.ASSMT_UPDT_TS DESC LIMIT 1"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/cigna/coach/LifeStyle$AnswerSource;->USER:Lcom/cigna/coach/LifeStyle$AnswerSource;

    invoke-virtual {v8}, Lcom/cigna/coach/LifeStyle$AnswerSource;->getAnswerSourceKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 620
    if-eqz v1, :cond_2

    .line 621
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 622
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_5

    .line 623
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 633
    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    .line 634
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 635
    :cond_3
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 636
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getLastAssesmentTimeForCategory: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    :cond_4
    return-object v2

    .line 625
    :cond_5
    const-wide/16 v3, 0x0

    :try_start_1
    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 629
    :catch_0
    move-exception v0

    .line 630
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in getGoalForQuestionGroup: "

    invoke-static {v3, v4, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 631
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 633
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_6

    .line 634
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 635
    :cond_6
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 636
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getLastAssesmentTimeForCategory: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v3
.end method

.method public getSubcategoriesWithCompletedGoalCountPerCategory(Ljava/lang/String;)Ljava/util/Map;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1288
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1289
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getSubcategoriesWithCompletedGoalCountPerCategory: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1291
    :cond_0
    const/4 v3, 0x0

    .line 1292
    .local v3, "cursor":Landroid/database/Cursor;
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1294
    .local v2, "counts":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 1296
    .local v7, "userKey":I
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v1, v0, v5

    .line 1297
    .local v1, "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v2, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1296
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1300
    .end local v1    # "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/db/GoalDBQueries;->SUBCATEGORIES_WITH_COMPLETED_GOAL_COUNT_PER_CATEGORY:Ljava/lang/String;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1302
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1303
    const/4 v8, 0x0

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v2, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1302
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1305
    .end local v0    # "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "userKey":I
    :catch_0
    move-exception v4

    .line 1306
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in getSubcategoriesWithCompletedGoalCountPerCategory: "

    invoke-static {v8, v9, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1307
    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1309
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v3, :cond_2

    .line 1310
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1311
    :cond_2
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1312
    sget-object v9, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getSubcategoriesWithCompletedGoalCountPerCategory: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v8

    .line 1309
    .restart local v0    # "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v7    # "userKey":I
    :cond_4
    if-eqz v3, :cond_5

    .line 1310
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1311
    :cond_5
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1312
    sget-object v8, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getSubcategoriesWithCompletedGoalCountPerCategory: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1316
    :cond_6
    return-object v2
.end method

.method public getUnalignedUserGoalForQuestionGroup(Ljava/lang/String;I)Lcom/cigna/coach/dataobjects/UserGoalData;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "qGrpId"    # I

    .prologue
    .line 643
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 644
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getUnalignedUserGoalForQuestionGroup: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    :cond_0
    new-instance v3, Lcom/cigna/coach/dataobjects/UserGoalData;

    invoke-direct {v3}, Lcom/cigna/coach/dataobjects/UserGoalData;-><init>()V

    .line 647
    .local v3, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    const/4 v2, -0x1

    .line 648
    .local v2, "existingGoalId":I
    const/4 v0, 0x0

    .line 650
    .local v0, "c1":Landroid/database/Cursor;
    :try_start_0
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 651
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting userkey"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    .line 654
    .local v4, "userKey":I
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 655
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Querying for existing goals: SELECT GOAL_ID,LFSTYL_ASSMT_QUEST_GRP_SCORE FROM USR_GOAL  WHERE LFSTYL_ASSMT_QUEST_GRP_ID=? AND USR_ID = ? AND STAT_IND IN (?,?,?)   "

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    :cond_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, " SELECT GOAL_ID,LFSTYL_ASSMT_QUEST_GRP_SCORE FROM USR_GOAL  WHERE LFSTYL_ASSMT_QUEST_GRP_ID=? AND USR_ID = ? AND STAT_IND IN (?,?,?)   "

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 666
    if-eqz v0, :cond_9

    .line 667
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 668
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_6

    .line 669
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalId(I)V

    .line 670
    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionGroupScore(I)V

    .line 671
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 672
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Return goal id is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 689
    :cond_3
    :goto_0
    if-eqz v0, :cond_4

    .line 690
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 691
    :cond_4
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 692
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getUnalignedUserGoalForQuestionGroup: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    :cond_5
    return-object v3

    .line 675
    :cond_6
    :try_start_1
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 676
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "No existing goal found for question group"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 685
    .end local v4    # "userKey":I
    :catch_0
    move-exception v1

    .line 686
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Error in getNonCompletedUserGoalForQuestionGroup: "

    invoke-static {v5, v6, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 687
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 689
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_7

    .line 690
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 691
    :cond_7
    sget-object v6, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 692
    sget-object v6, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getUnalignedUserGoalForQuestionGroup: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v5

    .line 680
    .restart local v4    # "userKey":I
    :cond_9
    :try_start_3
    invoke-virtual {v3, v2}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalId(I)V

    .line 681
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 682
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "No existing goal found for question group"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public getUserGoalStatus(I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .locals 8
    .param p1, "goalSeqId"    # I

    .prologue
    .line 1007
    sget-object v2, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1008
    sget-object v2, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getUserGoalStatus: START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1010
    :cond_0
    const/4 v1, 0x0

    .line 1011
    .local v1, "status":Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    const/4 v0, 0x0

    .line 1014
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "SELECT STAT_IND  FROM  USR_GOAL  WHERE SEQ_ID = ? "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1016
    if-eqz v0, :cond_1

    .line 1017
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1018
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1019
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1024
    :cond_1
    if-eqz v0, :cond_2

    .line 1025
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1028
    :cond_2
    sget-object v2, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1029
    sget-object v2, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getUserGoalStatus: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1033
    :cond_3
    return-object v1

    .line 1024
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_4

    .line 1025
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1028
    :cond_4
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1029
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getUserGoalStatus: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v2
.end method

.method public getUserGoalStatus(Ljava/lang/String;I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I

    .prologue
    .line 976
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 977
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getUserGoalStatus: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    :cond_0
    const/4 v1, 0x0

    .line 980
    .local v1, "status":Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    const/4 v0, 0x0

    .line 982
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 984
    .local v2, "userKey":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "SELECT STAT_IND  FROM  USR_GOAL  WHERE USR_ID = ? AND GOAL_ID = ?  ORDER BY SEQ_ID DESC LIMIT 1 "

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 986
    if-eqz v0, :cond_1

    .line 987
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 988
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_1

    .line 989
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 994
    :cond_1
    if-eqz v0, :cond_2

    .line 995
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 998
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 999
    sget-object v3, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getUserGoalStatus: END, status:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    :cond_3
    return-object v1

    .line 994
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_4

    .line 995
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 998
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 999
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getUserGoalStatus: END, status:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v3
.end method

.method public getUserGoalsWithCompletedMissions(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
    .locals 27
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 701
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 702
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getUserGoalsWithCompletedMissions: START"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    :cond_0
    const/4 v12, 0x0

    .line 705
    .local v12, "goalsCursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 706
    .local v16, "missionsCursor":Landroid/database/Cursor;
    new-instance v13, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;

    invoke-direct {v13}, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;-><init>()V

    .line 707
    .local v13, "goalsMissionsHistory":Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;
    new-instance v14, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v14, v0}, Lcom/cigna/coach/db/LifeStyleDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 708
    .local v14, "lsDBManager":Lcom/cigna/coach/db/LifeStyleDBManager;
    new-instance v8, Lcom/cigna/coach/db/GoalDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 711
    .local v8, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    :try_start_0
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 712
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Query = SELECT A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID WHERE A.USR_ID = ? AND A.STAT_IND NOT IN  (?,?,?)  ORDER BY A.END_DT DESC"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    :cond_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 716
    .local v9, "goalInfoMissionsInfos":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    invoke-virtual {v13, v9}, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->setGoalInfoMissionsInfos(Ljava/util/List;)V

    .line 717
    const/16 v21, 0xf2e

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->setCompletedGoalsContentId(I)V

    .line 718
    const/16 v21, 0xf2f

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->setCompletedMissionsContentId(I)V

    .line 720
    const/4 v4, 0x0

    .line 721
    .local v4, "completedGoals":I
    const/4 v5, 0x0

    .line 723
    .local v5, "completedMissions":I
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v19

    .line 724
    .local v19, "userKey":I
    const/16 v21, -0x1

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 725
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "No user key in the USR_INFO table: Returning empty result list"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809
    if-eqz v16, :cond_2

    .line 810
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 812
    :cond_2
    if-eqz v12, :cond_3

    .line 813
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 815
    :cond_3
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 816
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getUserGoalsWithCompletedMissions: END"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    :cond_4
    :goto_0
    return-object v13

    .line 728
    :cond_5
    :try_start_1
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 729
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v23, "user key found: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ""

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v21

    const-string v22, "SELECT A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID WHERE A.USR_ID = ? AND A.STAT_IND NOT IN  (?,?,?)  ORDER BY A.END_DT DESC"

    const/16 v23, 0x4

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v26, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v26 .. v26}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x2

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v26, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v26 .. v26}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x3

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v26, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v26 .. v26}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v21 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v12

    .line 734
    if-nez v12, :cond_9

    .line 809
    if-eqz v16, :cond_7

    .line 810
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 812
    :cond_7
    if-eqz v12, :cond_8

    .line 813
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 815
    :cond_8
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 816
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getUserGoalsWithCompletedMissions: END"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 737
    :cond_9
    :try_start_2
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 739
    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v21

    if-nez v21, :cond_18

    .line 740
    const-string v21, "SEQ_ID"

    move-object/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v21

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 743
    .local v11, "goalSeqId":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v12, v1}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserGoalDataFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;

    move-result-object v18

    .line 744
    .local v18, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionsAnswerGroup()Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v17

    .line 745
    .local v17, "questionGrpId":I
    const/16 v21, 0x0

    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move-object/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v14, v0, v1, v2, v3}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserResponseForQuestionGroup(Ljava/lang/String;ILjava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 746
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 748
    .local v6, "completedMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/db/GoalDBQueries;->SELECT_COMPLETED_USER_MISSIONS_QUERY:Ljava/lang/String;

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ""

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-virtual/range {v21 .. v23}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 750
    if-eqz v16, :cond_12

    .line 751
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    .line 752
    :goto_2
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v21

    if-nez v21, :cond_12

    .line 753
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v20

    .line 754
    .local v20, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    if-eqz v20, :cond_e

    .line 755
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->getUserMissionDetailsList(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v15

    .line 757
    .local v15, "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionDetails(Ljava/util/List;)V

    .line 758
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/cigna/coach/db/GoalDBManager;->getMissionActivityCompletedCount(Ljava/util/List;)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionActivityCompletedTotalCount(I)V

    .line 761
    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 762
    add-int/lit8 v5, v5, 0x1

    .line 764
    :cond_a
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 765
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Mission ID "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Mission Status "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentMissionStatus()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    :cond_b
    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getUserId()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Lcom/cigna/coach/db/GoalDBManager;->getIsUserGoalActive(Ljava/lang/String;I)Z

    move-result v10

    .line 770
    .local v10, "goalIsActive":Z
    if-eqz v10, :cond_d

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionStage()Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_d

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_c

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_c

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_c

    invoke-virtual/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_d

    .line 776
    :cond_c
    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionRepeatable(Z)V

    .line 778
    :cond_d
    if-eqz v6, :cond_e

    .line 779
    move-object/from16 v0, v20

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 781
    .end local v10    # "goalIsActive":Z
    .end local v15    # "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :cond_e
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    .line 805
    .end local v4    # "completedGoals":I
    .end local v5    # "completedMissions":I
    .end local v6    # "completedMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v9    # "goalInfoMissionsInfos":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .end local v11    # "goalSeqId":I
    .end local v17    # "questionGrpId":I
    .end local v18    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .end local v19    # "userKey":I
    .end local v20    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v7

    .line 806
    .local v7, "e":Ljava/lang/RuntimeException;
    :try_start_3
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Error in getUserGoalsWithCompletedMissions: "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v1, v7}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 807
    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 809
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v21

    if-eqz v16, :cond_f

    .line 810
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 812
    :cond_f
    if-eqz v12, :cond_10

    .line 813
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 815
    :cond_10
    sget-object v22, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_11

    .line 816
    sget-object v22, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v23, "getUserGoalsWithCompletedMissions: END"

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_11
    throw v21

    .line 784
    .restart local v4    # "completedGoals":I
    .restart local v5    # "completedMissions":I
    .restart local v6    # "completedMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .restart local v9    # "goalInfoMissionsInfos":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;>;"
    .restart local v11    # "goalSeqId":I
    .restart local v17    # "questionGrpId":I
    .restart local v18    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v19    # "userKey":I
    :cond_12
    if-eqz v16, :cond_13

    .line 785
    :try_start_4
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 787
    :cond_13
    if-eqz v6, :cond_17

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v21

    if-lez v21, :cond_17

    .line 789
    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v21

    sget-object v22, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_14

    .line 790
    add-int/lit8 v4, v4, 0x1

    .line 792
    :cond_14
    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalMissionDetailInfoList(Ljava/util/List;)V

    .line 793
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_15

    .line 794
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Goal ID "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Goal Status "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    :cond_15
    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 801
    :cond_16
    :goto_3
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_1

    .line 798
    :cond_17
    sget-object v21, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v21

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_16

    sget-object v21, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v21

    invoke-virtual/range {v18 .. v18}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_16

    .line 799
    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 803
    .end local v6    # "completedMissionsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;>;"
    .end local v11    # "goalSeqId":I
    .end local v17    # "questionGrpId":I
    .end local v18    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :cond_18
    invoke-virtual {v13, v4}, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->setCompletedGoalsCount(I)V

    .line 804
    invoke-virtual {v13, v5}, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->setCompletedMissionsCount(I)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 809
    if-eqz v16, :cond_19

    .line 810
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 812
    :cond_19
    if-eqz v12, :cond_1a

    .line 813
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 815
    :cond_1a
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 816
    sget-object v21, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "getUserGoalsWithCompletedMissions: END"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public updateUserGoal(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)I
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goal"    # Lcom/cigna/coach/dataobjects/UserGoalData;

    .prologue
    const/4 v4, -0x1

    .line 855
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 856
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserGoal: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Update user goal : START , goal id: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    :cond_0
    const/4 v1, -0x1

    .line 861
    .local v1, "noOfRowsUpdated":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 862
    .local v2, "userKey":I
    if-ne v2, v4, :cond_2

    .line 899
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 900
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserGoal: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    :cond_1
    :goto_0
    return v4

    .line 866
    :cond_2
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 868
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "STAT_IND"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 870
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 871
    const-string v4, "START_DT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getStartDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 874
    :cond_3
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCompletedDate()Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 875
    const-string v4, "END_DT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCompletedDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 878
    :cond_4
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getLastMissionCountedDate()Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 879
    const-string v4, "LAST_MSSN_CNTD_DT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getLastMissionCountedDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 882
    :cond_5
    const-string v4, "LFSTYL_ASSMT_QUEST_GRP_SCORE"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getQuestionGroupScore()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 883
    const-string v4, "LFSTYL_ASSMT_CTRGY_SCORE"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getCategoryScore()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 884
    const-string v4, "CMPLTD_MSSN_CNT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getNumMissionsCompleted()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 886
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "USR_GOAL"

    const-string v6, "USR_ID = ? AND GOAL_ID = ? AND STAT_IND= ?"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->getGoalId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 890
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V

    .line 892
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 893
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No of Rows Updated = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 899
    :cond_6
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 900
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserGoal: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    move v4, v1

    .line 904
    goto/16 :goto_0

    .line 895
    .end local v2    # "userKey":I
    .end local v3    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 896
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v4, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in updateUserGoal: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 897
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 899
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 900
    sget-object v5, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserGoal: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v4
.end method

.method public updateUserGoalStatus(Ljava/lang/String;ILcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .param p3, "goalStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .prologue
    const/4 v6, 0x0

    .line 908
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 909
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "updateUserGoalStatus: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Update user goal status: START , status = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    :cond_0
    const/4 v1, -0x1

    .line 914
    .local v1, "noOfRowsUpdated":I
    if-nez p3, :cond_2

    .line 968
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 969
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "updateUserGoalStatus: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    :cond_1
    :goto_0
    return v6

    .line 917
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 918
    .local v2, "userKey":I
    const/4 v6, -0x1

    if-ne v2, v6, :cond_3

    .line 919
    const/4 v6, -0x1

    .line 968
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 969
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "updateUserGoalStatus: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 921
    :cond_3
    :try_start_1
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 922
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "USR_ID = ? AND GOAL_ID = ? "

    .line 923
    .local v4, "whereClause":Ljava/lang/String;
    const/4 v5, 0x0

    .line 925
    .local v5, "whereClauseArgs":[Ljava/lang/String;
    const-string v6, "STAT_IND"

    invoke-virtual {p3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 927
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p3, v6, :cond_7

    .line 928
    const-string v6, "END_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 929
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "STAT_IND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "= ?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 930
    const/4 v6, 0x3

    new-array v5, v6, [Ljava/lang/String;

    .end local v5    # "whereClauseArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .line 958
    .restart local v5    # "whereClauseArgs":[Ljava/lang/String;
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "USR_GOAL"

    invoke-virtual {v6, v7, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 959
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V

    .line 961
    sget-object v6, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 962
    sget-object v6, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No of Rows Updated = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 968
    :cond_5
    sget-object v6, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 969
    sget-object v6, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "updateUserGoalStatus: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    move v6, v1

    .line 972
    goto/16 :goto_0

    .line 932
    :cond_7
    :try_start_2
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p3, v6, :cond_8

    .line 933
    const-string v6, "END_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 934
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "STAT_IND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " = ?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 935
    const/4 v6, 0x3

    new-array v5, v6, [Ljava/lang/String;

    .end local v5    # "whereClauseArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .restart local v5    # "whereClauseArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 937
    :cond_8
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p3, v6, :cond_9

    .line 938
    const-string v6, "END_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 939
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "STAT_IND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " IN (?,?,?)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 940
    const/4 v6, 0x5

    new-array v5, v6, [Ljava/lang/String;

    .end local v5    # "whereClauseArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .restart local v5    # "whereClauseArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 942
    :cond_9
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p3, v6, :cond_a

    .line 943
    const-string v6, "START_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 944
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "STAT_IND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " IN (?,?)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 945
    const/4 v6, 0x4

    new-array v5, v6, [Ljava/lang/String;

    .end local v5    # "whereClauseArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .restart local v5    # "whereClauseArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 947
    :cond_a
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p3, v6, :cond_b

    .line 948
    const-string v6, "END_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 949
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "STAT_IND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " IN (?,?)"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 950
    const/4 v6, 0x4

    new-array v5, v6, [Ljava/lang/String;

    .end local v5    # "whereClauseArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    .restart local v5    # "whereClauseArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 952
    :cond_b
    sget-object v6, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p3, v6, :cond_4

    .line 953
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "STAT_IND"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "= ?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 954
    const-string v6, "END_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 955
    const/4 v6, 0x3

    new-array v5, v6, [Ljava/lang/String;

    .end local v5    # "whereClauseArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .restart local v5    # "whereClauseArgs":[Ljava/lang/String;
    goto/16 :goto_1

    .line 964
    .end local v2    # "userKey":I
    .end local v3    # "values":Landroid/content/ContentValues;
    .end local v4    # "whereClause":Ljava/lang/String;
    .end local v5    # "whereClauseArgs":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 965
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_3
    sget-object v6, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Error in updateUserGoalStatus: "

    invoke-static {v6, v7, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 966
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 968
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 969
    sget-object v7, Lcom/cigna/coach/db/GoalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "updateUserGoalStatus: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v6
.end method
