.class public Lcom/cigna/coach/db/UserDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "UserDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final SELECT_DEFAULT_USER_ID_TXT_QUERY:Ljava/lang/String; = " SELECT USR_ID_TXT FROM USR_INFO"

.field private static final SELECT_USER_COUNT:Ljava/lang/String; = " SELECT COUNT(*) FROM USR_INFO"

.field private static final SELECT_USER_ID_QUERY:Ljava/lang/String; = " SELECT USR_ID FROM USR_INFO WHERE USR_ID_TXT = ?"

.field private static final SELECT_USER_ID_TXT_QUERY:Ljava/lang/String; = " SELECT USR_ID_TXT FROM USR_INFO WHERE USR_ID = ?"

.field public static final SINGLE_USER_ID:I = 0x1

.field public static final SINGLE_USER_ID_TXT:Ljava/lang/String; = "_default"

.field private static userNameToId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/cigna/coach/db/UserDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/db/UserDBManager;->userNameToId:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 65
    return-void
.end method

.method private static basicGetUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 6
    .param p0, "sqlDatabase"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 161
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 162
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Checking for user"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_0
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->userNameToId:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 166
    .local v2, "userKeyFromCache":Ljava/lang/Integer;
    if-eqz v2, :cond_3

    .line 167
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 168
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "User exists in cache: with id "

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 192
    :cond_2
    :goto_0
    return v1

    .line 172
    :cond_3
    const-string v3, " SELECT USR_ID FROM USR_INFO WHERE USR_ID_TXT = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 174
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 175
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_5

    .line 176
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 177
    .local v1, "userId":I
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->userNameToId:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 179
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "User exists: Adding to cache"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :cond_4
    if-eqz v0, :cond_2

    .line 185
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 184
    .end local v1    # "userId":I
    :cond_5
    if-eqz v0, :cond_6

    .line 185
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 189
    :cond_6
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 190
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "No such user: "

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_7
    const/4 v1, -0x1

    goto :goto_0

    .line 184
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_8

    .line 185
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v3
.end method

.method public static clearCache()V
    .locals 1

    .prologue
    .line 358
    sget-object v0, Lcom/cigna/coach/db/UserDBManager;->userNameToId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 359
    return-void
.end method

.method public static getUserCount(Landroid/database/sqlite/SQLiteDatabase;)I
    .locals 4
    .param p0, "sqlDatabase"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 205
    invoke-static {}, Lcom/cigna/coach/settings/CoachConfig;->getConfig()Lcom/cigna/coach/settings/CoachConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/settings/CoachConfig;->multipleUsersAllowed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 206
    const/4 v0, 0x1

    .line 217
    :cond_0
    :goto_0
    return v0

    .line 209
    :cond_1
    const/4 v1, 0x0

    .line 211
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v2, " SELECT COUNT(*) FROM USR_INFO"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 212
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 213
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 216
    .local v0, "count":I
    if-eqz v1, :cond_0

    .line 217
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 216
    .end local v0    # "count":I
    :catchall_0
    move-exception v2

    if-eqz v1, :cond_2

    .line 217
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2
.end method

.method public static getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    .locals 5
    .param p0, "sqlDatabase"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 232
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 233
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getUserId: START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_0
    const/4 v1, -0x1

    .line 236
    .local v1, "userId":I
    invoke-static {}, Lcom/cigna/coach/settings/CoachConfig;->getConfig()Lcom/cigna/coach/settings/CoachConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/settings/CoachConfig;->multipleUsersAllowed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 237
    const/4 v1, 0x1

    .line 238
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 239
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Multi-user support disabled. User id"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_1
    if-gez v1, :cond_2

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 243
    const/4 v1, 0x1

    .line 244
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 245
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Blank username requested. Skipping cache and DB checks and answering "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_2
    if-gez v1, :cond_3

    .line 249
    invoke-static {p0, p1}, Lcom/cigna/coach/db/UserDBManager;->basicGetUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v1

    .line 251
    :cond_3
    if-gez v1, :cond_5

    .line 252
    invoke-static {p0}, Lcom/cigna/coach/db/UserDBManager;->getUserCount(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v0

    .line 253
    .local v0, "userCount":I
    invoke-static {p0}, Lcom/cigna/coach/db/UserDBManager;->getUserCount(Landroid/database/sqlite/SQLiteDatabase;)I

    move-result v2

    if-nez v2, :cond_9

    .line 254
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 255
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "No users present. Adding user "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_4
    const/4 v2, 0x1

    invoke-static {p0, p1, v2}, Lcom/cigna/coach/db/UserDBManager;->insertUserInformation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V

    .line 258
    invoke-static {p0, p1}, Lcom/cigna/coach/db/UserDBManager;->basicGetUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v1

    .line 265
    .end local v0    # "userCount":I
    :cond_5
    :goto_0
    if-gez v1, :cond_6

    const-string v2, "_default"

    invoke-static {p0, v2, p1}, Lcom/cigna/coach/db/UserDBManager;->updateUserName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 266
    invoke-static {p0, p1}, Lcom/cigna/coach/db/UserDBManager;->basicGetUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v1

    .line 268
    :cond_6
    if-gez v1, :cond_7

    const-string v2, ""

    invoke-static {p0, v2, p1}, Lcom/cigna/coach/db/UserDBManager;->updateUserName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 269
    invoke-static {p0, p1}, Lcom/cigna/coach/db/UserDBManager;->basicGetUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v1

    .line 271
    :cond_7
    if-gez v1, :cond_a

    .line 272
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 273
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "User could not be found or created: "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getUserId: FAILED"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_8
    new-instance v2, Lcom/cigna/coach/exceptions/CoachUserNotFoundException;

    invoke-direct {v2, p1}, Lcom/cigna/coach/exceptions/CoachUserNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 260
    .restart local v0    # "userCount":I
    :cond_9
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 261
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " users already in database. Not adding user "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 279
    .end local v0    # "userCount":I
    :cond_a
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 280
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Resolved username "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getUserId: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_b
    return v1
.end method

.method private static insertUserInformation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    .locals 5
    .param p0, "sqLiteDatabase"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "userName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .prologue
    .line 370
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 371
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserInformation : START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    :cond_0
    :try_start_0
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 375
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Preparing to insert  user information to database"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 379
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "USR_ID"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 380
    const-string v2, "USR_ID_TXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v2, "USR_INFO_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 383
    const-string v2, "USR_INFO"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 384
    invoke-static {}, Lcom/cigna/coach/db/UserDBManager;->clearCache()V

    .line 385
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 386
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Insert completed"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    :cond_2
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 393
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserInformation: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_3
    return-void

    .line 388
    .end local v1    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 389
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserInformation: "

    invoke-static {v2, v3, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 390
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 392
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 393
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "insertUserInformation: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v2
.end method

.method private static updateUserName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p0, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "currentUserName"    # Ljava/lang/String;
    .param p2, "newUserName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 411
    sget-object v6, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 412
    sget-object v6, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "updateUserName: START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :cond_0
    invoke-static {p0, p1}, Lcom/cigna/coach/db/UserDBManager;->basicGetUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 416
    .local v2, "userKey":I
    if-gez v2, :cond_1

    .line 434
    :goto_0
    return v5

    .line 418
    :cond_1
    :try_start_0
    invoke-static {}, Lcom/cigna/coach/db/UserDBManager;->clearCache()V

    .line 419
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 420
    .local v3, "values":Landroid/content/ContentValues;
    const-string v6, "USR_ID_TXT"

    invoke-virtual {v3, v6, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v6, "USR_INFO"

    const-string v7, "USR_ID = ? "

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v6, v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 430
    .local v1, "noOfRowsUpdated":I
    sget-object v6, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 431
    sget-object v6, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "updateUserId: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :cond_2
    if-ne v1, v4, :cond_4

    :goto_1
    move v5, v4

    goto :goto_0

    .line 426
    .end local v1    # "noOfRowsUpdated":I
    .end local v3    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 427
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in updateUserMissionWithMissionFailedReason: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 428
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 430
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 431
    sget-object v5, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserId: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v4

    .restart local v1    # "noOfRowsUpdated":I
    .restart local v3    # "values":Landroid/content/ContentValues;
    :cond_4
    move v4, v5

    .line 434
    goto :goto_1
.end method


# virtual methods
.method public getDefaultUserId()Ljava/lang/String;
    .locals 5

    .prologue
    .line 331
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 332
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getDefaultUserId: START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, " SELECT USR_ID_TXT FROM USR_INFO"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 336
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 337
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_4

    .line 338
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, "userId":Ljava/lang/String;
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 340
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "User key set to "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 347
    :cond_1
    if-eqz v0, :cond_2

    .line 348
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_2
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 351
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getDefaultUserId: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    .end local v1    # "userId":Ljava/lang/String;
    :cond_3
    :goto_0
    return-object v1

    .line 344
    :cond_4
    :try_start_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "_default"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/cigna/coach/db/UserDBManager;->insertUserInformation(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 347
    if-eqz v0, :cond_5

    .line 348
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_5
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 351
    sget-object v2, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getDefaultUserId: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_6
    const-string v1, "_default"

    goto :goto_0

    .line 347
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_7

    .line 348
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_7
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 351
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getDefaultUserId: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v2
.end method

.method public getUserName(I)Ljava/lang/String;
    .locals 8
    .param p1, "userId"    # I

    .prologue
    .line 296
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 297
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getUserName: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_0
    const-string v2, ""

    .line 300
    .local v2, "userName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 302
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, " SELECT USR_ID_TXT FROM USR_INFO WHERE USR_ID = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 303
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 304
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_1

    .line 305
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 306
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 307
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "User Id set to "

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    :cond_1
    if-eqz v0, :cond_2

    .line 315
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 317
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 318
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getUserName: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :cond_3
    return-object v2

    .line 310
    :catch_0
    move-exception v1

    .line 311
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getUserName: "

    invoke-static {v3, v4, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 312
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_4

    .line 315
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 317
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 318
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getUserName: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v3
.end method

.method public resetAllUserEntries()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 75
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 77
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Beginning to clear all user entries "

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    invoke-static {}, Lcom/cigna/coach/db/UserDBManager;->clearCache()V

    .line 83
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->getTablesToReset(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 85
    .local v0, "allUserTableNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 86
    .local v3, "table":Ljava/lang/String;
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 87
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deleting data from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4, v3, v7, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 92
    .local v2, "numRowsDeleted":I
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 93
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " rows deleted from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    .end local v2    # "numRowsDeleted":I
    .end local v3    # "table":Ljava/lang/String;
    :cond_3
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 98
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "All user data reset"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_4
    return-void
.end method

.method public resetUserEntries()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 108
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 109
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 110
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Beginning to clear all user entries except metrics"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->getTablesToReset(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 120
    .local v0, "allUserTableNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 122
    .local v3, "table":Ljava/lang/String;
    const-string v4, "USR_INFO"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "USR_METRCS"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 128
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 129
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deleting data from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v4, v3, v7, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 134
    .local v2, "numRowsDeleted":I
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 135
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " rows deleted from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    .end local v2    # "numRowsDeleted":I
    .end local v3    # "table":Ljava/lang/String;
    :cond_3
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 140
    sget-object v4, Lcom/cigna/coach/db/UserDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "All user data reset except metrics"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_4
    return-void
.end method

.method public updateUserName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "currentUserName"    # Ljava/lang/String;
    .param p2, "newUserName"    # Ljava/lang/String;

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/cigna/coach/db/UserDBManager;->updateUserName(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
