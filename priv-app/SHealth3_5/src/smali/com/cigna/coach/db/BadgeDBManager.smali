.class public Lcom/cigna/coach/db/BadgeDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "BadgeDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/cigna/coach/db/BadgeDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/BadgeDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 56
    return-void
.end method


# virtual methods
.method public getAllBadges()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/cigna/coach/db/BadgeDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/BadgeDBQueries;->ALL_BADGES:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 165
    .local v3, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 168
    .local v1, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 169
    .end local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .local v2, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_1

    .line 170
    new-instance v0, Lcom/cigna/coach/dataobjects/BadgeData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/BadgeData;-><init>()V

    .line 171
    .local v0, "badgeData":Lcom/cigna/coach/dataobjects/BadgeData;
    const-string v4, "BADGE_ID"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/BadgeData;->setBadgeId(I)V

    .line 172
    const-string v4, "BADGE_CNTNT_ID"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/BadgeData;->setContentId(I)V

    .line 173
    const-string v4, "BADGE_DESC_CNTNT_ID"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/BadgeData;->setDescriptionContentId(I)V

    .line 174
    const-string v4, "BADGE_IMG_CNTNT_ID"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/BadgeData;->setImageContentId(I)V

    .line 175
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 179
    .end local v0    # "badgeData":Lcom/cigna/coach/dataobjects/BadgeData;
    :catchall_0
    move-exception v4

    move-object v1, v2

    .end local v2    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :goto_1
    if-eqz v3, :cond_0

    .line 180
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v4

    .line 179
    .end local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v2    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :cond_1
    if-eqz v3, :cond_2

    .line 180
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 183
    :cond_2
    return-object v2

    .line 179
    .end local v2    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :catchall_1
    move-exception v4

    goto :goto_1
.end method

.method public getBadgesById([Ljava/lang/Integer;)Ljava/util/List;
    .locals 12
    .param p1, "ids"    # [Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    array-length v7, p1

    new-array v5, v7, [Ljava/lang/String;

    .line 66
    .local v5, "selectionArgs":[Ljava/lang/String;
    array-length v6, v5

    .line 67
    .local v6, "slectionArgSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v6, :cond_0

    .line 68
    aget-object v7, p1, v4

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v4

    .line 67
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 71
    :cond_0
    sget-object v7, Lcom/cigna/coach/db/BadgeDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 72
    sget-object v7, Lcom/cigna/coach/db/BadgeDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Getting badges with IDs in [%s]."

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, ", "

    invoke-static {v11, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/BadgeDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    array-length v8, p1

    invoke-static {v8}, Lcom/cigna/coach/db/BadgeDBQueries;->badgesById(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 75
    .local v3, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 78
    .local v1, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-direct {v2, v7}, Ljava/util/ArrayList;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 79
    .end local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .local v2, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_3

    .line 80
    new-instance v0, Lcom/cigna/coach/dataobjects/BadgeData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/BadgeData;-><init>()V

    .line 81
    .local v0, "badgeData":Lcom/cigna/coach/dataobjects/BadgeData;
    const-string v7, "BADGE_ID"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setBadgeId(I)V

    .line 82
    const-string v7, "BADGE_CNTNT_ID"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setContentId(I)V

    .line 83
    const-string v7, "BADGE_IMG_CNTNT_ID"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setImageContentId(I)V

    .line 84
    const-string v7, "BADGE_DESC_CNTNT_ID"

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v3, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setDescriptionContentId(I)V

    .line 85
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 88
    .end local v0    # "badgeData":Lcom/cigna/coach/dataobjects/BadgeData;
    :catchall_0
    move-exception v7

    move-object v1, v2

    .end local v2    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :goto_2
    if-eqz v3, :cond_2

    .line 89
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v7

    .line 88
    .end local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v2    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :cond_3
    if-eqz v3, :cond_4

    .line 89
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 93
    :cond_4
    return-object v2

    .line 88
    .end local v2    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    :catchall_1
    move-exception v7

    goto :goto_2
.end method

.method public getCountOfAllBadges()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-virtual {p0}, Lcom/cigna/coach/db/BadgeDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    sget-object v3, Lcom/cigna/coach/db/BadgeDBQueries;->COUNT_OF_ALL_BADGES:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 105
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 106
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_1

    .line 107
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 111
    if-eqz v0, :cond_0

    .line 112
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_0
    return v1

    .line 111
    :cond_1
    if-eqz v0, :cond_0

    .line 112
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 111
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_2

    .line 112
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method

.method public getEarnedBadgesForUser(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "username"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Badge;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/BadgeDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 134
    .local v6, "userId":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/BadgeDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/db/BadgeDBQueries;->ALL_BADGES_USER_OWNS:Ljava/lang/String;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 137
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 138
    .local v1, "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_2

    .line 139
    new-instance v0, Lcom/cigna/coach/dataobjects/BadgeData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/BadgeData;-><init>()V

    .line 140
    .local v0, "badgeData":Lcom/cigna/coach/dataobjects/BadgeData;
    const-string v7, "BADGE_ID"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setBadgeId(I)V

    .line 141
    const-string v7, "BADGE_CNTNT_ID"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setContentId(I)V

    .line 142
    const-string v7, "BADGE_DESC_CNTNT_ID"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setDescriptionContentId(I)V

    .line 143
    const-string v7, "BADGE_IMG_CNTNT_ID"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setImageContentId(I)V

    .line 144
    const-string v7, "CMPLT_DT"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 145
    .local v4, "timestamp":J
    invoke-static {v4, v5}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/BadgeData;->setBadgeEarnedDate(Ljava/util/Calendar;)V

    .line 146
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    .end local v0    # "badgeData":Lcom/cigna/coach/dataobjects/BadgeData;
    .end local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .end local v4    # "timestamp":J
    :catchall_0
    move-exception v7

    if-eqz v2, :cond_0

    .line 152
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_0
    throw v7

    .line 129
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v6    # "userId":I
    :catch_0
    move-exception v3

    .line 130
    .local v3, "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    sget-object v7, Lcom/cigna/coach/db/BadgeDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Unable to resolve username in getEarnedBadgesForUser."

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 152
    .end local v3    # "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    :cond_1
    :goto_1
    return-object v1

    .line 151
    .restart local v1    # "badges":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Badge;>;"
    .restart local v2    # "cursor":Landroid/database/Cursor;
    .restart local v6    # "userId":I
    :cond_2
    if-eqz v2, :cond_1

    .line 152
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method public saveBadgesToUser(Ljava/lang/String;Ljava/util/List;Ljava/util/Calendar;)Ljava/util/List;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "when"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    .local p2, "badgeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/cigna/coach/db/BadgeDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 199
    .local v3, "userKey":I
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 201
    .local v2, "inserted":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 202
    .local v0, "badgeId":I
    new-instance v4, Landroid/content/ContentValues;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 203
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "BADGE_ID"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    const-string v5, "USR_ID"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 205
    const-string v5, "CMPLT_DT"

    invoke-static {p3}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 206
    const-wide/16 v5, -0x1

    invoke-virtual {p0}, Lcom/cigna/coach/db/BadgeDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, "USR_BADGE"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    .line 207
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 211
    .end local v0    # "badgeId":I
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_1
    return-object v2
.end method
