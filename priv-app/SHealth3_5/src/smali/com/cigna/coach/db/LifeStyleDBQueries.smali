.class public final Lcom/cigna/coach/db/LifeStyleDBQueries;
.super Ljava/lang/Object;
.source "LifeStyleDBQueries.java"


# static fields
.field static final CONSECUTIVE_ASSESSMENT_SCORES:Ljava/lang/String;

.field static final CONSECUTIVE_LIFESTYLE_SCORES:Ljava/lang/String;

.field public static final GET_LIFETIME_SCORE_HISTORY:Ljava/lang/String; = " SELECT LFSTYL_ASSMT_TIMESTMP, SCORE_TY_IND, SCORE_VAL FROM  LFSTYL_ASSMT_SCORE WHERE USR_ID = ? AND (SCORE_TY_IND = ?  OR SCORE_TY_IND= ?  ) ORDER BY LFSTYL_ASSMT_TIMESTMP ASC"

.field static final GET_NUMBER_OF_SUBCATEGORIES_PER_CATEGORY:Ljava/lang/String;

.field static final GetCategoryOfQuestionGroup_SELECT:Ljava/lang/String; = " SELECT CTGRY_ID FROM LFSTYL_ASSMT_QUEST_GRP WHERE LFSTYL_ASSMT_QUEST_GRP_ID = ?"

.field static final GetLifeStyleAssesmentQA_ORDER_BY:Ljava/lang/String; = " ORDER BY     A.CTGRY_ID,     A.LFSTYL_ASSMT_QUEST_GRP_ID,     A.DISPL_ORDR_NUM,     B.LFSTYL_ASSMT_QUEST_ID,    B.DISPL_ORDR_NUM,     C.DISPL_ORDR_NUM"

.field static final GetLifeStyleAssesmentQA_SELECT:Ljava/lang/String; = " SELECT  \tA.CTGRY_ID, \tA.LFSTYL_ASSMT_QUEST_GRP_ID, \tA.CNTNT_ID AS A_CNTNT_ID, \tA.DISPL_ORDR_NUM AS A_DISPL_ORDR_NUM, \tB.LFSTYL_ASSMT_QUEST_ID, \tB.CNTNT_ID AS B_CNTNT_ID, \tB.ANS_TY_IND, \tB.DISPL_ORDR_NUM AS B_DISPL_ORDR_NUM, \tC.LFSTYL_ASSMT_ANS_ID, \tC.CNTNT_ID AS C_CNTNT_ID, \tC.DISPL_ORDR_NUM AS C_DISPL_ORDR_NUM,   A.IMG_CNTNT_ID AS A_IMG_CNTNT_ID,   B.IMG_CNTNT_ID AS B_IMG_CNTNT_ID,   C.IMG_CNTNT_ID AS C_IMG_CNTNT_ID,   C.ANS_SMMRY_CNTNT_ID AS ANS_SMMRY_CNTNT_ID FROM LFSTYL_ASSMT_QUEST_GRP A  INNER JOIN LFSTYL_ASSMT_QUEST B ON    A.LFSTYL_ASSMT_QUEST_GRP_ID = B.LFSTYL_ASSMT_QUEST_GRP_ID  LEFT OUTER JOIN LFSTYL_ASSMT_ANS C ON    B.LFSTYL_ASSMT_QUEST_GRP_ID = C.LFSTYL_ASSMT_QUEST_GRP_ID AND   B.LFSTYL_ASSMT_QUEST_ID = C.LFSTYL_ASSMT_QUEST_ID"

.field static final GetLifeStyleAssesmentQA_WHERE:Ljava/lang/String; = " WHERE    A.ACTV_IND = 1 AND   B.ACTV_IND = 1 AND   (C.ACTV_IND IS null OR C.ACTV_IND = 1)"

.field static final GetNextOptimalAnswerForUser_SELECT_QUERY_FOR_NEXT_ANSWER:Ljava/lang/String; = " SELECT LFSTYL_ASSMT_ANS_ID, LFSTYL_ASSMT_QUEST_ID FROM LFSTYL_ASSMT_ANS  WHERE LFSTYL_ASSMT_QUEST_GRP_ID=? AND  LFSTYL_ASSMT_ANS_PRGRS_SEQ_NUM > (   SELECT LFSTYL_ASSMT_ANS_PRGRS_SEQ_NUM FROM LFSTYL_ASSMT_ANS    WHERE LFSTYL_ASSMT_ANS_ID = ? )  ORDER BY LFSTYL_ASSMT_ANS_PRGRS_SEQ_NUM LIMIT 1"

.field static final GetNextOptimalAnswerForUser_SELECT_QUERY_FOR_USER_RESPONSE:Ljava/lang/String; = " SELECT B.RESP_REF FROM USR_LFSTYL_ASSMT_RESP B  INNER JOIN  USR_GOAL A ON B.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID  AND B.USR_ID = A.USR_ID  WHERE B.USR_ID=? AND A.GOAL_ID = ? AND B.ANS_TY_IND = ?  ORDER BY B.ASSMT_UPDT_TS DESC LIMIT 1"

.field static final GetUserLifeStyleAssesmentResponses_ORDERBY_LIMIT_CLAUSE:Ljava/lang/String; = " ORDER BY A.ASSMT_UPDT_TS DESC LIMIT 1 "

.field static final GetUserLifeStyleAssesmentResponses_SELECT_QUESTION_FOR_QGRP:Ljava/lang/String; = " SELECT LFSTYL_ASSMT_QUEST_ID FROM LFSTYL_ASSMT_QUEST WHERE LFSTYL_ASSMT_QUEST_GRP_ID = ?"

.field static final GetUserLifeStyleAssesmentResponses_SELECT_QUESTION_GROUP_FOR_CATEGORY:Ljava/lang/String; = " SELECT LFSTYL_ASSMT_QUEST_GRP_ID FROM LFSTYL_ASSMT_QUEST_GRP "

.field static final GetUserLifeStyleAssesmentResponses_SELECT_USER_RESPONSE_FOR_QUESTION:Ljava/lang/String; = " SELECT A.ANS_TY_IND, A.RESP_REF, A.RESP_TXT, B.ANS_SMMRY_CNTNT_ID, A.ANS_SRC FROM USR_LFSTYL_ASSMT_RESP A \tLEFT OUTER JOIN LFSTYL_ASSMT_ANS B ON A.RESP_REF = B.LFSTYL_ASSMT_ANS_ID "

.field static final GetUserLifeStyleAssesmentResponses_WHERE_CLAUSE:Ljava/lang/String; = " WHERE A.LFSTYL_ASSMT_QUEST_GRP_ID = ? AND        A.LFSTYL_ASSMT_QUEST_ID = ? AND\t\tA.USR_ID = ? AND        A.ASSMT_UPDT_TS <= ? "

.field static final GetUserOverallScore_SELECT_CLAUSE_FOR_FINDING_SCORE:Ljava/lang/String; = " SELECT   A.SCORE_VAL, A.LFSTYL_ASSMT_TIMESTMP  FROM LFSTYL_ASSMT_SCORE A WHERE    USR_ID = ? AND    (SCORE_TY_IND = ? OR SCORE_TY_IND = ?) AND    LFSTYL_ASSMT_TIMESTMP <= ? ORDER BY A.LFSTYL_ASSMT_TIMESTMP DESC LIMIT 1"

.field static final GetUserScoreForCategory_SELECT:Ljava/lang/String; = " SELECT   A.SCORE_VAL  FROM LFSTYL_ASSMT_SCORE A WHERE    USR_ID = ? AND    (SCORE_TY_IND = ? OR SCORE_TY_IND = ?) AND    SCORE_TY_ID = ? AND    LFSTYL_ASSMT_TIMESTMP <= ? ORDER BY A.LFSTYL_ASSMT_TIMESTMP DESC LIMIT 1"

.field static final PER_CATEGORY_BEST_EVER_SCORES:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 130
    const-string v0, "SELECT %s, MAX(%s) FROM %s WHERE (%s = %s OR %s = %s) AND %s = ? GROUP BY %s"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "SCORE_TY_ID"

    aput-object v2, v1, v4

    const-string v2, "SCORE_VAL"

    aput-object v2, v1, v5

    const-string v2, "LFSTYL_ASSMT_SCORE"

    aput-object v2, v1, v6

    const-string v2, "SCORE_TY_IND"

    aput-object v2, v1, v7

    sget-object v2, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "SCORE_TY_IND"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "SCORE_TY_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/LifeStyleDBQueries;->PER_CATEGORY_BEST_EVER_SCORES:Ljava/lang/String;

    .line 143
    const-string v0, "SELECT %s, COUNT(*) FROM %s GROUP BY %s"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "CTGRY_ID"

    aput-object v2, v1, v4

    const-string v2, "SUB_CTGRY"

    aput-object v2, v1, v5

    const-string v2, "CTGRY_ID"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/LifeStyleDBQueries;->GET_NUMBER_OF_SUBCATEGORIES_PER_CATEGORY:Ljava/lang/String;

    .line 150
    const-string v0, "SELECT %s, %s FROM %s WHERE (%s = %s OR %s = %s) AND %s = ? ORDER BY %s ASC"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "SCORE_VAL"

    aput-object v2, v1, v4

    const-string v2, "LFSTYL_ASSMT_TIMESTMP"

    aput-object v2, v1, v5

    const-string v2, "LFSTYL_ASSMT_SCORE"

    aput-object v2, v1, v6

    const-string v2, "SCORE_TY_IND"

    aput-object v2, v1, v7

    sget-object v2, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v2}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "SCORE_TY_IND"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v3}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "LFSTYL_ASSMT_TIMESTMP"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/LifeStyleDBQueries;->CONSECUTIVE_ASSESSMENT_SCORES:Ljava/lang/String;

    .line 163
    const-string v0, "SELECT %s, %s, %s, %s FROM %s WHERE %s = ? ORDER BY %s DESC"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "SCORE_VAL"

    aput-object v2, v1, v4

    const-string v2, "SCORE_TY_IND"

    aput-object v2, v1, v5

    const-string v2, "SCORE_TY_ID"

    aput-object v2, v1, v6

    const-string v2, "LFSTYL_ASSMT_TIMESTMP"

    aput-object v2, v1, v7

    const-string v2, "LFSTYL_ASSMT_SCORE"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "LFSTYL_ASSMT_TIMESTMP"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/LifeStyleDBQueries;->CONSECUTIVE_LIFESTYLE_SCORES:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
