.class public Lcom/cigna/coach/db/MissionDBQueries;
.super Ljava/lang/Object;
.source "MissionDBQueries.java"


# static fields
.field static final COMPLETED_MISSION_AFTER_FAILURE_COUNT:Ljava/lang/String;

.field static final COMPLETED_MISSION_COUNT:Ljava/lang/String;

.field static final COMPLETED_MISSION_COUNT_BY_STAGE:Ljava/lang/String;

.field static final COMPLETED_MISSION_FREQUENCIES:Ljava/lang/String;

.field static final GET_COMPLETED_MISSION_COUNT_PER_STAGE:Ljava/lang/String;

.field static final GetActiveUserMissionDetails_SELECT_ACTIVE_USER_MISSIONS_QUERY:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.STAT_CD = ? AND A.MSSN_ID = ?"

.field static final GetAvailableUserMissionsForGoal_SELECT_AVAILABLE_USER_MISSIONS_FOR_GOAL_QUERY:Ljava/lang/String; = "SELECT  A.DEFLT_FREQ_IND, A.FREQ_START_RNG_NUM, A.FREQ_END_RNG_NUM, B.MSSN_ID, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_FREQ_CNTNT_ID, B.MSSN_STG_ID, B.DLY_TRGT_NUM, B.DLY_MAX_NUM, B.UNT_NUM, B.DEPNDNT_MSSN_ID, B.MSSN_SPAN_DAY_NUM, B.TRCKER_RULE_ID, B.SPCF_TRCKER_ID, B.ACTV_MSSN_IND, B.PRIM_CTGRY_ID,B.PRIM_SUB_CTGRY_ID,A.DISPL_ORDR_NUM FROM  MSSN_GOAL A INNER JOIN MSSN B ON B.MSSN_ID = A.MSSN_ID WHERE A.GOAL_ID = ?  AND B.MSSN_ID NOT IN (SELECT DISTINCT(C.MSSN_ID) FROM USR_MSSN C INNER JOIN MSSN E ON E.MSSN_ID = C.MSSN_ID WHERE (C.STAT_CD = ? OR \t\t( (C.STAT_CD = ? OR (C.STAT_CD = ? AND E.MSSN_STG_ID != ?)) AND C.MSSN_END_DT > ?)  AND C.USR_ID = ?) ) AND ((B.DEPNDNT_MSSN_ID IS NULL OR B.DEPNDNT_MSSN_ID = -1) OR ((SELECT COUNT(D.MSSN_ID) FROM USR_MSSN D WHERE D.MSSN_ID = B.DEPNDNT_MSSN_ID AND D.STAT_CD = ? AND D.USR_ID = ?) > 0) ) ORDER BY B.MSSN_STG_ID, A.DISPL_ORDR_NUM "

.field static final GetNonCompleteUserMissionsForGoal_SELECT_QUERY_FOR_USER_MISSIONS:Ljava/lang/String; = " SELECT MSSN_ID,MSSN_SEQ_ID  FROM USR_MSSN  WHERE GOAL_ID=? AND USR_ID = ? AND STAT_CD = ?   "

.field static final GetUserMissionDetails_SELECT_USER_LATEST_USER_MISSIONS_HAVING_PROGRESS_QUERY_FOR_ALL_GOALS:Ljava/lang/String;

.field static final GetUserMissionDetails_SELECT_USER_MISSIONS_HAVING_PROGRESS_QUERY:Ljava/lang/String;

.field static final GetUserMissionDetails_SELECT_USER_MISSIONS_NO_ACTIVITY_QUERY:Ljava/lang/String; = "SELECT MAX(MSSN_END_DT) FROM USR_MSSN A WHERE A.GOAL_ID = ? AND A.USR_ID=?"

.field static final GetUserMissionDetails_SELECT_USER_MISSIONS_WITH_LATEST_CHANGE:Ljava/lang/String; = "SELECT MSSN_START_DT , MSSN_END_DT FROM USR_MSSN WHERE USR_ID = ? ORDER BY MSSN_START_DT , MSSN_END_DT DESC"

.field static final GetUserMissionDetails_SELECT_USER_MISSIONS_WITH_LATEST_CHANGE_BY_GOAL:Ljava/lang/String; = "SELECT MSSN_START_DT , MSSN_END_DT FROM USR_MSSN WHERE USR_ID = ?  AND GOAL_SEQ_ID = ?  ORDER BY MSSN_START_DT DESC, MSSN_END_DT DESC"

.field static final GetUserMissionDetails_SELECT_USING_SEQUENCE_ID_MISSIONS_QUERY:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.MSSN_SEQ_ID = ? "

.field static final RECENT_MISSION_COUNT:Ljava/lang/String;

.field static final SELECT_ACTIVE_USER_MISSIONS_WITH_TRACKERS:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.STAT_CD = ? AND B.TRCKER_RULE_ID != -1"

.field static final SELECT_ALL_ACTIVE_USER_MISSIONS_IN_DISPLAY_ORDER:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.STAT_CD = ? ORDER BY E.DISPL_ORDR_NUM"

.field static final SELECT_USER_MISSION_DATA_FOR_A_MISSION_QUERY:Ljava/lang/String; = "SELECT MSSN_ACCOMP_DT, MSSN_ACTY_CNT, SRC_REF  FROM  USR_MSSN_DATA WHERE MSSN_ID = ? AND USR_ID = ? AND MSSN_SEQ_ID = ?"


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT COUNT(*) FROM USR_MSSN A INNER JOIN USR_GOAL B ON A.GOAL_ID =  B.GOAL_ID WHERE A.USR_ID = ? AND (( A.STAT_CD = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "STAT_CD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MSSN_END_DT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " + (? * "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/32 v1, 0x5265c00

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") > ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "STAT_CD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->GetUserMissionDetails_SELECT_USER_LATEST_USER_MISSIONS_HAVING_PROGRESS_QUERY_FOR_ALL_GOALS:Ljava/lang/String;

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT COUNT(*) FROM USR_MSSN A INNER JOIN USR_GOAL B ON A.GOAL_ID =  B.GOAL_ID WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND (( A.STAT_CD = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "STAT_CD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MSSN_END_DT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " + (? * "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/32 v1, 0x5265c00

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") > ?)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "STAT_CD"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->GetUserMissionDetails_SELECT_USER_MISSIONS_HAVING_PROGRESS_QUERY:Ljava/lang/String;

    .line 158
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s = ? AND %s = %s"

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "USR_MSSN"

    aput-object v2, v1, v4

    const-string v2, "USR_ID"

    aput-object v2, v1, v5

    const-string v2, "STAT_CD"

    aput-object v2, v1, v6

    sget-object v2, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_COUNT:Ljava/lang/String;

    .line 166
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s = ? AND %s > ? AND %s = %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "USR_MSSN"

    aput-object v2, v1, v4

    const-string v2, "USR_ID"

    aput-object v2, v1, v5

    const-string v2, "MSSN_END_DT"

    aput-object v2, v1, v6

    const-string v2, "STAT_CD"

    aput-object v2, v1, v7

    sget-object v2, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->RECENT_MISSION_COUNT:Ljava/lang/String;

    .line 175
    const-string v0, "SELECT COUNT(*) FROM (%s JOIN %s ON %s.%s = %s.%s) WHERE %s.%s = ? AND %s.%s = %s AND %s.%s = ?"

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "USR_MSSN"

    aput-object v2, v1, v4

    const-string v2, "MSSN"

    aput-object v2, v1, v5

    const-string v2, "USR_MSSN"

    aput-object v2, v1, v6

    const-string v2, "MSSN_ID"

    aput-object v2, v1, v7

    const-string v2, "MSSN"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "MSSN_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "USR_MSSN"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "USR_MSSN"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "STAT_CD"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "MSSN"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "MSSN_STG_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_COUNT_BY_STAGE:Ljava/lang/String;

    .line 192
    const-string v0, "SELECT %s, COUNT(*) FROM %s WHERE %s = ? AND %s = %s GROUP BY %s"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "USR_MSSN_FREQ"

    aput-object v2, v1, v4

    const-string v2, "USR_MSSN"

    aput-object v2, v1, v5

    const-string v2, "USR_ID"

    aput-object v2, v1, v6

    const-string v2, "STAT_CD"

    aput-object v2, v1, v7

    sget-object v2, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "USR_MSSN_FREQ"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_FREQUENCIES:Ljava/lang/String;

    .line 204
    const-string v0, "SELECT COUNT(DISTINCT A.%s) FROM (SELECT * FROM %s WHERE %s = %s AND %s = ?) AS A JOIN (SELECT * FROM %s WHERE %s = %s) AS B ON A.%s = B.%s AND A.%s = B.%s AND A.%s >= B.%s"

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "MSSN_ID"

    aput-object v2, v1, v4

    const-string v2, "USR_MSSN"

    aput-object v2, v1, v5

    const-string v2, "STAT_CD"

    aput-object v2, v1, v6

    sget-object v2, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const-string v2, "USR_ID"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "USR_MSSN"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "STAT_CD"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "MSSN_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "MSSN_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "MSSN_END_DT"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "MSSN_END_DT"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_AFTER_FAILURE_COUNT:Ljava/lang/String;

    .line 224
    const-string v0, "SELECT %s, COUNT(*) FROM %s UM JOIN %s M ON UM.%s = M.%s AND UM.%s = ? AND UM.%s = %s GROUP BY %s"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "MSSN_STG_ID"

    aput-object v2, v1, v4

    const-string v2, "USR_MSSN"

    aput-object v2, v1, v5

    const-string v2, "MSSN"

    aput-object v2, v1, v6

    const-string v2, "MSSN_ID"

    aput-object v2, v1, v7

    const-string v2, "MSSN_ID"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "STAT_CD"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "MSSN_STG_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBQueries;->GET_COMPLETED_MISSION_COUNT_PER_STAGE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
