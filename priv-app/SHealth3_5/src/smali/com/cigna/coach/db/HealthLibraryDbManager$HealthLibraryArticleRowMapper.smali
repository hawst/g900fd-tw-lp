.class Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryArticleRowMapper;
.super Ljava/lang/Object;
.source "HealthLibraryDbManager.java"

# interfaces
.implements Lcom/cigna/coach/db/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/HealthLibraryDbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HealthLibraryArticleRowMapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/cigna/coach/db/RowMapper",
        "<",
        "Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cigna/coach/db/HealthLibraryDbManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/cigna/coach/db/HealthLibraryDbManager$1;

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryArticleRowMapper;-><init>()V

    return-void
.end method


# virtual methods
.method public mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 696
    new-instance v0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;-><init>()V

    .line 697
    .local v0, "row":Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setId(I)V

    .line 698
    const/4 v3, 0x3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setTitleId(I)V

    .line 699
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setDetailId(I)V

    .line 700
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setImageId(I)V

    .line 701
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setCategoryId(I)V

    .line 702
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setSubCategoryId(I)V

    .line 703
    const/4 v3, 0x6

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setCatDescId(I)V

    .line 704
    const/4 v3, 0x7

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setCatImageId(I)V

    .line 705
    const/16 v3, 0x8

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setSubCatDescId(I)V

    .line 706
    const/16 v3, 0x9

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setSubCatImageId(I)V

    .line 707
    const/16 v3, 0xb

    invoke-interface {p1, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->setFavorite(Z)V

    .line 708
    return-object v0

    :cond_0
    move v1, v2

    .line 707
    goto :goto_0
.end method

.method public bridge synthetic mapRow(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 690
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryArticleRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;

    move-result-object v0

    return-object v0
.end method
