.class public Lcom/cigna/coach/db/TrackerDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "TrackerDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field static final SELECT_SAMSUNG_SPECIFIC_TRACKER_ID:Ljava/lang/String; = "SELECT SAMSUNG_EXERCISE_ID  FROM   CIGNA_SAMSUNG_EXERCISE_ID WHERE CIGNA_EXERCISE_ID = ? ORDER BY SAMSUNG_EXERCISE_ID DESC"

.field static final SELECT_TRACKER_RULE:Ljava/lang/String; = "SELECT TRCKER_RULE_ID,  TYPE_IND,  VALUE,  MIN_MET_VALUE,  MAX_MET_VALUE,  TRCKER_TABLE  FROM   TRCKER_RULE WHERE TRCKER_RULE_ID = ?"

.field protected static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/cigna/coach/db/TrackerDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 61
    return-void
.end method


# virtual methods
.method public getSamsungExerciseIds(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)Ljava/util/List;
    .locals 9
    .param p1, "specificTracker"    # Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    if-eqz p1, :cond_0

    sget-object v3, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->NOT_DEFINED:Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    if-ne p1, v3, :cond_2

    .line 100
    :cond_0
    const/4 v2, 0x0

    .line 128
    :cond_1
    :goto_0
    return-object v2

    .line 102
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 104
    .local v2, "samsungTrackerIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 105
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getSamsungTrackerIDs: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_3
    const/4 v0, 0x0

    .line 110
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/TrackerDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "SELECT SAMSUNG_EXERCISE_ID  FROM   CIGNA_SAMSUNG_EXERCISE_ID WHERE CIGNA_EXERCISE_ID = ? ORDER BY SAMSUNG_EXERCISE_ID DESC"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->getIntValue()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 112
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 113
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_6

    .line 114
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 117
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in getSamsungTrackerIDs: "

    invoke-static {v3, v4, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 119
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 121
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_4

    .line 122
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 123
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 124
    sget-object v4, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getSamsungTrackerIDs: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v3

    .line 121
    :cond_6
    if-eqz v0, :cond_7

    .line 122
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 123
    :cond_7
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getSamsungTrackerIDs: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getTrackerRuleInfo(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;)Lcom/cigna/coach/dataobjects/TrackerRuleData;
    .locals 9
    .param p1, "trackerRule"    # Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .prologue
    .line 64
    new-instance v2, Lcom/cigna/coach/dataobjects/TrackerRuleData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/TrackerRuleData;-><init>()V

    .line 66
    .local v2, "trackerRuleData":Lcom/cigna/coach/dataobjects/TrackerRuleData;
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 67
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getTrackerRuleInfo: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    :cond_0
    const/4 v0, 0x0

    .line 72
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/TrackerDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "SELECT TRCKER_RULE_ID,  TYPE_IND,  VALUE,  MIN_MET_VALUE,  MAX_MET_VALUE,  TRCKER_TABLE  FROM   TRCKER_RULE WHERE TRCKER_RULE_ID = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->getIntValue()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 74
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 75
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_1

    .line 76
    const-string v3, "MAX_MET_VALUE"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->setMaxMetValue(I)V

    .line 77
    const-string v3, "MIN_MET_VALUE"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->setMinMetValue(I)V

    .line 78
    const-string v3, "TRCKER_RULE_ID"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->setTrackerRuleId(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;)V

    .line 79
    const-string v3, "VALUE"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->setTrackerValue(I)V

    .line 80
    const-string v3, "TYPE_IND"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->setTrackerValueTypeInd(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;)V

    .line 81
    const-string v3, "TRCKER_TABLE"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/TrackerRuleData;->setTrackerTable(Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :cond_1
    if-eqz v0, :cond_2

    .line 88
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 89
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 90
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getTrackerRuleInfo: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_3
    return-object v2

    .line 83
    :catch_0
    move-exception v1

    .line 84
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in getTrackerRuleInfo: "

    invoke-static {v3, v4, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 85
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_4

    .line 88
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 89
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 90
    sget-object v4, Lcom/cigna/coach/db/TrackerDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getTrackerRuleInfo: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v3
.end method
