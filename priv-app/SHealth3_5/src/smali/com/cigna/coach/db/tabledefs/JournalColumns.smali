.class public Lcom/cigna/coach/db/tabledefs/JournalColumns;
.super Ljava/lang/Object;
.source "JournalColumns.java"


# static fields
.field public static final KEY_ACTN_CD:Ljava/lang/String; = "ACTN_CD"

.field public static final KEY_ACTN_TIMESTMP:Ljava/lang/String; = "ACTN_TIMESTMP"

.field public static final KEY_CD_BASE_ID:Ljava/lang/String; = "CD_BASE_ID"

.field public static final KEY_DATA:Ljava/lang/String; = "DATA"

.field public static final KEY_DB_VERSN:Ljava/lang/String; = "DB_VERSN"

.field public static final KEY_KEY:Ljava/lang/String; = "KEY"

.field public static final KEY_SEQ_ID:Ljava/lang/String; = "SEQ_ID"

.field public static final KEY_TBL_NM:Ljava/lang/String; = "TBL_NM"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "USR_ACTY_JRNL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
