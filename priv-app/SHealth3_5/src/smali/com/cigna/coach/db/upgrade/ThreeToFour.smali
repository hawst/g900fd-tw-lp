.class public Lcom/cigna/coach/db/upgrade/ThreeToFour;
.super Ljava/lang/Object;
.source "ThreeToFour.java"

# interfaces
.implements Lcom/cigna/coach/db/upgrade/IDBUpgrade;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/upgrade/ThreeToFour$CoachMessageChanges;,
        Lcom/cigna/coach/db/upgrade/ThreeToFour$MCCCountryCodes;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final ENABLE_JOURNALLING:Ljava/lang/String; = "UPDATE USR_ACTY_JRNL_SWITCH SET JRNL_ON=1"

.field private static final SET_BACKUP_TYPE_FULL:Ljava/lang/String; = "INSERT OR REPLACE INTO USR_METRCS (USR_ID, METRC_NM, METRC_VAL_TXT) VALUES (1,7,1)"

.field private static final SET_HAS_SEEN_WELCOME_SCREEN:Ljava/lang/String; = "INSERT OR REPLACE INTO USR_METRCS (USR_ID, METRC_NM, METRC_VAL_TXT) VALUES (1,8,1)"

.field private static final USR_API_CALL_LOG_CREATE:Ljava/lang/String; = "CREATE TABLE USR_API_CALL_LOG (SEQ_ID       INTEGER PRIMARY KEY AUTOINCREMENT,USR_ID        INTEGER NOT NULL,API_CD        INTEGER NOT NULL,API_CALL_SRC_CD  INTEGER NOT NULL,API_CALL_TS      INTEGER NOT NULL,API_CALL_RESP_TM INTEGER NOT NULL,API_CALL_ERR_CD  INTEGER,FOREIGN KEY (USR_ID) REFERENCES USR_INFO (USR_ID))"

.field private static final USR_API_CALL_LOG_DROP:Ljava/lang/String; = "DROP TABLE IF EXISTS USR_API_CALL_LOG"

.field private static final USR_COACH_MSG_CREATE:Ljava/lang/String; = "CREATE TABLE USR_COACH_MSG (USR_ID INTEGER NOT NULL, CNTXT_ID INTEGER NOT NULL,CNTXT_FILTER_CD   INTEGER NOT NULL, MSG_TY_CD INTEGER NOT NULL, ACTY_CNT INTEGER, MIN_MAX_REF_CD INTEGER, MIN_VAL INTEGER, MAX_VAL INTEGER, LAST_DELIVERED_TIME  INTEGER, PRIMARY KEY (USR_ID, CNTXT_ID, CNTXT_FILTER_CD, MSG_TY_CD,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL), FOREIGN KEY (USR_ID) REFERENCES USR_INFO (USR_ID))"

.field private static final USR_COACH_MSG_DROP:Ljava/lang/String; = "DROP TABLE IF EXISTS USR_COACH_MSG"

.field private static final WIDGT_NOTFTN_CREATE:Ljava/lang/String; = "CREATE TABLE WIDGT_NOTFTN (SEQ_ID         INTEGER PRIMARY KEY AUTOINCREMENT,DISPL_RANK_NUM INTEGER NOT NULL,WIDGT_TYPE     INTEGER NOT NULL,MSG_TYPE       INTEGER NOT NULL,COACH_MSG_ID   INTEGER NOT NULL,USR_ID         INTEGER NOT NULL,UNIQ_KEY       TEXT,INTNT          TEXT)"

.field private static final WIDGT_NOTFTN_DROP:Ljava/lang/String; = "DROP TABLE IF EXISTS WIDGT_NOTFTN"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/cigna/coach/db/upgrade/ThreeToFour;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 359
    return-void
.end method

.method private addCreateQueriesList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 204
    .local v0, "createQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "CREATE TABLE USR_COACH_MSG (USR_ID INTEGER NOT NULL, CNTXT_ID INTEGER NOT NULL,CNTXT_FILTER_CD   INTEGER NOT NULL, MSG_TY_CD INTEGER NOT NULL, ACTY_CNT INTEGER, MIN_MAX_REF_CD INTEGER, MIN_VAL INTEGER, MAX_VAL INTEGER, LAST_DELIVERED_TIME  INTEGER, PRIMARY KEY (USR_ID, CNTXT_ID, CNTXT_FILTER_CD, MSG_TY_CD,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL), FOREIGN KEY (USR_ID) REFERENCES USR_INFO (USR_ID))"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v1, "CREATE TABLE USR_API_CALL_LOG (SEQ_ID       INTEGER PRIMARY KEY AUTOINCREMENT,USR_ID        INTEGER NOT NULL,API_CD        INTEGER NOT NULL,API_CALL_SRC_CD  INTEGER NOT NULL,API_CALL_TS      INTEGER NOT NULL,API_CALL_RESP_TM INTEGER NOT NULL,API_CALL_ERR_CD  INTEGER,FOREIGN KEY (USR_ID) REFERENCES USR_INFO (USR_ID))"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    const-string v1, "CREATE TABLE WIDGT_NOTFTN (SEQ_ID         INTEGER PRIMARY KEY AUTOINCREMENT,DISPL_RANK_NUM INTEGER NOT NULL,WIDGT_TYPE     INTEGER NOT NULL,MSG_TYPE       INTEGER NOT NULL,COACH_MSG_ID   INTEGER NOT NULL,USR_ID         INTEGER NOT NULL,UNIQ_KEY       TEXT,INTNT          TEXT)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    const-string v1, "CREATE TABLE COUNTRY_MCC_CODES (ISO_COUNTRY_CODE TEXT NOT NULL, MCC_CODE INTEGER NOT NULL)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    return-object v0
.end method

.method private addDropQueriesToList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 241
    .local v0, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "DROP TABLE IF EXISTS USR_API_CALL_LOG"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    const-string v1, "DROP TABLE IF EXISTS USR_COACH_MSG"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    const-string v1, "DROP TABLE IF EXISTS COUNTRY_MCC_CODES"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    const-string v1, "DROP TABLE IF EXISTS WIDGT_NOTFTN"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    return-object v0
.end method

.method private executeCreateTableSqlScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 177
    .local p2, "createTableQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 178
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Preparing to execute the SQL scripts from list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 183
    .local v2, "sqlStatement":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 184
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 189
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 195
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 196
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    :cond_3
    return-void
.end method

.method private executeDropTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 214
    .local p2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 215
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Preparing to execute the SQL scripts from list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 220
    .local v2, "sqlStatement":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 221
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 232
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 233
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_3
    return-void
.end method

.method private executeInsertSqlScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 268
    .local p2, "insertTableQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 269
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Preparing to execute the SQL scripts from list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 274
    .local v2, "sqlStatement":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 275
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 280
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 286
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 287
    sget-object v3, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_3
    return-void
.end method

.method private updateContent(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 149
    sget-object v1, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    sget-object v1, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Updating content"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/CMSHelper;

    invoke-direct {v0, p1}, Lcom/cigna/coach/utils/CMSHelper;-><init>(Landroid/content/Context;)V

    .line 154
    .local v0, "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/cigna/coach/utils/CMSHelper;->checkAndCopyDefaultZipFilesToLocalPath(ZLandroid/database/sqlite/SQLiteDatabase;)Z

    .line 155
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/cigna/coach/utils/CMSHelper;->updateLanguageContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    .line 157
    sget-object v1, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 158
    sget-object v1, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Content update complete"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_1
    return-void
.end method

.method private updateDbVersionTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 164
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 165
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "DB_VERSN"

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 166
    const-string v2, "DB_VERSN"

    const-string v3, "CD_BASE_ID =?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    return-void

    .line 167
    .end local v1    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 168
    .local v0, "ex":Landroid/database/sqlite/SQLiteException;
    sget-object v2, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "An error occured while executing sql statement"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    sget-object v2, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    new-instance v2, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v2, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private userTableNames()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v0, "tableNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "USR_INFO"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    const-string v1, "USR_METRCS"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    const-string v1, "USR_LFSTYL_ASSMT_RESP"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    const-string v1, "USR_COACH_MSG"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    const-string v1, "USR_GOAL"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    const-string v1, "USR_MSSN"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    const-string v1, "USR_MSSN_DATA"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    const-string v1, "USR_ARTCL"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    const-string v1, "USR_BADGE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    const-string v1, "LFSTYL_ASSMT_SCORE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    const-string v1, "USR_API_CALL_LOG"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    return-object v0
.end method


# virtual methods
.method public applyUpgrade(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 106
    sget-object v5, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 107
    sget-object v5, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "***********************************"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    :cond_0
    invoke-static {p1, p2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->dropJournalTriggers(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 114
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->addDropQueriesToList()Ljava/util/List;

    move-result-object v1

    .line 115
    .local v1, "dropTableQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v1}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->executeDropTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 118
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->addCreateQueriesList()Ljava/util/List;

    move-result-object v0

    .line 119
    .local v0, "createTableQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v0}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->executeCreateTableSqlScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 122
    invoke-static {}, Lcom/cigna/coach/db/upgrade/ThreeToFour$MCCCountryCodes;->addInsertQueriesList()Ljava/util/List;

    move-result-object v3

    .line 123
    .local v3, "insertMCCQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v3}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->executeInsertSqlScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 126
    invoke-static {}, Lcom/cigna/coach/db/upgrade/ThreeToFour$CoachMessageChanges;->addInsertQueriesList()Ljava/util/List;

    move-result-object v2

    .line 127
    .local v2, "insertCoachMsgQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v2}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->executeInsertSqlScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 129
    invoke-direct {p0, p2}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->updateDbVersionTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 131
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->userTableNames()Ljava/util/List;

    move-result-object v4

    .line 134
    .local v4, "tableNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2, v4}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->setJournalTriggers(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 136
    const-string v5, "UPDATE USR_ACTY_JRNL_SWITCH SET JRNL_ON=1"

    invoke-virtual {p2, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 138
    invoke-direct {p0, p1, p2}, Lcom/cigna/coach/db/upgrade/ThreeToFour;->updateContent(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 140
    const-string v5, "INSERT OR REPLACE INTO USR_METRCS (USR_ID, METRC_NM, METRC_VAL_TXT) VALUES (1,7,1)"

    invoke-virtual {p2, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    const-string v5, "INSERT OR REPLACE INTO USR_METRCS (USR_ID, METRC_NM, METRC_VAL_TXT) VALUES (1,8,1)"

    invoke-virtual {p2, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 142
    sget-object v5, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 143
    sget-object v5, Lcom/cigna/coach/db/upgrade/ThreeToFour;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "***********************************"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    :cond_1
    return-void
.end method
