.class public Lcom/cigna/coach/db/NotificationDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "NotificationDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field protected static final serialVersionUID:J = 0x1L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/cigna/coach/db/NotificationDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 67
    return-void
.end method


# virtual methods
.method public addNotificationToQueue(Ljava/util/List;)Z
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "lstNot":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 199
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "addNotificationToQueue: START"

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :cond_0
    const/4 v11, 0x0

    .line 202
    .local v11, "success":Z
    const/4 v9, 0x0

    .line 203
    .local v9, "numRowsInserted":I
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    .line 205
    .local v7, "lstNotSize":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v7, :cond_2

    .line 206
    :try_start_0
    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/cigna/coach/dataobjects/NotificationData;

    .line 207
    .local v8, "notification":Lcom/cigna/coach/dataobjects/NotificationData;
    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationDate()Ljava/util/Calendar;

    move-result-object v13

    invoke-static {v13}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v3

    .line 208
    .local v3, "deliveryDate":J
    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    .line 209
    .local v12, "values":Landroid/content/ContentValues;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 210
    .local v1, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v10, Ljava/io/ObjectOutputStream;

    invoke-direct {v10, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 211
    .local v10, "oos":Ljava/io/ObjectOutputStream;
    invoke-virtual {v10, v8}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 212
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    .line 214
    .local v2, "bytes":[B
    const-string v13, "DLVRY_DT"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 215
    const-string v13, "NOTFTN_OBJ"

    invoke-virtual {v12, v13, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 217
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/NotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v13

    const-string v14, "NOTFTN_QUEUE_BLOB"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15, v12}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 218
    add-int/lit8 v9, v9, 0x1

    .line 220
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 221
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "addNotificationToQueue: Notification Data : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/NotificationData;->getMessageLine1()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 225
    .end local v1    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "bytes":[B
    .end local v3    # "deliveryDate":J
    .end local v8    # "notification":Lcom/cigna/coach/dataobjects/NotificationData;
    .end local v10    # "oos":Ljava/io/ObjectOutputStream;
    .end local v12    # "values":Landroid/content/ContentValues;
    :cond_2
    const/4 v11, 0x1

    .line 230
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 231
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "addNotificationToQueue added "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " new row(s)"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "addNotificationToQueue: END"

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    :cond_3
    return v11

    .line 226
    :catch_0
    move-exception v5

    .line 227
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "addNotificationToQueue: "

    invoke-static {v13, v14, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 228
    new-instance v13, Lcom/cigna/coach/exceptions/CoachException;

    const-string v14, "addNotificationToQueue error :"

    invoke-direct {v13, v14, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v13

    sget-object v14, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 231
    sget-object v14, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "addNotificationToQueue added "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " new row(s)"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    sget-object v14, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "addNotificationToQueue: END"

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v13
.end method

.method public deleteNotificationsFromQueue(Ljava/util/Calendar;)Z
    .locals 12
    .param p1, "aofOfDate"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 302
    sget-object v5, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 303
    sget-object v5, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "deleteNotificationsFromQueue: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    sget-object v5, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Deleting Notifications on or before date:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/util/Calendar;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_0
    const/4 v4, 0x0

    .line 307
    .local v4, "success":Z
    const/4 v3, 0x0

    .line 309
    .local v3, "rowsDeleted":I
    :try_start_0
    invoke-static {p1}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v0

    .line 310
    .local v0, "asOfDateLong":J
    invoke-virtual {p0}, Lcom/cigna/coach/db/NotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "NOTFTN_QUEUE_BLOB"

    const-string v7, "(DLVRY_DT <= ?)"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v5, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 314
    const/4 v4, 0x1

    .line 319
    sget-object v5, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 320
    sget-object v5, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deleted "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " available Notification(s) from queue."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    sget-object v5, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "deleteNotificationsFromQueue: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_1
    return v4

    .line 315
    .end local v0    # "asOfDateLong":J
    :catch_0
    move-exception v2

    .line 316
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v5, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Error in deleteNotificationsFromQueue: "

    invoke-static {v5, v6, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 317
    new-instance v5, Lcom/cigna/coach/exceptions/CoachException;

    const-string v6, "deleteNotificationsFromQueue error :"

    invoke-direct {v5, v6, v2}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 319
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    sget-object v6, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 320
    sget-object v6, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "deleted "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " available Notification(s) from queue."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    sget-object v6, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "deleteNotificationsFromQueue: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v5
.end method

.method public getNotificationStateInfo()Lcom/cigna/coach/dataobjects/NotificationStateData;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 75
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 76
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "getNotificationStateInfo START"

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    :cond_0
    new-instance v10, Lcom/cigna/coach/dataobjects/NotificationStateData;

    invoke-direct {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;-><init>()V

    .line 80
    .local v10, "nsd":Lcom/cigna/coach/dataobjects/NotificationStateData;
    const-string v12, "SELECT  A.LAST_STRTUP_DT, A.LAST_TM_CHG_DT, A.NOTFTN_STRT_TM, A.NOTFTN_END_TM FROM NOTFTN_STE A"

    .line 82
    .local v12, "sql":Ljava/lang/String;
    const/4 v13, 0x0

    new-array v11, v13, [Ljava/lang/String;

    .line 83
    .local v11, "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 85
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/NotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v13

    invoke-virtual {v13, v12, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 86
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lez v13, :cond_1

    .line 87
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 88
    const-string v13, "LAST_STRTUP_DT"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 89
    .local v2, "lastStartupDateInt":J
    const-string v13, "LAST_TM_CHG_DT"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 90
    .local v4, "lastTimeChangeDateInt":J
    const-string v13, "NOTFTN_STRT_TM"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 91
    .local v8, "notificationStartTimeInt":J
    const-string v13, "NOTFTN_END_TM"

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 93
    .local v6, "notificationEndTimeInt":J
    invoke-static {v2, v3}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v10, v13}, Lcom/cigna/coach/dataobjects/NotificationStateData;->setLastStartupDate(Ljava/util/Calendar;)V

    .line 94
    invoke-static {v4, v5}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v10, v13}, Lcom/cigna/coach/dataobjects/NotificationStateData;->setLastTimeChangeDate(Ljava/util/Calendar;)V

    .line 95
    invoke-virtual {v10, v8, v9}, Lcom/cigna/coach/dataobjects/NotificationStateData;->setNotificationStartTime(J)V

    .line 96
    invoke-virtual {v10, v6, v7}, Lcom/cigna/coach/dataobjects/NotificationStateData;->setNotificationEndTime(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v2    # "lastStartupDateInt":J
    .end local v4    # "lastTimeChangeDateInt":J
    .end local v6    # "notificationEndTimeInt":J
    .end local v8    # "notificationStartTimeInt":J
    :cond_1
    if-eqz v0, :cond_2

    .line 105
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 108
    :cond_2
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 109
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getNotificationStateInfo END, LastStartupDate:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastStartupDate()Ljava/util/Calendar;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", LastTimeChangeDate:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastTimeChangeDate()Ljava/util/Calendar;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", NotificationStartTime:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationStartTime()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", NotificationEndTime:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationEndTime()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_3
    return-object v10

    .line 99
    :catch_0
    move-exception v1

    .line 100
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v13, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "Error getNotificationStateInfo: "

    invoke-static {v13, v14, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 101
    new-instance v13, Lcom/cigna/coach/exceptions/CoachException;

    const-string v14, "getNotificationStateInfo error :"

    invoke-direct {v13, v14, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v13

    if-eqz v0, :cond_4

    .line 105
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 108
    :cond_4
    sget-object v14, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v14}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 109
    sget-object v14, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getNotificationStateInfo END, LastStartupDate:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastStartupDate()Ljava/util/Calendar;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", LastTimeChangeDate:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastTimeChangeDate()Ljava/util/Calendar;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", NotificationStartTime:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationStartTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", NotificationEndTime:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationEndTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v13
.end method

.method public getNotificationsFromQueue(Ljava/util/Calendar;)Ljava/util/List;
    .locals 15
    .param p1, "aofOfDate"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/Notification;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 246
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 247
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getNotificationsFromQueue: START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Getting all Available Notification as of date:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Ljava/util/Calendar;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v6, "listNot":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Notification;>;"
    const/4 v4, 0x0

    .line 253
    .local v4, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v0

    .line 254
    .local v0, "asOfDateLong":J
    invoke-virtual {p0}, Lcom/cigna/coach/db/NotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const-string v10, "SELECT  A.NOTFTN_OBJ FROM NOTFTN_QUEUE_BLOB A WHERE  (A.DLVRY_DT <= ?)"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 258
    if-eqz v4, :cond_5

    .line 259
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 260
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Actual Query = SELECT  A.NOTFTN_OBJ FROM NOTFTN_QUEUE_BLOB A WHERE  (A.DLVRY_DT <= ?)"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 264
    :cond_2
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_5

    .line 265
    const-string v9, "NOTFTN_OBJ"

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v4, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 266
    .local v3, "bytes":[B
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 267
    .local v2, "bais":Ljava/io/ByteArrayInputStream;
    new-instance v8, Ljava/io/ObjectInputStream;

    invoke-direct {v8, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 269
    .local v8, "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v8}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/dataobjects/NotificationData;

    .line 271
    .local v7, "notification":Lcom/cigna/coach/dataobjects/NotificationData;
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    .line 274
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 275
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Notifiction Date = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/NotificationData;->getNotificationDate()Ljava/util/Calendar;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Calendar;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 279
    .end local v0    # "asOfDateLong":J
    .end local v2    # "bais":Ljava/io/ByteArrayInputStream;
    .end local v3    # "bytes":[B
    .end local v7    # "notification":Lcom/cigna/coach/dataobjects/NotificationData;
    .end local v8    # "ois":Ljava/io/ObjectInputStream;
    :catch_0
    move-exception v5

    .line 280
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Error in getNotificationsFromQueue: "

    invoke-static {v9, v10, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 281
    new-instance v9, Lcom/cigna/coach/exceptions/CoachException;

    const-string v10, "getNotificationsFromQueue error :"

    invoke-direct {v9, v10, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 283
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v9

    if-eqz v4, :cond_3

    .line 284
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 286
    :cond_3
    sget-object v10, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 287
    sget-object v10, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Found "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " available Notification(s) in queue."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    sget-object v10, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getNotificationsFromQueue: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v9

    .line 283
    .restart local v0    # "asOfDateLong":J
    :cond_5
    if-eqz v4, :cond_6

    .line 284
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 286
    :cond_6
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 287
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Found "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " available Notification(s) in queue."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    sget-object v9, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getNotificationsFromQueue: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_7
    return-object v6
.end method

.method public setNotificationStateInfo(Lcom/cigna/coach/dataobjects/NotificationStateData;)Z
    .locals 24
    .param p1, "nsd"    # Lcom/cigna/coach/dataobjects/NotificationStateData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 128
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 129
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v20, "setNotificationStateInfo START"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    const/4 v13, 0x0

    .line 134
    .local v13, "success":Z
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastStartupDate()Ljava/util/Calendar;

    move-result-object v6

    .line 135
    .local v6, "lastStartupDate":Ljava/util/Calendar;
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastTimeChangeDate()Ljava/util/Calendar;

    move-result-object v7

    .line 136
    .local v7, "lastTimeChangeDate":Ljava/util/Calendar;
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationStartTime()J

    move-result-wide v11

    .line 137
    .local v11, "notificationStartTime":J
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationEndTime()J

    move-result-wide v9

    .line 139
    .local v9, "notificationEndTime":J
    if-eqz v6, :cond_8

    const/4 v14, 0x1

    .line 140
    .local v14, "updateLastStartupDate":Z
    :goto_0
    if-eqz v7, :cond_9

    const/4 v15, 0x1

    .line 141
    .local v15, "updateLastTimeChangeDate":Z
    :goto_1
    const-wide/16 v19, 0x0

    cmp-long v19, v11, v19

    if-ltz v19, :cond_a

    const/16 v17, 0x1

    .line 142
    .local v17, "updateNotificationStartTime":Z
    :goto_2
    const-wide/16 v19, 0x0

    cmp-long v19, v9, v19

    if-ltz v19, :cond_b

    const/16 v16, 0x1

    .line 144
    .local v16, "updateNotificationEndTime":Z
    :goto_3
    const/4 v8, -0x1

    .line 146
    .local v8, "noOfRowsUpdated":I
    :try_start_0
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 148
    .local v18, "values":Landroid/content/ContentValues;
    if-eqz v14, :cond_1

    .line 149
    const-string v19, "LAST_STRTUP_DT"

    invoke-static {v6}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 151
    :cond_1
    if-eqz v15, :cond_2

    .line 152
    const-string v19, "LAST_TM_CHG_DT"

    invoke-static {v7}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 154
    :cond_2
    if-eqz v17, :cond_3

    .line 155
    const-string v19, "NOTFTN_STRT_TM"

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 157
    :cond_3
    if-eqz v16, :cond_4

    .line 158
    const-string v19, "NOTFTN_END_TM"

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v18 .. v20}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 161
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/NotificationDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v19

    const-string v20, "NOTFTN_STE"

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    move-object/from16 v3, v21

    move-object/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 165
    if-lez v8, :cond_5

    .line 166
    const/4 v13, 0x1

    .line 169
    :cond_5
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 170
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "setNotificationStateInfo: No of Rows Updated = "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    :cond_6
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 178
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "setNotificationStateInfo END, LastStartupDate:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastStartupDate()Ljava/util/Calendar;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", LastTimeChangeDate:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastTimeChangeDate()Ljava/util/Calendar;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", NotificationStartTime:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationStartTime()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", NotificationEndTime:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationEndTime()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v20, "setNotificationStateInfo: END"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_7
    return v13

    .line 139
    .end local v8    # "noOfRowsUpdated":I
    .end local v14    # "updateLastStartupDate":Z
    .end local v15    # "updateLastTimeChangeDate":Z
    .end local v16    # "updateNotificationEndTime":Z
    .end local v17    # "updateNotificationStartTime":Z
    .end local v18    # "values":Landroid/content/ContentValues;
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 140
    .restart local v14    # "updateLastStartupDate":Z
    :cond_9
    const/4 v15, 0x0

    goto/16 :goto_1

    .line 141
    .restart local v15    # "updateLastTimeChangeDate":Z
    :cond_a
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 142
    .restart local v17    # "updateNotificationStartTime":Z
    :cond_b
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 172
    .restart local v8    # "noOfRowsUpdated":I
    .restart local v16    # "updateNotificationEndTime":Z
    :catch_0
    move-exception v5

    .line 173
    .local v5, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v19, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v20, "Error in setNotificationStateInfo: "

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 174
    new-instance v19, Lcom/cigna/coach/exceptions/CoachException;

    const-string/jumbo v20, "setNotificationStateInfo error :"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v5}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v19

    sget-object v20, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 178
    sget-object v20, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v22, "setNotificationStateInfo END, LastStartupDate:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastStartupDate()Ljava/util/Calendar;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", LastTimeChangeDate:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getLastTimeChangeDate()Ljava/util/Calendar;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", NotificationStartTime:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationStartTime()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", NotificationEndTime:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/dataobjects/NotificationStateData;->getNotificationEndTime()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v20, Lcom/cigna/coach/db/NotificationDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v21, "setNotificationStateInfo: END"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v19
.end method
