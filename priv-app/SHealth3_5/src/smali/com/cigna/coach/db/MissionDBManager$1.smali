.class Lcom/cigna/coach/db/MissionDBManager$1;
.super Ljava/lang/Object;
.source "MissionDBManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cigna/coach/db/MissionDBManager;->getAllActiveMissionsInExpirationOrder(Ljava/lang/String;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cigna/coach/dataobjects/UserMissionData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cigna/coach/db/MissionDBManager;


# direct methods
.method constructor <init>(Lcom/cigna/coach/db/MissionDBManager;)V
    .locals 0

    .prologue
    .line 1040
    iput-object p1, p0, Lcom/cigna/coach/db/MissionDBManager$1;->this$0:Lcom/cigna/coach/db/MissionDBManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/dataobjects/UserMissionData;)I
    .locals 2
    .param p1, "lhs"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .param p2, "rhs"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    .line 1043
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getExpirationDate()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getExpirationDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 1040
    check-cast p1, Lcom/cigna/coach/dataobjects/UserMissionData;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/cigna/coach/dataobjects/UserMissionData;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/db/MissionDBManager$1;->compare(Lcom/cigna/coach/dataobjects/UserMissionData;Lcom/cigna/coach/dataobjects/UserMissionData;)I

    move-result v0

    return v0
.end method
