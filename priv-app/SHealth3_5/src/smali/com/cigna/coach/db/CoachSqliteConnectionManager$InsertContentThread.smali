.class Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
.super Ljava/lang/Thread;
.source "CoachSqliteConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/CoachSqliteConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InsertContentThread"
.end annotation


# static fields
.field private static final threadName:Ljava/lang/String; = "InsertContentThread"


# instance fields
.field allLangData:[Ljava/lang/String;

.field locSpecLangData:[Ljava/lang/String;

.field final synthetic this$0:Lcom/cigna/coach/db/CoachSqliteConnectionManager;


# direct methods
.method constructor <init>(Lcom/cigna/coach/db/CoachSqliteConnectionManager;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p2, "allLangData"    # [Ljava/lang/String;
    .param p3, "locSpecLangData"    # [Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 84
    iput-object p1, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->this$0:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    .line 85
    const-string v0, "InsertContentThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 80
    iput-object v1, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->allLangData:[Ljava/lang/String;

    .line 81
    iput-object v1, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->locSpecLangData:[Ljava/lang/String;

    .line 86
    iput-object p2, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->allLangData:[Ljava/lang/String;

    .line 87
    iput-object p3, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->locSpecLangData:[Ljava/lang/String;

    .line 88
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 91
    # getter for: Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 92
    # getter for: Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "InsertContentThread starting"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 95
    .local v2, "startTime":J
    new-instance v1, Lcom/cigna/coach/db/CoachSQLiteHelper;

    iget-object v4, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->this$0:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    iget-object v4, v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    invoke-direct {v1, v4, v6}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;Z)V

    .line 98
    .local v1, "sqliteHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_0
    iget-object v4, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->this$0:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    iget-object v6, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->allLangData:[Ljava/lang/String;

    iget-object v7, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->locSpecLangData:[Ljava/lang/String;

    # invokes: Lcom/cigna/coach/db/CoachSqliteConnectionManager;->insertDataToContentTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;[Ljava/lang/String;)V
    invoke-static {v4, v5, v6, v7}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->access$100(Lcom/cigna/coach/db/CoachSqliteConnectionManager;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;[Ljava/lang/String;)V

    .line 99
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V

    .line 100
    iget-object v4, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->this$0:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    iget-object v4, v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    const/4 v5, 0x1

    sget-object v6, Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;->COACH_STARTED_SETTING:Lcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;

    invoke-static {v4, v5, v6}, Lcom/cigna/coach/utils/SharedPrefUtils;->setCoachSetttings(Landroid/content/Context;ZLcom/cigna/coach/utils/SharedPrefUtils$CoachSettingsType;)V

    .line 101
    iget-object v4, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->this$0:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    iget-object v4, v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendStartServiceCompleteBroadcast()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 108
    :goto_0
    # getter for: Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 109
    # getter for: Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Time taken to insert content is :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    # getter for: Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "InsertContentThread finished"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_1
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    # getter for: Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Error while running content initialization scripts"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v4
.end method
