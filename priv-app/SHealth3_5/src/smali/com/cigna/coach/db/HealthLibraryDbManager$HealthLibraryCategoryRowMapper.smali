.class Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;
.super Ljava/lang/Object;
.source "HealthLibraryDbManager.java"

# interfaces
.implements Lcom/cigna/coach/db/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/HealthLibraryDbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HealthLibraryCategoryRowMapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/cigna/coach/db/RowMapper",
        "<",
        "Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;",
        ">;"
    }
.end annotation


# instance fields
.field final catIdCol:I

.field final cntntIdCol:I

.field final imgCntntIdCol:I


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "catIdCol"    # I
    .param p2, "cntntIdCol"    # I
    .param p3, "imgCntntIdCol"    # I

    .prologue
    .line 727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 728
    iput p1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;->catIdCol:I

    .line 729
    iput p2, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;->cntntIdCol:I

    .line 730
    iput p3, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;->imgCntntIdCol:I

    .line 731
    return-void
.end method


# virtual methods
.method public mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 738
    new-instance v0, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;-><init>()V

    .line 739
    .local v0, "row":Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;
    iget v1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;->catIdCol:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->setCategoryId(I)V

    .line 740
    iget v1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;->cntntIdCol:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->setCntntId(I)V

    .line 741
    iget v1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;->imgCntntIdCol:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;->setImgCntntId(I)V

    .line 742
    return-object v0
.end method

.method public bridge synthetic mapRow(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 715
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibraryCategoryRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/HealthLibraryCategoryData;

    move-result-object v0

    return-object v0
.end method
