.class Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;
.super Ljava/lang/Object;
.source "JournalDBManager.java"

# interfaces
.implements Lcom/cigna/coach/db/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/JournalDBManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "JournalEntryRowMapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/cigna/coach/db/RowMapper",
        "<",
        "Lcom/cigna/coach/dataobjects/JournalEntry;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cigna/coach/db/JournalDBManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/cigna/coach/db/JournalDBManager$1;

    .prologue
    .line 450
    invoke-direct {p0}, Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;-><init>()V

    return-void
.end method


# virtual methods
.method public mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/JournalEntry;
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 453
    new-instance v3, Lcom/cigna/coach/dataobjects/JournalEntry;

    invoke-direct {v3}, Lcom/cigna/coach/dataobjects/JournalEntry;-><init>()V

    .line 454
    .local v3, "je":Lcom/cigna/coach/dataobjects/JournalEntry;
    const-string v5, "SEQ_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setSeqId(I)V

    .line 455
    const-string v5, "CD_BASE_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setCodeBaseId(I)V

    .line 456
    const-string v5, "DB_VERSN"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setDbVersion(I)V

    .line 457
    const-string v5, "USR_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setUserId(I)V

    .line 458
    const-string v5, "ACTN_CD"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getInstance(I)Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setActionType(Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;)V

    .line 460
    :try_start_0
    const-string v5, "ACTN_TIMESTMP"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 461
    .local v4, "timestampString":Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/JournalDBManager;->sqliteTimestampFormat()Ljava/text/DateFormat;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 462
    .local v2, "entryTimestamp":Ljava/util/Date;
    new-instance v0, Ljava/util/GregorianCalendar;

    const-string v5, "UTC"

    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 463
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 464
    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/JournalEntry;->setActionTime(Ljava/util/Calendar;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 468
    const-string v5, "KEY"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setKey(Ljava/lang/String;)V

    .line 469
    const-string v5, "DATA"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setData(Ljava/lang/String;)V

    .line 470
    const-string v5, "TBL_NM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/cigna/coach/dataobjects/JournalEntry;->setTableName(Ljava/lang/String;)V

    .line 471
    return-object v3

    .line 465
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v2    # "entryTimestamp":Ljava/util/Date;
    .end local v4    # "timestampString":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 466
    .local v1, "e":Ljava/text/ParseException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public bridge synthetic mapRow(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 450
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/JournalEntry;

    move-result-object v0

    return-object v0
.end method
