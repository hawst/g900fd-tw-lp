.class public Lcom/cigna/coach/db/tabledefs/CoachMessageColumns;
.super Ljava/lang/Object;
.source "CoachMessageColumns.java"


# static fields
.field public static final KEY_CNTNT_ID:Ljava/lang/String; = "CNTNT_ID"

.field public static final KEY_CNTXT_FILTER_CD:Ljava/lang/String; = "CNTXT_FILTER_CD"

.field public static final KEY_CNTXT_ID:Ljava/lang/String; = "CNTXT_ID"

.field public static final KEY_CNTXT_SUB_GRP_ID:Ljava/lang/String; = "CNTXT_SUB_GRP_ID"

.field public static final KEY_COACH_MSG_ID:Ljava/lang/String; = "COACH_MSG_ID"

.field public static final KEY_DESC_CNTNT_ID:Ljava/lang/String; = "DESC_CNTNT_ID"

.field public static final KEY_DISPL_RANK_NUM:Ljava/lang/String; = "DISPL_RANK_NUM"

.field public static final KEY_IMG_CNTNT_ID:Ljava/lang/String; = "IMG_CNTNT_ID"

.field public static final KEY_INTENT_ACTION:Ljava/lang/String; = "INTENT_ACTION"

.field public static final KEY_INTENT_EXTRA:Ljava/lang/String; = "INTENT_EXTRA"

.field public static final KEY_IS_ENBLD:Ljava/lang/String; = "IS_ENBLD"

.field public static final KEY_MAX_VAL:Ljava/lang/String; = "MAX_VAL"

.field public static final KEY_MIN_MAX_REF_CD:Ljava/lang/String; = "MIN_MAX_REF_CD"

.field public static final KEY_MIN_VAL:Ljava/lang/String; = "MIN_VAL"

.field public static final KEY_MSG_TY_CD:Ljava/lang/String; = "MSG_TY_CD"

.field public static final KEY_REF_ID:Ljava/lang/String; = "REF_ID"

.field public static final KEY_REF_TY_IND:Ljava/lang/String; = "REF_TY_IND"

.field public static final KEY_RPT_INTVL:Ljava/lang/String; = "RPT_INTVL"

.field public static final TABLE_NAME:Ljava/lang/String; = "COACH_MSG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
