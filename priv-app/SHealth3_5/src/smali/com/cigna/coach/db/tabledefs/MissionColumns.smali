.class public Lcom/cigna/coach/db/tabledefs/MissionColumns;
.super Ljava/lang/Object;
.source "MissionColumns.java"


# static fields
.field public static final KEY_ACTV_MSSN_IND:Ljava/lang/String; = "ACTV_MSSN_IND"

.field public static final KEY_DEFLT_FREQ_IND:Ljava/lang/String; = "DEFLT_FREQ_IND"

.field public static final KEY_DEPNDNT_MSSN_ID:Ljava/lang/String; = "DEPNDNT_MSSN_ID"

.field public static final KEY_DLY_MAX_NUM:Ljava/lang/String; = "DLY_MAX_NUM"

.field public static final KEY_DLY_TRGT_NUM:Ljava/lang/String; = "DLY_TRGT_NUM"

.field public static final KEY_FREQ_END_RNG_NUM:Ljava/lang/String; = "FREQ_END_RNG_NUM"

.field public static final KEY_FREQ_START_RNG_NUM:Ljava/lang/String; = "FREQ_START_RNG_NUM"

.field public static final KEY_MSSN_CNTNT_ID:Ljava/lang/String; = "MSSN_CNTNT_ID"

.field public static final KEY_MSSN_DESC_CNTNT_ID:Ljava/lang/String; = "MSSN_DESC_CNTNT_ID"

.field public static final KEY_MSSN_FREQ_CNTNT_ID:Ljava/lang/String; = "MSSN_FREQ_CNTNT_ID"

.field public static final KEY_MSSN_ID:Ljava/lang/String; = "MSSN_ID"

.field public static final KEY_MSSN_IMG_CNTNT_ID:Ljava/lang/String; = "MSSN_IMG_CNTNT_ID"

.field public static final KEY_MSSN_SPAN_DAY_NUM:Ljava/lang/String; = "MSSN_SPAN_DAY_NUM"

.field public static final KEY_MSSN_STG_ID:Ljava/lang/String; = "MSSN_STG_ID"

.field public static final KEY_MSSN_WHAT_CNTNT_ID:Ljava/lang/String; = "MSSN_WHAT_CNTNT_ID"

.field public static final KEY_MSSN_WHY_CNTNT_ID:Ljava/lang/String; = "MSSN_WHY_CNTNT_ID"

.field public static final KEY_PRIM_CTGRY_ID:Ljava/lang/String; = "PRIM_CTGRY_ID"

.field public static final KEY_PRIM_SUB_CTGRY_ID:Ljava/lang/String; = "PRIM_SUB_CTGRY_ID"

.field public static final KEY_SPCF_TRCKER_ID:Ljava/lang/String; = "SPCF_TRCKER_ID"

.field public static final KEY_TRCKER_RULE_ID:Ljava/lang/String; = "TRCKER_RULE_ID"

.field public static final KEY_UNT_NUM:Ljava/lang/String; = "UNT_NUM"

.field public static final TABLE_NAME:Ljava/lang/String; = "MSSN"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
