.class Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;
.super Ljava/lang/Object;
.source "OneToTwo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/upgrade/OneToTwo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EnableJournalling"
.end annotation


# instance fields
.field tableNameList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->tableNameList:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lcom/cigna/coach/db/upgrade/OneToTwo$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/cigna/coach/db/upgrade/OneToTwo$1;

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->addCreateTableNamesToList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private addCreateQueriesToList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 306
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 308
    .local v2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->tableNameList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 309
    .local v3, "tableName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CREATE TABLE TEMP_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AS SELECT * FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "createQuery":Ljava/lang/String;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 311
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding create Query : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 316
    .end local v0    # "createQuery":Ljava/lang/String;
    .end local v3    # "tableName":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private addCreateTableNamesToList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 289
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 290
    .local v0, "tableNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "USR_INFO"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    const-string v1, "USR_METRCS"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    const-string v1, "USR_LFSTYL_ASSMT_RESP"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    const-string v1, "USR_COACH_MSG"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    const-string v1, "USR_GOAL"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    const-string v1, "USR_MSSN"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    const-string v1, "USR_MSSN_DATA"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    const-string v1, "USR_ARTCL"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    const-string v1, "USR_BADGE"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    return-object v0
.end method

.method private addDeleteQueriesToList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 335
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 337
    .local v2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->tableNameList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 338
    .local v3, "tableName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DELETE FROM "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 339
    .local v0, "deleteQuery":Ljava/lang/String;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 340
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding delete Query : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 345
    .end local v0    # "deleteQuery":Ljava/lang/String;
    .end local v3    # "tableName":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private addDropQueriesToList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 351
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 353
    .local v2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->tableNameList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 354
    .local v3, "tableName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DROP TABLE TEMP_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 355
    .local v0, "dropQuery":Ljava/lang/String;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 356
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding delete Query : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    :cond_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 361
    .end local v0    # "dropQuery":Ljava/lang/String;
    .end local v3    # "tableName":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private addInsertQueriesToList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 322
    .local v2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->tableNameList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 323
    .local v3, "tableName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "INSERT INTO "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SELECT * FROM TEMP_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 324
    .local v1, "insertQuery":Ljava/lang/String;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 325
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding delete Query : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :cond_0
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 330
    .end local v1    # "insertQuery":Ljava/lang/String;
    .end local v3    # "tableName":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private createTempUsrTablesAndInsertValues(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 180
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************Start createTempUserTableValues"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_0
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->addCreateQueriesToList()Ljava/util/List;

    move-result-object v0

    .line 186
    .local v0, "createQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->executeSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_0
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 192
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************End createTempUserTableValues"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_1
    return-void

    .line 187
    :catch_0
    move-exception v1

    .line 188
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteAllUserTableValues(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 239
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************Start deleteAllUserTableValues "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    :cond_0
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->addDeleteQueriesToList()Ljava/util/List;

    move-result-object v1

    .line 245
    .local v1, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->executeSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_0
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 251
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************End deleteAllUserTableValues"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    :cond_1
    return-void

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error occured while deleting tables values: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteTemporaryTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 198
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 199
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************Start deleteAllUserTableValues "

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_0
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->addDropQueriesToList()Ljava/util/List;

    move-result-object v1

    .line 204
    .local v1, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->executeSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************End deleteAllUserTableValues"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_1
    return-void

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error occured while deleting tables: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private executeSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 264
    .local p2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 265
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Preparing to execute the SQL scripts from list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 270
    .local v2, "sqlStatement":Ljava/lang/String;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 271
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 276
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Landroid/database/SQLException;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 282
    .end local v0    # "e":Landroid/database/SQLException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 283
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    :cond_3
    return-void
.end method

.method private insertBackAllUserTableValues(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 219
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************Start insertBackAllUserTableValues"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    :cond_0
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->addInsertQueriesToList()Ljava/util/List;

    move-result-object v1

    .line 225
    .local v1, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0, p1, v1}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->executeSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_0
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "***********************************End insertBackAllUserTableValues"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    :cond_1
    return-void

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Lcom/cigna/coach/exceptions/CoachException;
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error occured while inserting values: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public startJournalEnableProcess(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 3
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 135
    .local p2, "usrTableNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "***********************************Start StartJournalEnableProcess"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    iput-object p2, p0, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->tableNameList:Ljava/util/List;

    .line 141
    new-instance v0, Lcom/cigna/coach/db/JournalDBManager;

    invoke-direct {v0}, Lcom/cigna/coach/db/JournalDBManager;-><init>()V

    .line 143
    .local v0, "journalDBManager":Lcom/cigna/coach/db/JournalDBManager;
    invoke-virtual {v0, p1}, Lcom/cigna/coach/db/JournalDBManager;->stopJournalling(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 145
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->createTempUsrTablesAndInsertValues(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 152
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->deleteAllUserTableValues(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 155
    invoke-virtual {v0, p1}, Lcom/cigna/coach/db/JournalDBManager;->resumeJournalling(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 158
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->insertBackAllUserTableValues(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 161
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->deleteTemporaryTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 163
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    # getter for: Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/db/upgrade/OneToTwo;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v2, "***********************************End StartJournalEnableProcess"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_1
    return-void
.end method
