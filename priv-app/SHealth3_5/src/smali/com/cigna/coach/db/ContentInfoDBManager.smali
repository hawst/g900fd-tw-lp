.class public Lcom/cigna/coach/db/ContentInfoDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "ContentInfoDBManager.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/cigna/coach/db/ContentInfoDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 48
    return-void
.end method


# virtual methods
.method public deleteMultiLanguageContent()V
    .locals 6

    .prologue
    .line 67
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Deleting all multi language content from content table"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "CONTENT"

    const-string v2, "LANG_SPCF_CNTNT_IND=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 71
    return-void
.end method

.method public deleteNonMultiLanguageContent()V
    .locals 6

    .prologue
    .line 60
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Deleting all non multi language content from content table"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "CONTENT"

    const-string v2, "LANG_SPCF_CNTNT_IND=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "0"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 64
    return-void
.end method

.method public getContentInfo(Ljava/util/Collection;)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/ContentInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, "contentIdArray":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/Integer;>;"
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 117
    .local v14, "returnContentInfoMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Lcom/cigna/coach/dataobjects/ContentInfo;>;"
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v15, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 118
    .local v15, "sbWhereClause":Ljava/lang/StringBuilder;
    const-string v0, "CNTNT_ID"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    const-string v0, " IN ("

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 122
    .local v4, "whereParams":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 123
    .local v11, "index":I
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    .line 125
    .local v8, "contentId":Ljava/lang/Integer;
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding content id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to the query"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 132
    const-string v0, " ? )"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v11

    .line 149
    add-int/lit8 v11, v11, 0x1

    .line 150
    goto :goto_0

    .line 134
    :cond_1
    if-nez v11, :cond_2

    .line 136
    const-string v0, " ? "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 138
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v11, v0, :cond_3

    .line 141
    const-string v0, " , ? )"

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 145
    :cond_3
    const-string v0, ", ? "

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 152
    .end local v8    # "contentId":Ljava/lang/Integer;
    :cond_4
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 153
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Generated where clause is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_5
    const/4 v13, 0x0

    .line 159
    .local v13, "resultCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "CONTENT"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "CNTNT_ID"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "LOC_TY_IND"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "CNTNT_TXT"

    aput-object v5, v2, v3

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 165
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_2
    invoke-interface {v13}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_9

    .line 166
    const-string v0, "CNTNT_ID"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 168
    .local v8, "contentId":I
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/cigna/coach/dataobjects/ContentInfo;

    .line 169
    .local v9, "contentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    if-nez v9, :cond_6

    .line 170
    new-instance v9, Lcom/cigna/coach/dataobjects/ContentInfo;

    .end local v9    # "contentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    invoke-direct {v9, v8}, Lcom/cigna/coach/dataobjects/ContentInfo;-><init>(I)V

    .line 171
    .restart local v9    # "contentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v14, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    :cond_6
    const-string v0, "LOC_TY_IND"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(I)Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v12

    .line 177
    .local v12, "localeType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    const-string v0, "CNTNT_TXT"

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v13, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\\\n"

    const-string v2, "\\\n"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v12, v0}, Lcom/cigna/coach/dataobjects/ContentInfo;->addContent(Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;)V

    .line 182
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 183
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding content id:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "for localetype:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v12}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to resolved map. Text:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9, v12}, Lcom/cigna/coach/dataobjects/ContentInfo;->getContentForLocale(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_7
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_2

    .line 190
    .end local v8    # "contentId":I
    .end local v9    # "contentInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v12    # "localeType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    :catchall_0
    move-exception v0

    if-eqz v13, :cond_8

    .line 191
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 190
    :cond_9
    if-eqz v13, :cond_a

    .line 191
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 195
    :cond_a
    return-object v14
.end method

.method public getCurrentLocaleTypeLoaded()Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 10

    .prologue
    .line 74
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Getting current loaded locale type "

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_0
    const/4 v9, 0x0

    .line 78
    .local v9, "returnValue":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    const/4 v8, 0x0

    .line 80
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "CNTNT_CUR_LOC_TY"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 81
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 83
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(I)Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 87
    :cond_1
    if-eqz v8, :cond_2

    .line 88
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 91
    :cond_2
    return-object v9

    .line 87
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 88
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public insertContent([Ljava/lang/String;Z)V
    .locals 2
    .param p1, "data"    # [Ljava/lang/String;
    .param p2, "multiLanguage"    # Z

    .prologue
    .line 52
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Inserting data into content table"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/db/ContentInfoDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {p0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->loadContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V

    .line 56
    return-void
.end method

.method public setCurrentLocaleTypeLoaded(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V
    .locals 5
    .param p1, "localeType"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .prologue
    const/4 v4, 0x0

    .line 95
    if-eqz p1, :cond_2

    .line 96
    sget-object v1, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    sget-object v1, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Setting current loaded locale type to:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getKey()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "CNTNT_CUR_LOC_TY"

    invoke-virtual {v1, v2, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 102
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 103
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "LOC_TY_IND"

    invoke-virtual {p1}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 104
    invoke-virtual {p0}, Lcom/cigna/coach/db/ContentInfoDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "CNTNT_CUR_LOC_TY"

    invoke-virtual {v1, v2, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 110
    .end local v0    # "cv":Landroid/content/ContentValues;
    :cond_1
    :goto_0
    return-void

    .line 106
    :cond_2
    sget-object v1, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    sget-object v1, Lcom/cigna/coach/db/ContentInfoDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Locale type is null in setCurrentLocaleTypeLoaded"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
