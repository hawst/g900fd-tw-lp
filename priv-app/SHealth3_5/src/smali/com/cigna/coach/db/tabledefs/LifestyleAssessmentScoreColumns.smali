.class public Lcom/cigna/coach/db/tabledefs/LifestyleAssessmentScoreColumns;
.super Ljava/lang/Object;
.source "LifestyleAssessmentScoreColumns.java"


# static fields
.field public static final KEY_LFSTYL_ASSMT_SCORE_ID:Ljava/lang/String; = "LFSTYL_ASSMT_SCORE_ID"

.field public static final KEY_LFSTYL_ASSMT_TIMESTMP:Ljava/lang/String; = "LFSTYL_ASSMT_TIMESTMP"

.field public static final KEY_SCORE_TY_ID:Ljava/lang/String; = "SCORE_TY_ID"

.field public static final KEY_SCORE_TY_IND:Ljava/lang/String; = "SCORE_TY_IND"

.field public static final KEY_SCORE_VAL:Ljava/lang/String; = "SCORE_VAL"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "LFSTYL_ASSMT_SCORE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
