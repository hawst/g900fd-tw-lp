.class public final Lcom/cigna/coach/db/tabledefs/UserArticlesColumns;
.super Ljava/lang/Object;
.source "UserArticlesColumns.java"


# static fields
.field public static final KEY_ARTCL_ID:Ljava/lang/String; = "ARTCL_ID"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final QUALIFIED_KEY_ARTCL_ID:Ljava/lang/String; = "USR_ARTCL.ARTCL_ID"

.field public static final QUALIFIED_KEY_USR_ID:Ljava/lang/String; = "USR_ARTCL.USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "USR_ARTCL"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method
