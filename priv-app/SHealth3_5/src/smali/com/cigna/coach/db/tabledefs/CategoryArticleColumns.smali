.class public final Lcom/cigna/coach/db/tabledefs/CategoryArticleColumns;
.super Ljava/lang/Object;
.source "CategoryArticleColumns.java"


# static fields
.field public static final KEY_ARTCL_ID:Ljava/lang/String; = "ARTCL_ID"

.field public static final KEY_CTGRY_ID:Ljava/lang/String; = "CTGRY_ID"

.field public static final KEY_PRIM_ARTCL_IND:Ljava/lang/String; = "PRIM_ARTCL_IND"

.field public static final KEY_SUB_CTGRY_ID:Ljava/lang/String; = "SUB_CTGRY_ID"

.field public static final QUALIFIED_KEY_ARTCL_ID:Ljava/lang/String; = "CTGRY_ARTCL.ARTCL_ID"

.field public static final QUALIFIED_KEY_CTGRY_ID:Ljava/lang/String; = "CTGRY_ARTCL.CTGRY_ID"

.field public static final QUALIFIED_KEY_PRIM_ARTCL_IND:Ljava/lang/String; = "CTGRY_ARTCL.PRIM_ARTCL_IND"

.field public static final QUALIFIED_KEY_SUB_CTGRY_ID:Ljava/lang/String; = "CTGRY_ARTCL.SUB_CTGRY_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "CTGRY_ARTCL"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method
