.class public Lcom/cigna/coach/db/CoachMessageDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "CoachMessageDBManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field static coachMessageRetriveColumns:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    const-class v0, Lcom/cigna/coach/db/CoachMessageDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    .line 68
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "COACH_MSG_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MSG_TY_CD"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CNTNT_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "DESC_CNTNT_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "DISPL_RANK_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "IMG_CNTNT_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "RPT_INTVL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "MIN_MAX_REF_CD"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "MIN_VAL"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "MAX_VAL"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "CNTXT_FILTER_CD"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "INTENT_ACTION"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "INTENT_EXTRA"

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 64
    return-void
.end method

.method private insertUserCoachMessage(ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;IJIII)V
    .locals 5
    .param p1, "userId"    # I
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p3, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p4, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p5, "activityCount"    # I
    .param p6, "todayInMillis"    # J
    .param p8, "minValue"    # I
    .param p9, "maxValue"    # I
    .param p10, "minMaxRefValue"    # I

    .prologue
    .line 755
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 756
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Preparing to run query to save user coach message"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    :cond_0
    :try_start_0
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 760
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Preparing to run query to save user coach message"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 764
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "USR_ID"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 765
    const-string v2, "CNTXT_ID"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 766
    const-string v2, "CNTXT_FILTER_CD"

    invoke-virtual {p4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 767
    const-string v2, "ACTY_CNT"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 768
    const-string v2, "MSG_TY_CD"

    invoke-virtual {p3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 769
    const-string v2, "MIN_VAL"

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 770
    const-string v2, "MAX_VAL"

    invoke-static {p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 771
    const-string v2, "MIN_MAX_REF_CD"

    invoke-static {p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 772
    const-string v2, "LAST_DELIVERED_TIME"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 774
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "USR_COACH_MSG"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 779
    return-void

    .line 775
    .end local v1    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Error inserting UserCoachMessage"

    invoke-static {v2, v3, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 777
    throw v0
.end method

.method private performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 18
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;
    .param p4, "groupBy"    # Ljava/lang/String;
    .param p5, "having"    # Ljava/lang/String;
    .param p6, "orderBy"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 949
    .local p7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 995
    :cond_0
    :goto_0
    return-void

    .line 951
    :cond_1
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 952
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "performCoachDBQuery: START"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 954
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "performCoachDBQuery: columns :START"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    move-object/from16 v9, p1

    .local v9, "arr$":[Ljava/lang/String;
    array-length v15, v9

    .local v15, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_1
    if-ge v14, v15, :cond_2

    aget-object v11, v9, v14

    .line 956
    .local v11, "columnName":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "columnName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 958
    .end local v11    # "columnName":Ljava/lang/String;
    :cond_2
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "performCoachDBQuery: columns :END"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "performCoachDBQuery: selection query :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "performCoachDBQuery: selectionArgs :Start"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    move-object/from16 v9, p3

    array-length v15, v9

    const/4 v14, 0x0

    :goto_2
    if-ge v14, v15, :cond_3

    aget-object v17, v9, v14

    .line 963
    .local v17, "selectionArg":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "selectionArg Value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 965
    .end local v17    # "selectionArg":Ljava/lang/String;
    :cond_3
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "performCoachDBQuery: selectionArgs :END"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v14    # "i$":I
    .end local v15    # "len$":I
    :cond_4
    const/4 v12, 0x0

    .line 970
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "COACH_MSG"

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 971
    if-eqz v12, :cond_7

    .line 972
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 973
    new-instance v16, Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;

    invoke-direct/range {v16 .. v16}, Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;-><init>()V

    .line 975
    .local v16, "mapper":Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;
    :goto_3
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_7

    .line 976
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-result-object v10

    .line 977
    .local v10, "cm":Lcom/cigna/coach/dataobjects/CoachMessageData;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 978
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "performCoachDBQuery: CoachMessage Content Desc ID :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDescriptionContentId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    :cond_5
    move-object/from16 v0, p7

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 982
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 985
    .end local v10    # "cm":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .end local v16    # "mapper":Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;
    :catch_0
    move-exception v13

    .line 986
    .local v13, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error getting coach messages, query: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v13}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 987
    throw v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 989
    .end local v13    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v12, :cond_6

    .line 990
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v1

    .line 989
    :cond_7
    if-eqz v12, :cond_8

    .line 990
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 992
    :cond_8
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 993
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "performCoachDBQuery: END"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private updateUserCoachMessageActivityCount(ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;IJIII)V
    .locals 6
    .param p1, "userId"    # I
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p3, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p4, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p5, "activityCount"    # I
    .param p6, "todayInMillis"    # J
    .param p8, "minValue"    # I
    .param p9, "maxValue"    # I
    .param p10, "minMaxRefValue"    # I

    .prologue
    .line 783
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 784
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Preparing to run query to increment user coach message"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    :cond_0
    :try_start_0
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 788
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Preparing to run query to increment user coach message"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 792
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "ACTY_CNT"

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 793
    const-string v4, "LAST_DELIVERED_TIME"

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 796
    const-string v3, "USR_ID = ? AND CNTXT_ID = ? AND CNTXT_FILTER_CD = ? AND MSG_TY_CD = ? AND MIN_VAL = ? AND MAX_VAL = ? AND MIN_MAX_REF_CD = ?"

    .line 799
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v4, 0x7

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x2

    invoke-virtual {p4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x3

    invoke-virtual {p3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x4

    invoke-static {p8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x5

    invoke-static {p9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x6

    invoke-static/range {p10 .. p10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 807
    .local v2, "whereArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "USR_COACH_MSG"

    invoke-virtual {v4, v5, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 812
    return-void

    .line 808
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v2    # "whereArgs":[Ljava/lang/String;
    .end local v3    # "whereClause":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 809
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error updating UserCoachMessage"

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 810
    throw v0
.end method


# virtual methods
.method public checkActionMissionsAvailable(Ljava/lang/String;I)Z
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalSeqId"    # I

    .prologue
    .line 1336
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1337
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Preparing to run query to getMaxMissionEndDateForActiveActionMissions: for goalId ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1339
    :cond_0
    iget-object v4, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1343
    .local v3, "userIdKey":I
    const/4 v2, 0x0

    .line 1345
    .local v2, "isActionMissionExits":Z
    const/4 v0, 0x0

    .line 1347
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, " SELECT A.MSSN_ID FROM USR_MSSN A INNER JOIN MSSN B ON B.MSSN_ID = A.MSSN_ID WHERE A.USR_ID   = ? AND A.GOAL_SEQ_ID   = ? AND B.MSSN_STG_ID   = ? "

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getIntValue()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1353
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_3

    .line 1354
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1355
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1356
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1359
    const/4 v2, 0x1

    .line 1362
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1365
    :catch_0
    move-exception v1

    .line 1366
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error getting getMaxMissionEndDateForActiveActionMissions, query: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1367
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1369
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_2

    .line 1370
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v4

    .line 1369
    :cond_3
    if-eqz v0, :cond_4

    .line 1370
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1373
    :cond_4
    return v2
.end method

.method public checkForDisplayInterval(Ljava/util/List;I)V
    .locals 4
    .param p2, "minMaxValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 929
    .local p1, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 930
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 931
    .local v0, "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    if-eqz v0, :cond_0

    .line 932
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v1

    .line 934
    .local v1, "dbInterval":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    .line 935
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMinValue()I

    move-result v2

    .line 936
    .local v2, "minValue":I
    if-eq p2, v2, :cond_0

    add-int v3, v2, v1

    rem-int v3, p2, v3

    if-eqz v3, :cond_0

    .line 939
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 944
    .end local v0    # "coachMessageData":Lcom/cigna/coach/dataobjects/CoachMessageData;
    .end local v1    # "dbInterval":I
    .end local v2    # "minValue":I
    :cond_0
    return-void
.end method

.method public getActiveMissionNamesWithSpecificDaysSinceMissionStartDate(Ljava/lang/String;I)Ljava/util/Set;
    .locals 16
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "days"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1131
    sget-object v13, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1132
    sget-object v13, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Preparing to run query to getActiveMissionNamesWithSpecificDaysSinceMissionStartDate: for days ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    :cond_0
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 1135
    .local v8, "missionNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v13, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v12

    .line 1137
    .local v12, "userKey":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v11

    .line 1139
    .local v11, "targetDate":Ljava/util/Calendar;
    const/4 v13, 0x6

    move/from16 v0, p2

    neg-int v14, v0

    invoke-virtual {v11, v13, v14}, Ljava/util/Calendar;->add(II)V

    .line 1142
    invoke-static {v11}, Lcom/cigna/coach/utils/CalendarUtil;->getEndOfDayBefore(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 1145
    .local v3, "betweenStart":J
    invoke-static {v11}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDayAfter(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 1148
    .local v1, "betweenEnd":J
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " SELECT DISTINCT  \tC.CNTNT_TXT AS MSSN_NAME  FROM  \tUSR_GOAL UG  \tLEFT JOIN USR_MSSN UM ON (UG.GOAL_ID = UM.GOAL_ID AND UG.USR_ID = UM.USR_ID)  \tJOIN MSSN M ON (UM.MSSN_ID = M.MSSN_ID)  \tJOIN CONTENT C ON (M.MSSN_CNTNT_ID = C.CNTNT_ID)  WHERE  \tUG.STAT_IND = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v14}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " AND UG.USR_ID = ? "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " AND UM.STAT_CD = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v14}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " AND UM.MSSN_START_DT BETWEEN ? AND ? "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 1162
    .local v10, "sql":Ljava/lang/String;
    const/4 v13, 0x3

    new-array v9, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v9, v13

    const/4 v13, 0x1

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v9, v13

    const/4 v13, 0x2

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v9, v13

    .line 1163
    .local v9, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 1166
    .local v5, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v13

    invoke-virtual {v13, v10, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 1167
    if-eqz v5, :cond_2

    .line 1168
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1170
    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v13

    if-nez v13, :cond_2

    .line 1171
    const/4 v13, 0x0

    invoke-interface {v5, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1172
    .local v7, "missionName":Ljava/lang/String;
    invoke-interface {v8, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1173
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1176
    .end local v7    # "missionName":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 1177
    .local v6, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v13, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error running query to getActiveMissionNamesWithSpecificDaysSinceMissionStartDate: for days ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1178
    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1180
    .end local v6    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v13

    if-eqz v5, :cond_1

    .line 1181
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v13

    .line 1180
    :cond_2
    if-eqz v5, :cond_3

    .line 1181
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 1183
    :cond_3
    return-object v8
.end method

.method public getCategoryForGoalIfItsOnTrack(Ljava/lang/String;I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 28
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalSeqId"    # I

    .prologue
    .line 360
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 361
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "Getting category for goal if its on track..."

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_0
    const/4 v6, 0x0

    .line 365
    .local v6, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v24

    .line 367
    .local v24, "userIdKey":I
    const-string v18, "SELECT     QG.CTGRY_ID,     UG.START_DT,     UG.CMPLTD_MSSN_CNT,     G.MAX_DAYS_BTWN_MSSN_NUM  FROM USR_GOAL UG JOIN GOAL G     ON G.GOAL_ID = UG.GOAL_ID JOIN LFSTYL_ASSMT_QUEST_GRP QG     ON UG.LFSTYL_ASSMT_QUEST_GRP_ID = QG.LFSTYL_ASSMT_QUEST_GRP_ID  WHERE     UG. USR_ID = ? AND     UG. SEQ_ID = ? "

    .line 405
    .local v18, "sql1":Ljava/lang/String;
    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    const/16 v25, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v16, v25

    const/16 v25, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v16, v25

    .line 407
    .local v16, "selectionArgs1":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 408
    .local v3, "c1":Landroid/database/Cursor;
    const/4 v5, -0x1

    .line 409
    .local v5, "categoryId":I
    const-wide/16 v10, -0x1

    .line 410
    .local v10, "goalStartDateInMillis":J
    const-wide/16 v7, -0x1

    .line 411
    .local v7, "completedMissionCount":J
    const-wide/16 v12, -0x1

    .line 413
    .local v12, "maxDaysBetweenMissionNumber":J
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 414
    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v25

    if-lez v25, :cond_1

    .line 415
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 416
    const-string v25, "CTGRY_ID"

    move-object/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 417
    const-string v25, "START_DT"

    move-object/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 418
    const-string v25, "CMPLTD_MSSN_CNT"

    move-object/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 419
    const-string v25, "MAX_DAYS_BTWN_MSSN_NUM"

    move-object/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v25

    move/from16 v0, v25

    int-to-long v12, v0

    .line 425
    :cond_1
    if-eqz v3, :cond_2

    .line 426
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 428
    :cond_2
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 429
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "categoryId = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "goalStartDateInMillis = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "completedMissionCount = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v27, "maxDaysBetweenMissionNumber = "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :cond_3
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v22

    .line 439
    .local v22, "todayInMillis":J
    const-wide/16 v25, 0x0

    cmp-long v25, v7, v25

    if-nez v25, :cond_6

    .line 440
    const-wide/32 v25, 0x5265c00

    mul-long v25, v25, v12

    add-long v20, v10, v25

    .line 441
    .local v20, "thresholdInMillis":J
    cmp-long v25, v20, v22

    if-lez v25, :cond_4

    .line 443
    invoke-static {v5}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v6

    .line 496
    .end local v20    # "thresholdInMillis":J
    :cond_4
    :goto_0
    return-object v6

    .line 421
    .end local v22    # "todayInMillis":J
    :catch_0
    move-exception v9

    .line 422
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Error getting category for goal if its on track(1): "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 423
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 425
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v25

    if-eqz v3, :cond_5

    .line 426
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v25

    .line 447
    .restart local v22    # "todayInMillis":J
    :cond_6
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "SELECT COUNT (*)  FROM GOAL G  JOIN USR_GOAL UG     ON G.GOAL_ID = UG.GOAL_ID  WHERE     UG.USR_ID = ? AND     UG.SEQ_ID = ?   AND ((UG.LAST_MSSN_CNTD_DT + (G.MAX_DAYS_BTWN_MSSN_NUM * 86400000)) > "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ")"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 474
    .local v19, "sql2":Ljava/lang/String;
    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v25, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v17, v25

    const/16 v25, 0x1

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v26

    aput-object v26, v17, v25

    .line 475
    .local v17, "selectionArgs2":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 477
    .local v4, "c2":Landroid/database/Cursor;
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 478
    if-eqz v4, :cond_7

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v25

    if-lez v25, :cond_7

    .line 479
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 480
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 482
    .local v14, "rowsFound":J
    const-wide/16 v25, 0x0

    cmp-long v25, v14, v25

    if-lez v25, :cond_7

    .line 484
    invoke-static {v5}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 491
    .end local v14    # "rowsFound":J
    :cond_7
    if-eqz v4, :cond_4

    .line 492
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 487
    :catch_1
    move-exception v9

    .line 488
    .restart local v9    # "e":Ljava/lang/RuntimeException;
    :try_start_3
    sget-object v25, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Error getting category for goal if its on track(2): "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 489
    throw v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 491
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v25

    if-eqz v4, :cond_8

    .line 492
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v25
.end method

.method public getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)Ljava/util/List;
    .locals 8
    .param p1, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 609
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 610
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Getting coach messages..."

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 614
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const-string v2, "CNTXT_ID = ? AND MSG_TY_CD = ?  AND IS_ENBLD = 1"

    .line 616
    .local v2, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 618
    .local v3, "selectionArgs":[Ljava/lang/String;
    const-string v6, "DISPL_RANK_NUM DESC"

    .line 619
    .local v6, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    move-object v0, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 621
    return-object v7
.end method

.method public getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)Ljava/util/List;
    .locals 9
    .param p1, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 626
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Getting coach messages: contextType:"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ", coachMessageType:"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ", contextFilterType:"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 632
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const-string v2, "CNTXT_ID = ? AND MSG_TY_CD = ? AND CNTXT_FILTER_CD = ?  AND IS_ENBLD = 1"

    .line 635
    .local v2, "selection":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 637
    .local v3, "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 638
    .local v4, "groupBy":Ljava/lang/String;
    const/4 v5, 0x0

    .line 639
    .local v5, "having":Ljava/lang/String;
    const-string v6, "DISPL_RANK_NUM DESC"

    .line 640
    .local v6, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 641
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 642
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Got coach messages: "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    :cond_1
    return-object v7
.end method

.method public getCoachMessages(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;I)Ljava/util/List;
    .locals 9
    .param p1, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p4, "referenceType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;
    .param p5, "referenceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 650
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 651
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Getting coach messages..."

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 655
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const-string v2, "CNTXT_ID = ? AND MSG_TY_CD = ? AND CNTXT_FILTER_CD = ? AND REF_TY_IND = ? AND REF_ID = ? AND IS_ENBLD = 1"

    .line 659
    .local v2, "selection":Ljava/lang/String;
    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->getReferenceType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 661
    .local v3, "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 662
    .local v4, "groupBy":Ljava/lang/String;
    const/4 v5, 0x0

    .line 663
    .local v5, "having":Ljava/lang/String;
    const-string v6, "DISPL_RANK_NUM DESC"

    .line 664
    .local v6, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 665
    return-object v7
.end method

.method public getCoachMessagesBasedOnMinMaxValues(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;
    .locals 9
    .param p1, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 858
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 859
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Getting coach messages..."

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 863
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const-string v2, "CNTXT_ID = ? AND MSG_TY_CD = ? AND CNTXT_FILTER_CD = ? AND MIN_VAL <= ?  AND MAX_VAL >= ?  AND IS_ENBLD = 1"

    .line 866
    .local v2, "selection":Ljava/lang/String;
    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 869
    .local v3, "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 870
    .local v4, "groupBy":Ljava/lang/String;
    const/4 v5, 0x0

    .line 871
    .local v5, "having":Ljava/lang/String;
    const-string v6, "DISPL_RANK_NUM DESC"

    .line 873
    .local v6, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 875
    return-object v7
.end method

.method public getCoachMessagesBasedOnMinMaxValuesAndRefId(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;II)Ljava/util/List;
    .locals 9
    .param p1, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p4, "value"    # I
    .param p5, "refId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 881
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 882
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Getting coach messages..."

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 886
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const-string v2, "CNTXT_ID = ? AND MSG_TY_CD = ? AND CNTXT_FILTER_CD = ? AND REF_ID = ? AND MIN_VAL <= ?  AND MAX_VAL >= ?  AND IS_ENBLD = 1"

    .line 890
    .local v2, "selection":Ljava/lang/String;
    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 893
    .local v3, "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 894
    .local v4, "groupBy":Ljava/lang/String;
    const/4 v5, 0x0

    .line 895
    .local v5, "having":Ljava/lang/String;
    const-string v6, "DISPL_RANK_NUM DESC"

    .line 897
    .local v6, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 899
    return-object v7
.end method

.method public getCoachMessagesBasedOnMinValueAndInterval(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)Ljava/util/List;
    .locals 9
    .param p1, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p4, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 906
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 907
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Getting coach messages..."

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 911
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const-string v2, "CNTXT_ID = ? AND MSG_TY_CD = ? AND CNTXT_FILTER_CD = ? AND MIN_VAL <= ?  AND MAX_VAL >= ?  AND IS_ENBLD = 1"

    .line 915
    .local v2, "selection":Ljava/lang/String;
    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v8

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, ""

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 918
    .local v3, "selectionArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 919
    .local v4, "groupBy":Ljava/lang/String;
    const/4 v5, 0x0

    .line 920
    .local v5, "having":Ljava/lang/String;
    const-string v6, "DISPL_RANK_NUM DESC"

    .line 922
    .local v6, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 923
    invoke-virtual {p0, v7, p4}, Lcom/cigna/coach/db/CoachMessageDBManager;->checkForDisplayInterval(Ljava/util/List;I)V

    .line 925
    return-object v7
.end method

.method public getCoachMessagesForGoalOnTrackWithCategory(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)Ljava/util/List;
    .locals 8
    .param p1, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p2, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p3, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p4, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
            "Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CoachMessage;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 570
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 571
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "input parameters contextType,coachMessageType,contextFilterType,categoryType can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 573
    :cond_1
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 574
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Getting coach messages for goal on track with category..."

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Category is:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p4}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 579
    .local v7, "coachMsgList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CoachMessage;>;"
    const-string v2, "REF_TY_IND = ? AND REF_ID = ? AND CNTXT_ID = ? AND MSG_TY_CD = ? AND CNTXT_FILTER_CD = ?  AND IS_ENBLD = 1"

    .line 594
    .local v2, "selection":Ljava/lang/String;
    const/4 v0, 0x5

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->CATEGORY:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->getReferenceType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x1

    invoke-virtual {p4}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x3

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x4

    invoke-virtual {p3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v0

    .line 600
    .local v3, "selectionArgs":[Ljava/lang/String;
    const-string v6, "DISPL_RANK_NUM DESC"

    .line 602
    .local v6, "orderBy":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    move-object v0, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/cigna/coach/db/CoachMessageDBManager;->performCoachDBQuery([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 604
    return-object v7
.end method

.method public getLatestCategoryScore(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 998
    const/4 v2, -0x1

    .line 999
    .local v2, "returnValue":I
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1000
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Preparing to run query to get latest user score for category:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_0
    iget-object v4, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 1004
    .local v3, "userIdKey":I
    const/4 v0, 0x0

    .line 1006
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, " SELECT SCORE_VAL FROM LFSTYL_ASSMT_SCORE WHERE USR_ID   = ? AND (SCORE_TY_IND   = ? OR SCORE_TY_IND   = ?) AND SCORE_TY_ID   = ?  ORDER BY LFSTYL_ASSMT_TIMESTMP DESC"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v9}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1009
    if-eqz v0, :cond_1

    .line 1010
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1011
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1012
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1013
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1014
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Latest score in DB is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1026
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 1027
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1030
    :cond_2
    return v2

    .line 1017
    :cond_3
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1018
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "No entries for this category before the reference timestamp"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1022
    :catch_0
    move-exception v1

    .line 1023
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v4, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error getting getLatestCategoryScore, query: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1024
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1026
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_4

    .line 1027
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v4
.end method

.method public getLatestLifeStyleScoreDate(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 151
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "Getting max overall life style score date for user..."

    invoke-static {v1, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v14

    .line 155
    .local v14, "userIdKey":I
    const/4 v13, 0x0

    .line 157
    .local v13, "returnValue":Ljava/util/Calendar;
    const-string v2, "LFSTYL_ASSMT_SCORE"

    .line 158
    .local v2, "table":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v15, "MAX(LFSTYL_ASSMT_TIMESTMP)"

    aput-object v15, v3, v1

    .line 159
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, "USR_ID = ? AND SCORE_TY_IND = ?"

    .line 160
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    const/4 v1, 0x1

    sget-object v15, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v15}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .line 161
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 162
    .local v6, "groupBy":Ljava/lang/String;
    const/4 v7, 0x0

    .line 163
    .local v7, "having":Ljava/lang/String;
    const/4 v8, 0x0

    .line 164
    .local v8, "orderBy":Ljava/lang/String;
    const/4 v9, 0x0

    .line 166
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 167
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 168
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 169
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 170
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 171
    .local v11, "maxScoreDateAsLong":J
    invoke-static {v11, v12}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 179
    .end local v11    # "maxScoreDateAsLong":J
    :cond_1
    if-eqz v9, :cond_2

    .line 180
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 182
    :cond_2
    return-object v13

    .line 175
    :catch_0
    move-exception v10

    .line 176
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error getting max overall score date for user: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v1, v15, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 177
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 179
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_3

    .line 180
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public getListOfCategoryIdsForOnTrackGoals(Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 500
    sget-object v7, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 501
    sget-object v7, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Getting list of category Ids for on track goals..."

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 505
    .local v2, "categoryTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    const-string v6, "SELECT DISTINCT QG.CTGRY_ID FROM GOAL G JOIN USR_GOAL UG ON G.GOAL_ID = UG.GOAL_ID JOIN LFSTYL_ASSMT_QUEST_GRP QG ON UG.LFSTYL_ASSMT_QUEST_GRP_ID = QG.LFSTYL_ASSMT_QUEST_GRP_ID WHERE  UG. USR_ID = ? AND  (UG.LAST_MSSN_CNTD_DT + (G.MAX_DAYS_BTWN_MSSN_NUM * 86400000)) > ?"

    .line 542
    .local v6, "sql":Ljava/lang/String;
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 543
    .local v3, "currentTimeInMillis":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v5, v7, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v9

    const/4 v7, 0x1

    aput-object v3, v5, v7

    .line 545
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 547
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-virtual {v7, v6, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_2

    .line 549
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 551
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_2

    .line 552
    const/4 v7, 0x0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v1

    .line 553
    .local v1, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 557
    .end local v1    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :catch_0
    move-exception v4

    .line 558
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v7, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error getting list of category Ids for on track goals: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 559
    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 561
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_1

    .line 562
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v7

    .line 561
    :cond_2
    if-eqz v0, :cond_3

    .line 562
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 565
    :cond_3
    return-object v2
.end method

.method public getMaxScoreDate(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 116
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "Getting max score date for user..."

    invoke-static {v1, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v14

    .line 120
    .local v14, "userIdKey":I
    const/4 v13, 0x0

    .line 122
    .local v13, "returnValue":Ljava/util/Calendar;
    const-string v2, "LFSTYL_ASSMT_SCORE"

    .line 123
    .local v2, "table":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v15, "MAX(LFSTYL_ASSMT_TIMESTMP)"

    aput-object v15, v3, v1

    .line 124
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, "USR_ID = ? "

    .line 125
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .line 126
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 127
    .local v6, "groupBy":Ljava/lang/String;
    const/4 v7, 0x0

    .line 128
    .local v7, "having":Ljava/lang/String;
    const/4 v8, 0x0

    .line 129
    .local v8, "orderBy":Ljava/lang/String;
    const/4 v9, 0x0

    .line 131
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 132
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 133
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 134
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 135
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    .line 136
    .local v11, "maxScoreDateAsLong":J
    invoke-static {v11, v12}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 144
    .end local v11    # "maxScoreDateAsLong":J
    :cond_1
    if-eqz v9, :cond_2

    .line 145
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 147
    :cond_2
    return-object v13

    .line 140
    :catch_0
    move-exception v10

    .line 141
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error getting max score date for user: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v1, v15, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 142
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_3

    .line 145
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public getMinScore(Ljava/lang/String;)I
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 190
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Getting min score for user..."

    invoke-static {v0, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v11

    .line 194
    .local v11, "userIdKey":I
    const/4 v10, -0x1

    .line 196
    .local v10, "minScore":I
    const-string v1, "LFSTYL_ASSMT_SCORE"

    .line 197
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v12, "MIN(SCORE_VAL)"

    aput-object v12, v2, v0

    .line 198
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "USR_ID = ? "

    .line 199
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v0

    .line 200
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 201
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 202
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 203
    .local v7, "orderBy":Ljava/lang/String;
    const/4 v8, 0x0

    .line 205
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 206
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 207
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 208
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 214
    :cond_1
    if-eqz v8, :cond_2

    .line 215
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 217
    :cond_2
    return v10

    .line 210
    :catch_0
    move-exception v9

    .line 211
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error getting min score for user: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v0, v12, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 212
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 215
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getNumberOfActiveMissions(Ljava/lang/String;I)I
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I

    .prologue
    const/4 v8, 0x0

    .line 318
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 319
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Getting number of active missions..."

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :cond_0
    const/4 v2, 0x0

    .line 322
    .local v2, "numberOfActiveMissions":I
    iget-object v6, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v6}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 324
    .local v5, "userIdKey":I
    const-string v4, "SELECT COUNT (*) FROM USR_MSSN WHERE     USR_ID = ? AND     GOAL_ID = ? AND     STAT_CD = ? "

    .line 338
    .local v4, "sql":Ljava/lang/String;
    const/4 v6, 0x3

    new-array v3, v6, [Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v8

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x2

    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v7}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    .line 341
    .local v3, "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 343
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-virtual {v6, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 344
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_1

    .line 345
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 346
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 352
    :cond_1
    if-eqz v0, :cond_2

    .line 353
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 356
    :cond_2
    return v2

    .line 348
    :catch_0
    move-exception v1

    .line 349
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error getting number of active missions..."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 350
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_3

    .line 353
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v6
.end method

.method public getNumberOfAvailableGoals(Ljava/lang/String;)I
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 287
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Getting number of available goals..."

    invoke-static {v0, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v11

    .line 291
    .local v11, "userIdKey":I
    const/4 v9, -0x1

    .line 293
    .local v9, "count":I
    const-string v1, "USR_GOAL"

    .line 294
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v12, "COUNT(*)"

    aput-object v12, v2, v0

    .line 295
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "USR_ID = ? AND STAT_IND = ?"

    .line 296
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v0

    const/4 v0, 0x1

    sget-object v12, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v0

    .line 297
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 298
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 299
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 300
    .local v7, "orderBy":Ljava/lang/String;
    const/4 v8, 0x0

    .line 302
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 303
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 304
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 305
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 311
    :cond_1
    if-eqz v8, :cond_2

    .line 312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 314
    :cond_2
    return v9

    .line 307
    :catch_0
    move-exception v10

    .line 308
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error getting number of available goals: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v0, v12, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 309
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 311
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 312
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getNumberOfScores(Ljava/lang/String;)I
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 221
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Getting number of lifestyle assessment scores..."

    invoke-static {v0, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v11

    .line 225
    .local v11, "userIdKey":I
    const/4 v9, -0x1

    .line 227
    .local v9, "count":I
    const-string v1, "LFSTYL_ASSMT_SCORE"

    .line 228
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v12, "COUNT(*)"

    aput-object v12, v2, v0

    .line 229
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "USR_ID = ? "

    .line 230
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v0

    .line 231
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 232
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 233
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 234
    .local v7, "orderBy":Ljava/lang/String;
    const/4 v8, 0x0

    .line 236
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 237
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 238
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 239
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 245
    :cond_1
    if-eqz v8, :cond_2

    .line 246
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 248
    :cond_2
    return v9

    .line 241
    :catch_0
    move-exception v10

    .line 242
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error getting number of lifestyle assessment scores: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v0, v12, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 243
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 245
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 246
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getUserCoachMessageActivityCount(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;III)I
    .locals 22
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p3, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p4, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p5, "minValue"    # I
    .param p6, "maxValue"    # I
    .param p7, "minMaxRefValue"    # I

    .prologue
    .line 693
    const/4 v12, 0x0

    .line 694
    .local v12, "activityCount":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    .line 695
    .local v13, "todayInMillis":J
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 696
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Getting user coach message count"

    invoke-static {v2, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v2}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v21

    .line 700
    .local v21, "userIdKey":I
    const-string v3, "USR_COACH_MSG"

    .line 701
    .local v3, "table":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v7, "ACTY_CNT"

    aput-object v7, v4, v2

    .line 703
    .local v4, "columns":[Ljava/lang/String;
    const-string v5, "USR_ID = ? AND CNTXT_ID = ? AND CNTXT_FILTER_CD = ? AND MSG_TY_CD = ? AND MIN_VAL = ? AND MAX_VAL = ? AND MIN_MAX_REF_CD = ?"

    .line 706
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x7

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x2

    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x3

    invoke-virtual/range {p3 .. p3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x4

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x5

    invoke-static/range {p6 .. p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x6

    invoke-static/range {p7 .. p7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    .line 714
    .local v6, "selectionArgs":[Ljava/lang/String;
    const/16 v18, 0x0

    .line 716
    .local v18, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 717
    if-eqz v18, :cond_4

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_4

    .line 718
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_2

    .line 719
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    .line 720
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 723
    add-int/lit8 v12, v12, 0x1

    move-object/from16 v7, p0

    move/from16 v8, v21

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move/from16 v15, p5

    move/from16 v16, p6

    move/from16 v17, p7

    .line 724
    invoke-direct/range {v7 .. v17}, Lcom/cigna/coach/db/CoachMessageDBManager;->updateUserCoachMessageActivityCount(ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;IJIII)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 746
    :goto_0
    if-eqz v18, :cond_1

    .line 747
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 750
    :cond_1
    return v12

    .line 726
    :cond_2
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "More than one entry found in the USR_COACH_MSG for keys [USR_ID = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ", CNTXT_ID = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ", CNTXT_FILTER_CD = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ", MSG_TY_CD = "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 734
    .local v20, "msg":Ljava/lang/String;
    new-instance v19, Ljava/lang/UnsupportedOperationException;

    invoke-direct/range {v19 .. v20}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 735
    .local v19, "e":Ljava/lang/UnsupportedOperationException;
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v2, v0, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 736
    throw v19
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 742
    .end local v19    # "e":Ljava/lang/UnsupportedOperationException;
    .end local v20    # "msg":Ljava/lang/String;
    :catch_0
    move-exception v19

    .line 743
    .local v19, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error getting UserCoachMessageActivityCount, query: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-static {v2, v7, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 744
    throw v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 746
    .end local v19    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_3

    .line 747
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    :cond_4
    move-object/from16 v7, p0

    move/from16 v8, v21

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move/from16 v15, p5

    move/from16 v16, p6

    move/from16 v17, p7

    .line 740
    :try_start_3
    invoke-direct/range {v7 .. v17}, Lcom/cigna/coach/db/CoachMessageDBManager;->insertUserCoachMessage(ILcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;IJIII)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0
.end method

.method public getUserCoachMessageLastDeliveredTime(Ljava/lang/String;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;I)J
    .locals 15
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "contextType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .param p3, "coachMessageType"    # Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;
    .param p4, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .param p5, "minValue"    # I

    .prologue
    .line 816
    const-wide/16 v12, 0x0

    .line 817
    .local v12, "lastDeliveredTime":J
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 818
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting user coach message last delivered time"

    invoke-static {v1, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 820
    :cond_0
    iget-object v1, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v14

    .line 822
    .local v14, "userIdKey":I
    const-string v2, "USR_COACH_MSG"

    .line 823
    .local v2, "table":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "LAST_DELIVERED_TIME"

    aput-object v6, v3, v1

    .line 825
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, "USR_ID = ? AND CNTXT_ID = ? AND CNTXT_FILTER_CD = ? AND MSG_TY_CD = ? AND MIN_VAL = ?"

    .line 828
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x5

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->getContextType()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p4 .. p4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p3 .. p3}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getMessageType()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const/4 v1, 0x4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    .line 834
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 836
    .local v10, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 837
    if-eqz v10, :cond_1

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 838
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v6, 0x1

    if-ne v1, v6, :cond_1

    .line 839
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 841
    const-string v1, "LAST_DELIVERED_TIME"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v12

    .line 848
    :cond_1
    if-eqz v10, :cond_2

    .line 849
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 852
    :cond_2
    return-wide v12

    .line 844
    :catch_0
    move-exception v11

    .line 845
    .local v11, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error getting UserCoachMessageActivityCount, query: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 846
    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 848
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v10, :cond_3

    .line 849
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public getUserCreateDate(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 84
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v15, "Getting user create date..."

    invoke-static {v1, v15}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v14

    .line 88
    .local v14, "userIdKey":I
    const/4 v13, 0x0

    .line 90
    .local v13, "returnVal":Ljava/util/Calendar;
    const-string v2, "USR_INFO"

    .line 91
    .local v2, "table":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v15, "USR_INFO_DT"

    aput-object v15, v3, v1

    .line 92
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, "USR_ID = ? "

    .line 93
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v5, v1

    .line 94
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 95
    .local v6, "groupBy":Ljava/lang/String;
    const/4 v7, 0x0

    .line 96
    .local v7, "having":Ljava/lang/String;
    const/4 v8, 0x0

    .line 97
    .local v8, "orderBy":Ljava/lang/String;
    const/4 v9, 0x0

    .line 99
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 100
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 101
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 102
    const-string v1, "USR_INFO_DT"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 103
    .local v10, "createDate":J
    invoke-static {v10, v11}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 109
    .end local v10    # "createDate":J
    :cond_1
    if-eqz v9, :cond_2

    .line 110
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 112
    :cond_2
    return-object v13

    .line 105
    :catch_0
    move-exception v12

    .line 106
    .local v12, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Error getting user create date: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v1, v15, v12}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 107
    throw v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    .end local v12    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_3

    .line 110
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public getUserGoalStartDate(Ljava/lang/String;I)Ljava/util/Calendar;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalSeqId"    # I

    .prologue
    .line 1377
    const/4 v4, 0x0

    .line 1378
    .local v4, "returnDate":Ljava/util/Calendar;
    const-wide/16 v2, -0x1

    .line 1379
    .local v2, "milliseconds":J
    iget-object v6, p0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v6}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 1380
    .local v5, "userIdKey":I
    const/4 v0, 0x0

    .line 1382
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, " SELECT START_DT FROM USR_GOAL WHERE USR_ID   = ? AND SEQ_ID   = ? "

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1384
    if-eqz v0, :cond_0

    .line 1385
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1386
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1387
    const-string v6, "START_DT"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 1398
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1399
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1401
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-eqz v6, :cond_2

    .line 1402
    invoke-static {v2, v3}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v4

    .line 1404
    :cond_2
    return-object v4

    .line 1389
    :cond_3
    :try_start_1
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1390
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "No entries for this category before the reference timestamp"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1394
    :catch_0
    move-exception v1

    .line 1395
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error getting getLatestCategoryScore, query: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1396
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1398
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_4

    .line 1399
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v6
.end method

.method public hasActiveMissionsWithNoActivityWithSpecificDaysSinceMissionStartDate(Ljava/lang/String;I[I)Z
    .locals 25
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "days"    # I
    .param p3, "missionFrequency"    # [I

    .prologue
    .line 1034
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 1035
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Preparing to run query for hasActiveMissionsWithNoActivityWithSpecificDaysSinceMissionStartDate: for days ="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    :cond_0
    const/16 v17, 0x0

    .line 1039
    .local v17, "returnVal":Z
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v21

    .line 1041
    .local v21, "userKey":I
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v20

    .line 1043
    .local v20, "targetDate":Ljava/util/Calendar;
    const/16 v22, 0x6

    move/from16 v0, p2

    neg-int v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1046
    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/CalendarUtil;->getEndOfDayBefore(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v9

    .line 1047
    .local v9, "betweenStartCal":Ljava/util/Calendar;
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 1048
    .local v7, "betweenStart":J
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_1

    .line 1049
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "hasActiveMissionsWithNoActivityWithSpecificDaysSinceMissionStartDate::betweenStart="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-static {v9}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    :cond_1
    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDayAfter(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v6

    .line 1056
    .local v6, "betweenEndCal":Ljava/util/Calendar;
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 1057
    .local v4, "betweenEnd":J
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 1058
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "hasActiveMissionsWithNoActivityWithSpecificDaysSinceMissionStartDate::betweenEnd="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-static {v6}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    :cond_2
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, " SELECT  \tCOUNT(*)  FROM  \tUSR_GOAL UG  \tJOIN USR_MSSN UM ON (UG.GOAL_ID = UM.GOAL_ID AND UG.USR_ID = UM.USR_ID)  WHERE  \tUG.STAT_IND = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND UM.STAT_CD = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-object v23, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual/range {v23 .. v23}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND UG.USR_ID = ? "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND UM.USR_MSSN_FREQ IN ("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, p3

    array-length v0, v0

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Lcom/cigna/coach/db/CoachMessageDBManager;->makePlaceholders(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ") "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND UM.MSSN_START_DT BETWEEN ? AND ? "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " AND NOT EXISTS ( "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \tSELECT "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \t\tUMD.MSSN_ID, "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \t\tUMD.MSSN_SEQ_ID "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \tFROM "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \t\tUSR_MSSN_DATA UMD "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \tWHERE "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \t\tUM.MSSN_ID = UMD.MSSN_ID "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " \tAND UM.MSSN_SEQ_ID = UMD.MSSN_SEQ_ID "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " ) "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 1089
    .local v19, "sql":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1090
    .local v15, "listOfSelectionArgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1092
    move-object/from16 v3, p3

    .local v3, "arr$":[I
    array-length v14, v3

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_0
    if-ge v13, v14, :cond_3

    aget v12, v3, v13

    .line 1093
    .local v12, "f":I
    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1092
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 1096
    .end local v12    # "f":I
    :cond_3
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1097
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1099
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v15, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [Ljava/lang/String;

    .line 1101
    .local v18, "selectionArgs":[Ljava/lang/String;
    const/4 v10, 0x0

    .line 1104
    .local v10, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1105
    if-eqz v10, :cond_5

    .line 1106
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1108
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 1109
    .local v16, "missionCount":I
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v22 .. v22}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1110
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "hasActiveMissionsWithNoActivityWithSpecificDaysSinceMissionStartDate::missionCount="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1113
    :cond_4
    if-lez v16, :cond_5

    .line 1114
    const/16 v17, 0x1

    .line 1121
    .end local v16    # "missionCount":I
    :cond_5
    if-eqz v10, :cond_6

    .line 1122
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1125
    :cond_6
    return v17

    .line 1117
    :catch_0
    move-exception v11

    .line 1118
    .local v11, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v22, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Error running query for hasActiveMissionsWithNoActivityWithSpecificDaysSinceMissionStartDate: for days ="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v0, v1, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1119
    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1121
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v22

    if-eqz v10, :cond_7

    .line 1122
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v22
.end method

.method public hasScoreType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)Z
    .locals 16
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "scoreType"    # Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    .prologue
    .line 252
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 253
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Determining if user has score of type: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v1, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_0
    const/4 v12, 0x0

    .line 256
    .local v12, "returnVal":Z
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/db/CoachMessageDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v13

    .line 257
    .local v13, "userIdKey":I
    const/4 v10, -0x1

    .line 259
    .local v10, "count":I
    const-string v2, "LFSTYL_ASSMT_SCORE"

    .line 260
    .local v2, "table":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v14, "COUNT(*)"

    aput-object v14, v3, v1

    .line 261
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, "USR_ID = ? AND SCORE_TY_IND = ?"

    .line 262
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v1

    const/4 v1, 0x1

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v5, v1

    .line 263
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 264
    .local v6, "groupBy":Ljava/lang/String;
    const/4 v7, 0x0

    .line 265
    .local v7, "having":Ljava/lang/String;
    const/4 v8, 0x0

    .line 266
    .local v8, "orderBy":Ljava/lang/String;
    const/4 v9, 0x0

    .line 268
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 269
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 270
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 271
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 272
    if-lez v10, :cond_1

    .line 273
    const/4 v12, 0x1

    .line 280
    :cond_1
    if-eqz v9, :cond_2

    .line 281
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 283
    :cond_2
    return v12

    .line 276
    :catch_0
    move-exception v11

    .line 277
    .local v11, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Error determining if user has score of type: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v1, v14, v11}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 278
    throw v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 280
    .end local v11    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_3

    .line 281
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method public hasSpecificDaysPassedSinceLastMissionComplete(IILjava/lang/String;)Z
    .locals 12
    .param p1, "goalId"    # I
    .param p2, "days"    # I
    .param p3, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 1303
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1304
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Getting count of User Missions with no activity: START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1307
    .local v0, "NOW":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1308
    .local v4, "returnValue":Z
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p3}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 1310
    .local v5, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/db/MissionDBQueries;->GetUserMissionDetails_SELECT_USER_MISSIONS_HAVING_PROGRESS_QUERY:Ljava/lang/String;

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    aput-object v0, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1313
    .local v1, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1315
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1317
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    .line 1318
    .local v3, "missionCount":I
    if-lez v3, :cond_1

    .line 1319
    const/4 v4, 0x1

    .line 1327
    .end local v3    # "missionCount":I
    :cond_1
    if-eqz v1, :cond_2

    .line 1328
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1332
    :cond_2
    return v4

    .line 1323
    :catch_0
    move-exception v2

    .line 1324
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v6, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Error getting missions with activity: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1325
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1327
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    if-eqz v1, :cond_3

    .line 1328
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v6
.end method

.method public noOfDaysPassedSinceLastMissionActivity(Ljava/lang/String;)I
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 1250
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1251
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v12, "noOfDaysPassedSinceLastMissionActivity: START"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1253
    :cond_0
    const/4 v9, -0x1

    .line 1255
    .local v9, "noOfDaysPassedSinceLastMission":I
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v10

    .line 1257
    .local v10, "userKey":I
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    const-string v12, "SELECT MSSN_START_DT , MSSN_END_DT FROM USR_MSSN WHERE USR_ID = ? ORDER BY MSSN_START_DT , MSSN_END_DT DESC"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1259
    .local v1, "c":Landroid/database/Cursor;
    const-wide/16 v5, 0x0

    .line 1260
    .local v5, "latestMissionEndDate":J
    const-wide/16 v7, 0x0

    .line 1261
    .local v7, "latestMissionStartDate":J
    const-wide/16 v3, -0x1

    .line 1263
    .local v3, "latestChangeDateTime":J
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1264
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v11

    if-nez v11, :cond_5

    const/4 v11, 0x0

    invoke-interface {v1, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-nez v11, :cond_5

    .line 1265
    const-wide/16 v11, -0x1

    cmp-long v11, v5, v11

    if-eqz v11, :cond_1

    const-wide/16 v11, 0x0

    cmp-long v11, v5, v11

    if-nez v11, :cond_2

    .line 1266
    :cond_1
    const-string v11, "MSSN_END_DT"

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 1268
    :cond_2
    const-wide/16 v11, -0x1

    cmp-long v11, v7, v11

    if-eqz v11, :cond_3

    const-wide/16 v11, 0x0

    cmp-long v11, v7, v11

    if-nez v11, :cond_4

    .line 1269
    :cond_3
    const-string v11, "MSSN_START_DT"

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 1271
    :cond_4
    const-wide/16 v11, -0x1

    cmp-long v11, v5, v11

    if-eqz v11, :cond_8

    const-wide/16 v11, 0x0

    cmp-long v11, v5, v11

    if-eqz v11, :cond_8

    const-wide/16 v11, -0x1

    cmp-long v11, v7, v11

    if-eqz v11, :cond_8

    const-wide/16 v11, 0x0

    cmp-long v11, v7, v11

    if-eqz v11, :cond_8

    .line 1280
    :cond_5
    cmp-long v11, v5, v7

    if-lez v11, :cond_b

    .line 1281
    move-wide v3, v5

    .line 1285
    :goto_1
    const-wide/16 v11, 0x0

    cmp-long v11, v3, v11

    if-lez v11, :cond_6

    .line 1287
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfToday()Ljava/util/Calendar;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v11

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    sub-long/2addr v11, v13

    const-wide/32 v13, 0x5265c00

    div-long/2addr v11, v13
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    long-to-int v9, v11

    .line 1295
    :cond_6
    if-eqz v1, :cond_7

    .line 1296
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1299
    :cond_7
    return v9

    .line 1275
    :cond_8
    :try_start_1
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1276
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "latestMissionStartDate = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", latestMissionEndDate"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    :cond_9
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1291
    :catch_0
    move-exception v2

    .line 1292
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error noOfDaysPassedSinceLastMissionActivity: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1293
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1295
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v11

    if-eqz v1, :cond_a

    .line 1296
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_a
    throw v11

    .line 1283
    :cond_b
    move-wide v3, v7

    goto :goto_1
.end method

.method public noOfDaysPassedSinceLastMissionActivityForAGoal(Ljava/lang/String;I)I
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalSeqId"    # I

    .prologue
    .line 1187
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1188
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v12, "noOfDaysPassedSinceLastMissionActivity: START"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    :cond_0
    const/4 v9, -0x1

    .line 1192
    .local v9, "noOfDaysPassedSinceLastMission":I
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v10

    .line 1194
    .local v10, "userKey":I
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/CoachMessageDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    const-string v12, "SELECT MSSN_START_DT , MSSN_END_DT FROM USR_MSSN WHERE USR_ID = ?  AND GOAL_SEQ_ID = ?  ORDER BY MSSN_START_DT DESC, MSSN_END_DT DESC"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/String;

    const/4 v14, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, ""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1196
    .local v1, "c":Landroid/database/Cursor;
    const-wide/16 v5, 0x0

    .line 1197
    .local v5, "latestMissionEndDate":J
    const-wide/16 v7, 0x0

    .line 1198
    .local v7, "latestMissionStartDate":J
    const-wide/16 v3, -0x1

    .line 1199
    .local v3, "latestChangeDateTime":J
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1200
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Raw Query: SELECT MSSN_START_DT , MSSN_END_DT FROM USR_MSSN WHERE USR_ID = ?  AND GOAL_SEQ_ID = ?  ORDER BY MSSN_START_DT DESC, MSSN_END_DT DESC"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    :cond_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1205
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v11

    if-nez v11, :cond_7

    const/4 v11, 0x0

    invoke-interface {v1, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v11

    if-nez v11, :cond_7

    .line 1206
    const-wide/16 v11, -0x1

    cmp-long v11, v5, v11

    if-eqz v11, :cond_2

    const-wide/16 v11, 0x0

    cmp-long v11, v5, v11

    if-nez v11, :cond_3

    .line 1207
    :cond_2
    const-string v11, "MSSN_END_DT"

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 1209
    :cond_3
    const-wide/16 v11, -0x1

    cmp-long v11, v7, v11

    if-eqz v11, :cond_4

    const-wide/16 v11, 0x0

    cmp-long v11, v7, v11

    if-nez v11, :cond_5

    .line 1210
    :cond_4
    const-string v11, "MSSN_START_DT"

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 1212
    :cond_5
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1213
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "latestMissionStartDate = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", latestMissionEndDate"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    :cond_6
    const-wide/16 v11, -0x1

    cmp-long v11, v5, v11

    if-eqz v11, :cond_b

    const-wide/16 v11, 0x0

    cmp-long v11, v5, v11

    if-eqz v11, :cond_b

    const-wide/16 v11, -0x1

    cmp-long v11, v7, v11

    if-eqz v11, :cond_b

    const-wide/16 v11, 0x0

    cmp-long v11, v7, v11

    if-eqz v11, :cond_b

    .line 1221
    :cond_7
    cmp-long v11, v5, v7

    if-lez v11, :cond_d

    .line 1222
    move-wide v3, v5

    .line 1223
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1224
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "latestChangeDateTime = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    :cond_8
    :goto_1
    const-wide/16 v11, 0x0

    cmp-long v11, v3, v11

    if-lez v11, :cond_9

    .line 1234
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfToday()Ljava/util/Calendar;

    move-result-object v11

    invoke-virtual {v11}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v11

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v13

    sub-long/2addr v11, v13

    const-wide/32 v13, 0x5265c00

    div-long/2addr v11, v13
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    long-to-int v9, v11

    .line 1242
    :cond_9
    if-eqz v1, :cond_a

    .line 1243
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1246
    :cond_a
    return v9

    .line 1219
    :cond_b
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1238
    :catch_0
    move-exception v2

    .line 1239
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error noOfDaysPassedSinceLastMissionActivityByGoal: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1240
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1242
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v11

    if-eqz v1, :cond_c

    .line 1243
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_c
    throw v11

    .line 1227
    :cond_d
    move-wide v3, v7

    .line 1228
    :try_start_3
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1229
    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "latestChangeDateTime = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
