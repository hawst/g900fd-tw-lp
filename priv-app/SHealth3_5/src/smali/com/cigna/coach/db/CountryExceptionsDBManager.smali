.class public Lcom/cigna/coach/db/CountryExceptionsDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "CountryExceptionsDBManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;,
        Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    }
.end annotation


# static fields
.field public static final ALCOHOL_QUESTION_GROUP:I = 0x6

.field public static final ALCOHOL_RELATED_SUB_CTGRY_ID:I = 0xcd

.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 51
    return-void
.end method


# virtual methods
.method public getCountryCodeByMCCCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "mcc"    # Ljava/lang/String;

    .prologue
    .line 271
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getCountryCodeByMCCCode: START"

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_0
    const/4 v10, 0x0

    .line 276
    .local v10, "value":Ljava/lang/String;
    const/4 v8, 0x0

    .line 278
    .local v8, "c":Landroid/database/Cursor;
    if-nez p1, :cond_2

    .line 279
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v11, "mcc must not be null"

    invoke-direct {v0, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    :catch_0
    move-exception v9

    .line 298
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Error in getCountryCodeByMCCCode: "

    invoke-static {v0, v11, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 299
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 302
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 303
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 282
    :cond_2
    :try_start_2
    const-string v1, "COUNTRY_MCC_CODES"

    .line 283
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v11, "ISO_COUNTRY_CODE"

    aput-object v11, v2, v0

    .line 284
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "MCC_CODE = ?  "

    .line 285
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    .line 286
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 287
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 288
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 290
    .local v7, "orderBy":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 292
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 293
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 294
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 302
    :cond_3
    if-eqz v8, :cond_4

    .line 303
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_4
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 306
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getCountryCodeByMCCCode: END"

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    :cond_5
    return-object v10
.end method

.method public getCountryExceptionValue(Ljava/lang/String;Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;)Ljava/lang/String;
    .locals 13
    .param p1, "countryId"    # Ljava/lang/String;
    .param p2, "countryExceptionType"    # Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    .param p3, "referenceType"    # Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    .prologue
    .line 312
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getCountryExceptionValue: START"

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_0
    const/4 v10, 0x0

    .line 317
    .local v10, "value":Ljava/lang/String;
    const/4 v8, 0x0

    .line 319
    .local v8, "c":Landroid/database/Cursor;
    if-nez p1, :cond_2

    .line 320
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v11, "countryId must not be null"

    invoke-direct {v0, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 338
    :catch_0
    move-exception v9

    .line 339
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Error in getCountryExceptionValue: "

    invoke-static {v0, v11, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 340
    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 343
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 344
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 323
    :cond_2
    :try_start_2
    const-string v1, "CNTRY_EXCEP"

    .line 324
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v11, "EXCEP_REF_TXT"

    aput-object v11, v2, v0

    .line 325
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "CNTRY_ID = ? AND EXCEP_TY = ? AND EXCEP_REF_TY = ? "

    .line 326
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v0, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->getCountryExceptionType()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v0

    const/4 v0, 0x2

    invoke-virtual/range {p3 .. p3}, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->getReferenceType()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v0

    .line 327
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 328
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 329
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 331
    .local v7, "orderBy":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 333
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 334
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 335
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 343
    :cond_3
    if-eqz v8, :cond_4

    .line 344
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 346
    :cond_4
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 347
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "getCountryExceptionValue: END "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_5
    return-object v10
.end method

.method public getCountryUsingMCC()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "countryCode":Ljava/lang/String;
    const-string v1, ""

    .line 182
    .local v1, "mcc":Ljava/lang/String;
    iget-object v4, p0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string/jumbo v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 183
    .local v3, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "networkOperator":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v6, :cond_0

    .line 185
    const/4 v4, 0x0

    invoke-virtual {v2, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 186
    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0, v1}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->getCountryCodeByMCCCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    sget-object v4, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 189
    sget-object v4, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Country code for the MCC value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", is="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_0
    return-object v0
.end method

.method public shouldSuppress(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "countryId"    # Ljava/lang/String;
    .param p2, "currentAge"    # I

    .prologue
    .line 250
    const/4 v2, 0x1

    .line 251
    .local v2, "shouldSuppress":Z
    sget-object v3, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->ALCOHOL_SUPRESSION:Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    sget-object v4, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->AGE:Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    invoke-virtual {p0, p1, v3, v4}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->getCountryExceptionValue(Ljava/lang/String;Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;)Ljava/lang/String;

    move-result-object v1

    .line 253
    .local v1, "legalAgeAsString":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 254
    sget-object v3, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "ShouldSuppress Start "

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v3, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Current Age : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Legal Age : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_0
    if-eqz v1, :cond_1

    .line 262
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 263
    .local v0, "legalAge":I
    if-gt v0, p2, :cond_1

    .line 264
    const/4 v2, 0x0

    .line 267
    .end local v0    # "legalAge":I
    :cond_1
    return v2
.end method

.method public shouldSuppressAlcoholRelatedContent(Ljava/lang/String;)Z
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 55
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 56
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "shouldSuppressAlcoholRelatedContent: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Determining if user should see alcohol related content"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_0
    const/4 v4, 0x1

    .line 62
    .local v4, "shouldSuppress":Z
    :try_start_0
    new-instance v6, Lcom/cigna/coach/db/UserMetricsDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 64
    .local v6, "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    invoke-virtual {v6, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDateOfBirth(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v2

    .line 65
    .local v2, "dob":Ljava/util/Calendar;
    const/4 v0, 0x0

    .line 66
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 67
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 68
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "dob : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_1
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v5

    .line 71
    .local v5, "today":Ljava/util/Calendar;
    invoke-static {v2, v5}, Lcom/cigna/coach/utils/CalendarUtil;->absYearsBetween(Ljava/util/Calendar;Ljava/util/Calendar;)I

    move-result v1

    .line 73
    .local v1, "currentAge":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->getCountryUsingMCC()Ljava/lang/String;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_4

    .line 76
    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppress(Ljava/lang/String;I)Z

    move-result v4

    .line 78
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 79
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "shouldSuppressAlcoholRelatedContent: getCountryUsingMCC countryCode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", shouldSuppress ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    .end local v0    # "countryCode":Ljava/lang/String;
    .end local v1    # "currentAge":I
    .end local v2    # "dob":Ljava/util/Calendar;
    .end local v5    # "today":Ljava/util/Calendar;
    .end local v6    # "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :cond_2
    :goto_0
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 126
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "shouldSuppressAlcoholRelatedContent: userId: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", shouldSuppress="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v8, "shouldSuppressAlcoholRelatedContent: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_3
    return v4

    .line 98
    .restart local v0    # "countryCode":Ljava/lang/String;
    .restart local v1    # "currentAge":I
    .restart local v2    # "dob":Ljava/util/Calendar;
    .restart local v5    # "today":Ljava/util/Calendar;
    .restart local v6    # "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :cond_4
    :try_start_1
    invoke-virtual {v6, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_5

    .line 100
    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppress(Ljava/lang/String;I)Z

    move-result v4

    .line 102
    :cond_5
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 103
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "shouldSuppressAlcoholRelatedContent: Samsung countryCode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", shouldSuppress ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_6
    if-eqz v4, :cond_7

    if-nez v0, :cond_2

    .line 109
    :cond_7
    iget-object v7, p0, Lcom/cigna/coach/db/CountryExceptionsDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_8

    .line 111
    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppress(Ljava/lang/String;I)Z

    move-result v4

    .line 113
    :cond_8
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 114
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "shouldSuppressAlcoholRelatedContent: Locale countryCode "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", shouldSuppress ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 120
    .end local v0    # "countryCode":Ljava/lang/String;
    .end local v1    # "currentAge":I
    .end local v2    # "dob":Ljava/util/Calendar;
    .end local v5    # "today":Ljava/util/Calendar;
    .end local v6    # "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :catch_0
    move-exception v3

    .line 121
    .local v3, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 122
    sget-object v7, Lcom/cigna/coach/db/CountryExceptionsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "shouldSuppressAlcoholRelatedContent: Exceptoion: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
