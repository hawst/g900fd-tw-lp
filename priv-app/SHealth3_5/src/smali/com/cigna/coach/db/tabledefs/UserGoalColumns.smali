.class public Lcom/cigna/coach/db/tabledefs/UserGoalColumns;
.super Ljava/lang/Object;
.source "UserGoalColumns.java"


# static fields
.field public static final KEY_CMPLTD_MSSN_CNT:Ljava/lang/String; = "CMPLTD_MSSN_CNT"

.field public static final KEY_END_DT:Ljava/lang/String; = "END_DT"

.field public static final KEY_GOAL_ID:Ljava/lang/String; = "GOAL_ID"

.field public static final KEY_LAST_MSSN_CNTD_DT:Ljava/lang/String; = "LAST_MSSN_CNTD_DT"

.field public static final KEY_LFSTYL_ASSMT_CTRGY_SCORE:Ljava/lang/String; = "LFSTYL_ASSMT_CTRGY_SCORE"

.field public static final KEY_LFSTYL_ASSMT_QUEST_GRP_ID:Ljava/lang/String; = "LFSTYL_ASSMT_QUEST_GRP_ID"

.field public static final KEY_LFSTYL_ASSMT_QUEST_GRP_SCORE:Ljava/lang/String; = "LFSTYL_ASSMT_QUEST_GRP_SCORE"

.field public static final KEY_SEQ_ID:Ljava/lang/String; = "SEQ_ID"

.field public static final KEY_START_DT:Ljava/lang/String; = "START_DT"

.field public static final KEY_STAT_IND:Ljava/lang/String; = "STAT_IND"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "USR_GOAL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
