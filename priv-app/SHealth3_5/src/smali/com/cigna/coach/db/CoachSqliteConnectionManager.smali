.class public final Lcom/cigna/coach/db/CoachSqliteConnectionManager;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "CoachSqliteConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field static final DB_NAME:Ljava/lang/String; = "Coach.db"

.field public static final MASTER_TABLE_DDL:Ljava/lang/String; = "dbcreate"

.field public static final MASTER_TABLE_INSERT:Ljava/lang/String; = "dbinsertfields"

.field public static final RESTORE_SEQUENCE_FILENAME:Ljava/lang/String; = "backup_restore_sequence"

.field private static connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

.field static final contentInsertStatement:Ljava/lang/String;


# instance fields
.field context:Landroid/content/Context;

.field database:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 48
    const-class v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s,%s)"

    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "CONTENT"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "CNTNT_ID"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "LANG_CD"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "CNTRY_CD"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "CNTNT_TXT"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "CNTNT_KEYWRD"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "ACTV_IND"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string v4, "LOC_TY_IND"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string v4, "LANG_SPCF_CNTNT_IND"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "VALUES (%s,%s,%s)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->contentInsertStatement:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 182
    const-string v0, "Coach.db"

    const/4 v1, 0x4

    invoke-direct {p0, p1, v0, v2, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 51
    iput-object v2, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    .line 52
    iput-object v2, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 183
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    .line 184
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/cigna/coach/db/CoachSqliteConnectionManager;Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    .param p1, "x1"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "x2"    # [Ljava/lang/String;
    .param p3, "x3"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->insertDataToContentTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method private cacheContent([Ljava/lang/String;Z)V
    .locals 21
    .param p1, "data"    # [Ljava/lang/String;
    .param p2, "multiLanguage"    # Z

    .prologue
    .line 337
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 338
    if-eqz p2, :cond_2

    .line 339
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Caching multi lang content"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    .line 345
    .local v15, "startMillis":J
    const-wide/16 v13, 0x0

    .line 347
    .local v13, "numRowsCached":J
    const/4 v9, 0x0

    .line 348
    .local v9, "l":Ljava/util/Locale;
    const/4 v12, 0x0

    .line 350
    .local v12, "lt":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    if-eqz p1, :cond_3

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v17, v0

    if-lez v17, :cond_3

    .line 352
    move-object/from16 v1, p1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v11, v1

    .local v11, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_1
    if-ge v8, v11, :cond_4

    aget-object v6, v1, v8

    .line 354
    .local v6, "contentRow":Ljava/lang/String;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    if-lez v17, :cond_1

    .line 355
    const-string v17, "\\|"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 356
    .local v4, "columns":[Ljava/lang/String;
    const/16 v17, 0x0

    aget-object v17, v4, v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 357
    .local v5, "contentId":I
    const/16 v17, 0x1

    aget-object v17, v4, v17

    const/16 v18, 0x1

    const/16 v19, 0x1

    aget-object v19, v4, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    .line 358
    .local v10, "languageCode":Ljava/lang/String;
    const/16 v17, 0x2

    aget-object v17, v4, v17

    const/16 v18, 0x1

    const/16 v19, 0x2

    aget-object v19, v4, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 359
    .local v7, "countryCode":Ljava/lang/String;
    const/16 v17, 0x3

    aget-object v17, v4, v17

    const/16 v18, 0x1

    const/16 v19, 0x3

    aget-object v19, v4, v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v19

    add-int/lit8 v19, v19, -0x1

    invoke-virtual/range {v17 .. v19}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 361
    .local v3, "cntntTxt":Ljava/lang/String;
    new-instance v9, Ljava/util/Locale;

    .end local v9    # "l":Ljava/util/Locale;
    invoke-direct {v9, v10, v7}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    .restart local v9    # "l":Ljava/util/Locale;
    invoke-static {v9}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(Ljava/util/Locale;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v12

    .line 363
    if-eqz v12, :cond_1

    .line 364
    invoke-static {v5}, Lcom/cigna/coach/utils/ContentHelper;->getContentInfoFromCache(I)Lcom/cigna/coach/dataobjects/ContentInfo;

    move-result-object v2

    .line 365
    .local v2, "cInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    invoke-virtual {v2, v12, v3}, Lcom/cigna/coach/dataobjects/ContentInfo;->addContent(Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;)V

    .line 367
    const-wide/16 v17, 0x1

    add-long v13, v13, v17

    .line 352
    .end local v2    # "cInfo":Lcom/cigna/coach/dataobjects/ContentInfo;
    .end local v3    # "cntntTxt":Ljava/lang/String;
    .end local v4    # "columns":[Ljava/lang/String;
    .end local v5    # "contentId":I
    .end local v7    # "countryCode":Ljava/lang/String;
    .end local v10    # "languageCode":Ljava/lang/String;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 341
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v6    # "contentRow":Ljava/lang/String;
    .end local v8    # "i$":I
    .end local v9    # "l":Ljava/util/Locale;
    .end local v11    # "len$":I
    .end local v12    # "lt":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .end local v13    # "numRowsCached":J
    .end local v15    # "startMillis":J
    :cond_2
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Caching locale specific content"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 374
    .restart local v9    # "l":Ljava/util/Locale;
    .restart local v12    # "lt":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .restart local v13    # "numRowsCached":J
    .restart local v15    # "startMillis":J
    :cond_3
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 375
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "No data to cache"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_4
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 380
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " rows cached in "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    sub-long v19, v19, v15

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " milliseconds"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    :cond_5
    return-void
.end method

.method private createAndLoadReferenceTables(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;)V
    .locals 7
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 122
    .local p2, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 123
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "***********************************"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Printing the incoming map structure"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_0
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 127
    .local v3, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 128
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 129
    .local v2, "key":Ljava/lang/String;
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "File key:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    .end local v2    # "key":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "***********************************"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Executing table create scripts"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Getting file:dbcreate"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    const-string v4, "dbcreate"

    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 138
    .local v0, "fileContent":[Ljava/lang/String;
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 139
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " records"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    :cond_3
    invoke-direct {p0, p1, v0}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->executeCreateTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V

    .line 143
    const/4 v4, 0x0

    invoke-virtual {p0, p1, p2, v4}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->loadData(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;Z)V

    .line 144
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 145
    sget-object v4, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Reference data loaded"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    :cond_4
    return-void
.end method

.method private executeCreateTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "createScript"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 477
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 478
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Preparing to execute the SQL scripts from map"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    array-length v3, p2

    if-ge v1, v3, :cond_5

    .line 485
    aget-object v3, p2, v1

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 486
    .local v2, "sqlStatement":Ljava/lang/String;
    const-string v3, "/**"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 489
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 490
    const/16 v3, 0x7e

    const/16 v4, 0x3b

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    .line 492
    :cond_1
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 493
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 497
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 483
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 500
    :cond_4
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 501
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Ignoring comment:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 505
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 506
    .local v0, "e":Landroid/database/SQLException;
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 511
    .end local v0    # "e":Landroid/database/SQLException;
    :cond_5
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 512
    sget-object v3, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    :cond_6
    return-void
.end method

.method public static declared-synchronized getConnectionManager(Landroid/content/Context;)Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    const-class v1, Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    if-nez v0, :cond_1

    .line 189
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 190
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "connectionManager==null"

    invoke-static {v0, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_0
    new-instance v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    .line 195
    :cond_1
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private insertDataToContentTables(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "allLangData"    # [Ljava/lang/String;
    .param p3, "locSpecLangData"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 158
    if-eqz p2, :cond_2

    array-length v0, p2

    if-lez v0, :cond_2

    .line 159
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->insertContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V

    .line 160
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Multi language content loaded to db"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :cond_0
    :goto_0
    if-eqz p3, :cond_3

    array-length v0, p3

    if-lez v0, :cond_3

    .line 170
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p3, v0}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->insertContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V

    .line 171
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Locale specific content loaded to db"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_1
    :goto_1
    return-void

    .line 164
    :cond_2
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Multi language content data is empty"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_3
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Locale specific content data is empty"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized closeDatabase()V
    .locals 3

    .prologue
    .line 200
    monitor-enter p0

    :try_start_0
    const-class v1, Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 201
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "shutting down connectionManager"

    invoke-static {v0, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->connectionManager:Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    .line 205
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 209
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 209
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 200
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 3

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_1

    .line 214
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "database == null"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 220
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    sget-object v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Opened database version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->getVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected insertContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V
    .locals 25
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "data"    # [Ljava/lang/String;
    .param p3, "multiLanguage"    # Z

    .prologue
    .line 385
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 386
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Inserting data into content table"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    .line 389
    .local v19, "startMillis":J
    const-wide/16 v17, 0x0

    .line 390
    .local v17, "numRows":J
    const/4 v9, 0x1

    .line 391
    .local v9, "first":Z
    const/4 v12, 0x0

    .line 392
    .local v12, "l":Ljava/util/Locale;
    const/4 v15, 0x0

    .line 393
    .local v15, "lt":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    if-eqz p3, :cond_5

    const-string v16, "1"

    .line 395
    .local v16, "multiLangIndicator":Ljava/lang/String;
    :goto_0
    if-eqz p2, :cond_8

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v21, v0

    if-lez v21, :cond_8

    .line 396
    move-object/from16 v4, p2

    .local v4, "arr$":[Ljava/lang/String;
    array-length v14, v4

    .local v14, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v14, :cond_9

    aget-object v6, v4, v10

    .line 397
    .local v6, "contentRow":Ljava/lang/String;
    if-eqz v6, :cond_7

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    if-lez v21, :cond_7

    .line 398
    if-nez v9, :cond_1

    if-eqz p3, :cond_2

    .line 399
    :cond_1
    const-string v21, "\\|"

    move-object/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 400
    .local v5, "columns":[Ljava/lang/String;
    const/16 v21, 0x1

    aget-object v21, v5, v21

    const/16 v22, 0x1

    const/16 v23, 0x1

    aget-object v23, v5, v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 401
    .local v13, "languageCode":Ljava/lang/String;
    const/16 v21, 0x2

    aget-object v21, v5, v21

    const/16 v22, 0x1

    const/16 v23, 0x2

    aget-object v23, v5, v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    add-int/lit8 v23, v23, -0x1

    invoke-virtual/range {v21 .. v23}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 403
    .local v7, "countryCode":Ljava/lang/String;
    new-instance v12, Ljava/util/Locale;

    .end local v12    # "l":Ljava/util/Locale;
    invoke-direct {v12, v13, v7}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    .restart local v12    # "l":Ljava/util/Locale;
    invoke-static {v12}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(Ljava/util/Locale;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v15

    .line 407
    .end local v5    # "columns":[Ljava/lang/String;
    .end local v7    # "countryCode":Ljava/lang/String;
    .end local v13    # "languageCode":Ljava/lang/String;
    :cond_2
    if-eqz v15, :cond_6

    .line 408
    const-string v21, "\\|"

    const-string v22, ","

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 409
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->contentInsertStatement:Ljava/lang/String;

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v6, v22, v23

    const/16 v23, 0x1

    invoke-virtual {v15}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getKey()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v16, v22, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 410
    .local v11, "insertQuery":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 411
    const-wide/16 v21, 0x1

    add-long v17, v17, v21

    .line 419
    .end local v11    # "insertQuery":Ljava/lang/String;
    :cond_3
    :goto_2
    const/4 v9, 0x0

    .line 396
    :cond_4
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 393
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "contentRow":Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v14    # "len$":I
    .end local v16    # "multiLangIndicator":Ljava/lang/String;
    :cond_5
    const-string v16, "0"

    goto/16 :goto_0

    .line 414
    .restart local v4    # "arr$":[Ljava/lang/String;
    .restart local v6    # "contentRow":Ljava/lang/String;
    .restart local v10    # "i$":I
    .restart local v14    # "len$":I
    .restart local v16    # "multiLangIndicator":Ljava/lang/String;
    :cond_6
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_3

    if-eqz v12, :cond_3

    .line 415
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Locale not supported:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v12}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 421
    :cond_7
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 422
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Row data is not valid:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 428
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "contentRow":Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v14    # "len$":I
    :cond_8
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 429
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "No data to insert"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_9
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 434
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " rows inserted in "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    sub-long v23, v23, v19

    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " milliseconds"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    :cond_a
    if-nez p3, :cond_e

    .line 438
    if-eqz v15, :cond_f

    .line 439
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 440
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Deleting prior locale type record from semaphore"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_b
    const-string v21, "CNTNT_CUR_LOC_TY"

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 443
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 444
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v21, "LOC_TY_IND"

    invoke-virtual {v15}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getKey()I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 445
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 446
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Setting current loaded locale type to:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v15}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "("

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v15}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getKey()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ")"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :cond_c
    const-string v21, "CNTNT_CUR_LOC_TY"

    const/16 v22, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 449
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 450
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Setting current loaded locale type done"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    .end local v8    # "cv":Landroid/content/ContentValues;
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendContentUpdateCompleteBroadcast()V

    .line 459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    move-object/from16 v21, v0

    invoke-static/range {v21 .. v21}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V

    .line 463
    :cond_e
    return-void

    .line 453
    :cond_f
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v21 .. v21}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 454
    sget-object v21, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v22, "Locale type is null in setCurrentLocaleTypeLoaded"

    invoke-static/range {v21 .. v22}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public declared-synchronized isDatabaseCreated()Z
    .locals 1

    .prologue
    .line 240
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->database:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected loadContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V
    .locals 0
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "data"    # [Ljava/lang/String;
    .param p3, "multiLanguage"    # Z

    .prologue
    .line 332
    invoke-direct {p0, p2, p3}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->cacheContent([Ljava/lang/String;Z)V

    .line 333
    invoke-virtual {p0, p1, p2, p3}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->insertContent(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Z)V

    .line 334
    return-void
.end method

.method protected loadData(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;Z)V
    .locals 23
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p3, "delete"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 251
    .local p2, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    if-eqz p2, :cond_d

    invoke-interface/range {p2 .. p2}, Ljava/util/Map;->size()I

    move-result v20

    if-lez v20, :cond_d

    .line 254
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 255
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Figuring out which table need to have data pre populated"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    :cond_0
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 260
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Getting file:dbinsertfields"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_1
    const-string v20, "dbinsertfields"

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [Ljava/lang/String;

    .line 263
    .local v18, "tableAndFields":[Ljava/lang/String;
    if-eqz v18, :cond_b

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v20, v0

    if-lez v20, :cond_b

    .line 264
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 265
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Got:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " records"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_2
    move-object/from16 v4, v18

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    move v11, v10

    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v16    # "len$":I
    .local v11, "i$":I
    :goto_0
    move/from16 v0, v16

    if-ge v11, v0, :cond_c

    aget-object v12, v4, v11

    .line 269
    .local v12, "insertData":Ljava/lang/String;
    const-string v20, "="

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 270
    .local v14, "insertProperties":[Ljava/lang/String;
    if-eqz v14, :cond_a

    array-length v0, v14

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_a

    .line 271
    const/16 v20, 0x0

    aget-object v19, v14, v20

    .line 273
    .local v19, "tableName":Ljava/lang/String;
    if-eqz p3, :cond_3

    .line 274
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 277
    :cond_3
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 278
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Preparing to insert data into table:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_4
    const/16 v20, 0x1

    aget-object v13, v14, v20

    .line 283
    .local v13, "insertFields":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 285
    .local v7, "dataFileName":Ljava/lang/String;
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 286
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Reading data from file:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :cond_5
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    .line 290
    .local v9, "dataLines":[Ljava/lang/String;
    if-eqz v9, :cond_8

    .line 292
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 293
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, " "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    array-length v0, v9

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " lines read from "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_6
    move-object v5, v9

    .local v5, "arr$":[Ljava/lang/String;
    array-length v0, v5

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v10, 0x0

    .end local v11    # "i$":I
    .restart local v10    # "i$":I
    :goto_1
    move/from16 v0, v17

    if-ge v10, v0, :cond_9

    aget-object v8, v5, v10

    .line 296
    .local v8, "dataLine":Ljava/lang/String;
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_7

    .line 301
    const-string/jumbo v20, "\ufffd"

    const-string v21, "\'"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 302
    .local v6, "cleanedDataLine":Ljava/lang/String;
    const-string v20, "\'\'\'"

    const-string v21, "\'"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 304
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "INSERT INTO "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") VALUES ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ") "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 309
    .local v15, "insertSql":Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ";"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 295
    .end local v6    # "cleanedDataLine":Ljava/lang/String;
    .end local v15    # "insertSql":Ljava/lang/String;
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 314
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v8    # "dataLine":Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v17    # "len$":I
    .restart local v11    # "i$":I
    :cond_8
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 315
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " is empty."

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    .end local v7    # "dataFileName":Ljava/lang/String;
    .end local v9    # "dataLines":[Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v13    # "insertFields":Ljava/lang/String;
    .end local v19    # "tableName":Ljava/lang/String;
    :cond_9
    :goto_2
    add-int/lit8 v10, v11, 0x1

    .restart local v10    # "i$":I
    move v11, v10

    .end local v10    # "i$":I
    .restart local v11    # "i$":I
    goto/16 :goto_0

    .line 319
    :cond_a
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Insert data property is invalid: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 323
    .end local v11    # "i$":I
    .end local v12    # "insertData":Ljava/lang/String;
    .end local v14    # "insertProperties":[Ljava/lang/String;
    :cond_b
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "No data to be inserted"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    .end local v18    # "tableAndFields":[Ljava/lang/String;
    :cond_c
    :goto_3
    return-void

    .line 326
    :cond_d
    sget-object v20, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Received data is empty."

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 21
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 530
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 531
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Initializing the database for first time"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    .line 534
    .local v13, "startTime":J
    const/4 v7, 0x0

    .line 535
    .local v7, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 539
    const/4 v5, 0x0

    .line 542
    .local v5, "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    :try_start_0
    new-instance v4, Lcom/cigna/coach/utils/CMSHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-direct {v4, v0}, Lcom/cigna/coach/utils/CMSHelper;-><init>(Landroid/content/Context;)V

    .line 543
    .local v4, "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Lcom/cigna/coach/utils/CMSHelper;->checkAndCopyDefaultZipFilesToLocalPath(ZLandroid/database/sqlite/SQLiteDatabase;)Z

    .line 546
    invoke-virtual {v4}, Lcom/cigna/coach/utils/CMSHelper;->retrieveAllMasterTables()Ljava/util/Map;

    move-result-object v7

    .line 549
    if-eqz v7, :cond_1

    .line 550
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->createAndLoadReferenceTables(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Map;)V

    .line 553
    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/cigna/coach/utils/CMSHelper;->initializeVersionsTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->getUserTables(Landroid/content/Context;)Ljava/util/List;

    move-result-object v15

    .line 559
    .local v15, "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v0, v1, v15}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->setJournalTriggers(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/cigna/coach/utils/CMSHelper;->earlyCoachSQLiteHelper(Landroid/content/Context;ZLandroid/database/sqlite/SQLiteDatabase;)Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v9

    .line 565
    .local v9, "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v16, Lcom/cigna/coach/db/UserMetricsDBManager;

    move-object/from16 v0, v16

    invoke-direct {v0, v9}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 566
    .local v16, "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    const/16 v17, 0x1

    sget-object v18, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->FULL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    invoke-virtual/range {v16 .. v18}, Lcom/cigna/coach/db/UserMetricsDBManager;->setBackupType(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;)V

    .line 567
    invoke-virtual {v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 569
    :try_start_2
    invoke-virtual {v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 573
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 575
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 576
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Reference tables created and loaded in "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    sub-long v19, v19, v13

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " ms"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    :cond_2
    invoke-virtual {v4}, Lcom/cigna/coach/utils/CMSHelper;->retrieveAllLanguageContent()[Ljava/lang/String;

    move-result-object v3

    .line 581
    .local v3, "allLangContent":[Ljava/lang/String;
    const/16 v17, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v3, v1}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->cacheContent([Ljava/lang/String;Z)V

    .line 582
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 583
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Multi language content cached"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v11, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 588
    .local v11, "locale":Ljava/util/Locale;
    invoke-static {v11}, Lcom/cigna/coach/utils/ContentHelper$LocaleType;->getInstance(Ljava/util/Locale;)Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    move-result-object v12

    .line 589
    .local v12, "localeType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    invoke-virtual {v4, v12}, Lcom/cigna/coach/utils/CMSHelper;->retrieveContentForLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)[Ljava/lang/String;

    move-result-object v10

    .line 590
    .local v10, "langContent":[Ljava/lang/String;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v10, v1}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->cacheContent([Ljava/lang/String;Z)V

    .line 591
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 592
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "Locale specific content cached"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    :cond_4
    new-instance v6, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v3, v10}, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;-><init>(Lcom/cigna/coach/db/CoachSqliteConnectionManager;[Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 597
    .end local v5    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    .local v6, "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    :try_start_3
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 598
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Create database method completed in "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v19

    sub-long v19, v19, v13

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " ms"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 604
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    move-object v5, v6

    .line 606
    .end local v3    # "allLangContent":[Ljava/lang/String;
    .end local v4    # "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    .end local v6    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    .end local v9    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v10    # "langContent":[Ljava/lang/String;
    .end local v11    # "locale":Ljava/util/Locale;
    .end local v12    # "localeType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .end local v15    # "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v16    # "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    .restart local v5    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    :goto_0
    if-eqz v7, :cond_7

    if-eqz v5, :cond_7

    .line 607
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;->start()V

    .line 613
    :cond_6
    :goto_1
    return-void

    .line 569
    .restart local v4    # "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    .restart local v9    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v15    # "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v17

    :try_start_4
    invoke-virtual {v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    throw v17
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 600
    .end local v4    # "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    .end local v9    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v15    # "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v8

    .line 601
    .local v8, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Error while running initilization scripts: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 604
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v17

    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v17

    .line 609
    :cond_7
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v17 .. v17}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 610
    sget-object v17, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    const-string v18, "No content data to insert"

    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 604
    .end local v5    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    .restart local v3    # "allLangContent":[Ljava/lang/String;
    .restart local v4    # "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    .restart local v6    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    .restart local v9    # "helper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v10    # "langContent":[Ljava/lang/String;
    .restart local v11    # "locale":Ljava/util/Locale;
    .restart local v12    # "localeType":Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .restart local v15    # "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v16    # "userMetricDbManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :catchall_2
    move-exception v17

    move-object v5, v6

    .end local v6    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    .restart local v5    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    goto :goto_3

    .line 600
    .end local v5    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    .restart local v6    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    :catch_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    .restart local v5    # "contentInsertThread":Lcom/cigna/coach/db/CoachSqliteConnectionManager$InsertContentThread;
    goto :goto_2
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 628
    sget-object v2, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 629
    sget-object v2, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Upgrading database from version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :cond_0
    if-le p3, p2, :cond_1

    .line 632
    new-instance v0, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;

    invoke-direct {v0}, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;-><init>()V

    .line 634
    .local v0, "coachUpdateDBManager":Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;
    :try_start_0
    iget-object v2, p0, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->context:Landroid/content/Context;

    invoke-virtual {v0, v2, p2, p3, p1}, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->applyDBUpgrade(Landroid/content/Context;IILandroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachException; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    .end local v0    # "coachUpdateDBManager":Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;
    :cond_1
    :goto_0
    sget-object v2, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 640
    sget-object v2, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Finished upgrading database from version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    :cond_2
    return-void

    .line 635
    .restart local v0    # "coachUpdateDBManager":Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;
    :catch_0
    move-exception v1

    .line 636
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachException;
    sget-object v2, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->CLASS_NAME:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
