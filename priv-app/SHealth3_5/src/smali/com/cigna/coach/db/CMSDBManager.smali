.class public Lcom/cigna/coach/db/CMSDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "CMSDBManager.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/cigna/coach/db/CMSDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 46
    return-void
.end method


# virtual methods
.method public getTableVersion(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/VersionTableData;
    .locals 12
    .param p1, "tableName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 91
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTableVersion("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "): START"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    const-string v1, "VERSN"

    .line 97
    .local v1, "table":Ljava/lang/String;
    const-string v3, "FILE_NAME = ?"

    .line 98
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v11, [Ljava/lang/String;

    aput-object p1, v4, v7

    .line 99
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "VERSN_CODE"

    aput-object v0, v2, v7

    const-string v0, "CHK_SUM"

    aput-object v0, v2, v11

    .line 100
    .local v2, "columns":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 103
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CMSDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 104
    if-eqz v8, :cond_4

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_4

    .line 105
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 106
    new-instance v10, Lcom/cigna/coach/dataobjects/VersionTableData;

    invoke-direct {v10}, Lcom/cigna/coach/dataobjects/VersionTableData;-><init>()V

    .line 107
    .local v10, "version":Lcom/cigna/coach/dataobjects/VersionTableData;
    invoke-virtual {v10, p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->setTableName(Ljava/lang/String;)V

    .line 108
    const-string v0, "VERSN_CODE"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/VersionTableData;->setVersionCode(Ljava/lang/String;)V

    .line 109
    const-string v0, "CHK_SUM"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/VersionTableData;->setChksum(Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getTableVersion: found version"

    invoke-static {v0, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    :cond_1
    :goto_0
    if-eqz v8, :cond_2

    .line 124
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 128
    :cond_2
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 129
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTableVersion: version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; checksum: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v10}, Lcom/cigna/coach/dataobjects/VersionTableData;->getChksum()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getTableVersion: END"

    invoke-static {v0, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_3
    return-object v10

    .line 114
    .end local v10    # "version":Lcom/cigna/coach/dataobjects/VersionTableData;
    :cond_4
    :try_start_1
    invoke-static {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->nullVersion(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/VersionTableData;

    move-result-object v10

    .line 115
    .restart local v10    # "version":Lcom/cigna/coach/dataobjects/VersionTableData;
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getTableVersion: no versions found; using null version"

    invoke-static {v0, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 119
    .end local v10    # "version":Lcom/cigna/coach/dataobjects/VersionTableData;
    :catch_0
    move-exception v9

    .line 120
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getTableVersion: ERROR: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v9}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    new-instance v0, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v0, v9}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_5

    .line 124
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method public getVersionTableAllData()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/VersionTableData;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 136
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getVersionTableAllData"

    invoke-static {v0, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .local v11, "versionTableDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/VersionTableData;>;"
    const-string v1, "VERSN"

    .line 142
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "FILE_NAME"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "VERSN_CODE"

    aput-object v3, v2, v0

    .line 143
    .local v2, "columns":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 146
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/CMSDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 147
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 148
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 149
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 150
    new-instance v10, Lcom/cigna/coach/dataobjects/VersionTableData;

    invoke-direct {v10}, Lcom/cigna/coach/dataobjects/VersionTableData;-><init>()V

    .line 151
    .local v10, "versionTableData":Lcom/cigna/coach/dataobjects/VersionTableData;
    const-string v0, "FILE_NAME"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/VersionTableData;->setTableName(Ljava/lang/String;)V

    .line 152
    const-string v0, "VERSN_CODE"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/VersionTableData;->setVersionCode(Ljava/lang/String;)V

    .line 154
    invoke-interface {v11, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 158
    .end local v10    # "versionTableData":Lcom/cigna/coach/dataobjects/VersionTableData;
    :catch_0
    move-exception v9

    .line 159
    .local v9, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getVersionTableAllData"

    invoke-static {v0, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    sget-object v0, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v9}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    new-instance v0, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v0, v9}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    .end local v9    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 164
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 163
    :cond_2
    if-eqz v8, :cond_3

    .line 164
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 166
    :cond_3
    return-object v11
.end method

.method public updateIsLoadedFlag(Ljava/lang/String;)I
    .locals 8
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 170
    if-nez p1, :cond_0

    .line 171
    const/4 v2, -0x1

    .line 192
    :goto_0
    return v2

    .line 172
    :cond_0
    const/4 v2, -0x1

    .line 174
    .local v2, "noOfRowsUpdated":I
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 175
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateVersionTable"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_1
    const-string v4, "FILE_NAME = ?"

    .line 178
    .local v4, "whereClause":Ljava/lang/String;
    new-array v3, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v3, v5

    .line 181
    .local v3, "whereArgs":[Ljava/lang/String;
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 182
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v5, "FILE_NAME"

    invoke-virtual {v0, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v5, "LOAD_IND"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 184
    invoke-virtual {p0}, Lcom/cigna/coach/db/CMSDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "VERSN"

    invoke-virtual {v5, v6, v0, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 187
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 188
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "An error occured while inserting into the Version table"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    new-instance v5, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method public updateVersionTable(Lcom/cigna/coach/dataobjects/VersionTableData;I)V
    .locals 8
    .param p1, "newData"    # Lcom/cigna/coach/dataobjects/VersionTableData;
    .param p2, "loaded"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 50
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 53
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateVersionTable"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_2
    const-string v4, "FILE_NAME = ?"

    .line 56
    .local v4, "whereClause":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    .line 59
    .local v3, "whereArgs":[Ljava/lang/String;
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 60
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v5, "FILE_NAME"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getTableName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 62
    const-string v5, "VERSN_CODE"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    :cond_3
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getChksum()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 65
    const-string v5, "CHK_SUM"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getChksum()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_4
    if-eq p2, v7, :cond_5

    .line 68
    const-string v5, "LOAD_IND"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 70
    :cond_5
    invoke-virtual {p0}, Lcom/cigna/coach/db/CMSDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "VERSN"

    invoke-virtual {v5, v6, v0, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 73
    .local v2, "noOfRowsUpdated":I
    if-eq v2, v7, :cond_6

    if-nez v2, :cond_0

    .line 75
    :cond_6
    invoke-virtual {p0}, Lcom/cigna/coach/db/CMSDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "VERSN"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 77
    .end local v0    # "contentValues":Landroid/content/ContentValues;
    .end local v2    # "noOfRowsUpdated":I
    :catch_0
    move-exception v1

    .line 78
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "An error occured while inserting into the Version table"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    sget-object v5, Lcom/cigna/coach/db/CMSDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error is:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    new-instance v5, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v5, v1}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method
