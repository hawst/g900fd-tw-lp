.class public Lcom/cigna/coach/db/JournalDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "JournalDBManager.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "DefaultLocale"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/JournalDBManager$1;,
        Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static tableFieldsToJSONMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Properties;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const-class v0, Lcom/cigna/coach/db/JournalDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/db/JournalDBManager;->tableFieldsToJSONMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 78
    return-void
.end method

.method private getData(Ljava/util/Set;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 6
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "fields":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 254
    .local v0, "data":Ljava/lang/StringBuilder;
    const-string v3, ""

    .line 255
    .local v3, "result":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 257
    .local v1, "field":Ljava/lang/String;
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 258
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string/jumbo v4, "|"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 261
    :cond_0
    const-string v4, "_NULL_"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string/jumbo v4, "|"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 265
    .end local v1    # "field":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 266
    return-object v3
.end method

.method public static getJournalTableDetails(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/cigna/coach/db/DbTableInfo;
    .locals 10
    .param p0, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 428
    sget-object v7, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 429
    sget-object v7, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Looking up table: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_0
    new-instance v4, Lcom/cigna/coach/db/DbTableInfo;

    invoke-direct {v4, p1}, Lcom/cigna/coach/db/DbTableInfo;-><init>(Ljava/lang/String;)V

    .line 432
    .local v4, "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    const-string v3, " PRAGMA TABLE_INFO(%s) "

    .line 433
    .local v3, "tableDetailsQuery":Ljava/lang/String;
    new-array v7, v5, [Ljava/lang/Object;

    aput-object p1, v7, v6

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 434
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    .line 435
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 436
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_2

    .line 437
    const-string/jumbo v7, "name"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 438
    .local v1, "fieldName":Ljava/lang/String;
    const-string/jumbo v7, "pk"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-lez v7, :cond_1

    move v2, v5

    .line 439
    .local v2, "isPrimaryKey":Z
    :goto_1
    invoke-virtual {v4, v1, v2}, Lcom/cigna/coach/db/DbTableInfo;->addField(Ljava/lang/String;Z)V

    .line 440
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .end local v2    # "isPrimaryKey":Z
    :cond_1
    move v2, v6

    .line 438
    goto :goto_1

    .line 442
    .end local v1    # "fieldName":Ljava/lang/String;
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 444
    :cond_3
    return-object v4
.end method

.method public static getJournalTableDetails(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)Ljava/util/Set;
    .locals 6
    .param p0, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lcom/cigna/coach/db/DbTableInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409
    .local p1, "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v4, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 410
    sget-object v4, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Getting journal table information"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    :cond_0
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 414
    .local v2, "tableInfoSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/cigna/coach/db/DbTableInfo;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 415
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 416
    .local v3, "tableName":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/cigna/coach/db/JournalDBManager;->getJournalTableDetails(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/cigna/coach/db/DbTableInfo;

    move-result-object v1

    .line 417
    .local v1, "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 420
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "tableInfo":Lcom/cigna/coach/db/DbTableInfo;
    .end local v3    # "tableName":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 421
    sget-object v4, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "No table names supplied"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    :cond_2
    return-object v2
.end method

.method public static sqliteTimestampFormat()Ljava/text/DateFormat;
    .locals 3

    .prologue
    .line 63
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy-MM-dd HH:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 64
    .local v0, "format":Ljava/text/DateFormat;
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 65
    return-object v0
.end method


# virtual methods
.method public deleteAllJournalEntriesForUser(Ljava/lang/String;J)V
    .locals 9
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "tillTimestamp"    # J
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 319
    sget-object v3, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 320
    sget-object v3, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting all journal entries for user "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " till "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    :cond_0
    const/4 v2, 0x1

    .line 326
    .local v2, "userId":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "USR_ACTY_JRNL"

    const-string v5, "USR_ID = ? "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 332
    .local v1, "numRowsDeleted":I
    sget-object v3, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 333
    sget-object v3, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleted "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rows from journal table"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    .end local v1    # "numRowsDeleted":I
    :cond_1
    :goto_0
    return-void

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "cunfe":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    sget-object v3, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "User "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found. No entries were deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public deleteUserTableEntries(Ljava/lang/String;)I
    .locals 5
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 341
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 342
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deleting all entries for user table:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v1, p1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 347
    .local v0, "numRowsDeleted":I
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 348
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Deleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rows from user table:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    :cond_1
    return v0
.end method

.method public generateAllJournalEntriesFromUserTables(Ljava/util/List;IIILcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/List;
    .locals 17
    .param p2, "userId"    # I
    .param p3, "codeBaseId"    # I
    .param p4, "dbVersn"    # I
    .param p5, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;III",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/JournalEntry;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 393
    .local p1, "tables":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide/16 v2, 0x0

    move-object/from16 v0, p5

    invoke-interface {v0, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 394
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 395
    .local v6, "journalEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/JournalEntry;>;"
    const/4 v7, 0x1

    .line 396
    .local v7, "seqId":I
    const-string v2, "__MARKER"

    const-string v8, "__MARKER"

    const-string v9, "__MARKER"

    move-object/from16 v1, p0

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    invoke-virtual/range {v1 .. v9}, Lcom/cigna/coach/db/JournalDBManager;->journalEntryMapper(Ljava/lang/String;IIILjava/util/List;ILjava/lang/String;Ljava/lang/String;)V

    .line 397
    const/16 v16, 0x0

    .line 398
    .local v16, "tablesCompleted":I
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "iterator":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 400
    .local v9, "table":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cigna/coach/db/JournalDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v1}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {v1, v9}, Lcom/cigna/coach/db/JournalDBManager;->getJournalTableDetails(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/cigna/coach/db/DbTableInfo;

    move-result-object v10

    .local v10, "dbTableInfo":Lcom/cigna/coach/db/DbTableInfo;
    move-object/from16 v8, p0

    move/from16 v11, p2

    move/from16 v12, p3

    move/from16 v13, p4

    move-object v14, v6

    .line 401
    invoke-virtual/range {v8 .. v14}, Lcom/cigna/coach/db/JournalDBManager;->getUserTableEntries(Ljava/lang/String;Lcom/cigna/coach/db/DbTableInfo;IIILjava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 402
    sget-object v1, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    int-to-double v2, v0

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    int-to-double v4, v4

    div-double/2addr v2, v4

    move-object/from16 v0, p5

    invoke-interface {v0, v1, v2, v3}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    goto :goto_0

    .line 405
    .end local v9    # "table":Ljava/lang/String;
    .end local v10    # "dbTableInfo":Lcom/cigna/coach/db/DbTableInfo;
    :cond_0
    return-object v6
.end method

.method public getAllJournalEntriesForUser(Ljava/lang/String;Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;)Ljava/util/List;
    .locals 12
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/JournalEntry;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 279
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retrieving all journal entries for user: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_0
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 284
    .local v9, "entries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/JournalEntry;>;"
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/cigna/coach/db/UserDBManager;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v11

    .line 285
    .local v11, "userId":I
    new-instance v10, Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;

    invoke-direct {v10, v3}, Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;-><init>(Lcom/cigna/coach/db/JournalDBManager$1;)V

    .line 287
    .local v10, "rowMapper":Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;
    const/4 v8, 0x0

    .line 289
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v1, 0x3fa999999999999aL    # 0.05

    invoke-interface {p2, v0, v1, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V

    .line 290
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "USR_ACTY_JRNL"

    const/4 v2, 0x0

    const-string v3, "USR_ID = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "ACTN_TIMESTMP ASC"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 292
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 293
    invoke-virtual {v10, v8}, Lcom/cigna/coach/db/JournalDBManager$JournalEntryRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/JournalEntry;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 297
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 298
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 301
    :cond_1
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 302
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieved "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " journal entries"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v0

    .line 295
    :cond_3
    :try_start_1
    sget-object v0, Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;->REQUEST_FOR_BACKUP:Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;

    const-wide v1, 0x3fee666666666666L    # 0.95

    invoke-interface {p2, v0, v1, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper$FractionalActivityListener;->onProgress(Lcom/cigna/coach/interfaces/IBackupAndRestore$RequestType;D)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 297
    if-eqz v8, :cond_4

    .line 298
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 301
    :cond_4
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 302
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retrieved "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " journal entries"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    :cond_5
    return-object v9
.end method

.method public getDbVersion()Lcom/cigna/coach/dataobjects/DbVersion;
    .locals 10

    .prologue
    .line 152
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Retrieving dbversion and code base id "

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_0
    new-instance v9, Lcom/cigna/coach/dataobjects/DbVersion;

    invoke-direct {v9}, Lcom/cigna/coach/dataobjects/DbVersion;-><init>()V

    .line 157
    .local v9, "dbVersion":Lcom/cigna/coach/dataobjects/DbVersion;
    const/4 v8, 0x0

    .line 159
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "DB_VERSN"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 160
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 161
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 162
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/cigna/coach/dataobjects/DbVersion;->setCodeBaseId(I)V

    .line 163
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/cigna/coach/dataobjects/DbVersion;->setDbVersion(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    :cond_1
    if-eqz v8, :cond_2

    .line 167
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 170
    :cond_2
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 171
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retrieved dbversion "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/DbVersion;->getDbVersion()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  and code base id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/DbVersion;->getCodeBaseId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_3
    return-object v9

    .line 166
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 167
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 170
    :cond_4
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 171
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieved dbversion "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/DbVersion;->getDbVersion()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  and code base id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/DbVersion;->getCodeBaseId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v0
.end method

.method public getUserTableEntries(Ljava/lang/String;Lcom/cigna/coach/db/DbTableInfo;IIILjava/util/List;)Ljava/util/List;
    .locals 11
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "dbTableInfo"    # Lcom/cigna/coach/db/DbTableInfo;
    .param p3, "userId"    # I
    .param p4, "codeBaseId"    # I
    .param p5, "dbVersn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/db/DbTableInfo;",
            "III",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/JournalEntry;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/JournalEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    .local p6, "journalEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/JournalEntry;>;"
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retrieving all journal entries "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v10, v0, 0x1

    .line 196
    .local v10, "seqId":I
    const/4 v9, 0x0

    .line 198
    .local v9, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 199
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move v6, v10

    .end local v10    # "seqId":I
    .local v6, "seqId":I
    :goto_0
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_4

    .line 201
    invoke-virtual {p2}, Lcom/cigna/coach/db/DbTableInfo;->getAllFields()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0, v9}, Lcom/cigna/coach/db/JournalDBManager;->getData(Ljava/util/Set;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    .line 202
    .local v7, "data":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/cigna/coach/db/DbTableInfo;->getPrimaryKeys()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0, v9}, Lcom/cigna/coach/db/JournalDBManager;->getData(Ljava/util/Set;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    .line 204
    .local v8, "keys":Ljava/lang/String;
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DATA "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KEYS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move v2, p3

    move v3, p4

    move/from16 v4, p5

    move-object/from16 v5, p6

    .line 209
    invoke-virtual/range {v0 .. v8}, Lcom/cigna/coach/db/JournalDBManager;->journalEntryMapper(Ljava/lang/String;IIILjava/util/List;ILjava/lang/String;Ljava/lang/String;)V

    .line 210
    add-int/lit8 v6, v6, 0x1

    .line 199
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 213
    .end local v7    # "data":Ljava/lang/String;
    .end local v8    # "keys":Ljava/lang/String;
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v9, :cond_2

    .line 214
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 217
    :cond_2
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 218
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieved "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " journal entries"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v0

    .line 213
    :cond_4
    if-eqz v9, :cond_5

    .line 214
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 217
    :cond_5
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 218
    sget-object v0, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retrieved "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " journal entries"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_6
    return-object p6

    .line 213
    .end local v6    # "seqId":I
    .restart local v10    # "seqId":I
    :catchall_1
    move-exception v0

    move v6, v10

    .end local v10    # "seqId":I
    .restart local v6    # "seqId":I
    goto :goto_1
.end method

.method public journalEntryMapper(Ljava/lang/String;IIILjava/util/List;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "codeBaseId"    # I
    .param p4, "dbVersn"    # I
    .param p6, "seqId"    # I
    .param p7, "data"    # Ljava/lang/String;
    .param p8, "keys"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "III",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/JournalEntry;",
            ">;I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 237
    .local p5, "journalEntries":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/JournalEntry;>;"
    new-instance v0, Lcom/cigna/coach/dataobjects/JournalEntry;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/JournalEntry;-><init>()V

    .line 238
    .local v0, "journalEntry":Lcom/cigna/coach/dataobjects/JournalEntry;
    invoke-virtual {v0, p6}, Lcom/cigna/coach/dataobjects/JournalEntry;->setSeqId(I)V

    .line 239
    invoke-virtual {v0, p3}, Lcom/cigna/coach/dataobjects/JournalEntry;->setCodeBaseId(I)V

    .line 240
    invoke-virtual {v0, p4}, Lcom/cigna/coach/dataobjects/JournalEntry;->setDbVersion(I)V

    .line 241
    invoke-virtual {v0, p2}, Lcom/cigna/coach/dataobjects/JournalEntry;->setUserId(I)V

    .line 242
    sget-object v1, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/JournalEntry;->setActionType(Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;)V

    .line 243
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/JournalEntry;->setActionTime(Ljava/util/Calendar;)V

    .line 244
    invoke-virtual {v0, p8}, Lcom/cigna/coach/dataobjects/JournalEntry;->setKey(Ljava/lang/String;)V

    .line 245
    invoke-virtual {v0, p7}, Lcom/cigna/coach/dataobjects/JournalEntry;->setData(Ljava/lang/String;)V

    .line 246
    invoke-virtual {v0, p1}, Lcom/cigna/coach/dataobjects/JournalEntry;->setTableName(Ljava/lang/String;)V

    .line 247
    invoke-interface {p5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    return-void
.end method

.method public restoreTable(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 10
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "userName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserTableJournalData;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 360
    .local p3, "records":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserTableJournalData;>;"
    sget-object v7, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 361
    sget-object v7, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Restoring table:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ". Number of records:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/db/JournalDBManager;->getJournalTableDetails(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/cigna/coach/db/DbTableInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/cigna/coach/db/DbTableInfo;->getAllFields()Ljava/util/Set;

    move-result-object v1

    .line 364
    .local v1, "columnNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 365
    .local v2, "cv":Landroid/content/ContentValues;
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/dataobjects/UserTableJournalData;

    .line 366
    .local v5, "record":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    new-instance v2, Landroid/content/ContentValues;

    .end local v2    # "cv":Landroid/content/ContentValues;
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 367
    .restart local v2    # "cv":Landroid/content/ContentValues;
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 368
    .local v0, "columnName":Ljava/lang/String;
    invoke-virtual {v5, v0}, Lcom/cigna/coach/dataobjects/UserTableJournalData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 369
    .local v6, "value":Ljava/lang/String;
    const-string v7, "_NULL_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 370
    invoke-virtual {v2, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1

    .line 372
    :cond_1
    invoke-virtual {v2, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 375
    .end local v0    # "columnName":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_2
    const-string v7, "USR_INFO"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 376
    const-string v7, "USR_ID_TXT"

    invoke-virtual {v2, v7, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_3
    invoke-virtual {p0, p1, v2}, Lcom/cigna/coach/db/JournalDBManager;->restoreUserTableRow(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 381
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "record":Lcom/cigna/coach/dataobjects/UserTableJournalData;
    :cond_4
    const/4 v7, 0x1

    return v7
.end method

.method public restoreUserTableRow(Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 2
    .param p1, "table"    # Ljava/lang/String;
    .param p2, "cv"    # Landroid/content/ContentValues;

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public resumeJournalling()V
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/db/JournalDBManager;->resumeJournalling(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 100
    return-void
.end method

.method public resumeJournalling(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 106
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "resumeJournalling: Start"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 110
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "JRNL_ON"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 111
    const-string v1, "USR_ACTY_JRNL_SWITCH"

    invoke-virtual {p1, v1, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 112
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 113
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "resumeJournalling: End"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_1
    return-void
.end method

.method public resumeJournalling(Ljava/lang/String;)V
    .locals 6
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 139
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 140
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "JRNL_ON"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 141
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "USR_ACTY_JRNL_SWITCH"

    const-string v3, "TBL_NM=?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 142
    return-void
.end method

.method public stopJournalling()V
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/db/JournalDBManager;->stopJournalling(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 85
    return-void
.end method

.method public stopJournalling(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v3, 0x0

    .line 90
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 91
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "JRNL_ON"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 92
    const-string v1, "USR_ACTY_JRNL_SWITCH"

    invoke-virtual {p1, v1, v0, v3, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 93
    return-void
.end method

.method public stopJournalling(Ljava/lang/String;)V
    .locals 6
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 122
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "stopJournalling: start"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 126
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "JRNL_ON"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 127
    invoke-virtual {p0}, Lcom/cigna/coach/db/JournalDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "USR_ACTY_JRNL_SWITCH"

    const-string v3, "TBL_NM=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 128
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 129
    sget-object v1, Lcom/cigna/coach/db/JournalDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "stopJournalling: End"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_1
    return-void
.end method
