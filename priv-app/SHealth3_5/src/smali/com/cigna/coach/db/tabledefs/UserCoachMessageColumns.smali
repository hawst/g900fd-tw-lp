.class public Lcom/cigna/coach/db/tabledefs/UserCoachMessageColumns;
.super Ljava/lang/Object;
.source "UserCoachMessageColumns.java"


# static fields
.field public static final KEY_ACTY_CNT:Ljava/lang/String; = "ACTY_CNT"

.field public static final KEY_CNTXT_FILTER_CD:Ljava/lang/String; = "CNTXT_FILTER_CD"

.field public static final KEY_CNTXT_ID:Ljava/lang/String; = "CNTXT_ID"

.field public static final KEY_LAST_DELIVERED_TIME:Ljava/lang/String; = "LAST_DELIVERED_TIME"

.field public static final KEY_MAX_VAL:Ljava/lang/String; = "MAX_VAL"

.field public static final KEY_MIN_MAX_REF_CD:Ljava/lang/String; = "MIN_MAX_REF_CD"

.field public static final KEY_MIN_VAL:Ljava/lang/String; = "MIN_VAL"

.field public static final KEY_MSG_TY_CD:Ljava/lang/String; = "MSG_TY_CD"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "USR_COACH_MSG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
