.class public Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;
.super Ljava/lang/Object;
.source "CoachDBUpgradeManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public applyDBUpgrade(Landroid/content/Context;IILandroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I
    .param p4, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 28
    sget-object v4, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 29
    sget-object v4, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "***********************************"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    sget-object v4, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "newVersion:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    sget-object v4, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "oldVersion:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :cond_0
    if-gt p2, p3, :cond_1

    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    .line 53
    :cond_1
    :goto_0
    return-void

    .line 36
    :cond_2
    new-instance v0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;

    invoke-direct {v0, p1}, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;-><init>(Landroid/content/Context;)V

    .line 37
    .local v0, "dbUpgradeTask":Lcom/cigna/coach/db/upgrade/DBUpgradeTask;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v3, "upgradeActions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/db/upgrade/IDBUpgrade;>;"
    move v1, p2

    .local v1, "i":I
    :goto_1
    if-ge v1, p3, :cond_4

    .line 41
    add-int/lit8 v4, v1, 0x1

    invoke-static {v4}, Lcom/cigna/coach/db/upgrade/DBUpgradeFactory;->getUpgradeAction(I)Lcom/cigna/coach/db/upgrade/IDBUpgrade;

    move-result-object v2

    .line 42
    .local v2, "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    if-eqz v2, :cond_3

    .line 44
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 47
    .end local v2    # "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    :cond_4
    invoke-virtual {v0, v3}, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->setUpgradeActions(Ljava/util/List;)V

    .line 48
    invoke-virtual {v0, p4}, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->performDBUpgradeActions(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 50
    sget-object v4, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 51
    sget-object v4, Lcom/cigna/coach/db/upgrade/CoachDBUpgradeManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "***********************************"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
