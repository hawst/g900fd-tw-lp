.class public Lcom/cigna/coach/db/tabledefs/GoalColumns;
.super Ljava/lang/Object;
.source "GoalColumns.java"


# static fields
.field public static final KEY_ACTV_GOAL_IND:Ljava/lang/String; = "ACTV_GOAL_IND"

.field public static final KEY_DISPL_ORDR_NUM:Ljava/lang/String; = "DISPL_ORDR_NUM"

.field public static final KEY_GOAL_CNTNT_ID:Ljava/lang/String; = "GOAL_CNTNT_ID"

.field public static final KEY_GOAL_FREQ_CNTNT_ID:Ljava/lang/String; = "GOAL_FREQ_CNTNT_ID"

.field public static final KEY_GOAL_ID:Ljava/lang/String; = "GOAL_ID"

.field public static final KEY_GOAL_IMG_CNTNT_ID:Ljava/lang/String; = "GOAL_IMG_CNTNT_ID"

.field public static final KEY_GOAL_WHY_CNTNT_ID:Ljava/lang/String; = "GOAL_WHY_CNTNT_ID"

.field public static final KEY_MAX_DAYS_BTWN_MSSN_NUM:Ljava/lang/String; = "MAX_DAYS_BTWN_MSSN_NUM"

.field public static final KEY_MAX_DAY_GRACE_PER_NUM:Ljava/lang/String; = "MAX_DAY_GRACE_PER_NUM"

.field public static final KEY_MIN_DAYS_BTWN_MSSN_NUM:Ljava/lang/String; = "MIN_DAYS_BTWN_MSSN_NUM"

.field public static final KEY_MIN_MSSN_FREQ_NUM:Ljava/lang/String; = "MIN_MSSN_FREQ_NUM"

.field public static final KEY_MSSN_TO_CMPLT_NUM:Ljava/lang/String; = "MSSN_TO_CMPLT_NUM"

.field public static final TABLE_NAME:Ljava/lang/String; = "GOAL"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
