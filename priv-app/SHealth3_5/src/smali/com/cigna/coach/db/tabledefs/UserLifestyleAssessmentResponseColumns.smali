.class public Lcom/cigna/coach/db/tabledefs/UserLifestyleAssessmentResponseColumns;
.super Ljava/lang/Object;
.source "UserLifestyleAssessmentResponseColumns.java"


# static fields
.field public static final KEY_ANS_SRC_CD:Ljava/lang/String; = "ANS_SRC"

.field public static final KEY_ANS_TY_IND:Ljava/lang/String; = "ANS_TY_IND"

.field public static final KEY_ASSMT_TIMESTMP:Ljava/lang/String; = "ASSMT_UPDT_TS"

.field public static final KEY_LFSTYL_ASSMT_QUEST_GRP_ID:Ljava/lang/String; = "LFSTYL_ASSMT_QUEST_GRP_ID"

.field public static final KEY_LFSTYL_ASSMT_QUEST_ID:Ljava/lang/String; = "LFSTYL_ASSMT_QUEST_ID"

.field public static final KEY_RESP_REF:Ljava/lang/String; = "RESP_REF"

.field public static final KEY_RESP_TXT:Ljava/lang/String; = "RESP_TXT"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "USR_LFSTYL_ASSMT_RESP"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
