.class public Lcom/cigna/coach/db/ApiCallDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "ApiCallDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/cigna/coach/db/ApiCallDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 38
    return-void
.end method


# virtual methods
.method public deleteAllApiCallEntriesForUser(Ljava/lang/String;)V
    .locals 9
    .param p1, "username"    # Ljava/lang/String;

    .prologue
    .line 48
    sget-object v3, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 49
    sget-object v3, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting all call log entries for user "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    const/4 v2, 0x1

    .line 54
    .local v2, "userId":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/ApiCallDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "USR_API_CALL_LOG"

    const-string v5, "USR_ID = ? "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 57
    .local v1, "numRowsDeleted":I
    sget-object v3, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 58
    sget-object v3, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleted "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rows from api call table"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    .end local v1    # "numRowsDeleted":I
    :cond_1
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "cunfe":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    sget-object v3, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "User "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " not found. No entries were deleted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public insertApiCallLogEntry(Lcom/cigna/coach/dataobjects/ApiCallLogEntry;)V
    .locals 4
    .param p1, "entry"    # Lcom/cigna/coach/dataobjects/ApiCallLogEntry;

    .prologue
    .line 66
    sget-object v1, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    sget-object v1, Lcom/cigna/coach/db/ApiCallDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Inserting an api call entry "

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 71
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "USR_ID"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 72
    const-string v1, "API_CD"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->getMethod()Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->getKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 73
    const-string v1, "API_CALL_SRC_CD"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->getSource()Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->getKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 74
    const-string v1, "API_CALL_TS"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->getInvocationTime()Ljava/util/Calendar;

    move-result-object v2

    invoke-static {v2}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 75
    const-string v1, "API_CALL_RESP_TM"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->getInvocationRespTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 76
    const-string v2, "API_CALL_ERR_CD"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->getExceptionType()Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->getExceptionType()Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->getKey()I

    move-result v1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 79
    invoke-virtual {p0}, Lcom/cigna/coach/db/ApiCallDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v2, "USR_API_CALL_LOG"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 81
    return-void

    .line 76
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
