.class public final Lcom/cigna/coach/db/BadgeDBQueries;
.super Ljava/lang/Object;
.source "BadgeDBQueries.java"


# static fields
.field public static final ALL_BADGES:Ljava/lang/String;

.field public static final ALL_BADGES_USER_OWNS:Ljava/lang/String;

.field private static final BADGES_BY_ID_:Ljava/lang/String;

.field public static final COUNT_OF_ALL_BADGES:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    const-string v0, "SELECT %s, %s, %s, %s FROM %s ORDER BY %s"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "BADGE_ID"

    aput-object v2, v1, v4

    const-string v2, "BADGE_CNTNT_ID"

    aput-object v2, v1, v5

    const-string v2, "BADGE_DESC_CNTNT_ID"

    aput-object v2, v1, v6

    const-string v2, "BADGE_IMG_CNTNT_ID"

    aput-object v2, v1, v7

    const-string v2, "BADGE"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "DISPL_ORDR_NUM"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/BadgeDBQueries;->ALL_BADGES:Ljava/lang/String;

    .line 30
    const-string v0, "SELECT %s.%s, %s, %s, %s, %s FROM %s JOIN %s ON %s.%s = %s.%s WHERE %s = ? ORDER BY %s.%s"

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "BADGE"

    aput-object v2, v1, v4

    const-string v2, "BADGE_ID"

    aput-object v2, v1, v5

    const-string v2, "BADGE_CNTNT_ID"

    aput-object v2, v1, v6

    const-string v2, "BADGE_DESC_CNTNT_ID"

    aput-object v2, v1, v7

    const-string v2, "BADGE_IMG_CNTNT_ID"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "CMPLT_DT"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "BADGE"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "USR_BADGE"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "BADGE"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "BADGE_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "USR_BADGE"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "BADGE_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "BADGE"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "DISPL_ORDR_NUM"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/BadgeDBQueries;->ALL_BADGES_USER_OWNS:Ljava/lang/String;

    .line 49
    const-string v0, "SELECT COUNT(*) FROM %s"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "BADGE"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/BadgeDBQueries;->COUNT_OF_ALL_BADGES:Ljava/lang/String;

    .line 54
    const-string v0, "SELECT %s, %s, %s, %s FROM %s WHERE %s IN (?) ORDER BY %s"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "BADGE_ID"

    aput-object v2, v1, v4

    const-string v2, "BADGE_CNTNT_ID"

    aput-object v2, v1, v5

    const-string v2, "BADGE_DESC_CNTNT_ID"

    aput-object v2, v1, v6

    const-string v2, "BADGE_IMG_CNTNT_ID"

    aput-object v2, v1, v7

    const-string v2, "BADGE"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "BADGE_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "DISPL_ORDR_NUM"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/BadgeDBQueries;->BADGES_BY_ID_:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final badgesById(I)Ljava/lang/String;
    .locals 5
    .param p0, "nBadges"    # I

    .prologue
    .line 73
    new-array v2, p0, [Ljava/lang/String;

    .line 74
    .local v2, "qs":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 75
    const-string v3, "?"

    aput-object v3, v2, v0

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    const-string v3, ","

    invoke-static {v3, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "paramList":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/BadgeDBQueries;->BADGES_BY_ID_:Ljava/lang/String;

    const-string v4, "?"

    invoke-virtual {v3, v4, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
