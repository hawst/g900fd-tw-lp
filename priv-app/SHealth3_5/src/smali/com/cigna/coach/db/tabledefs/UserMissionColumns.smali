.class public Lcom/cigna/coach/db/tabledefs/UserMissionColumns;
.super Ljava/lang/Object;
.source "UserMissionColumns.java"


# static fields
.field public static final KEY_FAILED_REASN_CD:Ljava/lang/String; = "FAILED_REASN_CD"

.field public static final KEY_GOAL_ID:Ljava/lang/String; = "GOAL_ID"

.field public static final KEY_GOAL_SEQ_ID:Ljava/lang/String; = "GOAL_SEQ_ID"

.field public static final KEY_MSSN_CMPLT_IND:Ljava/lang/String; = "MSSN_CMPLT_IND"

.field public static final KEY_MSSN_END_DT:Ljava/lang/String; = "MSSN_END_DT"

.field public static final KEY_MSSN_ID:Ljava/lang/String; = "MSSN_ID"

.field public static final KEY_MSSN_SEQ_ID:Ljava/lang/String; = "MSSN_SEQ_ID"

.field public static final KEY_MSSN_START_DT:Ljava/lang/String; = "MSSN_START_DT"

.field public static final KEY_SEL_MSSN_FREQ:Ljava/lang/String; = "USR_MSSN_FREQ"

.field public static final KEY_STAT_CD:Ljava/lang/String; = "STAT_CD"

.field public static final KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "USR_MSSN"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
