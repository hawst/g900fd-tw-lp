.class public Lcom/cigna/coach/db/UserMetricsDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "UserMetricsDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/cigna/coach/db/UserMetricsDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 47
    return-void
.end method

.method private insertUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)V
    .locals 5
    .param p1, "userKey"    # I
    .param p2, "userMetricName"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 50
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserMetric : START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 59
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 60
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "USR_ID"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 61
    const-string v2, "METRC_NM"

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->getMetricNameKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 62
    const-string v2, "METRC_VAL_TXT"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "USR_METRCS"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 65
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 66
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Insert completed"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_1
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 74
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserMetric: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_2
    return-void

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserMetric: "

    invoke-static {v2, v3, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 71
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 74
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "insertUserMetric: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v2
.end method

.method private updateUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)V
    .locals 7
    .param p1, "userKey"    # I
    .param p2, "userMetricName"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 80
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 81
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMetric: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_0
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 89
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 90
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "METRC_VAL_TXT"

    invoke-virtual {v1, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v3, "USR_ID = ? AND METRC_NM = ? "

    .line 93
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->getMetricNameKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 95
    .local v2, "whereArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "USR_METRCS"

    invoke-virtual {v4, v5, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 96
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 97
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Update completed"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v2    # "whereArgs":[Ljava/lang/String;
    .end local v3    # "whereClause":Ljava/lang/String;
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 105
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMetric: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    :cond_2
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in updateUserMetric: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 102
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 105
    sget-object v5, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserMetric: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v4
.end method


# virtual methods
.method public createOrUpdateUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z
    .locals 6
    .param p1, "userKey"    # I
    .param p2, "userMetricName"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 180
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 181
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "createOrUpdateUserMetric: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    :cond_0
    const/4 v1, 0x0

    .line 185
    .local v1, "isNewMetric":Z
    if-nez p2, :cond_2

    .line 186
    :try_start_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "userMetricName must not be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in createOrUpdateUserMetric: "

    invoke-static {v3, v4, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 208
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 210
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 211
    sget-object v4, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "createOrUpdateUserMetric: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    throw v3

    .line 188
    :cond_2
    if-eqz p3, :cond_3

    :try_start_2
    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    .line 189
    :cond_3
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "value for user metric: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " must not be null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 192
    :cond_4
    invoke-virtual {p0, p1, p2}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetricCount(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)I

    move-result v2

    .line 193
    .local v2, "userMetricCount":I
    if-nez v2, :cond_7

    .line 194
    const/4 v1, 0x1

    .line 195
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 196
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Inserting new metric: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lcom/cigna/coach/db/UserMetricsDBManager;->insertUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 210
    :goto_0
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 211
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "createOrUpdateUserMetric: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :cond_6
    return v1

    .line 201
    :cond_7
    :try_start_3
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 202
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Updating existing metric: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_8
    invoke-direct {p0, p1, p2, p3}, Lcom/cigna/coach/db/UserMetricsDBManager;->updateUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z
    .locals 3
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "userMetricName"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 172
    if-nez p1, :cond_0

    .line 173
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "userId must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 175
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v0

    .line 176
    .local v0, "userKey":I
    invoke-virtual {p0, v0, p2, p3}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public createOrUpdateUserMetrics(Lcom/cigna/coach/dataobjects/UserInfo;)V
    .locals 4
    .param p1, "userInfo"    # Lcom/cigna/coach/dataobjects/UserInfo;

    .prologue
    .line 148
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 149
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "createOrUpdateUserMetrics: START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_0
    if-eqz p1, :cond_1

    .line 153
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "userId":Ljava/lang/String;
    move-object v1, p1

    .line 156
    .local v1, "userMetrics":Lcom/cigna/coach/apiobjects/UserMetrics;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setCountry(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getDateOfBirth()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setDateOfBirth(Ljava/lang/String;Ljava/util/Calendar;)V

    .line 158
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getGender()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setGender(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;)V

    .line 159
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getHeight()F

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setHeight(Ljava/lang/String;F)V

    .line 160
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getMaritalStatus()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setMaritalStatus(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;)V

    .line 161
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getWeight()F

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->setWeight(Ljava/lang/String;F)V

    .line 164
    .end local v0    # "userId":Ljava/lang/String;
    .end local v1    # "userMetrics":Lcom/cigna/coach/apiobjects/UserMetrics;
    :cond_1
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 165
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "createOrUpdateUserMetrics: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_2
    return-void
.end method

.method public getBackupType(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 336
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->BACKUP_TYPE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 338
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->INCREMENTAL:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .line 340
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->getInstance(I)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    move-result-object v1

    goto :goto_0
.end method

.method public getCountry(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 282
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->COUNTRY:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 284
    const-string v0, ""

    .line 286
    .end local v0    # "str":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getDateOfBirth(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 3
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 291
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->DATE_OF_BIRTH:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 292
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 293
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v1

    .line 295
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v1

    goto :goto_0
.end method

.method public getGender(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 300
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 302
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .line 304
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->getInstance(I)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v1

    goto :goto_0
.end method

.method public getHasSeenWelcomeScreen(I)Z
    .locals 2
    .param p1, "userId"    # I

    .prologue
    .line 345
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HAS_SEEN_WELCOME_SCREEN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 346
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 347
    const/4 v1, 0x0

    .line 349
    :goto_0
    return v1

    :cond_0
    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getHeight(Ljava/lang/String;)F
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 309
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 311
    const/4 v1, 0x0

    .line 313
    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public getMaritalStatus(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 318
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 319
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 320
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .line 322
    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->getInstance(I)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    move-result-object v1

    goto :goto_0
.end method

.method public getUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;
    .locals 15
    .param p1, "userKey"    # I
    .param p2, "userMetricName"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .prologue
    .line 218
    sget-object v1, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    sget-object v1, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getUserMetric userMetricName"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " : START"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v1, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_0
    const/4 v11, 0x0

    .line 223
    .local v11, "value":Ljava/lang/String;
    const/4 v9, 0x0

    .line 225
    .local v9, "c":Landroid/database/Cursor;
    if-nez p2, :cond_3

    .line 226
    :try_start_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v12, "userMetricName must not be null"

    invoke-direct {v1, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :catch_0
    move-exception v10

    .line 245
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v1, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Error in getUserMetric userMetricName "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v1, v12, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 246
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v1

    if-eqz v9, :cond_1

    .line 250
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 251
    :cond_1
    sget-object v12, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 252
    sget-object v12, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "getUserMetric userMetricName "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " : END"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v1

    .line 229
    :cond_3
    :try_start_2
    const-string v2, "USR_METRCS"

    .line 230
    .local v2, "table":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v12, "METRC_VAL_TXT"

    aput-object v12, v3, v1

    .line 231
    .local v3, "columns":[Ljava/lang/String;
    const-string v4, "USR_ID = ? AND METRC_NM = ? "

    .line 232
    .local v4, "selection":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v1

    const/4 v1, 0x1

    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->getMetricNameKey()I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v5, v1

    .line 233
    .local v5, "selectionArgs":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 234
    .local v6, "groupBy":Ljava/lang/String;
    const/4 v7, 0x0

    .line 235
    .local v7, "having":Ljava/lang/String;
    const/4 v8, 0x0

    .line 237
    .local v8, "orderBy":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 239
    if-eqz v9, :cond_4

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    .line 240
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 241
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v11

    .line 249
    :cond_4
    if-eqz v9, :cond_5

    .line 250
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 251
    :cond_5
    sget-object v1, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 252
    sget-object v1, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getUserMetric userMetricName "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " : END"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v1, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :cond_6
    return-object v11
.end method

.method public getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "userMetricName"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .prologue
    .line 259
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getUserMetric userMetricName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : START"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_0
    if-nez p1, :cond_2

    .line 265
    :try_start_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "userId must not be null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v2, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error in getUserMetric userMetricName "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 272
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 276
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getUserMetric userMetricName "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : END"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    throw v2

    .line 267
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v1

    .line 268
    .local v1, "userKey":I
    invoke-virtual {p0, v1, p2}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 275
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 276
    sget-object v3, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getUserMetric userMetricName "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : END"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-object v2
.end method

.method getUserMetricCount(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)I
    .locals 13
    .param p1, "userKey"    # I
    .param p2, "userMetricName"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    .prologue
    .line 111
    sget-object v0, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    sget-object v0, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getUserMetricCount: START"

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    const/4 v9, 0x0

    .line 116
    .local v9, "count":I
    const/4 v8, 0x0

    .line 118
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "USR_METRCS"

    .line 119
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v11, "COUNT(*)"

    aput-object v11, v2, v0

    .line 120
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "USR_ID = ? AND METRC_NM = ? "

    .line 121
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v0

    const/4 v0, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->getMetricNameKey()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v4, v0

    .line 122
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 123
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 124
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 126
    .local v7, "orderBy":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 128
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 129
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 130
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 138
    :cond_1
    if-eqz v8, :cond_2

    .line 139
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 140
    :cond_2
    sget-object v0, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 141
    sget-object v0, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getUserMetricCount: END"

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_3
    return v9

    .line 133
    .end local v1    # "table":Ljava/lang/String;
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v4    # "selectionArgs":[Ljava/lang/String;
    .end local v5    # "groupBy":Ljava/lang/String;
    .end local v6    # "having":Ljava/lang/String;
    .end local v7    # "orderBy":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 134
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Error in getUserMetricCount: "

    invoke-static {v0, v11, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 135
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 139
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 140
    :cond_4
    sget-object v11, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 141
    sget-object v11, Lcom/cigna/coach/db/UserMetricsDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getUserMetricCount: END"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v0
.end method

.method public getUserMetrics(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 410
    new-instance v0, Lcom/cigna/coach/apiobjects/UserMetrics;

    invoke-direct {v0}, Lcom/cigna/coach/apiobjects/UserMetrics;-><init>()V

    .line 411
    .local v0, "ans":Lcom/cigna/coach/apiobjects/UserMetrics;
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setCountry(Ljava/lang/String;)V

    .line 412
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getDateOfBirth(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setDateOfBirth(Ljava/util/Calendar;)V

    .line 413
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getGender(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setGender(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;)V

    .line 414
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getHeight(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setHeight(F)V

    .line 415
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getMaritalStatus(Ljava/lang/String;)Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setMaritalStatus(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;)V

    .line 416
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getWeight(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/UserMetrics;->setWeight(F)V

    .line 417
    return-object v0
.end method

.method public getWeight(Ljava/lang/String;)F
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 327
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->WEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1}, Lcom/cigna/coach/db/UserMetricsDBManager;->getUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;)Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "str":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 329
    const/4 v1, 0x0

    .line 331
    :goto_0
    return v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public setBackupType(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;)V
    .locals 2
    .param p1, "userId"    # I
    .param p2, "backupType"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .prologue
    .line 398
    if-eqz p2, :cond_0

    .line 399
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->getBackupTypeKey()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, "value":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->BACKUP_TYPE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1, v0}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 402
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setBackupType(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;)V
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "backupType"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;

    .prologue
    .line 387
    if-eqz p2, :cond_0

    .line 388
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricBackupType;->getBackupTypeKey()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 389
    .local v0, "value":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->BACKUP_TYPE:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1, v0}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 391
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setCountry(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;

    .prologue
    .line 354
    if-eqz p2, :cond_0

    .line 355
    sget-object v0, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->COUNTRY:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v0, p2}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 357
    :cond_0
    return-void
.end method

.method public setDateOfBirth(Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 4
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "c"    # Ljava/util/Calendar;

    .prologue
    .line 360
    if-eqz p2, :cond_0

    .line 361
    invoke-static {p2}, Lcom/cigna/coach/utils/CalendarUtil;->fromCalendarToMilliSecs(Ljava/util/Calendar;)J

    move-result-wide v0

    .line 362
    .local v0, "millis":J
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 363
    .local v2, "value":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->DATE_OF_BIRTH:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v3, v2}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 365
    .end local v0    # "millis":J
    .end local v2    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setGender(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;)V
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "gender"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    .prologue
    .line 368
    if-eqz p2, :cond_0

    .line 369
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->getGenderTypeKey()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 370
    .local v0, "value":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->GENDER:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1, v0}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 372
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setHasSeenWelcomeScreen(IZ)V
    .locals 2
    .param p1, "userId"    # I
    .param p2, "hasSeenWelcomeScreen"    # Z

    .prologue
    .line 394
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HAS_SEEN_WELCOME_SCREEN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    if-eqz p2, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, p1, v1, v0}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(ILcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 395
    return-void

    .line 394
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public setHeight(Ljava/lang/String;F)V
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "height"    # F

    .prologue
    .line 375
    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "value":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->HEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1, v0}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 377
    return-void
.end method

.method public setMaritalStatus(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;)V
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "maritalStatus"    # Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    .prologue
    .line 380
    if-eqz p2, :cond_0

    .line 381
    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;->getMaritalStatusKey()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, "value":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->MARITAL_STATUS:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1, v0}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 384
    .end local v0    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setWeight(Ljava/lang/String;F)V
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "weight"    # F

    .prologue
    .line 405
    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    .line 406
    .local v0, "value":Ljava/lang/String;
    sget-object v1, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;->WEIGHT:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;

    invoke-virtual {p0, p1, v1, v0}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetric(Ljava/lang/String;Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricName;Ljava/lang/String;)Z

    .line 407
    return-void
.end method
