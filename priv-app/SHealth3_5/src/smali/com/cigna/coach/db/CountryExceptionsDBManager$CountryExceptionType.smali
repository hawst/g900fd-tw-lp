.class public final enum Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
.super Ljava/lang/Enum;
.source "CountryExceptionsDBManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/CountryExceptionsDBManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CountryExceptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

.field public static final enum ALCOHOL_SUPRESSION:Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field countryExceptionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 354
    new-instance v4, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    const-string v5, "ALCOHOL_SUPRESSION"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->ALCOHOL_SUPRESSION:Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    .line 353
    new-array v4, v7, [Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    sget-object v5, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->ALCOHOL_SUPRESSION:Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    aput-object v5, v4, v6

    sput-object v4, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->$VALUES:[Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    .line 366
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->codeValueMap:Ljava/util/HashMap;

    .line 368
    invoke-static {}, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->values()[Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 369
    .local v3, "type":Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    sget-object v4, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->getCountryExceptionType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 371
    .end local v3    # "type":Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 360
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 361
    iput p3, p0, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->countryExceptionType:I

    .line 362
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    .locals 2
    .param p0, "countryExceptionTypeKey"    # I

    .prologue
    .line 380
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 353
    const-class v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;
    .locals 1

    .prologue
    .line 353
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->$VALUES:[Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    invoke-virtual {v0}, [Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;

    return-object v0
.end method


# virtual methods
.method public getCountryExceptionType()I
    .locals 1

    .prologue
    .line 375
    iget v0, p0, Lcom/cigna/coach/db/CountryExceptionsDBManager$CountryExceptionType;->countryExceptionType:I

    return v0
.end method
