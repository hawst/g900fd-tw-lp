.class public final Lcom/cigna/coach/db/HealthLibraryDBQueries;
.super Ljava/lang/Object;
.source "HealthLibraryDBQueries.java"


# static fields
.field static final ACTV_ARTCL_COUNT_QUERY:Ljava/lang/String; = "SELECT COUNT(*) FROM ARTCL WHERE ARTCL_ID=? AND ACTV_IND=1"

.field static final ACTV_USR_ARTCL_COUNT_QUERY:Ljava/lang/String; = "SELECT COUNT(*) FROM USR_ARTCL WHERE ARTCL_ID=? AND USR_ID=?"

.field static final ALCOHOL_EXCLUSION_QUERY:Ljava/lang/String; = "SELECT A.ARTCL_ID FROM ARTCL_DTL_VW A WHERE A.SUB_CTGRY_ID = 205"

.field static final ARTCL_DTL_VW_ARTCL_ID_IDX:I = 0x0

.field static final ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

.field static final ARTCL_DTL_VW_CTGRY_CNTNT_ID_IDX:I = 0x6

.field static final ARTCL_DTL_VW_CTGRY_ID_IDX:I = 0x1

.field static final ARTCL_DTL_VW_CTGRY_IMG_CNTNT_ID_IDX:I = 0x7

.field static final ARTCL_DTL_VW_DTL_CNTNT_ID_IDX:I = 0x4

.field static final ARTCL_DTL_VW_IMG_CNTNT_ID_IDX:I = 0x5

.field static final ARTCL_DTL_VW_PRIM_ARTCL_IND:I = 0xa

.field static final ARTCL_DTL_VW_SUB_CTGRY_CNTNT_ID_IDX:I = 0x8

.field static final ARTCL_DTL_VW_SUB_CTGRY_ID_IDX:I = 0x2

.field static final ARTCL_DTL_VW_SUB_CTGRY_IMG_CNTNT_ID_IDX:I = 0x9

.field static final ARTCL_DTL_VW_TTL_CNTNT_ID_IDX:I = 0x3

.field static final ARTICLES_KEY_ARTICLE_ID:Ljava/lang/String; = "ARTCL_ID"

.field static final ARTICLES_TABLE_NAME:Ljava/lang/String; = "ARTCL"

.field static final ARTICLE_DETAIL_QUERY:Ljava/lang/String;

.field static final ARTICLE_TABLE_COLUMNS:Ljava/lang/String;

.field static final CATEGORY_ARTICLE_COLUMNS:Ljava/lang/String;

.field static final CATEGORY_ARTICLE_TABLE_NAME:Ljava/lang/String; = "CTGRY_ARTCL"

.field static final CONTENT_TABLE_COLUMNS:Ljava/lang/String;

.field static final CONTENT_TABLE_NAME:Ljava/lang/String; = "CONTENT"

.field static final FAVES_ARTICLES_BY_CTGRY_QUERY:Ljava/lang/String;

.field static final FAVES_ENTRY_WHERE:Ljava/lang/String; = "USR_ID=? AND ARTCL_ID=?"

.field static final FAVES_KEY_ARTICLE_ID:Ljava/lang/String; = "ARTCL_ID"

.field static final FAVES_KEY_USR_ID:Ljava/lang/String; = "USR_ID"

.field static final FAVES_TABLE_NAME:Ljava/lang/String; = "USR_ARTCL"

.field static final FEAT_ARTICLES_LIST_QUERY_STUB:Ljava/lang/String;

.field static final FEAT_ARTICLES_UNIVERSE_ARTCL_ID_IDX:I = 0x0

.field static final FEAT_ARTICLES_UNIVERSE_CTGRY_ID_IDX:I = 0x1

.field static final FEAT_ARTICLES_UNIVERSE_ORDER_BY:Ljava/lang/String; = " ORDER BY CTGRY.RANK_NUM"

.field static final FEAT_ARTICLES_UNIVERSE_QUERY:Ljava/lang/String; = "SELECT CTGRY_ARTCL.ARTCL_ID AS C0,CTGRY_ARTCL.CTGRY_ID AS C1 FROM CTGRY_ARTCL JOIN CTGRY ON CTGRY.CTGRY_ID=CTGRY_ARTCL.CTGRY_ID JOIN ARTCL ON ARTCL.ARTCL_ID=CTGRY_ARTCL.ARTCL_ID WHERE PRIM_ARTCL_IND=1 AND ARTCL.ACTV_IND=1 AND CTGRY.ACTV_IND=1"

.field static final KWD_ARTICLES_QUERY:Ljava/lang/String;

.field static final NON_PRIMARY_ARTICLE_DETAIL_QUERY:Ljava/lang/String;

.field static final SUBCATEGORY_ARTICLES_QUERY:Ljava/lang/String;

.field static final USER_ARTICLE_COLUMNS:Ljava/lang/String;

.field static final USER_INFO_COLUMNS:Ljava/lang/String;

.field static final USER_INFO_TABLE_NAME:Ljava/lang/String; = "USR_INFO"

.field static final USR_ARTCL_USR_ID_IDX:I = 0xb


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 46
    const-string v0, ","

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ARTCL_DTL_VW.ARTCL_ID"

    aput-object v2, v1, v4

    const-string v2, "ARTCL_DTL_VW.CTGRY_ID"

    aput-object v2, v1, v5

    const-string v2, "ARTCL_DTL_VW.SUB_CTGRY_ID"

    aput-object v2, v1, v6

    const-string v2, "ARTCL_DTL_VW.TTL_CNTNT_ID"

    aput-object v2, v1, v7

    const-string v2, "ARTCL_DTL_VW.DTL_CNTNT_ID"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "ARTCL_DTL_VW.IMG_CNTNT_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "ARTCL_DTL_VW.CTGRY_CNTNT_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "ARTCL_DTL_VW.CTGRY_IMG_CNTNT_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "ARTCL_DTL_VW.SUB_CTGRY_CNTNT_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "ARTCL_DTL_VW.SUB_CTGRY_IMG_CNTNT_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "ARTCL_DTL_VW.PRIM_ARTCL_IND"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    new-array v2, v6, [Ljava/lang/String;

    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

    aput-object v3, v2, v4

    const-string v3, "USR_ARTCL.USR_ID"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM ARTCL_DTL_VW"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN USR_ARTCL ON USR_ARTCL.ARTCL_ID=ARTCL_DTL_VW.ARTCL_ID AND USR_ARTCL.USR_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE ARTCL_DTL_VW.CTGRY_ID=? AND ARTCL_DTL_VW.SUB_CTGRY_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->SUBCATEGORY_ARTICLES_QUERY:Ljava/lang/String;

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT DISTINCT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    new-array v2, v6, [Ljava/lang/String;

    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

    aput-object v3, v2, v4

    const-string v3, "USR_ARTCL.USR_ID"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM ARTCL_DTL_VW"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " JOIN FTS_CNTNT_KEYWRD ON ARTCL_DTL_VW.TTL_CNTNT_ID=FTS_CNTNT_KEYWRD.CNTNT_ID OR ARTCL_DTL_VW.DTL_CNTNT_ID=FTS_CNTNT_KEYWRD.CNTNT_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "       AND FTS_CNTNT_KEYWRD.LOC_TY_IND = ? "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN USR_ARTCL ON USR_ARTCL.ARTCL_ID=ARTCL_DTL_VW.ARTCL_ID AND USR_ARTCL.USR_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE FTS_CNTNT_KEYWRD.CNTNT_KEYWRD MATCH ? AND ARTCL_DTL_VW.PRIM_ARTCL_IND=1 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->KWD_ARTICLES_QUERY:Ljava/lang/String;

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    new-array v2, v6, [Ljava/lang/String;

    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

    aput-object v3, v2, v4

    const-string v3, "USR_ARTCL.USR_ID"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM ARTCL_DTL_VW"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN USR_ARTCL ON USR_ARTCL.ARTCL_ID=ARTCL_DTL_VW.ARTCL_ID AND USR_ARTCL.USR_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE ARTCL_DTL_VW.PRIM_ARTCL_IND=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->FEAT_ARTICLES_LIST_QUERY_STUB:Ljava/lang/String;

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    new-array v2, v6, [Ljava/lang/String;

    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

    aput-object v3, v2, v4

    const-string v3, "USR_ARTCL.USR_ID"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM ARTCL_DTL_VW"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN USR_ARTCL ON USR_ARTCL.ARTCL_ID=ARTCL_DTL_VW.ARTCL_ID AND USR_ARTCL.USR_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE ARTCL_DTL_VW.PRIM_ARTCL_IND=1 AND ARTCL_DTL_VW.ARTCL_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTICLE_DETAIL_QUERY:Ljava/lang/String;

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    new-array v2, v6, [Ljava/lang/String;

    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

    aput-object v3, v2, v4

    const-string v3, "USR_ARTCL.USR_ID"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM ARTCL_DTL_VW"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN USR_ARTCL ON USR_ARTCL.ARTCL_ID=ARTCL_DTL_VW.ARTCL_ID AND USR_ARTCL.USR_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE ARTCL_DTL_VW.SUB_CTGRY_ID=? AND ARTCL_DTL_VW.ARTCL_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->NON_PRIMARY_ARTICLE_DETAIL_QUERY:Ljava/lang/String;

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    new-array v2, v6, [Ljava/lang/String;

    sget-object v3, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTCL_DTL_VW_COLUMNS:Ljava/lang/String;

    aput-object v3, v2, v4

    const-string v3, "USR_ARTCL.USR_ID"

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM ARTCL_DTL_VW"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " JOIN USR_ARTCL ON ARTCL_DTL_VW.ARTCL_ID=USR_ARTCL.ARTCL_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE ARTCL_DTL_VW.PRIM_ARTCL_IND=1 AND USR_ARTCL.USR_ID=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->FAVES_ARTICLES_BY_CTGRY_QUERY:Ljava/lang/String;

    .line 171
    const-string v0, "%s,%s,%s,%s,%s,%s"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "CNTNT_ID"

    aput-object v2, v1, v4

    const-string v2, "LOC_TY_IND"

    aput-object v2, v1, v5

    const-string v2, "LANG_SPCF_CNTNT_IND"

    aput-object v2, v1, v6

    const-string v2, "CNTNT_TXT"

    aput-object v2, v1, v7

    const-string v2, "CNTNT_KEYWRD"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "ACTV_IND"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->CONTENT_TABLE_COLUMNS:Ljava/lang/String;

    .line 178
    const-string v0, "%s,%s,%s,%s,%s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "ARTCL_ID"

    aput-object v2, v1, v4

    const-string v2, "CNTNT_ID"

    aput-object v2, v1, v5

    const-string v2, "DTL_CNTNT_ID"

    aput-object v2, v1, v6

    const-string v2, "IMG_CNTNT_ID"

    aput-object v2, v1, v7

    const-string v2, "ACTV_IND"

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->ARTICLE_TABLE_COLUMNS:Ljava/lang/String;

    .line 183
    const-string v0, "%s,%s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "USR_ID"

    aput-object v2, v1, v4

    const-string v2, "ARTCL_ID"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->USER_ARTICLE_COLUMNS:Ljava/lang/String;

    .line 187
    const-string v0, "%s,%s,%s"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "USR_ID"

    aput-object v2, v1, v4

    const-string v2, "USR_ID_TXT"

    aput-object v2, v1, v5

    const-string v2, "USR_INFO_DT"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->USER_INFO_COLUMNS:Ljava/lang/String;

    .line 192
    const-string v0, "%s,%s,%s,%s"

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "ARTCL_ID"

    aput-object v2, v1, v4

    const-string v2, "CTGRY_ID"

    aput-object v2, v1, v5

    const-string v2, "SUB_CTGRY_ID"

    aput-object v2, v1, v6

    const-string v2, "PRIM_ARTCL_IND"

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/HealthLibraryDBQueries;->CATEGORY_ARTICLE_COLUMNS:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    return-void
.end method
