.class public final Lcom/cigna/coach/db/CoachMessageDBQueries;
.super Ljava/lang/Object;
.source "CoachMessageDBQueries.java"


# static fields
.field public static final GET_21_DAYS_NO_ACTION_MISSION:Ljava/lang/String; = " SELECT A.MSSN_ID, MAX (A.MSSN_END_DT)  AS END_DATE FROM USR_MSSN A INNER JOIN MSSN B ON B.MSSN_ID = A.MSSN_ID WHERE A.USR_ID   = ? AND A.GOAL_ID   = ? AND B.MSSN_STG_ID   = ? AND (A.STAT_CD   = ? OR A.STAT_CD = ? )"

.field public static final GET_ACTION_MISSIONS:Ljava/lang/String; = " SELECT A.MSSN_ID FROM USR_MSSN A INNER JOIN MSSN B ON B.MSSN_ID = A.MSSN_ID WHERE A.USR_ID   = ? AND A.GOAL_SEQ_ID   = ? AND B.MSSN_STG_ID   = ? "

.field public static final GET_ACTIVE_USER_MISSIONS_WITH_NO_MISSION_DATA_ENTERED:Ljava/lang/String;

.field public static final GET_LATEST_CATEGORY_SCORE:Ljava/lang/String; = " SELECT SCORE_VAL FROM LFSTYL_ASSMT_SCORE WHERE USR_ID   = ? AND (SCORE_TY_IND   = ? OR SCORE_TY_IND   = ?) AND SCORE_TY_ID   = ?  ORDER BY LFSTYL_ASSMT_TIMESTMP DESC"

.field public static final GET_NUMBER_OF_ACTIVE_USER_GOALS:Ljava/lang/String; = "SELECT COUNT(*) FROM USR_GOAL WHERE USR_ID =? AND STAT_IND =? "

.field public static final GET_NUMBER_OF_ACTIVE_USER_MISSIONS:Ljava/lang/String; = "SELECT COUNT(*) FROM USR_MSSN WHERE USR_ID =? AND STAT_CD =? "

.field public static final GET_NUMBER_OF_USER_GOALS_THAT_ARE_BEHIND_SQL:Ljava/lang/String; = "SELECT COUNT(*) FROM GOAL G INNER JOIN USR_GOAL UG  ON G.GOAL_ID = UG.GOAL_ID  WHERE  (UG.LAST_MSSN_CNTD_DT + (G.MAX_DAYS_BTWN_MSSN_NUM * 86400000)) > ?"

.field public static final GET_USER_GOAL_START_DATE:Ljava/lang/String; = " SELECT START_DT FROM USR_GOAL WHERE USR_ID   = ? AND SEQ_ID   = ? "


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.STAT_CD = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MSSN_SEQ_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NOT IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " SELECT DISTINCT("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MSSN_SEQ_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "USR_MSSN_DATA"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/CoachMessageDBQueries;->GET_ACTIVE_USER_MISSIONS_WITH_NO_MISSION_DATA_ENTERED:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
