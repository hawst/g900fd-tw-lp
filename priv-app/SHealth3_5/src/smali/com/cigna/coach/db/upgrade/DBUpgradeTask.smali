.class public Lcom/cigna/coach/db/upgrade/DBUpgradeTask;
.super Ljava/lang/Object;
.source "DBUpgradeTask.java"


# instance fields
.field private context:Landroid/content/Context;

.field private upgradeActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/db/upgrade/IDBUpgrade;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->upgradeActions:Ljava/util/List;

    .line 15
    iput-object p1, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->context:Landroid/content/Context;

    .line 16
    return-void
.end method


# virtual methods
.method public getUpgradeActions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/db/upgrade/IDBUpgrade;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->upgradeActions:Ljava/util/List;

    return-object v0
.end method

.method performDBUpgradeActions(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 34
    iget-object v2, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->upgradeActions:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->upgradeActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 42
    :cond_0
    return-void

    .line 37
    :cond_1
    iget-object v2, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->upgradeActions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/db/upgrade/IDBUpgrade;

    .line 38
    .local v0, "dbUpgradeAction":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    if-eqz v0, :cond_2

    .line 39
    iget-object v2, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->context:Landroid/content/Context;

    invoke-interface {v0, v2, p1}, Lcom/cigna/coach/db/upgrade/IDBUpgrade;->applyUpgrade(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method

.method public setUpgradeActions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/db/upgrade/IDBUpgrade;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "upgradeActions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/db/upgrade/IDBUpgrade;>;"
    iput-object p1, p0, Lcom/cigna/coach/db/upgrade/DBUpgradeTask;->upgradeActions:Ljava/util/List;

    .line 31
    return-void
.end method
