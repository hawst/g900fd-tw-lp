.class public abstract Lcom/cigna/coach/db/GoalMissoinDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "GoalMissoinDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/cigna/coach/db/GoalMissoinDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 75
    return-void
.end method

.method private getMissionTips(I)Ljava/util/List;
    .locals 12
    .param p1, "missionId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionTip;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    sget-object v0, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    sget-object v0, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMissionTips :Start"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    :cond_0
    const/4 v9, 0x0

    .line 306
    .local v9, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    const/4 v8, 0x0

    .line 309
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "MSSN_TIP"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TIP_CNTNT_ID"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "TIP_TITLE_CNTNT_ID"

    aput-object v4, v2, v3

    const-string v3, "MSSN_ID = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 313
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 314
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 316
    .end local v9    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    .local v10, "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    :goto_0
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 318
    new-instance v11, Lcom/cigna/coach/dataobjects/MissionTipData;

    invoke-direct {v11}, Lcom/cigna/coach/dataobjects/MissionTipData;-><init>()V

    .line 319
    .local v11, "tip":Lcom/cigna/coach/dataobjects/MissionTipData;
    const-string v0, "TIP_CNTNT_ID"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/MissionTipData;->setContentId(I)V

    .line 320
    const-string v0, "TIP_TITLE_CNTNT_ID"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v11, v0}, Lcom/cigna/coach/dataobjects/MissionTipData;->setTitleContentId(I)V

    .line 321
    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 327
    .end local v11    # "tip":Lcom/cigna/coach/dataobjects/MissionTipData;
    :catchall_0
    move-exception v0

    move-object v9, v10

    .end local v10    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    .restart local v9    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    :goto_1
    if-eqz v8, :cond_1

    .line 328
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 330
    :cond_1
    sget-object v1, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 331
    sget-object v1, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "getMissionTips :End"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v0

    .end local v9    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    .restart local v10    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    :cond_3
    move-object v9, v10

    .line 327
    .end local v10    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    .restart local v9    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    :cond_4
    if-eqz v8, :cond_5

    .line 328
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 330
    :cond_5
    sget-object v0, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 331
    sget-object v0, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getMissionTips :End"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    :cond_6
    return-object v9

    .line 327
    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public getActiveUserMissionsCount(Ljava/lang/String;)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 439
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 440
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting all Active User Missions Count: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_0
    if-nez p1, :cond_2

    move v2, v4

    .line 474
    :cond_1
    :goto_0
    return v2

    .line 446
    :cond_2
    const/4 v0, 0x0

    .line 447
    .local v0, "c":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 450
    .local v2, "returnValue":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 452
    .local v3, "userKey":I
    if-eq v3, v4, :cond_3

    .line 454
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "SELECT COUNT(*) FROM USR_MSSN WHERE USR_ID =? AND STAT_CD =? "

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 457
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_3

    .line 458
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 459
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 466
    :cond_3
    if-eqz v0, :cond_4

    .line 467
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 469
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 470
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Getting all Active User Missions Count: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    .end local v3    # "userKey":I
    :catch_0
    move-exception v1

    .line 463
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error getting number of active missions: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 464
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 466
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_5

    .line 467
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 469
    :cond_5
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 470
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Getting all Active User Missions Count: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v4
.end method

.method public getCompletedMissionsCountBetweenDays(Ljava/lang/String;JJ)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "startDate"    # J
    .param p4, "endDate"    # J

    .prologue
    const/4 v4, -0x1

    .line 400
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 401
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCompletedMissionsCountBetweenDays: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    :cond_0
    const/4 v2, 0x0

    .line 404
    .local v2, "returnValue":I
    if-nez p1, :cond_1

    .line 434
    :goto_0
    return v4

    .line 407
    :cond_1
    const/4 v0, 0x0

    .line 410
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 412
    .local v3, "userKey":I
    if-eq v3, v4, :cond_2

    .line 413
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "SELECT COUNT(*) FROM USR_MSSN WHERE USR_ID = ?  AND STAT_CD = ?  AND MSSN_END_DT >= ?  AND MSSN_END_DT <= ? "

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 417
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 418
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 419
    const/4 v4, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 426
    :cond_2
    if-eqz v0, :cond_3

    .line 427
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 429
    :cond_3
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 430
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedMissionsCountBetweenDays: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    move v4, v2

    .line 434
    goto/16 :goto_0

    .line 422
    .end local v3    # "userKey":I
    :catch_0
    move-exception v1

    .line 423
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error getting number of completed missions: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 424
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 426
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_5

    .line 427
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 429
    :cond_5
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 430
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCompletedMissionsCountBetweenDays: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v4
.end method

.method public getMissionActivityCompletedCount(Ljava/util/List;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 479
    .local p1, "missionDetails":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    if-eqz p1, :cond_0

    .line 480
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 482
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUserMissionDetailsList(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;
    .locals 10
    .param p1, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 341
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserMissionDetailsList :Start"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_0
    new-instance v7, Lcom/cigna/coach/db/TrackerDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/cigna/coach/db/TrackerDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSpecificTracker()Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/cigna/coach/db/TrackerDBManager;->getSamsungExerciseIds(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)Ljava/util/List;

    move-result-object v4

    .line 345
    .local v4, "samsungSpecificExerciseIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getUserId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 346
    .local v5, "userKey":I
    const-string v3, "SELECT MSSN_ACCOMP_DT, MSSN_ACTY_CNT, SRC_REF  FROM  USR_MSSN_DATA WHERE MSSN_ID = ? AND USR_ID = ? AND MSSN_SEQ_ID = ?"

    .line 347
    .local v3, "query":Ljava/lang/String;
    const/4 v7, 0x3

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    .line 350
    .local v6, "whereParams":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 351
    .local v1, "missionDetailsCursor":Landroid/database/Cursor;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 354
    .local v2, "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-virtual {v7, v3, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 356
    if-eqz v1, :cond_4

    .line 357
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 359
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_4

    .line 361
    new-instance v0, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;-><init>()V

    .line 362
    .local v0, "missionDetails":Lcom/cigna/coach/dataobjects/MissionDetailsData;
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    .line 363
    const-string v7, "MSSN_ACCOMP_DT"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setMissionActivityCompletedDate(Ljava/util/Calendar;)V

    .line 366
    const-string v7, "MSSN_ACTY_CNT"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setNoOfActivities(I)V

    .line 368
    const-string v7, "SRC_REF"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->getInstance(I)Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setActivitySource(Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    .line 370
    invoke-virtual {v0, v4}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setexerciseIds(Ljava/util/List;)V

    .line 372
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 374
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "adding mission details to mission"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MissionActivityCompletedDate :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v9

    invoke-static {v9}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "NoOfActivities :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getNoOfActivities()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ExerciseId :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getExerciseId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ActivitySource :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getActivitySource()Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 386
    .end local v0    # "missionDetails":Lcom/cigna/coach/dataobjects/MissionDetailsData;
    :catchall_0
    move-exception v7

    if-eqz v1, :cond_2

    .line 387
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 389
    :cond_2
    sget-object v8, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 390
    sget-object v8, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserMissionDetailsList :End"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v7

    .line 386
    :cond_4
    if-eqz v1, :cond_5

    .line 387
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 389
    :cond_5
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 390
    sget-object v7, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserMissionDetailsList :End"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :cond_6
    return-object v2
.end method

.method public retriveGoalMissionDataFromCursor(Landroid/database/Cursor;I)Lcom/cigna/coach/dataobjects/GoalMissionData;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "goalId"    # I

    .prologue
    .line 251
    sget-object v1, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    sget-object v1, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "retriveGoalMissionDataFromCursor :Start"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :cond_0
    :try_start_0
    new-instance v0, Lcom/cigna/coach/dataobjects/GoalMissionData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;-><init>()V

    .line 259
    .local v0, "goalMissionData":Lcom/cigna/coach/dataobjects/GoalMissionData;
    const-string v1, "MSSN_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setContentId(I)V

    .line 260
    const-string v1, "MSSN_IMG_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setImageContentId(I)V

    .line 261
    const-string v1, "MSSN_DESC_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setDescriptionContentId(I)V

    .line 263
    const-string v1, "MSSN_WHY_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setWhyContentId(I)V

    .line 264
    const-string v1, "MSSN_WHAT_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setWhatContentId(I)V

    .line 265
    const-string v1, "MSSN_FREQ_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setFrequencyContentId(I)V

    .line 267
    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setCurrentStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    .line 268
    const-string v1, "DLY_TRGT_NUM"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setDailyTarget(I)V

    .line 269
    const-string v1, "DEFLT_FREQ_IND"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setDefaultFrequency(I)V

    .line 270
    const-string v1, "DEPNDNT_MSSN_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setDependentMissionId(I)V

    .line 271
    invoke-virtual {v0, p2}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setGoalId(I)V

    .line 272
    const-string v1, "DLY_TRGT_NUM"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setMaxDailyTarget(I)V

    .line 273
    const-string v1, "FREQ_END_RNG_NUM"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setMaxFrequency(I)V

    .line 274
    const-string v1, "FREQ_START_RNG_NUM"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setMinFrequency(I)V

    .line 275
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getMaxFrequency()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setFrequencyMax(I)V

    .line 276
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getMinFrequency()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setFrequencyMin(I)V

    .line 278
    const-string v1, "MSSN_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setMissionId(I)V

    .line 279
    const-string v1, "MSSN_STG_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setMissionStage(Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;)V

    .line 281
    const-string v1, "PRIM_CTGRY_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setPrimaryCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 283
    const-string v1, "PRIM_SUB_CTGRY_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setPrimarySubcategory(Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;)V

    .line 286
    const-string v1, "MSSN_SPAN_DAY_NUM"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setSpanInDays(I)V

    .line 287
    const-string v1, "SPCF_TRCKER_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->getInstance(I)Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setSpecificTracker(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)V

    .line 289
    const-string v1, "TRCKER_RULE_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setTrackerRuleId(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;)V

    .line 291
    const-string v1, "UNT_NUM"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setUnitsToTrack(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    sget-object v1, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 294
    sget-object v1, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v2, "retriveGoalMissionDataFromCursor :End"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_1
    return-object v0

    .line 293
    .end local v0    # "goalMissionData":Lcom/cigna/coach/dataobjects/GoalMissionData;
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 294
    sget-object v2, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "retriveGoalMissionDataFromCursor :End"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v1
.end method

.method public retriveUserGoalDataFromCursor(Landroid/database/Cursor;Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserGoalData;
    .locals 6
    .param p1, "goalsCursor"    # Landroid/database/Cursor;
    .param p2, "userId"    # Ljava/lang/String;

    .prologue
    .line 78
    sget-object v3, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 79
    sget-object v3, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "retriveUserGoalDataFromCursor :Start"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    const/4 v1, 0x0

    .line 84
    .local v1, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :try_start_0
    new-instance v2, Lcom/cigna/coach/dataobjects/UserGoalData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/UserGoalData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    .end local v1    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .local v2, "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :try_start_1
    const-string v3, "GOAL_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalId(I)V

    .line 86
    const-string v3, "SEQ_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setGoalSequenceId(I)V

    .line 87
    const-string v3, "GOAL_CNTNT_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setContentId(I)V

    .line 88
    const-string v3, "GOAL_IMG_CNTNT_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setImageContentId(I)V

    .line 90
    const-string v3, "MSSN_TO_CMPLT_NUM"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setNumMissionsToComplete(I)V

    .line 92
    const-string v3, "LFSTYL_ASSMT_CTRGY_SCORE"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryScore(I)V

    .line 94
    const-string v3, "END_DT"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCompletedDate(Ljava/util/Calendar;)V

    .line 96
    const-string v3, "DISPL_ORDR_NUM"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setDisplayOrderNumber(I)V

    .line 98
    const-string v3, "GOAL_FREQ_CNTNT_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setFrequencyContentId(I)V

    .line 100
    const-string v3, "LAST_MSSN_CNTD_DT"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setLastMissionCountedDate(Ljava/util/Calendar;)V

    .line 102
    const-string v3, "MAX_DAY_GRACE_PER_NUM"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setMaxGracePeriodDays(I)V

    .line 104
    const-string v3, "MIN_MSSN_FREQ_NUM"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setMinMissionFrequencyNumber(I)V

    .line 106
    const-string v3, "MAX_DAYS_BTWN_MSSN_NUM"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setNumMaxDaysBetweenMissions(I)V

    .line 108
    const-string v3, "MIN_DAYS_BTWN_MSSN_NUM"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setNumMinDaysBetweenMissions(I)V

    .line 110
    const-string v3, "CMPLTD_MSSN_CNT"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setNumMissionsCompleted(I)V

    .line 112
    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_SCORE"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionGroupScore(I)V

    .line 114
    const-string v3, "CTGRY_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 117
    new-instance v0, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    invoke-direct {v0}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;-><init>()V

    .line 118
    .local v0, "questionAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->setQuestionGroupId(I)V

    .line 120
    invoke-virtual {v2, v0}, Lcom/cigna/coach/dataobjects/UserGoalData;->setQuestionsAnswerGroup(Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;)V

    .line 122
    const-string v3, "START_DT"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setStartDate(Ljava/util/Calendar;)V

    .line 124
    const-string v3, "STAT_IND"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V

    .line 126
    invoke-virtual {v2, p2}, Lcom/cigna/coach/dataobjects/UserGoalData;->setUserId(Ljava/lang/String;)V

    .line 127
    const-string v3, "GOAL_WHY_CNTNT_ID"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/UserGoalData;->setWhyContentId(I)V

    .line 130
    new-instance v3, Lcom/cigna/coach/utils/WeightChangeListener;

    invoke-virtual {p0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/cigna/coach/utils/WeightChangeListener;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    invoke-virtual {v3, p2, v2}, Lcom/cigna/coach/utils/WeightChangeListener;->populateWeightGoalNameFields(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserGoalData;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 133
    sget-object v3, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 134
    sget-object v3, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "retriveUserGoalDataFromCursor :END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_1
    return-object v2

    .line 133
    .end local v0    # "questionAnswerGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    .end local v2    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v1    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :catchall_0
    move-exception v3

    :goto_0
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 134
    sget-object v4, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "retriveUserGoalDataFromCursor :END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v3

    .line 133
    .end local v1    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v2    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    .restart local v1    # "userGoalData":Lcom/cigna/coach/dataobjects/UserGoalData;
    goto :goto_0
.end method

.method public retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;
    .locals 1
    .param p1, "missionsCursor"    # Landroid/database/Cursor;

    .prologue
    .line 141
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/cigna/coach/db/GoalMissoinDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;Z)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v0

    return-object v0
.end method

.method public retriveUserMissionDataFromCursor(Landroid/database/Cursor;Z)Lcom/cigna/coach/dataobjects/UserMissionData;
    .locals 9
    .param p1, "missionsCursor"    # Landroid/database/Cursor;
    .param p2, "isSetTips"    # Z

    .prologue
    const/4 v6, 0x1

    .line 145
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 146
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "retriveUserMissionDataFromCursor :Start"

    invoke-static {v5, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_0
    :try_start_0
    new-instance v4, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-direct {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;-><init>()V

    .line 152
    .local v4, "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    const-string v5, "MSSN_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 153
    .local v2, "missionId":I
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 154
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Reading mission:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :cond_1
    invoke-virtual {v4, v2}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionId(I)V

    .line 158
    const-string v5, "MSSN_CNTNT_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setContentId(I)V

    .line 160
    const-string v5, "MSSN_IMG_CNTNT_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setImageContentId(I)V

    .line 162
    const-string v5, "MSSN_FREQ_CNTNT_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setFrequencyContentId(I)V

    .line 164
    const-string v5, "MSSN_WHAT_CNTNT_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setWhatContentId(I)V

    .line 166
    const-string v5, "MSSN_WHY_CNTNT_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setWhyContentId(I)V

    .line 168
    const-string v5, "DLY_TRGT_NUM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDailyTarget(I)V

    .line 170
    const-string v5, "DEFLT_FREQ_IND"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDefaultFrequency(I)V

    .line 172
    const-string v5, "DEPNDNT_MSSN_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDependentMissionId(I)V

    .line 174
    const-string v5, "GOAL_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setGoalId(I)V

    .line 176
    const-string v5, "GOAL_SEQ_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setGoalSequenceId(I)V

    .line 178
    const-string v5, "DLY_TRGT_NUM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMaxDailyTarget(I)V

    .line 180
    const-string v5, "FREQ_END_RNG_NUM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMaxFrequency(I)V

    .line 182
    const-string v5, "FREQ_START_RNG_NUM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMinFrequency(I)V

    .line 184
    const-string v5, "TRCKER_RULE_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setTrackerRuleId(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;)V

    .line 186
    const-string v5, "UNT_NUM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setUnitsToTrack(I)V

    .line 188
    const-string v5, "MSSN_SEQ_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionSeqId(I)V

    .line 190
    const-string v5, "MSSN_DESC_CNTNT_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDescriptionContentId(I)V

    .line 192
    const-string v5, "MSSN_START_DT"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setStartDate(Ljava/util/Calendar;)V

    .line 194
    const-string v5, "MSSN_END_DT"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionCompletedDate(Ljava/util/Calendar;)V

    .line 197
    const-string v5, "MSSN_STG_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionStage(Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;)V

    .line 199
    const-string v5, "STAT_CD"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setCurrentStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V

    .line 201
    const-string v5, "MSSN_CMPLT_IND"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-lt v5, v6, :cond_6

    move v5, v6

    :goto_0
    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDisplayedToUser(Z)V

    .line 204
    const-string v5, "PRIM_CTGRY_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setPrimaryCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 206
    const-string v5, "PRIM_SUB_CTGRY_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setPrimarySubcategory(Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;)V

    .line 210
    const-string v5, "USR_MSSN_FREQ"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setSelectedFrequencyDays(I)V

    .line 212
    const-string v5, "MSSN_SPAN_DAY_NUM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setSpanInDays(I)V

    .line 214
    const-string v5, "SPCF_TRCKER_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;->getInstance(I)Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setSpecificTracker(Lcom/cigna/coach/apiobjects/GoalMissionInfo$SpecificTracker;)V

    .line 217
    const-string v5, "USR_ID_TXT"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setUserId(Ljava/lang/String;)V

    .line 219
    const-string v5, "TRCKER_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;->getInstance(I)Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setTrackerType(Lcom/cigna/coach/apiobjects/GoalMissionInfo$TrackerType;)V

    .line 221
    const-string v5, "TRCKER_CNTNT_ID"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setTrackerNameContentId(I)V

    .line 223
    const-string v5, "DISPL_ORDR_NUM"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setDisplayOrder(I)V

    .line 226
    const-string v5, "FAILED_REASN_CD"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 227
    .local v1, "missionFailedReasonCodeIndex":I
    invoke-interface {p1, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v5

    if-nez v5, :cond_2

    .line 228
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->getInstance(I)Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    move-result-object v0

    .line 230
    .local v0, "missionFailedReason":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    invoke-virtual {v4, v0}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionFailedReason(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)V

    .line 233
    .end local v0    # "missionFailedReason":Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    :cond_2
    if-ne p2, v6, :cond_4

    .line 234
    invoke-direct {p0, v2}, Lcom/cigna/coach/db/GoalMissoinDBManager;->getMissionTips(I)Ljava/util/List;

    move-result-object v3

    .line 235
    .local v3, "tips":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    if-eqz v3, :cond_3

    .line 236
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 237
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Num tips:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    :cond_3
    invoke-virtual {v4, v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->setTips(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    .end local v3    # "tips":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionTip;>;"
    :cond_4
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 244
    sget-object v5, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "retriveUserMissionDataFromCursor :End"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_5
    return-object v4

    .line 201
    .end local v1    # "missionFailedReasonCodeIndex":I
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 243
    .end local v2    # "missionId":I
    .end local v4    # "userGoalMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catchall_0
    move-exception v5

    sget-object v6, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 244
    sget-object v6, Lcom/cigna/coach/db/GoalMissoinDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v7, "retriveUserMissionDataFromCursor :End"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v5
.end method
