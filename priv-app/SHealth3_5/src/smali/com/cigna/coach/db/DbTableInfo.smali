.class public Lcom/cigna/coach/db/DbTableInfo;
.super Ljava/lang/Object;
.source "DbTableInfo.java"


# instance fields
.field otherFields:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field primaryKeys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field tableName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/db/DbTableInfo;->primaryKeys:Ljava/util/Set;

    .line 23
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/db/DbTableInfo;->otherFields:Ljava/util/Set;

    .line 27
    iput-object p1, p0, Lcom/cigna/coach/db/DbTableInfo;->tableName:Ljava/lang/String;

    .line 28
    return-void
.end method


# virtual methods
.method public addField(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "primaryKey"    # Z

    .prologue
    .line 35
    if-eqz p2, :cond_0

    .line 36
    iget-object v0, p0, Lcom/cigna/coach/db/DbTableInfo;->primaryKeys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/cigna/coach/db/DbTableInfo;->otherFields:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getAllFields()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    .line 48
    .local v0, "returnValues":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/cigna/coach/db/DbTableInfo;->primaryKeys:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 49
    iget-object v1, p0, Lcom/cigna/coach/db/DbTableInfo;->otherFields:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 50
    return-object v0
.end method

.method public getPrimaryKeys()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/cigna/coach/db/DbTableInfo;->primaryKeys:Ljava/util/Set;

    return-object v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/cigna/coach/db/DbTableInfo;->tableName:Ljava/lang/String;

    return-object v0
.end method
