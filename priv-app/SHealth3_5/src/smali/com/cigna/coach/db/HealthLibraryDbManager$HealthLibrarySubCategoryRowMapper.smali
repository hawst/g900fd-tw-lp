.class Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;
.super Ljava/lang/Object;
.source "HealthLibraryDbManager.java"

# interfaces
.implements Lcom/cigna/coach/db/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/HealthLibraryDbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "HealthLibrarySubCategoryRowMapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/cigna/coach/db/RowMapper",
        "<",
        "Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;",
        ">;"
    }
.end annotation


# instance fields
.field final catIdCol:I

.field final cntntIdCol:I

.field final imgCntntIdCol:I

.field final subCatIdCol:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "catIdCol"    # I
    .param p2, "subCatIdCol"    # I
    .param p3, "cntntIdCol"    # I
    .param p4, "imgCntntIdCol"    # I

    .prologue
    .line 764
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 765
    iput p1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->catIdCol:I

    .line 766
    iput p2, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->subCatIdCol:I

    .line 767
    iput p3, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->cntntIdCol:I

    .line 768
    iput p4, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->imgCntntIdCol:I

    .line 769
    return-void
.end method


# virtual methods
.method public mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 776
    new-instance v0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;-><init>()V

    .line 777
    .local v0, "row":Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;
    iget v1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->catIdCol:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->setCategoryId(I)V

    .line 778
    iget v1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->subCatIdCol:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->setSubCategoryId(I)V

    .line 779
    iget v1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->cntntIdCol:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->setCntntId(I)V

    .line 780
    iget v1, p0, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->imgCntntIdCol:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->setImgCntntId(I)V

    .line 781
    return-object v0
.end method

.method public bridge synthetic mapRow(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 749
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/HealthLibraryDbManager$HealthLibrarySubCategoryRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;

    move-result-object v0

    return-object v0
.end method
