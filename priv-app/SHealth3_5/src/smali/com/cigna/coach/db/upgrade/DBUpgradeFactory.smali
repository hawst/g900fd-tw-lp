.class public Lcom/cigna/coach/db/upgrade/DBUpgradeFactory;
.super Ljava/lang/Object;
.source "DBUpgradeFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getUpgradeAction(I)Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    .locals 2
    .param p0, "newVersion"    # I

    .prologue
    .line 6
    const/4 v0, 0x0

    .line 8
    .local v0, "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    const/4 v1, 0x2

    if-ne p0, v1, :cond_1

    .line 9
    new-instance v0, Lcom/cigna/coach/db/upgrade/OneToTwo;

    .end local v0    # "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    invoke-direct {v0}, Lcom/cigna/coach/db/upgrade/OneToTwo;-><init>()V

    .line 17
    .restart local v0    # "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    :cond_0
    :goto_0
    return-object v0

    .line 10
    :cond_1
    const/4 v1, 0x3

    if-ne p0, v1, :cond_2

    .line 11
    new-instance v0, Lcom/cigna/coach/db/upgrade/TwoToThree;

    .end local v0    # "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    invoke-direct {v0}, Lcom/cigna/coach/db/upgrade/TwoToThree;-><init>()V

    .restart local v0    # "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    goto :goto_0

    .line 12
    :cond_2
    const/4 v1, 0x4

    if-ne p0, v1, :cond_0

    .line 13
    new-instance v0, Lcom/cigna/coach/db/upgrade/ThreeToFour;

    .end local v0    # "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    invoke-direct {v0}, Lcom/cigna/coach/db/upgrade/ThreeToFour;-><init>()V

    .restart local v0    # "idbUpgrade":Lcom/cigna/coach/db/upgrade/IDBUpgrade;
    goto :goto_0
.end method
