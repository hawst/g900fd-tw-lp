.class Lcom/cigna/coach/db/upgrade/ThreeToFour$CoachMessageChanges;
.super Ljava/lang/Object;
.source "ThreeToFour.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/upgrade/ThreeToFour;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CoachMessageChanges"
.end annotation


# static fields
.field private static final COACH_MSG_INSERT:Ljava/lang/String; = "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES ("

.field private static final COLUMNS:Ljava/lang/String; = "COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 359
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addInsertQueriesList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 364
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 366
    .local v0, "insertQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "UPDATE COACH_MSG SET COACH_MSG_ID = 117, CNTXT_ID = 2, MSG_TY_CD = 7, CNTXT_FILTER_CD = 19, REF_TY_IND = -1, REF_ID = -1, MIN_MAX_REF_CD = 11, MIN_VAL = 0, MAX_VAL = 599, RPT_INTVL = -1, DISPL_RANK_NUM = 117, CNTNT_ID = 3657, DESC_CNTNT_ID = 3800, IMG_CNTNT_ID = 4601, INTENT_ACTION = \'\', INTENT_EXTRA = -1, IS_ENBLD = 1 WHERE COACH_MSG_ID = 117"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 370
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (183,1,9,49,-1,-1,6,7,21,7,183,6122,6124,4601,\'com.sec.android.app.shealth.cignacoach.DESTINATION_MAIN\',-1,0)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 371
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (184,1,12,1,-1,-1,-1,-1,-1,-1,184,6610,6610,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (185,1,12,7,-1,-1,6,2,2,-1,185,6605,6605,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (186,1,12,7,-1,-1,6,3,3,-1,186,6606,6606,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (187,1,12,7,-1,-1,6,5,5,-1,187,6607,6607,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 375
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (188,1,12,7,-1,-1,6,6,6,-1,188,6608,6608,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (189,1,12,9,-1,-1,6,7,7,-1,189,6609,6609,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (190,6,12,10,-1,-1,-1,-1,-1,-1,190,6604,6604,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (191,6,12,35,-1,-1,-1,-1,-1,-1,191,6603,6603,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (192,1,12,15,-1,-1,3,0,-1,-1,192,6602,6602,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 380
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (193,1,12,14,-1,-1,2,0,-1,-1,193,6601,6601,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_SUGGEST_GOAL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (194,1,12,-1,-1,-1,-1,-1,-1,-1,194,6600,6600,-1,\'\',-1,0)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (195,1,13,1,-1,-1,-1,-1,-1,-1,184,6610,6610,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 383
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (196,1,13,7,-1,-1,6,2,2,-1,185,6605,6605,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 384
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (197,1,13,7,-1,-1,6,3,3,-1,186,6606,6606,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (198,1,13,7,-1,-1,6,5,5,-1,187,6607,6607,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (199,1,13,7,-1,-1,6,6,6,-1,188,6608,6608,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (200,1,13,9,-1,-1,6,7,7,-1,189,6609,6609,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_CURRENT_MISSION_DETAIL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 388
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (201,6,13,10,-1,-1,-1,-1,-1,-1,190,6604,6604,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (202,6,13,35,-1,-1,-1,-1,-1,-1,191,6603,6603,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (203,1,13,15,-1,-1,3,0,-1,-1,192,6602,6602,-1,\'\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 391
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (204,1,13,14,-1,-1,2,0,-1,-1,193,6601,6601,-1,\'com.sec.android.app.shealth.cignacoach.DESTINATION_SUGGEST_GOAL\',-1,1)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 392
    const-string v1, "INSERT OR REPLACE INTO COACH_MSG (COACH_MSG_ID,CNTXT_ID,MSG_TY_CD,CNTXT_FILTER_CD,REF_TY_IND,REF_ID,MIN_MAX_REF_CD,MIN_VAL,MAX_VAL,RPT_INTVL,DISPL_RANK_NUM,CNTNT_ID,DESC_CNTNT_ID,IMG_CNTNT_ID,INTENT_ACTION,INTENT_EXTRA,IS_ENBLD) VALUES (205,1,13,-1,-1,-1,-1,-1,-1,-1,194,6600,6600,-1,\'\',-1,0)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 394
    return-object v0
.end method
