.class Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;
.super Ljava/lang/Object;
.source "CoachMessageDBManager.java"

# interfaces
.implements Lcom/cigna/coach/db/RowMapper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/CoachMessageDBManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CoachMessageDataRowMapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/cigna/coach/db/RowMapper",
        "<",
        "Lcom/cigna/coach/dataobjects/CoachMessageData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/CoachMessageData;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 672
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;-><init>()V

    .line 673
    .local v0, "cmd":Lcom/cigna/coach/dataobjects/CoachMessageData;
    const-string v1, "COACH_MSG_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setCoachMessageId(I)V

    .line 674
    const-string v1, "MSG_TY_CD"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;->getInstance(I)Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 675
    const-string v1, "CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 676
    const-string v1, "DESC_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageDescriptionContentId(I)V

    .line 677
    const-string v1, "DISPL_RANK_NUM"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageDisplayRank(I)V

    .line 678
    const-string v1, "IMG_CNTNT_ID"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageImageContentId(I)V

    .line 679
    const-string v1, "RPT_INTVL"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setRepeatInterval(I)V

    .line 680
    const-string v1, "MIN_MAX_REF_CD"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMinMaxRefValue(I)V

    .line 681
    const-string v1, "MIN_VAL"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMinValue(I)V

    .line 682
    const-string v1, "MAX_VAL"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMaxValue(I)V

    .line 683
    const-string v1, "CNTXT_FILTER_CD"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getInstance(I)Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setContextFilterType(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V

    .line 684
    const-string v1, "INTENT_ACTION"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setIntentAction(Ljava/lang/String;)V

    .line 685
    const-string v1, "INTENT_EXTRA"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->getInstance(I)Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setIntentExtraType(Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;)V

    .line 686
    return-object v0
.end method

.method public bridge synthetic mapRow(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/database/Cursor;

    .prologue
    .line 668
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/CoachMessageDBManager$CoachMessageDataRowMapper;->mapRow(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/CoachMessageData;

    move-result-object v0

    return-object v0
.end method
