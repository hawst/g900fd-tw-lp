.class public Lcom/cigna/coach/db/MissionDBManager;
.super Lcom/cigna/coach/db/GoalMissoinDBManager;
.source "MissionDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/cigna/coach/db/MissionDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/GoalMissoinDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 65
    return-void
.end method

.method private sortAvailableMissionsList(IILjava/util/List;)Ljava/util/List;
    .locals 20
    .param p1, "userIdKey"    # I
    .param p2, "goalId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 309
    .local p3, "goalMissionInfos":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    sget-object v2, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310
    sget-object v2, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "sortAvailableMissionsList: START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    const/4 v13, 0x0

    .line 313
    .local v13, "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v12, 0x0

    .line 315
    .local v12, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const/4 v3, 0x1

    const-string v4, "USR_MSSN"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "MSSN_ID"

    aput-object v7, v5, v6

    const-string v6, "STAT_CD = ? AND USR_ID = ? AND GOAL_ID = ? "

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->DO_NOT_LIKE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v2 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 327
    if-eqz v12, :cond_3

    .line 328
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 329
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    .end local v13    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v14, "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 331
    const-string v2, "MSSN_ID"

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 335
    :catch_0
    move-exception v15

    move-object v13, v14

    .line 336
    .end local v14    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v13    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v15, "e":Ljava/lang/RuntimeException;
    :goto_1
    :try_start_2
    sget-object v2, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error in sortAvailableMissionsList: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v15}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 337
    throw v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 339
    .end local v15    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    :goto_2
    if-eqz v12, :cond_1

    .line 340
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .end local v13    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v14    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_2
    move-object v13, v14

    .line 339
    .end local v14    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v13    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_3
    if-eqz v12, :cond_4

    .line 340
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 342
    :cond_4
    if-eqz v13, :cond_7

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_7

    if-eqz p3, :cond_7

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 345
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 346
    .local v18, "missionId":I
    const/16 v19, 0x0

    .local v19, "position":I
    :goto_4
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v19

    if-ge v0, v2, :cond_5

    .line 347
    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/cigna/coach/apiobjects/GoalMissionInfo;

    .line 348
    .local v16, "goalMissionInfo":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    if-eqz v16, :cond_6

    .line 349
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/GoalMissionInfo;->getMissionId()I

    move-result v2

    move/from16 v0, v18

    if-ne v2, v0, :cond_6

    .line 350
    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 351
    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 346
    :cond_6
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 358
    .end local v16    # "goalMissionInfo":Lcom/cigna/coach/apiobjects/GoalMissionInfo;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v18    # "missionId":I
    .end local v19    # "position":I
    :cond_7
    sget-object v2, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 359
    sget-object v2, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v3, "sortAvailableMissionsList: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    :cond_8
    return-object p3

    .line 339
    .end local v13    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v14    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catchall_1
    move-exception v2

    move-object v13, v14

    .end local v14    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v13    # "doNotLikeMissionIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_2

    .line 335
    :catch_1
    move-exception v15

    goto/16 :goto_1
.end method


# virtual methods
.method public createUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)V
    .locals 13
    .param p1, "username"    # Ljava/lang/String;
    .param p2, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    const-wide/16 v8, 0x0

    .line 173
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 174
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "createUserMission: START"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_0
    const/4 v3, 0x0

    .line 177
    .local v3, "numRowsInserted":I
    const-wide/16 v4, -0x1

    .line 179
    .local v4, "rowId":J
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    invoke-static {v10, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    .line 183
    .local v6, "userKey":I
    new-instance v1, Lcom/cigna/coach/db/GoalDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v10

    invoke-direct {v1, v10}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 184
    .local v1, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v10

    invoke-virtual {v1, p1, v10}, Lcom/cigna/coach/db/GoalDBManager;->getActiveUserGoalSequenceId(Ljava/lang/String;I)I

    move-result v2

    .line 186
    .local v2, "goalSequenceId":I
    const/4 v3, 0x0

    .line 187
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 189
    .local v7, "values":Landroid/content/ContentValues;
    const-string v10, "GOAL_ID"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 190
    const-string v10, "GOAL_SEQ_ID"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 191
    const-string v10, "MSSN_CMPLT_IND"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->isDisplayedToUser()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 192
    const-string v12, "MSSN_END_DT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    :goto_0
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7, v12, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 194
    const-string v10, "MSSN_ID"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 195
    const-string v10, "USR_MSSN_FREQ"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 196
    const-string v10, "MSSN_START_DT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    :cond_1
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 198
    const-string v8, "STAT_CD"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v9

    invoke-virtual {v9}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 199
    const-string v8, "USR_ID"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 201
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "USR_MSSN"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 202
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    add-int/lit8 v3, v3, 0x1

    .line 209
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 210
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "User Mission with ID "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Created @ rowId = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v10, "with "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " new row(s)"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "createUserMission: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    :cond_2
    return-void

    :cond_3
    move-wide v10, v8

    .line 192
    goto/16 :goto_0

    .line 205
    .end local v1    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v2    # "goalSequenceId":I
    .end local v6    # "userKey":I
    .end local v7    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "createUserMission: "

    invoke-static {v8, v9, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 207
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 210
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "User Mission with ID "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Created @ rowId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "with "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " new row(s)"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "createUserMission: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v8
.end method

.method public createUserMissionData(Ljava/lang/String;IILjava/util/List;)I
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I
    .param p3, "missionSequenceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/MissionDetails;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 114
    .local p4, "userMissionTrackData":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 115
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "createUserMissionData: START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_0
    if-nez p4, :cond_2

    .line 120
    const/4 v3, 0x0

    .line 158
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 159
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "createUserMissionData: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_1
    :goto_0
    return v3

    .line 122
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 123
    .local v6, "userKey":I
    const/4 v9, -0x1

    if-ne v6, v9, :cond_3

    .line 124
    const/4 v3, 0x0

    .line 158
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 159
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "createUserMissionData: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_3
    const/4 v3, 0x0

    .line 128
    .local v3, "numRowsInserted":I
    if-eqz p4, :cond_6

    :try_start_1
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_6

    .line 129
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/cigna/coach/apiobjects/MissionDetails;

    .line 130
    .local v7, "userMissionData":Lcom/cigna/coach/apiobjects/MissionDetails;
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 132
    .local v8, "values":Landroid/content/ContentValues;
    const-string v9, "MSSN_ID"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 133
    const-string v9, "MSSN_ACCOMP_DT"

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 135
    const-string v9, "MSSN_ACTY_CNT"

    invoke-virtual {v7}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 136
    const-string v9, "USR_ID"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    const-string v9, "MSSN_SEQ_ID"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 139
    sget-object v0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->MANUAL:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    .line 140
    .local v0, "activitySource":Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    instance-of v9, v7, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    if-eqz v9, :cond_4

    .line 141
    check-cast v7, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    .end local v7    # "userMissionData":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-virtual {v7}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getActivitySource()Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    move-result-object v0

    .line 143
    :cond_4
    const-string v9, "SRC_REF"

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->getSourceType()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 145
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const-string v10, "USR_MSSN_DATA"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 146
    .local v4, "rowId":J
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 147
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "User Mission Data Created @ rowId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    :cond_5
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v9

    invoke-virtual {v9}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    add-int/lit8 v3, v3, 0x1

    .line 152
    goto/16 :goto_1

    .line 158
    .end local v0    # "activitySource":Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "rowId":J
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_6
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 159
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "createUserMissionData: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 154
    .end local v3    # "numRowsInserted":I
    .end local v6    # "userKey":I
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Error in createUserMissionData: "

    invoke-static {v9, v10, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 156
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 158
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v9

    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 159
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "createUserMissionData: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v9
.end method

.method public deleteUserMissionData(Ljava/lang/String;II)I
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I
    .param p3, "missionSequenceId"    # I

    .prologue
    .line 218
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 219
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "deleteUserMissionData: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :cond_0
    const/4 v1, 0x0

    .line 223
    .local v1, "noOfRowsDeleted":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 224
    .local v2, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "USR_MSSN_DATA"

    const-string v5, "USR_ID = ? AND MSSN_ID = ? AND MSSN_SEQ_ID = ?  "

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 233
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 234
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No of Rows Deleted = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "deleteUserMissionData: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :cond_1
    return v1

    .line 229
    .end local v2    # "userKey":I
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in deleteUserMissionData: "

    invoke-static {v3, v4, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 231
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 234
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No of Rows Deleted = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "deleteUserMissionData: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v3
.end method

.method public getActiveUserMissionDetails(ILjava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;
    .locals 11
    .param p1, "missionId"    # I
    .param p2, "userId"    # Ljava/lang/String;

    .prologue
    .line 417
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 418
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getActiveUserMissionDetails: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_0
    const/4 v4, 0x0

    .line 421
    .local v4, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    const/4 v2, 0x0

    .line 423
    .local v2, "subQuery":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 424
    .local v3, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.STAT_CD = ? AND A.MSSN_ID = ?"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 426
    if-eqz v2, :cond_3

    .line 428
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 429
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT_ALL_USER_MISSIONS_QUERY records = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 435
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_3

    .line 436
    invoke-virtual {p0, v2}, Lcom/cigna/coach/db/MissionDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v4

    .line 438
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 439
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Mission ID"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    :cond_2
    invoke-virtual {p0, v4}, Lcom/cigna/coach/db/MissionDBManager;->getUserMissionDetailsList(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v1

    .line 444
    .local v1, "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    invoke-virtual {v4, v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionDetails(Ljava/util/List;)V

    .line 445
    invoke-virtual {p0, v1}, Lcom/cigna/coach/db/MissionDBManager;->getMissionActivityCompletedCount(Ljava/util/List;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionActivityCompletedTotalCount(I)V

    .line 447
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 454
    .end local v1    # "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :cond_3
    if-eqz v2, :cond_4

    .line 455
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 456
    :cond_4
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 457
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getActiveUserMissionDetails: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    :cond_5
    return-object v4

    .line 450
    .end local v3    # "userKey":I
    :catch_0
    move-exception v0

    .line 451
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Error in getActiveUserMissionDetails: "

    invoke-static {v5, v6, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 452
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    if-eqz v2, :cond_6

    .line 455
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 456
    :cond_6
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 457
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getActiveUserMissionDetails: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v5
.end method

.method public getActiveUserMissionsWithTrackerRules(Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 885
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 886
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getActiveUserMissionsWithTrackerRules: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 889
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 891
    .local v4, "userMissionDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v2

    .line 892
    .local v2, "userKey":I
    const/4 v0, 0x0

    .line 894
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.STAT_CD = ? AND B.TRCKER_RULE_ID != -1"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v10}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 897
    if-eqz v0, :cond_5

    .line 899
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 900
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT_ALL_USER_MISSIONS_QUERY records = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 904
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_5

    .line 905
    invoke-virtual {p0, v0}, Lcom/cigna/coach/db/MissionDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v3

    .line 906
    .local v3, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 907
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 911
    .end local v3    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v1

    .line 912
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Error in getActiveUserMissionsWithTrackerRules: "

    invoke-static {v5, v6, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 913
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 915
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_2

    .line 916
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v5
    :try_end_3
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 918
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "userKey":I
    :catch_1
    move-exception v1

    .line 919
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    :try_start_4
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 920
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getActiveUserMissionsWithTrackerRules: no userID found for :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 923
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 924
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getActiveUserMissionsWithTrackerRules: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    .end local v1    # "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    :cond_4
    :goto_1
    return-object v4

    .line 915
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "userKey":I
    :cond_5
    if-eqz v0, :cond_6

    .line 916
    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 923
    :cond_6
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 924
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getActiveUserMissionsWithTrackerRules: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 923
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "userKey":I
    :catchall_1
    move-exception v5

    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 924
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getActiveUserMissionsWithTrackerRules: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    throw v5
.end method

.method public getAllActiveMissionsInDisplayOrder(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 985
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 986
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAllActiveMissionsInDisplayOrder: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 991
    .local v6, "userMissionDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    .line 992
    .local v4, "userKey":I
    const/4 v0, 0x0

    .line 994
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    const-string v8, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.STAT_CD = ? ORDER BY E.DISPL_ORDR_NUM"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v12}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ""

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 997
    if-eqz v0, :cond_4

    .line 999
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1000
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SELECT_ALL_USER_MISSIONS_QUERY records = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1004
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1005
    invoke-virtual {p0, v0}, Lcom/cigna/coach/db/MissionDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v5

    .line 1006
    .local v5, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1011
    .end local v5    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v1

    .line 1012
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error in getAllActiveMissionsInDisplayOrder: "

    invoke-static {v7, v8, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1013
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1015
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_2

    .line 1016
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v7
    :try_end_3
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1018
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v4    # "userKey":I
    :catch_1
    move-exception v1

    .line 1019
    .local v1, "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    :try_start_4
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1020
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getAllActiveMissionsInDisplayOrder: no userID found for :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1023
    :cond_3
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1024
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getAllActiveMissionsInDisplayOrder: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " missions found:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 1026
    .local v3, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mission: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; goal: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; display order: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getDisplayOrder()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1015
    .end local v1    # "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v4    # "userKey":I
    :cond_4
    if-eqz v0, :cond_5

    .line 1016
    :try_start_5
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_5
    .catch Lcom/cigna/coach/exceptions/CoachUserNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1023
    :cond_5
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1024
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getAllActiveMissionsInDisplayOrder: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " missions found:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 1026
    .restart local v3    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "    mission: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; goal: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "; display order: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getDisplayOrder()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1029
    .end local v3    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_6
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAllActiveMissionsInDisplayOrder: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "userKey":I
    :cond_7
    :goto_3
    return-object v6

    .line 1029
    .restart local v1    # "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_8
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getAllActiveMissionsInDisplayOrder: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1023
    .end local v1    # "e":Lcom/cigna/coach/exceptions/CoachUserNotFoundException;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_1
    move-exception v7

    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 1024
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getAllActiveMissionsInDisplayOrder: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " missions found:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1025
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 1026
    .restart local v3    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "    mission: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; goal: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; display order: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->getDisplayOrder()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 1029
    .end local v3    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_9
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getAllActiveMissionsInDisplayOrder: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_a
    throw v7
.end method

.method public getAllActiveMissionsInExpirationOrder(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1036
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1037
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getAllActiveMissionsInExpirationOrder: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    :cond_0
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/MissionDBManager;->getAllActiveMissionsInDisplayOrder(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 1040
    .local v2, "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    new-instance v3, Lcom/cigna/coach/db/MissionDBManager$1;

    invoke-direct {v3, p0}, Lcom/cigna/coach/db/MissionDBManager$1;-><init>(Lcom/cigna/coach/db/MissionDBManager;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1046
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1047
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAllActiveMissionsInExpirationOrder: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " missions found:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    .line 1049
    .local v1, "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "    mission: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; goal: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; expiration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getExpirationDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-static {v5}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "; display order: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/cigna/coach/dataobjects/UserMissionData;->getDisplayOrder()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1053
    .end local v1    # "mission":Lcom/cigna/coach/dataobjects/UserMissionData;
    :cond_1
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getAllActiveMissionsInExpirationOrder: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1055
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    return-object v2
.end method

.method public getAllMissionsWithMissionDataNotReportedWithinTimeSpanFreqencyGracePeriod(Ljava/lang/String;I)Ljava/util/List;
    .locals 16
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "afterNoOfDays"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 930
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 931
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getAllMissionsWithMissionDataNotReportedWithinTimeSpanFreqencyGracePeriod: START"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 933
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 935
    .local v9, "userMissionDatas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 937
    .local v7, "userKey":I
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 938
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "hasSpecificDaysPassedSinceLastMissionCompleteForAllActiveGoals: SQL: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Lcom/cigna/coach/db/CoachMessageDBQueries;->GET_ACTIVE_USER_MISSIONS_WITH_NO_MISSION_DATA_ENTERED:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    sget-object v11, Lcom/cigna/coach/db/CoachMessageDBQueries;->GET_ACTIVE_USER_MISSIONS_WITH_NO_MISSION_DATA_ENTERED:Ljava/lang/String;

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 943
    .local v1, "c":Landroid/database/Cursor;
    if-eqz v1, :cond_5

    .line 944
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 946
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v10

    if-nez v10, :cond_5

    .line 948
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/cigna/coach/db/MissionDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v8

    .line 952
    .local v8, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    invoke-virtual {v8}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v10

    invoke-static {v10}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 955
    .local v3, "missionStartDate":J
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfToday()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    .line 962
    .local v5, "today":J
    const-wide/32 v10, 0x5265c00

    move/from16 v0, p2

    int-to-long v12, v0

    mul-long/2addr v10, v12

    add-long/2addr v10, v3

    cmp-long v10, v5, v10

    if-nez v10, :cond_2

    .line 963
    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 969
    .end local v3    # "missionStartDate":J
    .end local v5    # "today":J
    .end local v8    # "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v2

    .line 970
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error getting missions with activity(no goal id): "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 971
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 973
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v10

    if-eqz v1, :cond_3

    .line 974
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 976
    :cond_3
    sget-object v11, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 977
    sget-object v11, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getAllMissionsWithMissionDataNotReportedWithinTimeSpanFreqencyGracePeriod: END"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v10

    .line 973
    :cond_5
    if-eqz v1, :cond_6

    .line 974
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 976
    :cond_6
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 977
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getAllMissionsWithMissionDataNotReportedWithinTimeSpanFreqencyGracePeriod: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    :cond_7
    return-object v9
.end method

.method public getAvailableUserMissionsForGoal(Ljava/lang/String;I)Ljava/util/List;
    .locals 17
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/GoalMissionInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 244
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getAvailableUserMissionsForGoal: START"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Getting all Availalbe User Missions for goal:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v7, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    new-instance v4, Lcom/cigna/coach/db/GoalDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/MissionDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v10

    invoke-direct {v4, v10}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 250
    .local v4, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    const/4 v9, 0x0

    .line 251
    .local v9, "userKey":I
    const/4 v2, 0x0

    .line 253
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v9

    .line 255
    move/from16 v0, p2

    invoke-virtual {v4, v0, v9}, Lcom/cigna/coach/db/GoalDBManager;->getLastAssesmentTimeForGoalCategory(II)Ljava/util/Calendar;

    move-result-object v6

    .line 257
    .local v6, "lastAssesmentForGoal":Ljava/util/Calendar;
    if-eqz v6, :cond_4

    .line 258
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 259
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Last assesment timestamp for goal id:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " is:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v12

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_1
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v10

    const-string v11, "SELECT  A.DEFLT_FREQ_IND, A.FREQ_START_RNG_NUM, A.FREQ_END_RNG_NUM, B.MSSN_ID, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_FREQ_CNTNT_ID, B.MSSN_STG_ID, B.DLY_TRGT_NUM, B.DLY_MAX_NUM, B.UNT_NUM, B.DEPNDNT_MSSN_ID, B.MSSN_SPAN_DAY_NUM, B.TRCKER_RULE_ID, B.SPCF_TRCKER_ID, B.ACTV_MSSN_IND, B.PRIM_CTGRY_ID,B.PRIM_SUB_CTGRY_ID,A.DISPL_ORDR_NUM FROM  MSSN_GOAL A INNER JOIN MSSN B ON B.MSSN_ID = A.MSSN_ID WHERE A.GOAL_ID = ?  AND B.MSSN_ID NOT IN (SELECT DISTINCT(C.MSSN_ID) FROM USR_MSSN C INNER JOIN MSSN E ON E.MSSN_ID = C.MSSN_ID WHERE (C.STAT_CD = ? OR \t\t( (C.STAT_CD = ? OR (C.STAT_CD = ? AND E.MSSN_STG_ID != ?)) AND C.MSSN_END_DT > ?)  AND C.USR_ID = ?) ) AND ((B.DEPNDNT_MSSN_ID IS NULL OR B.DEPNDNT_MSSN_ID = -1) OR ((SELECT COUNT(D.MSSN_ID) FROM USR_MSSN D WHERE D.MSSN_ID = B.DEPNDNT_MSSN_ID AND D.STAT_CD = ? AND D.USR_ID = ?) > 0) ) ORDER BY B.MSSN_STG_ID, A.DISPL_ORDR_NUM "

    const/16 v12, 0x9

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v15}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v15}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v15}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    invoke-virtual {v15}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getIntValue()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x5

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x6

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x7

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v15, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v15}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    const/16 v13, 0x8

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ""

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 277
    if-nez v2, :cond_5

    .line 296
    if-eqz v2, :cond_2

    .line 297
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 298
    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v9, v1, v7}, Lcom/cigna/coach/db/MissionDBManager;->sortAvailableMissionsList(IILjava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 299
    .end local v7    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .local v8, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 300
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Found "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " available User Missions"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getAvailableUserMissionsForGoal: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object v10, v7

    move-object v7, v8

    .line 304
    .end local v6    # "lastAssesmentForGoal":Ljava/util/Calendar;
    .end local v8    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    .restart local v7    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/GoalMissionInfo;>;"
    :goto_1
    return-object v10

    .line 262
    .restart local v6    # "lastAssesmentForGoal":Ljava/util/Calendar;
    :cond_4
    :try_start_1
    new-instance v6, Ljava/util/GregorianCalendar;

    .end local v6    # "lastAssesmentForGoal":Ljava/util/Calendar;
    invoke-direct {v6}, Ljava/util/GregorianCalendar;-><init>()V

    .restart local v6    # "lastAssesmentForGoal":Ljava/util/Calendar;
    goto/16 :goto_0

    .line 279
    :cond_5
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 280
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Actual Query = SELECT  A.DEFLT_FREQ_IND, A.FREQ_START_RNG_NUM, A.FREQ_END_RNG_NUM, B.MSSN_ID, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_FREQ_CNTNT_ID, B.MSSN_STG_ID, B.DLY_TRGT_NUM, B.DLY_MAX_NUM, B.UNT_NUM, B.DEPNDNT_MSSN_ID, B.MSSN_SPAN_DAY_NUM, B.TRCKER_RULE_ID, B.SPCF_TRCKER_ID, B.ACTV_MSSN_IND, B.PRIM_CTGRY_ID,B.PRIM_SUB_CTGRY_ID,A.DISPL_ORDR_NUM FROM  MSSN_GOAL A INNER JOIN MSSN B ON B.MSSN_ID = A.MSSN_ID WHERE A.GOAL_ID = ?  AND B.MSSN_ID NOT IN (SELECT DISTINCT(C.MSSN_ID) FROM USR_MSSN C INNER JOIN MSSN E ON E.MSSN_ID = C.MSSN_ID WHERE (C.STAT_CD = ? OR \t\t( (C.STAT_CD = ? OR (C.STAT_CD = ? AND E.MSSN_STG_ID != ?)) AND C.MSSN_END_DT > ?)  AND C.USR_ID = ?) ) AND ((B.DEPNDNT_MSSN_ID IS NULL OR B.DEPNDNT_MSSN_ID = -1) OR ((SELECT COUNT(D.MSSN_ID) FROM USR_MSSN D WHERE D.MSSN_ID = B.DEPNDNT_MSSN_ID AND D.STAT_CD = ? AND D.USR_ID = ?) > 0) ) ORDER BY B.MSSN_STG_ID, A.DISPL_ORDR_NUM "

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 284
    :cond_7
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v10

    if-nez v10, :cond_a

    .line 285
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v2, v1}, Lcom/cigna/coach/db/MissionDBManager;->retriveGoalMissionDataFromCursor(Landroid/database/Cursor;I)Lcom/cigna/coach/dataobjects/GoalMissionData;

    move-result-object v5

    .line 286
    .local v5, "goalMissionData":Lcom/cigna/coach/dataobjects/GoalMissionData;
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 289
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 290
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Mission ID "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/GoalMissionData;->getMissionId()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 293
    .end local v5    # "goalMissionData":Lcom/cigna/coach/dataobjects/GoalMissionData;
    .end local v6    # "lastAssesmentForGoal":Ljava/util/Calendar;
    :catch_0
    move-exception v3

    .line 294
    .local v3, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Error in getAvailableUserMissionsForGoal: "

    invoke-static {v10, v11, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 296
    if-eqz v2, :cond_8

    .line 297
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 298
    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v9, v1, v7}, Lcom/cigna/coach/db/MissionDBManager;->sortAvailableMissionsList(IILjava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 299
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 300
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Found "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " available User Missions"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getAvailableUserMissionsForGoal: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v3    # "e":Ljava/lang/RuntimeException;
    :cond_9
    :goto_3
    move-object v10, v7

    .line 304
    goto/16 :goto_1

    .line 296
    .restart local v6    # "lastAssesmentForGoal":Ljava/util/Calendar;
    :cond_a
    if-eqz v2, :cond_b

    .line 297
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 298
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v9, v1, v7}, Lcom/cigna/coach/db/MissionDBManager;->sortAvailableMissionsList(IILjava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 299
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 300
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Found "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " available User Missions"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    sget-object v10, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getAvailableUserMissionsForGoal: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 296
    .end local v6    # "lastAssesmentForGoal":Ljava/util/Calendar;
    :catchall_0
    move-exception v10

    if-eqz v2, :cond_c

    .line 297
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 298
    :cond_c
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v9, v1, v7}, Lcom/cigna/coach/db/MissionDBManager;->sortAvailableMissionsList(IILjava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 299
    sget-object v11, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 300
    sget-object v11, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Found "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " available User Missions"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    sget-object v11, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getAvailableUserMissionsForGoal: END"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_d
    throw v10
.end method

.method public getClosetEndingDateMission(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/UserMissionData;
    .locals 2
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 1060
    invoke-virtual {p0, p1}, Lcom/cigna/coach/db/MissionDBManager;->getAllActiveMissionsInExpirationOrder(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1061
    .local v0, "missions":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 1062
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/dataobjects/UserMissionData;

    goto :goto_0
.end method

.method public getCompletedMissionCount(Ljava/lang/String;)I
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 718
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 719
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedMissionCount: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_0
    const/4 v0, 0x0

    .line 722
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 723
    .local v3, "userKey":I
    const/4 v1, 0x0

    .line 725
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_COUNT:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 727
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 728
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 733
    if-eqz v1, :cond_1

    .line 734
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 735
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 736
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedMissionCount: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    :cond_2
    return v0

    .line 729
    :catch_0
    move-exception v2

    .line 730
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getCompletedMissionCount: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 731
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 733
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 734
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 735
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 736
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCompletedMissionCount: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getCompletedMissionCountByStage(Ljava/lang/String;Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;)I
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionStage"    # Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .prologue
    .line 769
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 770
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedMissionCountByStage: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 773
    .local v3, "userKey":I
    const/4 v0, 0x0

    .line 774
    .local v0, "count":I
    const/4 v1, 0x0

    .line 776
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_COUNT_BY_STAGE:Ljava/lang/String;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getIntValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 779
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 780
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 785
    if-eqz v1, :cond_1

    .line 786
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 787
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 788
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCompletedMissionCountByStage: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    :cond_2
    return v0

    .line 781
    :catch_0
    move-exception v2

    .line 782
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getCompletedMissionCountByStage: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 783
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 785
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 786
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 787
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 788
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getCompletedMissionCountByStage: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getCompletedMissionCountPerStage(Ljava/lang/String;)Ljava/util/Map;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 854
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 855
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getCompletedMissionCountPerStage: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 858
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 859
    .local v7, "userKey":I
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 860
    .local v5, "result":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->values()[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v6, v0, v3

    .line 861
    .local v6, "stage":Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v6, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 860
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 864
    .end local v6    # "stage":Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    :cond_1
    const/4 v1, 0x0

    .line 866
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/db/MissionDBQueries;->GET_COMPLETED_MISSION_COUNT_PER_STAGE:Ljava/lang/String;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 867
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_4

    .line 868
    const/4 v8, 0x0

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v5, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 867
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 870
    :catch_0
    move-exception v2

    .line 871
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in getCompletedMissionCountPerStage: "

    invoke-static {v8, v9, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 873
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 875
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v1, :cond_2

    .line 876
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 877
    :cond_2
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 878
    sget-object v9, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getCompletedMissionCountPerStage: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v8

    .line 875
    :cond_4
    if-eqz v1, :cond_5

    .line 876
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 877
    :cond_5
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 878
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getCompletedMissionCountPerStage: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    :cond_6
    return-object v5
.end method

.method public getFrequenciesOfCompletedMissions(Ljava/lang/String;)Ljava/util/Map;
    .locals 10
    .param p1, "userId"    # Ljava/lang/String;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 796
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 797
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFrequenciesOfCompletedMissions: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 801
    .local v2, "frequencies":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    const/4 v5, 0x7

    if-gt v3, v5, :cond_1

    .line 802
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 804
    :cond_1
    const/4 v0, 0x0

    .line 806
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    .line 807
    .local v4, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    sget-object v6, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_FREQUENCIES:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 809
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_5

    .line 810
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 811
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Found %d missions at %d/wk."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    :cond_2
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 815
    .end local v4    # "userKey":I
    :catch_0
    move-exception v1

    .line 816
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Error in getFrequenciesOfCompletedMissions: "

    invoke-static {v5, v6, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 817
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 819
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_3

    .line 820
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 821
    :cond_3
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 822
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getFrequenciesOfCompletedMissions: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v5

    .line 819
    .restart local v4    # "userKey":I
    :cond_5
    if-eqz v0, :cond_6

    .line 820
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 821
    :cond_6
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 822
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getFrequenciesOfCompletedMissions: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    :cond_7
    return-object v2
.end method

.method public getIsUserMissionActiveForAnyActiveGoal(Ljava/lang/String;I)Z
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I

    .prologue
    const/4 v9, 0x0

    .line 68
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 69
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getIsUserMissionActiveForAnyActiveGoal START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    const/4 v3, 0x0

    .line 72
    .local v3, "returnVal":Z
    iget-object v7, p0, Lcom/cigna/coach/db/MissionDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v7}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-static {v7, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    .line 73
    .local v6, "userIdKey":I
    const/4 v1, -0x1

    .line 75
    .local v1, "count":I
    const-string v5, " SELECT  \tCOUNT(*)  FROM  \tUSR_MSSN M  \tJOIN USR_GOAL G ON (M.GOAL_ID = G.GOAL_ID AND M.USR_ID = G.USR_ID)  WHERE  \tG.USR_ID = ?  AND G.STAT_IND = ?  AND M.STAT_CD = ?  AND M.MSSN_ID = ? "

    .line 87
    .local v5, "sql":Ljava/lang/String;
    const/4 v7, 0x4

    new-array v4, v7, [Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v9

    const/4 v7, 0x1

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x2

    sget-object v8, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v8}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x3

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v7

    .line 88
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 90
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    invoke-virtual {v7, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_1

    .line 92
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 93
    const/4 v7, 0x0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 94
    if-lez v1, :cond_1

    .line 95
    const/4 v3, 0x1

    .line 104
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 105
    :cond_2
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 106
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getIsUserMissionActiveForAnyActiveGoal END, isUserMissionActiveForAnyActiveGoal:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", userId:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", missionId:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_3
    return v3

    .line 99
    :catch_0
    move-exception v2

    .line 100
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error getIsUserMissionActiveForAnyActiveGoal: "

    invoke-static {v7, v8, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 101
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 104
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 105
    :cond_4
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 106
    sget-object v8, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getIsUserMissionActiveForAnyActiveGoal END, isUserMissionActiveForAnyActiveGoal:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", userId:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", missionId:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v7
.end method

.method public getMissionsCompletedAfterFailureCount(Ljava/lang/String;)I
    .locals 9
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 829
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 830
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getMissionsCompletedAfterFailureCount: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v3

    .line 833
    .local v3, "userKey":I
    const/4 v0, 0x0

    .line 834
    .local v0, "count":I
    const/4 v1, 0x0

    .line 836
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/db/MissionDBQueries;->COMPLETED_MISSION_AFTER_FAILURE_COUNT:Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 838
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 839
    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 844
    if-eqz v1, :cond_1

    .line 845
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 847
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getMissionsCompletedAfterFailureCount: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 850
    :cond_2
    return v0

    .line 840
    :catch_0
    move-exception v2

    .line 841
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in getMissionsCompletedAfterFailureCount: "

    invoke-static {v4, v5, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 842
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 844
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    if-eqz v1, :cond_3

    .line 845
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 846
    :cond_3
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 847
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getMissionsCompletedAfterFailureCount: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method

.method public getNonCompleteUserMissionsForGoal(Ljava/lang/String;I)Ljava/util/List;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "goalId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/UserMissionData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 365
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 366
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getNonCompleteUserMissionsForGoal: START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_0
    const/4 v2, 0x0

    .line 369
    .local v2, "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 370
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Getting userkey"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :cond_1
    const/4 v0, 0x0

    .line 374
    .local v0, "c1":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 375
    .local v5, "userKey":I
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 376
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Querying for existing missions; goal id:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ":, userKey:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "::Query:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " SELECT MSSN_ID,MSSN_SEQ_ID  FROM USR_MSSN  WHERE GOAL_ID=? AND USR_ID = ? AND STAT_CD = ?   "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, " SELECT MSSN_ID,MSSN_SEQ_ID  FROM USR_MSSN  WHERE GOAL_ID=? AND USR_ID = ? AND STAT_CD = ?   "

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v11}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 383
    if-eqz v0, :cond_6

    .line 384
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 385
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    .end local v2    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .local v3, "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    :goto_0
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_a

    .line 387
    new-instance v4, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-direct {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;-><init>()V

    .line 389
    .local v4, "um":Lcom/cigna/coach/dataobjects/UserMissionData;
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionId(I)V

    .line 390
    const/4 v6, 0x1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionSeqId(I)V

    .line 392
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 393
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Mission id is:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " , sequence id:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    :cond_3
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 403
    .end local v4    # "um":Lcom/cigna/coach/dataobjects/UserMissionData;
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 404
    .end local v3    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .end local v5    # "userKey":I
    .local v1, "e":Ljava/lang/RuntimeException;
    .restart local v2    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    :goto_1
    :try_start_2
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Error in getNonCompleteUserMissionsForGoal: "

    invoke-static {v6, v7, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 405
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 407
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    :goto_2
    if-eqz v0, :cond_4

    .line 408
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 409
    :cond_4
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 410
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getNonCompleteUserMissionsForGoal: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v6

    .line 399
    .restart local v5    # "userKey":I
    :cond_6
    :try_start_3
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 400
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "No existing missions found for goal id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 407
    :cond_7
    :goto_3
    if-eqz v0, :cond_8

    .line 408
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 409
    :cond_8
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 410
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getNonCompleteUserMissionsForGoal: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    :cond_9
    return-object v2

    .line 407
    .end local v2    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .restart local v3    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .restart local v2    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    goto :goto_2

    .line 403
    .end local v5    # "userKey":I
    :catch_1
    move-exception v1

    goto :goto_1

    .end local v2    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .restart local v3    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .restart local v5    # "userKey":I
    :cond_a
    move-object v2, v3

    .end local v3    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    .restart local v2    # "returnMissionIds":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/UserMissionData;>;"
    goto :goto_3
.end method

.method public getRecentlyCompletedMissionCount(Ljava/lang/String;I)I
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "days"    # I

    .prologue
    .line 743
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 744
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getRecentlyCompletedMissionCount: START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    :cond_0
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfTodayInMilliSec()J

    move-result-wide v6

    invoke-static {p2}, Lcom/cigna/coach/utils/CalendarUtil;->fromDaysToMilliSecs(I)J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 747
    .local v2, "cutoffTime":J
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 748
    .local v5, "userKey":I
    const/4 v0, 0x0

    .line 749
    .local v0, "count":I
    const/4 v1, 0x0

    .line 751
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/db/MissionDBQueries;->RECENT_MISSION_COUNT:Ljava/lang/String;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 753
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 754
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 759
    if-eqz v1, :cond_1

    .line 760
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 761
    :cond_1
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 762
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getRecentlyCompletedMissionCount: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    :cond_2
    return v0

    .line 755
    :catch_0
    move-exception v4

    .line 756
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Error in getRecentlyCompletedMissionCount: "

    invoke-static {v6, v7, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 757
    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 759
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    if-eqz v1, :cond_3

    .line 760
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 761
    :cond_3
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 762
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getRecentlyCompletedMissionCount: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v6
.end method

.method public getUserMissionDetails(I)Lcom/cigna/coach/dataobjects/UserMissionData;
    .locals 11
    .param p1, "missionSequenceId"    # I

    .prologue
    .line 465
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 466
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getUserMissionDetails(final int missionSequenceId): START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :cond_0
    const/4 v5, 0x0

    .line 469
    .local v5, "userMissionData":Lcom/cigna/coach/dataobjects/UserMissionData;
    const/4 v4, 0x0

    .line 471
    .local v4, "subQuery":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.MSSN_SEQ_ID = ? "

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 473
    if-eqz v4, :cond_5

    .line 475
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 476
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GetUserMissionDetails_SELECT_USING_SEQUENCE_ID_MISSIONS_QUERY records = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_1
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 480
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_5

    .line 481
    invoke-virtual {p0, v4}, Lcom/cigna/coach/db/MissionDBManager;->retriveUserMissionDataFromCursor(Landroid/database/Cursor;)Lcom/cigna/coach/dataobjects/UserMissionData;

    move-result-object v5

    .line 483
    if-eqz v5, :cond_4

    .line 484
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 485
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Mission ID"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    :cond_2
    invoke-virtual {p0, v5}, Lcom/cigna/coach/db/MissionDBManager;->getUserMissionDetailsList(Lcom/cigna/coach/dataobjects/UserMissionData;)Ljava/util/List;

    move-result-object v3

    .line 489
    .local v3, "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    invoke-virtual {v5, v3}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionDetails(Ljava/util/List;)V

    .line 490
    invoke-virtual {p0, v3}, Lcom/cigna/coach/db/MissionDBManager;->getMissionActivityCompletedCount(Ljava/util/List;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionActivityCompletedTotalCount(I)V

    .line 493
    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionStage()Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    if-ne v6, v7, :cond_4

    .line 495
    new-instance v1, Lcom/cigna/coach/db/GoalDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v6

    invoke-direct {v1, v6}, Lcom/cigna/coach/db/GoalDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 496
    .local v1, "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getUserId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v7

    invoke-virtual {v1, v6, v7}, Lcom/cigna/coach/db/GoalDBManager;->getIsUserGoalActive(Ljava/lang/String;I)Z

    move-result v2

    .line 497
    .local v2, "goalIsActive":Z
    if-eqz v2, :cond_4

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq v6, v7, :cond_3

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq v6, v7, :cond_3

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq v6, v7, :cond_3

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v6

    sget-object v7, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne v6, v7, :cond_4

    .line 502
    :cond_3
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionRepeatable(Z)V

    .line 506
    .end local v1    # "goalDBManager":Lcom/cigna/coach/db/GoalDBManager;
    .end local v2    # "goalIsActive":Z
    .end local v3    # "missionDetailsList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/MissionDetails;>;"
    :cond_4
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    :cond_5
    if-eqz v4, :cond_6

    .line 514
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 515
    :cond_6
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 516
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getUserMissionDetails(final int missionSequenceId): END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    :cond_7
    return-object v5

    .line 509
    :catch_0
    move-exception v0

    .line 510
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v6, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Error in getUserMissionDetails(final int missionSequenceId): "

    invoke-static {v6, v7, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 511
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 513
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    if-eqz v4, :cond_8

    .line 514
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 515
    :cond_8
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 516
    sget-object v7, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getUserMissionDetails(final int missionSequenceId): END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    throw v6
.end method

.method public updateUserMission(Ljava/lang/String;Lcom/cigna/coach/dataobjects/UserMissionData;)I
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "userMissionData"    # Lcom/cigna/coach/dataobjects/UserMissionData;

    .prologue
    const/4 v1, 0x0

    .line 607
    if-nez p2, :cond_1

    .line 678
    :cond_0
    :goto_0
    return v1

    .line 609
    :cond_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 610
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMission: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 612
    :cond_2
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 613
    .local v3, "values":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 615
    .local v1, "noOfRowsUpdated":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 617
    .local v2, "userKey":I
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq v4, v5, :cond_3

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq v4, v5, :cond_3

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq v4, v5, :cond_3

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v4

    sget-object v5, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne v4, v5, :cond_5

    .line 621
    :cond_3
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_5

    .line 624
    :cond_4
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/cigna/coach/dataobjects/UserMissionData;->setMissionCompletedDate(Ljava/util/Calendar;)V

    .line 628
    :cond_5
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->isDisplayedToUser()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 629
    const-string v4, "MSSN_CMPLT_IND"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    :cond_6
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 633
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 634
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "End date is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v6

    invoke-static {v6}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :cond_7
    const-string v4, "MSSN_END_DT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionCompletedDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 639
    :cond_8
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 640
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 641
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start date is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v6

    invoke-static {v6}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    :cond_9
    const-string v4, "MSSN_START_DT"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 646
    :cond_a
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 647
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 648
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start date is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    :cond_b
    const-string v4, "STAT_CD"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 653
    :cond_c
    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v4

    if-lez v4, :cond_e

    .line 654
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 655
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Start date is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    :cond_d
    const-string v4, "USR_MSSN_FREQ"

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSelectedFrequencyDays()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 660
    :cond_e
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "USR_MSSN"

    const-string v6, "MSSN_ID = ? AND USR_ID = ? AND MSSN_SEQ_ID =?"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionSeqId()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 667
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 673
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 674
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No of Rows Updated = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMission: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 669
    .end local v2    # "userKey":I
    :catch_0
    move-exception v0

    .line 670
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in updateUserMission: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 671
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 673
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 674
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No of Rows Updated = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserMission: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    throw v4
.end method

.method public updateUserMissionData(Ljava/lang/String;IILcom/cigna/coach/apiobjects/MissionDetails;)I
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I
    .param p3, "missionSequenceId"    # I
    .param p4, "missionDetails"    # Lcom/cigna/coach/apiobjects/MissionDetails;

    .prologue
    .line 524
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 525
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMissionData: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :cond_0
    const/4 v1, 0x0

    .line 529
    .local v1, "noOfRowsUpdated":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 530
    .local v2, "userKey":I
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 531
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "MSSN_ACTY_CNT"

    invoke-virtual {p4}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 532
    const-string v4, "MSSN_ACCOMP_DT"

    invoke-virtual {p4}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 535
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "USR_MSSN_DATA"

    const-string v6, "MSSN_ID = ? AND USR_ID = ? AND MSSN_SEQ_ID = ? AND MSSN_ACCOMP_DT = ?"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 546
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 547
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No of Rows Updated = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMissionData: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    :cond_1
    return v1

    .line 542
    .end local v2    # "userKey":I
    .end local v3    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in updateUserMissionData: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 544
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 546
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 547
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No of Rows Updated = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserMissionData: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v4
.end method

.method public updateUserMissionStatus(Ljava/lang/String;IILcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "missionId"    # I
    .param p3, "missionSeqNum"    # I
    .param p4, "status"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .prologue
    .line 682
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 683
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMissionStatus: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 686
    .local v3, "values":Landroid/content/ContentValues;
    const/4 v1, 0x0

    .line 688
    .local v1, "noOfRowsUpdated":I
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v2

    .line 689
    .local v2, "userKey":I
    const-string v4, "STAT_CD"

    invoke-virtual {p4}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 690
    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq p4, v4, :cond_1

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq p4, v4, :cond_1

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq p4, v4, :cond_1

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->REMOVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne p4, v4, :cond_2

    .line 692
    :cond_1
    const-string v4, "MSSN_END_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 695
    :cond_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "USR_MSSN"

    const-string v6, "MSSN_ID = ? AND USR_ID = ? AND MSSN_SEQ_ID = ?"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 702
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 708
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 709
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No of Rows Updated = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_3
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 712
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMissionStatus: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 715
    :cond_4
    return-void

    .line 704
    .end local v2    # "userKey":I
    :catch_0
    move-exception v0

    .line 705
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in updateUserMissionStatus: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 706
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 708
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 709
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "No of Rows Updated = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :cond_5
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 712
    sget-object v5, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserMissionStatus: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v4
.end method

.method public updateUserMissionWithFailedStatus(I)I
    .locals 9
    .param p1, "missionSequenceId"    # I

    .prologue
    .line 580
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 581
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateUserMissionWithFailedStatus: START, missionSequenceId:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    :cond_0
    const/4 v1, 0x0

    .line 585
    .local v1, "noOfRowsUpdated":I
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 586
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "STAT_CD"

    sget-object v4, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v4}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 587
    const-string v3, "MSSN_END_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 589
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "USR_MSSN"

    const-string v5, "MSSN_SEQ_ID = ? "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 598
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 599
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No of Rows Updated = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "updateUserMissionWithFailedStatus: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :cond_1
    return v1

    .line 594
    .end local v2    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 595
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in updateUserMissionWithFailedStatus: "

    invoke-static {v3, v4, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 596
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 598
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 599
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No of Rows Updated = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMissionWithFailedStatus: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v3
.end method

.method public updateUserMissionWithMissionFailedReason(ILcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)I
    .locals 9
    .param p1, "missionSequenceId"    # I
    .param p2, "missionFailedReason"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .prologue
    .line 555
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 556
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "updateUserMissionWithMissionFailedReason: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :cond_0
    const/4 v1, 0x0

    .line 560
    .local v1, "noOfRowsUpdated":I
    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 561
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "FAILED_REASN_CD"

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;->getReasonKey()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 562
    invoke-virtual {p0}, Lcom/cigna/coach/db/MissionDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, "USR_MSSN"

    const-string v5, "MSSN_SEQ_ID = ? "

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 571
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 572
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No of Rows Updated = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v4, "updateUserMissionWithMissionFailedReason: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    :cond_1
    return v1

    .line 567
    .end local v2    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 568
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Error in updateUserMissionWithMissionFailedReason: "

    invoke-static {v3, v4, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 569
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 571
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 572
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No of Rows Updated = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    sget-object v4, Lcom/cigna/coach/db/MissionDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserMissionWithMissionFailedReason: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v3
.end method
