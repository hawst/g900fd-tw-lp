.class public Lcom/cigna/coach/db/NotificationDBQueries;
.super Ljava/lang/Object;
.source "NotificationDBQueries.java"


# static fields
.field static final DeleteNotificationsFromQueue_WHERE_CLAUSE:Ljava/lang/String; = "(DLVRY_DT <= ?)"

.field static final GetNotificationStateInfo_SELECT_NOTIFICATION_STATE_INFO:Ljava/lang/String; = "SELECT  A.LAST_STRTUP_DT, A.LAST_TM_CHG_DT, A.NOTFTN_STRT_TM, A.NOTFTN_END_TM FROM NOTFTN_STE A"

.field static final GetNotificationsFromQueue_SELECT_NOTIFICATION_OBJECTS:Ljava/lang/String; = "SELECT  A.NOTFTN_OBJ FROM NOTFTN_QUEUE_BLOB A WHERE  (A.DLVRY_DT <= ?)"

.field protected static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
