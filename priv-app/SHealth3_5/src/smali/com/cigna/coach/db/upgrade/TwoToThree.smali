.class public Lcom/cigna/coach/db/upgrade/TwoToThree;
.super Ljava/lang/Object;
.source "TwoToThree.java"

# interfaces
.implements Lcom/cigna/coach/db/upgrade/IDBUpgrade;


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final DB_VERSN_CREATE:Ljava/lang/String; = "CREATE TABLE DB_VERSN(CD_BASE_ID INTEGER NOT NULL,DB_VERSN INTEGER NOT NULL)"

.field private static final USR_ACTY_JRNL_CREATE:Ljava/lang/String; = "CREATE TABLE USR_ACTY_JRNL (SEQ_ID   INTEGER PRIMARY KEY AUTOINCREMENT,USR_ID   INTEGER NOT NULL,CD_BASE_ID INTEGER NOT NULL,DB_VERSN INTEGER NOT NULL,TBL_NM   TEXT,ACTN_TIMESTMP DATETIME DEFAULT CURRENT_TIMESTAMP,ACTN_CD  INTEGER,KEY      TEXT,DATA     TEXT)"

.field private static final USR_ACTY_JRNL_DROP:Ljava/lang/String; = "DROP TABLE USR_ACTY_JRNL"

.field private static final USR_API_CALL_LOG_CREATE:Ljava/lang/String; = "CREATE TABLE USR_API_CALL_LOG (USR_ID        INTEGER NOT NULL,API_CD        INTEGER NOT NULL,API_CALL_SRC_CD  INTEGER NOT NULL,API_CALL_TS      INTEGER NOT NULL,API_CALL_RESP_TM INTEGER NOT NULL,API_CALL_ERR_CD  INTEGER,FOREIGN KEY (USR_ID) REFERENCES USR_INFO (USR_ID))"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/cigna/coach/db/upgrade/TwoToThree;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private addCreateQueriesList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 190
    .local v0, "createQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "CREATE TABLE USR_ACTY_JRNL (SEQ_ID   INTEGER PRIMARY KEY AUTOINCREMENT,USR_ID   INTEGER NOT NULL,CD_BASE_ID INTEGER NOT NULL,DB_VERSN INTEGER NOT NULL,TBL_NM   TEXT,ACTN_TIMESTMP DATETIME DEFAULT CURRENT_TIMESTAMP,ACTN_CD  INTEGER,KEY      TEXT,DATA     TEXT)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    const-string v1, "CREATE TABLE DB_VERSN(CD_BASE_ID INTEGER NOT NULL,DB_VERSN INTEGER NOT NULL)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    const-string v1, "CREATE TABLE USR_API_CALL_LOG (USR_ID        INTEGER NOT NULL,API_CD        INTEGER NOT NULL,API_CALL_SRC_CD  INTEGER NOT NULL,API_CALL_TS      INTEGER NOT NULL,API_CALL_RESP_TM INTEGER NOT NULL,API_CALL_ERR_CD  INTEGER,FOREIGN KEY (USR_ID) REFERENCES USR_INFO (USR_ID))"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    return-object v0
.end method

.method private addDropQueriesToList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    const-string v1, "DROP TABLE USR_ACTY_JRNL"

    .line 226
    .local v1, "userActivityJournalDropQuery":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 227
    .local v0, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    return-object v0
.end method

.method private executeCreateTableSqlScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 163
    .local p2, "createTableQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 164
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Preparing to execute the SQL scripts from list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 169
    .local v2, "sqlStatement":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 170
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 181
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 182
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    :cond_3
    return-void
.end method

.method private executeDropTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 199
    .local p2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 200
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Preparing to execute the SQL scripts from list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 205
    .local v2, "sqlStatement":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 206
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 211
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 212
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 217
    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 218
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_3
    return-void
.end method

.method private insertDataIntoDbVersionTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 134
    :try_start_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 135
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "CD_BASE_ID"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 136
    const-string v2, "DB_VERSN"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 137
    const-string v2, "DB_VERSN"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    return-void

    .line 138
    .end local v1    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 139
    .local v0, "ex":Landroid/database/sqlite/SQLiteException;
    sget-object v2, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "An error occured while executing sql statement"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    sget-object v2, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error is:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v2, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v2, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private insertDataIntoUsrActyJrnlSwitchTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 147
    .local p2, "tableNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 148
    .local v2, "tableName":Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 149
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "TBL_NM"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v4, "JRNL_ON"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 151
    const-string v4, "USR_ACTY_JRNL_SWITCH"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 153
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "tableName":Ljava/lang/String;
    .end local v3    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v0

    .line 154
    .local v0, "ex":Landroid/database/sqlite/SQLiteException;
    sget-object v4, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "An error occured while executing sql statement"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    sget-object v4, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error is:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v4, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 158
    .end local v0    # "ex":Landroid/database/sqlite/SQLiteException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    return-void
.end method

.method private updateContent(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 119
    sget-object v1, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 120
    sget-object v1, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Updating content"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/CMSHelper;

    invoke-direct {v0, p1}, Lcom/cigna/coach/utils/CMSHelper;-><init>(Landroid/content/Context;)V

    .line 124
    .local v0, "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/cigna/coach/utils/CMSHelper;->checkAndCopyDefaultZipFilesToLocalPath(ZLandroid/database/sqlite/SQLiteDatabase;)Z

    .line 125
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/cigna/coach/utils/CMSHelper;->updateLanguageContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    .line 127
    sget-object v1, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    sget-object v1, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Content update complete"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_1
    return-void
.end method


# virtual methods
.method public applyUpgrade(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 87
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "***********************************"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_0
    invoke-static {p1, p2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->dropJournalTriggers(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 95
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/TwoToThree;->addDropQueriesToList()Ljava/util/List;

    move-result-object v1

    .line 96
    .local v1, "dropTableQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v1}, Lcom/cigna/coach/db/upgrade/TwoToThree;->executeDropTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 99
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/TwoToThree;->addCreateQueriesList()Ljava/util/List;

    move-result-object v0

    .line 100
    .local v0, "createTableQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v0}, Lcom/cigna/coach/db/upgrade/TwoToThree;->executeCreateTableSqlScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 102
    invoke-direct {p0, p2}, Lcom/cigna/coach/db/upgrade/TwoToThree;->insertDataIntoDbVersionTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 104
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v2, "journalTableNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v3, "LFSTYL_ASSMT_SCORE"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    const-string v3, "USR_API_CALL_LOG"

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-direct {p0, p2, v2}, Lcom/cigna/coach/db/upgrade/TwoToThree;->insertDataIntoUsrActyJrnlSwitchTable(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 110
    invoke-direct {p0, p1, p2}, Lcom/cigna/coach/db/upgrade/TwoToThree;->updateContent(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 112
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 113
    sget-object v3, Lcom/cigna/coach/db/upgrade/TwoToThree;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "***********************************"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_1
    return-void
.end method
