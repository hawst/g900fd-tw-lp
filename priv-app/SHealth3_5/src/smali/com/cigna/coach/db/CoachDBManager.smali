.class public abstract Lcom/cigna/coach/db/CoachDBManager;
.super Ljava/lang/Object;
.source "CoachDBManager.java"


# instance fields
.field helper:Lcom/cigna/coach/db/CoachSQLiteHelper;


# direct methods
.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 1
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/db/CoachDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 40
    iput-object p1, p0, Lcom/cigna/coach/db/CoachDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    .line 41
    return-void
.end method

.method protected static makePlaceholders(I)Ljava/lang/String;
    .locals 4
    .param p0, "len"    # I

    .prologue
    .line 52
    const/4 v2, 0x1

    if-ge p0, v2, :cond_0

    .line 54
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "No placeholders"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 56
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    mul-int/lit8 v2, p0, 0x2

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 57
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/16 v2, 0x3f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 59
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, p0, :cond_1

    .line 60
    const-string v2, ",?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method protected final getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/cigna/coach/db/CoachDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    invoke-virtual {v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method protected getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cigna/coach/db/CoachDBManager;->helper:Lcom/cigna/coach/db/CoachSQLiteHelper;

    return-object v0
.end method
