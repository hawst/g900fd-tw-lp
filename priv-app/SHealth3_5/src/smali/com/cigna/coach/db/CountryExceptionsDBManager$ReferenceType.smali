.class public final enum Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;
.super Ljava/lang/Enum;
.source "CountryExceptionsDBManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/CountryExceptionsDBManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReferenceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

.field public static final enum AGE:Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;


# instance fields
.field referenceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 386
    new-instance v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    const-string v1, "AGE"

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->AGE:Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    .line 385
    new-array v0, v3, [Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    sget-object v1, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->AGE:Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->$VALUES:[Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 392
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 393
    iput p3, p0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->referenceType:I

    .line 394
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 385
    const-class v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;
    .locals 1

    .prologue
    .line 385
    sget-object v0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->$VALUES:[Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    invoke-virtual {v0}, [Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;

    return-object v0
.end method


# virtual methods
.method public getReferenceType()I
    .locals 1

    .prologue
    .line 398
    iget v0, p0, Lcom/cigna/coach/db/CountryExceptionsDBManager$ReferenceType;->referenceType:I

    return v0
.end method
