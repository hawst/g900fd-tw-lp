.class public Lcom/cigna/coach/db/LifeStyleDBManager;
.super Lcom/cigna/coach/db/CoachDBManager;
.source "LifeStyleDBManager.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final SINGLE_USER_ID:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const-class v0, Lcom/cigna/coach/db/LifeStyleDBManager;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V
    .locals 0
    .param p1, "helper"    # Lcom/cigna/coach/db/CoachSQLiteHelper;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/CoachDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 84
    return-void
.end method

.method private insertUserInformation(Lcom/cigna/coach/dataobjects/UserInfo;)V
    .locals 5
    .param p1, "userInfo"    # Lcom/cigna/coach/dataobjects/UserInfo;

    .prologue
    .line 256
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserInformation : START"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_0
    if-eqz p1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 261
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 262
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Preparing to insert  user information to database"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 266
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "USR_ID"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 267
    const-string v2, "USR_ID_TXT"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v2, "USR_INFO_DT"

    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 270
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "USR_INFO"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 271
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 272
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "Insert completed"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_2
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v2}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 280
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserInformation: END"

    invoke-static {v2, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :cond_3
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v2, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "insertUserInformation: "

    invoke-static {v2, v3, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 277
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 279
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 280
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "insertUserInformation: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v2
.end method

.method private updateUserInformation(Lcom/cigna/coach/dataobjects/UserInfo;)V
    .locals 7
    .param p1, "userInfo"    # Lcom/cigna/coach/dataobjects/UserInfo;

    .prologue
    .line 286
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 287
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserInformation: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_0
    if-eqz p1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 291
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 292
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Preparing to update  user information to database"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 300
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "USR_ID_TXT"

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const-string v3, "USR_ID = ?"

    .line 303
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 305
    .local v2, "whereArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "USR_INFO"

    invoke-virtual {v4, v5, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 306
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 307
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Update completed"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    .end local v1    # "values":Landroid/content/ContentValues;
    .end local v2    # "whereArgs":[Ljava/lang/String;
    .end local v3    # "whereClause":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 315
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v5, "updateUserInformation: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    :cond_3
    return-void

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in updateUserInformation: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 312
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 315
    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v6, "updateUserInformation: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v4
.end method


# virtual methods
.method public createOrUpdateUserInformation(Lcom/cigna/coach/dataobjects/UserInfo;)Z
    .locals 7
    .param p1, "userInfo"    # Lcom/cigna/coach/dataobjects/UserInfo;

    .prologue
    .line 364
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 365
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "createOrUpdateUserInformation: START"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_0
    const/4 v1, 0x0

    .line 369
    .local v1, "isNewUser":Z
    if-eqz p1, :cond_4

    .line 370
    :try_start_0
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 372
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/UserInfo;->getGender()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v4

    if-nez v4, :cond_2

    .line 373
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 374
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Defaulting gender to unknown"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    :cond_1
    sget-object v4, Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;->UNKNOWN:Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    invoke-virtual {p1, v4}, Lcom/cigna/coach/dataobjects/UserInfo;->setGender(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;)V

    .line 378
    :cond_2
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserCount()I

    move-result v2

    .line 379
    .local v2, "userCount":I
    if-nez v2, :cond_6

    .line 380
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 381
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Inserting new user"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    :cond_3
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/LifeStyleDBManager;->insertUserInformation(Lcom/cigna/coach/dataobjects/UserInfo;)V

    .line 384
    const/4 v1, 0x1

    .line 394
    :goto_0
    new-instance v3, Lcom/cigna/coach/db/UserMetricsDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/cigna/coach/db/UserMetricsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 395
    .local v3, "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    invoke-virtual {v3, p1}, Lcom/cigna/coach/db/UserMetricsDBManager;->createOrUpdateUserMetrics(Lcom/cigna/coach/dataobjects/UserInfo;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    .end local v2    # "userCount":I
    .end local v3    # "userMetricsDBManager":Lcom/cigna/coach/db/UserMetricsDBManager;
    :cond_4
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 406
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "createOrUpdateUserInformation: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    :cond_5
    return v1

    .line 387
    .restart local v2    # "userCount":I
    :cond_6
    :try_start_1
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 388
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "User id already exists"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    :cond_7
    invoke-direct {p0, p1}, Lcom/cigna/coach/db/LifeStyleDBManager;->updateUserInformation(Lcom/cigna/coach/dataobjects/UserInfo;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 401
    .end local v2    # "userCount":I
    :catch_0
    move-exception v0

    .line 402
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "Error in createOrUpdateUserInformation: "

    invoke-static {v4, v5, v0}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 403
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 405
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 406
    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "createOrUpdateUserInformation: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    throw v4

    .line 398
    :cond_9
    :try_start_3
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v5, "userId must not be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public getAllCategories()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/CategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 87
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAllCategories: START"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v11, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/CategoryInfo;>;"
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "CTGRY_ID"

    aput-object v0, v2, v3

    const-string v0, "CNTNT_ID"

    aput-object v0, v2, v4

    const-string v0, "ACTV_IND"

    aput-object v0, v2, v5

    const-string v0, "IMG_CNTNT_ID"

    aput-object v0, v2, v6

    .line 92
    .local v2, "columns":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 94
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "CTGRY"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "RANK_NUM"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 95
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 96
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_3

    .line 97
    new-instance v9, Lcom/cigna/coach/dataobjects/CategoryInfoData;

    invoke-direct {v9}, Lcom/cigna/coach/dataobjects/CategoryInfoData;-><init>()V

    .line 98
    .local v9, "catInfo":Lcom/cigna/coach/dataobjects/CategoryInfoData;
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/cigna/coach/dataobjects/CategoryInfoData;->setCategoryType(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V

    .line 99
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/cigna/coach/dataobjects/CategoryInfoData;->setContentId(I)V

    .line 100
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/cigna/coach/dataobjects/CategoryInfoData;->setIsActive(I)V

    .line 101
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {v9, v0}, Lcom/cigna/coach/dataobjects/CategoryInfoData;->setImageContentId(I)V

    .line 103
    invoke-interface {v11, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    .end local v9    # "catInfo":Lcom/cigna/coach/dataobjects/CategoryInfoData;
    :catch_0
    move-exception v10

    .line 108
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "Error in createUserGoals: "

    invoke-static {v0, v1, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 109
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 112
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 113
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 114
    :cond_1
    sget-object v1, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 115
    sget-object v1, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v3, "getAllCategories: END"

    invoke-static {v1, v3}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    throw v0

    .line 112
    :cond_3
    if-eqz v8, :cond_4

    .line 113
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 114
    :cond_4
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 115
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "getAllCategories: END"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :cond_5
    return-object v11
.end method

.method public getCategoryOfQuestionGroup(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 9
    .param p1, "qGrpId"    # I

    .prologue
    .line 413
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 414
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getCategoryOfQuestionGroup: START"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :cond_0
    const/4 v2, 0x0

    .line 417
    .local v2, "returnType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 418
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Finding category for question group :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    :cond_1
    const/4 v0, 0x0

    .line 422
    .local v0, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const-string v4, " SELECT CTGRY_ID FROM LFSTYL_ASSMT_QUEST_GRP WHERE LFSTYL_ASSMT_QUEST_GRP_ID = ?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 424
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 425
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 431
    :cond_2
    if-eqz v0, :cond_3

    .line 432
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_3
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    if-eqz v2, :cond_4

    .line 435
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Category is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getCategoryOfQuestionGroup: END"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    :cond_4
    return-object v2

    .line 427
    :catch_0
    move-exception v1

    .line 428
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "getCategoryOfQuestionGroup: "

    invoke-static {v3, v4, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 429
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 431
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    if-eqz v0, :cond_5

    .line 432
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 434
    :cond_5
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v4}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    if-eqz v2, :cond_6

    .line 435
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Category is:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    sget-object v4, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v5, "getCategoryOfQuestionGroup: END"

    invoke-static {v4, v5}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    throw v3
.end method

.method public getDistinctTimeForUserResponse(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1320
    sget-object v6, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1321
    sget-object v6, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getDistinctTimeForUserResponse: START"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1325
    .local v2, "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Calendar;>;"
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-static {v6, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v5

    .line 1327
    .local v5, "userKey":I
    const/4 v6, -0x1

    if-ne v5, v6, :cond_2

    .line 1328
    const/4 v2, 0x0

    .line 1356
    .end local v2    # "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Calendar;>;"
    :cond_1
    :goto_0
    return-object v2

    .line 1330
    .restart local v2    # "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Calendar;>;"
    :cond_2
    const/4 v0, 0x0

    .line 1332
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "SELECT DISTINCT ASSMT_UPDT_TS FROM USR_LFSTYL_ASSMT_RESP WHERE USR_ID = ? ORDER BY ASSMT_UPDT_TS ASC"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1335
    if-eqz v0, :cond_5

    .line 1336
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1337
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_5

    .line 1339
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 1340
    .local v3, "timeInMillisec":J
    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1342
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1345
    .end local v3    # "timeInMillisec":J
    :catch_0
    move-exception v1

    .line 1346
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v6, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "Error getDistinctTimeForUserResponse..."

    invoke-static {v6, v7, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1347
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1349
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_3

    .line 1350
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1351
    :cond_3
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1352
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getDistinctTimeForUserResponse: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v6

    .line 1349
    :cond_5
    if-eqz v0, :cond_6

    .line 1350
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1351
    :cond_6
    sget-object v6, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1352
    sget-object v6, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getDistinctTimeForUserResponse: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLifeStyleAssesmentQA(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;)Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .locals 37
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "categoryFilter"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    .prologue
    .line 123
    sget-object v34, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v34 .. v34}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_0

    .line 124
    sget-object v34, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v35, "getLifeStyleAssessmentQA: START"

    invoke-static/range {v34 .. v35}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_0
    const/4 v7, 0x0

    .line 127
    .local v7, "args":[Ljava/lang/String;
    const-string v33, " WHERE    A.ACTV_IND = 1 AND   B.ACTV_IND = 1 AND   (C.ACTV_IND IS null OR C.ACTV_IND = 1)"

    .line 128
    .local v33, "whereClauseToUse":Ljava/lang/String;
    const/16 v28, 0x0

    .line 129
    .local v28, "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    const/4 v14, -0x1

    .line 130
    .local v14, "prevCatId":I
    const/4 v15, -0x1

    .line 131
    .local v15, "prevQGrpId":I
    const/16 v16, -0x1

    .line 132
    .local v16, "prevQId":I
    const/16 v27, 0x0

    .line 134
    .local v27, "result":Landroid/database/Cursor;
    :try_start_0
    new-instance v11, Lcom/cigna/coach/db/CountryExceptionsDBManager;

    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v34

    move-object/from16 v0, v34

    invoke-direct {v11, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 135
    .local v11, "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    const/16 v32, 0x0

    .line 136
    .local v32, "shouldSuppressAlcoholRelatedContent":Z
    sget-object v34, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    move-object/from16 v0, p2

    move-object/from16 v1, v34

    if-eq v0, v1, :cond_1

    sget-object v34, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->NUTRITION:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    move-object/from16 v0, p2

    move-object/from16 v1, v34

    if-ne v0, v1, :cond_2

    .line 138
    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppressAlcoholRelatedContent(Ljava/lang/String;)Z

    move-result v32

    .line 140
    :cond_2
    if-eqz v32, :cond_4

    .line 141
    sget-object v34, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v34 .. v34}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_3

    .line 142
    sget-object v34, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, "getLifeStyleAssessmentQA: suppressing alcohol related content, categoryFilter : "

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v34 .. v35}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_3
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " AND A."

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, "SUB_CTGRY_ID"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " != "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const/16 v35, 0xcd

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " "

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 147
    :cond_4
    sget-object v34, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->ALL:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;

    move-object/from16 v0, p2

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->equals(Ljava/lang/Object;)Z

    move-result v34

    if-nez v34, :cond_5

    .line 148
    new-instance v34, Ljava/lang/StringBuilder;

    invoke-direct/range {v34 .. v34}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    const-string v35, " AND A.CTGRY_ID = ?"

    invoke-virtual/range {v34 .. v35}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v34

    invoke-virtual/range {v34 .. v34}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 149
    invoke-virtual/range {p2 .. p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryTypeRequest;->toString()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->valueOf(Ljava/lang/String;)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v30

    .line 150
    .local v30, "selectedCategory":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-virtual/range {v30 .. v30}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v31

    .line 151
    .local v31, "selectedCategoryId":I
    const/16 v34, 0x1

    move/from16 v0, v34

    new-array v8, v0, [Ljava/lang/String;

    const/16 v34, 0x0

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, ""

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    aput-object v35, v8, v34

    .end local v7    # "args":[Ljava/lang/String;
    .local v8, "args":[Ljava/lang/String;
    move-object v7, v8

    .line 154
    .end local v8    # "args":[Ljava/lang/String;
    .end local v30    # "selectedCategory":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v31    # "selectedCategoryId":I
    .restart local v7    # "args":[Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v34

    new-instance v35, Ljava/lang/StringBuilder;

    invoke-direct/range {v35 .. v35}, Ljava/lang/StringBuilder;-><init>()V

    const-string v36, " SELECT  \tA.CTGRY_ID, \tA.LFSTYL_ASSMT_QUEST_GRP_ID, \tA.CNTNT_ID AS A_CNTNT_ID, \tA.DISPL_ORDR_NUM AS A_DISPL_ORDR_NUM, \tB.LFSTYL_ASSMT_QUEST_ID, \tB.CNTNT_ID AS B_CNTNT_ID, \tB.ANS_TY_IND, \tB.DISPL_ORDR_NUM AS B_DISPL_ORDR_NUM, \tC.LFSTYL_ASSMT_ANS_ID, \tC.CNTNT_ID AS C_CNTNT_ID, \tC.DISPL_ORDR_NUM AS C_DISPL_ORDR_NUM,   A.IMG_CNTNT_ID AS A_IMG_CNTNT_ID,   B.IMG_CNTNT_ID AS B_IMG_CNTNT_ID,   C.IMG_CNTNT_ID AS C_IMG_CNTNT_ID,   C.ANS_SMMRY_CNTNT_ID AS ANS_SMMRY_CNTNT_ID FROM LFSTYL_ASSMT_QUEST_GRP A  INNER JOIN LFSTYL_ASSMT_QUEST B ON    A.LFSTYL_ASSMT_QUEST_GRP_ID = B.LFSTYL_ASSMT_QUEST_GRP_ID  LEFT OUTER JOIN LFSTYL_ASSMT_ANS C ON    B.LFSTYL_ASSMT_QUEST_GRP_ID = C.LFSTYL_ASSMT_QUEST_GRP_ID AND   B.LFSTYL_ASSMT_QUEST_ID = C.LFSTYL_ASSMT_QUEST_ID"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    move-object/from16 v0, v35

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    const-string v36, " ORDER BY     A.CTGRY_ID,     A.LFSTYL_ASSMT_QUEST_GRP_ID,     A.DISPL_ORDR_NUM,     B.LFSTYL_ASSMT_QUEST_ID,    B.DISPL_ORDR_NUM,     C.DISPL_ORDR_NUM"

    invoke-virtual/range {v35 .. v36}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v35

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-virtual {v0, v1, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v27

    .line 157
    if-eqz v27, :cond_10

    .line 158
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToFirst()Z

    .line 160
    new-instance v29, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;

    invoke-direct/range {v29 .. v29}, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;-><init>()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    .end local v28    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .local v29, "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    const/4 v10, 0x0

    .line 162
    .local v10, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :try_start_1
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 163
    .local v25, "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    const/16 v24, 0x0

    .line 164
    .local v24, "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    const/16 v26, 0x0

    .line 165
    .local v26, "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    const/16 v23, 0x0

    .line 166
    .local v23, "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    const/4 v6, 0x0

    .line 168
    .local v6, "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    :goto_0
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v34

    if-nez v34, :cond_f

    .line 169
    const/16 v34, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 170
    .local v9, "catId":I
    const/16 v34, 0x1

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 171
    .local v20, "qGrpId":I
    const/16 v34, 0x2

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 175
    .local v19, "qGroupCntntId":I
    const/16 v34, 0x4

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v21

    .line 176
    .local v21, "qId":I
    const/16 v34, 0x5

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 177
    .local v18, "qCntntId":I
    const/16 v34, 0x6

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 178
    .local v17, "qAnsType":I
    const/16 v34, 0xc

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 181
    .local v22, "qImgCntntId":I
    sget-object v34, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual/range {v34 .. v34}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->getAnswerTypeKey()I

    move-result v34

    move/from16 v0, v17

    move/from16 v1, v34

    if-ne v0, v1, :cond_d

    const/4 v13, 0x1

    .line 182
    .local v13, "isAnswer":Z
    :goto_1
    const/16 v34, 0x8

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 183
    .local v4, "ansId":I
    const/16 v34, 0x9

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 184
    .local v3, "ansCntntId":I
    const/16 v34, 0xe

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 188
    .local v5, "ansSummCntntId":I
    if-eq v9, v14, :cond_6

    if-eqz v29, :cond_6

    .line 189
    invoke-static {v9}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v10

    .line 190
    new-instance v25, Ljava/util/ArrayList;

    .end local v25    # "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .restart local v25    # "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v10, v1}, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    move v14, v9

    .line 196
    :cond_6
    move/from16 v0, v20

    if-eq v15, v0, :cond_7

    .line 197
    move/from16 v15, v20

    .line 198
    new-instance v24, Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;

    .end local v24    # "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    invoke-direct/range {v24 .. v24}, Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;-><init>()V

    .line 199
    .restart local v24    # "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;->setQuestionGroupId(I)V

    .line 200
    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;->setContentId(I)V

    .line 202
    new-instance v26, Ljava/util/ArrayList;

    .end local v26    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .restart local v26    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;->setAnswer(Ljava/util/List;)V

    .line 204
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_7
    move/from16 v0, v16

    move/from16 v1, v21

    if-eq v0, v1, :cond_8

    if-eqz v26, :cond_8

    .line 208
    move/from16 v16, v21

    .line 209
    new-instance v23, Lcom/cigna/coach/dataobjects/QuestionAnswersData;

    .end local v23    # "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    invoke-direct/range {v23 .. v23}, Lcom/cigna/coach/dataobjects/QuestionAnswersData;-><init>()V

    .line 210
    .restart local v23    # "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->setQuestionId(I)V

    .line 211
    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->setContentId(I)V

    .line 212
    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->setImageContentId(I)V

    .line 213
    const/16 v34, 0xf19

    move-object/from16 v0, v23

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->setTrackerSaysContentId(I)V

    .line 214
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .restart local v6    # "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->setAnswerList(Ljava/util/List;)V

    .line 216
    move-object/from16 v0, v26

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_8
    new-instance v2, Lcom/cigna/coach/dataobjects/AnswerData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/AnswerData;-><init>()V

    .line 220
    .local v2, "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    invoke-virtual {v2, v3}, Lcom/cigna/coach/dataobjects/AnswerData;->setContentId(I)V

    .line 221
    invoke-virtual {v2, v5}, Lcom/cigna/coach/dataobjects/AnswerData;->setSummaryContentId(I)V

    .line 223
    if-eqz v13, :cond_e

    .line 224
    sget-object v34, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerType(Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;)V

    .line 225
    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerId(I)V

    .line 226
    const-string v34, ""

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswer(Ljava/lang/String;)V

    .line 232
    :goto_2
    if-eqz v6, :cond_9

    .line 233
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_9
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->isLast()Z

    move-result v34

    if-eqz v34, :cond_a

    if-eqz v29, :cond_a

    .line 237
    move-object/from16 v0, v29

    move-object/from16 v1, v25

    invoke-virtual {v0, v10, v1}, Lcom/cigna/coach/apiobjects/CategoryGroupQAs;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    :cond_a
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto/16 :goto_0

    .line 242
    .end local v2    # "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    .end local v3    # "ansCntntId":I
    .end local v4    # "ansId":I
    .end local v5    # "ansSummCntntId":I
    .end local v6    # "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    .end local v9    # "catId":I
    .end local v13    # "isAnswer":Z
    .end local v17    # "qAnsType":I
    .end local v18    # "qCntntId":I
    .end local v19    # "qGroupCntntId":I
    .end local v20    # "qGrpId":I
    .end local v21    # "qId":I
    .end local v22    # "qImgCntntId":I
    .end local v23    # "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    .end local v24    # "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    .end local v25    # "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .end local v26    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    :catch_0
    move-exception v12

    move-object/from16 v28, v29

    .line 243
    .end local v10    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v11    # "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    .end local v29    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .end local v32    # "shouldSuppressAlcoholRelatedContent":Z
    .local v12, "e":Ljava/lang/RuntimeException;
    .restart local v28    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    :goto_3
    :try_start_2
    sget-object v34, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v35, "Error in getLifeStyleAssessmentQA: "

    move-object/from16 v0, v34

    move-object/from16 v1, v35

    invoke-static {v0, v1, v12}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 244
    throw v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    .end local v12    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v34

    :goto_4
    if-eqz v27, :cond_b

    .line 247
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 248
    :cond_b
    sget-object v35, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v35 .. v35}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v35

    if-eqz v35, :cond_c

    .line 249
    sget-object v35, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v36, "getLifeStyleAssessmentQA: END"

    invoke-static/range {v35 .. v36}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v34

    .line 181
    .end local v28    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .restart local v6    # "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    .restart local v9    # "catId":I
    .restart local v10    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .restart local v11    # "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    .restart local v17    # "qAnsType":I
    .restart local v18    # "qCntntId":I
    .restart local v19    # "qGroupCntntId":I
    .restart local v20    # "qGrpId":I
    .restart local v21    # "qId":I
    .restart local v22    # "qImgCntntId":I
    .restart local v23    # "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    .restart local v24    # "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    .restart local v25    # "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .restart local v26    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    .restart local v29    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .restart local v32    # "shouldSuppressAlcoholRelatedContent":Z
    :cond_d
    const/4 v13, 0x0

    goto/16 :goto_1

    .line 228
    .restart local v2    # "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    .restart local v3    # "ansCntntId":I
    .restart local v4    # "ansId":I
    .restart local v5    # "ansSummCntntId":I
    .restart local v13    # "isAnswer":Z
    :cond_e
    :try_start_3
    sget-object v34, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerType(Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;)V

    .line 229
    invoke-virtual {v2, v4}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerId(I)V

    .line 230
    const-string v34, ""

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswer(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    .line 246
    .end local v2    # "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    .end local v3    # "ansCntntId":I
    .end local v4    # "ansId":I
    .end local v5    # "ansSummCntntId":I
    .end local v6    # "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    .end local v9    # "catId":I
    .end local v13    # "isAnswer":Z
    .end local v17    # "qAnsType":I
    .end local v18    # "qCntntId":I
    .end local v19    # "qGroupCntntId":I
    .end local v20    # "qGrpId":I
    .end local v21    # "qId":I
    .end local v22    # "qImgCntntId":I
    .end local v23    # "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    .end local v24    # "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    .end local v25    # "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .end local v26    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    :catchall_1
    move-exception v34

    move-object/from16 v28, v29

    .end local v29    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .restart local v28    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    goto :goto_4

    .end local v28    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .restart local v6    # "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    .restart local v23    # "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    .restart local v24    # "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    .restart local v25    # "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .restart local v26    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    .restart local v29    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    :cond_f
    move-object/from16 v28, v29

    .end local v6    # "answers":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/Answer;>;"
    .end local v10    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v23    # "qa":Lcom/cigna/coach/dataobjects/QuestionAnswersData;
    .end local v24    # "qaGroup":Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
    .end local v25    # "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;>;"
    .end local v26    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswers;>;"
    .end local v29    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    .restart local v28    # "returnResult":Lcom/cigna/coach/apiobjects/CategoryGroupQAs;
    :cond_10
    if-eqz v27, :cond_11

    .line 247
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    .line 248
    :cond_11
    sget-object v34, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v34 .. v34}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v34

    if-eqz v34, :cond_12

    .line 249
    sget-object v34, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v35, "getLifeStyleAssessmentQA: END"

    invoke-static/range {v34 .. v35}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    :cond_12
    return-object v28

    .line 242
    .end local v11    # "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    .end local v32    # "shouldSuppressAlcoholRelatedContent":Z
    :catch_1
    move-exception v12

    goto :goto_3
.end method

.method public getNextOptimalAnswerForUser(Ljava/lang/String;II)Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "qGrpId"    # I
    .param p3, "currentGoalId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 613
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 614
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getNextOptimalAnswerForUser: START"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Finding the next optimal answer for user"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    :cond_0
    const/4 v8, 0x0

    .line 619
    .local v8, "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 620
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Getting the current answer for q grp id: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", goalId: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    invoke-static {v11, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v10

    .line 623
    .local v10, "userKey":I
    if-gez v10, :cond_2

    .line 624
    new-instance v11, Lcom/cigna/coach/exceptions/CoachException;

    const-string v12, "LifeStyleDBManager:getNextOptimalAnswerForUser error - UserId does not exist"

    invoke-direct {v11, v12}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 626
    :cond_2
    const/4 v11, 0x3

    new-array v2, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v2, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v2, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual {v13}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->getAnswerTypeKey()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v2, v11

    .line 627
    .local v2, "args1":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 629
    .local v4, "c1":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    const-string v12, " SELECT B.RESP_REF FROM USR_LFSTYL_ASSMT_RESP B  INNER JOIN  USR_GOAL A ON B.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID  AND B.USR_ID = A.USR_ID  WHERE B.USR_ID=? AND A.GOAL_ID = ? AND B.ANS_TY_IND = ?  ORDER BY B.ASSMT_UPDT_TS DESC LIMIT 1"

    invoke-virtual {v11, v12, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 631
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    if-eqz v11, :cond_c

    .line 632
    const/4 v11, 0x0

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 633
    .local v6, "currAnswerId":I
    const/4 v11, 0x2

    new-array v3, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v3, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v3, v11
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 634
    .local v3, "args2":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 636
    .local v5, "c2":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    const-string v12, " SELECT LFSTYL_ASSMT_ANS_ID, LFSTYL_ASSMT_QUEST_ID FROM LFSTYL_ASSMT_ANS  WHERE LFSTYL_ASSMT_QUEST_GRP_ID=? AND  LFSTYL_ASSMT_ANS_PRGRS_SEQ_NUM > (   SELECT LFSTYL_ASSMT_ANS_PRGRS_SEQ_NUM FROM LFSTYL_ASSMT_ANS    WHERE LFSTYL_ASSMT_ANS_ID = ? )  ORDER BY LFSTYL_ASSMT_ANS_PRGRS_SEQ_NUM LIMIT 1"

    invoke-virtual {v11, v12, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 638
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 639
    new-instance v9, Lcom/cigna/coach/dataobjects/QuestionAnswerData;

    invoke-direct {v9}, Lcom/cigna/coach/dataobjects/QuestionAnswerData;-><init>()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 640
    .end local v8    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    .local v9, "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    :try_start_2
    new-instance v1, Lcom/cigna/coach/dataobjects/AnswerData;

    invoke-direct {v1}, Lcom/cigna/coach/dataobjects/AnswerData;-><init>()V

    .line 641
    .local v1, "answer":Lcom/cigna/coach/dataobjects/AnswerData;
    const/4 v11, 0x0

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    invoke-virtual {v1, v11}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerId(I)V

    .line 642
    const-string v11, ""

    invoke-virtual {v1, v11}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswer(Ljava/lang/String;)V

    .line 643
    sget-object v11, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual {v1, v11}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerType(Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;)V

    .line 644
    const/4 v11, 0x1

    invoke-interface {v5, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    invoke-virtual {v9, v11}, Lcom/cigna/coach/dataobjects/QuestionAnswerData;->setQuestionId(I)V

    .line 645
    invoke-virtual {v9, v1}, Lcom/cigna/coach/dataobjects/QuestionAnswerData;->setAnswer(Lcom/cigna/coach/apiobjects/Answer;)V

    .line 646
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 647
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Next optimal answer id is "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v9}, Lcom/cigna/coach/dataobjects/QuestionAnswerData;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v13

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerId()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_3
    move-object v8, v9

    .line 658
    .end local v1    # "answer":Lcom/cigna/coach/dataobjects/AnswerData;
    .end local v9    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    .restart local v8    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    :cond_4
    :goto_0
    if-eqz v5, :cond_5

    .line 659
    :try_start_3
    invoke-interface {v5}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 671
    .end local v3    # "args2":[Ljava/lang/String;
    .end local v5    # "c2":Landroid/database/Cursor;
    .end local v6    # "currAnswerId":I
    :cond_5
    :goto_1
    if-eqz v4, :cond_6

    .line 672
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 673
    :cond_6
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 674
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getNextOptimalAnswerForUser: END"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    :cond_7
    return-object v8

    .line 650
    .restart local v3    # "args2":[Ljava/lang/String;
    .restart local v5    # "c2":Landroid/database/Cursor;
    .restart local v6    # "currAnswerId":I
    :cond_8
    :try_start_4
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 651
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Looks like current answer is the most optimal answer."

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 654
    :catch_0
    move-exception v7

    .line 655
    .local v7, "e":Ljava/lang/RuntimeException;
    :goto_2
    :try_start_5
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Error in getNextOptimalAnswerForUser: "

    invoke-static {v11, v12, v7}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 656
    throw v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 658
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v11

    :goto_3
    if-eqz v5, :cond_9

    .line 659
    :try_start_6
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v11
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 667
    .end local v3    # "args2":[Ljava/lang/String;
    .end local v5    # "c2":Landroid/database/Cursor;
    .end local v6    # "currAnswerId":I
    :catch_1
    move-exception v7

    .line 668
    .restart local v7    # "e":Ljava/lang/RuntimeException;
    :try_start_7
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Error in getNextOptimalAnswerForUser: "

    invoke-static {v11, v12, v7}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 669
    throw v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 671
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v11

    if-eqz v4, :cond_a

    .line 672
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 673
    :cond_a
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 674
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getNextOptimalAnswerForUser: END"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    throw v11

    .line 663
    :cond_c
    :try_start_8
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 664
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "No answer match found."

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    .line 658
    .end local v8    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    .restart local v3    # "args2":[Ljava/lang/String;
    .restart local v5    # "c2":Landroid/database/Cursor;
    .restart local v6    # "currAnswerId":I
    .restart local v9    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    :catchall_2
    move-exception v11

    move-object v8, v9

    .end local v9    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    .restart local v8    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    goto :goto_3

    .line 654
    .end local v8    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    .restart local v9    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    :catch_2
    move-exception v7

    move-object v8, v9

    .end local v9    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    .restart local v8    # "returnValue":Lcom/cigna/coach/dataobjects/QuestionAnswerData;
    goto :goto_2
.end method

.method public getNoOfAssessmentScores(Ljava/lang/String;)I
    .locals 14
    .param p1, "defaultUserId"    # Ljava/lang/String;

    .prologue
    .line 444
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getNoOfAssessmentScores: START"

    invoke-static {v0, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Getting number of SCORES..."

    invoke-static {v0, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    :cond_0
    const/4 v9, 0x0

    .line 449
    .local v9, "count":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v11

    .line 450
    .local v11, "userKey":I
    const-string v1, "LFSTYL_ASSMT_SCORE"

    .line 451
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v12, "COUNT(*)"

    aput-object v12, v2, v0

    .line 452
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "USR_ID = ? "

    .line 453
    .local v3, "selection":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v4, v0

    .line 454
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 455
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 456
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 457
    .local v7, "orderBy":Ljava/lang/String;
    const/4 v8, 0x0

    .line 459
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 461
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 462
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 463
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 471
    :cond_1
    if-eqz v8, :cond_2

    .line 472
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_2
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 474
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getNoOfAssessmentScores: END"

    invoke-static {v0, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    :cond_3
    return v9

    .line 466
    :catch_0
    move-exception v10

    .line 467
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "Error in getNoOfAssessmentScores: "

    invoke-static {v0, v12, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 468
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 471
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 472
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 473
    :cond_4
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 474
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getNoOfAssessmentScores: END"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v0
.end method

.method public getNumberOfSubcategoriesPerCategory()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 1080
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1081
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getNumberOfSubcategoriesPerCategory: START"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1084
    .local v6, "subcategoryCounts":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 1085
    .local v1, "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1087
    .end local v1    # "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_1
    const/4 v2, 0x0

    .line 1089
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBQueries;->GET_NUMBER_OF_SUBCATEGORIES_PER_CATEGORY:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1090
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v7

    if-nez v7, :cond_4

    .line 1091
    const/4 v7, 0x0

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {v7}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1090
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1093
    :catch_0
    move-exception v3

    .line 1094
    .local v3, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error in getNumberOfSubcategoriesPerCategory: "

    invoke-static {v7, v8, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1095
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1097
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    if-eqz v2, :cond_2

    .line 1098
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1099
    :cond_2
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1100
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getNumberOfSubcategoriesPerCategory: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v7

    .line 1097
    :cond_4
    if-eqz v2, :cond_5

    .line 1098
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1099
    :cond_5
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1100
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "getNumberOfSubcategoriesPerCategory: END"

    invoke-static {v7, v8}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1103
    :cond_6
    return-object v6
.end method

.method public getPerCategoryBestEverScores(Ljava/lang/String;)Ljava/util/Map;
    .locals 13
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1049
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1050
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getPerCategoryBestEverScores: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1052
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 1054
    .local v7, "userKey":I
    const/4 v2, 0x0

    .line 1056
    .local v2, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBQueries;->PER_CATEGORY_BEST_EVER_SCORES:Ljava/lang/String;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1059
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1060
    .local v6, "scores":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->values()[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v1, v0, v4

    .line 1061
    .local v1, "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    const/4 v8, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1060
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1063
    .end local v1    # "category":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1064
    const/4 v8, 0x0

    invoke-interface {v2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-static {v8}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v8

    const/4 v9, 0x1

    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v6, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1063
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1066
    .end local v0    # "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "scores":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    :catch_0
    move-exception v3

    .line 1067
    .local v3, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in getPerCategoryBestEverScores: "

    invoke-static {v8, v9, v3}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1068
    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1070
    .end local v3    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v2, :cond_2

    .line 1071
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1072
    :cond_2
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1073
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getPerCategoryBestEverScores: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v8

    .line 1070
    .restart local v0    # "arr$":[Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "scores":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/Integer;>;"
    :cond_4
    if-eqz v2, :cond_5

    .line 1071
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1072
    :cond_5
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1073
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getPerCategoryBestEverScores: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    :cond_6
    return-object v6
.end method

.method getUserCount()I
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 321
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 322
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getUserCount: START"

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Getting number of users..."

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    :cond_0
    const/4 v9, 0x0

    .line 327
    .local v9, "count":I
    const-string v1, "USR_INFO"

    .line 328
    .local v1, "table":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "COUNT(*)"

    aput-object v0, v2, v12

    .line 329
    .local v2, "columns":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 330
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 331
    .local v4, "selectionArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 332
    .local v5, "groupBy":Ljava/lang/String;
    const/4 v6, 0x0

    .line 333
    .local v6, "having":Ljava/lang/String;
    const/4 v7, 0x0

    .line 334
    .local v7, "orderBy":Ljava/lang/String;
    const/4 v8, 0x0

    .line 336
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 338
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 339
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 340
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 348
    :cond_1
    if-eqz v8, :cond_2

    .line 349
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_2
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 351
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "getUserCount: END"

    invoke-static {v0, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :cond_3
    return v9

    .line 343
    :catch_0
    move-exception v10

    .line 344
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v0, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "Error in getUserCount: "

    invoke-static {v0, v11, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 345
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 349
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_4
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v11}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 351
    sget-object v11, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v12, "getUserCount: END"

    invoke-static {v11, v12}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v0
.end method

.method public getUserLifeStyleAssesmentResponses(Ljava/lang/String;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/util/Calendar;Z)Ljava/util/List;
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p3, "asOfDateTime"    # Ljava/util/Calendar;
    .param p4, "onlyUserResponse"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            "Ljava/util/Calendar;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;",
            ">;"
        }
    .end annotation

    .prologue
    .line 811
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 812
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserLifeStyleAssesmentResponses: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 816
    .local v5, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " WHERE CTGRY_ID = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 817
    .local v7, "whereClauseToUse":Ljava/lang/String;
    new-instance v1, Lcom/cigna/coach/db/CountryExceptionsDBManager;

    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/cigna/coach/db/CountryExceptionsDBManager;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 818
    .local v1, "countryExceptionsDBManager":Lcom/cigna/coach/db/CountryExceptionsDBManager;
    invoke-virtual {v1, p1}, Lcom/cigna/coach/db/CountryExceptionsDBManager;->shouldSuppressAlcoholRelatedContent(Ljava/lang/String;)Z

    move-result v6

    .line 820
    .local v6, "shouldSuppressAlcoholRelatedContent":Z
    if-eqz v6, :cond_2

    .line 821
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 822
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getLifeStyleAssessmentQA: suppressing alcohol related content"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " AND "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "SUB_CTGRY_ID"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " != "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0xcd

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 828
    :cond_2
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 829
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Finding question groups for category:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 831
    :cond_3
    const/4 v0, 0x0

    .line 838
    .local v0, "c1":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " SELECT LFSTYL_ASSMT_QUEST_GRP_ID FROM LFSTYL_ASSMT_QUEST_GRP "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 841
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 843
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_6

    .line 844
    const/4 v8, 0x0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 845
    .local v3, "qGrpId":I
    invoke-virtual {p0, p1, v3, p3, p4}, Lcom/cigna/coach/db/LifeStyleDBManager;->getUserResponseForQuestionGroup(Ljava/lang/String;ILjava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    move-result-object v4

    .line 846
    .local v4, "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 847
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 849
    .end local v3    # "qGrpId":I
    .end local v4    # "qaGrp":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :catch_0
    move-exception v2

    .line 850
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in getUserLifeStyleAssesmentResponses: "

    invoke-static {v8, v9, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 851
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 853
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_4

    .line 854
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 855
    :cond_4
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 856
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getUserLifeStyleAssesmentResponses: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v8

    .line 853
    :cond_6
    if-eqz v0, :cond_7

    .line 854
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 855
    :cond_7
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 856
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserLifeStyleAssesmentResponses: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 859
    :cond_8
    return-object v5
.end method

.method public getUserLifeStyleOverallAndCurrentScoresList(Ljava/lang/String;)Ljava/util/List;
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1201
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1202
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserLifeStyleOverallAndCurrentScoresList: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1206
    .local v3, "lifeStyleScoresList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 1208
    .local v7, "userKey":I
    const/4 v8, -0x1

    if-ne v7, v8, :cond_2

    .line 1209
    const/4 v3, 0x0

    .line 1253
    .end local v3    # "lifeStyleScoresList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    :cond_1
    :goto_0
    return-object v3

    .line 1211
    .restart local v3    # "lifeStyleScoresList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    :cond_2
    const/4 v0, 0x0

    .line 1213
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, " SELECT LFSTYL_ASSMT_TIMESTMP, SCORE_TY_IND, SCORE_VAL FROM  LFSTYL_ASSMT_SCORE WHERE USR_ID = ? AND (SCORE_TY_IND = ?  OR SCORE_TY_IND= ?  ) ORDER BY LFSTYL_ASSMT_TIMESTMP ASC"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1215
    if-eqz v0, :cond_6

    .line 1216
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1217
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_6

    .line 1219
    new-instance v2, Lcom/cigna/coach/dataobjects/LifeStyleScoreData;

    invoke-direct {v2}, Lcom/cigna/coach/dataobjects/LifeStyleScoreData;-><init>()V

    .line 1221
    .local v2, "lifeStyleScore":Lcom/cigna/coach/dataobjects/LifeStyleScoreData;
    const-string v8, "LFSTYL_ASSMT_TIMESTMP"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 1222
    .local v5, "timeInMillisec":J
    const-string v8, "SCORE_TY_IND"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 1224
    .local v4, "scoreTypeInt":I
    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    .line 1225
    invoke-static {v5, v6}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/cigna/coach/dataobjects/LifeStyleScoreData;->setScoreAssignmentDate(Ljava/util/Calendar;)V

    .line 1226
    const-string v8, "SCORE_VAL"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v2, v8}, Lcom/cigna/coach/dataobjects/LifeStyleScoreData;->setScore(I)V

    .line 1227
    invoke-static {v4}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getInstance(I)Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/cigna/coach/dataobjects/LifeStyleScoreData;->setScoreType(Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;)V

    .line 1228
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1230
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1233
    .end local v2    # "lifeStyleScore":Lcom/cigna/coach/dataobjects/LifeStyleScoreData;
    .end local v4    # "scoreTypeInt":I
    .end local v5    # "timeInMillisec":J
    :catch_0
    move-exception v1

    .line 1234
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error getLifeStyleScoreHistory..."

    invoke-static {v8, v9, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1235
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_4

    .line 1238
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1239
    :cond_4
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1240
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getUserLifeStyleOverallAndCurrentScoresList: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    throw v8

    .line 1237
    :cond_6
    if-eqz v0, :cond_7

    .line 1238
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1239
    :cond_7
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1240
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserLifeStyleOverallAndCurrentScoresList: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getUserOverallScore(Ljava/lang/String;Ljava/util/Calendar;)Lcom/cigna/coach/dataobjects/ScoresData;
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "dateTime"    # Ljava/util/Calendar;

    .prologue
    .line 481
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 482
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserOverallScore: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Preparing to run query to get overall user score "

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    :cond_0
    new-instance v6, Lcom/cigna/coach/dataobjects/ScoresData;

    invoke-direct {v6}, Lcom/cigna/coach/dataobjects/ScoresData;-><init>()V

    .line 486
    .local v6, "returnScore":Lcom/cigna/coach/dataobjects/ScoresData;
    const/4 v8, -0x1

    invoke-virtual {v6, v8}, Lcom/cigna/coach/dataobjects/ScoresData;->setOverallScore(I)V

    .line 488
    if-nez p2, :cond_2

    .line 489
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 490
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "No datetime was provided, defaulting to current time"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :cond_1
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object p2

    .line 495
    :cond_2
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string/jumbo v8, "yyyy-MM-dd HH:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-direct {v3, v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 496
    .local v3, "fmt":Ljava/text/SimpleDateFormat;
    invoke-virtual/range {p2 .. p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 497
    .local v1, "dateFormatted":Ljava/lang/String;
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 498
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Reference time being used is: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 501
    .local v4, "refTimeInMilliSecs":J
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 502
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Reference time in millis: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :cond_4
    const/4 v0, 0x0

    .line 506
    .local v0, "c2":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v7

    .line 507
    .local v7, "userKey":I
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, " SELECT   A.SCORE_VAL, A.LFSTYL_ASSMT_TIMESTMP  FROM LFSTYL_ASSMT_SCORE A WHERE    USR_ID = ? AND    (SCORE_TY_IND = ? OR SCORE_TY_IND = ?) AND    LFSTYL_ASSMT_TIMESTMP <= ? ORDER BY A.LFSTYL_ASSMT_TIMESTMP DESC LIMIT 1"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CURRENT_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v13}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 511
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 512
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v8

    if-nez v8, :cond_8

    .line 513
    const/4 v8, 0x0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-virtual {v6, v8}, Lcom/cigna/coach/dataobjects/ScoresData;->setOverallScore(I)V

    .line 514
    const/4 v8, 0x1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/cigna/coach/dataobjects/ScoresData;->setAssesmentTimestamp(Ljava/util/Calendar;)V

    .line 516
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 517
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Latest score in DB is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lcom/cigna/coach/dataobjects/ScoresData;->getOverallScore()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 528
    :cond_5
    :goto_0
    if-eqz v0, :cond_6

    .line 529
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 530
    :cond_6
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 531
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "getUserOverallScore: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    :cond_7
    return-object v6

    .line 520
    :cond_8
    :try_start_1
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 521
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "No entries for this user before the reference timestamp"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 524
    .end local v7    # "userKey":I
    :catch_0
    move-exception v2

    .line 525
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in getUserOverallScore: "

    invoke-static {v8, v9, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 526
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 528
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    if-eqz v0, :cond_9

    .line 529
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 530
    :cond_9
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 531
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "getUserOverallScore: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    throw v8
.end method

.method public getUserResponseCategoriesForDateTime(Ljava/lang/String;Ljava/util/Calendar;)Ljava/util/List;
    .locals 12
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "dateTime"    # Ljava/util/Calendar;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Calendar;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1361
    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1362
    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getUserResponseCategoriesForDateTime: START"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1364
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1366
    .local v3, "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v4

    .line 1368
    .local v4, "userKey":I
    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    .line 1369
    const/4 v3, 0x0

    .line 1397
    .end local v3    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_1
    :goto_0
    return-object v3

    .line 1371
    .restart local v3    # "returnList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;>;"
    :cond_2
    const/4 v1, 0x0

    .line 1373
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "SELECT DISTINCT B.CTGRY_ID FROM USR_LFSTYL_ASSMT_RESP A INNER JOIN LFSTYL_ASSMT_QUEST_GRP B ON A.LFSTYL_ASSMT_QUEST_GRP_ID = B.LFSTYL_ASSMT_QUEST_GRP_ID AND A.ASSMT_UPDT_TS = ? AND A.USR_ID = ?"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1376
    if-eqz v1, :cond_5

    .line 1377
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1378
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_5

    .line 1380
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1381
    .local v0, "catId":I
    invoke-static {v0}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1383
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1386
    .end local v0    # "catId":I
    :catch_0
    move-exception v2

    .line 1387
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "Error getUserResponseCategoriesForDateTime..."

    invoke-static {v5, v6, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1388
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1390
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    if-eqz v1, :cond_3

    .line 1391
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1392
    :cond_3
    sget-object v6, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v6}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1393
    sget-object v6, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v7, "getUserResponseCategoriesForDateTime: END"

    invoke-static {v6, v7}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    throw v5

    .line 1390
    :cond_5
    if-eqz v1, :cond_6

    .line 1391
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1392
    :cond_6
    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v5}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1393
    sget-object v5, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v6, "getUserResponseCategoriesForDateTime: END"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getUserResponseForQuestionGroup(Ljava/lang/String;ILjava/util/Calendar;Z)Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    .locals 31
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "questionGrpId"    # I
    .param p3, "asOfDateTime"    # Ljava/util/Calendar;
    .param p4, "onlyUserResponse"    # Z

    .prologue
    .line 682
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 683
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "getUserResponseForQuestionGroup: START"

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    :cond_0
    if-nez p3, :cond_2

    .line 687
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_1

    .line 688
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "No datetime was provided, defaulting to current time"

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    :cond_1
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object p3

    .line 693
    :cond_2
    new-instance v15, Ljava/text/SimpleDateFormat;

    const-string/jumbo v25, "yyyy-MM-dd HH:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v15, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 694
    .local v15, "fmt":Ljava/text/SimpleDateFormat;
    invoke-virtual/range {p3 .. p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    .line 695
    .local v13, "dateFormatted":Ljava/lang/String;
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 696
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Reference time being used is: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    :cond_3
    invoke-virtual/range {p3 .. p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v22

    .line 699
    .local v22, "refTimeInMilliSecs":J
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 700
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Reference time in secs: "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Getting user key for id:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v24

    .line 705
    .local v24, "userKey":I
    const-string v9, " WHERE A.LFSTYL_ASSMT_QUEST_GRP_ID = ? AND        A.LFSTYL_ASSMT_QUEST_ID = ? AND\t\tA.USR_ID = ? AND        A.ASSMT_UPDT_TS <= ? "

    .line 707
    .local v9, "answerQueryWhereClause":Ljava/lang/String;
    if-eqz p4, :cond_5

    .line 708
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " AND ANS_SRC = ? "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 710
    :cond_5
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, " SELECT A.ANS_TY_IND, A.RESP_REF, A.RESP_TXT, B.ANS_SMMRY_CNTNT_ID, A.ANS_SRC FROM USR_LFSTYL_ASSMT_RESP A \tLEFT OUTER JOIN LFSTYL_ASSMT_ANS B ON A.RESP_REF = B.LFSTYL_ASSMT_ANS_ID "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " ORDER BY A.ASSMT_UPDT_TS DESC LIMIT 1 "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 714
    .local v8, "answerQuery":Ljava/lang/String;
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 715
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Question grp:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 717
    :cond_6
    new-instance v20, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;

    invoke-direct/range {v20 .. v20}, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;-><init>()V

    .line 718
    .local v20, "qaGrp":Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;
    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;->setQuestionGroupId(I)V

    .line 719
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 720
    .local v21, "qaList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    invoke-virtual/range {v20 .. v21}, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;->setQuestionAnswer(Ljava/util/List;)V

    .line 723
    const/4 v11, 0x0

    .line 725
    .local v11, "c2":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v25

    const-string v26, " SELECT LFSTYL_ASSMT_QUEST_ID FROM LFSTYL_ASSMT_QUEST WHERE LFSTYL_ASSMT_QUEST_GRP_ID = ?"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, ""

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-virtual/range {v25 .. v27}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 728
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 729
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v25

    if-nez v25, :cond_13

    .line 730
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 731
    .local v18, "qId":I
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 732
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Question id:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    :cond_7
    new-instance v19, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    invoke-direct/range {v19 .. v19}, Lcom/cigna/coach/apiobjects/QuestionAnswer;-><init>()V

    .line 736
    .local v19, "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->setQuestionId(I)V

    .line 737
    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 739
    const/4 v10, 0x0

    .line 740
    .local v10, "args":[Ljava/lang/String;
    if-eqz p4, :cond_c

    .line 741
    const/16 v25, 0x5

    move/from16 v0, v25

    new-array v10, v0, [Ljava/lang/String;

    .end local v10    # "args":[Ljava/lang/String;
    const/16 v25, 0x0

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25

    const/16 v25, 0x1

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25

    const/16 v25, 0x2

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25

    const/16 v25, 0x3

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25

    const/16 v25, 0x4

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    sget-object v27, Lcom/cigna/coach/LifeStyle$AnswerSource;->USER:Lcom/cigna/coach/LifeStyle$AnswerSource;

    invoke-virtual/range {v27 .. v27}, Lcom/cigna/coach/LifeStyle$AnswerSource;->getAnswerSourceKey()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 747
    .restart local v10    # "args":[Ljava/lang/String;
    :goto_1
    const/4 v12, 0x0

    .line 749
    .local v12, "c3":Landroid/database/Cursor;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v8, v10}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 750
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v25

    if-eqz v25, :cond_10

    .line 751
    :goto_2
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v25

    if-nez v25, :cond_11

    .line 753
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 754
    .local v17, "qAnsType":I
    sget-object v25, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual/range {v25 .. v25}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->getAnswerTypeKey()I

    move-result v25

    move/from16 v0, v17

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    const/16 v16, 0x1

    .line 755
    .local v16, "isAnswer":Z
    :goto_3
    const/16 v25, 0x1

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 756
    .local v4, "ansId":I
    if-nez v16, :cond_e

    const/16 v25, 0x2

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 757
    .local v7, "ansText":Ljava/lang/String;
    :goto_4
    const/16 v25, 0x3

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 758
    .local v6, "ansSummaryContentId":I
    const/16 v25, 0x4

    move/from16 v0, v25

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 760
    .local v5, "ansSource":I
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 761
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Answer id:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Answer text:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Answer summary content id:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    :cond_8
    new-instance v3, Lcom/cigna/coach/dataobjects/AnswerData;

    invoke-direct {v3}, Lcom/cigna/coach/dataobjects/AnswerData;-><init>()V

    .line 767
    .local v3, "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    invoke-virtual {v3, v4}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerId(I)V

    .line 768
    invoke-virtual {v3, v6}, Lcom/cigna/coach/dataobjects/AnswerData;->setSummaryContentId(I)V

    .line 769
    invoke-static {v5}, Lcom/cigna/coach/LifeStyle$AnswerSource;->getInstance(I)Lcom/cigna/coach/LifeStyle$AnswerSource;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswerSource(Lcom/cigna/coach/LifeStyle$AnswerSource;)V

    .line 771
    if-eqz v16, :cond_f

    .line 772
    const-string v25, ""

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswer(Ljava/lang/String;)V

    .line 776
    :goto_5
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->setAnswer(Lcom/cigna/coach/apiobjects/Answer;)V

    .line 777
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 784
    .end local v3    # "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    .end local v4    # "ansId":I
    .end local v5    # "ansSource":I
    .end local v6    # "ansSummaryContentId":I
    .end local v7    # "ansText":Ljava/lang/String;
    .end local v16    # "isAnswer":Z
    .end local v17    # "qAnsType":I
    :catch_0
    move-exception v14

    .line 785
    .local v14, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "Error in getUserResponseForQuestionGroup: "

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v14}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 786
    throw v14
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 788
    .end local v14    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v25

    if-eqz v12, :cond_9

    .line 789
    :try_start_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v25
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793
    .end local v10    # "args":[Ljava/lang/String;
    .end local v12    # "c3":Landroid/database/Cursor;
    .end local v18    # "qId":I
    .end local v19    # "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    :catch_1
    move-exception v14

    .line 794
    .restart local v14    # "e":Ljava/lang/RuntimeException;
    :try_start_4
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "Error in getUserResponseForQuestionGroup: "

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v0, v1, v14}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 795
    throw v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 797
    .end local v14    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v25

    if-eqz v11, :cond_a

    .line 798
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 799
    :cond_a
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 800
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "getUserResponseForQuestionGroup: END"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    throw v25

    .line 744
    .restart local v10    # "args":[Ljava/lang/String;
    .restart local v18    # "qId":I
    .restart local v19    # "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    :cond_c
    const/16 v25, 0x4

    :try_start_5
    move/from16 v0, v25

    new-array v10, v0, [Ljava/lang/String;

    .end local v10    # "args":[Ljava/lang/String;
    const/16 v25, 0x0

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25

    const/16 v25, 0x1

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25

    const/16 v25, 0x2

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25

    const/16 v25, 0x3

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    aput-object v26, v10, v25
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .restart local v10    # "args":[Ljava/lang/String;
    goto/16 :goto_1

    .line 754
    .restart local v12    # "c3":Landroid/database/Cursor;
    .restart local v17    # "qAnsType":I
    :cond_d
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 756
    .restart local v4    # "ansId":I
    .restart local v16    # "isAnswer":Z
    :cond_e
    :try_start_6
    const-string v7, ""

    goto/16 :goto_4

    .line 774
    .restart local v3    # "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    .restart local v5    # "ansSource":I
    .restart local v6    # "ansSummaryContentId":I
    .restart local v7    # "ansText":Ljava/lang/String;
    :cond_f
    invoke-virtual {v3, v7}, Lcom/cigna/coach/dataobjects/AnswerData;->setAnswer(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 780
    .end local v3    # "ans":Lcom/cigna/coach/dataobjects/AnswerData;
    .end local v4    # "ansId":I
    .end local v5    # "ansSource":I
    .end local v6    # "ansSummaryContentId":I
    .end local v7    # "ansText":Ljava/lang/String;
    .end local v16    # "isAnswer":Z
    .end local v17    # "qAnsType":I
    :cond_10
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 781
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "No answer found for question"

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 788
    :cond_11
    if-eqz v12, :cond_12

    .line 789
    :try_start_7
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 791
    :cond_12
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_0

    .line 797
    .end local v10    # "args":[Ljava/lang/String;
    .end local v12    # "c3":Landroid/database/Cursor;
    .end local v18    # "qId":I
    .end local v19    # "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    :cond_13
    if-eqz v11, :cond_14

    .line 798
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 799
    :cond_14
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v25 .. v25}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v25

    if-eqz v25, :cond_15

    .line 800
    sget-object v25, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v26, "getUserResponseForQuestionGroup: END"

    invoke-static/range {v25 .. v26}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 804
    :cond_15
    return-object v20
.end method

.method public getUserScoreForCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;Ljava/lang/String;Ljava/util/Calendar;)Ljava/lang/Integer;
    .locals 18
    .param p1, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p2, "userId"    # Ljava/lang/String;
    .param p3, "dateTime"    # Ljava/util/Calendar;

    .prologue
    .line 545
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 546
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getUserScoreForCategory: START"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    :cond_0
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 549
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Preparing to run query to get all user score for category:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    :cond_1
    const/4 v9, -0x1

    .line 553
    .local v9, "returnValue":I
    if-nez p3, :cond_3

    .line 554
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 555
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "No datetime was provided, defaulting to current time"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    :cond_2
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object p3

    .line 559
    :cond_3
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string/jumbo v12, "yyyy-MM-dd HH:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v13

    invoke-direct {v5, v12, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 560
    .local v5, "fmt":Ljava/text/SimpleDateFormat;
    invoke-virtual/range {p3 .. p3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 561
    .local v3, "dateFormatted":Ljava/lang/String;
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 562
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Reference time being used is: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :cond_4
    invoke-virtual/range {p3 .. p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 566
    .local v7, "refTimeInMilliSecs":J
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 567
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Reference time in secs: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    :cond_5
    sget-object v12, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v12}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v10

    .line 571
    .local v10, "scoretypeKey":I
    sget-object v12, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {v12}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v6

    .line 572
    .local v6, "goalscoretypeKey":I
    invoke-virtual/range {p1 .. p1}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v2

    .line 574
    .local v2, "categoryKey":I
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 575
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "User Id is: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Score Type is: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Category Type is: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    :cond_6
    const/4 v1, 0x0

    .line 582
    .local v1, "c2":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-static {v12, v0}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v11

    .line 583
    .local v11, "userKey":I
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v12

    const-string v13, " SELECT   A.SCORE_VAL  FROM LFSTYL_ASSMT_SCORE A WHERE    USR_ID = ? AND    (SCORE_TY_IND = ? OR SCORE_TY_IND = ?) AND    SCORE_TY_ID = ? AND    LFSTYL_ASSMT_TIMESTMP <= ? ORDER BY A.LFSTYL_ASSMT_TIMESTMP DESC LIMIT 1"

    const/4 v14, 0x5

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x3

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x4

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, ""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 588
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 589
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v12

    if-nez v12, :cond_a

    .line 590
    const/4 v12, 0x0

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 591
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 592
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Latest score in DB is "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 603
    :cond_7
    :goto_0
    if-eqz v1, :cond_8

    .line 604
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 605
    :cond_8
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 606
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "getUserScoreForCategory: END"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_9
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    return-object v12

    .line 595
    :cond_a
    :try_start_1
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v12}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 596
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "No entries for this category before the reference timestamp"

    invoke-static {v12, v13}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 599
    .end local v11    # "userKey":I
    :catch_0
    move-exception v4

    .line 600
    .local v4, "e":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v12, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v13, "Error in getUserScoreForCategory: "

    invoke-static {v12, v13, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 601
    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 603
    .end local v4    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v12

    if-eqz v1, :cond_b

    .line 604
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 605
    :cond_b
    sget-object v13, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v13}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 606
    sget-object v13, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v14, "getUserScoreForCategory: END"

    invoke-static {v13, v14}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    throw v12
.end method

.method public hasEverImprovedBetweenConsecutiveAssessments(Ljava/lang/String;)Z
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1107
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1108
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "hasEverImprovedBetweenConsecutiveAssessments: START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v6

    .line 1111
    .local v6, "userKey":I
    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    .line 1112
    .local v5, "scoresByAscendingTimestamp":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 1114
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBQueries;->CONSECUTIVE_ASSESSMENT_SCORES:Ljava/lang/String;

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1115
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_4

    .line 1116
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1117
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Score: %d."

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-interface {v1, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    :cond_1
    const/4 v9, 0x0

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1115
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1128
    :catch_0
    move-exception v2

    .line 1129
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v8, "Error in hasEverImprovedBetweenConsecutiveAssessments: "

    invoke-static {v7, v8, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1130
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1132
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    if-eqz v1, :cond_2

    .line 1133
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1134
    :cond_2
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1135
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "hasEverImprovedBetweenConsecutiveAssessments: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v7

    .line 1121
    :cond_4
    const/4 v4, 0x0

    .line 1122
    .local v4, "previous":Ljava/lang/Integer;
    :try_start_2
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1123
    .local v0, "current":Ljava/lang/Integer;
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v10

    if-ge v9, v10, :cond_7

    .line 1132
    if-eqz v1, :cond_5

    .line 1133
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1134
    :cond_5
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1135
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "hasEverImprovedBetweenConsecutiveAssessments: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    .end local v0    # "current":Ljava/lang/Integer;
    :cond_6
    :goto_2
    return v7

    .line 1126
    .restart local v0    # "current":Ljava/lang/Integer;
    :cond_7
    move-object v4, v0

    .line 1127
    goto :goto_1

    .line 1132
    .end local v0    # "current":Ljava/lang/Integer;
    :cond_8
    if-eqz v1, :cond_9

    .line 1133
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1134
    :cond_9
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v7}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1135
    sget-object v7, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "hasEverImprovedBetweenConsecutiveAssessments: END"

    invoke-static {v7, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    move v7, v8

    .line 1138
    goto :goto_2
.end method

.method public hasImprovedScoreRatingByCompletingGoals(Ljava/lang/String;)Z
    .locals 14
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 1142
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1143
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "hasImprovedScoreRatingByCompletingGoals: START"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1145
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    invoke-static {v9, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v8

    .line 1147
    .local v8, "userKey":I
    const/4 v4, 0x0

    .line 1148
    .local v4, "lastCompletedGoalCategory":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    const/4 v3, -0x1

    .line 1149
    .local v3, "goalCompletionCatScore":I
    const/4 v5, -0x1

    .line 1151
    .local v5, "lifeStyleCatScore":I
    const/4 v1, 0x0

    .line 1153
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBQueries;->CONSECUTIVE_LIFESTYLE_SCORES:Ljava/lang/String;

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1155
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v9

    if-nez v9, :cond_5

    .line 1156
    const-string v9, "SCORE_VAL"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1157
    .local v6, "score":I
    const-string v9, "SCORE_TY_IND"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getInstance(I)Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-result-object v7

    .line 1159
    .local v7, "scoreType":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    const-string v9, "SCORE_TY_ID"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getInstance(I)Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    move-result-object v0

    .line 1162
    .local v0, "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    sget-object v9, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    if-ne v7, v9, :cond_4

    if-nez v4, :cond_4

    .line 1163
    move-object v4, v0

    .line 1164
    move v3, v6

    .line 1155
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1186
    .end local v0    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v6    # "score":I
    .end local v7    # "scoreType":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    :catch_0
    move-exception v2

    .line 1187
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "Error in hasImprovedScoreRatingByCompletingGoals: "

    invoke-static {v9, v10, v2}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1188
    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1190
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v9

    if-eqz v1, :cond_2

    .line 1191
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1192
    :cond_2
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1193
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "hasImprovedScoreRatingByCompletingGoals: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v9

    .line 1165
    .restart local v0    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .restart local v6    # "score":I
    .restart local v7    # "scoreType":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    :cond_4
    :try_start_2
    sget-object v9, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    if-ne v7, v9, :cond_1

    if-eqz v4, :cond_1

    .line 1166
    if-ne v4, v0, :cond_1

    const/4 v9, -0x1

    if-eq v3, v9, :cond_1

    .line 1167
    move v5, v6

    .line 1173
    .end local v0    # "categoryType":Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .end local v6    # "score":I
    .end local v7    # "scoreType":Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    :cond_5
    const/4 v9, -0x1

    if-eq v5, v9, :cond_c

    const/4 v9, -0x1

    if-eq v3, v9, :cond_c

    if-le v3, v5, :cond_c

    .line 1174
    invoke-static {v5}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->LOW:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    if-ne v9, v10, :cond_8

    invoke-static {v3}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->MEDIUM:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-ne v9, v10, :cond_8

    .line 1176
    const/4 v9, 0x1

    .line 1190
    if-eqz v1, :cond_6

    .line 1191
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1192
    :cond_6
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1193
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "hasImprovedScoreRatingByCompletingGoals: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    :cond_7
    :goto_1
    return v9

    .line 1177
    :cond_8
    :try_start_3
    invoke-static {v5}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->MEDIUM:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    if-ne v9, v10, :cond_a

    invoke-static {v3}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-ne v9, v10, :cond_a

    .line 1179
    const/4 v9, 0x1

    .line 1190
    if-eqz v1, :cond_9

    .line 1191
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1192
    :cond_9
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1193
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "hasImprovedScoreRatingByCompletingGoals: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1180
    :cond_a
    :try_start_4
    invoke-static {v5}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->LOW:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    if-ne v9, v10, :cond_c

    invoke-static {v3}, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->getRatingForScore(I)Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;

    move-result-object v9

    sget-object v10, Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;->HIGH:Lcom/cigna/coach/utils/ScoringHelper$ScoreRating;
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-ne v9, v10, :cond_c

    .line 1182
    const/4 v9, 0x1

    .line 1190
    if-eqz v1, :cond_b

    .line 1191
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1192
    :cond_b
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v10}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1193
    sget-object v10, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v11, "hasImprovedScoreRatingByCompletingGoals: END"

    invoke-static {v10, v11}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1190
    :cond_c
    if-eqz v1, :cond_d

    .line 1191
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1192
    :cond_d
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1193
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v10, "hasImprovedScoreRatingByCompletingGoals: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1196
    :cond_e
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public setLifeStyleAssesmentAnswers(Ljava/lang/String;Ljava/util/List;Lcom/cigna/coach/LifeStyle$AnswerSource;)V
    .locals 29
    .param p1, "userId"    # Ljava/lang/String;
    .param p3, "answerSource"    # Lcom/cigna/coach/LifeStyle$AnswerSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;",
            ">;",
            "Lcom/cigna/coach/LifeStyle$AnswerSource;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 864
    .local p2, "qaGroups":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;>;"
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_0

    .line 865
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v27, "setLifeStyleAssesmentAnswers: START"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "Preparing to run query to save user responses to db"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    :cond_0
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v8

    .line 869
    .local v8, "c":Ljava/util/Calendar;
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string/jumbo v26, "yyyy-MM-dd HH:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-direct {v11, v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 870
    .local v11, "fmt":Ljava/text/SimpleDateFormat;
    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v11, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    .line 871
    .local v9, "dateFormatted":Ljava/lang/String;
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_1

    .line 872
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Reference time being used is: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    :cond_1
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v22

    .line 876
    .local v22, "updtTimeStampInMilliSecs":J
    if-eqz p2, :cond_15

    :try_start_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v26

    if-lez v26, :cond_15

    .line 877
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_2

    .line 878
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Processing "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " question groups"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I

    move-result v24

    .line 882
    .local v24, "userKey":I
    const/16 v26, -0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-ne v0, v1, :cond_4

    .line 884
    new-instance v26, Lcom/cigna/coach/exceptions/CoachException;

    const-string v27, "LifeStyleDBManager:setLifeStyleAssesmentAnswers error - UserId does not exist"

    invoke-direct/range {v26 .. v27}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v26
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 967
    .end local v24    # "userKey":I
    :catch_0
    move-exception v10

    .line 968
    .local v10, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "Error in setLifeStyleAssesmentAnswers: "

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-static {v0, v1, v10}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 969
    throw v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 971
    .end local v10    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v26

    sget-object v27, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v27 .. v27}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v27

    if-eqz v27, :cond_3

    .line 972
    sget-object v27, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v28, "setLifeStyleAssesmentAnswers: END"

    invoke-static/range {v27 .. v28}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    throw v26

    .line 886
    .restart local v24    # "userKey":I
    :cond_4
    const/16 v26, -0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-le v0, v1, :cond_12

    .line 887
    :try_start_2
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 888
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "Preparing to run query to save user responses to db"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    :cond_5
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_6
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_13

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;

    .line 891
    .local v17, "qaGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    if-eqz v17, :cond_6

    .line 892
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionGroupId()I

    move-result v14

    .line 893
    .local v14, "qGrpKey":I
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 894
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Question Group ID ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    :cond_7
    const/16 v26, -0x1

    move/from16 v0, v26

    if-le v14, v0, :cond_11

    .line 898
    invoke-virtual/range {v17 .. v17}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;->getQuestionAnswer()Ljava/util/List;

    move-result-object v18

    .line 899
    .local v18, "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    if-eqz v18, :cond_10

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v26

    if-lez v26, :cond_10

    .line 901
    const/16 v19, 0x0

    .line 902
    .local v19, "recordsInserted":I
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/cigna/coach/apiobjects/QuestionAnswer;

    .line 903
    .local v16, "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getQuestionId()I

    move-result v15

    .line 904
    .local v15, "qKey":I
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 905
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Question  ID ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    :cond_9
    invoke-virtual/range {v16 .. v16}, Lcom/cigna/coach/apiobjects/QuestionAnswer;->getAnswer()Lcom/cigna/coach/apiobjects/Answer;

    move-result-object v4

    .line 909
    .local v4, "ans":Lcom/cigna/coach/apiobjects/Answer;
    if-eqz v4, :cond_f

    .line 910
    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerId()I

    move-result v5

    .line 911
    .local v5, "ansKey":I
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 912
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Answer  ID ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    :cond_a
    const-string v6, ""

    .line 916
    .local v6, "ansText":Ljava/lang/String;
    const/4 v7, -0x1

    .line 918
    .local v7, "ansType":I
    sget-object v26, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 919
    sget-object v26, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->REFERENCE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual/range {v26 .. v26}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->getAnswerTypeKey()I

    move-result v7

    .line 926
    :goto_2
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 927
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Answer  Text ="

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 930
    :cond_b
    new-instance v25, Landroid/content/ContentValues;

    invoke-direct/range {v25 .. v25}, Landroid/content/ContentValues;-><init>()V

    .line 931
    .local v25, "values":Landroid/content/ContentValues;
    const-string v26, "LFSTYL_ASSMT_QUEST_GRP_ID"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 932
    const-string v26, "LFSTYL_ASSMT_QUEST_ID"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 933
    const-string v26, "USR_ID"

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 934
    const-string v26, "ASSMT_UPDT_TS"

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 935
    const-string v26, "ANS_SRC"

    invoke-virtual/range {p3 .. p3}, Lcom/cigna/coach/LifeStyle$AnswerSource;->getAnswerSourceKey()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 936
    const-string v26, "ANS_TY_IND"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 937
    const-string v26, "RESP_REF"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 938
    const-string v26, "RESP_TXT"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    invoke-virtual/range {p0 .. p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v26

    const-string v27, "USR_LFSTYL_ASSMT_RESP"

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, v25

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v20

    .line 941
    .local v20, "rowId":J
    const-wide/16 v26, -0x1

    cmp-long v26, v20, v26

    if-nez v26, :cond_e

    .line 942
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "Error: duplicate keys - can not load answer"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    new-instance v26, Lcom/cigna/coach/exceptions/CoachException;

    const-string v27, "Duplicate rows submitted on request"

    invoke-direct/range {v26 .. v27}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 920
    .end local v20    # "rowId":J
    .end local v25    # "values":Landroid/content/ContentValues;
    :cond_c
    sget-object v26, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Answer;->getAnswerType()Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 921
    invoke-virtual {v4}, Lcom/cigna/coach/apiobjects/Answer;->getAnswer()Ljava/lang/String;

    move-result-object v6

    .line 922
    sget-object v26, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->VALUE:Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;

    invoke-virtual/range {v26 .. v26}, Lcom/cigna/coach/interfaces/ILifeStyle$AnswerType;->getAnswerTypeKey()I

    move-result v7

    goto/16 :goto_2

    .line 924
    :cond_d
    new-instance v26, Lcom/cigna/coach/exceptions/CoachException;

    const-string v27, "LifeStyleDBManager:setLifeStyleAssesmentAnswers error - AnswerType is not valid"

    invoke-direct/range {v26 .. v27}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 945
    .restart local v20    # "rowId":J
    .restart local v25    # "values":Landroid/content/ContentValues;
    :cond_e
    add-int/lit8 v19, v19, 0x1

    .line 946
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 947
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "Inserting row: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 950
    .end local v5    # "ansKey":I
    .end local v6    # "ansText":Ljava/lang/String;
    .end local v7    # "ansType":I
    .end local v20    # "rowId":J
    .end local v25    # "values":Landroid/content/ContentValues;
    :cond_f
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "Error: received null answer"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 954
    .end local v4    # "ans":Lcom/cigna/coach/apiobjects/Answer;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v15    # "qKey":I
    .end local v16    # "qa":Lcom/cigna/coach/apiobjects/QuestionAnswer;
    .end local v19    # "recordsInserted":I
    :cond_10
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "Error: received a null question answer list"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 957
    .end local v18    # "qas":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/QuestionAnswer;>;"
    :cond_11
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "Error: received a negative question group"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 962
    .end local v14    # "qGrpKey":I
    .end local v17    # "qaGroup":Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
    :cond_12
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "User key could not be found"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 971
    .end local v24    # "userKey":I
    :cond_13
    :goto_3
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v26 .. v26}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_14

    .line 972
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v27, "setLifeStyleAssesmentAnswers: END"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    :cond_14
    return-void

    .line 965
    :cond_15
    :try_start_3
    sget-object v26, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v27, "The user response is empty"

    invoke-static/range {v26 .. v27}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3
.end method

.method public setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;I)V
    .locals 6
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "scoreType"    # Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    .param p3, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p4, "score"    # I

    .prologue
    .line 980
    invoke-static {}, Lcom/cigna/coach/utils/CalendarUtil;->createTodayCalendar()Ljava/util/Calendar;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/cigna/coach/db/LifeStyleDBManager;->setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;ILjava/util/Calendar;)V

    .line 981
    return-void
.end method

.method public setLifeStyleAssesmentScore(Ljava/lang/String;Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;ILjava/util/Calendar;)V
    .locals 11
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "scoreType"    # Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;
    .param p3, "category"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .param p4, "score"    # I
    .param p5, "scoreDateTime"    # Ljava/util/Calendar;

    .prologue
    .line 985
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 986
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v9, "setLifeStyleAssesmentScore: START"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 987
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Preparing to run query to save user score to db"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v8, "yyyy-MM-dd HH:mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-direct {v2, v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 991
    .local v2, "fmt":Ljava/text/SimpleDateFormat;
    invoke-virtual/range {p5 .. p5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 992
    .local v0, "dateFormatted":Ljava/lang/String;
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 993
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Reference time being used is: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    :cond_1
    invoke-virtual/range {p5 .. p5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    .line 997
    .local v4, "updtTimeStampInMilliSecs":J
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 998
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Getting user key"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, p1}, Lcom/cigna/coach/utils/UserIdUtils;->getUserId(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    .line 1003
    .local v6, "userKey":I
    const/4 v8, -0x1

    if-ne v6, v8, :cond_4

    .line 1037
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1038
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v9, "setLifeStyleAssesmentScore: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    :cond_3
    :goto_0
    return-void

    .line 1007
    :cond_4
    const/4 v8, -0x1

    if-le v6, v8, :cond_9

    .line 1008
    const/4 v3, -0x1

    .line 1009
    .local v3, "scoreTypeId":I
    :try_start_1
    sget-object v8, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_LIFESTYLE:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {p2, v8}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    sget-object v8, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->CATEGORY_CURRENT:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    invoke-virtual {p2, v8}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1010
    :cond_5
    if-eqz p3, :cond_6

    .line 1011
    invoke-virtual {p3}, Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;->getCategoryTypeKey()I

    move-result v3

    .line 1014
    :cond_6
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1015
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "SCORE_TY_IND"

    invoke-virtual {p2}, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->getScoreType()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1016
    const-string v8, "SCORE_TY_ID"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1017
    const-string v8, "USR_ID"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1018
    const-string v8, "LFSTYL_ASSMT_TIMESTMP"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1019
    const-string v8, "SCORE_VAL"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1020
    const-string v8, "LFSTYL_ASSMT_SCORE_ID"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1021
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1022
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Inserting record"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    :cond_7
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    const-string v9, "LFSTYL_ASSMT_SCORE"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1025
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1026
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Inserting completed"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    :cond_8
    invoke-virtual {p0}, Lcom/cigna/coach/db/LifeStyleDBManager;->getHelper()Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/cigna/coach/db/CoachSQLiteHelper;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->getInstance(Landroid/content/Context;)Lcom/cigna/mobile/coach/notification/BroadcastManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/cigna/mobile/coach/notification/BroadcastManager;->sendInvalidateCoachWidget()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1037
    .end local v3    # "scoreTypeId":I
    .end local v7    # "values":Landroid/content/ContentValues;
    :goto_1
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v8}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1038
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v9, "setLifeStyleAssesmentScore: END"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1031
    :cond_9
    :try_start_2
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "User key could not be found"

    invoke-static {v8, v9}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1033
    .end local v6    # "userKey":I
    :catch_0
    move-exception v1

    .line 1034
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_3
    sget-object v8, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string v9, "Error in setLifeStyleAssesmentScore: "

    invoke-static {v8, v9, v1}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1035
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1037
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v8

    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v9}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1038
    sget-object v9, Lcom/cigna/coach/db/LifeStyleDBManager;->CLASS_NAME:Ljava/lang/String;

    const-string/jumbo v10, "setLifeStyleAssesmentScore: END"

    invoke-static {v9, v10}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    throw v8
.end method
