.class public Lcom/cigna/coach/db/GoalDBQueries;
.super Ljava/lang/Object;
.source "GoalDBQueries.java"


# static fields
.field static final ACTIVE_USER_GOAL_SEQUENCE_ID:Ljava/lang/String;

.field static final ALL_USER_GOAL_DATA_OBJECT_COLUMNS_IN_JOIN:Ljava/lang/String; = " A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID"

.field static final ALL_USER_MISSION_DATA_OBJECT_COLUMNS_IN_JOIN:Ljava/lang/String; = " A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM"

.field static final AVAILABLE_GOAL_COUNT:Ljava/lang/String;

.field static final AVAILABLE_GOAL_SINGLE_CATEGORY_COUNT:Ljava/lang/String;

.field static final COMPLETED_GOAL_COUNT:Ljava/lang/String;

.field static final COMPLETED_GOAL_DISTINCT_CATEGORY_COUNT:Ljava/lang/String;

.field static final COMPLETED_GOAL_SINGLE_CATEGORY_COUNT:Ljava/lang/String;

.field static final COMPLETED_GOAL_SINGLE_SUBCATEGORY_COUNT:Ljava/lang/String;

.field static final GET_COMPLETED_GOAL_COUNT_PER_CATEGORY:Ljava/lang/String;

.field static final GetAllActiveUserGoalsAndMissions_SELECT_ACTIVE_USER_GOALS_QUERY:Ljava/lang/String; = "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND = ? ORDER BY A.START_DT"

.field static final GetAllActiveUserGoalsAndMissions_SELECT_MISSIONS_FOR_GOAL_QUERY_COMPLETED_FALSE:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.GOAL_SEQ_ID = ?  AND A.STAT_CD = ? ORDER BY A.MSSN_START_DT"

.field static final GetAllActiveUserGoalsAndMissions_SELECT_MISSIONS_FOR_GOAL_QUERY_COMPLETED_TRUE:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.USR_ID = ? AND A.GOAL_SEQ_ID = ?  AND (A.STAT_CD = ?  OR (A.STAT_CD = ? AND MSSN_CMPLT_IND = ? )) ORDER BY A.MSSN_START_DT"

.field static final GetAllActiveUserGoalsMissions_SELECT_ACTIVE_USER_GOALS_BEHIND14_QUERY:Ljava/lang/String; = "SELECT  (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A  INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?,?)  AND B.MSSN_END_DT >= ? "

.field static final GetAllActiveUserGoalsMissions_SELECT_ACTIVE_USER_GOALS_BEHIND21_QUERY:Ljava/lang/String; = "SELECT (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  INNER JOIN MSSN C ON (C.MSSN_ID = B.MSSN_ID)  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?,?)  AND (A.START_DT < ?)  AND C.MSSN_STG_ID = ? "

.field static final GetAllActiveUserGoalsMissions_SELECT_ACTIVE_USER_GOALS_BEHIND7_QUERY:Ljava/lang/String; = "SELECT  (COUNT(*) = 0) AS BEHIND FROM USR_GOAL A INNER JOIN USR_MSSN B ON ((A.GOAL_ID = B.GOAL_ID) AND (A.SEQ_ID = B.GOAL_SEQ_ID) AND (A.USR_ID = B.USR_ID))  WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.SEQ_ID = ? AND B.STAT_CD IN (?, ?)  AND (A.START_DT < ?) "

.field static final GetAllActiveUserGoalsMissions_USER_IS_GOAL_OLDER_THAN:Ljava/lang/String; = "SELECT  COUNT(*) > 0 AS RESULT FROM  USR_GOAL A  WHERE A.USR_ID = ?  AND A.GOAL_ID = ?  AND A.SEQ_ID = ?  AND A.START_DT < ?"

.field static final GetAvailableUserGoalsByCategory_SELECT_AVAILABLE_USER_GOALS_QUERY:Ljava/lang/String; = "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND IN (?,?)  AND C.CTGRY_ID = ?  ORDER BY A.STAT_IND, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, E.RANK_NUM, D.RANK_NUM"

.field static final GetAvailableUserGoals_SELECT_AVAILABLE_USER_GOALS_QUERY:Ljava/lang/String; = "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID INNER JOIN SUB_CTGRY D On D.SUB_CTGRY_ID = C.SUB_CTGRY_ID INNER JOIN CTGRY E On E.CTGRY_ID = D.CTGRY_ID WHERE A.USR_ID = ? AND A.STAT_IND IN (?,?)  ORDER BY A.STAT_IND, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, E.RANK_NUM, D.RANK_NUM"

.field static final GetLastAssesmentTimeForCategory_SELECT_QUERY:Ljava/lang/String; = "SELECT  B.ASSMT_UPDT_TS  FROM USR_GOAL A    INNER JOIN USR_LFSTYL_ASSMT_RESP B ON A.LFSTYL_ASSMT_QUEST_GRP_ID = B.LFSTYL_ASSMT_QUEST_GRP_ID \t\tAND A.USR_ID = B.USR_ID AND B.ANS_SRC = ?  WHERE A.GOAL_ID = ? AND A. USR_ID = ?  ORDER BY B.ASSMT_UPDT_TS DESC LIMIT 1"

.field static final GetUnalignedUserGoalForQuestionGroup_SELECT_QUERY_FOR_USER_GOAL:Ljava/lang/String; = " SELECT GOAL_ID,LFSTYL_ASSMT_QUEST_GRP_SCORE FROM USR_GOAL  WHERE LFSTYL_ASSMT_QUEST_GRP_ID=? AND USR_ID = ? AND STAT_IND IN (?,?,?)   "

.field static final GetUserGoalDetails_SELECT_ALL_USER_MISSIONS_QUERY:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.GOAL_ID = ? AND A.USR_ID = ? AND A.GOAL_SEQ_ID = ? "

.field static final QUERY_USER_MISSION_DATA_OBJECT_COLUMNS_IN_JOIN:Ljava/lang/String; = " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID"

.field static final SELECT_ACTIVE_USER_GOAL:Ljava/lang/String; = "SELECT  A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID  FROM  USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID  INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID WHERE A.USR_ID = ? AND A.GOAL_ID = ? AND A.STAT_IND = ?"

.field static final SELECT_ALL_USER_GOALS_HISTORY:Ljava/lang/String; = "SELECT A.GOAL_ID, A.SEQ_ID, A.USR_ID, A.LFSTYL_ASSMT_QUEST_GRP_ID, A.LFSTYL_ASSMT_QUEST_GRP_SCORE, A.LFSTYL_ASSMT_CTRGY_SCORE, A.START_DT, A.END_DT, A.STAT_IND, A.CMPLTD_MSSN_CNT, A.LAST_MSSN_CNTD_DT, B.GOAL_CNTNT_ID, B.GOAL_FREQ_CNTNT_ID, B.GOAL_WHY_CNTNT_ID, B.GOAL_IMG_CNTNT_ID, B.MSSN_TO_CMPLT_NUM, B.MIN_DAYS_BTWN_MSSN_NUM, B.MAX_DAYS_BTWN_MSSN_NUM, B.MIN_MSSN_FREQ_NUM, B.MAX_DAY_GRACE_PER_NUM, B.ACTV_GOAL_IND, B.DISPL_ORDR_NUM, C.CTGRY_ID FROM USR_GOAL A INNER JOIN GOAL B ON A.GOAL_ID = B.GOAL_ID INNER JOIN LFSTYL_ASSMT_QUEST_GRP C ON C.LFSTYL_ASSMT_QUEST_GRP_ID = A.LFSTYL_ASSMT_QUEST_GRP_ID WHERE A.USR_ID = ? AND A.STAT_IND NOT IN  (?,?,?)  ORDER BY A.END_DT DESC"

.field public static final SELECT_COMPLETED_GOALS_BETWEEN_DATES:Ljava/lang/String; = "SELECT COUNT(*) FROM USR_GOAL WHERE USR_ID = ?  AND STAT_IND = ?  AND END_DT >= ?  AND END_DT <= ? "

.field public static final SELECT_COMPLETED_MISSIONS_BETWEEN_DATES:Ljava/lang/String; = "SELECT COUNT(*) FROM USR_MSSN WHERE USR_ID = ?  AND STAT_CD = ?  AND MSSN_END_DT >= ?  AND MSSN_END_DT <= ? "

.field static final SELECT_COMPLETED_USER_MISSIONS_QUERY:Ljava/lang/String;

.field static final SELECT_USER_GOAL_STATUS:Ljava/lang/String; = "SELECT STAT_IND  FROM  USR_GOAL  WHERE USR_ID = ? AND GOAL_ID = ?  ORDER BY SEQ_ID DESC LIMIT 1 "

.field static final SELECT_USER_GOAL_STATUS_USING_SEQ_ID:Ljava/lang/String; = "SELECT STAT_IND  FROM  USR_GOAL  WHERE SEQ_ID = ? "

.field static final SUBCATEGORIES_WITH_COMPLETED_GOAL_COUNT_PER_CATEGORY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " SELECT  A.GOAL_ID, A.GOAL_SEQ_ID, A.MSSN_CMPLT_IND, A.MSSN_END_DT, A.MSSN_ID, A.MSSN_SEQ_ID, A.MSSN_START_DT, A.USR_MSSN_FREQ, A.STAT_CD, A.USR_ID, A.FAILED_REASN_CD, B.ACTV_MSSN_IND, B.DEPNDNT_MSSN_ID, B.DLY_MAX_NUM, B.DLY_TRGT_NUM, B.MSSN_CNTNT_ID, B.MSSN_DESC_CNTNT_ID, B.MSSN_IMG_CNTNT_ID, B.MSSN_SPAN_DAY_NUM, B.MSSN_STG_ID, B.MSSN_WHAT_CNTNT_ID, B.MSSN_WHY_CNTNT_ID, B.PRIM_CTGRY_ID, B.PRIM_SUB_CTGRY_ID, B.SPCF_TRCKER_ID, B.TRCKER_RULE_ID, B.UNT_NUM, B.MSSN_FREQ_CNTNT_ID, C.USR_ID_TXT, D.TRCKER_ID, D.TRCKER_CNTNT_ID, E.DEFLT_FREQ_IND, E.FREQ_END_RNG_NUM, E.FREQ_START_RNG_NUM, E.DISPL_ORDR_NUM FROM USR_MSSN A  INNER JOIN MSSN B ON A.MSSN_ID = B.MSSN_ID   INNER JOIN USR_INFO C ON A.USR_ID = C.USR_ID  INNER JOIN MSSN_GOAL E ON A.MSSN_ID = E.MSSN_ID  AND A.GOAL_ID = E.GOAL_ID  LEFT OUTER JOIN TRCKER_RULE_TRCKER D ON B.TRCKER_RULE_ID = D.TRCKER_RULE_ID WHERE A.GOAL_SEQ_ID = ? AND A.STAT_CD IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " , "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    invoke-virtual {v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->getStatusKey()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND A."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "USR_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ?  ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MSSN_END_DT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->SELECT_COMPLETED_USER_MISSIONS_QUERY:Ljava/lang/String;

    .line 244
    const-string v0, "SELECT %s FROM %s WHERE %s = ? AND %s = ? AND %s = %s"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "SEQ_ID"

    aput-object v2, v1, v4

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v5

    const-string v2, "USR_ID"

    aput-object v2, v1, v6

    const-string v2, "GOAL_ID"

    aput-object v2, v1, v7

    const-string v2, "STAT_IND"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->ACTIVE_USER_GOAL_SEQUENCE_ID:Ljava/lang/String;

    .line 254
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s = ? AND %s = %s"

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v4

    const-string v2, "USR_ID"

    aput-object v2, v1, v5

    const-string v2, "STAT_IND"

    aput-object v2, v1, v6

    sget-object v2, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_COUNT:Ljava/lang/String;

    .line 262
    const-string v0, "SELECT COUNT(DISTINCT %s.%s) FROM (%s JOIN %s ON %s.%s = %s.%s) WHERE %s.%s = ? AND %s.%s = %s"

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v4

    const-string v2, "CTGRY_ID"

    aput-object v2, v1, v5

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v6

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v7

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "STAT_IND"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_DISTINCT_CATEGORY_COUNT:Ljava/lang/String;

    .line 279
    const-string v0, "SELECT COUNT(*) FROM (%s JOIN %s ON %s.%s = %s.%s) WHERE %s.%s = ? AND %s.%s = %s AND %s.%s = ?"

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v4

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v5

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v6

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v2, v1, v7

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "STAT_IND"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "CTGRY_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_SINGLE_CATEGORY_COUNT:Ljava/lang/String;

    .line 296
    const-string v0, "SELECT COUNT(*) FROM %s WHERE %s = ? AND %s = %s"

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v4

    const-string v2, "USR_ID"

    aput-object v2, v1, v5

    const-string v2, "STAT_IND"

    aput-object v2, v1, v6

    sget-object v2, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v2}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->AVAILABLE_GOAL_COUNT:Ljava/lang/String;

    .line 304
    const-string v0, "SELECT COUNT(*) FROM (%s JOIN %s ON %s.%s = %s.%s) WHERE %s.%s = ? AND %s.%s = %s AND %s.%s = ?"

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v4

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v5

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v6

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v2, v1, v7

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "STAT_IND"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->AVAILABLE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "CTGRY_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->AVAILABLE_GOAL_SINGLE_CATEGORY_COUNT:Ljava/lang/String;

    .line 321
    const-string v0, "SELECT COUNT(*) FROM (%s JOIN %s ON %s.%s = %s.%s) WHERE %s.%s = ? AND %s.%s = %s AND %s.%s = ?"

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v4

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v5

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v6

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v2, v1, v7

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "USR_GOAL"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "STAT_IND"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "SUB_CTGRY_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->COMPLETED_GOAL_SINGLE_SUBCATEGORY_COUNT:Ljava/lang/String;

    .line 338
    const-string v0, "SELECT %s, COUNT(DISTINCT B.%s) FROM (%s A JOIN %s B ON A.%s = B.%s AND A.%s = ? AND A.%s = %s) GROUP BY B.%s"

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "CTGRY_ID"

    aput-object v2, v1, v4

    const-string v2, "SUB_CTGRY_ID"

    aput-object v2, v1, v5

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v6

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v7

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "STAT_IND"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "CTGRY_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->SUBCATEGORIES_WITH_COMPLETED_GOAL_COUNT_PER_CATEGORY:Ljava/lang/String;

    .line 352
    const-string v0, "SELECT L.%s, COUNT(*) FROM %s UG JOIN %s L ON UG.%s = L.%s AND UG.%s = ? AND UG.%s = %s GROUP BY L.%s"

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "CTGRY_ID"

    aput-object v2, v1, v4

    const-string v2, "USR_GOAL"

    aput-object v2, v1, v5

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP"

    aput-object v2, v1, v6

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v2, v1, v7

    const-string v2, "LFSTYL_ASSMT_QUEST_GRP_ID"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "USR_ID"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "STAT_IND"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    invoke-virtual {v3}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->getStatusKey()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "CTGRY_ID"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/GoalDBQueries;->GET_COMPLETED_GOAL_COUNT_PER_CATEGORY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
