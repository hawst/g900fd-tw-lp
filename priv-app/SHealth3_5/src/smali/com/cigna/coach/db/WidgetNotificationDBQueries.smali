.class public Lcom/cigna/coach/db/WidgetNotificationDBQueries;
.super Ljava/lang/Object;
.source "WidgetNotificationDBQueries.java"


# static fields
.field static final DISMISS_LATEST:Ljava/lang/String; = "DELETE FROM WIDGT_NOTFTN WHERE SEQ_ID IN (SELECT SEQ_ID FROM WIDGT_NOTFTN WHERE WIDGT_NOTFTN.USR_ID = ? AND WIDGT_NOTFTN.WIDGT_TYPE = ? ORDER BY DISPL_RANK_NUM, WIDGT_NOTFTN.SEQ_ID DESC LIMIT 1)"

.field static final GET_LATEST_NOTIFICATION_QUERY:Ljava/lang/String;

.field static final GET_LATEST_NOTIFICATION_QUERY_ONLY_SEQ_ID:Ljava/lang/String; = "SELECT SEQ_ID FROM WIDGT_NOTFTN WHERE WIDGT_NOTFTN.USR_ID = ? AND WIDGT_NOTFTN.WIDGT_TYPE = ? ORDER BY DISPL_RANK_NUM, WIDGT_NOTFTN.SEQ_ID DESC LIMIT 1"

.field static final LATEST_FILTER:Ljava/lang/String; = " WHERE WIDGT_NOTFTN.USR_ID = ? AND WIDGT_NOTFTN.WIDGT_TYPE = ? ORDER BY WIDGT_RANK, WIDGT_NOTFTN.SEQ_ID DESC LIMIT 1"

.field static final LATEST_FILTER_SEQ_ID:Ljava/lang/String; = " WHERE WIDGT_NOTFTN.USR_ID = ? AND WIDGT_NOTFTN.WIDGT_TYPE = ? ORDER BY DISPL_RANK_NUM, WIDGT_NOTFTN.SEQ_ID DESC LIMIT 1"

.field static final WIDGET_NOTIFICATION_COLUMNS:Ljava/lang/String;

.field static final WIDGET_NOTIFICATION_QUERY:Ljava/lang/String;

.field static final WIDGET_NOTIFICATION_QUERY_SEQ_ID:Ljava/lang/String; = "SELECT SEQ_ID FROM WIDGT_NOTFTN"

.field static final WIDGET_RANK_COLUMN:Ljava/lang/String; = "WIDGT_RANK"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " WIDGT_NOTFTN.SEQ_ID, WIDGT_NOTFTN.DISPL_RANK_NUM AS WIDGT_RANK, WIDGT_NOTFTN.WIDGT_TYPE, WIDGT_NOTFTN.MSG_TYPE, WIDGT_NOTFTN.USR_ID, WIDGT_NOTFTN.UNIQ_KEY, WIDGT_NOTFTN.INTNT, COACH_MSG."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", COACH_MSG."

    sget-object v2, Lcom/cigna/coach/db/CoachMessageDBManager;->coachMessageRetriveColumns:[Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/WidgetNotificationDBQueries;->WIDGET_NOTIFICATION_COLUMNS:Ljava/lang/String;

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/db/WidgetNotificationDBQueries;->WIDGET_NOTIFICATION_COLUMNS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "WIDGT_NOTFTN"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " INNER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "COACH_MSG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " USING ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "COACH_MSG_ID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/WidgetNotificationDBQueries;->WIDGET_NOTIFICATION_QUERY:Ljava/lang/String;

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/cigna/coach/db/WidgetNotificationDBQueries;->WIDGET_NOTIFICATION_QUERY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE WIDGT_NOTFTN.USR_ID = ? AND WIDGT_NOTFTN.WIDGT_TYPE = ? ORDER BY WIDGT_RANK, WIDGT_NOTFTN.SEQ_ID DESC LIMIT 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/WidgetNotificationDBQueries;->GET_LATEST_NOTIFICATION_QUERY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
