.class public final Lcom/cigna/coach/db/tabledefs/ContentInfoColumns;
.super Ljava/lang/Object;
.source "ContentInfoColumns.java"


# static fields
.field public static final KEY_ACTV_IND:Ljava/lang/String; = "ACTV_IND"

.field public static final KEY_CNTNT_ID:Ljava/lang/String; = "CNTNT_ID"

.field public static final KEY_CNTNT_KEYWRD:Ljava/lang/String; = "CNTNT_KEYWRD"

.field public static final KEY_CNTNT_TXT:Ljava/lang/String; = "CNTNT_TXT"

.field public static final KEY_CNTRY_CD:Ljava/lang/String; = "CNTRY_CD"

.field public static final KEY_LANG_CD:Ljava/lang/String; = "LANG_CD"

.field public static final KEY_LANG_SPCF_CNTNT_IND:Ljava/lang/String; = "LANG_SPCF_CNTNT_IND"

.field public static final KEY_LOC_TY_IND:Ljava/lang/String; = "LOC_TY_IND"

.field public static final TABLE_NAME:Ljava/lang/String; = "CONTENT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
