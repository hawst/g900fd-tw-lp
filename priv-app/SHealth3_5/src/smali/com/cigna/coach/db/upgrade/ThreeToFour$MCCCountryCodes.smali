.class Lcom/cigna/coach/db/upgrade/ThreeToFour$MCCCountryCodes;
.super Ljava/lang/Object;
.source "ThreeToFour.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/db/upgrade/ThreeToFour;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MCCCountryCodes"
.end annotation


# static fields
.field static final COUNTRY_MCC_CODES_DROP:Ljava/lang/String; = "DROP TABLE IF EXISTS COUNTRY_MCC_CODES"

.field static final COUNTRY_MCC_CODE_CREATE:Ljava/lang/String; = "CREATE TABLE COUNTRY_MCC_CODES (ISO_COUNTRY_CODE TEXT NOT NULL, MCC_CODE INTEGER NOT NULL)"

.field private static final COUNTRY_MCC_CODE_INSERT:Ljava/lang/String; = "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES ("


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addInsertQueriesList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v0, "insertQueryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'US\',310)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'US\',311)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'US\',312)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'US\',316)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'CN\',460)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'KR\',450)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'AE\',424)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'AE\',430)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 319
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'AE\',431)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 320
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'GB\',234)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 321
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'GB\',235)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 322
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'DE\',262)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'JP\',440)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'JP\',441)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'IT\',222)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'FR\',208)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'AU\',505)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'HK\',454)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'SE\',240)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 330
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'BR\',724)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'CA\',302)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'ES\',214)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'ZA\',655)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 334
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'NL\',204)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 335
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'TR\',286)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'IN\',404)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 337
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'IN\',405)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'RU\',250)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'MX\',334)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'MY\',502)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'TW\',466)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 342
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'AR\',722)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'SG\',525)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'TH\',520)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'PA\',714)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'CH\',228)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 347
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'ID\',510)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'AT\',232)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'IL\',425)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 350
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'PH\',515)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 351
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'CO\',732)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 352
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'CL\',730)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'PL\',260)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 354
    const-string v1, "INSERT OR REPLACE INTO COUNTRY_MCC_CODES (ISO_COUNTRY_CODE,MCC_CODE) VALUES (\'NZ\',530)"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 355
    return-object v0
.end method
