.class public Lcom/cigna/coach/db/upgrade/OneToTwo;
.super Ljava/lang/Object;
.source "OneToTwo.java"

# interfaces
.implements Lcom/cigna/coach/db/upgrade/IDBUpgrade;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/db/upgrade/OneToTwo$1;,
        Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;

.field private static final USER_MSSN_DATA_ALTERQUERY_V2:Ljava/lang/String; = "ALTER TABLE USR_MSSN_DATA ADD COLUMN SRC_REF INTEGER DEFAULT 1 NOT NULL"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/cigna/coach/db/upgrade/OneToTwo;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    return-object v0
.end method

.method private addAlterQueriesToList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    const-string v1, "ALTER TABLE USR_MSSN_DATA ADD COLUMN SRC_REF INTEGER DEFAULT 1 NOT NULL"

    .line 65
    .local v1, "userMissionAlterQuery":Ljava/lang/String;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v0, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    return-object v0
.end method

.method private executeAlterTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .locals 6
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 79
    .local p2, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 80
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Preparing to execute the SQL scripts from list"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_0
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 85
    .local v2, "sqlStatement":Ljava/lang/String;
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 86
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Executing the below sql statement:"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "sqlStatement":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Landroid/database/SQLException;
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "An error occured while executing sql statement"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error is:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v3, Lcom/cigna/coach/exceptions/CoachException;

    invoke-direct {v3, v0}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 97
    .end local v0    # "e":Landroid/database/SQLException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 98
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "Sql script execution completed"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    :cond_3
    return-void
.end method

.method private updateContent(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 109
    sget-object v1, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    sget-object v1, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Updating content"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :cond_0
    new-instance v0, Lcom/cigna/coach/utils/CMSHelper;

    invoke-direct {v0, p1}, Lcom/cigna/coach/utils/CMSHelper;-><init>(Landroid/content/Context;)V

    .line 114
    .local v0, "cmsHelper":Lcom/cigna/coach/utils/CMSHelper;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p2}, Lcom/cigna/coach/utils/CMSHelper;->checkAndCopyDefaultZipFilesToLocalPath(ZLandroid/database/sqlite/SQLiteDatabase;)Z

    .line 115
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, p2, v1}, Lcom/cigna/coach/utils/CMSHelper;->updateLanguageContent(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Locale;)V

    .line 117
    sget-object v1, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 118
    sget-object v1, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v2, "Content update complete"

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    :cond_1
    return-void
.end method

.method private updateValuesToNewlyAddedColumns(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 103
    sget-object v0, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    sget-object v0, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v1, "No data updated"

    invoke-static {v0, v1}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    :cond_0
    return-void
.end method


# virtual methods
.method public applyUpgrade(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "database"    # Landroid/database/sqlite/SQLiteDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 34
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "***********************************"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    :cond_0
    invoke-direct {p0}, Lcom/cigna/coach/db/upgrade/OneToTwo;->addAlterQueriesToList()Ljava/util/List;

    move-result-object v1

    .line 39
    .local v1, "queryList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v1}, Lcom/cigna/coach/db/upgrade/OneToTwo;->executeAlterTableSQLScript(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 41
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v3}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 42
    sget-object v3, Lcom/cigna/coach/db/upgrade/OneToTwo;->CLASS_NAME:Ljava/lang/String;

    const-string v4, "***********************************"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_1
    invoke-direct {p0, p2}, Lcom/cigna/coach/db/upgrade/OneToTwo;->updateValuesToNewlyAddedColumns(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/cigna/coach/db/upgrade/OneToTwo;->updateContent(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 49
    new-instance v0, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;-><init>(Lcom/cigna/coach/db/upgrade/OneToTwo$1;)V

    .line 50
    .local v0, "enableJournalling":Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;
    # invokes: Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->addCreateTableNamesToList()Ljava/util/List;
    invoke-static {v0}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->access$100(Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;)Ljava/util/List;

    move-result-object v2

    .line 53
    .local v2, "tableNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1, p2, v2}, Lcom/cigna/coach/utils/backuprestore/JournalHelper;->setJournalTriggers(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 55
    invoke-virtual {v0, p2, v2}, Lcom/cigna/coach/db/upgrade/OneToTwo$EnableJournalling;->startJournalEnableProcess(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 56
    return-void
.end method
