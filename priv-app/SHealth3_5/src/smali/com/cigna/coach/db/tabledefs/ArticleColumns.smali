.class public final Lcom/cigna/coach/db/tabledefs/ArticleColumns;
.super Ljava/lang/Object;
.source "ArticleColumns.java"


# static fields
.field public static final KEY_ACTV_IND:Ljava/lang/String; = "ACTV_IND"

.field public static final KEY_ARTCL_ID:Ljava/lang/String; = "ARTCL_ID"

.field public static final KEY_CNTNT_ID:Ljava/lang/String; = "CNTNT_ID"

.field public static final KEY_DTL_CNTNT_ID:Ljava/lang/String; = "DTL_CNTNT_ID"

.field public static final KEY_IMG_CNTNT_ID:Ljava/lang/String; = "IMG_CNTNT_ID"

.field public static final QUALIFIED_IMG_CNTNT_ID:Ljava/lang/String; = "ARTCL.IMG_CNTNT_ID"

.field public static final QUALIFIED_KEY_ACTV_IND:Ljava/lang/String; = "ARTCL.ACTV_IND"

.field public static final QUALIFIED_KEY_ARTCL_ID:Ljava/lang/String; = "ARTCL.ARTCL_ID"

.field public static final QUALIFIED_KEY_CNTNT_ID:Ljava/lang/String; = "ARTCL.CNTNT_ID"

.field public static final QUALIFIED_KEY_DTL_CNTNT_ID:Ljava/lang/String; = "ARTCL.DTL_CNTNT_ID"

.field public static final TABLE_NAME:Ljava/lang/String; = "ARTCL"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method
