.class public Lcom/cigna/coach/dataobjects/BadgeData;
.super Lcom/cigna/coach/apiobjects/Badge;
.source "BadgeData.java"


# instance fields
.field private backgroundImageContentId:I

.field private contentId:I

.field private descriptionContentId:I

.field private imageContentId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/Badge;-><init>()V

    .line 35
    const/16 v0, 0x125e

    iput v0, p0, Lcom/cigna/coach/dataobjects/BadgeData;->backgroundImageContentId:I

    return-void
.end method


# virtual methods
.method public getBackgroundImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setBadgeBackgroundImage"
    .end annotation

    .prologue
    .line 85
    iget v0, p0, Lcom/cigna/coach/dataobjects/BadgeData;->backgroundImageContentId:I

    return v0
.end method

.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setBadge"
    .end annotation

    .prologue
    .line 50
    iget v0, p0, Lcom/cigna/coach/dataobjects/BadgeData;->contentId:I

    return v0
.end method

.method public getDescriptionContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setBadgeDescription"
    .end annotation

    .prologue
    .line 65
    iget v0, p0, Lcom/cigna/coach/dataobjects/BadgeData;->descriptionContentId:I

    return v0
.end method

.method public getImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setBadgeImage"
    .end annotation

    .prologue
    .line 80
    iget v0, p0, Lcom/cigna/coach/dataobjects/BadgeData;->imageContentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/cigna/coach/dataobjects/BadgeData;->contentId:I

    .line 43
    return-void
.end method

.method public setDescriptionContentId(I)V
    .locals 0
    .param p1, "descriptionContentId"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/cigna/coach/dataobjects/BadgeData;->descriptionContentId:I

    .line 58
    return-void
.end method

.method public setImageContentId(I)V
    .locals 0
    .param p1, "imageContentId"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/cigna/coach/dataobjects/BadgeData;->imageContentId:I

    .line 73
    return-void
.end method
