.class public Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;
.super Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;
.source "QuestionsAnswersGroupData.java"


# instance fields
.field private contentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/QuestionsAnswersGroup;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;->contentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/cigna/coach/dataobjects/QuestionsAnswersGroupData;->contentId:I

    .line 46
    return-void
.end method
