.class public Lcom/cigna/coach/dataobjects/MetExerciseTracker;
.super Ljava/lang/Object;
.source "MetExerciseTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    }
.end annotation


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private dateInMsec:J

.field private distance:F

.field private durationMin:I

.field private exerciseInfoId:I

.field private exerciseType:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

.field private metName:Ljava/lang/String;

.field private metValue:F

.field private runningSteps:I

.field private samsungExerciseId:I

.field private speed:F

.field private totalSteps:I

.field private updownSteps:I

.field private userDeviceId:Ljava/lang/String;

.field private walkingSteps:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method


# virtual methods
.method public getDateInMsec()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->dateInMsec:J

    return-wide v0
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->distance:F

    return v0
.end method

.method public getDurationMin()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->durationMin:I

    return v0
.end method

.method public getExerciseInfoId()I
    .locals 1

    .prologue
    .line 414
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->exerciseInfoId:I

    return v0
.end method

.method public getExerciseType()Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->exerciseType:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    return-object v0
.end method

.method public getMetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->metName:Ljava/lang/String;

    return-object v0
.end method

.method public getMetValue()F
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->metValue:F

    return v0
.end method

.method public getRunningSteps()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->runningSteps:I

    return v0
.end method

.method public getSamsungExerciseId()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->samsungExerciseId:I

    return v0
.end method

.method public getSpeed()F
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->speed:F

    return v0
.end method

.method public getTotalSteps()I
    .locals 1

    .prologue
    .line 316
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->totalSteps:I

    return v0
.end method

.method public getUpdownSteps()I
    .locals 1

    .prologue
    .line 334
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->updownSteps:I

    return v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getWalkingSteps()I
    .locals 1

    .prologue
    .line 352
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->walkingSteps:I

    return v0
.end method

.method public setDateInMsec(J)V
    .locals 0
    .param p1, "dateInMsec"    # J

    .prologue
    .line 155
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->dateInMsec:J

    .line 156
    return-void
.end method

.method public setDistance(F)V
    .locals 0
    .param p1, "distance"    # F

    .prologue
    .line 173
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->distance:F

    .line 174
    return-void
.end method

.method public setDurationMin(I)V
    .locals 0
    .param p1, "durationMin"    # I

    .prologue
    .line 192
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->durationMin:I

    .line 193
    return-void
.end method

.method public setExerciseInfoId(I)V
    .locals 0
    .param p1, "exerciseInfoId"    # I

    .prologue
    .line 423
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->exerciseInfoId:I

    .line 424
    return-void
.end method

.method public setExerciseType(Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)V
    .locals 0
    .param p1, "exerciseType"    # Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->exerciseType:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 212
    return-void
.end method

.method public setMetName(Ljava/lang/String;)V
    .locals 0
    .param p1, "metName"    # Ljava/lang/String;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->metName:Ljava/lang/String;

    .line 250
    return-void
.end method

.method public setMetValue(F)V
    .locals 0
    .param p1, "metValue"    # F

    .prologue
    .line 230
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->metValue:F

    .line 231
    return-void
.end method

.method public setRunningSteps(I)V
    .locals 0
    .param p1, "runningSteps"    # I

    .prologue
    .line 268
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->runningSteps:I

    .line 269
    return-void
.end method

.method public setSamsungExerciseId(I)V
    .locals 0
    .param p1, "samsungExerciseId"    # I

    .prologue
    .line 286
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->samsungExerciseId:I

    .line 287
    return-void
.end method

.method public setSpeed(F)V
    .locals 0
    .param p1, "speed"    # F

    .prologue
    .line 306
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->speed:F

    .line 307
    return-void
.end method

.method public setTotalSteps(I)V
    .locals 0
    .param p1, "totalSteps"    # I

    .prologue
    .line 325
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->totalSteps:I

    .line 326
    return-void
.end method

.method public setUpdownSteps(I)V
    .locals 0
    .param p1, "updownSteps"    # I

    .prologue
    .line 343
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->updownSteps:I

    .line 344
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 379
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->userDeviceId:Ljava/lang/String;

    .line 380
    return-void
.end method

.method public setWalkingSteps(I)V
    .locals 0
    .param p1, "walkingSteps"    # I

    .prologue
    .line 361
    iput p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->walkingSteps:I

    .line 362
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 387
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 388
    .local v0, "gc":Ljava/util/GregorianCalendar;
    iget-wide v2, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->dateInMsec:J

    invoke-virtual {v0, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 389
    const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v0, v2}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 391
    .local v1, "theDate":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MetExerciseTracker = { metName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->metName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", date: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", dateInMsec: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->dateInMsec:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", distance: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->distance:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", durationMin: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->durationMin:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", exerciseType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->exerciseType:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", metValue: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->metValue:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", runningSteps: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->runningSteps:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", samsungExerciseId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->samsungExerciseId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", speed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->speed:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", totalSteps: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->totalSteps:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", updownSteps: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->updownSteps:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", walkingSteps: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->walkingSteps:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", userDeviceId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker;->userDeviceId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
