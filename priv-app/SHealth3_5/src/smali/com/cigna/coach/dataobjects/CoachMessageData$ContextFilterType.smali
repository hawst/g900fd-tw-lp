.class public final enum Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
.super Ljava/lang/Enum;
.source "CoachMessageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/CoachMessageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContextFilterType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum ANY_GOAL_BEHIND:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum ASSESSMENT_NOT_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum CATCH_ALL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum CATEGORY_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum COACH_LAUNCHED_NO_ACTIVITY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum COACH_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum COACH_MESSAGE_MISSION_COUNT_GREATER_THAN_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum COACH_MESSAGE_MISSION_COUNT_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum CURRENT_SCORE_HIGHER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum DAYS_NO_ACTION_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum DEFAULT_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum FORGOT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum GOAL_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum GOAL_MISSIONS_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum GOAL_ON_TRACK:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum MISSION_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NONE_OF_THESE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NOT_ALL_QUESTIONS_ANSWERED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NO_GOALS_LEFT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NO_GOALS_SET:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NO_GOAL_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NO_MISSION_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NO_MISSION_PROGRESS_BEFORE_TIMESPAN_FREQUENCY_LIMIT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum NO_QUESTIONS_ANSWERED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum OVERALL_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum PRE_AND_POST_MISSION_EXPIRY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum RE_ASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum RE_ASSESS_WARNING:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum SIX_MONTH_LIFESTYLE_REASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum TOO_BUSY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum TOO_EASY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum TOO_HARD:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum TOTAL_MISSIONS_ONE_OR_MORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum TOTAL_MISSIONS_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum TRACKER_MISSION_COMPLETED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum UPDATE_YOUR_WEIGHT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum USER_ADDS_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum USER_DELETES_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum USER_DELETES_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum USER_NOT_STARTED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum WEIGHT_GOAL_BEHIND_LOWER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum WEIGHT_GOAL_BEHIND_UPPER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum ZERO_ACTIVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field public static final enum ZERO_ACTIVE_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field contextFilterType:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x6

    const/4 v11, 0x5

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 53
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "CATCH_ALL"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CATCH_ALL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 54
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NO_GOALS_SET"

    invoke-direct {v4, v5, v8, v9}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOALS_SET:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 55
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "ASSESSMENT_NOT_COMPLETE"

    invoke-direct {v4, v5, v9, v10}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ASSESSMENT_NOT_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 57
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "SIX_MONTH_LIFESTYLE_REASSESS"

    invoke-direct {v4, v5, v10, v11}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->SIX_MONTH_LIFESTYLE_REASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 58
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NO_GOAL_PROGRESS"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6, v12}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOAL_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 59
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NO_MISSION_PROGRESS"

    const/4 v6, 0x7

    invoke-direct {v4, v5, v11, v6}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_MISSION_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 60
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NO_MISSION_PROGRESS_BEFORE_TIMESPAN_FREQUENCY_LIMIT"

    const/16 v6, 0x8

    invoke-direct {v4, v5, v12, v6}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_MISSION_PROGRESS_BEFORE_TIMESPAN_FREQUENCY_LIMIT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 61
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "PRE_AND_POST_MISSION_EXPIRY"

    const/4 v6, 0x7

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->PRE_AND_POST_MISSION_EXPIRY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 62
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "TRACKER_MISSION_COMPLETED"

    const/16 v6, 0x8

    const/16 v7, 0xa

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TRACKER_MISSION_COMPLETED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 63
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NO_QUESTIONS_ANSWERED"

    const/16 v6, 0x9

    const/16 v7, 0xb

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_QUESTIONS_ANSWERED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 64
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "COACH_LAUNCHED_NO_ACTIVITY"

    const/16 v6, 0xa

    const/16 v7, 0xc

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_LAUNCHED_NO_ACTIVITY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 65
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NOT_ALL_QUESTIONS_ANSWERED"

    const/16 v6, 0xb

    const/16 v7, 0xd

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NOT_ALL_QUESTIONS_ANSWERED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 66
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "ZERO_ACTIVE_GOALS"

    const/16 v6, 0xc

    const/16 v7, 0xe

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ZERO_ACTIVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 67
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "ZERO_ACTIVE_MISSIONS"

    const/16 v6, 0xd

    const/16 v7, 0xf

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ZERO_ACTIVE_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 71
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "OVERALL_SCORE"

    const/16 v6, 0xe

    const/16 v7, 0x11

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->OVERALL_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 72
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "RE_ASSESS"

    const/16 v6, 0xf

    const/16 v7, 0x12

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 73
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "RE_ASSESS_WARNING"

    const/16 v6, 0x10

    const/16 v7, 0x13

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS_WARNING:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 74
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "BADGE"

    const/16 v6, 0x11

    const/16 v7, 0x14

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 75
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "MISSION_COMPLETE"

    const/16 v6, 0x12

    const/16 v7, 0x15

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->MISSION_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 76
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "COACH_MESSAGE"

    const/16 v6, 0x13

    const/16 v7, 0x16

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 77
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "COACH_MESSAGE_MISSION_COUNT_ZERO"

    const/16 v6, 0x14

    const/16 v7, 0x17

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE_MISSION_COUNT_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 78
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "COACH_MESSAGE_MISSION_COUNT_GREATER_THAN_ZERO"

    const/16 v6, 0x15

    const/16 v7, 0x18

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE_MISSION_COUNT_GREATER_THAN_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 80
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "TOTAL_MISSIONS_ONE_OR_MORE"

    const/16 v6, 0x16

    const/16 v7, 0x19

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOTAL_MISSIONS_ONE_OR_MORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 81
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "ANY_GOAL_BEHIND"

    const/16 v6, 0x17

    const/16 v7, 0x1a

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ANY_GOAL_BEHIND:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 82
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "TOTAL_MISSIONS_ZERO"

    const/16 v6, 0x18

    const/16 v7, 0x1b

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOTAL_MISSIONS_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 83
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NO_GOALS_LEFT"

    const/16 v6, 0x19

    const/16 v7, 0x1c

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOALS_LEFT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 84
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "GOAL_MISSIONS_ZERO"

    const/16 v6, 0x1a

    const/16 v7, 0x1d

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_MISSIONS_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 86
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "DAYS_NO_ACTION_MISSION"

    const/16 v6, 0x1b

    const/16 v7, 0x1f

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->DAYS_NO_ACTION_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 87
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "GOAL_ON_TRACK"

    const/16 v6, 0x1c

    const/16 v7, 0x20

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_ON_TRACK:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 90
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "CANCEL_UNALIGNED_GOALS"

    const/16 v6, 0x1d

    const/16 v7, 0x21

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 91
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "CATEGORY_SCORE"

    const/16 v6, 0x1e

    const/16 v7, 0x22

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CATEGORY_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 94
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "GOAL_COMPLETE"

    const/16 v6, 0x1f

    const/16 v7, 0x23

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 97
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "USER_DELETES_GOAL"

    const/16 v6, 0x20

    const/16 v7, 0x24

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_DELETES_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 98
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "USER_DELETES_MISSION"

    const/16 v6, 0x21

    const/16 v7, 0x25

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_DELETES_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 99
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "USER_ADDS_GOAL"

    const/16 v6, 0x22

    const/16 v7, 0x26

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_ADDS_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 100
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "FORGOT"

    const/16 v6, 0x23

    const/16 v7, 0x27

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->FORGOT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 101
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "TOO_HARD"

    const/16 v6, 0x24

    const/16 v7, 0x28

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOO_HARD:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 102
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "TOO_EASY"

    const/16 v6, 0x25

    const/16 v7, 0x29

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOO_EASY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 103
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "NONE_OF_THESE"

    const/16 v6, 0x26

    const/16 v7, 0x2a

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NONE_OF_THESE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 104
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "TOO_BUSY"

    const/16 v6, 0x27

    const/16 v7, 0x2b

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOO_BUSY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 105
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "CURRENT_SCORE_HIGHER"

    const/16 v6, 0x28

    const/16 v7, 0x2c

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CURRENT_SCORE_HIGHER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 106
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "DEFAULT_MESSAGE"

    const/16 v6, 0x29

    const/16 v7, 0x2d

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->DEFAULT_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 107
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "USER_NOT_STARTED"

    const/16 v6, 0x2a

    const/16 v7, 0x2e

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_NOT_STARTED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 108
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "WEIGHT_GOAL_BEHIND_LOWER_RANGE"

    const/16 v6, 0x2b

    const/16 v7, 0x2f

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_LOWER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 109
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "WEIGHT_GOAL_BEHIND_UPPER_RANGE"

    const/16 v6, 0x2c

    const/16 v7, 0x30

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_UPPER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 110
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const-string v5, "UPDATE_YOUR_WEIGHT"

    const/16 v6, 0x2d

    const/16 v7, 0x31

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->UPDATE_YOUR_WEIGHT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 51
    const/16 v4, 0x2e

    new-array v4, v4, [Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CATCH_ALL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOALS_SET:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ASSESSMENT_NOT_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->SIX_MONTH_LIFESTYLE_REASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v5, v4, v10

    const/4 v5, 0x4

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOAL_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_MISSION_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_MISSION_PROGRESS_BEFORE_TIMESPAN_FREQUENCY_LIMIT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v5, v4, v12

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->PRE_AND_POST_MISSION_EXPIRY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TRACKER_MISSION_COMPLETED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_QUESTIONS_ANSWERED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_LAUNCHED_NO_ACTIVITY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NOT_ALL_QUESTIONS_ANSWERED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ZERO_ACTIVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ZERO_ACTIVE_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0xe

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->OVERALL_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0xf

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x10

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->RE_ASSESS_WARNING:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x11

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x12

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->MISSION_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x13

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x14

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE_MISSION_COUNT_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x15

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->COACH_MESSAGE_MISSION_COUNT_GREATER_THAN_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x16

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOTAL_MISSIONS_ONE_OR_MORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x17

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->ANY_GOAL_BEHIND:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x18

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOTAL_MISSIONS_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x19

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NO_GOALS_LEFT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x1a

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_MISSIONS_ZERO:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x1b

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->DAYS_NO_ACTION_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x1c

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_ON_TRACK:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x1d

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CANCEL_UNALIGNED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x1e

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CATEGORY_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x1f

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->GOAL_COMPLETE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x20

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_DELETES_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x21

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_DELETES_MISSION:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x22

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_ADDS_GOAL:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x23

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->FORGOT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x24

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOO_HARD:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x25

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOO_EASY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x26

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->NONE_OF_THESE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x27

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->TOO_BUSY:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x28

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->CURRENT_SCORE_HIGHER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x29

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->DEFAULT_MESSAGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x2a

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->USER_NOT_STARTED:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x2b

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_LOWER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x2c

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->WEIGHT_GOAL_BEHIND_UPPER_RANGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    const/16 v5, 0x2d

    sget-object v6, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->UPDATE_YOUR_WEIGHT:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 127
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->codeValueMap:Ljava/util/HashMap;

    .line 129
    invoke-static {}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->values()[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 130
    .local v3, "type":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->getContextFilterType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 132
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "fType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 117
    iput p3, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->contextFilterType:I

    .line 118
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .locals 2
    .param p0, "statusKey"    # I

    .prologue
    .line 136
    sget-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 51
    const-class v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    return-object v0
.end method


# virtual methods
.method public getContextFilterType()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;->contextFilterType:I

    return v0
.end method
