.class public Lcom/cigna/coach/dataobjects/JournalEntry;
.super Ljava/lang/Object;
.source "JournalEntry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    }
.end annotation


# instance fields
.field actionTime:Ljava/util/Calendar;

.field actionType:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

.field codeBaseId:I

.field data:Ljava/lang/String;

.field dbVersion:I

.field key:Ljava/lang/String;

.field seqId:I

.field tableName:Ljava/lang/String;

.field userId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method public getActionTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->actionTime:Ljava/util/Calendar;

    return-object v0
.end method

.method public getActionType()Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->actionType:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    return-object v0
.end method

.method public getCodeBaseId()I
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->codeBaseId:I

    return v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->data:Ljava/lang/String;

    return-object v0
.end method

.method public getDbVersion()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->dbVersion:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getSeqId()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->seqId:I

    return v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->tableName:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->userId:I

    return v0
.end method

.method public setActionTime(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "actionTime"    # Ljava/util/Calendar;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->actionTime:Ljava/util/Calendar;

    .line 113
    return-void
.end method

.method public setActionType(Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;)V
    .locals 0
    .param p1, "actionType"    # Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->actionType:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    .line 89
    return-void
.end method

.method public setCodeBaseId(I)V
    .locals 0
    .param p1, "codeBaseId"    # I

    .prologue
    .line 128
    iput p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->codeBaseId:I

    .line 129
    return-void
.end method

.method public setData(Ljava/lang/String;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->data:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setDbVersion(I)V
    .locals 0
    .param p1, "dbVersion"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->dbVersion:I

    .line 137
    return-void
.end method

.method public setKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->key:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setSeqId(I)V
    .locals 0
    .param p1, "seqId"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->seqId:I

    .line 121
    return-void
.end method

.method public setTableName(Ljava/lang/String;)V
    .locals 0
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->tableName:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->userId:I

    .line 81
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "JournalEntry [seqId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->seqId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", codeBaseId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->codeBaseId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dbVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->dbVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->userId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->actionType:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tableName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->tableName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->data:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", actionTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/JournalEntry;->actionTime:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
