.class public Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
.super Ljava/lang/Object;
.source "ApiCallLogEntry.java"


# instance fields
.field exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

.field invocationRespTimeInMillis:J

.field invocationTime:Ljava/util/Calendar;

.field method:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

.field userId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExceptionType()Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    return-object v0
.end method

.method public getInvocationRespTimeInMillis()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->invocationRespTimeInMillis:J

    return-wide v0
.end method

.method public getInvocationTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->invocationTime:Ljava/util/Calendar;

    return-object v0
.end method

.method public getMethod()Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->method:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    return-object v0
.end method

.method public getSource()Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->userId:I

    return v0
.end method

.method public setExceptionType(Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;)V
    .locals 0
    .param p1, "exceptionType"    # Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 79
    return-void
.end method

.method public setInvocationRespTimeInMillis(J)V
    .locals 0
    .param p1, "invocationRespTimeInMillis"    # J

    .prologue
    .line 70
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->invocationRespTimeInMillis:J

    .line 71
    return-void
.end method

.method public setInvocationTime(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "invocationTime"    # Ljava/util/Calendar;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->invocationTime:Ljava/util/Calendar;

    .line 63
    return-void
.end method

.method public setMethod(Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;)V
    .locals 0
    .param p1, "method"    # Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->method:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 47
    return-void
.end method

.method public setSource(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)V
    .locals 0
    .param p1, "source"    # Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .line 55
    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->userId:I

    .line 39
    return-void
.end method
