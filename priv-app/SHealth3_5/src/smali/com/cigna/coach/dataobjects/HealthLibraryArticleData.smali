.class public Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;
.super Lcom/cigna/coach/apiobjects/HealthLibraryArticle;
.source "HealthLibraryArticleData.java"


# instance fields
.field protected catDescId:I

.field protected catImageId:I

.field protected detailId:I

.field protected imageId:I

.field protected subCatDescId:I

.field protected subCatImageId:I

.field protected titleId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/HealthLibraryArticle;-><init>()V

    return-void
.end method


# virtual methods
.method public getCatDescId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCategory"
    .end annotation

    .prologue
    .line 61
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->catDescId:I

    return v0
.end method

.method public getCatImageId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCategoryImage"
    .end annotation

    .prologue
    .line 80
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->catImageId:I

    return v0
.end method

.method public getDetailId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setDetail"
    .end annotation

    .prologue
    .line 135
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->detailId:I

    return v0
.end method

.method public getImageId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTitleImg"
    .end annotation

    .prologue
    .line 172
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->imageId:I

    return v0
.end method

.method public getSubCatDescId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setSubCategory"
    .end annotation

    .prologue
    .line 191
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->subCatDescId:I

    return v0
.end method

.method public getSubCatImageId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setSubCategoryImage"
    .end annotation

    .prologue
    .line 210
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->subCatImageId:I

    return v0
.end method

.method public getTitleId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTitle"
    .end annotation

    .prologue
    .line 265
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->titleId:I

    return v0
.end method

.method public setCatDescId(I)V
    .locals 0
    .param p1, "catDescId"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->catDescId:I

    .line 52
    return-void
.end method

.method public setCatImageId(I)V
    .locals 0
    .param p1, "catImageId"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->catImageId:I

    .line 71
    return-void
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->category:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setCategoryId(I)V
    .locals 0
    .param p1, "categoryId"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->categoryId:I

    .line 99
    return-void
.end method

.method public setCategoryImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "categoryImage"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->categoryImage:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public setDetail(Ljava/lang/String;)V
    .locals 0
    .param p1, "detail"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->detail:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setDetailId(I)V
    .locals 0
    .param p1, "detailId"    # I

    .prologue
    .line 125
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->detailId:I

    .line 126
    return-void
.end method

.method public setFavorite(Z)V
    .locals 0
    .param p1, "favorite"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->favorite:Z

    .line 145
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->id:I

    .line 154
    return-void
.end method

.method public setImageId(I)V
    .locals 0
    .param p1, "imageId"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->imageId:I

    .line 163
    return-void
.end method

.method public setSubCatDescId(I)V
    .locals 0
    .param p1, "subCatDescId"    # I

    .prologue
    .line 181
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->subCatDescId:I

    .line 182
    return-void
.end method

.method public setSubCatImageId(I)V
    .locals 0
    .param p1, "subCatImageId"    # I

    .prologue
    .line 200
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->subCatImageId:I

    .line 201
    return-void
.end method

.method public setSubCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "subCategory"    # Ljava/lang/String;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->subCategory:Ljava/lang/String;

    .line 220
    return-void
.end method

.method public setSubCategoryId(I)V
    .locals 0
    .param p1, "subCategoryId"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->subCategoryId:I

    .line 229
    return-void
.end method

.method public setSubCategoryImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "subCategoryImage"    # Ljava/lang/String;

    .prologue
    .line 237
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->subCategoryImage:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->title:Ljava/lang/String;

    .line 247
    return-void
.end method

.method public setTitleId(I)V
    .locals 0
    .param p1, "titleId"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->titleId:I

    .line 256
    return-void
.end method

.method public setTitleImg(Ljava/lang/String;)V
    .locals 0
    .param p1, "titleImg"    # Ljava/lang/String;

    .prologue
    .line 274
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibraryArticleData;->titleImg:Ljava/lang/String;

    .line 275
    return-void
.end method
