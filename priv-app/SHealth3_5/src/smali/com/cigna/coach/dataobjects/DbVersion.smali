.class public Lcom/cigna/coach/dataobjects/DbVersion;
.super Ljava/lang/Object;
.source "DbVersion.java"


# instance fields
.field codeBaseId:I

.field dbVersion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodeBaseId()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/cigna/coach/dataobjects/DbVersion;->codeBaseId:I

    return v0
.end method

.method public getDbVersion()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/cigna/coach/dataobjects/DbVersion;->dbVersion:I

    return v0
.end method

.method public setCodeBaseId(I)V
    .locals 0
    .param p1, "codeBaseId"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/cigna/coach/dataobjects/DbVersion;->codeBaseId:I

    .line 33
    return-void
.end method

.method public setDbVersion(I)V
    .locals 0
    .param p1, "dbVersion"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/cigna/coach/dataobjects/DbVersion;->dbVersion:I

    .line 49
    return-void
.end method
