.class public Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;
.super Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;
.source "GoalsMissionsHistoryData.java"


# instance fields
.field private completedGoalsContentId:I

.field private completedMissionsContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/GoalsMissionsHistory;-><init>()V

    return-void
.end method


# virtual methods
.method public getCompletedGoalsContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCompletedGoalsLabel"
    .end annotation

    .prologue
    .line 25
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->completedGoalsContentId:I

    return v0
.end method

.method public getCompletedMissionsContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCompletedMissionsLabel"
    .end annotation

    .prologue
    .line 38
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->completedMissionsContentId:I

    return v0
.end method

.method public setCompletedGoalsContentId(I)V
    .locals 0
    .param p1, "completedGoalsContentId"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->completedGoalsContentId:I

    .line 32
    return-void
.end method

.method public setCompletedMissionsContentId(I)V
    .locals 0
    .param p1, "completedMissionsContentId"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalsMissionsHistoryData;->completedMissionsContentId:I

    .line 45
    return-void
.end method
