.class public final enum Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;
.super Ljava/lang/Enum;
.source "CoachMessageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/CoachMessageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReferenceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

.field public static final enum BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

.field public static final enum CATEGORY:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

.field public static final enum NO_OF_DAYS:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

.field public static final enum NO_OF_HOURS:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;


# instance fields
.field referenceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 200
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v5, v2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->CATEGORY:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    .line 201
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    const-string v1, "BADGE"

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    .line 202
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    const-string v1, "NO_OF_DAYS"

    invoke-direct {v0, v1, v3, v4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->NO_OF_DAYS:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    .line 203
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    const-string v1, "NO_OF_HOURS"

    invoke-direct {v0, v1, v4, v6}, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->NO_OF_HOURS:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    .line 199
    new-array v0, v6, [Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->CATEGORY:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->BADGE:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->NO_OF_DAYS:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->NO_OF_HOURS:Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 209
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 210
    iput p3, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->referenceType:I

    .line 211
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 199
    const-class v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;
    .locals 1

    .prologue
    .line 199
    sget-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;

    return-object v0
.end method


# virtual methods
.method public getReferenceType()I
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;->referenceType:I

    return v0
.end method
