.class public final enum Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
.super Ljava/lang/Enum;
.source "CoachMessageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/CoachMessageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContextType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum GET_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum GET_SUGGESTED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum GET_SUGGESTED_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum HISTORICAL_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum SET_MISSION_DATA:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

.field public static final enum SET_QUESTION_ANSWER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;


# instance fields
.field contextType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 27
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "CHECK_USER_PROGRESS"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 28
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "GET_SCORES"

    invoke-direct {v0, v1, v4, v5}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 29
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "GET_SUGGESTED_GOALS"

    invoke-direct {v0, v1, v5, v6}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SUGGESTED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 30
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "GET_SUGGESTED_MISSIONS"

    invoke-direct {v0, v1, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SUGGESTED_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 31
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "RETRIEVE_GOALS"

    invoke-direct {v0, v1, v7, v8}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 32
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "SET_MISSION_DATA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_MISSION_DATA:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 33
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "SET_QUESTION_ANSWER"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_QUESTION_ANSWER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 34
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const-string v1, "HISTORICAL_SCORES"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->HISTORICAL_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    .line 26
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    const/4 v1, 0x0

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->CHECK_USER_PROGRESS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SUGGESTED_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->GET_SUGGESTED_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->RETRIEVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_MISSION_DATA:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->SET_QUESTION_ANSWER:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->HISTORICAL_SCORES:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "cType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->contextType:I

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;

    return-object v0
.end method


# virtual methods
.method public getContextType()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;->contextType:I

    return v0
.end method
