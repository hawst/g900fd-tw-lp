.class public Lcom/cigna/coach/dataobjects/AndroidWidgetData;
.super Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;
.source "AndroidWidgetData.java"


# instance fields
.field private cignaLogoURIContentId:I

.field private messageContentId:I

.field private titleContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/AndroidWidgetInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public getCignaLogoURIContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCignaLogoImageURI"
    .end annotation

    .prologue
    .line 82
    iget v0, p0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->cignaLogoURIContentId:I

    return v0
.end method

.method public getMessageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMessageText"
    .end annotation

    .prologue
    .line 66
    iget v0, p0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->messageContentId:I

    return v0
.end method

.method public getTitleContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTitleText"
    .end annotation

    .prologue
    .line 50
    iget v0, p0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->titleContentId:I

    return v0
.end method

.method public setCignaLogoURIContentId(I)V
    .locals 0
    .param p1, "cignaLogoURIContentId"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->cignaLogoURIContentId:I

    .line 91
    return-void
.end method

.method public setMessageTextContentId(I)V
    .locals 0
    .param p1, "messageContentId"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->messageContentId:I

    .line 75
    return-void
.end method

.method public setTitleContentId(I)V
    .locals 0
    .param p1, "titleContentId"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/cigna/coach/dataobjects/AndroidWidgetData;->titleContentId:I

    .line 59
    return-void
.end method
