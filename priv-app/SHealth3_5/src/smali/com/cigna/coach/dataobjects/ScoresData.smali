.class public Lcom/cigna/coach/dataobjects/ScoresData;
.super Lcom/cigna/coach/apiobjects/Scores;
.source "ScoresData.java"


# instance fields
.field private assesmentTimestamp:Ljava/util/Calendar;

.field private outOfMessageContentId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/Scores;-><init>()V

    .line 25
    const/16 v0, 0xf6f

    iput v0, p0, Lcom/cigna/coach/dataobjects/ScoresData;->outOfMessageContentId:I

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/dataobjects/ScoresData;->assesmentTimestamp:Ljava/util/Calendar;

    return-void
.end method


# virtual methods
.method public getAssesmentTimestamp()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ScoresData;->assesmentTimestamp:Ljava/util/Calendar;

    return-object v0
.end method

.method public getOutOfMessageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setOutOfMessage"
    .end annotation

    .prologue
    .line 35
    iget v0, p0, Lcom/cigna/coach/dataobjects/ScoresData;->outOfMessageContentId:I

    return v0
.end method

.method public setAssesmentTimestamp(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "assesmentTimestamp"    # Ljava/util/Calendar;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ScoresData;->assesmentTimestamp:Ljava/util/Calendar;

    .line 46
    return-void
.end method
