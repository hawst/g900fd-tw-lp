.class public final enum Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
.super Ljava/lang/Enum;
.source "MissionDetailsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/MissionDetailsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivitySource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

.field public static final enum MANUAL:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

.field public static final enum TRACKER:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field activitySourceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 13
    new-instance v4, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    const-string v5, "MANUAL"

    invoke-direct {v4, v5, v7, v6}, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->MANUAL:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    .line 14
    new-instance v4, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    const-string v5, "TRACKER"

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->TRACKER:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    .line 12
    new-array v4, v8, [Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    sget-object v5, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->MANUAL:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->TRACKER:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    aput-object v5, v4, v6

    sput-object v4, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->$VALUES:[Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    .line 26
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->codeValueMap:Ljava/util/HashMap;

    .line 28
    invoke-static {}, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->values()[Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 29
    .local v3, "type":Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    sget-object v4, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->getSourceType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 31
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "mType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    iput p3, p0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->activitySourceType:I

    .line 22
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    .locals 2
    .param p0, "activitySourceTypeKey"    # I

    .prologue
    .line 40
    sget-object v0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->$VALUES:[Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    return-object v0
.end method


# virtual methods
.method public getSourceType()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;->activitySourceType:I

    return v0
.end method
