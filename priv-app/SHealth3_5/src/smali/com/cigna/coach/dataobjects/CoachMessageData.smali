.class public Lcom/cigna/coach/dataobjects/CoachMessageData;
.super Lcom/cigna/coach/apiobjects/CoachMessage;
.source "CoachMessageData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;,
        Lcom/cigna/coach/dataobjects/CoachMessageData$ReferenceType;,
        Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;,
        Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;,
        Lcom/cigna/coach/dataobjects/CoachMessageData$ContextType;
    }
.end annotation


# instance fields
.field private coachMessageId:I

.field private contextFilterType:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field private intentAction:Ljava/lang/String;

.field private intentExtraType:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

.field private maxValue:I

.field private messageContentId:I

.field private messageDescriptionContentId:I

.field private messageDisplayRank:I

.field private messageImageContentId:I

.field private minMaxRefValue:I

.field private minValue:I

.field private missionContentId:I

.field private notificationDeepLinkInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private repeatInterval:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 23
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/CoachMessage;-><init>()V

    .line 141
    iput v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->coachMessageId:I

    .line 250
    iput v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageDisplayRank:I

    .line 264
    iput v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->repeatInterval:I

    return-void
.end method


# virtual methods
.method public copy(Lcom/cigna/coach/apiobjects/CoachMessage;)V
    .locals 2
    .param p1, "copyfrom"    # Lcom/cigna/coach/apiobjects/CoachMessage;

    .prologue
    .line 408
    move-object v0, p1

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 409
    .local v0, "from":Lcom/cigna/coach/dataobjects/CoachMessageData;
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getCoachMessageId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setCoachMessageId(I)V

    .line 410
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setContextFilterType(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V

    .line 411
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setIntentAction(Ljava/lang/String;)V

    .line 412
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentExtraType()Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setIntentExtraType(Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;)V

    .line 413
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMaxValue(I)V

    .line 414
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessage(Ljava/lang/String;)V

    .line 415
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageContentId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageContentId(I)V

    .line 416
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageDescription(Ljava/lang/String;)V

    .line 417
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDescriptionContentId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageDescriptionContentId(I)V

    .line 418
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageDisplayRank()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageDisplayRank(I)V

    .line 419
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageImage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageImage(Ljava/lang/String;)V

    .line 420
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageImageContentId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageImageContentId(I)V

    .line 421
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMessageType()Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMessageType(Lcom/cigna/coach/apiobjects/CoachMessage$CoachMessageType;)V

    .line 422
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMinMaxRefValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMinMaxRefValue(I)V

    .line 423
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMaxValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMinValue(I)V

    .line 424
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getMissionContentId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setMissionContentId(I)V

    .line 425
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getNotificationDeepLinkInfo()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setNotificationDeepLinkInfo(Ljava/util/Map;)V

    .line 426
    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getRepeatInterval()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cigna/coach/dataobjects/CoachMessageData;->setRepeatInterval(I)V

    .line 428
    return-void
.end method

.method public getCoachMessageId()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->coachMessageId:I

    return v0
.end method

.method public getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->contextFilterType:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    return-object v0
.end method

.method public getIntentAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->intentAction:Ljava/lang/String;

    return-object v0
.end method

.method public getIntentExtraType()Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->intentExtraType:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    return-object v0
.end method

.method public getMaxValue()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->maxValue:I

    return v0
.end method

.method public getMessageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMessage"
    .end annotation

    .prologue
    .line 283
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageContentId:I

    return v0
.end method

.method public getMessageDescriptionContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMessageDescription"
    .end annotation

    .prologue
    .line 274
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageDescriptionContentId:I

    return v0
.end method

.method public getMessageDisplayRank()I
    .locals 1

    .prologue
    .line 253
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageDisplayRank:I

    return v0
.end method

.method public getMessageImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMessageImage"
    .end annotation

    .prologue
    .line 301
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageImageContentId:I

    return v0
.end method

.method public getMinMaxRefValue()I
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->minMaxRefValue:I

    return v0
.end method

.method public getMinValue()I
    .locals 1

    .prologue
    .line 340
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->minValue:I

    return v0
.end method

.method public getMissionContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "MISSION_NAME"
        tagValueType = .enum Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT_ID:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
    .end annotation

    .prologue
    .line 292
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->missionContentId:I

    return v0
.end method

.method public getNotificationDeepLinkInfo()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->notificationDeepLinkInfo:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->notificationDeepLinkInfo:Ljava/util/Map;

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->notificationDeepLinkInfo:Ljava/util/Map;

    const-string v1, "EXTRA_NAME_DESTINATION"

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/CoachMessageData;->getIntentAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->notificationDeepLinkInfo:Ljava/util/Map;

    return-object v0
.end method

.method public getRepeatInterval()I
    .locals 1

    .prologue
    .line 312
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->repeatInterval:I

    return v0
.end method

.method public setCoachMessageId(I)V
    .locals 0
    .param p1, "coachMessageId"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->coachMessageId:I

    .line 169
    return-void
.end method

.method public setContextFilterType(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V
    .locals 0
    .param p1, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->contextFilterType:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 376
    return-void
.end method

.method public setIntentAction(Ljava/lang/String;)V
    .locals 0
    .param p1, "intentAction"    # Ljava/lang/String;

    .prologue
    .line 389
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->intentAction:Ljava/lang/String;

    .line 390
    return-void
.end method

.method public setIntentExtraType(Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;)V
    .locals 0
    .param p1, "intentExtraType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->intentExtraType:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    .line 404
    return-void
.end method

.method public setMaxValue(I)V
    .locals 0
    .param p1, "maxValue"    # I

    .prologue
    .line 361
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->maxValue:I

    .line 362
    return-void
.end method

.method public setMessageContentId(I)V
    .locals 0
    .param p1, "messageContentId"    # I

    .prologue
    .line 287
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageContentId:I

    .line 288
    return-void
.end method

.method public setMessageDescriptionContentId(I)V
    .locals 0
    .param p1, "messageDescriptionContentId"    # I

    .prologue
    .line 278
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageDescriptionContentId:I

    .line 279
    return-void
.end method

.method public setMessageDisplayRank(I)V
    .locals 0
    .param p1, "messageDisplayRank"    # I

    .prologue
    .line 257
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageDisplayRank:I

    .line 258
    return-void
.end method

.method public setMessageImageContentId(I)V
    .locals 0
    .param p1, "messageImageContentId"    # I

    .prologue
    .line 305
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->messageImageContentId:I

    .line 306
    return-void
.end method

.method public setMinMaxRefValue(I)V
    .locals 0
    .param p1, "minMaxRefValue"    # I

    .prologue
    .line 333
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->minMaxRefValue:I

    .line 334
    return-void
.end method

.method public setMinValue(I)V
    .locals 0
    .param p1, "minValue"    # I

    .prologue
    .line 347
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->minValue:I

    .line 348
    return-void
.end method

.method public setMissionContentId(I)V
    .locals 0
    .param p1, "missionContentId"    # I

    .prologue
    .line 296
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->missionContentId:I

    .line 297
    return-void
.end method

.method public setNotificationDeepLinkInfo(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "notificationDeepLinkInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->notificationDeepLinkInfo:Ljava/util/Map;

    .line 161
    return-void
.end method

.method public setRepeatInterval(I)V
    .locals 0
    .param p1, "repeatInterval"    # I

    .prologue
    .line 319
    iput p1, p0, Lcom/cigna/coach/dataobjects/CoachMessageData;->repeatInterval:I

    .line 320
    return-void
.end method
