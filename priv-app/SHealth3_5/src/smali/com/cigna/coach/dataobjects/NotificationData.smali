.class public Lcom/cigna/coach/dataobjects/NotificationData;
.super Lcom/cigna/coach/apiobjects/Notification;
.source "NotificationData.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private coachMessageId:I

.field private contextFilterType:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

.field private featureContentId:I

.field private imageContentId:I

.field private messageLine1ContentId:I

.field multiLangImageContent:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private multiLangImageContentId:I

.field multiLangLine1Content:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private multiLangLine1ContentId:I

.field multiLangTitleContent:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private multiLangTitleContentId:I

.field private titleContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/Notification;-><init>()V

    return-void
.end method


# virtual methods
.method public getCoachMessageId()I
    .locals 1

    .prologue
    .line 158
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->coachMessageId:I

    return v0
.end method

.method public getContextFilterType()Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->contextFilterType:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    return-object v0
.end method

.method public getFeatureContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setFeature"
    .end annotation

    .prologue
    .line 69
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->featureContentId:I

    return v0
.end method

.method public getImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setImage"
    .end annotation

    .prologue
    .line 96
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->imageContentId:I

    return v0
.end method

.method public getMessageLine1ContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMessageLine1"
    .end annotation

    .prologue
    .line 87
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->messageLine1ContentId:I

    return v0
.end method

.method public getMultiLangImageContent()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangImageContent:Ljava/util/Map;

    return-object v0
.end method

.method public getMultiLangImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachMultiLanguageContentId;
        setTextMethod = "setMultiLangImageContent"
    .end annotation

    .prologue
    .line 123
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangImageContentId:I

    return v0
.end method

.method public getMultiLangLine1Content()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangLine1Content:Ljava/util/Map;

    return-object v0
.end method

.method public getMultiLangLine1ContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachMultiLanguageContentId;
        setTextMethod = "setMultiLangLine1Content"
    .end annotation

    .prologue
    .line 114
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangLine1ContentId:I

    return v0
.end method

.method public getMultiLangTitleContent()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangTitleContent:Ljava/util/Map;

    return-object v0
.end method

.method public getMultiLangTitleContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachMultiLanguageContentId;
        setTextMethod = "setMultiLangTitleContent"
    .end annotation

    .prologue
    .line 105
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangTitleContentId:I

    return v0
.end method

.method public getTitleContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTitle"
    .end annotation

    .prologue
    .line 78
    iget v0, p0, Lcom/cigna/coach/dataobjects/NotificationData;->titleContentId:I

    return v0
.end method

.method public setCoachMessageId(I)V
    .locals 0
    .param p1, "coachMessageId"    # I

    .prologue
    .line 165
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->coachMessageId:I

    .line 166
    return-void
.end method

.method public setContextFilterType(Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;)V
    .locals 0
    .param p1, "contextFilterType"    # Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->contextFilterType:Lcom/cigna/coach/dataobjects/CoachMessageData$ContextFilterType;

    .line 180
    return-void
.end method

.method public setFeatureContentId(I)V
    .locals 0
    .param p1, "featureContentId"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->featureContentId:I

    .line 74
    return-void
.end method

.method public setImageContentId(I)V
    .locals 0
    .param p1, "imageContentId"    # I

    .prologue
    .line 100
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->imageContentId:I

    .line 101
    return-void
.end method

.method public setMessageLine1ContentId(I)V
    .locals 0
    .param p1, "messageLine1ContentId"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->messageLine1ContentId:I

    .line 92
    return-void
.end method

.method public setMultiLangImageContent(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "multiLangImageContent":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangImageContent:Ljava/util/Map;

    .line 152
    return-void
.end method

.method public setMultiLangImageContentId(I)V
    .locals 0
    .param p1, "multiLangImageContentId"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangImageContentId:I

    .line 128
    return-void
.end method

.method public setMultiLangLine1Content(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "multiLangLine1Content":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangLine1Content:Ljava/util/Map;

    .line 144
    return-void
.end method

.method public setMultiLangLine1ContentId(I)V
    .locals 0
    .param p1, "multiLangLine1ContentId"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangLine1ContentId:I

    .line 119
    return-void
.end method

.method public setMultiLangTitleContent(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "multiLangTitleContent":Ljava/util/Map;, "Ljava/util/Map<Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangTitleContent:Ljava/util/Map;

    .line 136
    return-void
.end method

.method public setMultiLangTitleContentId(I)V
    .locals 0
    .param p1, "multiLangTitleContentId"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->multiLangTitleContentId:I

    .line 110
    return-void
.end method

.method public setTitleContentId(I)V
    .locals 0
    .param p1, "titleContentId"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/cigna/coach/dataobjects/NotificationData;->titleContentId:I

    .line 83
    return-void
.end method
