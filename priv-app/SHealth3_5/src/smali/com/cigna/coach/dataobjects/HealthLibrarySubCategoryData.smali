.class public Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;
.super Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;
.source "HealthLibrarySubCategoryData.java"


# instance fields
.field protected cntntId:I

.field protected imgCntntId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/HealthLibrarySubCategory;-><init>()V

    return-void
.end method


# virtual methods
.method public getCntntId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setDescription"
    .end annotation

    .prologue
    .line 55
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->cntntId:I

    return v0
.end method

.method public getImgCntntId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setImage"
    .end annotation

    .prologue
    .line 92
    iget v0, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->imgCntntId:I

    return v0
.end method

.method public setCategoryId(I)V
    .locals 0
    .param p1, "categoryId"    # I

    .prologue
    .line 36
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->categoryId:I

    .line 37
    return-void
.end method

.method public setCntntId(I)V
    .locals 0
    .param p1, "cntntId"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->cntntId:I

    .line 46
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->description:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 0
    .param p1, "image"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->image:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setImgCntntId(I)V
    .locals 0
    .param p1, "imgCntntId"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->imgCntntId:I

    .line 83
    return-void
.end method

.method public setSubCategoryId(I)V
    .locals 0
    .param p1, "subCategoryId"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/cigna/coach/dataobjects/HealthLibrarySubCategoryData;->subCategoryId:I

    .line 102
    return-void
.end method
