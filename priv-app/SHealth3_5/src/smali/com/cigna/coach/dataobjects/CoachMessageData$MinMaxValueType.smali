.class public final enum Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;
.super Ljava/lang/Enum;
.source "CoachMessageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/CoachMessageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MinMaxValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum BMI_100:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum CATEGORY_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum COUNT_OF_ACTIVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum COUNT_OF_ACTIVE_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum DAYS_SINCE_LAST_ASSESSMENT:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum LIFE_STYLE_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum MONTHS_SINCE_LAST_ASSESSMENT:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum NO_OF_DAYS_BETWEEN:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum NO_OF_HOURS_BETWEEN:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

.field public static final enum NO_OF_MONTHS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;


# instance fields
.field minMaxValueType:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 173
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "CATEGORY_SCORE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->CATEGORY_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 174
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "COUNT_OF_ACTIVE_GOALS"

    invoke-direct {v0, v1, v4, v5}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->COUNT_OF_ACTIVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 175
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "COUNT_OF_ACTIVE_MISSIONS"

    invoke-direct {v0, v1, v5, v6}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->COUNT_OF_ACTIVE_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 176
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "DAYS_SINCE_LAST_ASSESSMENT"

    invoke-direct {v0, v1, v6, v7}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->DAYS_SINCE_LAST_ASSESSMENT:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 177
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "LIFE_STYLE_SCORE"

    invoke-direct {v0, v1, v7, v8}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->LIFE_STYLE_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 178
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "NO_OF_DAYS_BETWEEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->NO_OF_DAYS_BETWEEN:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 179
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "NO_OF_HOURS_BETWEEN"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->NO_OF_HOURS_BETWEEN:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 180
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "NO_OF_MONTHS"

    const/4 v2, 0x7

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->NO_OF_MONTHS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 181
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "BMI_100"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->BMI_100:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 182
    new-instance v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const-string v1, "MONTHS_SINCE_LAST_ASSESSMENT"

    const/16 v2, 0x9

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->MONTHS_SINCE_LAST_ASSESSMENT:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    .line 172
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    const/4 v1, 0x0

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->CATEGORY_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->COUNT_OF_ACTIVE_GOALS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->COUNT_OF_ACTIVE_MISSIONS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->DAYS_SINCE_LAST_ASSESSMENT:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->LIFE_STYLE_SCORE:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->NO_OF_DAYS_BETWEEN:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->NO_OF_HOURS_BETWEEN:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->NO_OF_MONTHS:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->BMI_100:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->MONTHS_SINCE_LAST_ASSESSMENT:Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "fType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 188
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 189
    iput p3, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->minMaxValueType:I

    .line 190
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 172
    const-class v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;

    return-object v0
.end method


# virtual methods
.method public getMinMaxValueType()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$MinMaxValueType;->minMaxValueType:I

    return v0
.end method
