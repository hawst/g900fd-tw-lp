.class public Lcom/cigna/coach/dataobjects/GoalMissionData;
.super Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;
.source "GoalMissionData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;,
        Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;,
        Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;,
        Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    }
.end annotation


# instance fields
.field contentId:I

.field currentMissionStatusContentId:I

.field currentStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

.field dailyTarget:I

.field defaultFrequency:I

.field dependentMissionId:I

.field descriptionContentId:I

.field displayOrder:I

.field frequencyContentId:I

.field imageContentId:I

.field maxDailyTarget:I

.field maxFrequency:I

.field minFrequency:I

.field missionStage:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

.field primaryCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

.field primarySubcategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

.field spanInDays:I

.field trackerRuleId:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field unitsToTrack:I

.field whatContentId:I

.field whatImageContentId:I

.field whyContentId:I

.field whyImageContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/GoalMissionDetailInfo;-><init>()V

    .line 128
    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMission"
    .end annotation

    .prologue
    .line 386
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->contentId:I

    return v0
.end method

.method public getCurrentMissionStatusContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCurrentMissionStatus"
    .end annotation

    .prologue
    .line 466
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->currentMissionStatusContentId:I

    return v0
.end method

.method public getCurrentStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->currentStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    return-object v0
.end method

.method public getDailyTarget()I
    .locals 1

    .prologue
    .line 202
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->dailyTarget:I

    return v0
.end method

.method public getDefaultFrequency()I
    .locals 1

    .prologue
    .line 358
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->defaultFrequency:I

    return v0
.end method

.method public getDependentMissionId()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->dependentMissionId:I

    return v0
.end method

.method public getDescriptionContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMissionDescription"
    .end annotation

    .prologue
    .line 402
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->descriptionContentId:I

    return v0
.end method

.method public getDisplayOrder()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->displayOrder:I

    return v0
.end method

.method public getFrequencyContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setFrequencyDefault"
    .end annotation

    .prologue
    .line 450
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->frequencyContentId:I

    return v0
.end method

.method public getImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMissionImage"
    .end annotation

    .prologue
    .line 510
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->imageContentId:I

    return v0
.end method

.method public getMaxDailyTarget()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->maxDailyTarget:I

    return v0
.end method

.method public getMaxFrequency()I
    .locals 1

    .prologue
    .line 366
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->maxFrequency:I

    return v0
.end method

.method public getMinFrequency()I
    .locals 1

    .prologue
    .line 374
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->minFrequency:I

    return v0
.end method

.method public getMissionStage()Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->missionStage:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    return-object v0
.end method

.method public getPrimaryCategory()Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->primaryCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    return-object v0
.end method

.method public getPrimarySubcategory()Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->primarySubcategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    return-object v0
.end method

.method public getSpanInDays()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "span"
    .end annotation

    .prologue
    .line 248
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->spanInDays:I

    return v0
.end method

.method public getTrackerRuleId()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->trackerRuleId:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    return-object v0
.end method

.method public getUnitsToTrack()I
    .locals 1

    .prologue
    .line 315
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->unitsToTrack:I

    return v0
.end method

.method public getUserFrequency()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "frequency"
    .end annotation

    .prologue
    .line 253
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->defaultFrequency:I

    return v0
.end method

.method public getWhatContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setWhatToDo"
    .end annotation

    .prologue
    .line 434
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whatContentId:I

    return v0
.end method

.method public getWhatImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setWhatToDoImage"
    .end annotation

    .prologue
    .line 480
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whatImageContentId:I

    return v0
.end method

.method public getWhyContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setWhyDoIt"
    .end annotation

    .prologue
    .line 418
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whyContentId:I

    return v0
.end method

.method public getWhyImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setWhyDoItImage"
    .end annotation

    .prologue
    .line 495
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whyImageContentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 394
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->contentId:I

    .line 395
    return-void
.end method

.method public setCurrentMissionStatusContentId(I)V
    .locals 0
    .param p1, "currentMissionStatusContentId"    # I

    .prologue
    .line 473
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->currentMissionStatusContentId:I

    .line 474
    return-void
.end method

.method public setCurrentStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;)V
    .locals 1
    .param p1, "currentStatus"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->currentStatus:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    .line 339
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne p1, v0, :cond_1

    .line 341
    const/16 v0, 0xf5c

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setCurrentMissionStatusContentId(I)V

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->FAILED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne p1, v0, :cond_2

    .line 345
    const/16 v0, 0x1191

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setCurrentMissionStatusContentId(I)V

    goto :goto_0

    .line 347
    :cond_2
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne p1, v0, :cond_3

    .line 349
    const/16 v0, 0xf30

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setCurrentMissionStatusContentId(I)V

    goto :goto_0

    .line 351
    :cond_3
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-eq p1, v0, :cond_4

    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionStatus;

    if-ne p1, v0, :cond_0

    .line 353
    :cond_4
    const/16 v0, 0x1015

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setCurrentMissionStatusContentId(I)V

    goto :goto_0
.end method

.method public setDailyTarget(I)V
    .locals 0
    .param p1, "dailyTarget"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->dailyTarget:I

    .line 211
    return-void
.end method

.method public setDefaultFrequency(I)V
    .locals 0
    .param p1, "defaultFrequency"    # I

    .prologue
    .line 362
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->defaultFrequency:I

    .line 363
    return-void
.end method

.method public setDependentMissionId(I)V
    .locals 0
    .param p1, "dependentMissionId"    # I

    .prologue
    .line 240
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->dependentMissionId:I

    .line 241
    return-void
.end method

.method public setDescriptionContentId(I)V
    .locals 0
    .param p1, "descriptionContentId"    # I

    .prologue
    .line 410
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->descriptionContentId:I

    .line 411
    return-void
.end method

.method public setDisplayOrder(I)V
    .locals 0
    .param p1, "displayOrder"    # I

    .prologue
    .line 308
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->displayOrder:I

    .line 309
    return-void
.end method

.method public setFrequencyContentId(I)V
    .locals 0
    .param p1, "frequencyContentId"    # I

    .prologue
    .line 458
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->frequencyContentId:I

    .line 459
    return-void
.end method

.method public setImageContentId(I)V
    .locals 0
    .param p1, "imageContentId"    # I

    .prologue
    .line 517
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->imageContentId:I

    .line 518
    return-void
.end method

.method public setMaxDailyTarget(I)V
    .locals 0
    .param p1, "maxDailyTarget"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->maxDailyTarget:I

    .line 226
    return-void
.end method

.method public setMaxFrequency(I)V
    .locals 0
    .param p1, "maxFrequency"    # I

    .prologue
    .line 370
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->maxFrequency:I

    .line 371
    return-void
.end method

.method public setMinFrequency(I)V
    .locals 0
    .param p1, "minFrequency"    # I

    .prologue
    .line 378
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->minFrequency:I

    .line 379
    return-void
.end method

.method public setMissionStage(Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;)V
    .locals 0
    .param p1, "missionStage"    # Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->missionStage:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .line 196
    return-void
.end method

.method public setPrimaryCategory(Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;)V
    .locals 0
    .param p1, "primaryCategory"    # Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .prologue
    .line 293
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->primaryCategory:Lcom/cigna/coach/interfaces/ILifeStyle$CategoryType;

    .line 294
    return-void
.end method

.method public setPrimarySubcategory(Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;)V
    .locals 0
    .param p1, "primarySubcategoryType"    # Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .prologue
    .line 525
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->primarySubcategoryType:Lcom/cigna/coach/interfaces/ILifeStyle$SubcategoryType;

    .line 526
    return-void
.end method

.method public setSpanInDays(I)V
    .locals 0
    .param p1, "spanInDays"    # I

    .prologue
    .line 261
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->spanInDays:I

    .line 262
    return-void
.end method

.method public setTrackerRuleId(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;)V
    .locals 1
    .param p1, "trackerRuleId"    # Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->trackerRuleId:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 277
    if-eqz p1, :cond_0

    .line 278
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->getIntValue()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/GoalMissionData;->setTrackerMission(Z)V

    .line 279
    :cond_0
    return-void

    .line 278
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setUnitsToTrack(I)V
    .locals 0
    .param p1, "unitsToTrack"    # I

    .prologue
    .line 323
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->unitsToTrack:I

    .line 324
    return-void
.end method

.method public setWhatContentId(I)V
    .locals 0
    .param p1, "whatContentId"    # I

    .prologue
    .line 442
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whatContentId:I

    .line 443
    return-void
.end method

.method public setWhatImageContentId(I)V
    .locals 0
    .param p1, "whatImageContentId"    # I

    .prologue
    .line 487
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whatImageContentId:I

    .line 488
    return-void
.end method

.method public setWhyContentId(I)V
    .locals 0
    .param p1, "whyContentId"    # I

    .prologue
    .line 426
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whyContentId:I

    .line 427
    return-void
.end method

.method public setWhyImageContentId(I)V
    .locals 0
    .param p1, "whyImageContentId"    # I

    .prologue
    .line 502
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionData;->whyImageContentId:I

    .line 503
    return-void
.end method
