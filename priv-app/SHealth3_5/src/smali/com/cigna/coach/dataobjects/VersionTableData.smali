.class public Lcom/cigna/coach/dataobjects/VersionTableData;
.super Ljava/lang/Object;
.source "VersionTableData.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/cigna/coach/dataobjects/VersionTableData;",
        ">;"
    }
.end annotation


# static fields
.field public static final EMPTY_CHECKSUM:Ljava/lang/String; = "da39a3ee5e6b4b0d3255bfef95601890afd80709"

.field public static final EMPTY_VERSION:Ljava/lang/String; = "0"


# instance fields
.field private chksum:Ljava/lang/String;

.field private resourceId:I

.field private tableName:Ljava/lang/String;

.field private versionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static nullVersion(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/VersionTableData;
    .locals 2
    .param p0, "tableName"    # Ljava/lang/String;

    .prologue
    .line 87
    new-instance v0, Lcom/cigna/coach/dataobjects/VersionTableData;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/VersionTableData;-><init>()V

    .line 88
    .local v0, "version":Lcom/cigna/coach/dataobjects/VersionTableData;
    invoke-virtual {v0, p0}, Lcom/cigna/coach/dataobjects/VersionTableData;->setTableName(Ljava/lang/String;)V

    .line 89
    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/VersionTableData;->setVersionCode(Ljava/lang/String;)V

    .line 90
    const-string v1, "da39a3ee5e6b4b0d3255bfef95601890afd80709"

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/VersionTableData;->setChksum(Ljava/lang/String;)V

    .line 91
    return-object v0
.end method


# virtual methods
.method public compareTo(Lcom/cigna/coach/dataobjects/VersionTableData;)I
    .locals 2
    .param p1, "another"    # Lcom/cigna/coach/dataobjects/VersionTableData;

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, Lcom/cigna/coach/dataobjects/VersionTableData;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/cigna/coach/dataobjects/VersionTableData;->compareTo(Lcom/cigna/coach/dataobjects/VersionTableData;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "otherObject"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    if-ne p0, p1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    instance-of v3, p1, Lcom/cigna/coach/dataobjects/VersionTableData;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 103
    check-cast v0, Lcom/cigna/coach/dataobjects/VersionTableData;

    .line 104
    .local v0, "other":Lcom/cigna/coach/dataobjects/VersionTableData;
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getChksum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->chksum:Ljava/lang/String;

    return-object v0
.end method

.method public getResourceId()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->resourceId:I

    return v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->tableName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->versionCode:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/VersionTableData;->getVersionCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit16 v0, v0, 0xea

    return v0
.end method

.method public setChksum(Ljava/lang/String;)V
    .locals 0
    .param p1, "chksum"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->chksum:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setResourceId(I)V
    .locals 0
    .param p1, "resourceId"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->resourceId:I

    .line 118
    return-void
.end method

.method public setTableName(Ljava/lang/String;)V
    .locals 0
    .param p1, "tableName"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->tableName:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public setVersionCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "versionCode"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/VersionTableData;->versionCode:Ljava/lang/String;

    .line 69
    return-void
.end method
