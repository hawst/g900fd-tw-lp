.class public Lcom/cigna/coach/dataobjects/TrackerRuleData;
.super Ljava/lang/Object;
.source "TrackerRuleData.java"


# instance fields
.field private maxMetValue:I

.field private minMetValue:I

.field private trackerRuleId:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field private trackerTable:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

.field private trackerValue:I

.field private trackerValueTypeInd:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMaxMetValue()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->maxMetValue:I

    return v0
.end method

.method public getMinMetValue()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->minMetValue:I

    return v0
.end method

.method public getTrackerRuleId()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerRuleId:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    return-object v0
.end method

.method public getTrackerTable()Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerTable:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    return-object v0
.end method

.method public getTrackerValue()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerValue:I

    return v0
.end method

.method public getTrackerValueTypeInd()Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerValueTypeInd:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    return-object v0
.end method

.method public setMaxMetValue(I)V
    .locals 0
    .param p1, "maxMetValue"    # I

    .prologue
    .line 111
    iput p1, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->maxMetValue:I

    .line 112
    return-void
.end method

.method public setMinMetValue(I)V
    .locals 0
    .param p1, "minMetValue"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->minMetValue:I

    .line 97
    return-void
.end method

.method public setTrackerRuleId(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;)V
    .locals 0
    .param p1, "trackerRule"    # Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerRuleId:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 52
    return-void
.end method

.method public setTrackerTable(Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;)V
    .locals 0
    .param p1, "trackerTable"    # Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerTable:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    .line 126
    return-void
.end method

.method public setTrackerValue(I)V
    .locals 0
    .param p1, "trackerValue"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerValue:I

    .line 82
    return-void
.end method

.method public setTrackerValueTypeInd(Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;)V
    .locals 0
    .param p1, "trackerValueTypeInd"    # Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/TrackerRuleData;->trackerValueTypeInd:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    .line 67
    return-void
.end method
