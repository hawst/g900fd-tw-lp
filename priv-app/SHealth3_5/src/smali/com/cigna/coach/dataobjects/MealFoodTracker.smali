.class public Lcom/cigna/coach/dataobjects/MealFoodTracker;
.super Ljava/lang/Object;
.source "MealFoodTracker.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private calcium:F

.field private carbohydrate:F

.field private cholesterol:F

.field private databaseId:I

.field private dietaryFiber:F

.field private foodCategory:Ljava/lang/String;

.field private foodCategoryId:Ljava/lang/String;

.field private foodName:Ljava/lang/String;

.field private foodSubCategory:Ljava/lang/String;

.field private foodSubCategoryId:Ljava/lang/String;

.field private foodUnit:Ljava/lang/String;

.field private iron:F

.field private kilogramCalorie:F

.field private kilogramCalorieTotal:F

.field private mealDate:J

.field private metricServingAmount:I

.field private metricServingUnit:Ljava/lang/String;

.field private monosaturatedFat:F

.field private polysaturatedFat:F

.field private potassium:F

.field private protein:F

.field private saturatedFat:F

.field private servingAmount:F

.field private sodium:F

.field private sugar:F

.field private totalFat:F

.field private transFat:F

.field private vitaminA:F

.field private vitaminC:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCalcium()F
    .locals 1

    .prologue
    .line 156
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->calcium:F

    return v0
.end method

.method public getCarbohydrate()F
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->carbohydrate:F

    return v0
.end method

.method public getCholesterol()F
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->cholesterol:F

    return v0
.end method

.method public getDatabaseId()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->databaseId:I

    return v0
.end method

.method public getDietaryFiber()F
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->dietaryFiber:F

    return v0
.end method

.method public getFoodCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getFoodCategoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getFoodName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodName:Ljava/lang/String;

    return-object v0
.end method

.method public getFoodSubCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodSubCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getFoodSubCategoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodSubCategoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getFoodUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getIron()F
    .locals 1

    .prologue
    .line 376
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->iron:F

    return v0
.end method

.method public getKilogramCalorie()F
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->kilogramCalorie:F

    return v0
.end method

.method public getKilogramCalorieTotal()F
    .locals 1

    .prologue
    .line 416
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->kilogramCalorieTotal:F

    return v0
.end method

.method public getMealDate()J
    .locals 2

    .prologue
    .line 436
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->mealDate:J

    return-wide v0
.end method

.method public getMetricServingAmount()I
    .locals 1

    .prologue
    .line 456
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->metricServingAmount:I

    return v0
.end method

.method public getMetricServingUnit()Ljava/lang/String;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->metricServingUnit:Ljava/lang/String;

    return-object v0
.end method

.method public getMonosaturatedFat()F
    .locals 1

    .prologue
    .line 496
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->monosaturatedFat:F

    return v0
.end method

.method public getPolysaturatedFat()F
    .locals 1

    .prologue
    .line 516
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->polysaturatedFat:F

    return v0
.end method

.method public getPotassium()F
    .locals 1

    .prologue
    .line 536
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->potassium:F

    return v0
.end method

.method public getProtein()F
    .locals 1

    .prologue
    .line 556
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->protein:F

    return v0
.end method

.method public getSaturatedFat()F
    .locals 1

    .prologue
    .line 636
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->saturatedFat:F

    return v0
.end method

.method public getServingAmount()F
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->servingAmount:F

    return v0
.end method

.method public getSodium()F
    .locals 1

    .prologue
    .line 596
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->sodium:F

    return v0
.end method

.method public getSugar()F
    .locals 1

    .prologue
    .line 616
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->sugar:F

    return v0
.end method

.method public getTotalFat()F
    .locals 1

    .prologue
    .line 656
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->totalFat:F

    return v0
.end method

.method public getTransFat()F
    .locals 1

    .prologue
    .line 677
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->transFat:F

    return v0
.end method

.method public getVitaminA()F
    .locals 1

    .prologue
    .line 697
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->vitaminA:F

    return v0
.end method

.method public getVitaminC()F
    .locals 1

    .prologue
    .line 717
    iget v0, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->vitaminC:F

    return v0
.end method

.method public setCalcium(F)V
    .locals 0
    .param p1, "calcium"    # F

    .prologue
    .line 166
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->calcium:F

    .line 167
    return-void
.end method

.method public setCarbohydrate(F)V
    .locals 0
    .param p1, "carbohydrate"    # F

    .prologue
    .line 186
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->carbohydrate:F

    .line 187
    return-void
.end method

.method public setCholesterol(F)V
    .locals 0
    .param p1, "cholesterol"    # F

    .prologue
    .line 206
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->cholesterol:F

    .line 207
    return-void
.end method

.method public setDatabaseId(I)V
    .locals 0
    .param p1, "databaseId"    # I

    .prologue
    .line 226
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->databaseId:I

    .line 227
    return-void
.end method

.method public setDietaryFiber(F)V
    .locals 0
    .param p1, "dietaryFiber"    # F

    .prologue
    .line 246
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->dietaryFiber:F

    .line 247
    return-void
.end method

.method public setFoodCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "foodCategory"    # Ljava/lang/String;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodCategory:Ljava/lang/String;

    .line 267
    return-void
.end method

.method public setFoodCategoryId(Ljava/lang/String;)V
    .locals 0
    .param p1, "foodCategoryId"    # Ljava/lang/String;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodCategoryId:Ljava/lang/String;

    .line 287
    return-void
.end method

.method public setFoodName(Ljava/lang/String;)V
    .locals 0
    .param p1, "foodName"    # Ljava/lang/String;

    .prologue
    .line 306
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodName:Ljava/lang/String;

    .line 307
    return-void
.end method

.method public setFoodSubCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "foodSubCategory"    # Ljava/lang/String;

    .prologue
    .line 326
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodSubCategory:Ljava/lang/String;

    .line 327
    return-void
.end method

.method public setFoodSubCategoryId(Ljava/lang/String;)V
    .locals 0
    .param p1, "foodSubCategoryId"    # Ljava/lang/String;

    .prologue
    .line 346
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodSubCategoryId:Ljava/lang/String;

    .line 347
    return-void
.end method

.method public setFoodUnit(Ljava/lang/String;)V
    .locals 0
    .param p1, "foodUnit"    # Ljava/lang/String;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodUnit:Ljava/lang/String;

    .line 367
    return-void
.end method

.method public setIron(F)V
    .locals 0
    .param p1, "iron"    # F

    .prologue
    .line 386
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->iron:F

    .line 387
    return-void
.end method

.method public setKilogramCalorie(F)V
    .locals 0
    .param p1, "kilogramCalorie"    # F

    .prologue
    .line 406
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->kilogramCalorie:F

    .line 407
    return-void
.end method

.method public setKilogramCalorieTotal(F)V
    .locals 0
    .param p1, "kilogramCalorieTotal"    # F

    .prologue
    .line 426
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->kilogramCalorieTotal:F

    .line 427
    return-void
.end method

.method public setMealDate(J)V
    .locals 0
    .param p1, "mealDate"    # J

    .prologue
    .line 446
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->mealDate:J

    .line 447
    return-void
.end method

.method public setMetricServingAmount(I)V
    .locals 0
    .param p1, "metricServingAmount"    # I

    .prologue
    .line 466
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->metricServingAmount:I

    .line 467
    return-void
.end method

.method public setMetricServingUnit(Ljava/lang/String;)V
    .locals 0
    .param p1, "metricServingUnit"    # Ljava/lang/String;

    .prologue
    .line 486
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->metricServingUnit:Ljava/lang/String;

    .line 487
    return-void
.end method

.method public setMonosaturatedFat(F)V
    .locals 0
    .param p1, "monosaturatedFat"    # F

    .prologue
    .line 506
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->monosaturatedFat:F

    .line 507
    return-void
.end method

.method public setPolysaturatedFat(F)V
    .locals 0
    .param p1, "polysaturatedFat"    # F

    .prologue
    .line 526
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->polysaturatedFat:F

    .line 527
    return-void
.end method

.method public setPotassium(F)V
    .locals 0
    .param p1, "potassium"    # F

    .prologue
    .line 546
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->potassium:F

    .line 547
    return-void
.end method

.method public setProtein(F)V
    .locals 0
    .param p1, "protein"    # F

    .prologue
    .line 566
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->protein:F

    .line 567
    return-void
.end method

.method public setSaturatedFat(F)V
    .locals 0
    .param p1, "saturatedFat"    # F

    .prologue
    .line 646
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->saturatedFat:F

    .line 647
    return-void
.end method

.method public setServingAmount(F)V
    .locals 0
    .param p1, "servingAmount"    # F

    .prologue
    .line 586
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->servingAmount:F

    .line 587
    return-void
.end method

.method public setSodium(F)V
    .locals 0
    .param p1, "sodium"    # F

    .prologue
    .line 606
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->sodium:F

    .line 607
    return-void
.end method

.method public setSugar(F)V
    .locals 0
    .param p1, "sugar"    # F

    .prologue
    .line 626
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->sugar:F

    .line 627
    return-void
.end method

.method public setTotalFat(F)V
    .locals 0
    .param p1, "totalFat"    # F

    .prologue
    .line 666
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->totalFat:F

    .line 667
    return-void
.end method

.method public setTransFat(F)V
    .locals 0
    .param p1, "transFat"    # F

    .prologue
    .line 687
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->transFat:F

    .line 688
    return-void
.end method

.method public setVitaminA(F)V
    .locals 0
    .param p1, "vitaminA"    # F

    .prologue
    .line 707
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->vitaminA:F

    .line 708
    return-void
.end method

.method public setVitaminC(F)V
    .locals 0
    .param p1, "vitaminC"    # F

    .prologue
    .line 727
    iput p1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->vitaminC:F

    .line 728
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 735
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MealFoodTracker = {calcium=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->calcium:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', carbohydrate\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->carbohydrate:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', cholesterol\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->cholesterol:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', databaseId\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->databaseId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', dietaryFiber\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->dietaryFiber:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', foodCategory\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', foodCategoryId\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodCategoryId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', foodName\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', foodSubCategory\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodSubCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', foodSubCategoryId\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodSubCategoryId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', foodUnit\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->foodUnit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', iron\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->iron:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', kilogramCalorie\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->kilogramCalorie:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', kilogramCalorieTotal\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->kilogramCalorieTotal:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mealDate\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->mealDate:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', metricServingAmount\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->metricServingAmount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', metricServingUnit\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->metricServingUnit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', monosaturatedFat\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->monosaturatedFat:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', polysaturatedFat\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->polysaturatedFat:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', potassium\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->potassium:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', protein\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->protein:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', servingAmount\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->servingAmount:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', sodium\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->sodium:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', sugar\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->sugar:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', saturatedFat\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->saturatedFat:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', totalFat\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->totalFat:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', transFat\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->transFat:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', vitaminA\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->vitaminA:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', vitaminC\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/MealFoodTracker;->vitaminC:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
