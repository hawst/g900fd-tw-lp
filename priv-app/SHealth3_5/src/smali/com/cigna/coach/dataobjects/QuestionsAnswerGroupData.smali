.class public Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;
.super Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;
.source "QuestionsAnswerGroupData.java"


# instance fields
.field private contentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/QuestionsAnswerGroup;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;->contentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/cigna/coach/dataobjects/QuestionsAnswerGroupData;->contentId:I

    .line 47
    return-void
.end method
