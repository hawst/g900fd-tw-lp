.class public Lcom/cigna/coach/dataobjects/MissionDetailsData;
.super Lcom/cigna/coach/apiobjects/MissionDetails;
.source "MissionDetailsData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    }
.end annotation


# instance fields
.field private activitySource:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/MissionDetails;-><init>()V

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/cigna/coach/apiobjects/MissionDetails;Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V
    .locals 1
    .param p1, "original"    # Lcom/cigna/coach/apiobjects/MissionDetails;
    .param p2, "fallbackSource"    # Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;-><init>()V

    .line 54
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getExerciseIds()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setexerciseIds(Ljava/util/List;)V

    .line 55
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getMissionActivityCompletedDate()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setMissionActivityCompletedDate(Ljava/util/Calendar;)V

    .line 56
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/MissionDetails;->getNoOfActivities()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setNoOfActivities(I)V

    .line 58
    instance-of v0, p1, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    if-eqz v0, :cond_0

    .line 59
    check-cast p1, Lcom/cigna/coach/dataobjects/MissionDetailsData;

    .end local p1    # "original":Lcom/cigna/coach/apiobjects/MissionDetails;
    invoke-virtual {p1}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->getActivitySource()Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setActivitySource(Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    .line 63
    :goto_0
    return-void

    .line 61
    .restart local p1    # "original":Lcom/cigna/coach/apiobjects/MissionDetails;
    :cond_0
    invoke-virtual {p0, p2}, Lcom/cigna/coach/dataobjects/MissionDetailsData;->setActivitySource(Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V

    goto :goto_0
.end method


# virtual methods
.method public getActivitySource()Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/MissionDetailsData;->activitySource:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    return-object v0
.end method

.method public setActivitySource(Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;)V
    .locals 0
    .param p1, "activitySource"    # Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/MissionDetailsData;->activitySource:Lcom/cigna/coach/dataobjects/MissionDetailsData$ActivitySource;

    .line 71
    return-void
.end method
