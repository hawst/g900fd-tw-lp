.class public Lcom/cigna/coach/dataobjects/WeightTracker;
.super Ljava/lang/Object;
.source "WeightTracker.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private dateInMsec:J

.field private height:F

.field private weight:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDateInMsec()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->dateInMsec:J

    return-wide v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->height:F

    return v0
.end method

.method public getWeight()F
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->weight:F

    return v0
.end method

.method public setDateInMsec(J)V
    .locals 0
    .param p1, "dateInMsec"    # J

    .prologue
    .line 68
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->dateInMsec:J

    .line 69
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 86
    iput p1, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->height:F

    .line 87
    return-void
.end method

.method public setWeight(F)V
    .locals 0
    .param p1, "weight"    # F

    .prologue
    .line 104
    iput p1, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->weight:F

    .line 105
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 113
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 114
    .local v0, "gc":Ljava/util/GregorianCalendar;
    iget-wide v2, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->dateInMsec:J

    invoke-virtual {v0, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 115
    const-string/jumbo v2, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v0, v2}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "theDate":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WeightTracker = { date: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", dateInMsec: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->dateInMsec:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", height: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->height:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", weight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/cigna/coach/dataobjects/WeightTracker;->weight:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
