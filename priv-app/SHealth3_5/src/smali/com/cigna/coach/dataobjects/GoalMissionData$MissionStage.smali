.class public final enum Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
.super Ljava/lang/Enum;
.source "GoalMissionData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/GoalMissionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MissionStage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

.field public static final enum ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

.field public static final enum PREPARE:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

.field public static final enum REFLECT:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field stageKey:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 130
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    const-string v5, "REFLECT"

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->REFLECT:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .line 131
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    const-string v5, "PREPARE"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->PREPARE:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .line 132
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    const-string v5, "ACTION"

    invoke-direct {v4, v5, v7, v9}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .line 128
    new-array v4, v9, [Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->REFLECT:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->PREPARE:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->ACTION:Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    aput-object v5, v4, v7

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    .line 145
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->codeValueMap:Ljava/util/HashMap;

    .line 147
    invoke-static {}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->values()[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 148
    .local v3, "type":Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    sget-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->getIntValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 150
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "sKey"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 135
    iput p3, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->stageKey:I

    .line 136
    return-void
.end method

.method public static getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    .locals 2
    .param p0, "statusKey"    # I

    .prologue
    .line 153
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 128
    const-class v0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$MissionStage;->stageKey:I

    return v0
.end method
