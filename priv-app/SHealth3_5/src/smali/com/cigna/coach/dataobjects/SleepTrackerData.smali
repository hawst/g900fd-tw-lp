.class public Lcom/cigna/coach/dataobjects/SleepTrackerData;
.super Ljava/lang/Object;
.source "SleepTrackerData.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private awakeCount:I

.field private bedTime:Ljava/util/Calendar;

.field private bedTimeMsec:J

.field private createTime:Ljava/util/Calendar;

.field private duration:I

.field private efficiency:F

.field private id:J

.field private lsdtd:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepDataTrackerData;",
            ">;"
        }
    .end annotation
.end field

.field private movement:I

.field private noise:Ljava/lang/String;

.field private riseTime:Ljava/util/Calendar;

.field private riseTimeMsec:J

.field private sleepQuality:I

.field private updateTime:Ljava/util/Calendar;

.field private userDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-wide v3, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->id:J

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->efficiency:F

    .line 58
    iput-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    .line 61
    iput-wide v3, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTimeMsec:J

    .line 64
    iput-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    .line 67
    iput-wide v3, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTimeMsec:J

    .line 70
    iput v2, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    .line 73
    iput v2, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->awakeCount:I

    .line 76
    iput v2, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->sleepQuality:I

    .line 79
    iput v2, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->movement:I

    .line 82
    iput-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->noise:Ljava/lang/String;

    .line 85
    iput-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->userDeviceId:Ljava/lang/String;

    .line 94
    iput-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->lsdtd:Ljava/util/List;

    .line 100
    return-void
.end method


# virtual methods
.method public getAwakeCount()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->awakeCount:I

    return v0
.end method

.method public getBedTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    return-object v0
.end method

.method public getCreateTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->createTime:Ljava/util/Calendar;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    return v0
.end method

.method public getEfficiency()F
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->efficiency:F

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->id:J

    return-wide v0
.end method

.method public getMovement()I
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->movement:I

    return v0
.end method

.method public getNoise()Ljava/lang/String;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->noise:Ljava/lang/String;

    return-object v0
.end method

.method public getRiseTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    return-object v0
.end method

.method public getSleepDataTrackerDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepDataTrackerData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 386
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->lsdtd:Ljava/util/List;

    return-object v0
.end method

.method public getSleepQuality()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->sleepQuality:I

    return v0
.end method

.method public getUpdateTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->updateTime:Ljava/util/Calendar;

    return-object v0
.end method

.method public getUserDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->userDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public setAwakeCount(I)V
    .locals 0
    .param p1, "awakeCount"    # I

    .prologue
    .line 248
    iput p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->awakeCount:I

    .line 249
    return-void
.end method

.method public setBedTime(J)V
    .locals 5
    .param p1, "bedTime"    # J

    .prologue
    .line 148
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTimeMsec:J

    .line 149
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 150
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 152
    iput-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    .line 153
    iget-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-wide v3, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTimeMsec:J

    sub-long/2addr v1, v3

    long-to-int v1, v1

    div-int/lit16 v1, v1, 0x3e8

    iput v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    .line 156
    :cond_0
    return-void
.end method

.method public setBedTime(Ljava/util/Calendar;)V
    .locals 4
    .param p1, "bedTime"    # Ljava/util/Calendar;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    .line 136
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTimeMsec:J

    .line 137
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTimeMsec:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    .line 140
    :cond_0
    return-void
.end method

.method public setCreateTime(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "createTime"    # Ljava/util/Calendar;

    .prologue
    .line 320
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->createTime:Ljava/util/Calendar;

    .line 321
    return-void
.end method

.method public setDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 230
    iput p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    .line 231
    return-void
.end method

.method public setEfficiency(F)V
    .locals 0
    .param p1, "efficiency"    # F

    .prologue
    .line 173
    iput p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->efficiency:F

    .line 174
    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 117
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->id:J

    .line 118
    return-void
.end method

.method public setMovement(I)V
    .locals 0
    .param p1, "movement"    # I

    .prologue
    .line 284
    iput p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->movement:I

    .line 285
    return-void
.end method

.method public setNoise(Ljava/lang/String;)V
    .locals 0
    .param p1, "noise"    # Ljava/lang/String;

    .prologue
    .line 302
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->noise:Ljava/lang/String;

    .line 303
    return-void
.end method

.method public setRiseTime(J)V
    .locals 5
    .param p1, "riseTime"    # J

    .prologue
    .line 205
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTimeMsec:J

    .line 206
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 207
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 209
    iput-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    .line 210
    iget-object v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    if-eqz v1, :cond_0

    .line 211
    iget-wide v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTimeMsec:J

    iget-object v3, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    sub-long/2addr v1, v3

    long-to-int v1, v1

    div-int/lit16 v1, v1, 0x3e8

    iput v1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    .line 213
    :cond_0
    return-void
.end method

.method public setRiseTime(Ljava/util/Calendar;)V
    .locals 4
    .param p1, "riseTime"    # Ljava/util/Calendar;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    .line 193
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTimeMsec:J

    .line 194
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 195
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTimeMsec:J

    iget-object v2, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    div-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    .line 197
    :cond_0
    return-void
.end method

.method public setSleepDataTrackerDataList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/dataobjects/SleepDataTrackerData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "lsdtd":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/dataobjects/SleepDataTrackerData;>;"
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->lsdtd:Ljava/util/List;

    .line 378
    return-void
.end method

.method public setSleepQuality(I)V
    .locals 0
    .param p1, "sleepQuality"    # I

    .prologue
    .line 266
    iput p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->sleepQuality:I

    .line 267
    return-void
.end method

.method public setUpdateTime(J)V
    .locals 3
    .param p1, "updateTime"    # J

    .prologue
    .line 338
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 339
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 341
    iput-object v0, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->updateTime:Ljava/util/Calendar;

    .line 342
    return-void
.end method

.method public setUpdateTime(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "updateTime"    # Ljava/util/Calendar;

    .prologue
    .line 350
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->updateTime:Ljava/util/Calendar;

    .line 351
    return-void
.end method

.method public setUserDeviceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userDeviceId"    # Ljava/lang/String;

    .prologue
    .line 368
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->userDeviceId:Ljava/lang/String;

    .line 369
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 393
    const-string v3, "\'Date Not Set\'"

    .line 394
    .local v3, "theDate":Ljava/lang/String;
    const-string v0, "\'Bed Time Not Set\'"

    .line 395
    .local v0, "bedTimeStr":Ljava/lang/String;
    const-string v2, "\'Rise Time Not Set\'"

    .line 396
    .local v2, "riseTimeStr":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 398
    .local v4, "toString":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->updateTime:Ljava/util/Calendar;

    if-eqz v5, :cond_0

    .line 399
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->updateTime:Ljava/util/Calendar;

    const-string/jumbo v6, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 402
    :cond_0
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    if-eqz v5, :cond_1

    .line 403
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->bedTime:Ljava/util/Calendar;

    const-string/jumbo v6, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 406
    :cond_1
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    if-eqz v5, :cond_2

    .line 407
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->riseTime:Ljava/util/Calendar;

    const-string/jumbo v6, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v5, v6}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 409
    :cond_2
    const-string v5, "SleepTracker = { For record updateTime "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    const-string v5, ", id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    iget-wide v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->id:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 413
    const-string v5, ", userDeviceId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->userDeviceId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    const-string v5, ", bedTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 416
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    const-string v5, ", riseTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 419
    const-string v5, ", duration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    iget v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->duration:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 421
    const-string v5, ", efficiency: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 422
    iget v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->efficiency:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 423
    const-string v5, ", movement: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    iget v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->movement:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 425
    const-string v5, ", awakeCount: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    iget v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->awakeCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 427
    const-string v5, ", sleepQuality: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 428
    iget v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->sleepQuality:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 429
    const-string/jumbo v5, "}"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->lsdtd:Ljava/util/List;

    if-eqz v5, :cond_3

    .line 432
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->lsdtd:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 433
    const-string v5, "\n   "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 434
    iget-object v5, p0, Lcom/cigna/coach/dataobjects/SleepTrackerData;->lsdtd:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;

    invoke-virtual {v5}, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 438
    .end local v1    # "j":I
    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
