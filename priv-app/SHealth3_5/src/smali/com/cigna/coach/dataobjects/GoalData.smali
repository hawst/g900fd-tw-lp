.class public Lcom/cigna/coach/dataobjects/GoalData;
.super Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;
.source "GoalData.java"


# instance fields
.field contentId:I

.field currentGoalStatusContentId:I

.field displayOrderNumber:I

.field frequencyContentId:I

.field imageContentId:I

.field maxGracePeriodDays:I

.field minMissionFrequencyNumber:I

.field numMaxDaysBetweenMissions:I

.field numMinDaysBetweenMissions:I

.field numMissionsToComplete:I

.field private weightLossGoalInKilograms:I

.field private weightLossGoalInPounds:I

.field private weightLossMaxDurationInWeeks:I

.field private weightLossMinDurationInWeeks:I

.field whyContentId:I

.field whyImageContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/GoalInfoMissionsInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoal"
    .end annotation

    .prologue
    .line 69
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->contentId:I

    return v0
.end method

.method public getCurrentGoalStatusContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCurrentGoalStatus"
    .end annotation

    .prologue
    .line 84
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->currentGoalStatusContentId:I

    return v0
.end method

.method public getDisplayOrderNumber()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->displayOrderNumber:I

    return v0
.end method

.method public getFrequencyContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoalFrequency"
    .end annotation

    .prologue
    .line 115
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->frequencyContentId:I

    return v0
.end method

.method public getImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoalImage"
    .end annotation

    .prologue
    .line 130
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->imageContentId:I

    return v0
.end method

.method public getMaxGracePeriodDays()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->maxGracePeriodDays:I

    return v0
.end method

.method public getMinMissionFrequencyNumber()I
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->minMissionFrequencyNumber:I

    return v0
.end method

.method public getNumMaxDaysBetweenMissions()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->numMaxDaysBetweenMissions:I

    return v0
.end method

.method public getNumMinDaysBetweenMissions()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->numMinDaysBetweenMissions:I

    return v0
.end method

.method public getNumMissionsToComplete()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->numMissionsToComplete:I

    return v0
.end method

.method public getWeightLossGoalInKilograms()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "WEIGHT_TARGET_KG"
    .end annotation

    .prologue
    .line 261
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossGoalInKilograms:I

    return v0
.end method

.method public getWeightLossGoalInPounds()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "WEIGHT_TARGET_LBS"
    .end annotation

    .prologue
    .line 252
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossGoalInPounds:I

    return v0
.end method

.method public getWeightLossMaxDurationInWeeks()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "WEIGHT_END_RANGE_WEEKS"
    .end annotation

    .prologue
    .line 279
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossMaxDurationInWeeks:I

    return v0
.end method

.method public getWeightLossMinDurationInWeeks()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "WEIGHT_START_RANGE_WEEKS"
    .end annotation

    .prologue
    .line 270
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossMinDurationInWeeks:I

    return v0
.end method

.method public getWhyContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setWhyDoIt"
    .end annotation

    .prologue
    .line 221
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->whyContentId:I

    return v0
.end method

.method public getWhyImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setWhyDoItImage"
    .end annotation

    .prologue
    .line 236
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalData;->whyImageContentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->contentId:I

    .line 62
    return-void
.end method

.method public setCurrentGoalStatusContentId(I)V
    .locals 0
    .param p1, "currentGoalStatusContentId"    # I

    .prologue
    .line 76
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->currentGoalStatusContentId:I

    .line 77
    return-void
.end method

.method public setDisplayOrderNumber(I)V
    .locals 0
    .param p1, "displayOrderNumber"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->displayOrderNumber:I

    .line 93
    return-void
.end method

.method public setFrequencyContentId(I)V
    .locals 0
    .param p1, "frequencyContentId"    # I

    .prologue
    .line 107
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->frequencyContentId:I

    .line 108
    return-void
.end method

.method public setImageContentId(I)V
    .locals 0
    .param p1, "imageContentId"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->imageContentId:I

    .line 123
    return-void
.end method

.method public setMaxGracePeriodDays(I)V
    .locals 0
    .param p1, "maxGracePeriodDays"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->maxGracePeriodDays:I

    .line 139
    return-void
.end method

.method public setMinMissionFrequencyNumber(I)V
    .locals 0
    .param p1, "minMissionFrequencyNumber"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->minMissionFrequencyNumber:I

    .line 154
    return-void
.end method

.method public setNumMaxDaysBetweenMissions(I)V
    .locals 0
    .param p1, "numMaxDaysBetweenMissions"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->numMaxDaysBetweenMissions:I

    .line 169
    return-void
.end method

.method public setNumMinDaysBetweenMissions(I)V
    .locals 0
    .param p1, "numMinDaysBetweenMissions"    # I

    .prologue
    .line 183
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->numMinDaysBetweenMissions:I

    .line 184
    return-void
.end method

.method public setNumMissionsToComplete(I)V
    .locals 0
    .param p1, "numMissionsToComplete"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->numMissionsToComplete:I

    .line 199
    return-void
.end method

.method public setWeightLossGoalInKilograms(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 256
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossGoalInKilograms:I

    .line 257
    return-void
.end method

.method public setWeightLossGoalInPounds(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 247
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossGoalInPounds:I

    .line 248
    return-void
.end method

.method public setWeightLossMaxDurationInWeeks(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 274
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossMaxDurationInWeeks:I

    .line 275
    return-void
.end method

.method public setWeightLossMinDurationInWeeks(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 265
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->weightLossMinDurationInWeeks:I

    .line 266
    return-void
.end method

.method public setWhyContentId(I)V
    .locals 0
    .param p1, "whyContentId"    # I

    .prologue
    .line 213
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->whyContentId:I

    .line 214
    return-void
.end method

.method public setWhyImageContentId(I)V
    .locals 0
    .param p1, "whyImageContentId"    # I

    .prologue
    .line 228
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalData;->whyImageContentId:I

    .line 229
    return-void
.end method
