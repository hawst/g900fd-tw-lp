.class public Lcom/cigna/coach/dataobjects/QuestionAnswersData;
.super Lcom/cigna/coach/apiobjects/QuestionAnswers;
.source "QuestionAnswersData.java"


# instance fields
.field private contentId:I

.field private imageContentId:I

.field private trackerSaysContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/QuestionAnswers;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setQuestion"
    .end annotation

    .prologue
    .line 47
    iget v0, p0, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->contentId:I

    return v0
.end method

.method public getImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setQuestionImage"
    .end annotation

    .prologue
    .line 62
    iget v0, p0, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->imageContentId:I

    return v0
.end method

.method public getTrackerSaysContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTrackerSaysLabel"
    .end annotation

    .prologue
    .line 77
    iget v0, p0, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->trackerSaysContentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->contentId:I

    .line 40
    return-void
.end method

.method public setImageContentId(I)V
    .locals 0
    .param p1, "imageContentId"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->imageContentId:I

    .line 55
    return-void
.end method

.method public setTrackerSaysContentId(I)V
    .locals 0
    .param p1, "trackerSaysContentId"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/cigna/coach/dataobjects/QuestionAnswersData;->trackerSaysContentId:I

    .line 70
    return-void
.end method
