.class public final enum Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
.super Ljava/lang/Enum;
.source "JournalEntry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/JournalEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "JournalActionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

.field public static final enum DELETE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

.field public static final enum INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

.field public static final enum TEMP:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

.field public static final enum UPDATE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;",
            ">;"
        }
    .end annotation
.end field

.field private static keyValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field code:Ljava/lang/String;

.field key:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 22
    new-instance v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    const-string v5, "INSERT"

    const-string v6, "I"

    invoke-direct {v4, v5, v11, v8, v6}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    new-instance v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    const-string v5, "UPDATE"

    const-string v6, "U"

    invoke-direct {v4, v5, v8, v9, v6}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->UPDATE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    new-instance v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    const-string v5, "DELETE"

    const-string v6, "D"

    invoke-direct {v4, v5, v9, v10, v6}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->DELETE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    new-instance v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    const-string v5, "TEMP"

    const/16 v6, 0x63

    const-string v7, "T"

    invoke-direct {v4, v5, v10, v6, v7}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->TEMP:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    .line 21
    const/4 v4, 0x4

    new-array v4, v4, [Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    sget-object v5, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->INSERT:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->UPDATE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->DELETE:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->TEMP:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    aput-object v5, v4, v10

    sput-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->$VALUES:[Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    .line 41
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->keyValueMap:Ljava/util/HashMap;

    .line 43
    invoke-static {}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->values()[Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 44
    .local v3, "type":Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    sget-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->keyValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    :cond_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->codeValueMap:Ljava/util/HashMap;

    .line 51
    invoke-static {}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->values()[Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 52
    .restart local v3    # "type":Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    sget-object v4, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->getCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 54
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    :cond_1
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .param p3, "sKey"    # I
    .param p4, "sCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->key:I

    .line 25
    const-string v0, "?"

    iput-object v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->code:Ljava/lang/String;

    .line 28
    iput p3, p0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->key:I

    .line 29
    iput-object p4, p0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->code:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    .locals 2
    .param p0, "key"    # I

    .prologue
    .line 57
    sget-object v0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->keyValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    .locals 1
    .param p0, "code"    # Ljava/lang/String;

    .prologue
    .line 61
    sget-object v0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->$VALUES:[Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    return-object v0
.end method


# virtual methods
.method public getCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->code:Ljava/lang/String;

    return-object v0
.end method

.method public getKey()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;->key:I

    return v0
.end method
