.class public Lcom/cigna/coach/dataobjects/UserGoalData;
.super Lcom/cigna/coach/dataobjects/GoalData;
.source "UserGoalData.java"


# instance fields
.field categoryScore:I

.field goalSequenceId:I

.field lastMissionCountedDate:Ljava/util/Calendar;

.field numMissionsCompleted:I

.field questionGroupScore:I

.field startDate:Ljava/util/Calendar;

.field status:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

.field userId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/cigna/coach/dataobjects/GoalData;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategoryScore()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->categoryScore:I

    return v0
.end method

.method public getGoalSequenceId()I
    .locals 1

    .prologue
    .line 152
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->goalSequenceId:I

    return v0
.end method

.method public getLastMissionCountedDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->lastMissionCountedDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getNumMissionsCompleted()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->numMissionsCompleted:I

    return v0
.end method

.method public getQuestionGroupScore()I
    .locals 1

    .prologue
    .line 106
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->questionGroupScore:I

    return v0
.end method

.method public getStartDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->startDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getStatus()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->status:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public setCategoryScore(I)V
    .locals 0
    .param p1, "categoryScore"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->categoryScore:I

    .line 125
    return-void
.end method

.method public setGoalSequenceId(I)V
    .locals 0
    .param p1, "goalSequenceId"    # I

    .prologue
    .line 155
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->goalSequenceId:I

    .line 156
    return-void
.end method

.method public setLastMissionCountedDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "lastMissionCountedDate"    # Ljava/util/Calendar;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->lastMissionCountedDate:Ljava/util/Calendar;

    .line 101
    return-void
.end method

.method public setNumMissionsCompleted(I)V
    .locals 0
    .param p1, "numMissionsCompleted"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->numMissionsCompleted:I

    .line 89
    return-void
.end method

.method public setQuestionGroupScore(I)V
    .locals 0
    .param p1, "questionGroupScore"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->questionGroupScore:I

    .line 113
    return-void
.end method

.method public setStartDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "startDate"    # Ljava/util/Calendar;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->startDate:Ljava/util/Calendar;

    .line 65
    return-void
.end method

.method public setStatus(Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;)V
    .locals 1
    .param p1, "status"    # Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->status:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    .line 138
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p1, v0, :cond_1

    .line 140
    const/16 v0, 0xf5c

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCurrentGoalStatusContentId(I)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->USER_CANCELED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->LSR_CANCELLED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p1, v0, :cond_3

    .line 144
    :cond_2
    const/16 v0, 0x1015

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCurrentGoalStatusContentId(I)V

    goto :goto_0

    .line 146
    :cond_3
    sget-object v0, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;->ACTIVE:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalStatus;

    if-ne p1, v0, :cond_0

    .line 148
    const/16 v0, 0xf30

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserGoalData;->setCurrentGoalStatusContentId(I)V

    goto :goto_0
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserGoalData;->userId:Ljava/lang/String;

    .line 53
    return-void
.end method
