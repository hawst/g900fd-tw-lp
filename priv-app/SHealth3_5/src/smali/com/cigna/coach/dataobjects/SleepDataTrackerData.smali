.class public Lcom/cigna/coach/dataobjects/SleepDataTrackerData;
.super Ljava/lang/Object;
.source "SleepDataTrackerData.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private createTime:Ljava/util/Calendar;

.field private sampleTime:J

.field private sleepId:J

.field private sleepStatus:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepId:J

    .line 45
    iput-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepStatus:J

    .line 48
    iput-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sampleTime:J

    .line 57
    return-void
.end method


# virtual methods
.method public getCreateTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->createTime:Ljava/util/Calendar;

    return-object v0
.end method

.method public getSampleTime()J
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sampleTime:J

    return-wide v0
.end method

.method public getSleepId()J
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepId:J

    return-wide v0
.end method

.method public getSleepStatus()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepStatus:J

    return-wide v0
.end method

.method public setCreateTime(J)V
    .locals 3
    .param p1, "createTime"    # J

    .prologue
    .line 140
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 141
    .local v0, "gc":Ljava/util/GregorianCalendar;
    invoke-virtual {v0, p1, p2}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 143
    iput-object v0, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->createTime:Ljava/util/Calendar;

    .line 144
    return-void
.end method

.method public setCreateTime(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "createTime"    # Ljava/util/Calendar;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->createTime:Ljava/util/Calendar;

    .line 132
    return-void
.end method

.method public setSampleTime(J)V
    .locals 0
    .param p1, "sampleTime"    # J

    .prologue
    .line 111
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sampleTime:J

    .line 112
    return-void
.end method

.method public setSleepId(J)V
    .locals 0
    .param p1, "sleepId"    # J

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepId:J

    .line 75
    return-void
.end method

.method public setSleepStatus(J)V
    .locals 0
    .param p1, "sleepStatus"    # J

    .prologue
    .line 92
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepStatus:J

    .line 93
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 151
    const-string v2, "\'Date Not Set\'"

    .line 152
    .local v2, "theDate":Ljava/lang/String;
    const-string v1, "\'Date Not Set\'"

    .line 154
    .local v1, "sampleTimeDate":Ljava/lang/String;
    iget-object v3, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->createTime:Ljava/util/Calendar;

    if-eqz v3, :cond_0

    .line 155
    iget-object v3, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->createTime:Ljava/util/Calendar;

    const-string/jumbo v4, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v3, v4}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 158
    :cond_0
    iget-wide v3, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sampleTime:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_1

    .line 159
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;Ljava/util/Locale;)V

    .line 160
    .local v0, "gc":Ljava/util/GregorianCalendar;
    iget-wide v3, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sampleTime:J

    invoke-virtual {v0, v3, v4}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 161
    const-string/jumbo v3, "yyyy-MM-dd HH:mm:ss"

    invoke-static {v0, v3}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165
    .end local v0    # "gc":Ljava/util/GregorianCalendar;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SleepDataTracker = { For record createTime "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sleepId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sleepStatus: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sleepStatus:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sampleTime: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/cigna/coach/dataobjects/SleepDataTrackerData;->sampleTime:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
