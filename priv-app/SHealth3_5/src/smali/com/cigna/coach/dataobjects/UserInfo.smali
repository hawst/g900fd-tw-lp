.class public Lcom/cigna/coach/dataobjects/UserInfo;
.super Lcom/cigna/coach/apiobjects/UserMetrics;
.source "UserInfo.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private userId:Ljava/lang/String;

.field private userKey:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/UserMetrics;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "otherObject"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-super {p0, p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v1

    .line 86
    :cond_1
    instance-of v2, p1, Lcom/cigna/coach/dataobjects/UserInfo;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 87
    check-cast v0, Lcom/cigna/coach/dataobjects/UserInfo;

    .line 88
    .local v0, "other":Lcom/cigna/coach/dataobjects/UserInfo;
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserKey()I

    move-result v2

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserKey()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/cigna/coach/dataobjects/UserInfo;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserInfo;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public getUserKey()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserInfo;->userKey:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 94
    invoke-super {p0}, Lcom/cigna/coach/apiobjects/UserMetrics;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/UserInfo;->userKey:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/UserInfo;->userId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public populateFromUserMetrics(Lcom/cigna/coach/apiobjects/UserMetrics;)V
    .locals 2
    .param p1, "userMetrics"    # Lcom/cigna/coach/apiobjects/UserMetrics;

    .prologue
    .line 66
    if-eqz p1, :cond_2

    .line 67
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getCountry()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid country code. Country code cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getDateOfBirth()Ljava/util/Calendar;

    move-result-object v0

    if-nez v0, :cond_1

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid date of birth. Date of Birth cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_1
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserInfo;->setCountry(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getDateOfBirth()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserInfo;->setDateOfBirth(Ljava/util/Calendar;)V

    .line 74
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getGender()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserInfo;->setGender(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricGender;)V

    .line 75
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getMaritalStatus()Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserInfo;->setMaritalStatus(Lcom/cigna/coach/apiobjects/UserMetrics$UserMetricMaritalStatus;)V

    .line 76
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getHeight()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserInfo;->setHeight(F)V

    .line 77
    invoke-virtual {p1}, Lcom/cigna/coach/apiobjects/UserMetrics;->getWeight()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/UserInfo;->setWeight(F)V

    .line 81
    return-void

    .line 79
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "UserMetrics cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserInfo;->userId:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setUserKey(I)V
    .locals 0
    .param p1, "userKey"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserInfo;->userKey:I

    .line 60
    return-void
.end method
