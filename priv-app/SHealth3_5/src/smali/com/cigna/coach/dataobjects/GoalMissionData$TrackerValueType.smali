.class public final enum Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
.super Ljava/lang/Enum;
.source "GoalMissionData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/GoalMissionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrackerValueType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

.field public static final enum NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

.field public static final enum NO_OF_MINS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

.field public static final enum NO_OF_RUNNING_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

.field public static final enum NO_OF_TOTAL_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

.field public static final enum NO_OF_UP_DOWN_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

.field public static final enum NO_OF_WALKING_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field trackerRule:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 76
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    const-string v5, "NONE"

    const/4 v6, -0x1

    invoke-direct {v4, v5, v7, v6}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    const-string v5, "NO_OF_MINS"

    invoke-direct {v4, v5, v8, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_MINS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    const-string v5, "NO_OF_TOTAL_STEPS"

    invoke-direct {v4, v5, v9, v8}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_TOTAL_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    const-string v5, "NO_OF_RUNNING_STEPS"

    invoke-direct {v4, v5, v10, v9}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_RUNNING_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    const-string v5, "NO_OF_UP_DOWN_STEPS"

    invoke-direct {v4, v5, v11, v10}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_UP_DOWN_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    const-string v5, "NO_OF_WALKING_STEPS"

    const/4 v6, 0x5

    invoke-direct {v4, v5, v6, v11}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_WALKING_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    .line 74
    const/4 v4, 0x6

    new-array v4, v4, [Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_MINS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_TOTAL_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_RUNNING_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_UP_DOWN_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    aput-object v5, v4, v11

    const/4 v5, 0x5

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->NO_OF_WALKING_STEPS:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    .line 89
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->codeValueMap:Ljava/util/HashMap;

    .line 91
    invoke-static {}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->values()[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 92
    .local v3, "type":Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
    sget-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->getIntValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 94
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "trackerRule"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 81
    iput p3, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->trackerRule:I

    .line 82
    return-void
.end method

.method public static getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
    .locals 2
    .param p0, "trackerRule"    # I

    .prologue
    .line 97
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const-class v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerValueType;->trackerRule:I

    return v0
.end method
