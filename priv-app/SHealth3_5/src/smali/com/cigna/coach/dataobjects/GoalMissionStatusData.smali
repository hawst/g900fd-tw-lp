.class public Lcom/cigna/coach/dataobjects/GoalMissionStatusData;
.super Lcom/cigna/coach/apiobjects/GoalMissionStatus;
.source "GoalMissionStatusData.java"


# static fields
.field static final goalCompleteAccoladeContentId:I = 0xf7b

.field static final goalSuccessContentId:I = 0xf7c

.field static final missionCompleteAccoladeContentId:I = 0xf79

.field static final missionCompleteDescirptionContentId:I = 0xf77

.field static final missionProgressAccoladeContentId:I = 0xf78

.field static final missionProgressDescriptionContentId:I = 0xf76

.field static final missionProgressDetailContentId:I = 0xf7e

.field static final statusJoinContentId:I = 0xf7a


# instance fields
.field goalContentId:I

.field goalFrequencyContentId:I

.field goalImageContentId:I

.field missionContentId:I

.field numActivitiesCurrentlyUpdated:I

.field trackerName:Ljava/lang/String;

.field trackerNameContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/GoalMissionStatus;-><init>()V

    return-void
.end method


# virtual methods
.method public getChangedMissionActivityCount()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "activity_change"
    .end annotation

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getNumActivitiesCurrentlyUpdated()I

    move-result v0

    return v0
.end method

.method public getGoalContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoal"
    .end annotation

    .prologue
    .line 105
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->goalContentId:I

    return v0
.end method

.method public getGoalFrequencyContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoalFrequency"
    .end annotation

    .prologue
    .line 115
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->goalFrequencyContentId:I

    return v0
.end method

.method public getGoalImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoalImage"
    .end annotation

    .prologue
    .line 175
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->goalImageContentId:I

    return v0
.end method

.method public getGoalProgressAccoladeContentId()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoalStatusAccolade"
    .end annotation

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const/16 v0, 0xf7b

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGoalProgressDescriptionContentId()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoalProgressStatusDescription"
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/16 v0, 0xf7c

    .line 69
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMissionContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMission"
    .end annotation

    .prologue
    .line 110
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->missionContentId:I

    return v0
.end method

.method public getMissionProgressAccoladeContentId()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMissionStatusAccolade"
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const/16 v0, 0xf78

    .line 80
    :goto_0
    return v0

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    const/16 v0, 0xf79

    goto :goto_0

    .line 80
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMissionProgressDescriptionContentId()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMissionProgressStatusDescription"
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const/16 v0, 0xf76

    .line 51
    :goto_0
    return v0

    .line 48
    :cond_0
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const/16 v0, 0xf77

    goto :goto_0

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMissionProgressDetailContentId()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setMissionProgressDetail"
    .end annotation

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->PROGRESSED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const/16 v0, 0xf7e

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNumActivitiesCompletedInMission()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "activity_completed"
    .end annotation

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionActivityCompletedTotalCount()I

    move-result v0

    return v0
.end method

.method public getNumActivitiesCurrentlyUpdated()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->numActivitiesCurrentlyUpdated:I

    return v0
.end method

.method public getRemainingMissionActivityCount()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "activity_outstanding"
    .end annotation

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionActivityTotalCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionActivityCompletedTotalCount()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getStatusJoinContentId()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setGoalStatusTransition"
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getGoalProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$GoalProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->getMissionProgressStatusType()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    move-result-object v0

    sget-object v1, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->COMPLETED:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;

    invoke-virtual {v0, v1}, Lcom/cigna/coach/interfaces/IGoalsMissions$MissionProgressStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    const/16 v0, 0xf7a

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTrackerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->trackerName:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackerNameContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTrackerName"
    .end annotation

    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "tracker_info"
        tagValueType = .enum Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;->CONTENT_ID:Lcom/cigna/coach/utils/CoachContentTagValue$TagValueType;
    .end annotation

    .prologue
    .line 203
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->trackerNameContentId:I

    return v0
.end method

.method public setGoalContentId(I)V
    .locals 0
    .param p1, "goalContentId"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->goalContentId:I

    .line 145
    return-void
.end method

.method public setGoalFrequencyContentId(I)V
    .locals 0
    .param p1, "goalFrequencyContentId"    # I

    .prologue
    .line 159
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->goalFrequencyContentId:I

    .line 160
    return-void
.end method

.method public setGoalImageContentId(I)V
    .locals 0
    .param p1, "goalImageContentId"    # I

    .prologue
    .line 182
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->goalImageContentId:I

    .line 183
    return-void
.end method

.method public setMissionContentId(I)V
    .locals 0
    .param p1, "missionContentId"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->missionContentId:I

    .line 152
    return-void
.end method

.method public setNumActivitiesCurrentlyUpdated(I)V
    .locals 0
    .param p1, "numActivitiesCurrentlyUpdated"    # I

    .prologue
    .line 167
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->numActivitiesCurrentlyUpdated:I

    .line 168
    return-void
.end method

.method public setTrackerName(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackerName"    # Ljava/lang/String;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->trackerName:Ljava/lang/String;

    .line 196
    return-void
.end method

.method public setTrackerNameContentId(I)V
    .locals 0
    .param p1, "trackerNameContentId"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/cigna/coach/dataobjects/GoalMissionStatusData;->trackerNameContentId:I

    .line 211
    return-void
.end method
