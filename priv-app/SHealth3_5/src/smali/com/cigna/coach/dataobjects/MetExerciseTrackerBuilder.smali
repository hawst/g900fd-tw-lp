.class public Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
.super Lcom/cigna/coach/dataobjects/MetExerciseTracker;
.source "MetExerciseTrackerBuilder.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private exerciseId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTracker;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    invoke-direct {v0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;-><init>()V

    .line 42
    .local v0, "metb":Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getDateInMsec()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setDateInMsec(J)V

    .line 43
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getDistance()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setDistance(F)V

    .line 44
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getDurationMin()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setDurationMin(I)V

    .line 45
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setExerciseId(J)V

    .line 46
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseInfoId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setExerciseInfoId(I)V

    .line 47
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getExerciseType()Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setExerciseType(Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;)V

    .line 48
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getMetName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setMetName(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getMetValue()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setMetValue(F)V

    .line 50
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getRunningSteps()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setRunningSteps(I)V

    .line 51
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getSamsungExerciseId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setSamsungExerciseId(I)V

    .line 52
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getSpeed()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setSpeed(F)V

    .line 53
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getTotalSteps()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setTotalSteps(I)V

    .line 54
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getUpdownSteps()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setUpdownSteps(I)V

    .line 55
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getUserDeviceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setUserDeviceId(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->getWalkingSteps()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->setWalkingSteps(I)V

    .line 58
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->clone()Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;

    move-result-object v0

    return-object v0
.end method

.method public getExerciseId()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->exerciseId:J

    return-wide v0
.end method

.method public setExerciseId(J)V
    .locals 0
    .param p1, "exerciseId"    # J

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/MetExerciseTrackerBuilder;->exerciseId:J

    .line 77
    return-void
.end method
