.class public Lcom/cigna/coach/dataobjects/AnswerData;
.super Lcom/cigna/coach/apiobjects/Answer;
.source "AnswerData.java"


# instance fields
.field private answerSource:Lcom/cigna/coach/LifeStyle$AnswerSource;

.field private contentId:I

.field private summaryContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/Answer;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnswerSource()Lcom/cigna/coach/LifeStyle$AnswerSource;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/AnswerData;->answerSource:Lcom/cigna/coach/LifeStyle$AnswerSource;

    return-object v0
.end method

.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setAnswer"
    .end annotation

    .prologue
    .line 40
    iget v0, p0, Lcom/cigna/coach/dataobjects/AnswerData;->contentId:I

    return v0
.end method

.method public getSummaryContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setAnswerSummary"
    .end annotation

    .prologue
    .line 53
    iget v0, p0, Lcom/cigna/coach/dataobjects/AnswerData;->summaryContentId:I

    return v0
.end method

.method public setAnswerSource(Lcom/cigna/coach/LifeStyle$AnswerSource;)V
    .locals 0
    .param p1, "answerSource"    # Lcom/cigna/coach/LifeStyle$AnswerSource;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/AnswerData;->answerSource:Lcom/cigna/coach/LifeStyle$AnswerSource;

    .line 66
    return-void
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/cigna/coach/dataobjects/AnswerData;->contentId:I

    .line 49
    return-void
.end method

.method public setSummaryContentId(I)V
    .locals 0
    .param p1, "summaryContentId"    # I

    .prologue
    .line 57
    iput p1, p0, Lcom/cigna/coach/dataobjects/AnswerData;->summaryContentId:I

    .line 58
    return-void
.end method
