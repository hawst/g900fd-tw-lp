.class public Lcom/cigna/coach/dataobjects/UserMissionData;
.super Lcom/cigna/coach/dataobjects/GoalMissionData;
.source "UserMissionData.java"


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field expirationDate:Ljava/util/Calendar;

.field goalSequenceId:I

.field isDisplayedToUser:Z

.field missionFailedReason:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

.field selectedFrequencyDays:I

.field startDate:Ljava/util/Calendar;

.field trackerNameContentId:I

.field userId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/cigna/coach/dataobjects/UserMissionData;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/dataobjects/UserMissionData;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/cigna/coach/dataobjects/GoalMissionData;-><init>()V

    return-void
.end method


# virtual methods
.method public getExpirationDate()Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 160
    iget-object v1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->expirationDate:Ljava/util/Calendar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->expirationDate:Ljava/util/Calendar;

    .line 180
    :goto_0
    return-object v1

    .line 162
    :cond_0
    sget-object v1, Lcom/cigna/coach/dataobjects/UserMissionData;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 163
    sget-object v1, Lcom/cigna/coach/dataobjects/UserMissionData;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getExpirationDate: START. mission: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getMissionId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; goal: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getGoalId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v1

    invoke-static {v1}, Lcom/cigna/coach/utils/CalendarUtil;->toDefaultTimeZone(Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object v0

    .line 168
    .local v0, "date":Ljava/util/Calendar;
    sget-object v1, Lcom/cigna/coach/dataobjects/UserMissionData;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 169
    sget-object v1, Lcom/cigna/coach/dataobjects/UserMissionData;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getExpirationDate: start date is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getStartDate()Ljava/util/Calendar;

    move-result-object v3

    invoke-static {v3}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " in device time zone)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    :cond_2
    const/4 v1, 0x5

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/UserMissionData;->getSpanInDays()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 174
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/CalendarUtil;->getStartOfDay(J)Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->expirationDate:Ljava/util/Calendar;

    .line 176
    sget-object v1, Lcom/cigna/coach/dataobjects/UserMissionData;->CLASS_NAME:Ljava/lang/String;

    invoke-static {v1}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 177
    sget-object v1, Lcom/cigna/coach/dataobjects/UserMissionData;->CLASS_NAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getExpirationDate: END. computed expiration date: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->expirationDate:Ljava/util/Calendar;

    invoke-static {v3}, Lcom/cigna/coach/utils/CalendarUtil;->formatCalendarAsString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    :cond_3
    iget-object v1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->expirationDate:Ljava/util/Calendar;

    goto/16 :goto_0
.end method

.method public getGoalSequenceId()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->goalSequenceId:I

    return v0
.end method

.method public getMissionFailedReason()Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->missionFailedReason:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    return-object v0
.end method

.method public getSelectedFrequencyDays()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->selectedFrequencyDays:I

    return v0
.end method

.method public getStartDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->startDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getTrackerNameContentId()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->trackerNameContentId:I

    return v0
.end method

.method public getUserFrequency()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "frequency"
    .end annotation

    .prologue
    .line 125
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->selectedFrequencyDays:I

    return v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public isDisplayedToUser()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->isDisplayedToUser:Z

    return v0
.end method

.method public setDisplayedToUser(Z)V
    .locals 0
    .param p1, "isDisplayedToUser"    # Z

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->isDisplayedToUser:Z

    .line 108
    return-void
.end method

.method public setGoalSequenceId(I)V
    .locals 0
    .param p1, "goalSequenceId"    # I

    .prologue
    .line 150
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->goalSequenceId:I

    .line 151
    return-void
.end method

.method public setMissionFailedReason(Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;)V
    .locals 0
    .param p1, "missionFailedReason"    # Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->missionFailedReason:Lcom/cigna/coach/interfaces/IGoalsMissions$MissionFailedReason;

    .line 133
    return-void
.end method

.method public setSelectedFrequencyDays(I)V
    .locals 0
    .param p1, "selectedFrequencyDays"    # I

    .prologue
    .line 120
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->selectedFrequencyDays:I

    .line 121
    return-void
.end method

.method public setStartDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "startDate"    # Ljava/util/Calendar;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->startDate:Ljava/util/Calendar;

    .line 84
    return-void
.end method

.method public setTrackerNameContentId(I)V
    .locals 0
    .param p1, "trackerNameContentId"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->trackerNameContentId:I

    .line 145
    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/UserMissionData;->userId:Ljava/lang/String;

    .line 72
    return-void
.end method
