.class public final enum Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
.super Ljava/lang/Enum;
.source "MetExerciseTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/MetExerciseTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ExerciseType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

.field public static final enum ACTIVITY:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

.field public static final enum FITNESS:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

.field public static final enum NOT_DEFINED:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

.field public static final enum PEDOMETER:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field exerciseType:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 60
    new-instance v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    const-string v5, "ACTIVITY"

    const/16 v6, 0x4e21

    invoke-direct {v4, v5, v7, v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->ACTIVITY:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 61
    new-instance v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    const-string v5, "FITNESS"

    const/16 v6, 0x4e22

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->FITNESS:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 62
    new-instance v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    const-string v5, "PEDOMETER"

    const/16 v6, 0x4e23

    invoke-direct {v4, v5, v9, v6}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->PEDOMETER:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 63
    new-instance v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    const-string v5, "NOT_DEFINED"

    invoke-direct {v4, v5, v10, v7}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->NOT_DEFINED:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 59
    const/4 v4, 0x4

    new-array v4, v4, [Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    sget-object v5, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->ACTIVITY:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->FITNESS:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->PEDOMETER:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->NOT_DEFINED:Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    aput-object v5, v4, v10

    sput-object v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->$VALUES:[Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    .line 75
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->codeValueMap:Ljava/util/HashMap;

    .line 77
    invoke-static {}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->values()[Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 78
    .local v3, "type":Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    sget-object v4, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->getExerciseType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "eType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    iput p3, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->exerciseType:I

    .line 71
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .locals 2
    .param p0, "exerciseTypeKey"    # I

    .prologue
    .line 89
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 59
    const-class v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->$VALUES:[Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;

    return-object v0
.end method


# virtual methods
.method public getExerciseType()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/cigna/coach/dataobjects/MetExerciseTracker$ExerciseType;->exerciseType:I

    return v0
.end method
