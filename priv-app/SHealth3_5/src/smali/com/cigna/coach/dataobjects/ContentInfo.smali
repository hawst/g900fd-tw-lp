.class public Lcom/cigna/coach/dataobjects/ContentInfo;
.super Ljava/lang/Object;
.source "ContentInfo.java"


# instance fields
.field private contentId:I

.field private contentMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private requestedLocaleType:Lcom/cigna/coach/utils/ContentHelper$LocaleType;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "contentId"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->requestedLocaleType:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->contentMap:Ljava/util/Map;

    .line 24
    iput p1, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->contentId:I

    .line 25
    return-void
.end method


# virtual methods
.method public addContent(Lcom/cigna/coach/utils/ContentHelper$LocaleType;Ljava/lang/String;)V
    .locals 1
    .param p1, "locale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .param p2, "content"    # Ljava/lang/String;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->contentMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    return-void
.end method

.method public getContentForLocale(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)Ljava/lang/String;
    .locals 1
    .param p1, "locale"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->contentMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getContentForRequestedLocale()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->requestedLocaleType:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->contentMap:Ljava/util/Map;

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->requestedLocaleType:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 39
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getContentId()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->contentId:I

    return v0
.end method

.method public getContentMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/utils/ContentHelper$LocaleType;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->contentMap:Ljava/util/Map;

    return-object v0
.end method

.method public getRequestedLocaleType()Lcom/cigna/coach/utils/ContentHelper$LocaleType;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->requestedLocaleType:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    return-object v0
.end method

.method public setRequestedLocaleType(Lcom/cigna/coach/utils/ContentHelper$LocaleType;)V
    .locals 0
    .param p1, "requestedLocaleType"    # Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ContentInfo;->requestedLocaleType:Lcom/cigna/coach/utils/ContentHelper$LocaleType;

    .line 57
    return-void
.end method
