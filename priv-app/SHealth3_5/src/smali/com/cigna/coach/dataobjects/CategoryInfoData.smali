.class public Lcom/cigna/coach/dataobjects/CategoryInfoData;
.super Lcom/cigna/coach/apiobjects/CategoryInfo;
.source "CategoryInfoData.java"


# instance fields
.field private contentId:I

.field private imageContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/CategoryInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCategory"
    .end annotation

    .prologue
    .line 47
    iget v0, p0, Lcom/cigna/coach/dataobjects/CategoryInfoData;->contentId:I

    return v0
.end method

.method public getImageContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCategoryImage"
    .end annotation

    .prologue
    .line 62
    iget v0, p0, Lcom/cigna/coach/dataobjects/CategoryInfoData;->imageContentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/cigna/coach/dataobjects/CategoryInfoData;->contentId:I

    .line 40
    return-void
.end method

.method public setImageContentId(I)V
    .locals 0
    .param p1, "imageContentId"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/cigna/coach/dataobjects/CategoryInfoData;->imageContentId:I

    .line 55
    return-void
.end method
