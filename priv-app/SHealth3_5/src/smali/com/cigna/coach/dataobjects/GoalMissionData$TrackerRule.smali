.class public final enum Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
.super Ljava/lang/Enum;
.source "GoalMissionData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/GoalMissionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TrackerRule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum MODERATE_EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum MODERATE_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum RUNNING_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum STRECHING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum STRECHING_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum STRENGTH_TRANING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum STRENGTH_TRANING_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum UP_DOWN_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum VIGOROUS_EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum VIGOROUS_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum WALKING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum WALKING_10000:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum WALKING_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum WALKING_30:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field public static final enum WALKING_60:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field trackerRule:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 34
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "NONE"

    const/4 v6, 0x0

    const/4 v7, -0x1

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 35
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "MODERATE_EXCERCISE"

    invoke-direct {v4, v5, v8, v8}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->MODERATE_EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 36
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "VIGOROUS_EXCERCISE"

    invoke-direct {v4, v5, v9, v9}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->VIGOROUS_EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 37
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "STRECHING"

    invoke-direct {v4, v5, v10, v10}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRECHING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 38
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "STRENGTH_TRANING"

    invoke-direct {v4, v5, v11, v11}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRENGTH_TRANING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 39
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "MODERATE_SPECIFIC"

    invoke-direct {v4, v5, v12, v12}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->MODERATE_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 40
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "VIGOROUS_SPECIFIC"

    const/4 v6, 0x6

    const/4 v7, 0x6

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->VIGOROUS_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 41
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "STRECHING_SPECIFIC"

    const/4 v6, 0x7

    const/4 v7, 0x7

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRECHING_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 42
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "STRENGTH_TRANING_SPECIFIC"

    const/16 v6, 0x8

    const/16 v7, 0x8

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRENGTH_TRANING_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 43
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "WALKING"

    const/16 v6, 0x9

    const/16 v7, 0x9

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 44
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "WALKING_20"

    const/16 v6, 0xa

    const/16 v7, 0xa

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 45
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "WALKING_30"

    const/16 v6, 0xb

    const/16 v7, 0xb

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_30:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 46
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "WALKING_60"

    const/16 v6, 0xc

    const/16 v7, 0xc

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_60:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 47
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "WALKING_10000"

    const/16 v6, 0xd

    const/16 v7, 0xd

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_10000:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 48
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "RUNNING_20"

    const/16 v6, 0xe

    const/16 v7, 0xe

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->RUNNING_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 49
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const-string v5, "UP_DOWN_20"

    const/16 v6, 0xf

    const/16 v7, 0xf

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->UP_DOWN_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 33
    const/16 v4, 0x10

    new-array v4, v4, [Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->MODERATE_EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->VIGOROUS_EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRECHING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRENGTH_TRANING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->MODERATE_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v5, v4, v12

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->VIGOROUS_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRECHING_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->STRENGTH_TRANING_SPECIFIC:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_30:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_60:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->WALKING_10000:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0xe

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->RUNNING_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    const/16 v5, 0xf

    sget-object v6, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->UP_DOWN_20:Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    .line 63
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->codeValueMap:Ljava/util/HashMap;

    .line 65
    invoke-static {}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->values()[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 66
    .local v3, "type":Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    sget-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->getIntValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "trackerRule"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput p3, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->trackerRule:I

    .line 56
    return-void
.end method

.method public static getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    .locals 2
    .param p0, "trackerRule"    # I

    .prologue
    .line 71
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 33
    const-class v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$TrackerRule;->trackerRule:I

    return v0
.end method
