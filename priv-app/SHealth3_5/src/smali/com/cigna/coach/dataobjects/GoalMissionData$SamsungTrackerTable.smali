.class public final enum Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
.super Ljava/lang/Enum;
.source "GoalMissionData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/GoalMissionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SamsungTrackerTable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

.field public static final enum EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

.field public static final enum NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

.field public static final enum PEDOMETER:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field trackerRule:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 103
    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    const-string v5, "NONE"

    const/4 v6, -0x1

    invoke-direct {v4, v5, v9, v6}, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    const-string v5, "EXCERCISE"

    invoke-direct {v4, v5, v7, v7}, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    new-instance v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    const-string v5, "PEDOMETER"

    invoke-direct {v4, v5, v8, v8}, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->PEDOMETER:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    .line 101
    const/4 v4, 0x3

    new-array v4, v4, [Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->NONE:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    aput-object v5, v4, v9

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->EXCERCISE:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->PEDOMETER:Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    aput-object v5, v4, v8

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    .line 116
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->codeValueMap:Ljava/util/HashMap;

    .line 118
    invoke-static {}, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->values()[Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 119
    .local v3, "type":Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
    sget-object v4, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->getIntValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 121
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "trackerRule"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 108
    iput p3, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->trackerRule:I

    .line 109
    return-void
.end method

.method public static getEnumValue(I)Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
    .locals 2
    .param p0, "trackerRule"    # I

    .prologue
    .line 124
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 101
    const-class v0, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->$VALUES:[Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/cigna/coach/dataobjects/GoalMissionData$SamsungTrackerTable;->trackerRule:I

    return v0
.end method
