.class public Lcom/cigna/coach/dataobjects/NotificationStateData;
.super Ljava/lang/Object;
.source "NotificationStateData.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private lastStartupDate:Ljava/util/Calendar;

.field private lastTimeChangeDate:Ljava/util/Calendar;

.field private notificationEndTime:J

.field private notificationStartTime:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const-wide/16 v1, -0x1

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->lastStartupDate:Ljava/util/Calendar;

    .line 54
    iput-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->lastTimeChangeDate:Ljava/util/Calendar;

    .line 55
    iput-wide v1, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->notificationStartTime:J

    .line 56
    iput-wide v1, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->notificationEndTime:J

    .line 57
    return-void
.end method


# virtual methods
.method public getLastStartupDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->lastStartupDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getLastTimeChangeDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->lastTimeChangeDate:Ljava/util/Calendar;

    return-object v0
.end method

.method public getNotificationEndTime()J
    .locals 2

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->notificationEndTime:J

    return-wide v0
.end method

.method public getNotificationStartTime()J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->notificationStartTime:J

    return-wide v0
.end method

.method public setLastStartupDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "lastStartupDate"    # Ljava/util/Calendar;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->lastStartupDate:Ljava/util/Calendar;

    .line 77
    return-void
.end method

.method public setLastTimeChangeDate(Ljava/util/Calendar;)V
    .locals 0
    .param p1, "lastTimeChangeDate"    # Ljava/util/Calendar;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->lastTimeChangeDate:Ljava/util/Calendar;

    .line 97
    return-void
.end method

.method public setNotificationEndTime(J)V
    .locals 0
    .param p1, "notificationEndTime"    # J

    .prologue
    .line 140
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->notificationEndTime:J

    .line 141
    return-void
.end method

.method public setNotificationStartTime(J)V
    .locals 0
    .param p1, "notificationStartTime"    # J

    .prologue
    .line 118
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/NotificationStateData;->notificationStartTime:J

    .line 119
    return-void
.end method
