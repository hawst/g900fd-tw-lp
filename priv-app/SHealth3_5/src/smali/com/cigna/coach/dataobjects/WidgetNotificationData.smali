.class public Lcom/cigna/coach/dataobjects/WidgetNotificationData;
.super Ljava/lang/Object;
.source "WidgetNotificationData.java"


# static fields
.field public static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private coachMessageData:Lcom/cigna/coach/dataobjects/CoachMessageData;

.field private displayRank:I

.field private intent:Landroid/content/Intent;

.field private seqId:I

.field private uniqueKey:Ljava/lang/String;

.field private userId:I

.field private widgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

.field private widgetType:Lcom/cigna/coach/interfaces/IWidget$WidgetType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCoachMessageData()Lcom/cigna/coach/dataobjects/CoachMessageData;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->coachMessageData:Lcom/cigna/coach/dataobjects/CoachMessageData;

    return-object v0
.end method

.method public getDisplayRank()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->displayRank:I

    return v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public getSeqId()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->seqId:I

    return v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->uniqueKey:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->userId:I

    return v0
.end method

.method public getWidgetInfoType()Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->widgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    return-object v0
.end method

.method public getWidgetType()Lcom/cigna/coach/interfaces/IWidget$WidgetType;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->widgetType:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    return-object v0
.end method

.method public setCoachMessageData(Lcom/cigna/coach/dataobjects/CoachMessageData;)V
    .locals 0
    .param p1, "coachMessageData"    # Lcom/cigna/coach/dataobjects/CoachMessageData;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->coachMessageData:Lcom/cigna/coach/dataobjects/CoachMessageData;

    .line 100
    return-void
.end method

.method public setDisplayRank(I)V
    .locals 0
    .param p1, "displayRank"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->displayRank:I

    .line 70
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->intent:Landroid/content/Intent;

    .line 115
    return-void
.end method

.method public setSeqId(I)V
    .locals 0
    .param p1, "seqId"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->seqId:I

    .line 55
    return-void
.end method

.method public setUniqueKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "uniqueKey"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->uniqueKey:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .prologue
    .line 129
    iput p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->userId:I

    .line 130
    return-void
.end method

.method public setWidgetInfoType(Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;)V
    .locals 0
    .param p1, "widgetInfoType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->widgetInfoType:Lcom/cigna/coach/interfaces/IWidget$WidgetInfoType;

    .line 85
    return-void
.end method

.method public setWidgetType(Lcom/cigna/coach/interfaces/IWidget$WidgetType;)V
    .locals 0
    .param p1, "widgetType"    # Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/WidgetNotificationData;->widgetType:Lcom/cigna/coach/interfaces/IWidget$WidgetType;

    .line 159
    return-void
.end method
