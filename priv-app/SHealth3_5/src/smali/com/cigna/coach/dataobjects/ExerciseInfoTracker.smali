.class public Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;
.super Ljava/lang/Object;
.source "ExerciseInfoTracker.java"


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field private exerciseInfoId:I

.field private met:F

.field private metName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method


# virtual methods
.method public getMet()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->met:F

    return v0
.end method

.method public getMetName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->metName:Ljava/lang/String;

    return-object v0
.end method

.method public getexerciseInfoId()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->exerciseInfoId:I

    return v0
.end method

.method public setExerciseInfoId(I)V
    .locals 0
    .param p1, "exerciseInfoId"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->exerciseInfoId:I

    .line 69
    return-void
.end method

.method public setMet(F)V
    .locals 0
    .param p1, "met"    # F

    .prologue
    .line 87
    iput p1, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->met:F

    .line 88
    return-void
.end method

.method public setMetName(Ljava/lang/String;)V
    .locals 0
    .param p1, "metName"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->metName:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ExerciseInfo = { "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->exerciseInfoId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->metName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/cigna/coach/dataobjects/ExerciseInfoTracker;->met:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
