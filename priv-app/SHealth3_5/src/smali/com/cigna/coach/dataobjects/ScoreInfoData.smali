.class public Lcom/cigna/coach/dataobjects/ScoreInfoData;
.super Lcom/cigna/coach/apiobjects/ScoreInfo;
.source "ScoreInfoData.java"


# instance fields
.field private currentScoreMessage:Ljava/lang/String;

.field private rawLssList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation
.end field

.field private varianceText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/ScoreInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public getCurrentScore()I
    .locals 2
    .annotation runtime Lcom/cigna/coach/utils/CoachContentTagValue;
        tagName = "score"
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getCurrentScoreInfo()Lcom/cigna/coach/apiobjects/LifeStyleScores;

    move-result-object v0

    .line 109
    .local v0, "currentScoreInfo":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    .line 110
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScore()I

    move-result v1

    goto :goto_0
.end method

.method public getCurrentScoreContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setCurrentScoreMessage"
    .end annotation

    .prologue
    .line 98
    const/16 v0, 0xf75

    return v0
.end method

.method public getCurrentScoreInfo()Lcom/cigna/coach/apiobjects/LifeStyleScores;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-object v0

    .line 146
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    goto :goto_0
.end method

.method public getCurrentScoreMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ScoreInfoData;->currentScoreMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getLastAssessmentDate()Ljava/util/Calendar;
    .locals 2

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getLastAssessmentScoreInfo()Lcom/cigna/coach/apiobjects/LifeStyleScores;

    move-result-object v0

    .line 82
    .local v0, "assessmentScoreInfo":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 83
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreAssignmentDate()Ljava/util/Calendar;

    move-result-object v1

    goto :goto_0
.end method

.method public getLastAssessmentScoreInfo()Lcom/cigna/coach/apiobjects/LifeStyleScores;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 133
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v1, v2

    .line 141
    :cond_0
    :goto_0
    return-object v1

    .line 135
    :cond_1
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 136
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cigna/coach/apiobjects/LifeStyleScores;

    .line 137
    .local v1, "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    invoke-virtual {v1}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getScoreType()Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    move-result-object v3

    sget-object v4, Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;->LIFESTYLE_OVERALL:Lcom/cigna/coach/apiobjects/LifeStyleScores$ScoreType;

    if-eq v3, v4, :cond_0

    .line 135
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .end local v1    # "score":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    :cond_2
    move-object v1, v2

    .line 141
    goto :goto_0
.end method

.method public getLastUpdateContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setLastUpdateMessage"
    .end annotation

    .prologue
    .line 103
    const/16 v0, 0x1016

    return v0
.end method

.method public getOutOfContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setOutOfMessage"
    .end annotation

    .prologue
    .line 93
    const/16 v0, 0xf6f

    return v0
.end method

.method public getRawLssList()Ljava/util/List;
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentObject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ScoreInfoData;->rawLssList:Ljava/util/List;

    return-object v0
.end method

.method public getVarianceContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setVarianceText"
    .end annotation

    .prologue
    .line 88
    const/16 v0, 0xf72

    return v0
.end method

.method public getVarianceText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/ScoreInfoData;->varianceText:Ljava/lang/String;

    return-object v0
.end method

.method public getVarianceValue()I
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getCurrentScoreInfo()Lcom/cigna/coach/apiobjects/LifeStyleScores;

    move-result-object v0

    .line 152
    .local v0, "currentScore":Lcom/cigna/coach/apiobjects/LifeStyleScores;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 153
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Lcom/cigna/coach/apiobjects/LifeStyleScores;->getVarianceValue()I

    move-result v1

    goto :goto_0
.end method

.method public setCurrentScoreMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "currentScoreMessage"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ScoreInfoData;->currentScoreMessage:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setRawLssList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cigna/coach/apiobjects/LifeStyleScores;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "lssList":Ljava/util/List;, "Ljava/util/List<Lcom/cigna/coach/apiobjects/LifeStyleScores;>;"
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ScoreInfoData;->rawLssList:Ljava/util/List;

    .line 130
    return-void
.end method

.method public setVarianceText(Ljava/lang/String;)V
    .locals 0
    .param p1, "varianceText"    # Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/cigna/coach/dataobjects/ScoreInfoData;->varianceText:Ljava/lang/String;

    .line 180
    return-void
.end method

.method public summarize(Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)V
    .locals 1
    .param p1, "scoreGroupingType"    # Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->getRawLssList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/cigna/coach/utils/ScoringHelper;->summarizeScoresHistoryList(Ljava/util/List;Lcom/cigna/coach/interfaces/ILifeStyle$ScoreGroupingType;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cigna/coach/dataobjects/ScoreInfoData;->setLssList(Ljava/util/List;)V

    .line 162
    return-void
.end method
