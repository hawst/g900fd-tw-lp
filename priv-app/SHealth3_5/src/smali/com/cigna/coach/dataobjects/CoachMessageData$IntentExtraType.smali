.class public final enum Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
.super Ljava/lang/Enum;
.source "CoachMessageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/dataobjects/CoachMessageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "IntentExtraType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

.field public static final enum GOAL_MISSION_ID_SEQ_ID:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

.field public static final enum MISSION_ID_SEQ_ID:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 221
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    const-string v5, "MISSION_ID_SEQ_ID"

    invoke-direct {v4, v5, v7, v6}, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->MISSION_ID_SEQ_ID:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    .line 222
    new-instance v4, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    const-string v5, "GOAL_MISSION_ID_SEQ_ID"

    invoke-direct {v4, v5, v6, v8}, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->GOAL_MISSION_ID_SEQ_ID:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    .line 220
    new-array v4, v8, [Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->MISSION_ID_SEQ_ID:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->GOAL_MISSION_ID_SEQ_ID:Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    aput-object v5, v4, v6

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    .line 236
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->codeValueMap:Ljava/util/HashMap;

    .line 238
    invoke-static {}, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->values()[Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 239
    .local v3, "type":Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
    sget-object v4, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->getIntentExtraType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 241
    .end local v3    # "type":Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 227
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 228
    iput p3, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->value:I

    .line 229
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
    .locals 2
    .param p0, "statusKey"    # I

    .prologue
    .line 245
    sget-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 220
    const-class v0, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->$VALUES:[Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    invoke-virtual {v0}, [Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;

    return-object v0
.end method


# virtual methods
.method public getIntentExtraType()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/cigna/coach/dataobjects/CoachMessageData$IntentExtraType;->value:I

    return v0
.end method
