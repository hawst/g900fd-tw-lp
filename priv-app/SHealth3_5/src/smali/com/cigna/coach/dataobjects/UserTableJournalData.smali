.class public Lcom/cigna/coach/dataobjects/UserTableJournalData;
.super Lorg/json/JSONObject;
.source "UserTableJournalData.java"


# instance fields
.field private actionTypeCode:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

.field private codeBaseId:I

.field private dbModelVersion:I

.field private key:Ljava/lang/String;

.field private seqId:J

.field private tableName:Ljava/lang/String;

.field private transactionTimeStamp:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(JIILjava/lang/String;Ljava/lang/String;Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;Ljava/util/Calendar;)V
    .locals 4
    .param p1, "seqId"    # J
    .param p3, "codeBaseId"    # I
    .param p4, "dbModelVersion"    # I
    .param p5, "tableName"    # Ljava/lang/String;
    .param p6, "key"    # Ljava/lang/String;
    .param p7, "action"    # Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    .param p8, "transactionTimeStamp"    # Ljava/util/Calendar;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 39
    invoke-direct {p0}, Lorg/json/JSONObject;-><init>()V

    .line 30
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->seqId:J

    .line 31
    iput v3, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->codeBaseId:I

    .line 32
    iput v3, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->dbModelVersion:I

    .line 33
    iput-object v2, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->tableName:Ljava/lang/String;

    .line 34
    iput-object v2, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->key:Ljava/lang/String;

    .line 35
    iput-object v2, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->actionTypeCode:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    .line 36
    iput-object v2, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->transactionTimeStamp:Ljava/util/Calendar;

    .line 40
    iput-wide p1, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->seqId:J

    .line 41
    iput p3, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->codeBaseId:I

    .line 42
    iput p4, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->dbModelVersion:I

    .line 43
    iput-object p5, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->tableName:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->key:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->actionTypeCode:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    .line 46
    iput-object p8, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->transactionTimeStamp:Ljava/util/Calendar;

    .line 48
    return-void
.end method


# virtual methods
.method public getActionTypeCode()Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->actionTypeCode:Lcom/cigna/coach/dataobjects/JournalEntry$JournalActionType;

    return-object v0
.end method

.method public getCodeBaseId()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->codeBaseId:I

    return v0
.end method

.method public getDbModelVersion()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->dbModelVersion:I

    return v0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->key:Ljava/lang/String;

    return-object v0
.end method

.method public getSeqId()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->seqId:J

    return-wide v0
.end method

.method public getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->tableName:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionTimeStamp()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/cigna/coach/dataobjects/UserTableJournalData;->transactionTimeStamp:Ljava/util/Calendar;

    return-object v0
.end method
