.class public Lcom/cigna/coach/dataobjects/MissionTipData;
.super Lcom/cigna/coach/apiobjects/MissionTip;
.source "MissionTipData.java"


# instance fields
.field private contentId:I

.field private titleContentId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/cigna/coach/apiobjects/MissionTip;-><init>()V

    return-void
.end method


# virtual methods
.method public getContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTip"
    .end annotation

    .prologue
    .line 28
    iget v0, p0, Lcom/cigna/coach/dataobjects/MissionTipData;->contentId:I

    return v0
.end method

.method public getTitleContentId()I
    .locals 1
    .annotation runtime Lcom/cigna/coach/utils/CoachContentId;
        setTextMethod = "setTitle"
    .end annotation

    .prologue
    .line 43
    iget v0, p0, Lcom/cigna/coach/dataobjects/MissionTipData;->titleContentId:I

    return v0
.end method

.method public setContentId(I)V
    .locals 0
    .param p1, "contentId"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/cigna/coach/dataobjects/MissionTipData;->contentId:I

    .line 36
    return-void
.end method

.method public setTitleContentId(I)V
    .locals 0
    .param p1, "titleContentId"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/cigna/coach/dataobjects/MissionTipData;->titleContentId:I

    .line 51
    return-void
.end method
