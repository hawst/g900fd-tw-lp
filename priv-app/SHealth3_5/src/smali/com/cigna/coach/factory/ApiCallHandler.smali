.class public Lcom/cigna/coach/factory/ApiCallHandler;
.super Ljava/lang/Object;
.source "ApiCallHandler.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;,
        Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    }
.end annotation


# static fields
.field private static final CLASS_NAME:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

.field private target:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/cigna/coach/factory/ApiCallHandler;

    invoke-static {v0}, Lcom/cigna/coach/utils/Log;->getTag(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;Landroid/content/Context;)V
    .locals 0
    .param p1, "target"    # Ljava/lang/Object;
    .param p2, "source"    # Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/cigna/coach/factory/ApiCallHandler;->target:Ljava/lang/Object;

    .line 185
    iput-object p2, p0, Lcom/cigna/coach/factory/ApiCallHandler;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .line 186
    iput-object p3, p0, Lcom/cigna/coach/factory/ApiCallHandler;->context:Landroid/content/Context;

    .line 187
    return-void
.end method


# virtual methods
.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 24
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 191
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 192
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v20, "*******************************************************************"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Invocation on:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->target:Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",Method:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p2 .. p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",Source:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",ThreadId:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Thread;->getId()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",Thread:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    const/16 v16, 0x0

    .line 197
    .local v16, "result":Ljava/lang/Object;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    .line 199
    .local v17, "startTime":J
    sget-object v11, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->NO_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 200
    .local v11, "exceptionType":Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->target:Ljava/lang/Object;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "."

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {p2 .. p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->getInstance(Ljava/lang/String;)Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    move-result-object v3

    .line 204
    .local v3, "apiMethod":Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->target:Ljava/lang/Object;

    move-object/from16 v19, v0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v16

    .line 221
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 222
    .local v8, "endTime":J
    sub-long v14, v8, v17

    .line 223
    .local v14, "respTime":J
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 224
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Invocation took "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " ms"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->getConnectionManager(Landroid/content/Context;)Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    move-result-object v6

    .line 229
    .local v6, "dbManager":Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    invoke-virtual {v6}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->isDatabaseCreated()Z

    move-result v19

    if-eqz v19, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->isAuditEnabled()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 230
    new-instance v10, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;

    invoke-direct {v10}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;-><init>()V

    .line 231
    .local v10, "entry":Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
    invoke-virtual {v10, v3}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setMethod(Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setSource(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)V

    .line 233
    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setInvocationTime(Ljava/util/Calendar;)V

    .line 234
    invoke-virtual {v10, v14, v15}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setInvocationRespTimeInMillis(J)V

    .line 235
    invoke-virtual {v10, v11}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setExceptionType(Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;)V

    .line 237
    const/4 v4, 0x0

    .line 239
    .local v4, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_1
    new-instance v5, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->context:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 240
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .local v5, "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_2
    new-instance v12, Lcom/cigna/coach/utils/ApiCallLogHelper;

    invoke-direct {v12, v5}, Lcom/cigna/coach/utils/ApiCallLogHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 241
    .local v12, "helper":Lcom/cigna/coach/utils/ApiCallLogHelper;
    invoke-virtual {v12, v10}, Lcom/cigna/coach/utils/ApiCallLogHelper;->insertApiCallLogEntry(Lcom/cigna/coach/dataobjects/ApiCallLogEntry;)V

    .line 242
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 248
    if-eqz v5, :cond_2

    .line 249
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 254
    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v10    # "entry":Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
    .end local v12    # "helper":Lcom/cigna/coach/utils/ApiCallLogHelper;
    :cond_2
    :goto_0
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v19 .. v19}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 255
    invoke-virtual {v6}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->isDatabaseCreated()Z

    move-result v19

    if-nez v19, :cond_3

    .line 256
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v20, "Database not created; not logging api call"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_3
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Invocation completed:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->target:Ljava/lang/Object;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",Method:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {p2 .. p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",Source:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",ThreadId:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Thread;->getId()J

    move-result-wide v21

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ",Thread:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v20, "*******************************************************************"

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_4
    return-object v16

    .line 243
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v10    # "entry":Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
    :catch_0
    move-exception v7

    .line 245
    .local v7, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception while logging api call:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 248
    if-eqz v4, :cond_2

    .line 249
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto/16 :goto_0

    .line 248
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v19

    :goto_2
    if-eqz v4, :cond_5

    .line 249
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_5
    throw v19

    .line 206
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v6    # "dbManager":Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    .end local v8    # "endTime":J
    .end local v10    # "entry":Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
    .end local v14    # "respTime":J
    :catch_1
    move-exception v7

    .line 207
    .restart local v7    # "e":Ljava/lang/Exception;
    :try_start_4
    sget-object v19, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Exception in invocation:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    instance-of v0, v7, Ljava/lang/reflect/InvocationTargetException;

    move/from16 v19, v0

    if-eqz v19, :cond_b

    .line 209
    check-cast v7, Ljava/lang/reflect/InvocationTargetException;

    .end local v7    # "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v13

    .line 210
    .local v13, "ite":Ljava/lang/Throwable;
    instance-of v0, v13, Lcom/cigna/coach/exceptions/CoachException;

    move/from16 v19, v0

    if-eqz v19, :cond_a

    .line 211
    move-object v0, v13

    check-cast v0, Lcom/cigna/coach/exceptions/CoachException;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/cigna/coach/exceptions/CoachException;->getExceptionType()Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    move-result-object v11

    .line 215
    :goto_3
    throw v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 221
    .end local v13    # "ite":Ljava/lang/Throwable;
    :catchall_1
    move-exception v19

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 222
    .restart local v8    # "endTime":J
    sub-long v14, v8, v17

    .line 223
    .restart local v14    # "respTime":J
    sget-object v20, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 224
    sget-object v20, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Invocation took "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " ms"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->context:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->getConnectionManager(Landroid/content/Context;)Lcom/cigna/coach/db/CoachSqliteConnectionManager;

    move-result-object v6

    .line 229
    .restart local v6    # "dbManager":Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    invoke-virtual {v6}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->isDatabaseCreated()Z

    move-result v20

    if-eqz v20, :cond_7

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->isAuditEnabled()Z

    move-result v20

    if-eqz v20, :cond_7

    .line 230
    new-instance v10, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;

    invoke-direct {v10}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;-><init>()V

    .line 231
    .restart local v10    # "entry":Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
    invoke-virtual {v10, v3}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setMethod(Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setSource(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)V

    .line 233
    invoke-static/range {v17 .. v18}, Lcom/cigna/coach/utils/CalendarUtil;->fromMilliSecToCalendar(J)Ljava/util/Calendar;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setInvocationTime(Ljava/util/Calendar;)V

    .line 234
    invoke-virtual {v10, v14, v15}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setInvocationRespTimeInMillis(J)V

    .line 235
    invoke-virtual {v10, v11}, Lcom/cigna/coach/dataobjects/ApiCallLogEntry;->setExceptionType(Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;)V

    .line 237
    const/4 v4, 0x0

    .line 239
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_5
    new-instance v5, Lcom/cigna/coach/db/CoachSQLiteHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->context:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Lcom/cigna/coach/db/CoachSQLiteHelper;-><init>(Landroid/content/Context;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 240
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :try_start_6
    new-instance v12, Lcom/cigna/coach/utils/ApiCallLogHelper;

    invoke-direct {v12, v5}, Lcom/cigna/coach/utils/ApiCallLogHelper;-><init>(Lcom/cigna/coach/db/CoachSQLiteHelper;)V

    .line 241
    .restart local v12    # "helper":Lcom/cigna/coach/utils/ApiCallLogHelper;
    invoke-virtual {v12, v10}, Lcom/cigna/coach/utils/ApiCallLogHelper;->insertApiCallLogEntry(Lcom/cigna/coach/dataobjects/ApiCallLogEntry;)V

    .line 242
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->commit()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 248
    if-eqz v5, :cond_7

    .line 249
    invoke-virtual {v5}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    .line 254
    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .end local v10    # "entry":Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
    .end local v12    # "helper":Lcom/cigna/coach/utils/ApiCallLogHelper;
    :cond_7
    :goto_4
    sget-object v20, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    invoke-static/range {v20 .. v20}, Lcom/cigna/coach/utils/Log;->isD(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 255
    invoke-virtual {v6}, Lcom/cigna/coach/db/CoachSqliteConnectionManager;->isDatabaseCreated()Z

    move-result v20

    if-nez v20, :cond_8

    .line 256
    sget-object v20, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "Database not created; not logging api call"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_8
    sget-object v20, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Invocation completed:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->target:Ljava/lang/Object;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ",Method:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ",Source:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cigna/coach/factory/ApiCallHandler;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ",ThreadId:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Thread;->getId()J

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ",Thread:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    sget-object v20, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    const-string v21, "*******************************************************************"

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_9
    throw v19

    .line 213
    .end local v6    # "dbManager":Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    .end local v8    # "endTime":J
    .end local v14    # "respTime":J
    .restart local v13    # "ite":Ljava/lang/Throwable;
    :cond_a
    :try_start_7
    sget-object v11, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    goto/16 :goto_3

    .line 217
    .end local v13    # "ite":Ljava/lang/Throwable;
    .restart local v7    # "e":Ljava/lang/Exception;
    :cond_b
    sget-object v11, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 218
    new-instance v19, Lcom/cigna/coach/exceptions/CoachException;

    move-object/from16 v0, v19

    invoke-direct {v0, v7}, Lcom/cigna/coach/exceptions/CoachException;-><init>(Ljava/lang/Throwable;)V

    throw v19
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 243
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v6    # "dbManager":Lcom/cigna/coach/db/CoachSqliteConnectionManager;
    .restart local v8    # "endTime":J
    .restart local v10    # "entry":Lcom/cigna/coach/dataobjects/ApiCallLogEntry;
    .restart local v14    # "respTime":J
    :catch_2
    move-exception v7

    .line 245
    .restart local v7    # "e":Ljava/lang/Exception;
    :goto_5
    :try_start_8
    sget-object v20, Lcom/cigna/coach/factory/ApiCallHandler;->CLASS_NAME:Ljava/lang/String;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Exception while logging api call:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/cigna/coach/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 248
    if-eqz v4, :cond_7

    .line 249
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    goto/16 :goto_4

    .line 248
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_2
    move-exception v19

    :goto_6
    if-eqz v4, :cond_c

    .line 249
    invoke-virtual {v4}, Lcom/cigna/coach/db/CoachSQLiteHelper;->close()V

    :cond_c
    throw v19

    .line 248
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_3
    move-exception v19

    move-object v4, v5

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_6

    .line 243
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_3
    move-exception v7

    move-object v4, v5

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto :goto_5

    .line 248
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catchall_4
    move-exception v19

    move-object v4, v5

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto/16 :goto_2

    .line 243
    .end local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    :catch_4
    move-exception v7

    move-object v4, v5

    .end local v5    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    .restart local v4    # "dbHelper":Lcom/cigna/coach/db/CoachSQLiteHelper;
    goto/16 :goto_1
.end method
