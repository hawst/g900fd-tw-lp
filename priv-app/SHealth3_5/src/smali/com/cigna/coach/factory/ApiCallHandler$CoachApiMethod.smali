.class public final enum Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
.super Ljava/lang/Enum;
.source "ApiCallHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/factory/ApiCallHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoachApiMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum BACKUPANDRESTORE_STARTOPERATION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum CMSMANAGER_DOWNLOADCONTENTFILE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum CMSMANAGER_ISCOACHSUPPORTEDLANGUAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_GETMISSIONNOTIFICATIONFAILUREMESSAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_GETUSERAVAILABLEGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_GETUSERAVAILABLEGOALS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_GETUSERCOMPLETEDGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_GETUSERMISSIONDETAILS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_GETUSERMISSIONHISTORYDETAILS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_GETUSERSELECTEDGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_SETMISSIONFAILUREREASONCODE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_SETUSERGOALSTATUS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_SETUSERMISSION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum GOALSMISSIONS_SETUSERMISSIONDETAIL:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_ADDFAVORITE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_GETARTICLE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_GETARTICLESFORSUBCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_GETCATEGORIES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_GETFAVORITESBYCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_GETFAVORITESBYTITLE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_GETFEATUREDARTICLES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_GETSUBCATEGORIES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_REMOVEFAVORITE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum HEALTHLIBRARY_SEARCHARTICLES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_CLEARALLUSERINFORMATION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_CLEARUSERSELECTIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETBADGECOUNT:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETCATEGORYCOACHMESSAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETCATEGORYIDMSG:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETCOACHMSG:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETCOACHMSGS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETQUESTIONANSWERSBYCAT:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETSCOREBACKGROUNDIMAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETSCORES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_GETSCORESHISTORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_HANDLEUSERACCOUNTCHANGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_INITIALIZEDATABASE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_SETANSWERSBYCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_SETUSERMETRICS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum LIFESTYLE_UPDATEUSERID:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum TRACKER_CHECKFORINACTIVITY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum TRACKER_GETNOTIFICATIONSTARTTIME:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum TRACKER_GETQUEUEDNOTIFICATIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum TRACKER_PROCESSTRACKERCHANGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field public static final enum WIDGET_GETWIDGETINFO:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

.field private static codeMethodNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;",
            ">;"
        }
    .end annotation
.end field

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field auditEnabled:Z

.field key:I

.field methodName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x5

    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 85
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_CLEARALLUSERINFORMATION"

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-class v8, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".clearAllUserInformation"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v6, v10, v7}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_CLEARALLUSERINFORMATION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 86
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_CLEARUSERSELECTIONS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-class v7, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".clearUserSelections"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v10, v11, v6}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_CLEARUSERSELECTIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 87
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETBADGECOUNT"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-class v7, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".getBadgeCount"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v11, v12, v6}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETBADGECOUNT:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 88
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETCATEGORYCOACHMESSAGE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-class v7, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".getCategoryCoachMessage"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v12, v13, v6}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCATEGORYCOACHMESSAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 89
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETCATEGORYIDMSG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-class v7, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".getCategoryIdMsg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v13, v14, v6}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCATEGORYIDMSG:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 90
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETCOACHMSG"

    const/4 v6, 0x6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-class v8, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v8}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".getCoachMsg"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v14, v6, v7}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCOACHMSG:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 91
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETCOACHMSGS"

    const/4 v6, 0x6

    const/4 v7, 0x7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getCoachMsgs"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCOACHMSGS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 92
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETQUESTIONANSWERSBYCAT"

    const/4 v6, 0x7

    const/16 v7, 0x8

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getQuestionAnswersByCat"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETQUESTIONANSWERSBYCAT:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 93
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETSCOREBACKGROUNDIMAGE"

    const/16 v6, 0x8

    const/16 v7, 0x9

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getScoreBackgroundImage"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETSCOREBACKGROUNDIMAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 94
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETSCORES"

    const/16 v6, 0x9

    const/16 v7, 0xa

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getScores"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETSCORES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 95
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_GETSCORESHISTORY"

    const/16 v6, 0xa

    const/16 v7, 0xb

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getScoresHistory"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETSCORESHISTORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 96
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_INITIALIZEDATABASE"

    const/16 v6, 0xb

    const/16 v7, 0xc

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".initializeDatabase"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_INITIALIZEDATABASE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 97
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_SETANSWERSBYCATEGORY"

    const/16 v6, 0xc

    const/16 v7, 0xd

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".setAnswersByCategory"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_SETANSWERSBYCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 98
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_SETUSERMETRICS"

    const/16 v6, 0xd

    const/16 v7, 0xe

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".setUserMetrics"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_SETUSERMETRICS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 99
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_UPDATEUSERID"

    const/16 v6, 0xe

    const/16 v7, 0xf

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".updateUserId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_UPDATEUSERID:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 100
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "LIFESTYLE_HANDLEUSERACCOUNTCHANGE"

    const/16 v6, 0xf

    const/16 v7, 0x10

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/LifeStyle;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".handleUserAccountChange"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_HANDLEUSERACCOUNTCHANGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 102
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_GETMISSIONNOTIFICATIONFAILUREMESSAGE"

    const/16 v6, 0x10

    const/16 v7, 0x1f

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "getMissionNotificationFailureMessage"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETMISSIONNOTIFICATIONFAILUREMESSAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 103
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_GETUSERAVAILABLEGOALMISSIONS"

    const/16 v6, 0x11

    const/16 v7, 0x20

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getUserAvailableGoalMissions"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERAVAILABLEGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 104
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_GETUSERAVAILABLEGOALS"

    const/16 v6, 0x12

    const/16 v7, 0x21

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getUserAvailableGoals"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERAVAILABLEGOALS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 105
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_GETUSERCOMPLETEDGOALMISSIONS"

    const/16 v6, 0x13

    const/16 v7, 0x22

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getUserCompletedGoalMissions"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERCOMPLETEDGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 106
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_GETUSERMISSIONDETAILS"

    const/16 v6, 0x14

    const/16 v7, 0x23

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getUserMissionDetails"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERMISSIONDETAILS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 107
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_GETUSERMISSIONHISTORYDETAILS"

    const/16 v6, 0x15

    const/16 v7, 0x24

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getUserMissionHistoryDetails"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERMISSIONHISTORYDETAILS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 108
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_GETUSERSELECTEDGOALMISSIONS"

    const/16 v6, 0x16

    const/16 v7, 0x25

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getUserSelectedGoalMissions"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERSELECTEDGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 109
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_SETMISSIONFAILUREREASONCODE"

    const/16 v6, 0x17

    const/16 v7, 0x26

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".setMissionFailureReasonCode"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETMISSIONFAILUREREASONCODE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 110
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_SETUSERGOALSTATUS"

    const/16 v6, 0x18

    const/16 v7, 0x27

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".setUserGoalStatus"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETUSERGOALSTATUS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 111
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_SETUSERMISSION"

    const/16 v6, 0x19

    const/16 v7, 0x28

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".setUserMission"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETUSERMISSION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 112
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "GOALSMISSIONS_SETUSERMISSIONDETAIL"

    const/16 v6, 0x1a

    const/16 v7, 0x29

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/GoalsMissions;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".setUserMissionDetail"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETUSERMISSIONDETAIL:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 114
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_ADDFAVORITE"

    const/16 v6, 0x1b

    const/16 v7, 0x3d

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".addFavorite"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_ADDFAVORITE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 115
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_GETARTICLE"

    const/16 v6, 0x1c

    const/16 v7, 0x3e

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getArticle"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETARTICLE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 116
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_GETARTICLESFORSUBCATEGORY"

    const/16 v6, 0x1d

    const/16 v7, 0x3f

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getArticlesForSubCategory"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETARTICLESFORSUBCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 117
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_GETCATEGORIES"

    const/16 v6, 0x1e

    const/16 v7, 0x40

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getCategories"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETCATEGORIES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 118
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_GETFAVORITESBYCATEGORY"

    const/16 v6, 0x1f

    const/16 v7, 0x41

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getFavoritesByCategory"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETFAVORITESBYCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 119
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_GETFAVORITESBYTITLE"

    const/16 v6, 0x20

    const/16 v7, 0x42

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getFavoritesByTitle"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETFAVORITESBYTITLE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 120
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_GETFEATUREDARTICLES"

    const/16 v6, 0x21

    const/16 v7, 0x43

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getFeaturedArticles"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETFEATUREDARTICLES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 121
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_GETSUBCATEGORIES"

    const/16 v6, 0x22

    const/16 v7, 0x44

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getSubCategories"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETSUBCATEGORIES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 122
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_REMOVEFAVORITE"

    const/16 v6, 0x23

    const/16 v7, 0x45

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".removeFavorite"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_REMOVEFAVORITE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 123
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "HEALTHLIBRARY_SEARCHARTICLES"

    const/16 v6, 0x24

    const/16 v7, 0x46

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/HealthLibrary;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".searchArticles"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_SEARCHARTICLES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 125
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "WIDGET_GETWIDGETINFO"

    const/16 v6, 0x25

    const/16 v7, 0x5b

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/Widget;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getWidgetInfo"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->WIDGET_GETWIDGETINFO:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 127
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "TRACKER_CHECKFORINACTIVITY"

    const/16 v6, 0x26

    const/16 v7, 0x79

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/Tracker;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".checkForInactivity"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_CHECKFORINACTIVITY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 128
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "TRACKER_GETQUEUEDNOTIFICATIONS"

    const/16 v6, 0x27

    const/16 v7, 0x7a

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/Tracker;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getQueuedNotifications"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_GETQUEUEDNOTIFICATIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 129
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "TRACKER_GETNOTIFICATIONSTARTTIME"

    const/16 v6, 0x28

    const/16 v7, 0x7b

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/Tracker;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".getNotificationStartTime"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_GETNOTIFICATIONSTARTTIME:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 130
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "TRACKER_PROCESSTRACKERCHANGE"

    const/16 v6, 0x29

    const/16 v7, 0x7c

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/Tracker;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".processTrackerChange"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_PROCESSTRACKERCHANGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 132
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "BACKUPANDRESTORE_STARTOPERATION"

    const/16 v6, 0x2a

    const/16 v7, 0x97

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/BackupAndRestore;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".startOperation"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->BACKUPANDRESTORE_STARTOPERATION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 134
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "CMSMANAGER_DOWNLOADCONTENTFILE"

    const/16 v6, 0x2b

    const/16 v7, 0xb5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/CMSManager;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".downloadContentFile"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->CMSMANAGER_DOWNLOADCONTENTFILE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 135
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const-string v5, "CMSMANAGER_ISCOACHSUPPORTEDLANGUAGE"

    const/16 v6, 0x2c

    const/16 v7, 0xb6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-class v9, Lcom/cigna/coach/CMSManager;

    invoke-virtual {v9}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".isCoachSupportedLanguage"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->CMSMANAGER_ISCOACHSUPPORTEDLANGUAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 84
    const/16 v4, 0x2d

    new-array v4, v4, [Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    const/4 v5, 0x0

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_CLEARALLUSERINFORMATION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_CLEARUSERSELECTIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v5, v4, v10

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETBADGECOUNT:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v5, v4, v11

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCATEGORYCOACHMESSAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v5, v4, v12

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCATEGORYIDMSG:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v5, v4, v13

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCOACHMSG:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v5, v4, v14

    const/4 v5, 0x6

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETCOACHMSGS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/4 v5, 0x7

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETQUESTIONANSWERSBYCAT:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x8

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETSCOREBACKGROUNDIMAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x9

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETSCORES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0xa

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_GETSCORESHISTORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0xb

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_INITIALIZEDATABASE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0xc

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_SETANSWERSBYCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0xd

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_SETUSERMETRICS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0xe

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_UPDATEUSERID:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0xf

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->LIFESTYLE_HANDLEUSERACCOUNTCHANGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x10

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETMISSIONNOTIFICATIONFAILUREMESSAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x11

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERAVAILABLEGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x12

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERAVAILABLEGOALS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x13

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERCOMPLETEDGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x14

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERMISSIONDETAILS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x15

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERMISSIONHISTORYDETAILS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x16

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_GETUSERSELECTEDGOALMISSIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x17

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETMISSIONFAILUREREASONCODE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x18

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETUSERGOALSTATUS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x19

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETUSERMISSION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x1a

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->GOALSMISSIONS_SETUSERMISSIONDETAIL:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x1b

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_ADDFAVORITE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x1c

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETARTICLE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x1d

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETARTICLESFORSUBCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x1e

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETCATEGORIES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x1f

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETFAVORITESBYCATEGORY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x20

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETFAVORITESBYTITLE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x21

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETFEATUREDARTICLES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x22

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_GETSUBCATEGORIES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x23

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_REMOVEFAVORITE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x24

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->HEALTHLIBRARY_SEARCHARTICLES:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x25

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->WIDGET_GETWIDGETINFO:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x26

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_CHECKFORINACTIVITY:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x27

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_GETQUEUEDNOTIFICATIONS:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x28

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_GETNOTIFICATIONSTARTTIME:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x29

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->TRACKER_PROCESSTRACKERCHANGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x2a

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->BACKUPANDRESTORE_STARTOPERATION:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x2b

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->CMSMANAGER_DOWNLOADCONTENTFILE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    const/16 v5, 0x2c

    sget-object v6, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->CMSMANAGER_ISCOACHSUPPORTEDLANGUAGE:Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    aput-object v6, v4, v5

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->$VALUES:[Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    .line 140
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->codeValueMap:Ljava/util/HashMap;

    .line 141
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->codeMethodNameMap:Ljava/util/HashMap;

    .line 175
    invoke-static {}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->values()[Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 176
    .local v3, "type":Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    sget-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->getKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    sget-object v4, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->codeMethodNameMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    .end local v3    # "type":Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 6
    .param p3, "key"    # I
    .param p4, "methodName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 144
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;-><init>(Ljava/lang/String;IILjava/lang/String;Z)V

    .line 145
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Z)V
    .locals 1
    .param p3, "key"    # I
    .param p4, "methodName"    # Ljava/lang/String;
    .param p5, "auditEnabled"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->auditEnabled:Z

    .line 148
    iput p3, p0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->key:I

    .line 149
    iput-object p4, p0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->methodName:Ljava/lang/String;

    .line 150
    iput-boolean p5, p0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->auditEnabled:Z

    .line 151
    return-void
.end method

.method public static getInstance(I)Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    .locals 2
    .param p0, "key"    # I

    .prologue
    .line 167
    sget-object v0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    .locals 1
    .param p0, "methodName"    # Ljava/lang/String;

    .prologue
    .line 171
    sget-object v0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->codeMethodNameMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 84
    const-class v0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->$VALUES:[Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    invoke-virtual {v0}, [Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;

    return-object v0
.end method


# virtual methods
.method public getKey()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->key:I

    return v0
.end method

.method public getMethodName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->methodName:Ljava/lang/String;

    return-object v0
.end method

.method public isAuditEnabled()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/cigna/coach/factory/ApiCallHandler$CoachApiMethod;->auditEnabled:Z

    return v0
.end method
