.class public Lcom/cigna/coach/factory/InternalCoachFactory;
.super Lcom/cigna/coach/factory/CoachFactory;
.source "InternalCoachFactory.java"


# static fields
.field private static instanceMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;",
            "Lcom/cigna/coach/factory/InternalCoachFactory;",
            ">;"
        }
    .end annotation
.end field

.field private static trackerInstance:Lcom/cigna/coach/interfaces/ITracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/cigna/coach/factory/InternalCoachFactory;->instanceMap:Ljava/util/Map;

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/cigna/coach/factory/InternalCoachFactory;->trackerInstance:Lcom/cigna/coach/interfaces/ITracker;

    return-void
.end method

.method protected constructor <init>(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)V
    .locals 0
    .param p1, "source"    # Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/cigna/coach/factory/CoachFactory;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/cigna/coach/factory/InternalCoachFactory;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .line 42
    return-void
.end method

.method public static getInstance(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)Lcom/cigna/coach/factory/InternalCoachFactory;
    .locals 2
    .param p0, "source"    # Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .prologue
    .line 52
    sget-object v1, Lcom/cigna/coach/factory/InternalCoachFactory;->instanceMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/factory/InternalCoachFactory;

    .line 53
    .local v0, "instance":Lcom/cigna/coach/factory/InternalCoachFactory;
    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/cigna/coach/factory/InternalCoachFactory;

    .end local v0    # "instance":Lcom/cigna/coach/factory/InternalCoachFactory;
    invoke-direct {v0, p0}, Lcom/cigna/coach/factory/InternalCoachFactory;-><init>(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)V

    .line 55
    .restart local v0    # "instance":Lcom/cigna/coach/factory/InternalCoachFactory;
    sget-object v1, Lcom/cigna/coach/factory/InternalCoachFactory;->instanceMap:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getTracker(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ITracker;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    sget-object v0, Lcom/cigna/coach/factory/InternalCoachFactory;->trackerInstance:Lcom/cigna/coach/interfaces/ITracker;

    if-nez v0, :cond_0

    .line 63
    iput-object p1, p0, Lcom/cigna/coach/factory/InternalCoachFactory;->context:Landroid/content/Context;

    .line 64
    const-class v0, Lcom/cigna/coach/interfaces/ITracker;

    new-instance v1, Lcom/cigna/coach/Tracker;

    invoke-direct {v1, p1}, Lcom/cigna/coach/Tracker;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/factory/InternalCoachFactory;->getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ITracker;

    sput-object v0, Lcom/cigna/coach/factory/InternalCoachFactory;->trackerInstance:Lcom/cigna/coach/interfaces/ITracker;

    .line 66
    :cond_0
    sget-object v0, Lcom/cigna/coach/factory/InternalCoachFactory;->trackerInstance:Lcom/cigna/coach/interfaces/ITracker;

    return-object v0
.end method
