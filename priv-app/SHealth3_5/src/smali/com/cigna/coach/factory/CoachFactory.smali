.class public Lcom/cigna/coach/factory/CoachFactory;
.super Ljava/lang/Object;
.source "CoachFactory.java"


# static fields
.field private static backupAndRestoreInstance:Lcom/cigna/coach/interfaces/IBackupAndRestore;

.field private static cmsManagerInstance:Lcom/cigna/coach/interfaces/ICMSManager;

.field private static goalsMissionsInstance:Lcom/cigna/coach/interfaces/IGoalsMissions;

.field private static healthLibraryInstance:Lcom/cigna/coach/interfaces/IHealthLibrary;

.field private static instance:Lcom/cigna/coach/factory/CoachFactory;

.field private static lifestyleInstance:Lcom/cigna/coach/interfaces/ILifeStyle;

.field private static widgetInstance:Lcom/cigna/coach/interfaces/IWidget;


# instance fields
.field protected context:Landroid/content/Context;

.field protected source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    new-instance v0, Lcom/cigna/coach/factory/CoachFactory;

    invoke-direct {v0}, Lcom/cigna/coach/factory/CoachFactory;-><init>()V

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->instance:Lcom/cigna/coach/factory/CoachFactory;

    .line 51
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->lifestyleInstance:Lcom/cigna/coach/interfaces/ILifeStyle;

    .line 52
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->goalsMissionsInstance:Lcom/cigna/coach/interfaces/IGoalsMissions;

    .line 53
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->healthLibraryInstance:Lcom/cigna/coach/interfaces/IHealthLibrary;

    .line 54
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->widgetInstance:Lcom/cigna/coach/interfaces/IWidget;

    .line 55
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->backupAndRestoreInstance:Lcom/cigna/coach/interfaces/IBackupAndRestore;

    .line 56
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->cmsManagerInstance:Lcom/cigna/coach/interfaces/ICMSManager;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->SHEALTH_UI:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-direct {p0, v0}, Lcom/cigna/coach/factory/CoachFactory;-><init>(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)V

    .line 76
    return-void
.end method

.method protected constructor <init>(Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;)V
    .locals 1
    .param p1, "source"    # Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    .line 59
    sget-object v0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->SHEALTH_UI:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    iput-object v0, p0, Lcom/cigna/coach/factory/CoachFactory;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .line 80
    iput-object p1, p0, Lcom/cigna/coach/factory/CoachFactory;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .line 81
    return-void
.end method

.method public static getInstance()Lcom/cigna/coach/factory/CoachFactory;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->instance:Lcom/cigna/coach/factory/CoachFactory;

    return-object v0
.end method

.method public static reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    new-instance v0, Lcom/cigna/coach/factory/CoachFactory;

    invoke-direct {v0}, Lcom/cigna/coach/factory/CoachFactory;-><init>()V

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->instance:Lcom/cigna/coach/factory/CoachFactory;

    .line 63
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->lifestyleInstance:Lcom/cigna/coach/interfaces/ILifeStyle;

    .line 64
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->goalsMissionsInstance:Lcom/cigna/coach/interfaces/IGoalsMissions;

    .line 65
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->healthLibraryInstance:Lcom/cigna/coach/interfaces/IHealthLibrary;

    .line 66
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->widgetInstance:Lcom/cigna/coach/interfaces/IWidget;

    .line 67
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->backupAndRestoreInstance:Lcom/cigna/coach/interfaces/IBackupAndRestore;

    .line 68
    sput-object v1, Lcom/cigna/coach/factory/CoachFactory;->cmsManagerInstance:Lcom/cigna/coach/interfaces/ICMSManager;

    .line 69
    return-void
.end method


# virtual methods
.method public getBackupAndRestore(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IBackupAndRestore;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 178
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->backupAndRestoreInstance:Lcom/cigna/coach/interfaces/IBackupAndRestore;

    if-nez v0, :cond_0

    .line 179
    iput-object p1, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    .line 180
    invoke-static {p1}, Lcom/cigna/coach/utils/CommonUtils;->defineServerURLsbasedOnRegionGroup(Landroid/content/Context;)V

    .line 181
    const-class v0, Lcom/cigna/coach/interfaces/IBackupAndRestore;

    new-instance v1, Lcom/cigna/coach/BackupAndRestore;

    invoke-direct {v1, p1}, Lcom/cigna/coach/BackupAndRestore;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/factory/CoachFactory;->getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IBackupAndRestore;

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->backupAndRestoreInstance:Lcom/cigna/coach/interfaces/IBackupAndRestore;

    .line 183
    :cond_0
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->backupAndRestoreInstance:Lcom/cigna/coach/interfaces/IBackupAndRestore;

    return-object v0
.end method

.method public getCMSManager(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ICMSManager;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 195
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->cmsManagerInstance:Lcom/cigna/coach/interfaces/ICMSManager;

    if-nez v0, :cond_0

    .line 196
    iput-object p1, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    .line 197
    invoke-static {p1}, Lcom/cigna/coach/utils/CommonUtils;->defineServerURLsbasedOnRegionGroup(Landroid/content/Context;)V

    .line 198
    const-class v0, Lcom/cigna/coach/interfaces/ICMSManager;

    new-instance v1, Lcom/cigna/coach/CMSManager;

    invoke-direct {v1, p1}, Lcom/cigna/coach/CMSManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/factory/CoachFactory;->getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ICMSManager;

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->cmsManagerInstance:Lcom/cigna/coach/interfaces/ICMSManager;

    .line 200
    :cond_0
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->cmsManagerInstance:Lcom/cigna/coach/interfaces/ICMSManager;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    return-object v0
.end method

.method public getGoalsMissions(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IGoalsMissions;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->goalsMissionsInstance:Lcom/cigna/coach/interfaces/IGoalsMissions;

    if-nez v0, :cond_0

    .line 110
    iput-object p1, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    .line 111
    invoke-static {p1}, Lcom/cigna/coach/utils/CommonUtils;->defineServerURLsbasedOnRegionGroup(Landroid/content/Context;)V

    .line 112
    const-class v0, Lcom/cigna/coach/interfaces/IGoalsMissions;

    new-instance v1, Lcom/cigna/coach/GoalsMissions;

    invoke-direct {v1, p1}, Lcom/cigna/coach/GoalsMissions;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/factory/CoachFactory;->getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IGoalsMissions;

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->goalsMissionsInstance:Lcom/cigna/coach/interfaces/IGoalsMissions;

    .line 114
    :cond_0
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->goalsMissionsInstance:Lcom/cigna/coach/interfaces/IGoalsMissions;

    return-object v0
.end method

.method public getHealthLibrary(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IHealthLibrary;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->healthLibraryInstance:Lcom/cigna/coach/interfaces/IHealthLibrary;

    if-nez v0, :cond_0

    .line 145
    iput-object p1, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    .line 146
    invoke-static {p1}, Lcom/cigna/coach/utils/CommonUtils;->defineServerURLsbasedOnRegionGroup(Landroid/content/Context;)V

    .line 147
    const-class v0, Lcom/cigna/coach/interfaces/IHealthLibrary;

    new-instance v1, Lcom/cigna/coach/HealthLibrary;

    invoke-direct {v1, p1}, Lcom/cigna/coach/HealthLibrary;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/factory/CoachFactory;->getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IHealthLibrary;

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->healthLibraryInstance:Lcom/cigna/coach/interfaces/IHealthLibrary;

    .line 149
    :cond_0
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->healthLibraryInstance:Lcom/cigna/coach/interfaces/IHealthLibrary;

    return-object v0
.end method

.method public getLifeStyle(Landroid/content/Context;)Lcom/cigna/coach/interfaces/ILifeStyle;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->lifestyleInstance:Lcom/cigna/coach/interfaces/ILifeStyle;

    if-nez v0, :cond_0

    .line 127
    iput-object p1, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    .line 128
    invoke-static {p1}, Lcom/cigna/coach/utils/CommonUtils;->defineServerURLsbasedOnRegionGroup(Landroid/content/Context;)V

    .line 129
    const-class v0, Lcom/cigna/coach/interfaces/ILifeStyle;

    new-instance v1, Lcom/cigna/coach/LifeStyle;

    invoke-direct {v1, p1}, Lcom/cigna/coach/LifeStyle;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/factory/CoachFactory;->getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/ILifeStyle;

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->lifestyleInstance:Lcom/cigna/coach/interfaces/ILifeStyle;

    .line 132
    :cond_0
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->lifestyleInstance:Lcom/cigna/coach/interfaces/ILifeStyle;

    return-object v0
.end method

.method protected getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "O:TT;>(",
            "Ljava/lang/Class",
            "<TT;>;TO;)TT;"
        }
    .end annotation

    .prologue
    .line 97
    .local p1, "ifcClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p2, "apiObj":Ljava/lang/Object;, "TO;"
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    new-instance v2, Lcom/cigna/coach/factory/ApiCallHandler;

    invoke-virtual {p0}, Lcom/cigna/coach/factory/CoachFactory;->getSource()Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-result-object v3

    iget-object v4, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    invoke-direct {v2, p2, v3, v4}, Lcom/cigna/coach/factory/ApiCallHandler;-><init>(Ljava/lang/Object;Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;Landroid/content/Context;)V

    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected getSource()Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/cigna/coach/factory/CoachFactory;->source:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    return-object v0
.end method

.method public getWidget(Landroid/content/Context;)Lcom/cigna/coach/interfaces/IWidget;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->widgetInstance:Lcom/cigna/coach/interfaces/IWidget;

    if-nez v0, :cond_0

    .line 162
    iput-object p1, p0, Lcom/cigna/coach/factory/CoachFactory;->context:Landroid/content/Context;

    .line 163
    invoke-static {p1}, Lcom/cigna/coach/utils/CommonUtils;->defineServerURLsbasedOnRegionGroup(Landroid/content/Context;)V

    .line 164
    const-class v0, Lcom/cigna/coach/interfaces/IWidget;

    new-instance v1, Lcom/cigna/coach/Widget;

    invoke-direct {v1, p1}, Lcom/cigna/coach/Widget;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v0, v1}, Lcom/cigna/coach/factory/CoachFactory;->getProxy(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/interfaces/IWidget;

    sput-object v0, Lcom/cigna/coach/factory/CoachFactory;->widgetInstance:Lcom/cigna/coach/interfaces/IWidget;

    .line 166
    :cond_0
    sget-object v0, Lcom/cigna/coach/factory/CoachFactory;->widgetInstance:Lcom/cigna/coach/interfaces/IWidget;

    return-object v0
.end method
