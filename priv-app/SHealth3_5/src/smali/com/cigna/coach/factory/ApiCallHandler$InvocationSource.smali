.class public final enum Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
.super Ljava/lang/Enum;
.source "ApiCallHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/factory/ApiCallHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InvocationSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

.field public static final enum COACH_SERVICE:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

.field public static final enum DATA_PROCESSOR:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

.field public static final enum SHEALTH_UI:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field key:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 59
    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    const-string v5, "SHEALTH_UI"

    invoke-direct {v4, v5, v8, v6}, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->SHEALTH_UI:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    const-string v5, "COACH_SERVICE"

    invoke-direct {v4, v5, v6, v7}, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->COACH_SERVICE:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    new-instance v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    const-string v5, "DATA_PROCESSOR"

    invoke-direct {v4, v5, v7, v9}, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->DATA_PROCESSOR:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .line 58
    new-array v4, v9, [Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->SHEALTH_UI:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    aput-object v5, v4, v8

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->COACH_SERVICE:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->DATA_PROCESSOR:Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    aput-object v5, v4, v7

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->$VALUES:[Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    .line 62
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->codeValueMap:Ljava/util/HashMap;

    .line 77
    invoke-static {}, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->values()[Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 78
    .local v3, "type":Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    sget-object v4, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->getKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 80
    .end local v3    # "type":Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->key:I

    .line 66
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 58
    const-class v0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->$VALUES:[Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    invoke-virtual {v0}, [Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    return-object v0
.end method


# virtual methods
.method public getInstance(I)Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 73
    sget-object v0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;

    return-object v0
.end method

.method public getKey()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/cigna/coach/factory/ApiCallHandler$InvocationSource;->key:I

    return v0
.end method
