.class public Lcom/cigna/coach/CMSManager;
.super Ljava/lang/Object;
.source "CMSManager.java"

# interfaces
.implements Lcom/cigna/coach/interfaces/ICMSManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method


# virtual methods
.method public downloadContentFile(Lcom/cigna/coach/interfaces/ICMSManager$ICMSDownloadStatusCallback;)V
    .locals 0
    .param p1, "downloadStatusCallback"    # Lcom/cigna/coach/interfaces/ICMSManager$ICMSDownloadStatusCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 39
    return-void
.end method

.method public isCoachSupportedLanguage(Ljava/util/Locale;)Z
    .locals 1
    .param p1, "locale"    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/cigna/coach/exceptions/CoachException;
        }
    .end annotation

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method
