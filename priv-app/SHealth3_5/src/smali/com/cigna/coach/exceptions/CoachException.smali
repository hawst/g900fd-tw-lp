.class public Lcom/cigna/coach/exceptions/CoachException;
.super Ljava/lang/Exception;
.source "CoachException.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    }
.end annotation


# static fields
.field protected static final serialVersionUID:J = 0x1L


# instance fields
.field exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 59
    sget-object v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    iput-object v0, p0, Lcom/cigna/coach/exceptions/CoachException;->exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "inMessage"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 59
    sget-object v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    iput-object v0, p0, Lcom/cigna/coach/exceptions/CoachException;->exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "inMessage"    # Ljava/lang/String;
    .param p2, "inThrowable"    # Ljava/lang/Throwable;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 59
    sget-object v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    iput-object v0, p0, Lcom/cigna/coach/exceptions/CoachException;->exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 85
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "inThrowable"    # Ljava/lang/Throwable;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 59
    sget-object v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    iput-object v0, p0, Lcom/cigna/coach/exceptions/CoachException;->exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 94
    return-void
.end method


# virtual methods
.method public getExceptionType()Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/cigna/coach/exceptions/CoachException;->exceptionType:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    return-object v0
.end method
