.class public final enum Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
.super Ljava/lang/Enum;
.source "CoachException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cigna/coach/exceptions/CoachException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoachExceptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

.field public static final enum NO_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

.field public static final enum UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

.field public static final enum USER_NOT_FOUND_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

.field private static codeValueMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field key:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 33
    new-instance v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    const-string v5, "NO_EXCEPTION"

    invoke-direct {v4, v5, v6, v6}, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->NO_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    new-instance v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    const-string v5, "UNKNOWN_EXCEPTION"

    invoke-direct {v4, v5, v7, v7}, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    new-instance v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    const-string v5, "USER_NOT_FOUND_EXCEPTION"

    invoke-direct {v4, v5, v8, v8}, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->USER_NOT_FOUND_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 32
    const/4 v4, 0x3

    new-array v4, v4, [Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    sget-object v5, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->NO_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    aput-object v5, v4, v6

    sget-object v5, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->UNKNOWN_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    aput-object v5, v4, v7

    sget-object v5, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->USER_NOT_FOUND_EXCEPTION:Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    aput-object v5, v4, v8

    sput-object v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->$VALUES:[Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    .line 35
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sput-object v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->codeValueMap:Ljava/util/HashMap;

    .line 50
    invoke-static {}, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->values()[Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    move-result-object v0

    .local v0, "arr$":[Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 51
    .local v3, "type":Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    sget-object v4, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->codeValueMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->getKey()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    .end local v3    # "type":Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->key:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 32
    const-class v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    return-object v0
.end method

.method public static values()[Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->$VALUES:[Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    invoke-virtual {v0}, [Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    return-object v0
.end method


# virtual methods
.method public getInstance(I)Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 46
    sget-object v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->codeValueMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;

    return-object v0
.end method

.method public getKey()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lcom/cigna/coach/exceptions/CoachException$CoachExceptionType;->key:I

    return v0
.end method
