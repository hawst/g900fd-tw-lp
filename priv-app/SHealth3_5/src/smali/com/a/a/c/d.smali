.class public Lcom/a/a/c/d;
.super Ljava/lang/Object;
.source "UTUtdid.java"


# static fields
.field private static final b:Ljava/lang/Object;

.field private static c:Lcom/a/a/c/d;

.field private static final j:Ljava/lang/String;


# instance fields
.field private a:Landroid/content/Context;

.field private d:Ljava/lang/String;

.field private e:Lcom/a/a/c/e;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Lcom/a/a/b/a/c;

.field private i:Lcom/a/a/b/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/a/a/c/d;->b:Ljava/lang/Object;

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lcom/a/a/c/d;->c:Lcom/a/a/c/d;

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".UTSystemConfig"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Global"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/a/a/c/d;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v1, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    .line 28
    iput-object v1, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    .line 30
    const-string/jumbo v0, "xx_utdid_key"

    iput-object v0, p0, Lcom/a/a/c/d;->f:Ljava/lang/String;

    .line 31
    const-string/jumbo v0, "xx_utdid_domain"

    iput-object v0, p0, Lcom/a/a/c/d;->g:Ljava/lang/String;

    .line 34
    iput-object v1, p0, Lcom/a/a/c/d;->h:Lcom/a/a/b/a/c;

    .line 37
    iput-object v1, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    .line 43
    iput-object p1, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    .line 44
    new-instance v0, Lcom/a/a/b/a/c;

    sget-object v2, Lcom/a/a/c/d;->j:Ljava/lang/String;

    const-string v3, "Alvin2"

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/a/a/b/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    .line 47
    new-instance v0, Lcom/a/a/b/a/c;

    const-string v2, ".DataStorage"

    const-string v3, "ContextData"

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/a/a/b/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)V

    iput-object v0, p0, Lcom/a/a/c/d;->h:Lcom/a/a/b/a/c;

    .line 49
    new-instance v0, Lcom/a/a/c/e;

    invoke-direct {v0}, Lcom/a/a/c/e;-><init>()V

    iput-object v0, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    .line 50
    const-string v0, "K_%d"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/a/c/d;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/a/a/a/a/e;->b(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/c/d;->f:Ljava/lang/String;

    .line 51
    const-string v0, "D_%d"

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/a/a/c/d;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/a/a/a/a/e;->b(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/c/d;->g:Ljava/lang/String;

    .line 52
    return-void
.end method

.method static a(Lcom/a/a/c/a;)J
    .locals 5

    .prologue
    .line 74
    if-eqz p0, :cond_0

    .line 75
    const-string v0, "%s%s%s%s%s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/a/a/c/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/a/a/c/a;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/a/a/c/a;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/a/a/c/a;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/a/a/c/a;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {v0}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    new-instance v1, Ljava/util/zip/Adler32;

    invoke-direct {v1}, Ljava/util/zip/Adler32;-><init>()V

    .line 81
    invoke-virtual {v1}, Ljava/util/zip/Adler32;->reset()V

    .line 82
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/zip/Adler32;->update([B)V

    .line 83
    invoke-virtual {v1}, Ljava/util/zip/Adler32;->getValue()J

    move-result-wide v0

    .line 87
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/a/a/c/d;
    .locals 2

    .prologue
    .line 55
    if-eqz p0, :cond_1

    .line 56
    sget-object v0, Lcom/a/a/c/d;->c:Lcom/a/a/c/d;

    if-nez v0, :cond_1

    .line 57
    sget-object v1, Lcom/a/a/c/d;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 58
    :try_start_0
    sget-object v0, Lcom/a/a/c/d;->c:Lcom/a/a/c/d;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/a/a/c/d;

    invoke-direct {v0, p0}, Lcom/a/a/c/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/a/a/c/d;->c:Lcom/a/a/c/d;

    .line 61
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :cond_1
    sget-object v0, Lcom/a/a/c/d;->c:Lcom/a/a/c/d;

    return-object v0

    .line 61
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static a([B)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 415
    const-string v0, "d6fc3a4a06adbde89223bvefedc24fecde188aaa9161"

    .line 416
    const-string v1, "HmacSHA1"

    invoke-static {v1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    .line 417
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v1}, Ljavax/crypto/Mac;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 418
    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 419
    invoke-virtual {v1, p0}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v0

    .line 420
    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/a/a/a/a/b;->b([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 97
    if-eqz p1, :cond_0

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 100
    :cond_0
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x18

    if-ne v0, v1, :cond_5

    .line 101
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    if-eqz v0, :cond_5

    .line 102
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "UTDID"

    invoke-virtual {v0, v1}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 103
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "EI"

    invoke-virtual {v0, v1}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-static {v0}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 105
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/a/a/a/d;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v2, "SI"

    invoke-virtual {v1, v2}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-static {v1}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 109
    iget-object v1, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/a/a/a/a/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 111
    :cond_2
    iget-object v2, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v4, "DID"

    invoke-virtual {v2, v4}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    invoke-static {v2}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v2, v0

    .line 116
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 117
    :cond_4
    new-instance v3, Lcom/a/a/c/a;

    invoke-direct {v3}, Lcom/a/a/c/a;-><init>()V

    .line 118
    invoke-virtual {v3, v0}, Lcom/a/a/c/a;->a(Ljava/lang/String;)V

    .line 119
    invoke-virtual {v3, v1}, Lcom/a/a/c/a;->b(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v3, p1}, Lcom/a/a/c/a;->d(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v3, v2}, Lcom/a/a/c/a;->c(Ljava/lang/String;)V

    .line 122
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Lcom/a/a/c/a;->b(J)V

    .line 124
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "UTDID"

    invoke-virtual {v0, v1, p1}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "EI"

    invoke-virtual {v3}, Lcom/a/a/c/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "SI"

    invoke-virtual {v3}, Lcom/a/a/c/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "DID"

    invoke-virtual {v3}, Lcom/a/a/c/a;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string/jumbo v1, "timestamp"

    invoke-virtual {v3}, Lcom/a/a/c/a;->a()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;J)V

    .line 129
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "S"

    invoke-static {v3}, Lcom/a/a/c/d;->a(Lcom/a/a/c/a;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;J)V

    .line 130
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    invoke-virtual {v0}, Lcom/a/a/b/a/c;->a()Z

    .line 134
    :cond_5
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/a/a/c/d;->i:Lcom/a/a/b/a/c;

    const-string v1, "UTDID"

    invoke-virtual {v0, v1}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    invoke-static {v0}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    invoke-virtual {v1, v0}, Lcom/a/a/c/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 220
    if-eqz v1, :cond_0

    .line 225
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 142
    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/a/a/c/d;->h:Lcom/a/a/b/a/c;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/a/a/c/d;->h:Lcom/a/a/b/a/c;

    iget-object v1, p0, Lcom/a/a/c/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/a/a/c/d;->h:Lcom/a/a/b/a/c;

    iget-object v1, p0, Lcom/a/a/c/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/a/a/c/d;->h:Lcom/a/a/b/a/c;

    invoke-virtual {v0}, Lcom/a/a/b/a/c;->a()Z

    .line 151
    :cond_0
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    const-string v1, "android.permission.WRITE_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 162
    if-nez v0, :cond_1

    .line 163
    if-eqz p1, :cond_1

    .line 164
    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 167
    :cond_0
    const/16 v0, 0x18

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 168
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "mqBRboGZkQPcAkyk"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-static {v0}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "mqBRboGZkQPcAkyk"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 177
    :cond_1
    return-void
.end method

.method private final c()[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    .line 380
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 381
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 382
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    .line 383
    const/4 v3, 0x3

    .line 385
    invoke-static {v0}, Lcom/a/a/a/a/c;->a(I)[B

    move-result-object v0

    .line 386
    invoke-static {v2}, Lcom/a/a/a/a/c;->a(I)[B

    move-result-object v2

    .line 387
    invoke-virtual {v1, v0, v6, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 388
    invoke-virtual {v1, v2, v6, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 389
    invoke-virtual {v1, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 390
    invoke-virtual {v1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 393
    :try_start_0
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/a/a/a/a/d;->a(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 397
    :goto_0
    invoke-static {v0}, Lcom/a/a/a/a/e;->b(Ljava/lang/String;)I

    move-result v0

    .line 398
    invoke-static {v0}, Lcom/a/a/a/a/c;->a(I)[B

    move-result-object v0

    .line 399
    invoke-virtual {v1, v0, v6, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 400
    const-string v0, ""

    .line 401
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/c/d;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 402
    invoke-static {v0}, Lcom/a/a/a/a/e;->b(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/a/a/a/a/c;->a(I)[B

    move-result-object v0

    .line 403
    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 404
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    .line 394
    :catch_0
    move-exception v0

    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dxCRMxhQkdGePGnp"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "dxCRMxhQkdGePGnp"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 192
    :cond_0
    return-void
.end method

.method private e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    const-string v1, "android.permission.WRITE_SETTINGS"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 203
    if-nez v0, :cond_0

    .line 204
    if-eqz p1, :cond_0

    .line 205
    invoke-direct {p0, p1}, Lcom/a/a/c/d;->d(Ljava/lang/String;)V

    .line 208
    :cond_0
    return-void
.end method

.method private f(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 235
    if-eqz p1, :cond_1

    .line 237
    const/16 v0, 0x18

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v0, 0x19

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_1

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    :cond_0
    const/4 v0, 0x1

    .line 242
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 252
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 257
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "mqBRboGZkQPcAkyk"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_2

    const/16 v1, 0x18

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 263
    :cond_2
    new-instance v3, Lcom/a/a/c/f;

    invoke-direct {v3}, Lcom/a/a/c/f;-><init>()V

    .line 265
    const/4 v1, 0x0

    .line 269
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "dxCRMxhQkdGePGnp"

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 271
    invoke-static {v2}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 273
    invoke-virtual {v3, v2}, Lcom/a/a/c/f;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 274
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->f(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 276
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 252
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 280
    :cond_3
    :try_start_2
    invoke-virtual {v3, v2}, Lcom/a/a/c/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_e

    .line 282
    iget-object v4, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    invoke-virtual {v4, v0}, Lcom/a/a/c/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 283
    invoke-static {v0}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 285
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->e(Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/a/a/c/d;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "dxCRMxhQkdGePGnp"

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 292
    :goto_1
    iget-object v2, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    invoke-virtual {v2, v0}, Lcom/a/a/c/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 293
    invoke-direct {p0, v2}, Lcom/a/a/c/d;->f(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 294
    iput-object v2, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    .line 296
    invoke-direct {p0, v2}, Lcom/a/a/c/d;->a(Ljava/lang/String;)V

    .line 298
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->b(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/c/d;->c(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    goto :goto_0

    .line 306
    :cond_4
    const/4 v0, 0x1

    move v1, v0

    .line 310
    :cond_5
    invoke-direct {p0}, Lcom/a/a/c/d;->b()Ljava/lang/String;

    move-result-object v0

    .line 311
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 312
    iget-object v2, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    invoke-virtual {v2, v0}, Lcom/a/a/c/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 313
    if-eqz v1, :cond_6

    .line 315
    invoke-direct {p0, v2}, Lcom/a/a/c/d;->e(Ljava/lang/String;)V

    .line 318
    :cond_6
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->c(Ljava/lang/String;)V

    .line 320
    invoke-direct {p0, v2}, Lcom/a/a/c/d;->b(Ljava/lang/String;)V

    .line 321
    iput-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 326
    :cond_7
    iget-object v0, p0, Lcom/a/a/c/d;->h:Lcom/a/a/b/a/c;

    iget-object v2, p0, Lcom/a/a/c/d;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/a/a/b/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 327
    invoke-static {v2}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 328
    invoke-virtual {v3, v2}, Lcom/a/a/c/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 329
    if-nez v0, :cond_8

    .line 330
    iget-object v0, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    invoke-virtual {v0, v2}, Lcom/a/a/c/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 332
    :cond_8
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 333
    iget-object v2, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    invoke-virtual {v2, v0}, Lcom/a/a/c/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 334
    invoke-static {v0}, Lcom/a/a/a/a/e;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 335
    iput-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    .line 336
    if-eqz v1, :cond_9

    .line 338
    invoke-direct {p0, v2}, Lcom/a/a/c/d;->e(Ljava/lang/String;)V

    .line 341
    :cond_9
    iget-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/a/a/c/d;->a(Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 350
    :cond_a
    :try_start_3
    invoke-direct {p0}, Lcom/a/a/c/d;->c()[B

    move-result-object v0

    .line 351
    if-eqz v0, :cond_d

    .line 353
    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/a/a/a/a/b;->b([BI)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    .line 354
    iget-object v2, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/a/a/c/d;->a(Ljava/lang/String;)V

    .line 356
    iget-object v2, p0, Lcom/a/a/c/d;->e:Lcom/a/a/c/e;

    invoke-virtual {v2, v0}, Lcom/a/a/c/e;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 357
    if-eqz v0, :cond_c

    .line 358
    if-eqz v1, :cond_b

    .line 360
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->e(Ljava/lang/String;)V

    .line 363
    :cond_b
    invoke-direct {p0, v0}, Lcom/a/a/c/d;->b(Ljava/lang/String;)V

    .line 365
    :cond_c
    iget-object v0, p0, Lcom/a/a/c/d;->d:Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 367
    :catch_0
    move-exception v0

    .line 368
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 370
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_e
    move-object v0, v2

    goto/16 :goto_1
.end method
