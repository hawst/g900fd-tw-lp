.class public Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;
.super Ljava/lang/Object;
.source "MaximUVSensorEvent.java"


# instance fields
.field public mIsFixedDebugLocation:Z

.field public mIsFixedLocation:Z

.field public mSolarAltitude:F

.field public mSolarAzimuth:F

.field public mTimeStamp:J

.field public mUVIndex:F

.field public mUVMaxIndex:F

.field public mUVMaxSysTime:F

.field public mUVMaxTestTime:F

.field public mUV_ADC:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUV_ADC:I

    .line 5
    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    .line 6
    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxIndex:F

    .line 7
    iput-boolean v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedLocation:Z

    .line 8
    iput-boolean v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedDebugLocation:Z

    .line 10
    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxSysTime:F

    .line 11
    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxTestTime:F

    .line 13
    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mSolarAltitude:F

    .line 14
    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mSolarAzimuth:F

    .line 16
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mTimeStamp:J

    .line 3
    return-void
.end method
