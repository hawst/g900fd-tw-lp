.class public Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;
.super Ljava/lang/Object;
.source "CalculaterDiffAngle.java"


# static fields
.field static final TAG:Ljava/lang/String; = "UvSLocation"


# instance fields
.field private mAngleConvetor:Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;

.field private mDevEuler:[F

.field private mDevQ:[F

.field private mDiffAngle:F

.field private mSunEuler:[F


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mAngleConvetor:Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDiffAngle:F

    .line 12
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mSunEuler:[F

    .line 14
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    .line 15
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevEuler:[F

    .line 18
    new-instance v0, Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;

    invoke-direct {v0}, Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;-><init>()V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mAngleConvetor:Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;

    .line 20
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mSunEuler:[F

    .line 22
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    .line 23
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevEuler:[F

    .line 24
    return-void
.end method


# virtual methods
.method public GetDiffAngle([F[F)F
    .locals 9
    .param p1, "fDevice_q"    # [F
    .param p2, "fSun_pos"    # [F

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 27
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_1

    .line 30
    array-length v1, p1

    if-ne v1, v8, :cond_0

    .line 31
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    aget v3, v3, v6

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    aget v4, v4, v6

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    aget v3, v3, v5

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    aget v4, v4, v5

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    aget v3, v3, v7

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    aget v4, v4, v7

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    aput v2, v1, v8

    .line 34
    :cond_0
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mSunEuler:[F

    const/4 v2, 0x0

    aput v2, v1, v6

    .line 35
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mSunEuler:[F

    aget v2, p2, v5

    aput v2, v1, v5

    .line 36
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mSunEuler:[F

    aget v2, p2, v6

    aput v2, v1, v7

    .line 38
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mAngleConvetor:Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevEuler:[F

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    invoke-virtual {v1, v2, v3}, Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;->CalculateEulerFromQuaternionB([F[F)V

    .line 40
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevEuler:[F

    aget v1, v1, v5

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mSunEuler:[F

    aget v2, v2, v5

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iput v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDiffAngle:F

    .line 42
    iget v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDiffAngle:F

    return v1

    .line 28
    :cond_1
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->mDevQ:[F

    aget v2, p1, v0

    aput v2, v1, v0

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
