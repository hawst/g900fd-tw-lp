.class public Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;
.super Ljava/lang/Object;
.source "UVAlgorithmInputData.java"


# instance fields
.field public mAltitude:F

.field public mDate:F

.field public mExtTemper:F

.field public mGest:F

.field public mHR:F

.field public mIntervalTime:F

.field public mIsIndoor:I

.field public mIsLogging:I

.field public mLatitude:F

.field public mLightLux:F

.field public mLongitude:F

.field public mTemperature:F

.field public mTime:F

.field public mUV_ADC:I

.field public mmCurPressureVal:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    .line 5
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLatitude:F

    .line 6
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLongitude:F

    .line 7
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mAltitude:F

    .line 8
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTemperature:F

    .line 9
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mHR:F

    .line 10
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mGest:F

    .line 11
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLightLux:F

    .line 12
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsIndoor:I

    .line 13
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsLogging:I

    .line 15
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIntervalTime:F

    .line 16
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mmCurPressureVal:F

    .line 18
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mDate:F

    .line 19
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTime:F

    .line 20
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mExtTemper:F

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;)V
    .locals 2
    .param p1, "t"    # Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    .line 5
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLatitude:F

    .line 6
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLongitude:F

    .line 7
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mAltitude:F

    .line 8
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTemperature:F

    .line 9
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mHR:F

    .line 10
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mGest:F

    .line 11
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLightLux:F

    .line 12
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsIndoor:I

    .line 13
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsLogging:I

    .line 15
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIntervalTime:F

    .line 16
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mmCurPressureVal:F

    .line 18
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mDate:F

    .line 19
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTime:F

    .line 20
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mExtTemper:F

    .line 26
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    .line 27
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLatitude:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLatitude:F

    .line 28
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLongitude:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLongitude:F

    .line 29
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mAltitude:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mAltitude:F

    .line 30
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTemperature:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTemperature:F

    .line 31
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mHR:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mHR:F

    .line 32
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mGest:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mGest:F

    .line 33
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLightLux:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLightLux:F

    .line 34
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsIndoor:I

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsIndoor:I

    .line 35
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsLogging:I

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsLogging:I

    .line 37
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIntervalTime:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIntervalTime:F

    .line 38
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mmCurPressureVal:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mmCurPressureVal:F

    .line 40
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mDate:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mDate:F

    .line 41
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTime:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTime:F

    .line 42
    iget v0, p1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mExtTemper:F

    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mExtTemper:F

    .line 43
    return-void
.end method


# virtual methods
.method public InitValue()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    .line 46
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLatitude:F

    .line 47
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLongitude:F

    .line 48
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mAltitude:F

    .line 49
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTemperature:F

    .line 50
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mHR:F

    .line 51
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mGest:F

    .line 52
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLightLux:F

    .line 53
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsIndoor:I

    .line 54
    iput v1, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsLogging:I

    .line 56
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIntervalTime:F

    .line 57
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mmCurPressureVal:F

    .line 59
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mDate:F

    .line 60
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTime:F

    .line 61
    iput v0, p0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mExtTemper:F

    .line 62
    return-void
.end method
