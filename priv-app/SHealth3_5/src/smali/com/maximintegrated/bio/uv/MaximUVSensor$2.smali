.class Lcom/maximintegrated/bio/uv/MaximUVSensor$2;
.super Ljava/lang/Object;
.source "MaximUVSensor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/maximintegrated/bio/uv/MaximUVSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;


# direct methods
.method constructor <init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$2;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    .line 759
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 774
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 763
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$2;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v0, v1}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$24(Lcom/maximintegrated/bio/uv/MaximUVSensor;F)V

    .line 764
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    array-length v0, v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 765
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$2;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v0

    const/4 v1, 0x3

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 769
    :goto_0
    return-void

    .line 767
    :cond_0
    const-string v0, "UvSLocation"

    const-string v1, "Pressure Sensor has not Altitude info....."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
