.class public Lcom/maximintegrated/bio/uv/LogFileWrite;
.super Ljava/lang/Object;
.source "LogFileWrite.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mFos:Ljava/io/FileOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const-string v0, "LOGWRITE"

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->TAG:Ljava/lang/String;

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->mFos:Ljava/io/FileOutputStream;

    .line 16
    return-void
.end method


# virtual methods
.method CloseLogFile()Z
    .locals 3

    .prologue
    .line 54
    :try_start_0
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->mFos:Ljava/io/FileOutputStream;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 56
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->mFos:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 60
    const-string v1, "LOGWRITE"

    const-string v2, "File Close Error"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method CreateLogFile(Ljava/lang/String;)Z
    .locals 7
    .param p1, "strType"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 19
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 20
    .local v3, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    .line 21
    new-instance v2, Ljava/io/File;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 22
    .local v2, "fp":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 24
    :try_start_0
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :cond_0
    :goto_0
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    const/4 v5, 0x1

    invoke-direct {v4, v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v4, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->mFos:Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 36
    :goto_1
    return v6

    .line 25
    :catch_0
    move-exception v1

    .line 27
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 32
    .end local v1    # "e1":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 34
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method FileWrite(Ljava/lang/String;)Z
    .locals 5
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 41
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    .line 42
    .local v1, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 44
    .local v2, "theByteArray":[B
    :try_start_0
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->mFos:Ljava/io/FileOutputStream;

    if-eqz v3, :cond_0

    .line 45
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/LogFileWrite;->mFos:Ljava/io/FileOutputStream;

    invoke-virtual {v3, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
