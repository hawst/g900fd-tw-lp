.class Lcom/maximintegrated/bio/uv/MaximUVSensor$5;
.super Ljava/lang/Object;
.source "MaximUVSensor.java"

# interfaces
.implements Lcom/samsung/location/SCurrentLocListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/maximintegrated/bio/uv/MaximUVSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;


# direct methods
.method constructor <init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    .line 827
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCurrentLocation(Landroid/location/Location;)V
    .locals 6
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const/4 v5, 0x1

    .line 831
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUsingLocationType:I
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$30(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 832
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    invoke-static {v2, v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$31(Lcom/maximintegrated/bio/uv/MaximUVSensor;Z)V

    .line 833
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    double-to-float v3, v3

    aput v3, v2, v5

    .line 834
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    double-to-float v4, v4

    aput v4, v2, v3

    .line 837
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string/jumbo v3, "yyMMdd"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 838
    .local v0, "currentDate":Ljava/lang/String;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "HHmmss"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 840
    .local v1, "currentTime":Ljava/lang/String;
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    const/16 v3, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-float v4, v4

    aput v4, v2, v3

    .line 841
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    const/16 v3, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-float v4, v4

    aput v4, v2, v3

    .line 843
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    const/16 v3, 0xa

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 844
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v3}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v3

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$27(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->runAlgorithm([F[F)V

    .line 846
    const-string v2, "UvSLocation"

    const-string v3, "SLocation fixed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    .end local v0    # "currentDate":Ljava/lang/String;
    .end local v1    # "currentTime":Ljava/lang/String;
    :cond_0
    return-void
.end method
