.class Lcom/maximintegrated/bio/uv/MaximUVSensor$1;
.super Ljava/lang/Object;
.source "MaximUVSensor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/maximintegrated/bio/uv/MaximUVSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;


# direct methods
.method constructor <init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    .line 686
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 756
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 690
    monitor-enter p0

    .line 691
    :try_start_0
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    const/high16 v5, -0x40000000    # -2.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_1

    .line 692
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/4 v5, 0x6

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v4, v5

    .line 690
    :cond_0
    :goto_0
    monitor-exit p0

    .line 751
    return-void

    .line 693
    :cond_1
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    const/high16 v5, -0x3fc00000    # -3.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_2

    .line 694
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/4 v5, 0x5

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v7, 0x0

    aget v6, v6, v7

    aput v6, v4, v5

    goto :goto_0

    .line 690
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 695
    :cond_2
    :try_start_1
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    cmpl-float v4, v4, v6

    if-ltz v4, :cond_0

    .line 696
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v3, v4, v5

    .line 697
    .local v3, "uvCount":F
    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    const/high16 v5, 0x41800000    # 16.0f

    div-float v2, v4, v5

    .line 698
    .local v2, "temperature":F
    const/high16 v4, 0x42fe0000    # 127.0f

    cmpl-float v4, v2, v4

    if-lez v4, :cond_3

    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v5, 0x43800000    # 256.0f

    sub-float/2addr v5, v2

    mul-float v2, v4, v5

    .line 700
    :cond_3
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/4 v5, 0x0

    aput v3, v4, v5

    .line 701
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/4 v5, 0x4

    aput v2, v4, v5

    .line 703
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSampleCount:I
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$1(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I

    move-result v5

    add-int/lit8 v6, v5, 0x1

    invoke-static {v4, v6}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$2(Lcom/maximintegrated/bio/uv/MaximUVSensor;I)V

    rem-int/lit16 v4, v5, 0x96

    if-nez v4, :cond_4

    .line 704
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/16 v5, 0xb

    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # invokes: Lcom/maximintegrated/bio/uv/MaximUVSensor;->getExtTemperature()F
    invoke-static {v6}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$3(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F

    move-result v6

    aput v6, v4, v5

    .line 707
    :cond_4
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "yyMMdd"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 708
    .local v0, "currentDate":Ljava/lang/String;
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "HHmmss"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 710
    .local v1, "currentTime":Ljava/lang/String;
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/16 v5, 0xc

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    aput v6, v4, v5

    .line 711
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/16 v5, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    int-to-float v6, v6

    aput v6, v4, v5

    .line 714
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsIndoor:Z
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$4(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 715
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/16 v5, 0x8

    const/high16 v6, 0x3f800000    # 1.0f

    aput v6, v4, v5

    .line 726
    :goto_1
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$5(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z
    invoke-static {v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$6(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Z

    move-result v5

    iput-boolean v5, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedLocation:Z

    .line 727
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$5(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v5

    const/4 v6, 0x0

    aget v5, v5, v6

    float-to-int v5, v5

    iput v5, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUV_ADC:I

    .line 729
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # invokes: Lcom/maximintegrated/bio/uv/MaximUVSensor;->filled_CurInputDataFromAlgorithmData()V
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$7(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    .line 730
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsFirstUVData:Z
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$8(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 731
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-wide v5, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-static {v4, v5, v6}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$9(Lcom/maximintegrated/bio/uv/MaximUVSensor;J)V

    .line 733
    :cond_5
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-wide v5, p1, Landroid/hardware/SensorEvent;->timestamp:J

    invoke-static {v4, v5, v6}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$10(Lcom/maximintegrated/bio/uv/MaximUVSensor;J)V

    .line 734
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # invokes: Lcom/maximintegrated/bio/uv/MaximUVSensor;->getIntervalTime()F
    invoke-static {v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$11(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F

    move-result v5

    invoke-static {v4, v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$12(Lcom/maximintegrated/bio/uv/MaximUVSensor;F)V

    .line 735
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$13(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIntervalTime:F
    invoke-static {v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$14(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F

    move-result v5

    iput v5, v4, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIntervalTime:F

    .line 736
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$13(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    move-result-object v4

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurPressureVal:F
    invoke-static {v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$15(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F

    move-result v5

    iput v5, v4, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mmCurPressureVal:F

    .line 737
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$16(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Ljava/util/Vector;

    move-result-object v4

    new-instance v5, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;
    invoke-static {v6}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$13(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;-><init>(Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;)V

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 739
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$17(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I

    move-result v4

    if-nez v4, :cond_8

    .line 740
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # invokes: Lcom/maximintegrated/bio/uv/MaximUVSensor;->onlyUsingJIGTestLib()V
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$18(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    .line 747
    :cond_6
    :goto_2
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-object v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J
    invoke-static {v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$21(Lcom/maximintegrated/bio/uv/MaximUVSensor;)J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$22(Lcom/maximintegrated/bio/uv/MaximUVSensor;J)V

    .line 748
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$23(Lcom/maximintegrated/bio/uv/MaximUVSensor;Z)V

    goto/16 :goto_0

    .line 718
    :cond_7
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v4

    const/16 v5, 0x8

    const/4 v6, 0x0

    aput v6, v4, v5

    goto/16 :goto_1

    .line 741
    :cond_8
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$17(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I

    move-result v4

    if-ne v4, v7, :cond_9

    .line 742
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # invokes: Lcom/maximintegrated/bio/uv/MaximUVSensor;->onlyUsingUXTestLib()V
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$19(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    goto :goto_2

    .line 743
    :cond_9
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I
    invoke-static {v4}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$17(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 744
    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    const/4 v5, 0x0

    # invokes: Lcom/maximintegrated/bio/uv/MaximUVSensor;->onlyUsingSHealthLib(I)V
    invoke-static {v4, v5}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$20(Lcom/maximintegrated/bio/uv/MaximUVSensor;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method
