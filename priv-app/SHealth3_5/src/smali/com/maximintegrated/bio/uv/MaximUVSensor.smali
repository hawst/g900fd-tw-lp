.class public Lcom/maximintegrated/bio/uv/MaximUVSensor;
.super Ljava/lang/Object;
.source "MaximUVSensor.java"


# static fields
.field private static final ANGLE_COUNT:I = 0x4

.field private static final BSP_LSI:I = 0x1

.field private static final BSP_NON:I = 0x2

.field private static final BSP_QUA:I = 0x0

.field private static final FIFTEEN_DEGREE:I = 0x2

.field private static final FIVE_DEGREE:I = 0x0

.field private static final IN_ALTITUDE:I = 0x3

.field private static final IN_DATE:I = 0xc

.field private static final IN_EXT_TEMPERATURE:I = 0xb

.field private static final IN_GEST:I = 0x6

.field private static final IN_HR:I = 0x5

.field private static final IN_ISINDOOR:I = 0x8

.field private static final IN_LATITUDE:I = 0x1

.field private static final IN_LIGHT_LUX:I = 0x7

.field private static final IN_LOGGING:I = 0x9

.field private static final IN_LOG_ONOFF:I = 0xa

.field private static final IN_LONGITUDE:I = 0x2

.field private static final IN_MAX:I = 0xe

.field private static final IN_TEMPERATURE:I = 0x4

.field private static final IN_TIME:I = 0xd

.field private static final IN_UVRAW:I = 0x0

.field private static final JIG_TEST:I = 0x0

.field private static final LSI_TEMP_SYSFS:Ljava/lang/String; = "/sys/class/sec/sec-thermistor/temperature"

.field private static final MAX86902_ENHANCED_UV_GESTURE_MODE:I = -0x2

.field private static final MAX86902_ENHANCED_UV_HR_MODE:I = -0x3

.field private static final OUT_DAYLIGHTSAVING:I = 0xc

.field private static final OUT_FOV:I = 0x5

.field private static final OUT_MAX:I = 0xd

.field private static final OUT_SOLAR_ALTITUDE:I = 0x7

.field private static final OUT_SOLAR_AZIMUTH:I = 0x8

.field private static final OUT_SOLAR_DECLINATION:I = 0xb

.field private static final OUT_SOLAR_DISTANCE:I = 0x9

.field private static final OUT_SOLAR_RA:I = 0xa

.field private static final OUT_TIMEZONE:I = 0x6

.field private static final OUT_UVA:I = 0x2

.field private static final OUT_UVB:I = 0x3

.field private static final OUT_UVINDEX:I = 0x1

.field private static final OUT_UVINTENSITY:I = 0x4

.field private static final OUT_UVRAW:I = 0x0

.field private static final PLUS_ONE_SEC:I = 0x0

.field private static final PLUS_THREE_SEC:I = 0x1

.field private static final QUA_TEMP_SYSFS:Ljava/lang/String; = "/sys/devices/platform/sec-thermistor/temperature"

.field private static final SHEALTH:I = 0x2

.field static final TAG:Ljava/lang/String; = "UvSLocation"

.field private static final TEN_DEGREE:I = 0x1

.field private static final TIME_COUNTER:I = 0x4

.field private static final TOTAL_FIVE_SEC:I = 0x3

.field private static final TOTAL_THREE_SEC:I = 0x2

.field private static final TWENTY_DEGREE:I = 0x3

.field private static final UX_TEST:I = 0x1

.field static final VER:Ljava/lang/String; = "v2.2(1.7C)"

.field private static mIsJNILoaded:Z


# instance fields
.field ANGLE_BETWEEN_THRESHOLD:[F

.field public GameRotationEventListener:Landroid/hardware/SensorEventListener;

.field public LightSensorEventListener:Landroid/hardware/SensorEventListener;

.field public PressureSensorEventListener:Landroid/hardware/SensorEventListener;

.field TIME_BETWEEN_THRESHOLD:[F

.field public UVSensorEventListener:Landroid/hardware/SensorEventListener;

.field private algorithm_data_in:[F

.field private algorithm_data_out:[F

.field private mBSPType:I

.field private mContext:Landroid/content/Context;

.field private mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

.field private mCurPressureVal:F

.field private mCurUVTimeStamp:J

.field private mDiffAngle:[F

.field private mDiffAngleEngine:Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;

.field private mFactoryMode:I

.field private mGameRotationSensor:Landroid/hardware/Sensor;

.field private mInputVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;",
            ">;"
        }
    .end annotation
.end field

.field private mIntervalTime:F

.field private mIsAngleCondition:[Z

.field private mIsFirstUVData:Z

.field private mIsIndoor:Z

.field private mIsLocationFixed:Z

.field private mIsResultLog:Z

.field private mIsSHealthSendEvent:Z

.field private mMaxMeasureTime:I

.field private mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

.field private mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

.field private mMinMeasureTime:I

.field private mNewScenarioData:[[F

.field private mNewScenarioTime:[[F

.field private mPreUVTimeStamp:J

.field private mPressureSensor:Landroid/hardware/Sensor;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field public mSlocationListener:Lcom/samsung/location/SCurrentLocListener;

.field private mSlocationManager:Lcom/samsung/location/SLocationManager;

.field private mStartUVTimeStamp:J

.field private mTestMode:I

.field private mTimeAfterAngleFixed:[I

.field private mUVSampleCount:I

.field private mUVSensor:Landroid/hardware/Sensor;

.field private mUVSensorType:I

.field private mUsingLocationType:I

.field private requestId:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 31
    sput-boolean v2, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z

    .line 858
    :try_start_0
    const-string v1, "MxmUVSensor-jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 859
    const/4 v1, 0x1

    sput-boolean v1, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 864
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 860
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 861
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    sput-boolean v2, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z

    .line 862
    const-string v1, "UvSLocation"

    const-string v2, "JNI File Not Found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "sensor_type"    # I

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mContext:Landroid/content/Context;

    .line 24
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 25
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSensor:Landroid/hardware/Sensor;

    .line 26
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPressureSensor:Landroid/hardware/Sensor;

    .line 27
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mGameRotationSensor:Landroid/hardware/Sensor;

    .line 28
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    .line 29
    iput v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSensorType:I

    .line 32
    iput v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mFactoryMode:I

    .line 35
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationManager:Lcom/samsung/location/SLocationManager;

    .line 73
    const/16 v0, 0xe

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    .line 75
    const/16 v0, 0xd

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    .line 76
    iput-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsIndoor:Z

    .line 78
    const/4 v0, 0x5

    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUsingLocationType:I

    .line 79
    iput-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    .line 82
    iput v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurPressureVal:F

    .line 83
    iput v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIntervalTime:F

    .line 85
    iput v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMinMeasureTime:I

    .line 86
    iput v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaxMeasureTime:I

    .line 88
    iput-wide v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    .line 89
    iput-wide v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    .line 90
    iput-wide v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPreUVTimeStamp:J

    .line 91
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    .line 92
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    .line 94
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->requestId:I

    .line 96
    iput-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsFirstUVData:Z

    .line 101
    const/4 v0, 0x2

    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    .line 103
    iput-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsSHealthSendEvent:Z

    .line 105
    iput v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSampleCount:I

    .line 109
    const/4 v0, 0x2

    iput v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    .line 114
    iput-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngleEngine:Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;

    .line 115
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngle:[F

    .line 116
    new-array v0, v3, [Z

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    .line 117
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTimeAfterAngleFixed:[I

    .line 132
    filled-new-array {v3, v3}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    .line 133
    filled-new-array {v3, v3}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    .line 134
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->ANGLE_BETWEEN_THRESHOLD:[F

    .line 135
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    .line 136
    iput-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsResultLog:Z

    .line 686
    new-instance v0, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;

    invoke-direct {v0, p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor$1;-><init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->UVSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 759
    new-instance v0, Lcom/maximintegrated/bio/uv/MaximUVSensor$2;

    invoke-direct {v0, p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor$2;-><init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->PressureSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 777
    new-instance v0, Lcom/maximintegrated/bio/uv/MaximUVSensor$3;

    invoke-direct {v0, p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor$3;-><init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->LightSensorEventListener:Landroid/hardware/SensorEventListener;

    .line 792
    new-instance v0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;

    invoke-direct {v0, p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;-><init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->GameRotationEventListener:Landroid/hardware/SensorEventListener;

    .line 827
    new-instance v0, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;

    invoke-direct {v0, p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor$5;-><init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationListener:Lcom/samsung/location/SCurrentLocListener;

    .line 140
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mContext:Landroid/content/Context;

    .line 141
    iput p2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSensorType:I

    .line 143
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->init()V

    .line 144
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->getBSPType()V

    .line 145
    return-void
.end method

.method private MakeTempLogFile()V
    .locals 19

    .prologue
    .line 342
    new-instance v7, Ljava/lang/String;

    const-string v14, "/mnt/sdcard/MaximUVTestLog/"

    invoke-direct {v7, v14}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 343
    .local v7, "strFolder":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 344
    .local v11, "tempDirectory":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 345
    invoke-virtual {v11}, Ljava/io/File;->mkdirs()Z

    .line 348
    :cond_0
    const/4 v4, 0x0

    .line 349
    .local v4, "nFileNumber":I
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 350
    .local v1, "dirFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 351
    .local v2, "fileList":[Ljava/io/File;
    array-length v15, v2

    const/4 v14, 0x0

    :goto_0
    if-lt v14, v15, :cond_1

    .line 361
    const-string v14, "%sTempLog_%02d.csv"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v7, v15, v16

    const/16 v16, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 363
    .local v8, "strPathName":Ljava/lang/String;
    new-instance v10, Lcom/maximintegrated/bio/uv/LogFileWrite;

    invoke-direct {v10}, Lcom/maximintegrated/bio/uv/LogFileWrite;-><init>()V

    .line 365
    .local v10, "tLoggingData":Lcom/maximintegrated/bio/uv/LogFileWrite;
    invoke-virtual {v10, v8}, Lcom/maximintegrated/bio/uv/LogFileWrite;->CreateLogFile(Ljava/lang/String;)Z

    .line 367
    const-string v14, "DiffAngle,Plus 1 Sec,Plus 3 Sec,Total 3 Sec,Total 5 Sec,Plus 1 MaxTime,Plus 3 MaxTime, Total 3 MaxTime, Total 5 MaxTime"

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Object;

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 368
    .local v9, "strPrototype":Ljava/lang/String;
    invoke-virtual {v10, v9}, Lcom/maximintegrated/bio/uv/LogFileWrite;->FileWrite(Ljava/lang/String;)Z

    .line 370
    const/4 v6, 0x0

    .line 371
    .local v6, "strData":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    const/4 v14, 0x4

    if-lt v3, v14, :cond_3

    .line 383
    invoke-virtual {v10}, Lcom/maximintegrated/bio/uv/LogFileWrite;->CloseLogFile()Z

    .line 384
    return-void

    .line 351
    .end local v3    # "i":I
    .end local v6    # "strData":Ljava/lang/String;
    .end local v8    # "strPathName":Ljava/lang/String;
    .end local v9    # "strPrototype":Ljava/lang/String;
    .end local v10    # "tLoggingData":Lcom/maximintegrated/bio/uv/LogFileWrite;
    :cond_1
    aget-object v12, v2, v14

    .line 352
    .local v12, "tempFile":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->isFile()Z

    move-result v16

    if-eqz v16, :cond_2

    .line 353
    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    .line 354
    .local v13, "tempFileName":Ljava/lang/String;
    const-string v16, "UXScenarioTest"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 355
    const-string v16, "."

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v16

    add-int/lit8 v5, v16, -0x2

    .line 356
    .local v5, "nStartIndex":I
    add-int/lit8 v16, v5, 0x2

    move/from16 v0, v16

    invoke-virtual {v13, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    move/from16 v0, v16

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 351
    .end local v5    # "nStartIndex":I
    .end local v13    # "tempFileName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 372
    .end local v12    # "tempFile":Ljava/io/File;
    .restart local v3    # "i":I
    .restart local v6    # "strData":Ljava/lang/String;
    .restart local v8    # "strPathName":Ljava/lang/String;
    .restart local v9    # "strPrototype":Ljava/lang/String;
    .restart local v10    # "tLoggingData":Lcom/maximintegrated/bio/uv/LogFileWrite;
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    aget-boolean v14, v14, v3

    if-eqz v14, :cond_4

    .line 373
    const-string v14, "%d,%.4f,%.4f,%.4f,%.4f,%.3f,%.3f,%.3f,%.3f"

    const/16 v15, 0x9

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit8 v17, v3, 0x1

    mul-int/lit8 v17, v17, 0x5

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x0

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x1

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x2

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x3

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x5

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x0

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x1

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x2

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    move-object/from16 v17, v0

    aget-object v17, v17, v3

    const/16 v18, 0x3

    aget v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    .line 373
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 381
    :goto_2
    invoke-virtual {v10, v6}, Lcom/maximintegrated/bio/uv/LogFileWrite;->FileWrite(Ljava/lang/String;)Z

    .line 371
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 377
    :cond_4
    const-string v14, "%d,%.4f,%.4f,%.4f,%.4f,%.3f,%.3f,%.3f,%.3f"

    const/16 v15, 0x9

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    add-int/lit8 v17, v3, 0x1

    mul-int/lit8 v17, v17, 0x5

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    .line 378
    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x5

    .line 379
    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x6

    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x7

    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x8

    const/high16 v17, -0x40800000    # -1.0f

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    .line 377
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method

.method static synthetic access$0(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    return-object v0
.end method

.method static synthetic access$1(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSampleCount:I

    return v0
.end method

.method static synthetic access$10(Lcom/maximintegrated/bio/uv/MaximUVSensor;J)V
    .locals 0

    .prologue
    .line 89
    iput-wide p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    return-void
.end method

.method static synthetic access$11(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F
    .locals 1

    .prologue
    .line 533
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->getIntervalTime()F

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/maximintegrated/bio/uv/MaximUVSensor;F)V
    .locals 0

    .prologue
    .line 83
    iput p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIntervalTime:F

    return-void
.end method

.method static synthetic access$13(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    return-object v0
.end method

.method static synthetic access$14(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIntervalTime:F

    return v0
.end method

.method static synthetic access$15(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurPressureVal:F

    return v0
.end method

.method static synthetic access$16(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Ljava/util/Vector;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$17(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    return v0
.end method

.method static synthetic access$18(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V
    .locals 0

    .prologue
    .line 588
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->onlyUsingJIGTestLib()V

    return-void
.end method

.method static synthetic access$19(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V
    .locals 0

    .prologue
    .line 623
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->onlyUsingUXTestLib()V

    return-void
.end method

.method static synthetic access$2(Lcom/maximintegrated/bio/uv/MaximUVSensor;I)V
    .locals 0

    .prologue
    .line 105
    iput p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSampleCount:I

    return-void
.end method

.method static synthetic access$20(Lcom/maximintegrated/bio/uv/MaximUVSensor;I)V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0, p1}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->onlyUsingSHealthLib(I)V

    return-void
.end method

.method static synthetic access$21(Lcom/maximintegrated/bio/uv/MaximUVSensor;)J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    return-wide v0
.end method

.method static synthetic access$22(Lcom/maximintegrated/bio/uv/MaximUVSensor;J)V
    .locals 0

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPreUVTimeStamp:J

    return-void
.end method

.method static synthetic access$23(Lcom/maximintegrated/bio/uv/MaximUVSensor;Z)V
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsFirstUVData:Z

    return-void
.end method

.method static synthetic access$24(Lcom/maximintegrated/bio/uv/MaximUVSensor;F)V
    .locals 0

    .prologue
    .line 82
    iput p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurPressureVal:F

    return-void
.end method

.method static synthetic access$25(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    return-object v0
.end method

.method static synthetic access$26(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngleEngine:Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;

    return-object v0
.end method

.method static synthetic access$27(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    return-object v0
.end method

.method static synthetic access$28(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngle:[F

    return-object v0
.end method

.method static synthetic access$29(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTimeAfterAngleFixed:[I

    return-object v0
.end method

.method static synthetic access$3(Lcom/maximintegrated/bio/uv/MaximUVSensor;)F
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->getExtTemperature()F

    move-result v0

    return v0
.end method

.method static synthetic access$30(Lcom/maximintegrated/bio/uv/MaximUVSensor;)I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUsingLocationType:I

    return v0
.end method

.method static synthetic access$31(Lcom/maximintegrated/bio/uv/MaximUVSensor;Z)V
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    return-void
.end method

.method static synthetic access$4(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsIndoor:Z

    return v0
.end method

.method static synthetic access$5(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    return-object v0
.end method

.method static synthetic access$6(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    return v0
.end method

.method static synthetic access$7(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V
    .locals 0

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->filled_CurInputDataFromAlgorithmData()V

    return-void
.end method

.method static synthetic access$8(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsFirstUVData:Z

    return v0
.end method

.method static synthetic access$9(Lcom/maximintegrated/bio/uv/MaximUVSensor;J)V
    .locals 0

    .prologue
    .line 88
    iput-wide p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    return-void
.end method

.method private filled_AlgorithmDataFromInputVector(I)V
    .locals 4
    .param p1, "nIndex"    # I

    .prologue
    .line 443
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 444
    .local v0, "curObj":Ljava/lang/Object;
    instance-of v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    if-eqz v1, :cond_0

    .line 445
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v3, 0x0

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    int-to-float v1, v1

    aput v1, v2, v3

    .line 446
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v3, 0x4

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTemperature:F

    aput v1, v2, v3

    .line 447
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v3, 0x5

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mHR:F

    aput v1, v2, v3

    .line 448
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v3, 0x6

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mGest:F

    aput v1, v2, v3

    .line 449
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v3, 0x7

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLightLux:F

    aput v1, v2, v3

    .line 450
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v3, 0x8

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsIndoor:I

    int-to-float v1, v1

    aput v1, v2, v3

    .line 451
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v3, 0x9

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsLogging:I

    int-to-float v1, v1

    aput v1, v2, v3

    .line 453
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v3, 0xc

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mDate:F

    aput v1, v2, v3

    .line 454
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v3, 0xd

    move-object v1, v0

    check-cast v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v1, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTime:F

    aput v1, v2, v3

    .line 455
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v2, 0xb

    check-cast v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    .end local v0    # "curObj":Ljava/lang/Object;
    iget v3, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mExtTemper:F

    aput v3, v1, v2

    .line 457
    :cond_0
    return-void
.end method

.method private filled_CurInputDataFromAlgorithmData()V
    .locals 3

    .prologue
    .line 517
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-int v1, v1

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    .line 518
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLatitude:F

    .line 519
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLongitude:F

    .line 520
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mAltitude:F

    .line 521
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTemperature:F

    .line 522
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x5

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mHR:F

    .line 523
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mGest:F

    .line 524
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v2, 0x7

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mLightLux:F

    .line 525
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v2, 0x8

    aget v1, v1, v2

    float-to-int v1, v1

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsIndoor:I

    .line 526
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v2, 0x9

    aget v1, v1, v2

    float-to-int v1, v1

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mIsLogging:I

    .line 528
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v2, 0xc

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mDate:F

    .line 529
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v2, 0xd

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mTime:F

    .line 530
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v2, 0xb

    aget v1, v1, v2

    iput v1, v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mExtTemper:F

    .line 531
    return-void
.end method

.method private finalize_Variable()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 424
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    .line 425
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 428
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 431
    return-void

    .line 426
    :cond_0
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    aput v2, v1, v0

    .line 425
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    aput v2, v1, v0

    .line 428
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getBSPType()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 148
    const-string v2, "/sys/devices/platform/sec-thermistor/temperature"

    .line 150
    .local v2, "strTemperSysfs":Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 151
    .local v0, "br":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    iput v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    .line 152
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    .end local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    iget v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    if-ne v3, v4, :cond_0

    .line 157
    const-string v2, "/sys/class/sec/sec-thermistor/temperature"

    .line 159
    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/FileReader;

    invoke-direct {v3, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 160
    .restart local v0    # "br":Ljava/io/BufferedReader;
    const/4 v3, 0x1

    iput v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    .line 161
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 166
    .end local v0    # "br":Ljava/io/BufferedReader;
    :cond_0
    :goto_1
    return-void

    .line 153
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Ljava/lang/Exception;
    iput v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    goto :goto_0

    .line 162
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 163
    .restart local v1    # "e":Ljava/lang/Exception;
    iput v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    goto :goto_1
.end method

.method private getExtTemperature()F
    .locals 7

    .prologue
    .line 169
    const/4 v2, 0x0

    .line 172
    .local v2, "fExtTemperature":F
    iget v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    if-nez v5, :cond_0

    .line 173
    const-string v3, "/sys/devices/platform/sec-thermistor/temperature"

    .line 182
    .local v3, "strTemperSysfs":Ljava/lang/String;
    :goto_0
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 183
    .local v0, "br":Ljava/io/BufferedReader;
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 184
    .local v4, "temp":I
    int-to-float v5, v4

    const/high16 v6, 0x41200000    # 10.0f

    div-float v2, v5, v6

    .line 185
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v4    # "temp":I
    :goto_1
    move v5, v2

    .line 191
    .end local v3    # "strTemperSysfs":Ljava/lang/String;
    :goto_2
    return v5

    .line 175
    :cond_0
    iget v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mBSPType:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 176
    const-string v3, "/sys/class/sec/sec-thermistor/temperature"

    .line 177
    .restart local v3    # "strTemperSysfs":Ljava/lang/String;
    goto :goto_0

    .line 178
    .end local v3    # "strTemperSysfs":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    .line 186
    .restart local v3    # "strTemperSysfs":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 187
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    .line 188
    const-string v5, "UvSLocation"

    const-string v6, "Ext Temperature read error"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getIntervalTime()F
    .locals 5

    .prologue
    .line 534
    const/4 v0, 0x0

    .line 535
    .local v0, "fIntervalTime":F
    iget-wide v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPreUVTimeStamp:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 536
    iget-wide v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPreUVTimeStamp:J

    sub-long/2addr v1, v3

    long-to-float v1, v1

    const v2, 0x4e6e6b28    # 1.0E9f

    div-float v0, v1, v2

    .line 538
    :cond_0
    return v0
.end method

.method private getMaxUVADC()I
    .locals 4

    .prologue
    .line 460
    const/4 v1, 0x0

    .line 461
    .local v1, "nMaxADC":I
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v2

    .line 462
    .local v2, "nVecSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v2, :cond_0

    .line 467
    return v1

    .line 463
    :cond_0
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v3, v3, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    if-le v3, v1, :cond_1

    .line 464
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    iget v1, v3, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->mUV_ADC:I

    .line 462
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    .line 200
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSensorType:I

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSensor:Landroid/hardware/Sensor;

    .line 201
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPressureSensor:Landroid/hardware/Sensor;

    .line 202
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mGameRotationSensor:Landroid/hardware/Sensor;

    .line 203
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sec_location"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/location/SLocationManager;

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationManager:Lcom/samsung/location/SLocationManager;

    .line 205
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    if-nez v0, :cond_1

    .line 209
    new-instance v0, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    invoke-direct {v0}, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;-><init>()V

    iput-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    .line 212
    :cond_1
    const-string v0, "UvSLocation"

    const-string v1, "UV library is initialized(v2.2(1.7C))"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    return-void
.end method

.method private isLocationFixed()Z
    .locals 1

    .prologue
    .line 489
    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    return v0
.end method

.method private isRunAlgorithm()Z
    .locals 1

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isTimeCondition()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isLocationFixed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 501
    const/4 v0, 0x1

    .line 503
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRunUVADCLow()Z
    .locals 1

    .prologue
    .line 493
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isTimeCondition()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isUVADCLow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 494
    const/4 v0, 0x1

    .line 496
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTimeCondition()Z
    .locals 5

    .prologue
    .line 480
    iget-wide v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xf4240

    div-long/2addr v1, v3

    long-to-int v0, v1

    .line 481
    .local v0, "nLoggingTime":I
    iget v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMinMeasureTime:I

    if-le v0, v1, :cond_0

    iget v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaxMeasureTime:I

    if-ge v0, v1, :cond_0

    .line 482
    const-string v1, "UvSLocation"

    const-string v2, "Success UV measurement time."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    const/4 v1, 0x1

    .line 485
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isTimeLimit()Z
    .locals 5

    .prologue
    .line 507
    iget-wide v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xf4240

    div-long/2addr v1, v3

    long-to-int v0, v1

    .line 508
    .local v0, "nLoggingTime":I
    iget v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaxMeasureTime:I

    if-le v0, v1, :cond_0

    .line 509
    iget-boolean v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    if-nez v1, :cond_0

    .line 510
    const/4 v1, 0x1

    .line 513
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isUVADCLow()Z
    .locals 3

    .prologue
    .line 471
    const/16 v0, 0x12c

    .line 472
    .local v0, "UV_LOW_THRESHOLD":I
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->getMaxUVADC()I

    move-result v1

    .line 473
    .local v1, "nMaxADC":I
    if-ge v1, v0, :cond_0

    .line 474
    const/4 v2, 0x1

    .line 476
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onlyUsingJIGTestLib()V
    .locals 0

    .prologue
    .line 590
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->updateEveySampleEvent()V

    .line 591
    return-void
.end method

.method private onlyUsingSHealthLib(I)V
    .locals 3
    .param p1, "nAngleIndex"    # I

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 664
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_1

    .line 665
    sget-boolean v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsSHealthSendEvent:Z

    if-nez v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTimeAfterAngleFixed:[I

    aget v0, v0, p1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 667
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->updateMaxValueEvent()V

    .line 684
    :cond_0
    :goto_0
    return-void

    .line 671
    :cond_1
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isRunUVADCLow()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsSHealthSendEvent:Z

    if-nez v0, :cond_2

    .line 672
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->updateLOWUVEvent()V

    goto :goto_0

    .line 673
    :cond_2
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isRunAlgorithm()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsSHealthSendEvent:Z

    if-nez v0, :cond_3

    .line 674
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->updateMaxValueEvent()V

    goto :goto_0

    .line 675
    :cond_3
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->isTimeLimit()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iput v1, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    .line 677
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iput v1, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxIndex:F

    .line 678
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-boolean v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    iput-boolean v1, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedDebugLocation:Z

    .line 679
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mTimeStamp:J

    .line 680
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-interface {v0, v1}, Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;->onMaximUVSensorChanged(Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;)V

    .line 681
    const-string v0, "UvSLocation"

    const-string/jumbo v1, "onMaximUVSensorChanged!! - timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private onlyUsingUXTestLib()V
    .locals 0

    .prologue
    .line 625
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->updateEveySampleEvent()V

    .line 626
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->usingNewScenarioCheckData()V

    .line 627
    return-void
.end method

.method private native startAlgorithm(I)V
.end method

.method private native stopAlgorithm()V
.end method

.method private updateEveySampleEvent()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 557
    iget-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    if-eqz v2, :cond_2

    .line 558
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 576
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-boolean v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    iput-boolean v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedDebugLocation:Z

    .line 577
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    aget v3, v3, v7

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    .line 578
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mSolarAltitude:F

    .line 579
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/16 v4, 0x8

    aget v3, v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mSolarAzimuth:F

    .line 580
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-interface {v2, v3}, Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;->onMaximUVSensorChanged(Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;)V

    .line 581
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->clear()V

    .line 586
    .end local v1    # "i":I
    :goto_1
    return-void

    .line 559
    .restart local v1    # "i":I
    :cond_0
    invoke-direct {p0, v1}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->filled_AlgorithmDataFromInputVector(I)V

    .line 561
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v3, 0xa

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 562
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    invoke-virtual {p0, v2, v3}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->runAlgorithm([F[F)V

    .line 564
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->getIntervalTime()F

    move-result v2

    iput v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIntervalTime:F

    .line 568
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v2, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxIndex:F

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    aget v3, v3, v7

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 569
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 570
    .local v0, "c":Ljava/util/Calendar;
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x2710

    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    add-int/2addr v3, v4

    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/16 v4, 0xe

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    int-to-float v4, v4

    const v5, 0x3a83126f    # 0.001f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxSysTime:F

    .line 571
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-wide v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v3, v5

    long-to-float v3, v3

    const v4, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxTestTime:F

    .line 572
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    aget v3, v3, v7

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxIndex:F

    .line 558
    .end local v0    # "c":Ljava/util/Calendar;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 583
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-boolean v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    iput-boolean v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedDebugLocation:Z

    .line 584
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-interface {v2, v3}, Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;->onMaximUVSensorChanged(Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;)V

    goto :goto_1
.end method

.method private updateLOWUVEvent()V
    .locals 7

    .prologue
    .line 653
    const/16 v0, 0x12c

    .line 654
    .local v0, "UV_LOW_THRESHOLD":I
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->getMaxUVADC()I

    move-result v1

    .line 655
    .local v1, "nMaxUVADC":I
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-boolean v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    iput-boolean v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedDebugLocation:Z

    .line 656
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    const-wide/high16 v3, 0x3fe0000000000000L    # 0.5

    int-to-double v5, v0

    div-double/2addr v3, v5

    int-to-double v5, v1

    mul-double/2addr v3, v5

    double-to-float v3, v3

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxIndex:F

    .line 657
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mTimeStamp:J

    .line 658
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-interface {v2, v3}, Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;->onMaximUVSensorChanged(Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;)V

    .line 659
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsSHealthSendEvent:Z

    .line 660
    const-string v2, "UvSLocation"

    const-string/jumbo v3, "onMaximUVSensorChanged!!-LowCondition"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    return-void
.end method

.method private updateMaxValueEvent()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 630
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 643
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    aget v3, v3, v7

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    .line 644
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-boolean v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    iput-boolean v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mIsFixedDebugLocation:Z

    .line 645
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/4 v4, 0x7

    aget v3, v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mSolarAltitude:F

    .line 646
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    const/16 v4, 0x8

    aget v3, v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mSolarAzimuth:F

    .line 647
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-interface {v2, v3}, Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;->onMaximUVSensorChanged(Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;)V

    .line 648
    iput-boolean v7, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsSHealthSendEvent:Z

    .line 649
    const-string v2, "UvSLocation"

    const-string/jumbo v3, "onMaximUVSensorChanged!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    return-void

    .line 631
    :cond_0
    invoke-direct {p0, v1}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->filled_AlgorithmDataFromInputVector(I)V

    .line 632
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v3, 0xa

    const/4 v4, 0x0

    aput v4, v2, v3

    .line 633
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    invoke-virtual {p0, v2, v3}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->runAlgorithm([F[F)V

    .line 635
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v2, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxIndex:F

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    aget v3, v3, v7

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_1

    .line 636
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 637
    .local v0, "c":Ljava/util/Calendar;
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    mul-int/lit16 v3, v3, 0x2710

    const/16 v4, 0xc

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x64

    add-int/2addr v3, v4

    const/16 v4, 0xd

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/16 v4, 0xe

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    int-to-float v4, v4

    const v5, 0x3a83126f    # 0.001f

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxSysTime:F

    .line 638
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-wide v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v3, v5

    long-to-float v3, v3

    const v4, 0x4e6e6b28    # 1.0E9f

    div-float/2addr v3, v4

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxTestTime:F

    .line 639
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F

    aget v3, v3, v7

    iput v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVMaxIndex:F

    .line 640
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mTimeStamp:J

    .line 630
    .end local v0    # "c":Ljava/util/Calendar;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method private usingNewScenarioCheckData()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const v9, 0x4e6e6b28    # 1.0E9f

    const/4 v8, 0x2

    .line 594
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    array-length v3, v3

    if-lt v0, v3, :cond_0

    .line 605
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    array-length v3, v3

    if-lt v0, v3, :cond_3

    .line 615
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    array-length v3, v3

    if-lt v0, v3, :cond_5

    .line 621
    return-void

    .line 595
    :cond_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_3
    if-lt v1, v8, :cond_1

    .line 594
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 596
    :cond_1
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTimeAfterAngleFixed:[I

    aget v3, v3, v0

    int-to-float v3, v3

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    aget v4, v4, v1

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 597
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v3, v3, v0

    aget v3, v3, v1

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v4, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 598
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v4, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    aput v4, v3, v1

    .line 599
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    aget-object v3, v3, v0

    iget-wide v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v4, v6

    long-to-float v4, v4

    div-float/2addr v4, v9

    aput v4, v3, v1

    .line 595
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 606
    .end local v1    # "j":I
    :cond_3
    iget-wide v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v5, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v3, v5

    const-wide/32 v5, 0xf4240

    div-long/2addr v3, v5

    long-to-int v2, v3

    .line 607
    .local v2, "nLoggingTime":I
    int-to-float v3, v2

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    aget v4, v4, v8

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 608
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v3, v3, v0

    aget v3, v3, v8

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v4, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 609
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v4, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    aput v4, v3, v8

    .line 610
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    aget-object v3, v3, v0

    iget-wide v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v4, v6

    long-to-float v4, v4

    div-float/2addr v4, v9

    aput v4, v3, v8

    .line 605
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 616
    .end local v2    # "nLoggingTime":I
    :cond_5
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v3, v3, v0

    aget v3, v3, v10

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v4, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    .line 617
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    iget v4, v4, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;->mUVIndex:F

    aput v4, v3, v10

    .line 618
    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioTime:[[F

    aget-object v3, v3, v0

    iget-wide v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    iget-wide v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    sub-long/2addr v4, v6

    long-to-float v4, v4

    div-float/2addr v4, v9

    aput v4, v3, v10

    .line 615
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2
.end method


# virtual methods
.method public checkPassiveLocation()Z
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationManager:Lcom/samsung/location/SLocationManager;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationManager:Lcom/samsung/location/SLocationManager;

    invoke-virtual {v0}, Lcom/samsung/location/SLocationManager;->checkPassiveLocation()Z

    move-result v0

    .line 438
    :goto_0
    return v0

    .line 437
    :cond_0
    const-string v0, "UvSLocation"

    const-string v1, "SLocation is not Supported"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFactoryMode()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mFactoryMode:I

    return v0
.end method

.method public getSensor()Landroid/hardware/Sensor;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSensor:Landroid/hardware/Sensor;

    return-object v0
.end method

.method public registerListener(Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;II)Z
    .locals 12
    .param p1, "UVSensorListener"    # Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;
    .param p2, "nMinimumTime"    # I
    .param p3, "nTimeLimit"    # I

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 259
    iput-boolean v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsSHealthSendEvent:Z

    .line 260
    iput v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSampleCount:I

    .line 261
    const-string v6, "MaximUV register lib[%d,%d]"

    new-array v7, v10, [Ljava/lang/Object;

    iget v8, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    iget v8, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUsingLocationType:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 262
    .local v3, "strDebug":Ljava/lang/String;
    const-string v6, "UvSLocation"

    invoke-static {v6, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iput p2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMinMeasureTime:I

    iput p3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaxMeasureTime:I

    .line 264
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvnetListener:Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .line 265
    new-instance v6, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    invoke-direct {v6}, Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;-><init>()V

    iput-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mMaximUVEvent:Lcom/maximintegrated/bio/uv/MaximUVSensorEvent;

    .line 266
    iput-boolean v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsFirstUVData:Z

    .line 267
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    array-length v6, v6

    if-lt v0, v6, :cond_1

    .line 273
    iput-boolean v4, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsResultLog:Z

    .line 274
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    const/high16 v7, 0x447a0000    # 1000.0f

    aput v7, v6, v4

    .line 275
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    const v7, 0x453b8000    # 3000.0f

    aput v7, v6, v5

    .line 276
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    const v7, 0x453b8000    # 3000.0f

    aput v7, v6, v10

    .line 277
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->TIME_BETWEEN_THRESHOLD:[F

    const v7, 0x459c4000    # 5000.0f

    aput v7, v6, v11

    .line 279
    iget v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    if-ne v6, v5, :cond_0

    .line 280
    const/4 v0, 0x0

    :goto_1
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    array-length v6, v6

    if-lt v0, v6, :cond_2

    .line 287
    :cond_0
    sget-boolean v6, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z

    if-eqz v6, :cond_4

    .line 288
    iget v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mFactoryMode:I

    invoke-direct {p0, v6}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->startAlgorithm(I)V

    .line 293
    :goto_2
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v7, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->UVSensorEventListener:Landroid/hardware/SensorEventListener;

    iget-object v8, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUVSensor:Landroid/hardware/Sensor;

    invoke-virtual {v6, v7, v8, v11}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v6

    if-nez v6, :cond_5

    .line 294
    const-string v5, "UvSLocation"

    const-string v6, "UV Sensor Register Fail!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    :goto_3
    return v4

    .line 268
    :cond_1
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z

    aput-boolean v4, v6, v0

    .line 269
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTimeAfterAngleFixed:[I

    aput v4, v6, v0

    .line 270
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->ANGLE_BETWEEN_THRESHOLD:[F

    add-int/lit8 v7, v0, 0x1

    mul-int/lit8 v7, v7, 0x5

    int-to-float v7, v7

    aput v7, v6, v0

    .line 267
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    :cond_2
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_4
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v6, v6, v0

    array-length v6, v6

    if-lt v1, v6, :cond_3

    .line 280
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 282
    :cond_3
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mNewScenarioData:[[F

    aget-object v6, v6, v0

    aput v9, v6, v1

    .line 281
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 290
    .end local v1    # "j":I
    :cond_4
    const-string v6, "UvSLocation"

    const-string v7, "JNI not loaded calling startAlgorithm failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 297
    :cond_5
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v7, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->PressureSensorEventListener:Landroid/hardware/SensorEventListener;

    iget-object v8, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPressureSensor:Landroid/hardware/Sensor;

    invoke-virtual {v6, v7, v8, v11}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v6

    if-nez v6, :cond_6

    .line 298
    const-string v5, "UvSLocation"

    const-string v6, "Pressure Sensor Register Fail!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 302
    :cond_6
    iget v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    if-eq v6, v10, :cond_7

    iget v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    if-ne v6, v5, :cond_8

    .line 303
    :cond_7
    new-instance v6, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;

    invoke-direct {v6}, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;-><init>()V

    iput-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngleEngine:Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;

    .line 304
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v7, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->GameRotationEventListener:Landroid/hardware/SensorEventListener;

    iget-object v8, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mGameRotationSensor:Landroid/hardware/Sensor;

    invoke-virtual {v6, v7, v8, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 307
    :cond_8
    iput v9, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIntervalTime:F

    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mStartUVTimeStamp:J

    .line 308
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurUVTimeStamp:J

    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mPreUVTimeStamp:J

    .line 309
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    if-nez v6, :cond_9

    .line 310
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    iput-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    .line 315
    :goto_5
    iput v9, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurPressureVal:F

    .line 325
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    if-nez v6, :cond_a

    .line 326
    new-instance v6, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    invoke-direct {v6}, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;-><init>()V

    iput-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    .line 331
    :goto_6
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationManager:Lcom/samsung/location/SLocationManager;

    iget-object v7, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationListener:Lcom/samsung/location/SCurrentLocListener;

    invoke-virtual {v6, v7}, Lcom/samsung/location/SLocationManager;->requestCurrentLocation(Lcom/samsung/location/SCurrentLocListener;)I

    move-result v2

    .line 332
    .local v2, "ret":I
    if-gez v2, :cond_b

    .line 333
    const-string v5, "UvSLocation"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fail to register requestCurrentLocation : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 312
    .end local v2    # "ret":I
    :cond_9
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mInputVector:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->clear()V

    goto :goto_5

    .line 328
    :cond_a
    iget-object v6, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mCurInputData:Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;

    invoke-virtual {v6}, Lcom/maximintegrated/bio/uv/UVAlgorithmInputData;->InitValue()V

    goto :goto_6

    .line 336
    .restart local v2    # "ret":I
    :cond_b
    iput v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->requestId:I

    move v4, v5

    .line 338
    goto/16 :goto_3
.end method

.method public native runAlgorithm([F[F)V
.end method

.method public sendLocation(Landroid/location/Location;I)V
    .locals 4
    .param p1, "setLoc"    # Landroid/location/Location;
    .param p2, "nLocType"    # I

    .prologue
    const/4 v3, 0x1

    .line 248
    iput-boolean v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z

    .line 254
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    double-to-float v1, v1

    aput v1, v0, v3

    .line 255
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    double-to-float v2, v2

    aput v2, v0, v1

    .line 256
    return-void
.end method

.method public sendLocationType(I)V
    .locals 0
    .param p1, "nLocationType"    # I

    .prologue
    .line 244
    iput p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mUsingLocationType:I

    .line 245
    return-void
.end method

.method public setFactoryMode(I)V
    .locals 0
    .param p1, "nSetMode"    # I

    .prologue
    .line 224
    iput p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mFactoryMode:I

    .line 225
    return-void
.end method

.method public setLogging()V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_in:[F

    const/16 v1, 0x9

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 237
    return-void
.end method

.method public setManualIndoorMode(Z)V
    .locals 1
    .param p1, "manualValue"    # Z

    .prologue
    .line 228
    if-eqz p1, :cond_0

    .line 229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsIndoor:Z

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsIndoor:Z

    goto :goto_0
.end method

.method public setTestMode(I)V
    .locals 0
    .param p1, "nTestMode"    # I

    .prologue
    .line 240
    iput p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    .line 241
    return-void
.end method

.method public unregisterListener(Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;)V
    .locals 3
    .param p1, "uvListener"    # Lcom/maximintegrated/bio/uv/MaximUVSensorEventListener;

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 387
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    if-ne v0, v1, :cond_0

    .line 388
    iget-boolean v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsResultLog:Z

    if-nez v0, :cond_0

    .line 389
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->MakeTempLogFile()V

    .line 390
    iput-boolean v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsResultLog:Z

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_2

    .line 395
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->UVSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 396
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->PressureSensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 397
    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    if-eq v0, v2, :cond_1

    iget v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTestMode:I

    if-ne v0, v2, :cond_2

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->GameRotationEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 402
    :cond_2
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationManager:Lcom/samsung/location/SLocationManager;

    if-eqz v0, :cond_3

    .line 403
    iget-object v0, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationManager:Lcom/samsung/location/SLocationManager;

    iget v1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->requestId:I

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mSlocationListener:Lcom/samsung/location/SCurrentLocListener;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/location/SLocationManager;->removeCurrentLocation(ILcom/samsung/location/SCurrentLocListener;)I

    .line 415
    :cond_3
    sget-boolean v0, Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsJNILoaded:Z

    if-eqz v0, :cond_4

    .line 416
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->stopAlgorithm()V

    .line 420
    :goto_0
    invoke-direct {p0}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->finalize_Variable()V

    .line 421
    return-void

    .line 418
    :cond_4
    const-string v0, "UvSLocation"

    const-string v1, "JNI not loaded calling stopAlgorithm failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
