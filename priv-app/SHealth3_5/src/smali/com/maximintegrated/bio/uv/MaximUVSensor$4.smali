.class Lcom/maximintegrated/bio/uv/MaximUVSensor$4;
.super Ljava/lang/Object;
.source "MaximUVSensor.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/maximintegrated/bio/uv/MaximUVSensor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;


# direct methods
.method constructor <init>(Lcom/maximintegrated/bio/uv/MaximUVSensor;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    .line 792
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 822
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v7, 0x7

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 796
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$25(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[Z

    move-result-object v2

    array-length v2, v2

    if-lt v0, v2, :cond_0

    .line 817
    return-void

    .line 797
    :cond_0
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$25(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[Z

    move-result-object v2

    aget-boolean v2, v2, v0

    if-nez v2, :cond_3

    .line 798
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngleEngine:Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$26(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 799
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$27(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    aget v2, v2, v7

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsLocationFixed:Z
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$6(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 800
    const/4 v2, 0x2

    new-array v1, v2, [F

    .line 801
    .local v1, "tSunPos":[F
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$27(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    const/16 v3, 0x8

    aget v2, v2, v3

    aput v2, v1, v5

    .line 802
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->algorithm_data_out:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$27(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    aget v2, v2, v7

    aput v2, v1, v6

    .line 803
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngle:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$28(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngleEngine:Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;
    invoke-static {v3}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$26(Lcom/maximintegrated/bio/uv/MaximUVSensor;)Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;

    move-result-object v3

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    invoke-virtual {v3, v4, v1}, Lcom/maximintegrated/bio/uv/CalculaterDiffAngle;->GetDiffAngle([F[F)F

    move-result v3

    aput v3, v2, v0

    .line 804
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mDiffAngle:[F
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$28(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[F

    move-result-object v2

    aget v2, v2, v0

    iget-object v3, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    iget-object v3, v3, Lcom/maximintegrated/bio/uv/MaximUVSensor;->ANGLE_BETWEEN_THRESHOLD:[F

    aget v3, v3, v0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 805
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mIsAngleCondition:[Z
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$25(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[Z

    move-result-object v2

    aput-boolean v6, v2, v0

    .line 806
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTimeAfterAngleFixed:[I
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$29(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[I

    move-result-object v2

    aput v5, v2, v0

    .line 796
    .end local v1    # "tSunPos":[F
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 810
    :cond_2
    const-string v2, "UvSLocation"

    const-string v3, "Angle Calculater is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 813
    :cond_3
    iget-object v2, p0, Lcom/maximintegrated/bio/uv/MaximUVSensor$4;->this$0:Lcom/maximintegrated/bio/uv/MaximUVSensor;

    # getter for: Lcom/maximintegrated/bio/uv/MaximUVSensor;->mTimeAfterAngleFixed:[I
    invoke-static {v2}, Lcom/maximintegrated/bio/uv/MaximUVSensor;->access$29(Lcom/maximintegrated/bio/uv/MaximUVSensor;)[I

    move-result-object v2

    aget v3, v2, v0

    add-int/lit8 v3, v3, 0x14

    aput v3, v2, v0

    goto :goto_1
.end method
