.class public Lcom/maximintegrated/bio/uv/CalculateAngleConvetor;
.super Ljava/lang/Object;
.source "CalculateAngleConvetor.java"


# static fields
.field static final TAG:Ljava/lang/String; = "UvSLocation"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method public CalculateEulerFromQuaternionB([F[F)V
    .locals 29
    .param p1, "euler"    # [F
    .param p2, "q"    # [F

    .prologue
    .line 67
    const/16 v22, 0x9

    move/from16 v0, v22

    new-array v4, v0, [D

    .line 68
    .local v4, "rot":[D
    const/4 v5, 0x0

    .line 70
    .local v5, "tElevationAngle":F
    const/16 v22, 0x0

    aget v22, p2, v22

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v16, v0

    .local v16, "x":D
    const/16 v22, 0x1

    aget v22, p2, v22

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v18, v0

    .local v18, "y":D
    const/16 v22, 0x2

    aget v22, p2, v22

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v20, v0

    .local v20, "z":D
    const/16 v22, 0x3

    aget v22, p2, v22

    move/from16 v0, v22

    float-to-double v14, v0

    .line 72
    .local v14, "w":D
    const/16 v22, 0x0

    mul-double v23, v16, v16

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v14, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    add-double v23, v23, v25

    const-wide/high16 v25, 0x3ff0000000000000L    # 1.0

    sub-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 73
    const/16 v22, 0x1

    mul-double v23, v16, v18

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v20, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    sub-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 74
    const/16 v22, 0x2

    mul-double v23, v16, v20

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v18, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    add-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 75
    const/16 v22, 0x3

    mul-double v23, v16, v18

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v20, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    add-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 76
    const/16 v22, 0x4

    mul-double v23, v18, v18

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v14, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    add-double v23, v23, v25

    const-wide/high16 v25, 0x3ff0000000000000L    # 1.0

    sub-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 77
    const/16 v22, 0x5

    mul-double v23, v18, v20

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v16, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    sub-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 78
    const/16 v22, 0x6

    mul-double v23, v16, v20

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v18, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    sub-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 79
    const/16 v22, 0x7

    mul-double v23, v18, v20

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v16, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    add-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 80
    const/16 v22, 0x8

    mul-double v23, v20, v20

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    mul-double v23, v23, v25

    mul-double v25, v14, v14

    const-wide/high16 v27, 0x4000000000000000L    # 2.0

    mul-double v25, v25, v27

    add-double v23, v23, v25

    const-wide/high16 v25, 0x3ff0000000000000L    # 1.0

    sub-double v23, v23, v25

    aput-wide v23, v4, v22

    .line 82
    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    .line 83
    .local v2, "rad2deg":D
    const/16 v22, 0x2

    aget-wide v22, v4, v22

    move-wide/from16 v0, v22

    neg-double v8, v0

    .local v8, "tx":D
    const/16 v22, 0x5

    aget-wide v22, v4, v22

    move-wide/from16 v0, v22

    neg-double v10, v0

    .local v10, "ty":D
    const/16 v22, 0x8

    aget-wide v22, v4, v22

    move-wide/from16 v0, v22

    neg-double v12, v0

    .line 84
    .local v12, "tz":D
    mul-double v22, v8, v8

    mul-double v24, v10, v10

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    .line 85
    .local v6, "ts":D
    const-wide/16 v22, 0x0

    cmpl-double v22, v12, v22

    if-lez v22, :cond_0

    .line 86
    invoke-static {v6, v7}, Ljava/lang/Math;->acos(D)D

    move-result-wide v22

    mul-double v22, v22, v2

    move-wide/from16 v0, v22

    double-to-float v5, v0

    .line 91
    :goto_0
    const/16 v22, 0x2

    const/16 v23, 0x3

    aget-wide v23, v4, v23

    move-wide/from16 v0, v23

    neg-double v0, v0

    move-wide/from16 v23, v0

    const/16 v25, 0x0

    aget-wide v25, v4, v25

    invoke-static/range {v23 .. v26}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v23

    mul-double v23, v23, v2

    move-wide/from16 v0, v23

    double-to-float v0, v0

    move/from16 v23, v0

    aput v23, p1, v22

    .line 92
    const/16 v22, 0x1

    aput v5, p1, v22

    .line 93
    const/16 v22, 0x0

    const/16 v23, 0x6

    aget-wide v23, v4, v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Math;->asin(D)D

    move-result-wide v23

    mul-double v23, v23, v2

    move-wide/from16 v0, v23

    double-to-float v0, v0

    move/from16 v23, v0

    aput v23, p1, v22

    .line 94
    return-void

    .line 88
    :cond_0
    invoke-static {v6, v7}, Ljava/lang/Math;->acos(D)D

    move-result-wide v22

    mul-double v22, v22, v2

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    neg-float v5, v0

    goto :goto_0
.end method

.method public CalculaterDCMatrixFromQuaternion([[F[F)V
    .locals 16
    .param p1, "dcMat"    # [[F
    .param p2, "q"    # [F

    .prologue
    .line 11
    const/4 v8, 0x3

    aget v8, p2, v8

    float-to-double v0, v8

    .line 12
    .local v0, "a":D
    const/4 v8, 0x0

    aget v8, p2, v8

    float-to-double v2, v8

    .line 13
    .local v2, "b":D
    const/4 v8, 0x1

    aget v8, p2, v8

    float-to-double v4, v8

    .line 14
    .local v4, "c":D
    const/4 v8, 0x2

    aget v8, p2, v8

    float-to-double v6, v8

    .line 16
    .local v6, "d":D
    const/4 v8, 0x0

    aget-object v8, p1, v8

    const/4 v9, 0x0

    mul-double v10, v0, v0

    mul-double v12, v2, v2

    add-double/2addr v10, v12

    mul-double v12, v4, v4

    sub-double/2addr v10, v12

    mul-double v12, v6, v6

    sub-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 17
    const/4 v8, 0x0

    aget-object v8, p1, v8

    const/4 v9, 0x1

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double v12, v2, v4

    mul-double v14, v0, v6

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 18
    const/4 v8, 0x0

    aget-object v8, p1, v8

    const/4 v9, 0x2

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double v12, v2, v6

    mul-double v14, v0, v4

    add-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 20
    const/4 v8, 0x1

    aget-object v8, p1, v8

    const/4 v9, 0x0

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double v12, v2, v4

    mul-double v14, v0, v6

    add-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 21
    const/4 v8, 0x1

    aget-object v8, p1, v8

    const/4 v9, 0x1

    mul-double v10, v0, v0

    mul-double v12, v2, v2

    sub-double/2addr v10, v12

    mul-double v12, v4, v4

    add-double/2addr v10, v12

    mul-double v12, v6, v6

    sub-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 22
    const/4 v8, 0x1

    aget-object v8, p1, v8

    const/4 v9, 0x2

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double v12, v4, v6

    mul-double v14, v0, v2

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 24
    const/4 v8, 0x2

    aget-object v8, p1, v8

    const/4 v9, 0x0

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double v12, v2, v6

    mul-double v14, v0, v4

    sub-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 25
    const/4 v8, 0x2

    aget-object v8, p1, v8

    const/4 v9, 0x1

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double v12, v4, v6

    mul-double v14, v0, v2

    add-double/2addr v12, v14

    mul-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 26
    const/4 v8, 0x2

    aget-object v8, p1, v8

    const/4 v9, 0x2

    mul-double v10, v0, v0

    mul-double v12, v2, v2

    sub-double/2addr v10, v12

    mul-double v12, v4, v4

    sub-double/2addr v10, v12

    mul-double v12, v6, v6

    add-double/2addr v10, v12

    double-to-float v10, v10

    aput v10, v8, v9

    .line 27
    return-void
.end method

.method public CalculaterDot([F[F)F
    .locals 10
    .param p1, "q1"    # [F
    .param p2, "q2"    # [F

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 57
    aget v1, p1, v6

    aget v2, p2, v6

    mul-float/2addr v1, v2

    aget v2, p1, v7

    aget v3, p2, v7

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aget v2, p1, v8

    aget v3, p2, v8

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aget v2, p1, v9

    aget v3, p2, v9

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 58
    .local v0, "fDot":F
    float-to-double v1, v0

    aget v3, p1, v6

    aget v4, p1, v6

    mul-float/2addr v3, v4

    aget v4, p1, v7

    aget v5, p1, v7

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aget v4, p1, v8

    aget v5, p1, v8

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aget v4, p1, v9

    aget v5, p1, v9

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    div-double/2addr v1, v3

    double-to-float v0, v1

    .line 59
    float-to-double v1, v0

    aget v3, p2, v6

    aget v4, p2, v6

    mul-float/2addr v3, v4

    aget v4, p2, v7

    aget v5, p2, v7

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aget v4, p2, v8

    aget v5, p2, v8

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    aget v4, p2, v9

    aget v5, p2, v9

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    div-double/2addr v1, v3

    double-to-float v0, v1

    .line 60
    float-to-double v1, v0

    invoke-static {v1, v2}, Ljava/lang/Math;->acos(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    double-to-float v0, v1

    .line 61
    return v0
.end method

.method public CalculaterEulerFromDCMatrix([F[[F)V
    .locals 6
    .param p1, "euler"    # [F
    .param p2, "dcMat"    # [[F

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 30
    aget-object v1, p2, v5

    aget v1, v1, v3

    aget-object v2, p2, v5

    aget v2, v2, v5

    div-float/2addr v1, v2

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    move-result-wide v1

    double-to-float v1, v1

    aput v1, p1, v4

    .line 31
    aget-object v1, p2, v5

    aget v1, v1, v4

    neg-float v1, v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->asin(D)D

    move-result-wide v1

    double-to-float v1, v1

    aput v1, p1, v3

    .line 32
    aget-object v1, p2, v3

    aget v1, v1, v4

    float-to-double v1, v1

    aget-object v3, p2, v4

    aget v3, v3, v4

    float-to-double v3, v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v1

    double-to-float v1, v1

    aput v1, p1, v5

    .line 34
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 37
    return-void

    .line 35
    :cond_0
    aget v1, p1, v0

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v1

    double-to-float v1, v1

    aput v1, p1, v0

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public CalculaterQuaternionFromEuler([F[F)V
    .locals 17
    .param p1, "q"    # [F
    .param p2, "euler"    # [F

    .prologue
    .line 41
    const/4 v12, 0x2

    aget v12, p2, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 42
    .local v2, "cpsi2":D
    const/4 v12, 0x2

    aget v12, p2, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    .line 43
    .local v8, "spsi2":D
    const/4 v12, 0x1

    aget v12, p2, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 44
    .local v4, "cthe2":D
    const/4 v12, 0x1

    aget v12, p2, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 45
    .local v10, "sthe2":D
    const/4 v12, 0x0

    aget v12, p2, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 46
    .local v0, "cphi2":D
    const/4 v12, 0x0

    aget v12, p2, v12

    float-to-double v12, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    .line 49
    .local v6, "sphi2":D
    const/4 v12, 0x3

    mul-double v13, v0, v4

    mul-double/2addr v13, v2

    mul-double v15, v6, v10

    mul-double/2addr v15, v8

    add-double/2addr v13, v15

    double-to-float v13, v13

    aput v13, p1, v12

    .line 50
    const/4 v12, 0x0

    mul-double v13, v6, v4

    mul-double/2addr v13, v2

    mul-double v15, v0, v10

    mul-double/2addr v15, v8

    sub-double/2addr v13, v15

    double-to-float v13, v13

    aput v13, p1, v12

    .line 51
    const/4 v12, 0x1

    mul-double v13, v0, v10

    mul-double/2addr v13, v2

    mul-double v15, v6, v4

    mul-double/2addr v15, v8

    add-double/2addr v13, v15

    double-to-float v13, v13

    aput v13, p1, v12

    .line 52
    const/4 v12, 0x2

    mul-double v13, v0, v4

    mul-double/2addr v13, v8

    mul-double v15, v6, v10

    mul-double/2addr v15, v2

    add-double/2addr v13, v15

    double-to-float v13, v13

    aput v13, p1, v12

    .line 53
    return-void
.end method
