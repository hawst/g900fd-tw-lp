.class public Lcom/garmin/fit/SessionMesg;
.super Lcom/garmin/fit/Mesg;
.source "SessionMesg.java"

# interfaces
.implements Lcom/garmin/fit/MesgWithEvent;


# static fields
.field protected static final sessionMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    .line 28
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string/jumbo v1, "session"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    .line 29
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "message_index"

    const/16 v2, 0xfe

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "timestamp"

    const/16 v2, 0xfd

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "event"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "event_type"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "start_time"

    const/4 v2, 0x2

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 39
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "start_position_lat"

    const/4 v2, 0x3

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 41
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "start_position_long"

    const/4 v2, 0x4

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 43
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "sport"

    const/4 v2, 0x5

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 45
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "sub_sport"

    const/4 v2, 0x6

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 47
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_elapsed_time"

    const/4 v2, 0x7

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 49
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_timer_time"

    const/16 v2, 0x8

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 51
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_distance"

    const/16 v2, 0x9

    const/16 v3, 0x86

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 52
    const/16 v10, 0xc

    .line 53
    sget-object v11, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_cycles"

    const/16 v2, 0xa

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "cycles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 54
    const/4 v8, 0x0

    .line 55
    sget-object v0, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v9, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    new-instance v0, Lcom/garmin/fit/SubField;

    const-string/jumbo v1, "total_strides"

    const/16 v2, 0x86

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    const-string/jumbo v7, "strides"

    invoke-direct/range {v0 .. v7}, Lcom/garmin/fit/SubField;-><init>(Ljava/lang/String;IDDLjava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v0, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    const/4 v1, 0x5

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/garmin/fit/SubField;->addMap(IJ)V

    .line 59
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_calories"

    const/16 v2, 0xb

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "kcal"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 61
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_fat_calories"

    const/16 v2, 0xd

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "kcal"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 63
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_speed"

    const/16 v2, 0xe

    const/16 v3, 0x84

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 65
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_speed"

    const/16 v2, 0xf

    const/16 v3, 0x84

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 67
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_heart_rate"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 69
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_heart_rate"

    const/16 v2, 0x11

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 70
    const/16 v10, 0x13

    .line 71
    sget-object v11, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_cadence"

    const/16 v2, 0x12

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 72
    const/4 v8, 0x0

    .line 73
    sget-object v0, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v9, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    new-instance v0, Lcom/garmin/fit/SubField;

    const-string v1, "avg_running_cadence"

    const/4 v2, 0x2

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    const-string/jumbo v7, "strides/min"

    invoke-direct/range {v0 .. v7}, Lcom/garmin/fit/SubField;-><init>(Ljava/lang/String;IDDLjava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    const/4 v1, 0x5

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/garmin/fit/SubField;->addMap(IJ)V

    .line 76
    const/16 v10, 0x14

    .line 77
    sget-object v11, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_cadence"

    const/16 v2, 0x13

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 78
    const/4 v8, 0x0

    .line 79
    sget-object v0, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v9, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    new-instance v0, Lcom/garmin/fit/SubField;

    const-string/jumbo v1, "max_running_cadence"

    const/4 v2, 0x2

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    const-string/jumbo v7, "strides/min"

    invoke-direct/range {v0 .. v7}, Lcom/garmin/fit/SubField;-><init>(Ljava/lang/String;IDDLjava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    const/4 v1, 0x5

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/garmin/fit/SubField;->addMap(IJ)V

    .line 83
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_power"

    const/16 v2, 0x14

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "watts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 85
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_power"

    const/16 v2, 0x15

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "watts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 87
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_ascent"

    const/16 v2, 0x16

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 89
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_descent"

    const/16 v2, 0x17

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 91
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_training_effect"

    const/16 v2, 0x18

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 93
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "first_lap_index"

    const/16 v2, 0x19

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 95
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "num_laps"

    const/16 v2, 0x1a

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 97
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "event_group"

    const/16 v2, 0x1b

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 99
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "trigger"

    const/16 v2, 0x1c

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 101
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "nec_lat"

    const/16 v2, 0x1d

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 103
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "nec_long"

    const/16 v2, 0x1e

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 105
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "swc_lat"

    const/16 v2, 0x1f

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 107
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "swc_long"

    const/16 v2, 0x20

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 109
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "normalized_power"

    const/16 v2, 0x22

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "watts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 111
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "training_stress_score"

    const/16 v2, 0x23

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "tss"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 113
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "intensity_factor"

    const/16 v2, 0x24

    const/16 v3, 0x84

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "if"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 115
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "left_right_balance"

    const/16 v2, 0x25

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 117
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_stroke_count"

    const/16 v2, 0x29

    const/16 v3, 0x86

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "strokes/lap"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 119
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_stroke_distance"

    const/16 v2, 0x2a

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 121
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "swim_stroke"

    const/16 v2, 0x2b

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "swim_stroke"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 123
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "pool_length"

    const/16 v2, 0x2c

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 125
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "pool_length_unit"

    const/16 v2, 0x2e

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 127
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "num_active_lengths"

    const/16 v2, 0x2f

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "lengths"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 129
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_work"

    const/16 v2, 0x30

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "J"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 131
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_altitude"

    const/16 v2, 0x31

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    const-wide v6, 0x407f400000000000L    # 500.0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 133
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_altitude"

    const/16 v2, 0x32

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    const-wide v6, 0x407f400000000000L    # 500.0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 135
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "gps_accuracy"

    const/16 v2, 0x33

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 137
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_grade"

    const/16 v2, 0x34

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 139
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_pos_grade"

    const/16 v2, 0x35

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 141
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_neg_grade"

    const/16 v2, 0x36

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 143
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_pos_grade"

    const/16 v2, 0x37

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 145
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_neg_grade"

    const/16 v2, 0x38

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 147
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_temperature"

    const/16 v2, 0x39

    const/4 v3, 0x1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "C"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 149
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_temperature"

    const/16 v2, 0x3a

    const/4 v3, 0x1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "C"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 151
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_moving_time"

    const/16 v2, 0x3b

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 153
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_pos_vertical_speed"

    const/16 v2, 0x3c

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 155
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_neg_vertical_speed"

    const/16 v2, 0x3d

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 157
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_pos_vertical_speed"

    const/16 v2, 0x3e

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 159
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_neg_vertical_speed"

    const/16 v2, 0x3f

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 161
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "min_heart_rate"

    const/16 v2, 0x40

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 163
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "time_in_hr_zone"

    const/16 v2, 0x41

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 165
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "time_in_speed_zone"

    const/16 v2, 0x42

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 167
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "time_in_cadence_zone"

    const/16 v2, 0x43

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 169
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "time_in_power_zone"

    const/16 v2, 0x44

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 171
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_lap_time"

    const/16 v2, 0x45

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 173
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "best_lap_index"

    const/16 v2, 0x46

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 175
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "min_altitude"

    const/16 v2, 0x47

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    const-wide v6, 0x407f400000000000L    # 500.0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 177
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "player_score"

    const/16 v2, 0x52

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 179
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "opponent_score"

    const/16 v2, 0x53

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 181
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "opponent_name"

    const/16 v2, 0x54

    const/4 v3, 0x7

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 183
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "stroke_count"

    const/16 v2, 0x55

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "counts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 185
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "zone_count"

    const/16 v2, 0x56

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "counts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 187
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_ball_speed"

    const/16 v2, 0x57

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 189
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_ball_speed"

    const/16 v2, 0x58

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 191
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_vertical_oscillation"

    const/16 v2, 0x59

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "mm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 193
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_stance_time_percent"

    const/16 v2, 0x5a

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 195
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_stance_time"

    const/16 v2, 0x5b

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "ms"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 197
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_fractional_cadence"

    const/16 v2, 0x5c

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 199
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_fractional_cadence"

    const/16 v2, 0x5d

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 201
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "total_fractional_cycles"

    const/16 v2, 0x5e

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    const-wide/16 v6, 0x0

    const-string v8, "cycles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 203
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_total_hemoglobin_conc"

    const/16 v2, 0x5f

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "g/dL"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 205
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "min_total_hemoglobin_conc"

    const/16 v2, 0x60

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "g/dL"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 207
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_total_hemoglobin_conc"

    const/16 v2, 0x61

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "g/dL"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 209
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_saturated_hemoglobin_percent"

    const/16 v2, 0x62

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 211
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "min_saturated_hemoglobin_percent"

    const/16 v2, 0x63

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 213
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "max_saturated_hemoglobin_percent"

    const/16 v2, 0x64

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 215
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_left_torque_effectiveness"

    const/16 v2, 0x65

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 217
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_right_torque_effectiveness"

    const/16 v2, 0x66

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 219
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_left_pedal_smoothness"

    const/16 v2, 0x67

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 221
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_right_pedal_smoothness"

    const/16 v2, 0x68

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 223
    sget-object v10, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_combined_pedal_smoothness"

    const/16 v2, 0x69

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string/jumbo v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 225
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 228
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 229
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 233
    return-void
.end method


# virtual methods
.method public getAvgAltitude()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1220
    const/16 v0, 0x31

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgBallSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1850
    const/16 v0, 0x58

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgCadence()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 660
    const/16 v0, 0x12

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getAvgCombinedPedalSmoothness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 2262
    const/16 v0, 0x69

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgFractionalCadence()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1931
    const/16 v0, 0x5c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1280
    const/16 v0, 0x34

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 618
    const/16 v0, 0x10

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getAvgLapTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1656
    const/16 v0, 0x45

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgLeftPedalSmoothness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 2222
    const/16 v0, 0x67

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgLeftTorqueEffectiveness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 2182
    const/16 v0, 0x65

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgNegGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1320
    const/16 v0, 0x36

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgNegVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1460
    const/16 v0, 0x3d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgPosGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1300
    const/16 v0, 0x35

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgPosVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1440
    const/16 v0, 0x3c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgPower()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 742
    const/16 v0, 0x14

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getAvgRightPedalSmoothness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 2242
    const/16 v0, 0x68

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgRightTorqueEffectiveness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 2202
    const/16 v0, 0x66

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgRunningCadence()Ljava/lang/Short;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 681
    const/16 v0, 0x12

    invoke-virtual {p0, v0, v1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getAvgSaturatedHemoglobinPercent(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 2098
    const/16 v0, 0x62

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 576
    const/16 v0, 0xe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgStanceTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1910
    const/16 v0, 0x5b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgStanceTimePercent()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1890
    const/16 v0, 0x5a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgStrokeCount()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1074
    const/16 v0, 0x29

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgStrokeDistance()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1094
    const/16 v0, 0x2a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgTemperature()Ljava/lang/Byte;
    .locals 3

    .prologue
    .line 1380
    const/16 v0, 0x39

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldByteValue(III)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getAvgTotalHemoglobinConc(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 2005
    const/16 v0, 0x5f

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgVerticalOscillation()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1870
    const/16 v0, 0x59

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getBestLapIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1675
    const/16 v0, 0x46

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getEvent()Lcom/garmin/fit/Event;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 285
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, v0}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 286
    if-nez v0, :cond_0

    .line 287
    const/4 v0, 0x0

    .line 288
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Event;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Event;

    move-result-object v0

    goto :goto_0
.end method

.method public getEventGroup()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 876
    const/16 v0, 0x1b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getEventType()Lcom/garmin/fit/EventType;
    .locals 3

    .prologue
    .line 308
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 309
    if-nez v0, :cond_0

    .line 310
    const/4 v0, 0x0

    .line 311
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/EventType;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/EventType;

    move-result-object v0

    goto :goto_0
.end method

.method public getFirstLapIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 840
    const/16 v0, 0x19

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getGpsAccuracy()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 1260
    const/16 v0, 0x33

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getIntensityFactor()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1036
    const/16 v0, 0x24

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getLeftRightBalance()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1055
    const/16 v0, 0x25

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMaxAltitude()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1240
    const/16 v0, 0x32

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxBallSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1830
    const/16 v0, 0x57

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxCadence()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 701
    const/16 v0, 0x13

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMaxFractionalCadence()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1953
    const/16 v0, 0x5d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 639
    const/16 v0, 0x11

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMaxNegGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1360
    const/16 v0, 0x38

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxNegVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1500
    const/16 v0, 0x3f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxPosGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1340
    const/16 v0, 0x37

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxPosVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1480
    const/16 v0, 0x3e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxPower()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 763
    const/16 v0, 0x15

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMaxRunningCadence()Ljava/lang/Short;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 721
    const/16 v0, 0x13

    invoke-virtual {p0, v0, v1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMaxSaturatedHemoglobinPercent(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 2160
    const/16 v0, 0x64

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 597
    const/16 v0, 0xf

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxTemperature()Ljava/lang/Byte;
    .locals 3

    .prologue
    .line 1400
    const/16 v0, 0x3a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldByteValue(III)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getMaxTotalHemoglobinConc(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 2067
    const/16 v0, 0x61

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMessageIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 243
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMinAltitude()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1694
    const/16 v0, 0x47

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMinHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 1520
    const/16 v0, 0x40

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMinSaturatedHemoglobinPercent(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 2129
    const/16 v0, 0x63

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMinTotalHemoglobinConc(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 2036
    const/16 v0, 0x60

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getNecLat()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 916
    const/16 v0, 0x1d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNecLong()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 936
    const/16 v0, 0x1e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNormalizedPower()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 996
    const/16 v0, 0x22

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNumActiveLengths()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1179
    const/16 v0, 0x2f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNumAvgSaturatedHemoglobinPercent()I
    .locals 2

    .prologue
    .line 2086
    const/16 v0, 0x62

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumAvgTotalHemoglobinConc()I
    .locals 2

    .prologue
    .line 1993
    const/16 v0, 0x5f

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumLaps()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 858
    const/16 v0, 0x1a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNumMaxSaturatedHemoglobinPercent()I
    .locals 2

    .prologue
    .line 2148
    const/16 v0, 0x64

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumMaxTotalHemoglobinConc()I
    .locals 2

    .prologue
    .line 2055
    const/16 v0, 0x61

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumMinSaturatedHemoglobinPercent()I
    .locals 2

    .prologue
    .line 2117
    const/16 v0, 0x63

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumMinTotalHemoglobinConc()I
    .locals 2

    .prologue
    .line 2024
    const/16 v0, 0x60

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumStrokeCount()I
    .locals 2

    .prologue
    .line 1765
    const/16 v0, 0x55

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInCadenceZone()I
    .locals 2

    .prologue
    .line 1595
    const/16 v0, 0x43

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInHrZone()I
    .locals 2

    .prologue
    .line 1537
    const/16 v0, 0x41

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInPowerZone()I
    .locals 2

    .prologue
    .line 1624
    const/16 v0, 0x44

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInSpeedZone()I
    .locals 2

    .prologue
    .line 1566
    const/16 v0, 0x42

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumZoneCount()I
    .locals 2

    .prologue
    .line 1796
    const/16 v0, 0x56

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/SessionMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getOpponentName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1749
    const/16 v0, 0x54

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldStringValue(III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOpponentScore()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1731
    const/16 v0, 0x53

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerScore()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1713
    const/16 v0, 0x52

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getPoolLength()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1137
    const/16 v0, 0x2c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getPoolLengthUnit()Lcom/garmin/fit/DisplayMeasure;
    .locals 3

    .prologue
    .line 1156
    const/16 v0, 0x2e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 1157
    if-nez v0, :cond_0

    .line 1158
    const/4 v0, 0x0

    .line 1159
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayMeasure;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayMeasure;

    move-result-object v0

    goto :goto_0
.end method

.method public getSport()Lcom/garmin/fit/Sport;
    .locals 3

    .prologue
    .line 388
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 389
    if-nez v0, :cond_0

    .line 390
    const/4 v0, 0x0

    .line 391
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Sport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Sport;

    move-result-object v0

    goto :goto_0
.end method

.method public getStartPositionLat()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 349
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getStartPositionLong()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 369
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getStartTime()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 330
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/SessionMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public getStrokeCount(I)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1777
    const/16 v0, 0x55

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSubSport()Lcom/garmin/fit/SubSport;
    .locals 3

    .prologue
    .line 409
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 410
    if-nez v0, :cond_0

    .line 411
    const/4 v0, 0x0

    .line 412
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/SubSport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SubSport;

    move-result-object v0

    goto :goto_0
.end method

.method public getSwcLat()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 956
    const/16 v0, 0x1f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSwcLong()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 976
    const/16 v0, 0x20

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSwimStroke()Lcom/garmin/fit/SwimStroke;
    .locals 3

    .prologue
    .line 1114
    const/16 v0, 0x2b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 1115
    if-nez v0, :cond_0

    .line 1116
    const/4 v0, 0x0

    .line 1117
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/SwimStroke;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SwimStroke;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimeInCadenceZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1606
    const/16 v0, 0x43

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimeInHrZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1548
    const/16 v0, 0x41

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimeInPowerZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1635
    const/16 v0, 0x44

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimeInSpeedZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1577
    const/16 v0, 0x42

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimestamp()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 264
    const/16 v0, 0xfd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/SessionMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public getTotalAscent()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 783
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalCalories()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 535
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalCycles()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 495
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTotalDescent()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 803
    const/16 v0, 0x17

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalDistance()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 475
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalElapsedTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 432
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalFatCalories()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 555
    const/16 v0, 0xd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalFractionalCycles()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1975
    const/16 v0, 0x5e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalMovingTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1420
    const/16 v0, 0x3b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalStrides()Ljava/lang/Long;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 515
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTotalTimerTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 454
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalTrainingEffect()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 822
    const/16 v0, 0x18

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalWork()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 1200
    const/16 v0, 0x30

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTrainingStressScore()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1016
    const/16 v0, 0x23

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTrigger()Lcom/garmin/fit/SessionTrigger;
    .locals 3

    .prologue
    .line 894
    const/16 v0, 0x1c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/SessionMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 895
    if-nez v0, :cond_0

    .line 896
    const/4 v0, 0x0

    .line 897
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/SessionTrigger;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SessionTrigger;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoneCount(I)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1808
    const/16 v0, 0x56

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/SessionMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public setAvgAltitude(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1230
    const/16 v0, 0x31

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1231
    return-void
.end method

.method public setAvgBallSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1860
    const/16 v0, 0x58

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1861
    return-void
.end method

.method public setAvgCadence(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 671
    const/16 v0, 0x12

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 672
    return-void
.end method

.method public setAvgCombinedPedalSmoothness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2272
    const/16 v0, 0x69

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2273
    return-void
.end method

.method public setAvgFractionalCadence(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1942
    const/16 v0, 0x5c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1943
    return-void
.end method

.method public setAvgGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1290
    const/16 v0, 0x34

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1291
    return-void
.end method

.method public setAvgHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 629
    const/16 v0, 0x10

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 630
    return-void
.end method

.method public setAvgLapTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1666
    const/16 v0, 0x45

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1667
    return-void
.end method

.method public setAvgLeftPedalSmoothness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2232
    const/16 v0, 0x67

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2233
    return-void
.end method

.method public setAvgLeftTorqueEffectiveness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2192
    const/16 v0, 0x65

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2193
    return-void
.end method

.method public setAvgNegGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1330
    const/16 v0, 0x36

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1331
    return-void
.end method

.method public setAvgNegVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1470
    const/16 v0, 0x3d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1471
    return-void
.end method

.method public setAvgPosGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1310
    const/16 v0, 0x35

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1311
    return-void
.end method

.method public setAvgPosVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1450
    const/16 v0, 0x3c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1451
    return-void
.end method

.method public setAvgPower(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 753
    const/16 v0, 0x14

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 754
    return-void
.end method

.method public setAvgRightPedalSmoothness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2252
    const/16 v0, 0x68

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2253
    return-void
.end method

.method public setAvgRightTorqueEffectiveness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2212
    const/16 v0, 0x66

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2213
    return-void
.end method

.method public setAvgRunningCadence(Ljava/lang/Short;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 691
    const/16 v0, 0x12

    invoke-virtual {p0, v0, v1, p1, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 692
    return-void
.end method

.method public setAvgSaturatedHemoglobinPercent(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 2110
    const/16 v0, 0x62

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2111
    return-void
.end method

.method public setAvgSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 587
    const/16 v0, 0xe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 588
    return-void
.end method

.method public setAvgStanceTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1920
    const/16 v0, 0x5b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1921
    return-void
.end method

.method public setAvgStanceTimePercent(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1900
    const/16 v0, 0x5a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1901
    return-void
.end method

.method public setAvgStrokeCount(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1084
    const/16 v0, 0x29

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1085
    return-void
.end method

.method public setAvgStrokeDistance(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1104
    const/16 v0, 0x2a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1105
    return-void
.end method

.method public setAvgTemperature(Ljava/lang/Byte;)V
    .locals 3

    .prologue
    .line 1390
    const/16 v0, 0x39

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1391
    return-void
.end method

.method public setAvgTotalHemoglobinConc(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 2017
    const/16 v0, 0x5f

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2018
    return-void
.end method

.method public setAvgVerticalOscillation(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1880
    const/16 v0, 0x59

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1881
    return-void
.end method

.method public setBestLapIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1684
    const/16 v0, 0x46

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1685
    return-void
.end method

.method public setEvent(Lcom/garmin/fit/Event;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 298
    iget-short v0, p1, Lcom/garmin/fit/Event;->value:S

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    const v1, 0xffff

    invoke-virtual {p0, v2, v2, v0, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 299
    return-void
.end method

.method public setEventGroup(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 885
    const/16 v0, 0x1b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 886
    return-void
.end method

.method public setEventType(Lcom/garmin/fit/EventType;)V
    .locals 4

    .prologue
    .line 321
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/EventType;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 322
    return-void
.end method

.method public setFirstLapIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 849
    const/16 v0, 0x19

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 850
    return-void
.end method

.method public setGpsAccuracy(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 1270
    const/16 v0, 0x33

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1271
    return-void
.end method

.method public setIntensityFactor(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1046
    const/16 v0, 0x24

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1047
    return-void
.end method

.method public setLeftRightBalance(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1064
    const/16 v0, 0x25

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1065
    return-void
.end method

.method public setMaxAltitude(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1250
    const/16 v0, 0x32

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1251
    return-void
.end method

.method public setMaxBallSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1840
    const/16 v0, 0x57

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1841
    return-void
.end method

.method public setMaxCadence(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 711
    const/16 v0, 0x13

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 712
    return-void
.end method

.method public setMaxFractionalCadence(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1964
    const/16 v0, 0x5d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1965
    return-void
.end method

.method public setMaxHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 649
    const/16 v0, 0x11

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 650
    return-void
.end method

.method public setMaxNegGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1370
    const/16 v0, 0x38

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1371
    return-void
.end method

.method public setMaxNegVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1510
    const/16 v0, 0x3f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1511
    return-void
.end method

.method public setMaxPosGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1350
    const/16 v0, 0x37

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1351
    return-void
.end method

.method public setMaxPosVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1490
    const/16 v0, 0x3e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1491
    return-void
.end method

.method public setMaxPower(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 773
    const/16 v0, 0x15

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 774
    return-void
.end method

.method public setMaxRunningCadence(Ljava/lang/Short;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 731
    const/16 v0, 0x13

    invoke-virtual {p0, v0, v1, p1, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 732
    return-void
.end method

.method public setMaxSaturatedHemoglobinPercent(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 2172
    const/16 v0, 0x64

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2173
    return-void
.end method

.method public setMaxSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 607
    const/16 v0, 0xf

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 608
    return-void
.end method

.method public setMaxTemperature(Ljava/lang/Byte;)V
    .locals 3

    .prologue
    .line 1410
    const/16 v0, 0x3a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1411
    return-void
.end method

.method public setMaxTotalHemoglobinConc(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 2079
    const/16 v0, 0x61

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2080
    return-void
.end method

.method public setMessageIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 253
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 254
    return-void
.end method

.method public setMinAltitude(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1704
    const/16 v0, 0x47

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1705
    return-void
.end method

.method public setMinHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 1530
    const/16 v0, 0x40

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1531
    return-void
.end method

.method public setMinSaturatedHemoglobinPercent(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 2141
    const/16 v0, 0x63

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2142
    return-void
.end method

.method public setMinTotalHemoglobinConc(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 2048
    const/16 v0, 0x60

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2049
    return-void
.end method

.method public setNecLat(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 926
    const/16 v0, 0x1d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 927
    return-void
.end method

.method public setNecLong(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 946
    const/16 v0, 0x1e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 947
    return-void
.end method

.method public setNormalizedPower(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1006
    const/16 v0, 0x22

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1007
    return-void
.end method

.method public setNumActiveLengths(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1190
    const/16 v0, 0x2f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1191
    return-void
.end method

.method public setNumLaps(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 867
    const/16 v0, 0x1a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 868
    return-void
.end method

.method public setOpponentName(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1758
    const/16 v0, 0x54

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1759
    return-void
.end method

.method public setOpponentScore(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1740
    const/16 v0, 0x53

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1741
    return-void
.end method

.method public setPlayerScore(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1722
    const/16 v0, 0x52

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1723
    return-void
.end method

.method public setPoolLength(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1147
    const/16 v0, 0x2c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1148
    return-void
.end method

.method public setPoolLengthUnit(Lcom/garmin/fit/DisplayMeasure;)V
    .locals 4

    .prologue
    .line 1168
    const/16 v0, 0x2e

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayMeasure;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1169
    return-void
.end method

.method public setSport(Lcom/garmin/fit/Sport;)V
    .locals 4

    .prologue
    .line 400
    const/4 v0, 0x5

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Sport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 401
    return-void
.end method

.method public setStartPositionLat(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 359
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 360
    return-void
.end method

.method public setStartPositionLong(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 379
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 380
    return-void
.end method

.method public setStartTime(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 339
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 340
    return-void
.end method

.method public setStrokeCount(ILjava/lang/Integer;)V
    .locals 2

    .prologue
    .line 1789
    const/16 v0, 0x55

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1790
    return-void
.end method

.method public setSubSport(Lcom/garmin/fit/SubSport;)V
    .locals 4

    .prologue
    .line 421
    const/4 v0, 0x6

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/SubSport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 422
    return-void
.end method

.method public setSwcLat(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 966
    const/16 v0, 0x1f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 967
    return-void
.end method

.method public setSwcLong(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 986
    const/16 v0, 0x20

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 987
    return-void
.end method

.method public setSwimStroke(Lcom/garmin/fit/SwimStroke;)V
    .locals 4

    .prologue
    .line 1127
    const/16 v0, 0x2b

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/SwimStroke;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1128
    return-void
.end method

.method public setTimeInCadenceZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1617
    const/16 v0, 0x43

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1618
    return-void
.end method

.method public setTimeInHrZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1559
    const/16 v0, 0x41

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1560
    return-void
.end method

.method public setTimeInPowerZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1646
    const/16 v0, 0x44

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1647
    return-void
.end method

.method public setTimeInSpeedZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1588
    const/16 v0, 0x42

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1589
    return-void
.end method

.method public setTimestamp(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 275
    const/16 v0, 0xfd

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 276
    return-void
.end method

.method public setTotalAscent(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 793
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 794
    return-void
.end method

.method public setTotalCalories(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 545
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 546
    return-void
.end method

.method public setTotalCycles(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 505
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 506
    return-void
.end method

.method public setTotalDescent(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 813
    const/16 v0, 0x17

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 814
    return-void
.end method

.method public setTotalDistance(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 485
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 486
    return-void
.end method

.method public setTotalElapsedTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 443
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 444
    return-void
.end method

.method public setTotalFatCalories(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 565
    const/16 v0, 0xd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 566
    return-void
.end method

.method public setTotalFractionalCycles(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1986
    const/16 v0, 0x5e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1987
    return-void
.end method

.method public setTotalMovingTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1430
    const/16 v0, 0x3b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1431
    return-void
.end method

.method public setTotalStrides(Ljava/lang/Long;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 525
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v1, p1, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 526
    return-void
.end method

.method public setTotalTimerTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 465
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 466
    return-void
.end method

.method public setTotalTrainingEffect(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 831
    const/16 v0, 0x18

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 832
    return-void
.end method

.method public setTotalWork(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 1210
    const/16 v0, 0x30

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1211
    return-void
.end method

.method public setTrainingStressScore(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1026
    const/16 v0, 0x23

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1027
    return-void
.end method

.method public setTrigger(Lcom/garmin/fit/SessionTrigger;)V
    .locals 4

    .prologue
    .line 906
    const/16 v0, 0x1c

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/SessionTrigger;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 907
    return-void
.end method

.method public setZoneCount(ILjava/lang/Integer;)V
    .locals 2

    .prologue
    .line 1820
    const/16 v0, 0x56

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/SessionMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1821
    return-void
.end method
