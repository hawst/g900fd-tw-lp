.class public Lcom/garmin/fit/Fit;
.super Ljava/lang/Object;
.source "Fit.java"


# static fields
.field public static final BYTE_INVALID:Ljava/lang/Short;

.field public static final ENUM_INVALID:Ljava/lang/Short;

.field public static final FLOAT32_INVALID:Ljava/lang/Float;

.field public static final FLOAT64_INVALID:Ljava/lang/Double;

.field public static final SINT16_INVALID:Ljava/lang/Short;

.field public static final SINT32_INVALID:Ljava/lang/Integer;

.field public static final SINT8_INVALID:Ljava/lang/Byte;

.field public static final STRING_INVALID:Ljava/lang/String;

.field public static final SUBFIELD_NAME_MAIN_FIELD:Ljava/lang/String;

.field public static final UINT16Z_INVALID:Ljava/lang/Integer;

.field public static final UINT16_INVALID:Ljava/lang/Integer;

.field public static final UINT32Z_INVALID:Ljava/lang/Long;

.field public static final UINT32_INVALID:Ljava/lang/Long;

.field public static final UINT8Z_INVALID:Ljava/lang/Short;

.field public static final UINT8_INVALID:Ljava/lang/Short;

.field public static final baseTypeSizes:[I

.field public static debug:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/garmin/fit/Fit;->debug:Z

    .line 62
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    sput-object v0, Lcom/garmin/fit/Fit;->SUBFIELD_NAME_MAIN_FIELD:Ljava/lang/String;

    .line 68
    const-string v0, "0xFF"

    invoke-static {v0}, Ljava/lang/Short;->decode(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->ENUM_INVALID:Ljava/lang/Short;

    .line 70
    const-string v0, "0x7F"

    invoke-static {v0}, Ljava/lang/Byte;->decode(Ljava/lang/String;)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->SINT8_INVALID:Ljava/lang/Byte;

    .line 72
    const-string v0, "0xFF"

    invoke-static {v0}, Ljava/lang/Short;->decode(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->UINT8_INVALID:Ljava/lang/Short;

    .line 74
    const-string v0, "0x7FFF"

    invoke-static {v0}, Ljava/lang/Short;->decode(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->SINT16_INVALID:Ljava/lang/Short;

    .line 76
    const-string v0, "0xFFFF"

    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    .line 78
    const-string v0, "0x7FFFFFFF"

    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->SINT32_INVALID:Ljava/lang/Integer;

    .line 80
    const-string v0, "0xFFFFFFFF"

    invoke-static {v0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->UINT32_INVALID:Ljava/lang/Long;

    .line 82
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    sput-object v0, Lcom/garmin/fit/Fit;->STRING_INVALID:Ljava/lang/String;

    .line 84
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->FLOAT32_INVALID:Ljava/lang/Float;

    .line 86
    const-wide/16 v0, -0x1

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->FLOAT64_INVALID:Ljava/lang/Double;

    .line 88
    const-string v0, "0x00"

    invoke-static {v0}, Ljava/lang/Short;->decode(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->UINT8Z_INVALID:Ljava/lang/Short;

    .line 90
    const-string v0, "0x0000"

    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->UINT16Z_INVALID:Ljava/lang/Integer;

    .line 92
    const-string v0, "0x00000000"

    invoke-static {v0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->UINT32Z_INVALID:Ljava/lang/Long;

    .line 94
    const/16 v0, 0xff

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    sput-object v0, Lcom/garmin/fit/Fit;->BYTE_INVALID:Ljava/lang/Short;

    .line 98
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/garmin/fit/Fit;->baseTypeSizes:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x1
        0x1
        0x2
        0x2
        0x4
        0x4
        0x1
        0x2
        0x4
        0x1
        0x2
        0x4
        0x1
    .end array-data
.end method
