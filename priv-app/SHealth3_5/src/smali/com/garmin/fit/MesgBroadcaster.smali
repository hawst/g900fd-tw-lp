.class public Lcom/garmin/fit/MesgBroadcaster;
.super Ljava/lang/Object;
.source "MesgBroadcaster.java"

# interfaces
.implements Lcom/garmin/fit/MesgListener;


# instance fields
.field private final activityMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/ActivityMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final bikeProfileMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/BikeProfileMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final bloodPressureMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/BloodPressureMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final bufferedRecordMesgBroadcaster:Lcom/garmin/fit/BufferedRecordMesgBroadcaster;

.field private final cadenceZoneMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/CadenceZoneMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final capabilitiesMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/CapabilitiesMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final courseMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/CourseMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final coursePointMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/CoursePointMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final decode:Lcom/garmin/fit/Decode;

.field private final deviceInfoMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/DeviceInfoMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceSettingsMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/DeviceSettingsMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final eventMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/EventMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final fieldCapabilitiesMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/FieldCapabilitiesMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final fileCapabilitiesMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/FileCapabilitiesMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final fileCreatorMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/FileCreatorMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final fileIdMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/FileIdMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final goalMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/GoalMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final hrZoneMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/HrZoneMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final hrmProfileMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/HrmProfileMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final hrvMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/HrvMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final lapMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/LapMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final lengthMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/LengthMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final memoGlobMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/MemoGlobMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mesgCapabilitiesMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/MesgCapabilitiesMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/MesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

.field private final metZoneMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/MetZoneMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final monitoringInfoMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/MonitoringInfoMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final monitoringMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/MonitoringMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final padMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/PadMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final powerZoneMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/PowerZoneMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final recordMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/RecordMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final scheduleMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/ScheduleMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final sdmProfileMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/SdmProfileMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/SessionMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final slaveDeviceMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/SlaveDeviceMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final softwareMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/SoftwareMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final speedZoneMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/SpeedZoneMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final sportMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/SportMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final totalsMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/TotalsMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final userProfileMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/UserProfileMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final weightScaleMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/WeightScaleMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final workoutMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/WorkoutMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final workoutStepMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/WorkoutStepMesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private final zonesTargetMesgListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/ZonesTargetMesgListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/garmin/fit/Decode;

    invoke-direct {v0}, Lcom/garmin/fit/Decode;-><init>()V

    invoke-direct {p0, v0}, Lcom/garmin/fit/MesgBroadcaster;-><init>(Lcom/garmin/fit/Decode;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Decode;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/garmin/fit/MesgBroadcaster;->decode:Lcom/garmin/fit/Decode;

    .line 77
    new-instance v0, Lcom/garmin/fit/MesgWithEventBroadcaster;

    invoke-direct {v0}, Lcom/garmin/fit/MesgWithEventBroadcaster;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

    .line 78
    new-instance v0, Lcom/garmin/fit/BufferedRecordMesgBroadcaster;

    invoke-direct {v0}, Lcom/garmin/fit/BufferedRecordMesgBroadcaster;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bufferedRecordMesgBroadcaster:Lcom/garmin/fit/BufferedRecordMesgBroadcaster;

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgListeners:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileIdMesgListeners:Ljava/util/List;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileCreatorMesgListeners:Ljava/util/List;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->softwareMesgListeners:Ljava/util/List;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->slaveDeviceMesgListeners:Ljava/util/List;

    .line 84
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->capabilitiesMesgListeners:Ljava/util/List;

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileCapabilitiesMesgListeners:Ljava/util/List;

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgCapabilitiesMesgListeners:Ljava/util/List;

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fieldCapabilitiesMesgListeners:Ljava/util/List;

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->deviceSettingsMesgListeners:Ljava/util/List;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->userProfileMesgListeners:Ljava/util/List;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrmProfileMesgListeners:Ljava/util/List;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sdmProfileMesgListeners:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bikeProfileMesgListeners:Ljava/util/List;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->zonesTargetMesgListeners:Ljava/util/List;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sportMesgListeners:Ljava/util/List;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrZoneMesgListeners:Ljava/util/List;

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->speedZoneMesgListeners:Ljava/util/List;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->cadenceZoneMesgListeners:Ljava/util/List;

    .line 98
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->powerZoneMesgListeners:Ljava/util/List;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->metZoneMesgListeners:Ljava/util/List;

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->goalMesgListeners:Ljava/util/List;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->activityMesgListeners:Ljava/util/List;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sessionMesgListeners:Ljava/util/List;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->lapMesgListeners:Ljava/util/List;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->lengthMesgListeners:Ljava/util/List;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->recordMesgListeners:Ljava/util/List;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->eventMesgListeners:Ljava/util/List;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->deviceInfoMesgListeners:Ljava/util/List;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrvMesgListeners:Ljava/util/List;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->courseMesgListeners:Ljava/util/List;

    .line 110
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->coursePointMesgListeners:Ljava/util/List;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->workoutMesgListeners:Ljava/util/List;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->workoutStepMesgListeners:Ljava/util/List;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->scheduleMesgListeners:Ljava/util/List;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->totalsMesgListeners:Ljava/util/List;

    .line 115
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->weightScaleMesgListeners:Ljava/util/List;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bloodPressureMesgListeners:Ljava/util/List;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->monitoringInfoMesgListeners:Ljava/util/List;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->monitoringMesgListeners:Ljava/util/List;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->memoGlobMesgListeners:Ljava/util/List;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->padMesgListeners:Ljava/util/List;

    .line 121
    return-void
.end method


# virtual methods
.method public addListener(Lcom/garmin/fit/ActivityMesgListener;)V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->activityMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    return-void
.end method

.method public addListener(Lcom/garmin/fit/BikeProfileMesgListener;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bikeProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    return-void
.end method

.method public addListener(Lcom/garmin/fit/BloodPressureMesgListener;)V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bloodPressureMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    return-void
.end method

.method public addListener(Lcom/garmin/fit/BufferedRecordMesgListener;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bufferedRecordMesgBroadcaster:Lcom/garmin/fit/BufferedRecordMesgBroadcaster;

    invoke-virtual {v0, p1}, Lcom/garmin/fit/BufferedRecordMesgBroadcaster;->addListener(Lcom/garmin/fit/BufferedRecordMesgListener;)V

    .line 147
    return-void
.end method

.method public addListener(Lcom/garmin/fit/CadenceZoneMesgListener;)V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->cadenceZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    return-void
.end method

.method public addListener(Lcom/garmin/fit/CapabilitiesMesgListener;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->capabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    return-void
.end method

.method public addListener(Lcom/garmin/fit/CourseMesgListener;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->courseMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    return-void
.end method

.method public addListener(Lcom/garmin/fit/CoursePointMesgListener;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->coursePointMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    return-void
.end method

.method public addListener(Lcom/garmin/fit/DeviceInfoMesgListener;)V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->deviceInfoMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    return-void
.end method

.method public addListener(Lcom/garmin/fit/DeviceSettingsMesgListener;)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->deviceSettingsMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    return-void
.end method

.method public addListener(Lcom/garmin/fit/EventMesgListener;)V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->eventMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    return-void
.end method

.method public addListener(Lcom/garmin/fit/FieldCapabilitiesMesgListener;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fieldCapabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    return-void
.end method

.method public addListener(Lcom/garmin/fit/FileCapabilitiesMesgListener;)V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileCapabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    return-void
.end method

.method public addListener(Lcom/garmin/fit/FileCreatorMesgListener;)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileCreatorMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    return-void
.end method

.method public addListener(Lcom/garmin/fit/FileIdMesgListener;)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileIdMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    return-void
.end method

.method public addListener(Lcom/garmin/fit/GoalMesgListener;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->goalMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    return-void
.end method

.method public addListener(Lcom/garmin/fit/HrZoneMesgListener;)V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    return-void
.end method

.method public addListener(Lcom/garmin/fit/HrmProfileMesgListener;)V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrmProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    return-void
.end method

.method public addListener(Lcom/garmin/fit/HrvMesgListener;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrvMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    return-void
.end method

.method public addListener(Lcom/garmin/fit/LapMesgListener;)V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->lapMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    return-void
.end method

.method public addListener(Lcom/garmin/fit/LengthMesgListener;)V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->lengthMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MemoGlobMesgListener;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->memoGlobMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MesgCapabilitiesMesgListener;)V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgCapabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MesgListener;)V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MesgWithEventListener;)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

    invoke-virtual {v0, p1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->addListener(Lcom/garmin/fit/MesgWithEventListener;)V

    .line 143
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MetZoneMesgListener;)V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->metZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MonitoringInfoMesgListener;)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->monitoringInfoMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MonitoringMesgListener;)V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->monitoringMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    return-void
.end method

.method public addListener(Lcom/garmin/fit/PadMesgListener;)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->padMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    return-void
.end method

.method public addListener(Lcom/garmin/fit/PowerZoneMesgListener;)V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->powerZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    return-void
.end method

.method public addListener(Lcom/garmin/fit/RecordMesgListener;)V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->recordMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    return-void
.end method

.method public addListener(Lcom/garmin/fit/ScheduleMesgListener;)V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->scheduleMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    return-void
.end method

.method public addListener(Lcom/garmin/fit/SdmProfileMesgListener;)V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sdmProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    return-void
.end method

.method public addListener(Lcom/garmin/fit/SessionMesgListener;)V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sessionMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method public addListener(Lcom/garmin/fit/SlaveDeviceMesgListener;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->slaveDeviceMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    return-void
.end method

.method public addListener(Lcom/garmin/fit/SoftwareMesgListener;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->softwareMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    return-void
.end method

.method public addListener(Lcom/garmin/fit/SpeedZoneMesgListener;)V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->speedZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    return-void
.end method

.method public addListener(Lcom/garmin/fit/SportMesgListener;)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sportMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    return-void
.end method

.method public addListener(Lcom/garmin/fit/TotalsMesgListener;)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->totalsMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    return-void
.end method

.method public addListener(Lcom/garmin/fit/UserProfileMesgListener;)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->userProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    return-void
.end method

.method public addListener(Lcom/garmin/fit/WeightScaleMesgListener;)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->weightScaleMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    return-void
.end method

.method public addListener(Lcom/garmin/fit/WorkoutMesgListener;)V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->workoutMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    return-void
.end method

.method public addListener(Lcom/garmin/fit/WorkoutStepMesgListener;)V
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->workoutStepMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    return-void
.end method

.method public addListener(Lcom/garmin/fit/ZonesTargetMesgListener;)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->zonesTargetMesgListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    return-void
.end method

.method public onMesg(Lcom/garmin/fit/Mesg;)V
    .locals 3

    .prologue
    .line 274
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MesgListener;

    .line 275
    invoke-interface {v0, p1}, Lcom/garmin/fit/MesgListener;->onMesg(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 277
    :cond_0
    invoke-virtual {p1}, Lcom/garmin/fit/Mesg;->getNum()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 493
    :cond_1
    :goto_1
    return-void

    .line 279
    :sswitch_0
    new-instance v1, Lcom/garmin/fit/FileIdMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/FileIdMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 280
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileIdMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FileIdMesgListener;

    .line 281
    invoke-interface {v0, v1}, Lcom/garmin/fit/FileIdMesgListener;->onMesg(Lcom/garmin/fit/FileIdMesg;)V

    goto :goto_2

    .line 284
    :sswitch_1
    new-instance v1, Lcom/garmin/fit/FileCreatorMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/FileCreatorMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 285
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileCreatorMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FileCreatorMesgListener;

    .line 286
    invoke-interface {v0, v1}, Lcom/garmin/fit/FileCreatorMesgListener;->onMesg(Lcom/garmin/fit/FileCreatorMesg;)V

    goto :goto_3

    .line 289
    :sswitch_2
    new-instance v1, Lcom/garmin/fit/SoftwareMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/SoftwareMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 290
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->softwareMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SoftwareMesgListener;

    .line 291
    invoke-interface {v0, v1}, Lcom/garmin/fit/SoftwareMesgListener;->onMesg(Lcom/garmin/fit/SoftwareMesg;)V

    goto :goto_4

    .line 294
    :sswitch_3
    new-instance v1, Lcom/garmin/fit/SlaveDeviceMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/SlaveDeviceMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 295
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->slaveDeviceMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SlaveDeviceMesgListener;

    .line 296
    invoke-interface {v0, v1}, Lcom/garmin/fit/SlaveDeviceMesgListener;->onMesg(Lcom/garmin/fit/SlaveDeviceMesg;)V

    goto :goto_5

    .line 299
    :sswitch_4
    new-instance v1, Lcom/garmin/fit/CapabilitiesMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/CapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 300
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->capabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/CapabilitiesMesgListener;

    .line 301
    invoke-interface {v0, v1}, Lcom/garmin/fit/CapabilitiesMesgListener;->onMesg(Lcom/garmin/fit/CapabilitiesMesg;)V

    goto :goto_6

    .line 304
    :sswitch_5
    new-instance v1, Lcom/garmin/fit/FileCapabilitiesMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/FileCapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 305
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fileCapabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FileCapabilitiesMesgListener;

    .line 306
    invoke-interface {v0, v1}, Lcom/garmin/fit/FileCapabilitiesMesgListener;->onMesg(Lcom/garmin/fit/FileCapabilitiesMesg;)V

    goto :goto_7

    .line 309
    :sswitch_6
    new-instance v1, Lcom/garmin/fit/MesgCapabilitiesMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/MesgCapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 310
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgCapabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MesgCapabilitiesMesgListener;

    .line 311
    invoke-interface {v0, v1}, Lcom/garmin/fit/MesgCapabilitiesMesgListener;->onMesg(Lcom/garmin/fit/MesgCapabilitiesMesg;)V

    goto :goto_8

    .line 314
    :sswitch_7
    new-instance v1, Lcom/garmin/fit/FieldCapabilitiesMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/FieldCapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 315
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->fieldCapabilitiesMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldCapabilitiesMesgListener;

    .line 316
    invoke-interface {v0, v1}, Lcom/garmin/fit/FieldCapabilitiesMesgListener;->onMesg(Lcom/garmin/fit/FieldCapabilitiesMesg;)V

    goto :goto_9

    .line 319
    :sswitch_8
    new-instance v1, Lcom/garmin/fit/DeviceSettingsMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/DeviceSettingsMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 320
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->deviceSettingsMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/DeviceSettingsMesgListener;

    .line 321
    invoke-interface {v0, v1}, Lcom/garmin/fit/DeviceSettingsMesgListener;->onMesg(Lcom/garmin/fit/DeviceSettingsMesg;)V

    goto :goto_a

    .line 324
    :sswitch_9
    new-instance v1, Lcom/garmin/fit/UserProfileMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/UserProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 325
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->userProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/UserProfileMesgListener;

    .line 326
    invoke-interface {v0, v1}, Lcom/garmin/fit/UserProfileMesgListener;->onMesg(Lcom/garmin/fit/UserProfileMesg;)V

    goto :goto_b

    .line 329
    :sswitch_a
    new-instance v1, Lcom/garmin/fit/HrmProfileMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/HrmProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 330
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrmProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/HrmProfileMesgListener;

    .line 331
    invoke-interface {v0, v1}, Lcom/garmin/fit/HrmProfileMesgListener;->onMesg(Lcom/garmin/fit/HrmProfileMesg;)V

    goto :goto_c

    .line 334
    :sswitch_b
    new-instance v1, Lcom/garmin/fit/SdmProfileMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/SdmProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 335
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sdmProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SdmProfileMesgListener;

    .line 336
    invoke-interface {v0, v1}, Lcom/garmin/fit/SdmProfileMesgListener;->onMesg(Lcom/garmin/fit/SdmProfileMesg;)V

    goto :goto_d

    .line 339
    :sswitch_c
    new-instance v1, Lcom/garmin/fit/BikeProfileMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/BikeProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 340
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bikeProfileMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/BikeProfileMesgListener;

    .line 341
    invoke-interface {v0, v1}, Lcom/garmin/fit/BikeProfileMesgListener;->onMesg(Lcom/garmin/fit/BikeProfileMesg;)V

    goto :goto_e

    .line 344
    :sswitch_d
    new-instance v1, Lcom/garmin/fit/ZonesTargetMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/ZonesTargetMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 345
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->zonesTargetMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/ZonesTargetMesgListener;

    .line 346
    invoke-interface {v0, v1}, Lcom/garmin/fit/ZonesTargetMesgListener;->onMesg(Lcom/garmin/fit/ZonesTargetMesg;)V

    goto :goto_f

    .line 349
    :sswitch_e
    new-instance v1, Lcom/garmin/fit/SportMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/SportMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 350
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sportMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_10
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SportMesgListener;

    .line 351
    invoke-interface {v0, v1}, Lcom/garmin/fit/SportMesgListener;->onMesg(Lcom/garmin/fit/SportMesg;)V

    goto :goto_10

    .line 354
    :sswitch_f
    new-instance v1, Lcom/garmin/fit/HrZoneMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/HrZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 355
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/HrZoneMesgListener;

    .line 356
    invoke-interface {v0, v1}, Lcom/garmin/fit/HrZoneMesgListener;->onMesg(Lcom/garmin/fit/HrZoneMesg;)V

    goto :goto_11

    .line 359
    :sswitch_10
    new-instance v1, Lcom/garmin/fit/SpeedZoneMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/SpeedZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 360
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->speedZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SpeedZoneMesgListener;

    .line 361
    invoke-interface {v0, v1}, Lcom/garmin/fit/SpeedZoneMesgListener;->onMesg(Lcom/garmin/fit/SpeedZoneMesg;)V

    goto :goto_12

    .line 364
    :sswitch_11
    new-instance v1, Lcom/garmin/fit/CadenceZoneMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/CadenceZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 365
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->cadenceZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_13
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/CadenceZoneMesgListener;

    .line 366
    invoke-interface {v0, v1}, Lcom/garmin/fit/CadenceZoneMesgListener;->onMesg(Lcom/garmin/fit/CadenceZoneMesg;)V

    goto :goto_13

    .line 369
    :sswitch_12
    new-instance v1, Lcom/garmin/fit/PowerZoneMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/PowerZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 370
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->powerZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_14
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/PowerZoneMesgListener;

    .line 371
    invoke-interface {v0, v1}, Lcom/garmin/fit/PowerZoneMesgListener;->onMesg(Lcom/garmin/fit/PowerZoneMesg;)V

    goto :goto_14

    .line 374
    :sswitch_13
    new-instance v1, Lcom/garmin/fit/MetZoneMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/MetZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 375
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->metZoneMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MetZoneMesgListener;

    .line 376
    invoke-interface {v0, v1}, Lcom/garmin/fit/MetZoneMesgListener;->onMesg(Lcom/garmin/fit/MetZoneMesg;)V

    goto :goto_15

    .line 379
    :sswitch_14
    new-instance v1, Lcom/garmin/fit/GoalMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/GoalMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 380
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->goalMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/GoalMesgListener;

    .line 381
    invoke-interface {v0, v1}, Lcom/garmin/fit/GoalMesgListener;->onMesg(Lcom/garmin/fit/GoalMesg;)V

    goto :goto_16

    .line 384
    :sswitch_15
    new-instance v1, Lcom/garmin/fit/ActivityMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/ActivityMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 385
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->activityMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/ActivityMesgListener;

    .line 386
    invoke-interface {v0, v1}, Lcom/garmin/fit/ActivityMesgListener;->onMesg(Lcom/garmin/fit/ActivityMesg;)V

    goto :goto_17

    .line 387
    :cond_2
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->onMesg(Lcom/garmin/fit/MesgWithEvent;)V

    goto/16 :goto_1

    .line 390
    :sswitch_16
    new-instance v1, Lcom/garmin/fit/SessionMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/SessionMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 391
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->sessionMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_18
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SessionMesgListener;

    .line 392
    invoke-interface {v0, v1}, Lcom/garmin/fit/SessionMesgListener;->onMesg(Lcom/garmin/fit/SessionMesg;)V

    goto :goto_18

    .line 393
    :cond_3
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->onMesg(Lcom/garmin/fit/MesgWithEvent;)V

    goto/16 :goto_1

    .line 396
    :sswitch_17
    new-instance v1, Lcom/garmin/fit/LapMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/LapMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 397
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->lapMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/LapMesgListener;

    .line 398
    invoke-interface {v0, v1}, Lcom/garmin/fit/LapMesgListener;->onMesg(Lcom/garmin/fit/LapMesg;)V

    goto :goto_19

    .line 399
    :cond_4
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->onMesg(Lcom/garmin/fit/MesgWithEvent;)V

    goto/16 :goto_1

    .line 402
    :sswitch_18
    new-instance v1, Lcom/garmin/fit/LengthMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/LengthMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 403
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->lengthMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/LengthMesgListener;

    .line 404
    invoke-interface {v0, v1}, Lcom/garmin/fit/LengthMesgListener;->onMesg(Lcom/garmin/fit/LengthMesg;)V

    goto :goto_1a

    .line 405
    :cond_5
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->onMesg(Lcom/garmin/fit/MesgWithEvent;)V

    goto/16 :goto_1

    .line 408
    :sswitch_19
    new-instance v1, Lcom/garmin/fit/RecordMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/RecordMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 409
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->recordMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/RecordMesgListener;

    .line 410
    invoke-interface {v0, v1}, Lcom/garmin/fit/RecordMesgListener;->onMesg(Lcom/garmin/fit/RecordMesg;)V

    goto :goto_1b

    .line 411
    :cond_6
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bufferedRecordMesgBroadcaster:Lcom/garmin/fit/BufferedRecordMesgBroadcaster;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/BufferedRecordMesgBroadcaster;->onMesg(Lcom/garmin/fit/RecordMesg;)V

    goto/16 :goto_1

    .line 414
    :sswitch_1a
    new-instance v1, Lcom/garmin/fit/EventMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/EventMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 415
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->eventMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/EventMesgListener;

    .line 416
    invoke-interface {v0, v1}, Lcom/garmin/fit/EventMesgListener;->onMesg(Lcom/garmin/fit/EventMesg;)V

    goto :goto_1c

    .line 417
    :cond_7
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->mesgWithEventBroadcaster:Lcom/garmin/fit/MesgWithEventBroadcaster;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->onMesg(Lcom/garmin/fit/MesgWithEvent;)V

    goto/16 :goto_1

    .line 420
    :sswitch_1b
    new-instance v1, Lcom/garmin/fit/DeviceInfoMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/DeviceInfoMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 421
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->deviceInfoMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/DeviceInfoMesgListener;

    .line 422
    invoke-interface {v0, v1}, Lcom/garmin/fit/DeviceInfoMesgListener;->onMesg(Lcom/garmin/fit/DeviceInfoMesg;)V

    goto :goto_1d

    .line 425
    :sswitch_1c
    new-instance v1, Lcom/garmin/fit/HrvMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/HrvMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 426
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->hrvMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/HrvMesgListener;

    .line 427
    invoke-interface {v0, v1}, Lcom/garmin/fit/HrvMesgListener;->onMesg(Lcom/garmin/fit/HrvMesg;)V

    goto :goto_1e

    .line 430
    :sswitch_1d
    new-instance v1, Lcom/garmin/fit/CourseMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/CourseMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 431
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->courseMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/CourseMesgListener;

    .line 432
    invoke-interface {v0, v1}, Lcom/garmin/fit/CourseMesgListener;->onMesg(Lcom/garmin/fit/CourseMesg;)V

    goto :goto_1f

    .line 435
    :sswitch_1e
    new-instance v1, Lcom/garmin/fit/CoursePointMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/CoursePointMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 436
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->coursePointMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/CoursePointMesgListener;

    .line 437
    invoke-interface {v0, v1}, Lcom/garmin/fit/CoursePointMesgListener;->onMesg(Lcom/garmin/fit/CoursePointMesg;)V

    goto :goto_20

    .line 440
    :sswitch_1f
    new-instance v1, Lcom/garmin/fit/WorkoutMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/WorkoutMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 441
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->workoutMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/WorkoutMesgListener;

    .line 442
    invoke-interface {v0, v1}, Lcom/garmin/fit/WorkoutMesgListener;->onMesg(Lcom/garmin/fit/WorkoutMesg;)V

    goto :goto_21

    .line 445
    :sswitch_20
    new-instance v1, Lcom/garmin/fit/WorkoutStepMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/WorkoutStepMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 446
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->workoutStepMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_22
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/WorkoutStepMesgListener;

    .line 447
    invoke-interface {v0, v1}, Lcom/garmin/fit/WorkoutStepMesgListener;->onMesg(Lcom/garmin/fit/WorkoutStepMesg;)V

    goto :goto_22

    .line 450
    :sswitch_21
    new-instance v1, Lcom/garmin/fit/ScheduleMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/ScheduleMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 451
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->scheduleMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/ScheduleMesgListener;

    .line 452
    invoke-interface {v0, v1}, Lcom/garmin/fit/ScheduleMesgListener;->onMesg(Lcom/garmin/fit/ScheduleMesg;)V

    goto :goto_23

    .line 455
    :sswitch_22
    new-instance v1, Lcom/garmin/fit/TotalsMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/TotalsMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 456
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->totalsMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/TotalsMesgListener;

    .line 457
    invoke-interface {v0, v1}, Lcom/garmin/fit/TotalsMesgListener;->onMesg(Lcom/garmin/fit/TotalsMesg;)V

    goto :goto_24

    .line 460
    :sswitch_23
    new-instance v1, Lcom/garmin/fit/WeightScaleMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/WeightScaleMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 461
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->weightScaleMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_25
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/WeightScaleMesgListener;

    .line 462
    invoke-interface {v0, v1}, Lcom/garmin/fit/WeightScaleMesgListener;->onMesg(Lcom/garmin/fit/WeightScaleMesg;)V

    goto :goto_25

    .line 465
    :sswitch_24
    new-instance v1, Lcom/garmin/fit/BloodPressureMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/BloodPressureMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 466
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->bloodPressureMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/BloodPressureMesgListener;

    .line 467
    invoke-interface {v0, v1}, Lcom/garmin/fit/BloodPressureMesgListener;->onMesg(Lcom/garmin/fit/BloodPressureMesg;)V

    goto :goto_26

    .line 470
    :sswitch_25
    new-instance v1, Lcom/garmin/fit/MonitoringInfoMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/MonitoringInfoMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 471
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->monitoringInfoMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MonitoringInfoMesgListener;

    .line 472
    invoke-interface {v0, v1}, Lcom/garmin/fit/MonitoringInfoMesgListener;->onMesg(Lcom/garmin/fit/MonitoringInfoMesg;)V

    goto :goto_27

    .line 475
    :sswitch_26
    new-instance v1, Lcom/garmin/fit/MonitoringMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/MonitoringMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 476
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->monitoringMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MonitoringMesgListener;

    .line 477
    invoke-interface {v0, v1}, Lcom/garmin/fit/MonitoringMesgListener;->onMesg(Lcom/garmin/fit/MonitoringMesg;)V

    goto :goto_28

    .line 480
    :sswitch_27
    new-instance v1, Lcom/garmin/fit/MemoGlobMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/MemoGlobMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 481
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->memoGlobMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_29
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MemoGlobMesgListener;

    .line 482
    invoke-interface {v0, v1}, Lcom/garmin/fit/MemoGlobMesgListener;->onMesg(Lcom/garmin/fit/MemoGlobMesg;)V

    goto :goto_29

    .line 485
    :sswitch_28
    new-instance v1, Lcom/garmin/fit/PadMesg;

    invoke-direct {v1, p1}, Lcom/garmin/fit/PadMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 486
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->padMesgListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/PadMesgListener;

    .line 487
    invoke-interface {v0, v1}, Lcom/garmin/fit/PadMesgListener;->onMesg(Lcom/garmin/fit/PadMesg;)V

    goto :goto_2a

    .line 277
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_4
        0x2 -> :sswitch_8
        0x3 -> :sswitch_9
        0x4 -> :sswitch_a
        0x5 -> :sswitch_b
        0x6 -> :sswitch_c
        0x7 -> :sswitch_d
        0x8 -> :sswitch_f
        0x9 -> :sswitch_12
        0xa -> :sswitch_13
        0xc -> :sswitch_e
        0xf -> :sswitch_14
        0x12 -> :sswitch_16
        0x13 -> :sswitch_17
        0x14 -> :sswitch_19
        0x15 -> :sswitch_1a
        0x17 -> :sswitch_1b
        0x1a -> :sswitch_1f
        0x1b -> :sswitch_20
        0x1c -> :sswitch_21
        0x1e -> :sswitch_23
        0x1f -> :sswitch_1d
        0x20 -> :sswitch_1e
        0x21 -> :sswitch_22
        0x22 -> :sswitch_15
        0x23 -> :sswitch_2
        0x25 -> :sswitch_5
        0x26 -> :sswitch_6
        0x27 -> :sswitch_7
        0x31 -> :sswitch_1
        0x33 -> :sswitch_24
        0x35 -> :sswitch_10
        0x37 -> :sswitch_26
        0x4e -> :sswitch_1c
        0x65 -> :sswitch_18
        0x67 -> :sswitch_25
        0x69 -> :sswitch_28
        0x6a -> :sswitch_3
        0x83 -> :sswitch_11
        0x91 -> :sswitch_27
    .end sparse-switch
.end method

.method public run(Ljava/io/InputStream;)V
    .locals 1

    .prologue
    .line 129
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    if-lez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->decode:Lcom/garmin/fit/Decode;

    invoke-virtual {v0, p1, p0}, Lcom/garmin/fit/Decode;->read(Ljava/io/InputStream;Lcom/garmin/fit/MesgListener;)Z

    .line 131
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->decode:Lcom/garmin/fit/Decode;

    invoke-virtual {v0}, Lcom/garmin/fit/Decode;->nextFile()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    .line 135
    :cond_0
    return-void
.end method

.method public setSystemTimeOffset(J)V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/garmin/fit/MesgBroadcaster;->decode:Lcom/garmin/fit/Decode;

    invoke-virtual {v0, p1, p2}, Lcom/garmin/fit/Decode;->setSystemTimeOffset(J)V

    .line 125
    return-void
.end method
