.class public Lcom/garmin/fit/BloodPressureMesg;
.super Lcom/garmin/fit/Mesg;
.source "BloodPressureMesg.java"


# static fields
.field protected static final bloodPressureMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/16 v11, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const/4 v9, 0x0

    .line 26
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string v1, "blood_pressure"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    .line 27
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "timestamp"

    const/16 v2, 0xfd

    const/16 v3, 0x86

    const-string/jumbo v8, "s"

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 29
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "systolic_pressure"

    const-string/jumbo v8, "mmHg"

    move v2, v9

    move v3, v11

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "diastolic_pressure"

    const/4 v2, 0x1

    const-string/jumbo v8, "mmHg"

    move v3, v11

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "mean_arterial_pressure"

    const-string/jumbo v8, "mmHg"

    move v2, v12

    move v3, v11

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "map_3_sample_mean"

    const/4 v2, 0x3

    const-string/jumbo v8, "mmHg"

    move v3, v11

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "map_morning_values"

    const/4 v2, 0x4

    const-string/jumbo v8, "mmHg"

    move v3, v11

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 39
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "map_evening_values"

    const/4 v2, 0x5

    const-string/jumbo v8, "mmHg"

    move v3, v11

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 41
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "heart_rate"

    const/4 v2, 0x6

    const-string v8, "bpm"

    move v3, v12

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 43
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "heart_rate_type"

    const/4 v2, 0x7

    const-string v8, ""

    move v3, v9

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 45
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "status"

    const/16 v2, 0x8

    const-string v8, ""

    move v3, v9

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 47
    sget-object v10, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "user_profile_index"

    const/16 v2, 0x9

    const-string v8, ""

    move v3, v11

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 49
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const/16 v0, 0x33

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 57
    return-void
.end method


# virtual methods
.method public getDiastolicPressure()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 207
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getHeartRateType()Lcom/garmin/fit/HrType;
    .locals 3

    .prologue
    .line 226
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 227
    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x0

    .line 229
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/HrType;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/HrType;

    move-result-object v0

    goto :goto_0
.end method

.method public getMap3SampleMean()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 147
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMapEveningValues()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 187
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMapMorningValues()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 167
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMeanArterialPressure()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 127
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getStatus()Lcom/garmin/fit/BpStatus;
    .locals 3

    .prologue
    .line 247
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 248
    if-nez v0, :cond_0

    .line 249
    const/4 v0, 0x0

    .line 250
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/BpStatus;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/BpStatus;

    move-result-object v0

    goto :goto_0
.end method

.method public getSystolicPressure()Ljava/lang/Integer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, v0}, Lcom/garmin/fit/BloodPressureMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTimestamp()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 67
    const/16 v0, 0xfd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/BloodPressureMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public getUserProfileIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 269
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BloodPressureMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public setDiastolicPressure(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 117
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 118
    return-void
.end method

.method public setHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 217
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 218
    return-void
.end method

.method public setHeartRateType(Lcom/garmin/fit/HrType;)V
    .locals 4

    .prologue
    .line 238
    const/4 v0, 0x7

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/HrType;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 239
    return-void
.end method

.method public setMap3SampleMean(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 157
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 158
    return-void
.end method

.method public setMapEveningValues(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 197
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 198
    return-void
.end method

.method public setMapMorningValues(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 177
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 178
    return-void
.end method

.method public setMeanArterialPressure(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 137
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 138
    return-void
.end method

.method public setStatus(Lcom/garmin/fit/BpStatus;)V
    .locals 4

    .prologue
    .line 259
    const/16 v0, 0x8

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/BpStatus;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 260
    return-void
.end method

.method public setSystolicPressure(Ljava/lang/Integer;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, p1, v0}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 98
    return-void
.end method

.method public setTimestamp(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 77
    const/16 v0, 0xfd

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 78
    return-void
.end method

.method public setUserProfileIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 279
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BloodPressureMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 280
    return-void
.end method
