.class public Lcom/garmin/fit/Decode;
.super Ljava/lang/Object;
.source "Decode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/garmin/fit/Decode$1;,
        Lcom/garmin/fit/Decode$Accumulator;,
        Lcom/garmin/fit/Decode$AccumulatedField;,
        Lcom/garmin/fit/Decode$STATE;,
        Lcom/garmin/fit/Decode$RETURN;
    }
.end annotation


# static fields
.field private static final DECODE_DATA_RECORDS_ONLY:J = 0x7fffffffffffffffL


# instance fields
.field private accumulator:Lcom/garmin/fit/Decode$Accumulator;

.field private crc:I

.field private fieldBytesLeft:I

.field private fieldData:[B

.field private fieldDataIndex:I

.field private fieldIndex:I

.field private fileBytesLeft:J

.field private fileDataSize:J

.field private fileHdrOffset:B

.field private fileHdrSize:B

.field private in:Ljava/io/InputStream;

.field private instreamIsComplete:Z

.field private lastTimeOffset:I

.field private localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

.field private localMesgIndex:I

.field private mesg:Lcom/garmin/fit/Mesg;

.field private mesgDefListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/MesgDefinitionListener;",
            ">;"
        }
    .end annotation
.end field

.field private mesgListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/MesgListener;",
            ">;"
        }
    .end annotation
.end field

.field private numFields:I

.field private pause:Z

.field private state:Lcom/garmin/fit/Decode$STATE;

.field private systemTimeOffset:J

.field private timestamp:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/garmin/fit/MesgDefinition;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    .line 131
    const/16 v0, 0xff

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/garmin/fit/Decode;->fieldData:[B

    .line 135
    new-instance v0, Lcom/garmin/fit/Decode$Accumulator;

    invoke-direct {v0, p0}, Lcom/garmin/fit/Decode$Accumulator;-><init>(Lcom/garmin/fit/Decode;)V

    iput-object v0, p0, Lcom/garmin/fit/Decode;->accumulator:Lcom/garmin/fit/Decode$Accumulator;

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Decode;->mesgListeners:Ljava/util/ArrayList;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Decode;->mesgDefListeners:Ljava/util/ArrayList;

    .line 145
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->systemTimeOffset:J

    .line 146
    invoke-virtual {p0}, Lcom/garmin/fit/Decode;->nextFile()V

    .line 148
    sget-boolean v0, Lcom/garmin/fit/Fit;->debug:Z

    if-eqz v0, :cond_0

    .line 149
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Fit.Decode: Starting decode...\n"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    .line 150
    :cond_0
    return-void
.end method

.method public static checkIntegrity(Ljava/io/InputStream;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 368
    new-instance v1, Lcom/garmin/fit/Decode;

    invoke-direct {v1}, Lcom/garmin/fit/Decode;-><init>()V

    .line 372
    :pswitch_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    if-ltz v2, :cond_0

    .line 373
    sget-object v3, Lcom/garmin/fit/Decode$1;->$SwitchMap$com$garmin$fit$Decode$RETURN:[I

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Lcom/garmin/fit/Decode;->read(B)Lcom/garmin/fit/Decode$RETURN;

    move-result-object v2

    invoke-virtual {v2}, Lcom/garmin/fit/Decode$RETURN;->ordinal()I

    move-result v2

    aget v2, v3, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/garmin/fit/FitRuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    packed-switch v2, :pswitch_data_0

    .line 393
    :cond_0
    :goto_0
    return v0

    .line 380
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 386
    :catch_0
    move-exception v0

    .line 387
    new-instance v1, Lcom/garmin/fit/FitRuntimeException;

    invoke-direct {v1, v0}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 388
    :catch_1
    move-exception v1

    goto :goto_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static isFit(Ljava/io/InputStream;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 329
    new-instance v2, Lcom/garmin/fit/Decode;

    invoke-direct {v2}, Lcom/garmin/fit/Decode;-><init>()V

    .line 333
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v3

    if-ltz v3, :cond_1

    .line 334
    sget-object v4, Lcom/garmin/fit/Decode$1;->$SwitchMap$com$garmin$fit$Decode$RETURN:[I

    int-to-byte v3, v3

    invoke-virtual {v2, v3}, Lcom/garmin/fit/Decode;->read(B)Lcom/garmin/fit/Decode$RETURN;

    move-result-object v3

    invoke-virtual {v3}, Lcom/garmin/fit/Decode$RETURN;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 356
    :goto_0
    :pswitch_0
    return v0

    .line 347
    :pswitch_1
    iget-object v3, v2, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    sget-object v4, Lcom/garmin/fit/Decode$STATE;->FILE_HDR:Lcom/garmin/fit/Decode$STATE;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/garmin/fit/FitRuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    if-eq v3, v4, :cond_0

    goto :goto_0

    .line 350
    :catch_0
    move-exception v0

    .line 351
    new-instance v1, Lcom/garmin/fit/FitRuntimeException;

    invoke-direct {v1, v0}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 352
    :catch_1
    move-exception v0

    :cond_1
    move v0, v1

    .line 356
    goto :goto_0

    .line 334
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public addListener(Lcom/garmin/fit/MesgDefinitionListener;)V
    .locals 1

    .prologue
    .line 168
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesgDefListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesgDefListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    :cond_0
    return-void
.end method

.method public addListener(Lcom/garmin/fit/MesgListener;)V
    .locals 1

    .prologue
    .line 163
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesgListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesgListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    :cond_0
    return-void
.end method

.method protected expandComponents(Lcom/garmin/fit/Field;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/garmin/fit/Field;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/FieldComponent;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 693
    move v7, v0

    move v8, v0

    .line 696
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 697
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/garmin/fit/FieldComponent;

    .line 699
    iget v0, v6, Lcom/garmin/fit/FieldComponent;->fieldNum:I

    const/16 v1, 0xff

    if-eq v0, v1, :cond_2

    .line 700
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v0, v0, Lcom/garmin/fit/Mesg;->num:I

    iget v1, v6, Lcom/garmin/fit/FieldComponent;->fieldNum:I

    invoke-static {v0, v1}, Lcom/garmin/fit/Factory;->createField(II)Lcom/garmin/fit/Field;

    move-result-object v9

    .line 704
    iget v0, v6, Lcom/garmin/fit/FieldComponent;->bits:I

    invoke-virtual {v9}, Lcom/garmin/fit/Field;->isSignedInteger()Z

    move-result v1

    invoke-virtual {p1, v8, v0, v1}, Lcom/garmin/fit/Field;->getBitsValue(IIZ)Ljava/lang/Long;

    move-result-object v3

    .line 706
    if-nez v3, :cond_1

    .line 724
    :cond_0
    return-void

    .line 709
    :cond_1
    iget-boolean v0, v6, Lcom/garmin/fit/FieldComponent;->accumulate:Z

    if-eqz v0, :cond_4

    .line 710
    iget-object v0, p0, Lcom/garmin/fit/Decode;->accumulator:Lcom/garmin/fit/Decode$Accumulator;

    iget-object v1, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v1, v1, Lcom/garmin/fit/Mesg;->num:I

    iget v2, v6, Lcom/garmin/fit/FieldComponent;->fieldNum:I

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget v5, v6, Lcom/garmin/fit/FieldComponent;->bits:I

    invoke-virtual/range {v0 .. v5}, Lcom/garmin/fit/Decode$Accumulator;->accumulate(IIJI)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 712
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-double v0, v0

    iget-wide v2, v6, Lcom/garmin/fit/FieldComponent;->scale:D

    div-double/2addr v0, v2

    iget-wide v2, v6, Lcom/garmin/fit/FieldComponent;->offset:D

    sub-double/2addr v0, v2

    iget-wide v2, v9, Lcom/garmin/fit/Field;->offset:D

    add-double/2addr v0, v2

    iget-wide v2, v9, Lcom/garmin/fit/Field;->scale:D

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 714
    iget-object v1, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v2, v9, Lcom/garmin/fit/Field;->num:I

    invoke-virtual {v1, v2}, Lcom/garmin/fit/Mesg;->hasField(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 715
    iget-object v1, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v2, v9, Lcom/garmin/fit/Field;->num:I

    invoke-virtual {v1, v2}, Lcom/garmin/fit/Mesg;->getField(I)Lcom/garmin/fit/Field;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/garmin/fit/Field;->addValue(Ljava/lang/Object;)V

    .line 722
    :cond_2
    :goto_2
    iget v0, v6, Lcom/garmin/fit/FieldComponent;->bits:I

    add-int v1, v8, v0

    .line 696
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v8, v1

    goto :goto_0

    .line 718
    :cond_3
    invoke-virtual {v9, v0}, Lcom/garmin/fit/Field;->addValue(Ljava/lang/Object;)V

    .line 719
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    invoke-virtual {v0, v9}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    goto :goto_2

    :cond_4
    move-object v0, v3

    goto :goto_1
.end method

.method public getMesg()Lcom/garmin/fit/Mesg;
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    return-object v0
.end method

.method public incompleteStream()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/garmin/fit/Decode;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 210
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "Can\'t set incompleteStream option after Decode started!"

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/garmin/fit/Decode;->instreamIsComplete:Z

    .line 215
    return-void
.end method

.method public nextFile()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 153
    const-wide/16 v0, 0x3

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    .line 154
    iput-byte v2, p0, Lcom/garmin/fit/Decode;->fileHdrOffset:B

    .line 155
    iput v2, p0, Lcom/garmin/fit/Decode;->crc:I

    .line 156
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FILE_HDR:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    .line 157
    iput v2, p0, Lcom/garmin/fit/Decode;->lastTimeOffset:I

    .line 158
    iput-boolean v2, p0, Lcom/garmin/fit/Decode;->pause:Z

    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/garmin/fit/Decode;->instreamIsComplete:Z

    .line 160
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/garmin/fit/Decode;->pause:Z

    .line 238
    return-void
.end method

.method public read(B)Lcom/garmin/fit/Decode$RETURN;
    .locals 12

    .prologue
    const/16 v11, 0xfd

    const/16 v7, 0x10

    const-wide/16 v5, 0x1

    const/4 v4, 0x1

    const/4 v9, 0x0

    .line 397
    sget-boolean v0, Lcom/garmin/fit/Fit;->debug:Z

    if-eqz v0, :cond_1

    .line 398
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 399
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Fit.Decode: Expecting next 2 bytes to be end of file CRC = 0x%04X\n"

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/garmin/fit/Decode;->crc:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    .line 401
    :cond_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Fit.Decode: 0x%02X - %s\n"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    and-int/lit16 v3, p1, 0xff

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v9

    iget-object v3, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    invoke-virtual {v3}, Lcom/garmin/fit/Decode$STATE;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    .line 405
    :cond_1
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_5

    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 406
    iget v0, p0, Lcom/garmin/fit/Decode;->crc:I

    invoke-static {v0, p1}, Lcom/garmin/fit/CRC;->get16(IB)I

    move-result v0

    iput v0, p0, Lcom/garmin/fit/Decode;->crc:I

    .line 408
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    sub-long/2addr v0, v5

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    .line 410
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    cmp-long v0, v0, v5

    if-nez v0, :cond_3

    .line 411
    iget-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    sget-object v1, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    if-eq v0, v1, :cond_2

    .line 412
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: Decoder not in correct state after last data byte in file.  Check message definitions."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_2
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->CONTINUE:Lcom/garmin/fit/Decode$RETURN;

    .line 688
    :goto_0
    return-object v0

    .line 415
    :cond_3
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    .line 416
    iget v0, p0, Lcom/garmin/fit/Decode;->crc:I

    if-eqz v0, :cond_4

    .line 417
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: File CRC failed."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419
    :cond_4
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->END_OF_FILE:Lcom/garmin/fit/Decode$RETURN;

    goto :goto_0

    .line 423
    :cond_5
    sget-object v0, Lcom/garmin/fit/Decode$1;->$SwitchMap$com$garmin$fit$Decode$STATE:[I

    iget-object v1, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    invoke-virtual {v1}, Lcom/garmin/fit/Decode$STATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 688
    :cond_6
    :goto_1
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->CONTINUE:Lcom/garmin/fit/Decode$RETURN;

    goto :goto_0

    .line 425
    :pswitch_0
    iget-byte v0, p0, Lcom/garmin/fit/Decode;->fileHdrOffset:B

    add-int/lit8 v1, v0, 0x1

    int-to-byte v1, v1

    iput-byte v1, p0, Lcom/garmin/fit/Decode;->fileHdrOffset:B

    packed-switch v0, :pswitch_data_1

    .line 466
    :cond_7
    :goto_2
    :pswitch_1
    iget-byte v0, p0, Lcom/garmin/fit/Decode;->fileHdrOffset:B

    iget-byte v1, p0, Lcom/garmin/fit/Decode;->fileHdrSize:B

    if-ne v0, v1, :cond_6

    .line 467
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    .line 468
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto :goto_1

    .line 427
    :pswitch_2
    iput-byte p1, p0, Lcom/garmin/fit/Decode;->fileHdrSize:B

    .line 428
    iget-byte v0, p0, Lcom/garmin/fit/Decode;->fileHdrSize:B

    add-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    goto :goto_2

    .line 431
    :pswitch_3
    and-int/lit16 v0, p1, 0xf0

    if-le v0, v7, :cond_7

    .line 432
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIT decode error: Protocol version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit16 v2, p1, 0xf0

    shr-int/lit8 v2, v2, 0x4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit8 v2, p1, 0xf

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported.  Must be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".15 or earlier."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 435
    :pswitch_4
    and-int/lit16 v0, p1, 0xff

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    goto :goto_2

    .line 438
    :pswitch_5
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    and-int/lit16 v2, p1, 0xff

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    goto :goto_2

    .line 441
    :pswitch_6
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    and-int/lit16 v2, p1, 0xff

    int-to-long v2, v2

    shl-long/2addr v2, v7

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    goto :goto_2

    .line 444
    :pswitch_7
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    and-int/lit16 v2, p1, 0xff

    int-to-long v2, v2

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileDataSize:J

    goto :goto_2

    .line 447
    :pswitch_8
    const/16 v0, 0x2e

    if-eq p1, v0, :cond_7

    .line 448
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: File is not FIT format.  Check file header data type."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451
    :pswitch_9
    const/16 v0, 0x46

    if-eq p1, v0, :cond_7

    .line 452
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: File is not FIT format.  Check file header data type."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 455
    :pswitch_a
    const/16 v0, 0x49

    if-eq p1, v0, :cond_7

    .line 456
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: File is not FIT format.  Check file header data type."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 459
    :pswitch_b
    const/16 v0, 0x54

    if-eq p1, v0, :cond_7

    .line 460
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: File is not FIT format.  Check file header data type."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :pswitch_c
    iput v9, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    .line 474
    iput v9, p0, Lcom/garmin/fit/Decode;->fieldBytesLeft:I

    .line 476
    iget-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    cmp-long v0, v0, v5

    if-lez v0, :cond_e

    .line 477
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_a

    .line 479
    and-int/lit8 v0, p1, 0x1f

    .line 481
    and-int/lit8 v1, p1, 0x60

    shr-int/lit8 v1, v1, 0x5

    iput v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    .line 483
    iget-object v1, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v1, v1, v2

    if-nez v1, :cond_8

    .line 484
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIT decode error: Missing message definition for local message number "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 486
    :cond_8
    iget-object v1, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/garmin/fit/MesgDefinition;->num:I

    invoke-static {v1, v11}, Lcom/garmin/fit/Factory;->createField(II)Lcom/garmin/fit/Field;

    move-result-object v1

    .line 487
    iget-wide v2, p0, Lcom/garmin/fit/Decode;->timestamp:J

    iget v4, p0, Lcom/garmin/fit/Decode;->lastTimeOffset:I

    sub-int v4, v0, v4

    and-int/lit8 v4, v4, 0x1f

    int-to-long v4, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/garmin/fit/Decode;->timestamp:J

    .line 488
    iput v0, p0, Lcom/garmin/fit/Decode;->lastTimeOffset:I

    .line 489
    iget-wide v2, p0, Lcom/garmin/fit/Decode;->timestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/garmin/fit/Field;->setValue(Ljava/lang/Object;)V

    .line 491
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v2

    iget v0, v0, Lcom/garmin/fit/MesgDefinition;->num:I

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    iput-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    .line 492
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    iput v2, v0, Lcom/garmin/fit/Mesg;->localNum:I

    .line 493
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-wide v2, p0, Lcom/garmin/fit/Decode;->systemTimeOffset:J

    iput-wide v2, v0, Lcom/garmin/fit/Mesg;->systemTimeOffset:J

    .line 494
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 496
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 497
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->MESG:Lcom/garmin/fit/Decode$RETURN;

    goto/16 :goto_0

    .line 499
    :cond_9
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_DATA:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 501
    :cond_a
    and-int/lit8 v0, p1, 0xf

    iput v0, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    .line 503
    and-int/lit8 v0, p1, 0x40

    if-eqz v0, :cond_b

    .line 504
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    new-instance v2, Lcom/garmin/fit/MesgDefinition;

    invoke-direct {v2}, Lcom/garmin/fit/MesgDefinition;-><init>()V

    aput-object v2, v0, v1

    .line 505
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    iput v1, v0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    .line 506
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->RESERVED1:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 508
    :cond_b
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    if-nez v0, :cond_c

    .line 509
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIT decode error: Missing message definition for local message number "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 511
    :cond_c
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/garmin/fit/MesgDefinition;->num:I

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    iput-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    .line 512
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    iput v1, v0, Lcom/garmin/fit/Mesg;->localNum:I

    .line 513
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-wide v1, p0, Lcom/garmin/fit/Decode;->systemTimeOffset:J

    iput-wide v1, v0, Lcom/garmin/fit/Mesg;->systemTimeOffset:J

    .line 515
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_d

    .line 516
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->MESG:Lcom/garmin/fit/Decode$RETURN;

    goto/16 :goto_0

    .line 518
    :cond_d
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_DATA:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 523
    :cond_e
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FILE_CRC_HIGH:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 528
    :pswitch_d
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->ARCH:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 532
    :pswitch_e
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    and-int/lit16 v1, p1, 0xff

    iput v1, v0, Lcom/garmin/fit/MesgDefinition;->arch:I

    .line 533
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->MESG_NUM_0:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 539
    :pswitch_f
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    and-int/lit16 v1, p1, 0xff

    iput v1, v0, Lcom/garmin/fit/MesgDefinition;->num:I

    .line 540
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->MESG_NUM_1:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 544
    :pswitch_10
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/garmin/fit/MesgDefinition;->num:I

    and-int/lit16 v2, p1, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    iput v1, v0, Lcom/garmin/fit/MesgDefinition;->num:I

    .line 547
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/garmin/fit/MesgDefinition;->arch:I

    if-ne v0, v4, :cond_10

    .line 548
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/garmin/fit/MesgDefinition;->num:I

    shr-int/lit8 v1, v1, 0x8

    iget-object v2, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v3, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v2, v2, v3

    iget v2, v2, Lcom/garmin/fit/MesgDefinition;->num:I

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    iput v1, v0, Lcom/garmin/fit/MesgDefinition;->num:I

    .line 553
    :cond_f
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->NUM_FIELDS:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 549
    :cond_10
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/garmin/fit/MesgDefinition;->arch:I

    if-eqz v0, :cond_f

    .line 550
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIT decode error: Endian "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v3, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v2, v2, v3

    iget v2, v2, Lcom/garmin/fit/MesgDefinition;->arch:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 557
    :pswitch_11
    and-int/lit16 v0, p1, 0xff

    iput v0, p0, Lcom/garmin/fit/Decode;->numFields:I

    .line 559
    iget v0, p0, Lcom/garmin/fit/Decode;->numFields:I

    if-nez v0, :cond_11

    .line 560
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    .line 561
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->MESG_DEF:Lcom/garmin/fit/Decode$RETURN;

    goto/16 :goto_0

    .line 564
    :cond_11
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_NUM:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 568
    :pswitch_12
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    new-instance v1, Lcom/garmin/fit/FieldDefinition;

    invoke-direct {v1}, Lcom/garmin/fit/FieldDefinition;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    iget v1, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldDefinition;

    and-int/lit16 v1, p1, 0xff

    iput v1, v0, Lcom/garmin/fit/FieldDefinition;->num:I

    .line 570
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_SIZE:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 574
    :pswitch_13
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    iget v1, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldDefinition;

    and-int/lit16 v1, p1, 0xff

    iput v1, v0, Lcom/garmin/fit/FieldDefinition;->size:I

    .line 575
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_TYPE:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 579
    :pswitch_14
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    iget v1, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldDefinition;

    and-int/lit16 v1, p1, 0xff

    iput v1, v0, Lcom/garmin/fit/FieldDefinition;->type:I

    .line 581
    iget v0, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    iget v1, p0, Lcom/garmin/fit/Decode;->numFields:I

    if-lt v0, v1, :cond_12

    .line 582
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    .line 583
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->MESG_DEF:Lcom/garmin/fit/Decode$RETURN;

    goto/16 :goto_0

    .line 585
    :cond_12
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_NUM:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    goto/16 :goto_1

    .line 590
    :pswitch_15
    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    iget v1, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/garmin/fit/FieldDefinition;

    .line 592
    iget v0, p0, Lcom/garmin/fit/Decode;->fieldBytesLeft:I

    if-nez v0, :cond_13

    .line 593
    iput v9, p0, Lcom/garmin/fit/Decode;->fieldDataIndex:I

    .line 594
    iget v0, v10, Lcom/garmin/fit/FieldDefinition;->size:I

    iput v0, p0, Lcom/garmin/fit/Decode;->fieldBytesLeft:I

    .line 597
    :cond_13
    iget-object v0, p0, Lcom/garmin/fit/Decode;->fieldData:[B

    iget v1, p0, Lcom/garmin/fit/Decode;->fieldDataIndex:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/garmin/fit/Decode;->fieldDataIndex:I

    aput-byte p1, v0, v1

    .line 598
    iget v0, p0, Lcom/garmin/fit/Decode;->fieldBytesLeft:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/garmin/fit/Decode;->fieldBytesLeft:I

    .line 600
    iget v0, p0, Lcom/garmin/fit/Decode;->fieldBytesLeft:I

    if-nez v0, :cond_6

    .line 605
    iget v0, v10, Lcom/garmin/fit/FieldDefinition;->type:I

    and-int/lit8 v0, v0, 0x1f

    const/16 v1, 0xe

    if-ge v0, v1, :cond_18

    .line 606
    sget-object v0, Lcom/garmin/fit/Fit;->baseTypeSizes:[I

    iget v1, v10, Lcom/garmin/fit/FieldDefinition;->type:I

    and-int/lit8 v1, v1, 0x1f

    aget v2, v0, v1

    .line 607
    iget v0, v10, Lcom/garmin/fit/FieldDefinition;->size:I

    div-int v3, v0, v2

    .line 609
    iget v0, v10, Lcom/garmin/fit/FieldDefinition;->type:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/garmin/fit/MesgDefinition;->arch:I

    and-int/lit8 v0, v0, 0x1

    if-eq v0, v4, :cond_15

    move v1, v9

    .line 611
    :goto_3
    if-ge v1, v3, :cond_15

    move v0, v9

    .line 612
    :goto_4
    div-int/lit8 v4, v2, 0x2

    if-ge v0, v4, :cond_14

    .line 613
    iget-object v4, p0, Lcom/garmin/fit/Decode;->fieldData:[B

    mul-int v5, v1, v2

    add-int/2addr v5, v0

    aget-byte v4, v4, v5

    .line 614
    iget-object v5, p0, Lcom/garmin/fit/Decode;->fieldData:[B

    mul-int v6, v1, v2

    add-int/2addr v6, v0

    iget-object v7, p0, Lcom/garmin/fit/Decode;->fieldData:[B

    mul-int v8, v1, v2

    add-int/2addr v8, v2

    sub-int/2addr v8, v0

    add-int/lit8 v8, v8, -0x1

    aget-byte v7, v7, v8

    aput-byte v7, v5, v6

    .line 615
    iget-object v5, p0, Lcom/garmin/fit/Decode;->fieldData:[B

    mul-int v6, v1, v2

    add-int/2addr v6, v2

    sub-int/2addr v6, v0

    add-int/lit8 v6, v6, -0x1

    aput-byte v4, v5, v6

    .line 612
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 611
    :cond_14
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 620
    :cond_15
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v0, v0, Lcom/garmin/fit/Mesg;->num:I

    iget v1, v10, Lcom/garmin/fit/FieldDefinition;->num:I

    invoke-static {v0, v1}, Lcom/garmin/fit/Factory;->createField(II)Lcom/garmin/fit/Field;

    move-result-object v0

    .line 622
    if-eqz v0, :cond_18

    .line 625
    invoke-virtual {v0}, Lcom/garmin/fit/Field;->getName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "unknown"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 627
    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "unknown"

    iget v2, v10, Lcom/garmin/fit/FieldDefinition;->num:I

    iget v3, v10, Lcom/garmin/fit/FieldDefinition;->type:I

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    move-object v1, v0

    .line 629
    :goto_5
    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v2, p0, Lcom/garmin/fit/Decode;->fieldData:[B

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    iget v2, v10, Lcom/garmin/fit/FieldDefinition;->size:I

    invoke-virtual {v1, v0, v2}, Lcom/garmin/fit/Field;->read(Ljava/io/InputStream;I)Z

    .line 631
    iget v0, v10, Lcom/garmin/fit/FieldDefinition;->num:I

    if-ne v0, v11, :cond_16

    .line 632
    invoke-virtual {v1}, Lcom/garmin/fit/Field;->getLongValue()Ljava/lang/Long;

    move-result-object v0

    .line 634
    if-eqz v0, :cond_16

    .line 635
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/garmin/fit/Decode;->timestamp:J

    .line 636
    iget-wide v2, p0, Lcom/garmin/fit/Decode;->timestamp:J

    const-wide/16 v4, 0x1f

    and-long/2addr v2, v4

    long-to-int v0, v2

    iput v0, p0, Lcom/garmin/fit/Decode;->lastTimeOffset:I

    .line 640
    :cond_16
    invoke-virtual {v1}, Lcom/garmin/fit/Field;->getIsAccumulated()Z

    move-result v0

    if-eqz v0, :cond_17

    move v2, v9

    .line 642
    :goto_6
    invoke-virtual {v1}, Lcom/garmin/fit/Field;->getNumValues()I

    move-result v0

    if-ge v2, v0, :cond_17

    .line 643
    invoke-virtual {v1, v2}, Lcom/garmin/fit/Field;->getRawValue(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v3

    .line 644
    iget-object v0, p0, Lcom/garmin/fit/Decode;->accumulator:Lcom/garmin/fit/Decode$Accumulator;

    iget-object v5, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget v5, v5, Lcom/garmin/fit/Mesg;->num:I

    invoke-virtual {v1}, Lcom/garmin/fit/Field;->getNum()I

    move-result v6

    invoke-virtual {v0, v5, v6, v3, v4}, Lcom/garmin/fit/Decode$Accumulator;->set(IIJ)V

    .line 642
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 648
    :cond_17
    invoke-virtual {v1}, Lcom/garmin/fit/Field;->getNumValues()I

    move-result v0

    if-lez v0, :cond_18

    .line 649
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    invoke-virtual {v0, v1}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 654
    :cond_18
    iget v0, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    .line 656
    iget v0, p0, Lcom/garmin/fit/Decode;->fieldIndex:I

    iget-object v1, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v2, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_6

    .line 658
    :goto_7
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_1b

    .line 660
    iget-object v1, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    invoke-virtual {v0}, Lcom/garmin/fit/Field;->getNum()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/garmin/fit/Mesg;->GetActiveSubFieldIndex(I)I

    move-result v2

    .line 662
    const v0, 0xffff

    if-ne v2, v0, :cond_1a

    .line 663
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->components:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_19

    .line 665
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v1, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v1, v1, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/Field;

    iget-object v1, v1, Lcom/garmin/fit/Field;->components:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/Decode;->expandComponents(Lcom/garmin/fit/Field;Ljava/util/ArrayList;)V

    .line 658
    :cond_19
    :goto_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 669
    :cond_1a
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    iget-object v0, v0, Lcom/garmin/fit/SubField;->components:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_19

    .line 671
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v1, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    iget-object v1, v1, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/Field;

    iget-object v1, v1, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/SubField;

    iget-object v1, v1, Lcom/garmin/fit/SubField;->components:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/Decode;->expandComponents(Lcom/garmin/fit/Field;Ljava/util/ArrayList;)V

    goto :goto_8

    .line 677
    :cond_1b
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    .line 678
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->MESG:Lcom/garmin/fit/Decode$RETURN;

    goto/16 :goto_0

    :cond_1c
    move-object v1, v0

    goto/16 :goto_5

    .line 423
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch

    .line 425
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public read(Ljava/io/InputStream;)Z
    .locals 1

    .prologue
    .line 232
    iput-object p1, p0, Lcom/garmin/fit/Decode;->in:Ljava/io/InputStream;

    .line 233
    invoke-virtual {p0}, Lcom/garmin/fit/Decode;->resume()Z

    move-result v0

    return v0
.end method

.method public read(Ljava/io/InputStream;Lcom/garmin/fit/MesgListener;)Z
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Decode;->addListener(Lcom/garmin/fit/MesgListener;)V

    .line 228
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Decode;->read(Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method public resume()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 248
    iput-boolean v1, p0, Lcom/garmin/fit/Decode;->pause:Z

    .line 249
    sget-object v0, Lcom/garmin/fit/Decode$RETURN;->CONTINUE:Lcom/garmin/fit/Decode$RETURN;

    .line 252
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/garmin/fit/Decode;->in:Ljava/io/InputStream;

    invoke-virtual {v3}, Ljava/io/InputStream;->read()I

    move-result v3

    if-ltz v3, :cond_3

    .line 253
    iget-boolean v0, p0, Lcom/garmin/fit/Decode;->pause:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 311
    :goto_1
    return v0

    .line 256
    :cond_0
    int-to-byte v0, v3

    invoke-virtual {p0, v0}, Lcom/garmin/fit/Decode;->read(B)Lcom/garmin/fit/Decode$RETURN;

    move-result-object v3

    .line 258
    sget-object v0, Lcom/garmin/fit/Decode$1;->$SwitchMap$com$garmin$fit$Decode$RETURN:[I

    invoke-virtual {v3}, Lcom/garmin/fit/Decode$RETURN;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    .line 276
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FIT decode error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :catch_0
    move-exception v0

    .line 280
    new-instance v1, Lcom/garmin/fit/FitRuntimeException;

    invoke-direct {v1, v0}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/io/IOException;)V

    throw v1

    :pswitch_0
    move-object v0, v3

    .line 260
    goto :goto_0

    .line 263
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesgListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MesgListener;

    .line 264
    iget-object v5, p0, Lcom/garmin/fit/Decode;->mesg:Lcom/garmin/fit/Mesg;

    invoke-interface {v0, v5}, Lcom/garmin/fit/MesgListener;->onMesg(Lcom/garmin/fit/Mesg;)V

    goto :goto_2

    :cond_1
    move-object v0, v3

    .line 265
    goto :goto_0

    .line 268
    :pswitch_2
    iget-object v0, p0, Lcom/garmin/fit/Decode;->mesgDefListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MesgDefinitionListener;

    .line 269
    iget-object v5, p0, Lcom/garmin/fit/Decode;->localMesgDefs:[Lcom/garmin/fit/MesgDefinition;

    iget v6, p0, Lcom/garmin/fit/Decode;->localMesgIndex:I

    aget-object v5, v5, v6

    invoke-interface {v0, v5}, Lcom/garmin/fit/MesgDefinitionListener;->onMesgDefinition(Lcom/garmin/fit/MesgDefinition;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_2
    move-object v0, v3

    .line 270
    goto :goto_0

    :pswitch_3
    move v0, v2

    .line 273
    goto :goto_1

    .line 283
    :cond_3
    iget-boolean v3, p0, Lcom/garmin/fit/Decode;->instreamIsComplete:Z

    if-ne v3, v2, :cond_4

    iget-wide v3, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    const-wide v5, 0x7fffffffffffffffL

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    .line 286
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: Unexpected end of input stream."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_4
    iget-boolean v3, p0, Lcom/garmin/fit/Decode;->instreamIsComplete:Z

    if-nez v3, :cond_7

    .line 292
    sget-object v3, Lcom/garmin/fit/Decode$RETURN;->MESG:Lcom/garmin/fit/Decode$RETURN;

    if-eq v0, v3, :cond_5

    sget-object v3, Lcom/garmin/fit/Decode$RETURN;->MESG_DEF:Lcom/garmin/fit/Decode$RETURN;

    if-ne v0, v3, :cond_6

    :cond_5
    move v0, v2

    .line 295
    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 301
    goto/16 :goto_1

    .line 308
    :cond_7
    sget-object v1, Lcom/garmin/fit/Decode$RETURN;->MESG:Lcom/garmin/fit/Decode$RETURN;

    if-eq v0, v1, :cond_8

    sget-object v1, Lcom/garmin/fit/Decode$RETURN;->MESG_DEF:Lcom/garmin/fit/Decode$RETURN;

    if-ne v0, v1, :cond_9

    :cond_8
    move v0, v2

    .line 311
    goto/16 :goto_1

    .line 315
    :cond_9
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "FIT decode error: Unexpected end of input stream."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 258
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setSystemTimeOffset(J)V
    .locals 0

    .prologue
    .line 173
    iput-wide p1, p0, Lcom/garmin/fit/Decode;->systemTimeOffset:J

    .line 174
    return-void
.end method

.method public skipHeader()V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/garmin/fit/Decode;->in:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    .line 190
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "Can\'t set skipHeader option after Decode started!"

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    iput-object v0, p0, Lcom/garmin/fit/Decode;->state:Lcom/garmin/fit/Decode$STATE;

    .line 195
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/garmin/fit/Decode;->fileBytesLeft:J

    .line 196
    return-void
.end method
