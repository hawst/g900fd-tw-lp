.class public interface abstract Lcom/garmin/fit/MesgWithEvent;
.super Ljava/lang/Object;
.source "MesgWithEvent.java"


# virtual methods
.method public abstract getEvent()Lcom/garmin/fit/Event;
.end method

.method public abstract getEventGroup()Ljava/lang/Short;
.end method

.method public abstract getEventType()Lcom/garmin/fit/EventType;
.end method

.method public abstract getTimestamp()Lcom/garmin/fit/DateTime;
.end method

.method public abstract setEventType(Lcom/garmin/fit/EventType;)V
.end method

.method public abstract setTimestamp(Lcom/garmin/fit/DateTime;)V
.end method
