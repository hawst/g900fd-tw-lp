.class public Lcom/garmin/fit/Factory;
.super Ljava/lang/Object;
.source "Factory.java"


# static fields
.field private static final mesgs:[Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 171
    const/16 v0, 0x29

    new-array v0, v0, [Lcom/garmin/fit/Mesg;

    sput-object v0, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    .line 174
    const/4 v0, 0x0

    .line 175
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/FileIdMesg;->fileIdMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 176
    const/4 v0, 0x1

    .line 177
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/FileCreatorMesg;->fileCreatorMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 178
    const/4 v0, 0x2

    .line 179
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/SoftwareMesg;->softwareMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 180
    const/4 v0, 0x3

    .line 181
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/SlaveDeviceMesg;->slaveDeviceMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 182
    const/4 v0, 0x4

    .line 183
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/CapabilitiesMesg;->capabilitiesMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 184
    const/4 v0, 0x5

    .line 185
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/FileCapabilitiesMesg;->fileCapabilitiesMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 186
    const/4 v0, 0x6

    .line 187
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/MesgCapabilitiesMesg;->mesgCapabilitiesMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 188
    const/4 v0, 0x7

    .line 189
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/FieldCapabilitiesMesg;->fieldCapabilitiesMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 190
    const/16 v0, 0x8

    .line 191
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/DeviceSettingsMesg;->deviceSettingsMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 192
    const/16 v0, 0x9

    .line 193
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 194
    const/16 v0, 0xa

    .line 195
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/HrmProfileMesg;->hrmProfileMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 196
    const/16 v0, 0xb

    .line 197
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/SdmProfileMesg;->sdmProfileMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 198
    const/16 v0, 0xc

    .line 199
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 200
    const/16 v0, 0xd

    .line 201
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/ZonesTargetMesg;->zonesTargetMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 202
    const/16 v0, 0xe

    .line 203
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/SportMesg;->sportMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 204
    const/16 v0, 0xf

    .line 205
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/HrZoneMesg;->hrZoneMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 206
    const/16 v0, 0x10

    .line 207
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/SpeedZoneMesg;->speedZoneMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 208
    const/16 v0, 0x11

    .line 209
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/CadenceZoneMesg;->cadenceZoneMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 210
    const/16 v0, 0x12

    .line 211
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/PowerZoneMesg;->powerZoneMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 212
    const/16 v0, 0x13

    .line 213
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/MetZoneMesg;->metZoneMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 214
    const/16 v0, 0x14

    .line 215
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 216
    const/16 v0, 0x15

    .line 217
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/ActivityMesg;->activityMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 218
    const/16 v0, 0x16

    .line 219
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/SessionMesg;->sessionMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 220
    const/16 v0, 0x17

    .line 221
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 222
    const/16 v0, 0x18

    .line 223
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/LengthMesg;->lengthMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 224
    const/16 v0, 0x19

    .line 225
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/RecordMesg;->recordMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 226
    const/16 v0, 0x1a

    .line 227
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/EventMesg;->eventMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 228
    const/16 v0, 0x1b

    .line 229
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/DeviceInfoMesg;->deviceInfoMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 230
    const/16 v0, 0x1c

    .line 231
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/HrvMesg;->hrvMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 232
    const/16 v0, 0x1d

    .line 233
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/CourseMesg;->courseMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 234
    const/16 v0, 0x1e

    .line 235
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/CoursePointMesg;->coursePointMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 236
    const/16 v0, 0x1f

    .line 237
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/WorkoutMesg;->workoutMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 238
    const/16 v0, 0x20

    .line 239
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/WorkoutStepMesg;->workoutStepMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 240
    const/16 v0, 0x21

    .line 241
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/ScheduleMesg;->scheduleMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 242
    const/16 v0, 0x22

    .line 243
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 244
    const/16 v0, 0x23

    .line 245
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/WeightScaleMesg;->weightScaleMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 246
    const/16 v0, 0x24

    .line 247
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/BloodPressureMesg;->bloodPressureMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 248
    const/16 v0, 0x25

    .line 249
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/MonitoringInfoMesg;->monitoringInfoMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 250
    const/16 v0, 0x26

    .line 251
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/MonitoringMesg;->monitoringMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 252
    const/16 v0, 0x27

    .line 253
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/MemoGlobMesg;->memoGlobMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 254
    const/16 v0, 0x28

    .line 255
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/PadMesg;->padMesg:Lcom/garmin/fit/Mesg;

    aput-object v2, v1, v0

    .line 258
    return-void
.end method

.method public static createField(II)Lcom/garmin/fit/Field;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 148
    move v0, v3

    :goto_0
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 149
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/garmin/fit/Mesg;->num:I

    if-ne v1, p0, :cond_0

    .line 150
    new-instance v1, Lcom/garmin/fit/Field;

    sget-object v2, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    aget-object v0, v2, v0

    invoke-virtual {v0, p1}, Lcom/garmin/fit/Mesg;->getField(I)Lcom/garmin/fit/Field;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/garmin/fit/Field;-><init>(Lcom/garmin/fit/Field;)V

    move-object v0, v1

    .line 153
    :goto_1
    return-object v0

    .line 148
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    :cond_1
    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "unknown"

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    move v2, p1

    move v9, v3

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    goto :goto_1
.end method

.method public static createField(ILjava/lang/String;)Lcom/garmin/fit/Field;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 157
    move v0, v3

    :goto_0
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 158
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/garmin/fit/Mesg;->num:I

    if-ne v1, p0, :cond_0

    .line 159
    new-instance v1, Lcom/garmin/fit/Field;

    sget-object v2, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    aget-object v0, v2, v0

    invoke-virtual {v0, p1, v3}, Lcom/garmin/fit/Mesg;->getField(Ljava/lang/String;Z)Lcom/garmin/fit/Field;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/garmin/fit/Field;-><init>(Lcom/garmin/fit/Field;)V

    move-object v0, v1

    .line 162
    :goto_1
    return-object v0

    .line 157
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 162
    :cond_1
    new-instance v0, Lcom/garmin/fit/Field;

    const/16 v2, 0xff

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    move-object v1, p1

    move v9, v3

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    goto :goto_1
.end method

.method public static createMesg(I)Lcom/garmin/fit/Mesg;
    .locals 3

    .prologue
    .line 114
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 115
    sget-object v1, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/garmin/fit/Mesg;->num:I

    if-ne v1, p0, :cond_0

    .line 116
    new-instance v1, Lcom/garmin/fit/Mesg;

    sget-object v2, Lcom/garmin/fit/Factory;->mesgs:[Lcom/garmin/fit/Mesg;

    aget-object v0, v2, v0

    invoke-direct {v1, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    move-object v0, v1

    .line 118
    :goto_1
    return-object v0

    .line 114
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_1
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string/jumbo v1, "unknown"

    invoke-direct {v0, v1, p0}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public static createMesg(Lcom/garmin/fit/Mesg;)Lcom/garmin/fit/Mesg;
    .locals 3

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/garmin/fit/Mesg;->getNum()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 110
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string/jumbo v1, "unknown"

    sget v2, Lcom/garmin/fit/MesgNum;->INVALID:I

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    :goto_0
    return-object v0

    .line 26
    :sswitch_0
    new-instance v0, Lcom/garmin/fit/FileIdMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/FileIdMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 28
    :sswitch_1
    new-instance v0, Lcom/garmin/fit/FileCreatorMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/FileCreatorMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 30
    :sswitch_2
    new-instance v0, Lcom/garmin/fit/SoftwareMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/SoftwareMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 32
    :sswitch_3
    new-instance v0, Lcom/garmin/fit/SlaveDeviceMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/SlaveDeviceMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 34
    :sswitch_4
    new-instance v0, Lcom/garmin/fit/CapabilitiesMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/CapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 36
    :sswitch_5
    new-instance v0, Lcom/garmin/fit/FileCapabilitiesMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/FileCapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 38
    :sswitch_6
    new-instance v0, Lcom/garmin/fit/MesgCapabilitiesMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/MesgCapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 40
    :sswitch_7
    new-instance v0, Lcom/garmin/fit/FieldCapabilitiesMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/FieldCapabilitiesMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 42
    :sswitch_8
    new-instance v0, Lcom/garmin/fit/DeviceSettingsMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/DeviceSettingsMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 44
    :sswitch_9
    new-instance v0, Lcom/garmin/fit/UserProfileMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/UserProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 46
    :sswitch_a
    new-instance v0, Lcom/garmin/fit/HrmProfileMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/HrmProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 48
    :sswitch_b
    new-instance v0, Lcom/garmin/fit/SdmProfileMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/SdmProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 50
    :sswitch_c
    new-instance v0, Lcom/garmin/fit/BikeProfileMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/BikeProfileMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 52
    :sswitch_d
    new-instance v0, Lcom/garmin/fit/ZonesTargetMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/ZonesTargetMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 54
    :sswitch_e
    new-instance v0, Lcom/garmin/fit/SportMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/SportMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 56
    :sswitch_f
    new-instance v0, Lcom/garmin/fit/HrZoneMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/HrZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 58
    :sswitch_10
    new-instance v0, Lcom/garmin/fit/SpeedZoneMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/SpeedZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 60
    :sswitch_11
    new-instance v0, Lcom/garmin/fit/CadenceZoneMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/CadenceZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 62
    :sswitch_12
    new-instance v0, Lcom/garmin/fit/PowerZoneMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/PowerZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 64
    :sswitch_13
    new-instance v0, Lcom/garmin/fit/MetZoneMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/MetZoneMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 66
    :sswitch_14
    new-instance v0, Lcom/garmin/fit/GoalMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/GoalMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 68
    :sswitch_15
    new-instance v0, Lcom/garmin/fit/ActivityMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/ActivityMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 70
    :sswitch_16
    new-instance v0, Lcom/garmin/fit/SessionMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/SessionMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 72
    :sswitch_17
    new-instance v0, Lcom/garmin/fit/LapMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/LapMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 74
    :sswitch_18
    new-instance v0, Lcom/garmin/fit/LengthMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/LengthMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 76
    :sswitch_19
    new-instance v0, Lcom/garmin/fit/RecordMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/RecordMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 78
    :sswitch_1a
    new-instance v0, Lcom/garmin/fit/EventMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/EventMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 80
    :sswitch_1b
    new-instance v0, Lcom/garmin/fit/DeviceInfoMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/DeviceInfoMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 82
    :sswitch_1c
    new-instance v0, Lcom/garmin/fit/HrvMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/HrvMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 84
    :sswitch_1d
    new-instance v0, Lcom/garmin/fit/CourseMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/CourseMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 86
    :sswitch_1e
    new-instance v0, Lcom/garmin/fit/CoursePointMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/CoursePointMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 88
    :sswitch_1f
    new-instance v0, Lcom/garmin/fit/WorkoutMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/WorkoutMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 90
    :sswitch_20
    new-instance v0, Lcom/garmin/fit/WorkoutStepMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/WorkoutStepMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 92
    :sswitch_21
    new-instance v0, Lcom/garmin/fit/ScheduleMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/ScheduleMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 94
    :sswitch_22
    new-instance v0, Lcom/garmin/fit/TotalsMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/TotalsMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 96
    :sswitch_23
    new-instance v0, Lcom/garmin/fit/WeightScaleMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/WeightScaleMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 98
    :sswitch_24
    new-instance v0, Lcom/garmin/fit/BloodPressureMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/BloodPressureMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 100
    :sswitch_25
    new-instance v0, Lcom/garmin/fit/MonitoringInfoMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/MonitoringInfoMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 102
    :sswitch_26
    new-instance v0, Lcom/garmin/fit/MonitoringMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/MonitoringMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 104
    :sswitch_27
    new-instance v0, Lcom/garmin/fit/MemoGlobMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/MemoGlobMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 106
    :sswitch_28
    new-instance v0, Lcom/garmin/fit/PadMesg;

    invoke-direct {v0, p0}, Lcom/garmin/fit/PadMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    goto/16 :goto_0

    .line 24
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_4
        0x2 -> :sswitch_8
        0x3 -> :sswitch_9
        0x4 -> :sswitch_a
        0x5 -> :sswitch_b
        0x6 -> :sswitch_c
        0x7 -> :sswitch_d
        0x8 -> :sswitch_f
        0x9 -> :sswitch_12
        0xa -> :sswitch_13
        0xc -> :sswitch_e
        0xf -> :sswitch_14
        0x12 -> :sswitch_16
        0x13 -> :sswitch_17
        0x14 -> :sswitch_19
        0x15 -> :sswitch_1a
        0x17 -> :sswitch_1b
        0x1a -> :sswitch_1f
        0x1b -> :sswitch_20
        0x1c -> :sswitch_21
        0x1e -> :sswitch_23
        0x1f -> :sswitch_1d
        0x20 -> :sswitch_1e
        0x21 -> :sswitch_22
        0x22 -> :sswitch_15
        0x23 -> :sswitch_2
        0x25 -> :sswitch_5
        0x26 -> :sswitch_6
        0x27 -> :sswitch_7
        0x31 -> :sswitch_1
        0x33 -> :sswitch_24
        0x35 -> :sswitch_10
        0x37 -> :sswitch_26
        0x4e -> :sswitch_1c
        0x65 -> :sswitch_18
        0x67 -> :sswitch_25
        0x69 -> :sswitch_28
        0x6a -> :sswitch_3
        0x83 -> :sswitch_11
        0x91 -> :sswitch_27
    .end sparse-switch
.end method
