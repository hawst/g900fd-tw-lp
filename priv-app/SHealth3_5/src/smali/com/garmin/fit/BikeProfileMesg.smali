.class public Lcom/garmin/fit/BikeProfileMesg;
.super Lcom/garmin/fit/Mesg;
.source "BikeProfileMesg.java"


# static fields
.field protected static final bikeProfileMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    .line 26
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string v1, "bike_profile"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    .line 27
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "message_index"

    const/16 v2, 0xfe

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 29
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "name"

    const/4 v2, 0x0

    const/4 v3, 0x7

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "sport"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "sub_sport"

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "odometer"

    const/4 v2, 0x3

    const/16 v3, 0x86

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_spd_ant_id"

    const/4 v2, 0x4

    const/16 v3, 0x8b

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 39
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_cad_ant_id"

    const/4 v2, 0x5

    const/16 v3, 0x8b

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 41
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_spdcad_ant_id"

    const/4 v2, 0x6

    const/16 v3, 0x8b

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 43
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_power_ant_id"

    const/4 v2, 0x7

    const/16 v3, 0x8b

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 45
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "custom_wheelsize"

    const/16 v2, 0x8

    const/16 v3, 0x84

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 47
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "auto_wheelsize"

    const/16 v2, 0x9

    const/16 v3, 0x84

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 49
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_weight"

    const/16 v2, 0xa

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "kg"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 51
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "power_cal_factor"

    const/16 v2, 0xb

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 53
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "auto_wheel_cal"

    const/16 v2, 0xc

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 55
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "auto_power_zero"

    const/16 v2, 0xd

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 57
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "id"

    const/16 v2, 0xe

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 59
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "spd_enabled"

    const/16 v2, 0xf

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 61
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "cad_enabled"

    const/16 v2, 0x10

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 63
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "spdcad_enabled"

    const/16 v2, 0x11

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 65
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "power_enabled"

    const/16 v2, 0x12

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 67
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "crank_length"

    const/16 v2, 0x13

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide v6, -0x3fa4800000000000L    # -110.0

    const-string/jumbo v8, "mm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 69
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "enabled"

    const/16 v2, 0x14

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 71
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_spd_ant_id_trans_type"

    const/16 v2, 0x15

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 73
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_cad_ant_id_trans_type"

    const/16 v2, 0x16

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 75
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_spdcad_ant_id_trans_type"

    const/16 v2, 0x17

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 77
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "bike_power_ant_id_trans_type"

    const/16 v2, 0x18

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 79
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "odometer_rollover"

    const/16 v2, 0x25

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 81
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "front_gear_num"

    const/16 v2, 0x26

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 83
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "front_gear"

    const/16 v2, 0x27

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 85
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "rear_gear_num"

    const/16 v2, 0x28

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 87
    sget-object v10, Lcom/garmin/fit/BikeProfileMesg;->bikeProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string/jumbo v1, "rear_gear"

    const/16 v2, 0x29

    const/16 v3, 0xa

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 89
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 97
    return-void
.end method


# virtual methods
.method public getAutoPowerZero()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 377
    const/16 v0, 0xd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 378
    if-nez v0, :cond_0

    .line 379
    const/4 v0, 0x0

    .line 380
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getAutoWheelCal()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 356
    const/16 v0, 0xc

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 357
    if-nez v0, :cond_0

    .line 358
    const/4 v0, 0x0

    .line 359
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getAutoWheelsize()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 297
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getBikeCadAntId()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 222
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getBikeCadAntIdTransType()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 559
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getBikePowerAntId()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 258
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getBikePowerAntIdTransType()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 595
    const/16 v0, 0x18

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getBikeSpdAntId()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 204
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getBikeSpdAntIdTransType()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 541
    const/16 v0, 0x15

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getBikeSpdcadAntId()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 240
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getBikeSpdcadAntIdTransType()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 577
    const/16 v0, 0x17

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getBikeWeight()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 317
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getCadEnabled()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 437
    const/16 v0, 0x10

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 438
    if-nez v0, :cond_0

    .line 439
    const/4 v0, 0x0

    .line 440
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getCrankLength()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 501
    const/16 v0, 0x13

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getCustomWheelsize()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 277
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getEnabled()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 520
    const/16 v0, 0x14

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 521
    if-nez v0, :cond_0

    .line 522
    const/4 v0, 0x0

    .line 523
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getFrontGear(I)Ljava/lang/Short;
    .locals 2

    .prologue
    .line 662
    const/16 v0, 0x27

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getFrontGearNum()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 634
    const/16 v0, 0x26

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getId()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 398
    const/16 v0, 0xe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMessageIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 106
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 124
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, v0}, Lcom/garmin/fit/BikeProfileMesg;->getFieldStringValue(III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumFrontGear()I
    .locals 2

    .prologue
    .line 651
    const/16 v0, 0x27

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/BikeProfileMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumRearGear()I
    .locals 2

    .prologue
    .line 700
    const/16 v0, 0x29

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/BikeProfileMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getOdometer()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 185
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getOdometerRollover()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 614
    const/16 v0, 0x25

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getPowerCalFactor()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 337
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getPowerEnabled()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 479
    const/16 v0, 0x12

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 480
    if-nez v0, :cond_0

    .line 481
    const/4 v0, 0x0

    .line 482
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getRearGear(I)Ljava/lang/Short;
    .locals 2

    .prologue
    .line 711
    const/16 v0, 0x29

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getRearGearNum()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 683
    const/16 v0, 0x28

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getSpdEnabled()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 416
    const/16 v0, 0xf

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 417
    if-nez v0, :cond_0

    .line 418
    const/4 v0, 0x0

    .line 419
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getSpdcadEnabled()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 458
    const/16 v0, 0x11

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 459
    if-nez v0, :cond_0

    .line 460
    const/4 v0, 0x0

    .line 461
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getSport()Lcom/garmin/fit/Sport;
    .locals 3

    .prologue
    .line 142
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 143
    if-nez v0, :cond_0

    .line 144
    const/4 v0, 0x0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Sport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Sport;

    move-result-object v0

    goto :goto_0
.end method

.method public getSubSport()Lcom/garmin/fit/SubSport;
    .locals 3

    .prologue
    .line 163
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/BikeProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 164
    if-nez v0, :cond_0

    .line 165
    const/4 v0, 0x0

    .line 166
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/SubSport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SubSport;

    move-result-object v0

    goto :goto_0
.end method

.method public setAutoPowerZero(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 389
    const/16 v0, 0xd

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 390
    return-void
.end method

.method public setAutoWheelCal(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 368
    const/16 v0, 0xc

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 369
    return-void
.end method

.method public setAutoWheelsize(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 307
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 308
    return-void
.end method

.method public setBikeCadAntId(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 231
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 232
    return-void
.end method

.method public setBikeCadAntIdTransType(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 568
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 569
    return-void
.end method

.method public setBikePowerAntId(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 267
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 268
    return-void
.end method

.method public setBikePowerAntIdTransType(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 604
    const/16 v0, 0x18

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 605
    return-void
.end method

.method public setBikeSpdAntId(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 213
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 214
    return-void
.end method

.method public setBikeSpdAntIdTransType(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 550
    const/16 v0, 0x15

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 551
    return-void
.end method

.method public setBikeSpdcadAntId(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 249
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 250
    return-void
.end method

.method public setBikeSpdcadAntIdTransType(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 586
    const/16 v0, 0x17

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 587
    return-void
.end method

.method public setBikeWeight(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 327
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 328
    return-void
.end method

.method public setCadEnabled(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 449
    const/16 v0, 0x10

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 450
    return-void
.end method

.method public setCrankLength(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 511
    const/16 v0, 0x13

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 512
    return-void
.end method

.method public setCustomWheelsize(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 287
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 288
    return-void
.end method

.method public setEnabled(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 532
    const/16 v0, 0x14

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 533
    return-void
.end method

.method public setFrontGear(ILjava/lang/Short;)V
    .locals 2

    .prologue
    .line 673
    const/16 v0, 0x27

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 674
    return-void
.end method

.method public setFrontGearNum(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 644
    const/16 v0, 0x26

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 645
    return-void
.end method

.method public setId(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 407
    const/16 v0, 0xe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 408
    return-void
.end method

.method public setMessageIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 115
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 116
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 133
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, p1, v0}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 134
    return-void
.end method

.method public setOdometer(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 195
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 196
    return-void
.end method

.method public setOdometerRollover(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 624
    const/16 v0, 0x25

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 625
    return-void
.end method

.method public setPowerCalFactor(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 347
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 348
    return-void
.end method

.method public setPowerEnabled(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 491
    const/16 v0, 0x12

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 492
    return-void
.end method

.method public setRearGear(ILjava/lang/Short;)V
    .locals 2

    .prologue
    .line 722
    const/16 v0, 0x29

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 723
    return-void
.end method

.method public setRearGearNum(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 693
    const/16 v0, 0x28

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 694
    return-void
.end method

.method public setSpdEnabled(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 428
    const/16 v0, 0xf

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 429
    return-void
.end method

.method public setSpdcadEnabled(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 470
    const/16 v0, 0x11

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 471
    return-void
.end method

.method public setSport(Lcom/garmin/fit/Sport;)V
    .locals 4

    .prologue
    .line 154
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Sport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 155
    return-void
.end method

.method public setSubSport(Lcom/garmin/fit/SubSport;)V
    .locals 4

    .prologue
    .line 175
    const/4 v0, 0x2

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/SubSport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/BikeProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 176
    return-void
.end method
