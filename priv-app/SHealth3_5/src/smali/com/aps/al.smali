.class final Lcom/aps/al;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/aps/t;


# direct methods
.method constructor <init>(Lcom/aps/t;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    invoke-static {}, Landroid/os/Looper;->prepare()V

    iget-object v0, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/aps/t;->a(Lcom/aps/t;Landroid/os/Looper;)Landroid/os/Looper;

    iget-object v0, p0, Lcom/aps/al;->a:Lcom/aps/t;

    new-instance v1, Lcom/aps/an;

    iget-object v2, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-direct {v1, v2}, Lcom/aps/an;-><init>(Lcom/aps/t;)V

    invoke-static {v0, v1}, Lcom/aps/t;->a(Lcom/aps/t;Lcom/aps/an;)Lcom/aps/an;

    iget-object v0, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {v0}, Lcom/aps/t;->e(Lcom/aps/t;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {v1}, Lcom/aps/t;->d(Lcom/aps/t;)Lcom/aps/an;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addGpsStatusListener(Landroid/location/GpsStatus$Listener;)Z

    iget-object v0, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {v0}, Lcom/aps/t;->e(Lcom/aps/t;)Landroid/location/LocationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {v1}, Lcom/aps/t;->d(Lcom/aps/t;)Lcom/aps/an;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->addNmeaListener(Landroid/location/GpsStatus$NmeaListener;)Z

    iget-object v0, p0, Lcom/aps/al;->a:Lcom/aps/t;

    new-instance v1, Lcom/aps/am;

    invoke-direct {v1, p0}, Lcom/aps/am;-><init>(Lcom/aps/al;)V

    invoke-static {v0, v1}, Lcom/aps/t;->a(Lcom/aps/t;Landroid/os/Handler;)Landroid/os/Handler;

    iget-object v0, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {v0}, Lcom/aps/t;->e(Lcom/aps/t;)Landroid/location/LocationManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "passive"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {v0}, Lcom/aps/t;->e(Lcom/aps/t;)Landroid/location/LocationManager;

    move-result-object v0

    const-string/jumbo v1, "passive"

    const-wide/16 v2, 0x3e8

    invoke-static {}, Lcom/aps/t;->l()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/aps/al;->a:Lcom/aps/t;

    invoke-static {v5}, Lcom/aps/t;->f(Lcom/aps/t;)Landroid/location/LocationListener;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
