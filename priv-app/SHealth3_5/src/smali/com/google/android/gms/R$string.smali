.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final auth_client_needs_enabling_title:I = 0x7f090014

.field public static final auth_client_needs_installation_title:I = 0x7f090015

.field public static final auth_client_needs_update_title:I = 0x7f090016

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f090017

.field public static final auth_client_requested_by_msg:I = 0x7f090018

.field public static final auth_client_using_bad_version_title:I = 0x7f090013

.field public static final common_google_play_services_enable_button:I = 0x7f090006

.field public static final common_google_play_services_enable_text:I = 0x7f090005

.field public static final common_google_play_services_enable_title:I = 0x7f090004

.field public static final common_google_play_services_install_button:I = 0x7f090003

.field public static final common_google_play_services_install_text_phone:I = 0x7f090001

.field public static final common_google_play_services_install_text_tablet:I = 0x7f090002

.field public static final common_google_play_services_install_title:I = 0x7f090000

.field public static final common_google_play_services_invalid_account_text:I = 0x7f09000c

.field public static final common_google_play_services_invalid_account_title:I = 0x7f09000b

.field public static final common_google_play_services_network_error_text:I = 0x7f09000a

.field public static final common_google_play_services_network_error_title:I = 0x7f090009

.field public static final common_google_play_services_unknown_issue:I = 0x7f09000d

.field public static final common_google_play_services_unsupported_text:I = 0x7f09000f

.field public static final common_google_play_services_unsupported_title:I = 0x7f09000e

.field public static final common_google_play_services_update_button:I = 0x7f090010

.field public static final common_google_play_services_update_text:I = 0x7f090008

.field public static final common_google_play_services_update_title:I = 0x7f090007

.field public static final common_signin_button_text:I = 0x7f090011

.field public static final common_signin_button_text_long:I = 0x7f090012


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
