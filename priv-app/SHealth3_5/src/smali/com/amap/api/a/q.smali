.class Lcom/amap/api/a/q;
.super Ljava/lang/Object;
.source "CachManager.java"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/amap/api/a/p;

.field private c:Ljava/lang/String;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLcom/amap/api/a/as;)V
    .locals 3

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/q;->b:Lcom/amap/api/a/p;

    .line 46
    const-string v0, "/sdcard/Amap/RMap"

    iput-object v0, p0, Lcom/amap/api/a/q;->c:Ljava/lang/String;

    .line 48
    const/16 v0, 0x80

    iput v0, p0, Lcom/amap/api/a/q;->d:I

    .line 18
    iput-object p1, p0, Lcom/amap/api/a/q;->a:Landroid/content/Context;

    .line 19
    if-nez p3, :cond_1

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 22
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/q;->c:Ljava/lang/String;

    goto :goto_0

    .line 25
    :cond_2
    const/4 v0, 0x0

    .line 26
    iget-object v1, p3, Lcom/amap/api/a/as;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 27
    new-instance v1, Ljava/io/File;

    iget-object v0, p3, Lcom/amap/api/a/as;->l:Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 28
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    .line 29
    if-nez v0, :cond_3

    .line 30
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    .line 33
    :cond_3
    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/amap/api/a/q;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/amap/api/a/q;->c:Ljava/lang/String;

    invoke-static {v0, v1, p3}, Lcom/amap/api/a/q;->a(Landroid/content/Context;Ljava/lang/String;Lcom/amap/api/a/as;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/q;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(II)I
    .locals 2

    .prologue
    .line 151
    rem-int/lit16 v0, p1, 0x80

    .line 152
    rem-int/lit16 v1, p2, 0x80

    .line 153
    mul-int/lit16 v0, v0, 0x80

    add-int/2addr v0, v1

    .line 154
    return v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/amap/api/a/as;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 52
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 54
    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_0
    return-object v0

    .line 57
    :cond_0
    sget-object v0, Lcom/amap/api/maps2d/MapsInitializer;->sdcardDir:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/amap/api/maps2d/MapsInitializer;->sdcardDir:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 59
    :cond_1
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 61
    new-instance v1, Ljava/io/File;

    const-string v2, "Amap"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 63
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 64
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 66
    :cond_2
    new-instance v0, Ljava/io/File;

    iget-object v2, p2, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    .line 69
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    .line 71
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 73
    :cond_4
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/amap/api/maps2d/MapsInitializer;->sdcardDir:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_5

    .line 76
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 78
    :cond_5
    sget-object v0, Lcom/amap/api/maps2d/MapsInitializer;->sdcardDir:Ljava/lang/String;

    goto :goto_0
.end method

.method private a([B)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 131
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    aget-byte v0, p1, v2

    .line 135
    aget-byte v1, p1, v5

    aput-byte v1, p1, v2

    .line 136
    aput-byte v0, p1, v5

    .line 137
    aget-byte v0, p1, v3

    .line 138
    aget-byte v1, p1, v4

    aput-byte v1, p1, v3

    .line 139
    aput-byte v0, p1, v4

    goto :goto_0
.end method

.method private a(I)[B
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 123
    const/4 v1, 0x0

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 124
    const/4 v1, 0x1

    const v2, 0xff00

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 125
    const/4 v1, 0x2

    const/high16 v2, 0xff0000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 126
    const/4 v1, 0x3

    const/high16 v2, -0x1000000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x18

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 127
    return-object v0
.end method

.method private a(IIIZ)[Ljava/lang/String;
    .locals 7

    .prologue
    .line 85
    div-int/lit16 v0, p1, 0x80

    .line 86
    div-int/lit16 v1, p2, 0x80

    .line 87
    div-int/lit8 v2, v0, 0xa

    .line 88
    div-int/lit8 v3, v1, 0xa

    .line 90
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    .line 91
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    iget-object v6, p0, Lcom/amap/api/a/q;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 95
    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 97
    const-string v2, "/"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    const-string v2, "/"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    if-nez p4, :cond_0

    .line 101
    new-instance v2, Ljava/io/File;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 102
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 103
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 106
    :cond_0
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    const-string v2, "_"

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    const-string v0, "_"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".idx"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 113
    const/4 v0, 0x1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 114
    return-object v4
.end method

.method private b([B)I
    .locals 3

    .prologue
    .line 143
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    .line 144
    const/4 v1, 0x1

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 145
    const/4 v1, 0x2

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x10

    const/high16 v2, 0xff0000

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 146
    const/4 v1, 0x3

    aget-byte v1, p1, v1

    shl-int/lit8 v1, v1, 0x18

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    .line 147
    return v0
.end method


# virtual methods
.method public a(Lcom/amap/api/a/m$a;)I
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v0, -0x1

    .line 158
    iget v1, p1, Lcom/amap/api/a/m$a;->b:I

    iget v4, p1, Lcom/amap/api/a/m$a;->c:I

    iget v5, p1, Lcom/amap/api/a/m$a;->d:I

    invoke-direct {p0, v1, v4, v5, v3}, Lcom/amap/api/a/q;->a(IIIZ)[Ljava/lang/String;

    move-result-object v4

    .line 160
    if-eqz v4, :cond_0

    array-length v1, v4

    const/4 v5, 0x2

    if-ne v1, v5, :cond_0

    aget-object v1, v4, v6

    const-string v5, ""

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    array-length v1, v4

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v4, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 280
    :cond_0
    :goto_0
    return v0

    .line 165
    :cond_1
    new-instance v5, Ljava/io/File;

    aget-object v1, v4, v6

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 166
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    iget v1, p1, Lcom/amap/api/a/m$a;->b:I

    iget v6, p1, Lcom/amap/api/a/m$a;->c:I

    invoke-direct {p0, v1, v6}, Lcom/amap/api/a/q;->a(II)I

    move-result v6

    .line 170
    if-ltz v6, :cond_0

    .line 175
    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string/jumbo v7, "r"

    invoke-direct {v1, v5, v7}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 180
    :goto_1
    if-eqz v1, :cond_0

    .line 184
    mul-int/lit8 v5, v6, 0x4

    int-to-long v5, v5

    :try_start_1
    invoke-virtual {v1, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 189
    :goto_2
    new-array v5, v8, [B

    .line 191
    const/4 v6, 0x0

    const/4 v7, 0x4

    :try_start_2
    invoke-virtual {v1, v5, v6, v7}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 196
    :goto_3
    invoke-direct {p0, v5}, Lcom/amap/api/a/q;->a([B)V

    .line 197
    invoke-direct {p0, v5}, Lcom/amap/api/a/q;->b([B)I

    move-result v6

    .line 200
    :try_start_3
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 207
    :goto_4
    if-ltz v6, :cond_0

    .line 211
    new-instance v7, Ljava/io/File;

    aget-object v1, v4, v3

    invoke-direct {v7, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 212
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    :try_start_4
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string/jumbo v4, "r"

    invoke-direct {v1, v7, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_5

    move-object v4, v1

    .line 222
    :goto_5
    if-eqz v4, :cond_0

    .line 226
    int-to-long v6, v6

    :try_start_5
    invoke-virtual {v4, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 232
    :goto_6
    const/4 v1, 0x0

    const/4 v6, 0x4

    :try_start_6
    invoke-virtual {v4, v5, v1, v6}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7

    .line 237
    :goto_7
    invoke-direct {p0, v5}, Lcom/amap/api/a/q;->a([B)V

    .line 238
    invoke-direct {p0, v5}, Lcom/amap/api/a/q;->b([B)I

    move-result v5

    .line 239
    if-gtz v5, :cond_2

    .line 241
    :try_start_7
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    goto :goto_0

    .line 242
    :catch_0
    move-exception v1

    .line 244
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 176
    :catch_1
    move-exception v1

    .line 178
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v1, v2

    goto :goto_1

    .line 185
    :catch_2
    move-exception v5

    .line 187
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 192
    :catch_3
    move-exception v6

    .line 194
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 201
    :catch_4
    move-exception v1

    .line 203
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 218
    :catch_5
    move-exception v1

    .line 220
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v4, v2

    goto :goto_5

    .line 227
    :catch_6
    move-exception v1

    .line 229
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 233
    :catch_7
    move-exception v1

    .line 235
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 249
    :cond_2
    new-array v1, v5, [B

    .line 251
    const/4 v6, 0x0

    :try_start_8
    invoke-virtual {v4, v1, v6, v5}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    .line 257
    :goto_8
    :try_start_9
    invoke-virtual {v4}, Ljava/io/RandomAccessFile;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    .line 263
    :goto_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 264
    iget v5, p1, Lcom/amap/api/a/m$a;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 265
    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    iget v5, p1, Lcom/amap/api/a/m$a;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 267
    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    iget v5, p1, Lcom/amap/api/a/m$a;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 269
    iget-object v5, p0, Lcom/amap/api/a/q;->b:Lcom/amap/api/a/p;

    if-eqz v5, :cond_0

    .line 272
    iget-object v0, p0, Lcom/amap/api/a/q;->b:Lcom/amap/api/a/p;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/p;->a([BLjava/io/InputStream;ZLjava/util/List;Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 252
    :catch_8
    move-exception v5

    .line 254
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 258
    :catch_9
    move-exception v4

    .line 260
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9
.end method

.method public a(Lcom/amap/api/a/p;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/amap/api/a/q;->b:Lcom/amap/api/a/p;

    .line 119
    return-void
.end method

.method public declared-synchronized a([BIII)Z
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 285
    monitor-enter p0

    if-nez p1, :cond_1

    .line 432
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 288
    :cond_1
    :try_start_0
    array-length v6, p1

    .line 289
    if-lez v6, :cond_0

    .line 292
    const/4 v2, 0x0

    invoke-direct {p0, p2, p3, p4, v2}, Lcom/amap/api/a/q;->a(IIIZ)[Ljava/lang/String;

    move-result-object v9

    .line 293
    if-eqz v9, :cond_0

    array-length v2, v9

    const/4 v7, 0x2

    if-ne v2, v7, :cond_0

    const/4 v2, 0x0

    aget-object v2, v9, v2

    const-string v7, ""

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    array-length v2, v9

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v9, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 299
    new-instance v7, Ljava/io/File;

    const/4 v2, 0x1

    aget-object v2, v9, v2

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 300
    invoke-virtual {v7}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_2

    .line 303
    :try_start_1
    invoke-virtual {v7}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 308
    :goto_1
    if-eqz v2, :cond_0

    .line 315
    :cond_2
    :try_start_2
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v8, "rws"

    invoke-direct {v2, v7, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 320
    :goto_2
    if-eqz v2, :cond_0

    .line 324
    :try_start_3
    invoke-direct {p0, v6}, Lcom/amap/api/a/q;->a(I)[B

    move-result-object v10

    .line 325
    invoke-direct {p0, v10}, Lcom/amap/api/a/q;->a([B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 328
    :try_start_4
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->length()J
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-wide v6

    move-wide v7, v6

    .line 334
    :goto_3
    :try_start_5
    invoke-virtual {v2, v7, v8}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 340
    :goto_4
    :try_start_6
    invoke-virtual {v2, v10}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 346
    :goto_5
    :try_start_7
    invoke-virtual {v2, p1}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 352
    :goto_6
    :try_start_8
    invoke-virtual {v2}, Ljava/io/RandomAccessFile;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 359
    :goto_7
    :try_start_9
    new-instance v6, Ljava/io/File;

    const/4 v2, 0x0

    aget-object v2, v9, v2

    invoke-direct {v6, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 360
    invoke-virtual {v6}, Ljava/io/File;->exists()Z
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 363
    :try_start_a
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v2

    .line 368
    :goto_8
    if-eqz v2, :cond_0

    .line 374
    :cond_3
    :try_start_b
    new-instance v2, Ljava/io/RandomAccessFile;

    const-string/jumbo v9, "rws"

    invoke-direct {v2, v6, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_9
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-object v6, v2

    .line 379
    :goto_9
    if-eqz v6, :cond_0

    .line 384
    :try_start_c
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->length()J
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-wide v2

    .line 389
    :goto_a
    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 390
    const/high16 v2, 0x10000

    :try_start_d
    new-array v2, v2, [B

    .line 391
    const/4 v3, -0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 393
    :try_start_e
    invoke-virtual {v6, v2}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 400
    :cond_4
    :goto_b
    :try_start_f
    invoke-direct {p0, p2, p3}, Lcom/amap/api/a/q;->a(II)I
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    move-result v2

    .line 401
    if-gez v2, :cond_5

    .line 403
    :try_start_10
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_0
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_0

    .line 404
    :catch_0
    move-exception v1

    .line 406
    :try_start_11
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto/16 :goto_0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 304
    :catch_1
    move-exception v2

    .line 306
    :try_start_12
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move v2, v0

    goto :goto_1

    .line 316
    :catch_2
    move-exception v2

    .line 318
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v2, v3

    goto :goto_2

    .line 329
    :catch_3
    move-exception v6

    .line 331
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    move-wide v7, v4

    goto :goto_3

    .line 335
    :catch_4
    move-exception v6

    .line 337
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 341
    :catch_5
    move-exception v6

    .line 343
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 347
    :catch_6
    move-exception v6

    .line 349
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 353
    :catch_7
    move-exception v2

    .line 355
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 364
    :catch_8
    move-exception v2

    .line 366
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move v2, v0

    goto :goto_8

    .line 375
    :catch_9
    move-exception v2

    .line 377
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    move-object v6, v3

    goto :goto_9

    .line 385
    :catch_a
    move-exception v2

    .line 387
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-wide v2, v4

    goto :goto_a

    .line 394
    :catch_b
    move-exception v2

    .line 396
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto :goto_b

    .line 411
    :cond_5
    mul-int/lit8 v0, v2, 0x4

    int-to-long v2, v0

    :try_start_13
    invoke-virtual {v6, v2, v3}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_c
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 416
    :goto_c
    long-to-int v0, v7

    :try_start_14
    invoke-direct {p0, v0}, Lcom/amap/api/a/q;->a(I)[B

    move-result-object v0

    .line 417
    invoke-direct {p0, v0}, Lcom/amap/api/a/q;->a([B)V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 419
    :try_start_15
    invoke-virtual {v6, v0}, Ljava/io/RandomAccessFile;->write([B)V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_d
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 425
    :goto_d
    :try_start_16
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_e
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    :goto_e
    move v0, v1

    .line 432
    goto/16 :goto_0

    .line 412
    :catch_c
    move-exception v0

    .line 414
    :try_start_17
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 420
    :catch_d
    move-exception v0

    .line 422
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 426
    :catch_e
    move-exception v0

    .line 428
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    goto :goto_e
.end method
