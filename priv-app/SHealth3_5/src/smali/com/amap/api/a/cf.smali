.class Lcom/amap/api/a/cf;
.super Landroid/view/View;
.source "WaterMarkerView.java"


# instance fields
.field a:Landroid/graphics/Rect;

.field b:I

.field private c:Landroid/graphics/Bitmap;

.field private d:Landroid/graphics/Bitmap;

.field private e:Landroid/graphics/Paint;

.field private f:Z

.field private g:I

.field private h:Lcom/amap/api/a/b;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/a/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    .line 23
    iput-boolean v1, p0, Lcom/amap/api/a/cf;->f:Z

    .line 24
    iput v1, p0, Lcom/amap/api/a/cf;->g:I

    .line 26
    iput v1, p0, Lcom/amap/api/a/cf;->i:I

    .line 27
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/cf;->a:Landroid/graphics/Rect;

    .line 28
    const/16 v0, 0xa

    iput v0, p0, Lcom/amap/api/a/cf;->b:I

    .line 49
    iput-object p2, p0, Lcom/amap/api/a/cf;->h:Lcom/amap/api/a/b;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    .line 53
    :try_start_0
    sget-object v0, Lcom/amap/api/a/x;->e:Lcom/amap/api/a/x$a;

    sget-object v2, Lcom/amap/api/a/x$a;->b:Lcom/amap/api/a/x$a;

    if-ne v0, v2, :cond_0

    .line 54
    const-string v0, "apl.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 58
    :goto_0
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/amap/api/a/cf;->c:Landroid/graphics/Bitmap;

    .line 59
    iget-object v2, p0, Lcom/amap/api/a/cf;->c:Landroid/graphics/Bitmap;

    sget v3, Lcom/amap/api/a/x;->a:F

    invoke-static {v2, v3}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/amap/api/a/cf;->c:Landroid/graphics/Bitmap;

    .line 61
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 63
    sget-object v0, Lcom/amap/api/a/x;->e:Lcom/amap/api/a/x$a;

    sget-object v2, Lcom/amap/api/a/x$a;->b:Lcom/amap/api/a/x$a;

    if-ne v0, v2, :cond_1

    .line 64
    const-string v0, "apl1.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    .line 69
    :goto_1
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    .line 70
    iget-object v1, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    sget v2, Lcom/amap/api/a/x;->a:F

    invoke-static {v1, v2}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    .line 72
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 73
    iget-object v0, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/amap/api/a/cf;->g:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_2
    iget-object v0, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 80
    iget-object v0, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 81
    iget-object v0, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 83
    return-void

    .line 56
    :cond_0
    :try_start_1
    const-string v0, "ap.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_1
    const-string v0, "ap1.data"

    invoke-virtual {v1, v0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 75
    :catch_0
    move-exception v0

    .line 76
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 32
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/cf;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/amap/api/a/cf;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 36
    iget-object v0, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 38
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cf;->c:Landroid/graphics/Bitmap;

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 102
    iput p1, p0, Lcom/amap/api/a/cf;->i:I

    .line 103
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/amap/api/a/cf;->f:Z

    .line 94
    invoke-virtual {p0}, Lcom/amap/api/a/cf;->invalidate()V

    .line 95
    return-void
.end method

.method public b()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/amap/api/a/cf;->f:Z

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/cf;->c:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public c()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 98
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/amap/api/a/cf;->b:I

    invoke-virtual {p0}, Lcom/amap/api/a/cf;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/amap/api/a/cf;->g:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0xa

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 107
    iget-object v0, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    const-string v1, "V2.2.0.2"

    const/4 v2, 0x0

    const-string v3, "V2.2.0.2"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/a/cf;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 109
    iget-object v0, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    iget-object v1, p0, Lcom/amap/api/a/cf;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/2addr v0, v1

    .line 113
    iget v1, p0, Lcom/amap/api/a/cf;->i:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 114
    iget-object v1, p0, Lcom/amap/api/a/cf;->h:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->getWidth()I

    move-result v1

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/amap/api/a/cf;->b:I

    .line 121
    :goto_1
    sget-object v0, Lcom/amap/api/a/x;->e:Lcom/amap/api/a/x$a;

    sget-object v1, Lcom/amap/api/a/x$a;->b:Lcom/amap/api/a/x$a;

    if-ne v0, v1, :cond_3

    .line 122
    invoke-virtual {p0}, Lcom/amap/api/a/cf;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/a/cf;->b:I

    add-int/lit8 v1, v1, 0xf

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/a/cf;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/amap/api/a/cf;->g:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x8

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 124
    const-string v0, "V2.2.0.2"

    iget-object v1, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/amap/api/a/cf;->b:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x4

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/a/cf;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0x10

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 115
    :cond_1
    iget v1, p0, Lcom/amap/api/a/cf;->i:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 116
    iget-object v1, p0, Lcom/amap/api/a/cf;->h:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->getWidth()I

    move-result v1

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, -0xa

    iput v0, p0, Lcom/amap/api/a/cf;->b:I

    goto :goto_1

    .line 118
    :cond_2
    const/16 v0, 0xa

    iput v0, p0, Lcom/amap/api/a/cf;->b:I

    goto :goto_1

    .line 128
    :cond_3
    invoke-virtual {p0}, Lcom/amap/api/a/cf;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/a/cf;->b:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/a/cf;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/amap/api/a/cf;->g:I

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x8

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 130
    const-string v0, "V2.2.0.2"

    iget-object v1, p0, Lcom/amap/api/a/cf;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/amap/api/a/cf;->b:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/amap/api/a/cf;->getHeight()I

    move-result v2

    add-int/lit8 v2, v2, -0xc

    int-to-float v2, v2

    iget-object v3, p0, Lcom/amap/api/a/cf;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method
