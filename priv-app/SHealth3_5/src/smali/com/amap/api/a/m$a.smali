.class Lcom/amap/api/a/m$a;
.super Ljava/lang/Object;
.source "BaseMapTile.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field public a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public f:Landroid/graphics/PointF;

.field public g:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/m$a;->a:I

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/a/m$a;->g:I

    .line 63
    iput p1, p0, Lcom/amap/api/a/m$a;->b:I

    .line 64
    iput p2, p0, Lcom/amap/api/a/m$a;->c:I

    .line 65
    iput p3, p0, Lcom/amap/api/a/m$a;->d:I

    .line 66
    iput p4, p0, Lcom/amap/api/a/m$a;->e:I

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/amap/api/a/m$a;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/m$a;->a:I

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/a/m$a;->g:I

    .line 70
    iget v0, p1, Lcom/amap/api/a/m$a;->b:I

    iput v0, p0, Lcom/amap/api/a/m$a;->b:I

    .line 71
    iget v0, p1, Lcom/amap/api/a/m$a;->c:I

    iput v0, p0, Lcom/amap/api/a/m$a;->c:I

    .line 72
    iget v0, p1, Lcom/amap/api/a/m$a;->d:I

    iput v0, p0, Lcom/amap/api/a/m$a;->d:I

    .line 73
    iget v0, p1, Lcom/amap/api/a/m$a;->e:I

    iput v0, p0, Lcom/amap/api/a/m$a;->e:I

    .line 74
    iget-object v0, p1, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 75
    iget v0, p1, Lcom/amap/api/a/m$a;->a:I

    iput v0, p0, Lcom/amap/api/a/m$a;->a:I

    .line 76
    return-void
.end method


# virtual methods
.method public a()Lcom/amap/api/a/m$a;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/amap/api/a/m$a;

    invoke-direct {v0, p0}, Lcom/amap/api/a/m$a;-><init>(Lcom/amap/api/a/m$a;)V

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/amap/api/a/m$a;->a()Lcom/amap/api/a/m$a;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85
    if-ne p0, p1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 88
    :cond_1
    instance-of v2, p1, Lcom/amap/api/a/m$a;

    if-nez v2, :cond_2

    move v0, v1

    .line 89
    goto :goto_0

    .line 92
    :cond_2
    check-cast p1, Lcom/amap/api/a/m$a;

    .line 93
    iget v2, p0, Lcom/amap/api/a/m$a;->b:I

    iget v3, p1, Lcom/amap/api/a/m$a;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/a/m$a;->c:I

    iget v3, p1, Lcom/amap/api/a/m$a;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/a/m$a;->d:I

    iget v3, p1, Lcom/amap/api/a/m$a;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/a/m$a;->e:I

    iget v3, p1, Lcom/amap/api/a/m$a;->e:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 100
    iget v0, p0, Lcom/amap/api/a/m$a;->b:I

    mul-int/lit8 v0, v0, 0x7

    iget v1, p0, Lcom/amap/api/a/m$a;->c:I

    mul-int/lit8 v1, v1, 0xb

    add-int/2addr v0, v1

    iget v1, p0, Lcom/amap/api/a/m$a;->d:I

    mul-int/lit8 v1, v1, 0xd

    add-int/2addr v0, v1

    iget v1, p0, Lcom/amap/api/a/m$a;->e:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    iget v1, p0, Lcom/amap/api/a/m$a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 107
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    iget v1, p0, Lcom/amap/api/a/m$a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    iget v1, p0, Lcom/amap/api/a/m$a;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 111
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    iget v1, p0, Lcom/amap/api/a/m$a;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
