.class Lcom/amap/api/a/ba;
.super Ljava/lang/Object;
.source "MapProjection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/ba$a;
    }
.end annotation


# instance fields
.field a:D

.field b:I

.field c:D

.field d:D

.field public e:I

.field public f:I

.field public g:I

.field public h:[D

.field public i:Lcom/amap/api/a/bb;

.field public j:Lcom/amap/api/a/ac;

.field public k:Lcom/amap/api/a/ac;

.field public l:Landroid/graphics/Point;

.field public m:Lcom/amap/api/a/ba$a;

.field n:Lcom/amap/api/a/bf$d;

.field private o:D

.field private p:D

.field private q:D


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf$d;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide v0, 0x405d196b11c6d1e1L    # 116.39716

    iput-wide v0, p0, Lcom/amap/api/a/ba;->o:D

    .line 25
    const-wide v0, 0x4043f556191148feL    # 39.91669

    iput-wide v0, p0, Lcom/amap/api/a/ba;->p:D

    .line 30
    const-wide v0, 0x41031bf8456d5cfbL    # 156543.0339

    iput-wide v0, p0, Lcom/amap/api/a/ba;->a:D

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/ba;->b:I

    .line 40
    const-wide v0, -0x3e8ce407ba8f5c29L    # -2.003750834E7

    iput-wide v0, p0, Lcom/amap/api/a/ba;->c:D

    .line 41
    const-wide v0, 0x41731bf84570a3d7L    # 2.003750834E7

    iput-wide v0, p0, Lcom/amap/api/a/ba;->d:D

    .line 43
    sget v0, Lcom/amap/api/a/x;->d:I

    iput v0, p0, Lcom/amap/api/a/ba;->e:I

    .line 44
    sget v0, Lcom/amap/api/a/x;->c:I

    iput v0, p0, Lcom/amap/api/a/ba;->f:I

    .line 45
    const/16 v0, 0xa

    iput v0, p0, Lcom/amap/api/a/ba;->g:I

    .line 47
    iput-object v2, p0, Lcom/amap/api/a/ba;->h:[D

    .line 49
    iput-object v2, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    .line 51
    iput-object v2, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 52
    iput-object v2, p0, Lcom/amap/api/a/ba;->k:Lcom/amap/api/a/ac;

    .line 54
    iput-object v2, p0, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    .line 55
    iput-object v2, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    .line 57
    iput-object v2, p0, Lcom/amap/api/a/ba;->n:Lcom/amap/api/a/bf$d;

    .line 478
    const-wide v0, 0x3f91df46a2529d37L    # 0.01745329251994329

    iput-wide v0, p0, Lcom/amap/api/a/ba;->q:D

    .line 60
    iput-object p1, p0, Lcom/amap/api/a/ba;->n:Lcom/amap/api/a/bf$d;

    .line 61
    return-void
.end method

.method private a(II)I
    .locals 2

    .prologue
    .line 133
    const/4 v0, 0x1

    .line 134
    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    .line 135
    mul-int/2addr v0, p1

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    :cond_0
    return v0
.end method


# virtual methods
.method public a(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;)F
    .locals 20

    .prologue
    .line 488
    invoke-virtual/range {p1 .. p1}, Lcom/amap/api/a/ac;->c()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v1

    .line 489
    invoke-virtual/range {p1 .. p1}, Lcom/amap/api/a/ac;->d()J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v3

    .line 490
    invoke-virtual/range {p2 .. p2}, Lcom/amap/api/a/ac;->c()J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v5

    .line 491
    invoke-virtual/range {p2 .. p2}, Lcom/amap/api/a/ac;->d()J

    move-result-wide v7

    invoke-static {v7, v8}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v7

    .line 493
    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/amap/api/a/ba;->q:D

    mul-double/2addr v1, v9

    .line 494
    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/amap/api/a/ba;->q:D

    mul-double/2addr v3, v9

    .line 495
    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/amap/api/a/ba;->q:D

    mul-double/2addr v5, v9

    .line 496
    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/amap/api/a/ba;->q:D

    mul-double/2addr v7, v9

    .line 497
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v9

    .line 498
    invoke-static {v3, v4}, Ljava/lang/Math;->sin(D)D

    move-result-wide v11

    .line 499
    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    .line 500
    invoke-static {v3, v4}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    .line 501
    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v13

    .line 502
    invoke-static {v7, v8}, Ljava/lang/Math;->sin(D)D

    move-result-wide v15

    .line 503
    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    .line 504
    invoke-static {v7, v8}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    .line 505
    const/16 v17, 0x3

    move/from16 v0, v17

    new-array v0, v0, [D

    move-object/from16 v17, v0

    .line 506
    const/16 v18, 0x3

    move/from16 v0, v18

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 507
    const/16 v19, 0x0

    mul-double/2addr v1, v3

    aput-wide v1, v17, v19

    .line 508
    const/4 v1, 0x1

    mul-double v2, v3, v9

    aput-wide v2, v17, v1

    .line 509
    const/4 v1, 0x2

    aput-wide v11, v17, v1

    .line 510
    const/4 v1, 0x0

    mul-double v2, v7, v5

    aput-wide v2, v18, v1

    .line 511
    const/4 v1, 0x1

    mul-double v2, v7, v13

    aput-wide v2, v18, v1

    .line 512
    const/4 v1, 0x2

    aput-wide v15, v18, v1

    .line 513
    const/4 v1, 0x0

    aget-wide v1, v17, v1

    const/4 v3, 0x0

    aget-wide v3, v18, v3

    sub-double/2addr v1, v3

    const/4 v3, 0x0

    aget-wide v3, v17, v3

    const/4 v5, 0x0

    aget-wide v5, v18, v5

    sub-double/2addr v3, v5

    mul-double/2addr v1, v3

    const/4 v3, 0x1

    aget-wide v3, v17, v3

    const/4 v5, 0x1

    aget-wide v5, v18, v5

    sub-double/2addr v3, v5

    const/4 v5, 0x1

    aget-wide v5, v17, v5

    const/4 v7, 0x1

    aget-wide v7, v18, v7

    sub-double/2addr v5, v7

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    const/4 v3, 0x2

    aget-wide v3, v17, v3

    const/4 v5, 0x2

    aget-wide v5, v18, v5

    sub-double/2addr v3, v5

    const/4 v5, 0x2

    aget-wide v5, v17, v5

    const/4 v7, 0x2

    aget-wide v7, v18, v7

    sub-double/2addr v5, v7

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v1

    .line 517
    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->asin(D)D

    move-result-wide v1

    const-wide v3, 0x41684dae328e2ad1L    # 1.27420015798544E7

    mul-double/2addr v1, v3

    double-to-float v1, v1

    return v1
.end method

.method a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;
    .locals 5

    .prologue
    const/high16 v4, 0x43800000    # 256.0f

    const/4 v3, 0x0

    .line 419
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 421
    sub-int v1, p1, p3

    mul-int/lit16 v1, v1, 0x100

    int-to-float v1, v1

    iget v2, p5, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 423
    iget v1, p0, Lcom/amap/api/a/ba;->b:I

    if-nez v1, :cond_3

    .line 424
    sub-int v1, p2, p4

    mul-int/lit16 v1, v1, 0x100

    int-to-float v1, v1

    iget v2, p5, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 432
    :cond_0
    :goto_0
    iget v1, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v4

    cmpg-float v1, v1, v3

    if-lez v1, :cond_1

    iget v1, v0, Landroid/graphics/PointF;->x:F

    int-to-float v2, p6

    cmpl-float v1, v1, v2

    if-gez v1, :cond_1

    iget v1, v0, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v4

    cmpg-float v1, v1, v3

    if-lez v1, :cond_1

    iget v1, v0, Landroid/graphics/PointF;->y:F

    int-to-float v2, p7

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_2

    .line 436
    :cond_1
    const/4 v0, 0x0

    .line 438
    :cond_2
    return-object v0

    .line 426
    :cond_3
    iget v1, p0, Lcom/amap/api/a/ba;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 427
    iget v1, p5, Landroid/graphics/PointF;->y:F

    sub-int v2, p2, p4

    mul-int/lit16 v2, v2, 0x100

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method a(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;
    .locals 7

    .prologue
    .line 175
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 176
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v1

    invoke-virtual {p2}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v3

    sub-double/2addr v1, v3

    div-double/2addr v1, p4

    iget v3, p3, Landroid/graphics/Point;->x:I

    int-to-double v3, v3

    add-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 178
    iget v1, p3, Landroid/graphics/Point;->y:I

    int-to-double v1, v1

    invoke-virtual {p1}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v3

    invoke-virtual {p2}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v5

    sub-double/2addr v3, v5

    div-double/2addr v3, p4

    sub-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 181
    return-object v0
.end method

.method public a(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;
    .locals 1

    .prologue
    .line 214
    invoke-virtual/range {p0 .. p6}, Lcom/amap/api/a/ba;->b(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;
    .locals 11

    .prologue
    const-wide v9, 0x41731bf84570a3d7L    # 2.003750834E7

    const-wide v4, 0x412e848000000000L    # 1000000.0

    const-wide v7, 0x4066800000000000L    # 180.0

    .line 147
    if-nez p1, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 161
    :goto_0
    return-object v0

    .line 153
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->b()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v4

    .line 154
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->a()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v4

    .line 156
    mul-double/2addr v2, v9

    div-double v3, v2, v7

    .line 157
    const-wide v5, 0x4056800000000000L    # 90.0

    add-double/2addr v0, v5

    const-wide v5, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v5

    const-wide v5, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v5

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide v5, 0x3f91df46a2529d39L    # 0.017453292519943295

    div-double/2addr v0, v5

    .line 159
    mul-double/2addr v0, v9

    div-double v1, v0, v7

    .line 160
    new-instance v0, Lcom/amap/api/a/ac;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/a/ac;III)Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amap/api/a/ac;",
            "III)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 296
    iget-object v0, p0, Lcom/amap/api/a/ba;->h:[D

    iget v1, p0, Lcom/amap/api/a/ba;->g:I

    aget-wide v9, v0, v1

    .line 298
    const/4 v0, 0x0

    .line 301
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v1

    iget-wide v3, p0, Lcom/amap/api/a/ba;->c:D

    sub-double/2addr v1, v3

    const-wide/high16 v3, 0x4070000000000000L    # 256.0

    mul-double/2addr v3, v9

    div-double/2addr v1, v3

    double-to-int v11, v1

    .line 302
    mul-int/lit16 v1, v11, 0x100

    int-to-double v1, v1

    mul-double/2addr v1, v9

    iget-wide v3, p0, Lcom/amap/api/a/ba;->c:D

    add-double/2addr v3, v1

    .line 305
    const-wide/16 v1, 0x0

    .line 306
    iget v5, p0, Lcom/amap/api/a/ba;->b:I

    if-nez v5, :cond_3

    .line 307
    iget-wide v0, p0, Lcom/amap/api/a/ba;->d:D

    invoke-virtual {p1}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v5

    sub-double/2addr v0, v5

    const-wide/high16 v5, 0x4070000000000000L    # 256.0

    mul-double/2addr v5, v9

    div-double/2addr v0, v5

    double-to-int v0, v0

    .line 309
    iget-wide v1, p0, Lcom/amap/api/a/ba;->d:D

    mul-int/lit16 v5, v0, 0x100

    int-to-double v5, v5

    mul-double/2addr v5, v9

    sub-double/2addr v1, v5

    move v8, v0

    .line 317
    :goto_0
    new-instance v0, Lcom/amap/api/a/ac;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    .line 320
    iget-object v4, p0, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    move-object v1, p0

    move-object v2, v0

    move-object v3, p1

    move-wide v5, v9

    invoke-virtual/range {v1 .. v6}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;

    move-result-object v5

    .line 323
    new-instance v0, Lcom/amap/api/a/m$a;

    iget v1, p0, Lcom/amap/api/a/ba;->g:I

    const/4 v2, -0x1

    invoke-direct {v0, v11, v8, v1, v2}, Lcom/amap/api/a/m$a;-><init>(IIII)V

    .line 325
    iput-object v5, v0, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 326
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 327
    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    const/4 v0, 0x1

    move v10, v0

    .line 336
    :goto_1
    const/4 v9, 0x0

    .line 337
    sub-int v1, v11, v10

    :goto_2
    add-int v0, v11, v10

    if-gt v1, v0, :cond_4

    .line 339
    add-int v2, v8, v10

    move-object v0, p0

    move v3, v11

    move v4, v8

    move/from16 v6, p3

    move/from16 v7, p4

    .line 341
    invoke-virtual/range {v0 .. v7}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v3

    .line 344
    if-eqz v3, :cond_0

    .line 345
    if-nez v9, :cond_b

    .line 346
    const/4 v0, 0x1

    .line 348
    :goto_3
    new-instance v4, Lcom/amap/api/a/m$a;

    iget v6, p0, Lcom/amap/api/a/ba;->g:I

    const/4 v7, -0x1

    invoke-direct {v4, v1, v2, v6, v7}, Lcom/amap/api/a/m$a;-><init>(IIII)V

    .line 350
    iput-object v3, v4, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 351
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v9, v0

    .line 354
    :cond_0
    sub-int v2, v8, v10

    move-object v0, p0

    move v3, v11

    move v4, v8

    move/from16 v6, p3

    move/from16 v7, p4

    .line 356
    invoke-virtual/range {v0 .. v7}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_2

    .line 360
    if-nez v9, :cond_1

    .line 361
    const/4 v9, 0x1

    .line 363
    :cond_1
    new-instance v3, Lcom/amap/api/a/m$a;

    iget v4, p0, Lcom/amap/api/a/ba;->g:I

    const/4 v6, -0x1

    invoke-direct {v3, v1, v2, v4, v6}, Lcom/amap/api/a/m$a;-><init>(IIII)V

    .line 365
    iput-object v0, v3, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 366
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 311
    :cond_3
    iget v5, p0, Lcom/amap/api/a/ba;->b:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_c

    .line 312
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v0

    iget-wide v5, p0, Lcom/amap/api/a/ba;->d:D

    sub-double/2addr v0, v5

    const-wide/high16 v5, 0x4070000000000000L    # 256.0

    mul-double/2addr v5, v9

    div-double/2addr v0, v5

    double-to-int v0, v0

    .line 313
    add-int/lit8 v1, v0, 0x1

    mul-int/lit16 v1, v1, 0x100

    int-to-double v1, v1

    mul-double/2addr v1, v9

    move v8, v0

    goto/16 :goto_0

    .line 371
    :cond_4
    add-int v0, v8, v10

    add-int/lit8 v2, v0, -0x1

    :goto_4
    sub-int v0, v8, v10

    if-le v2, v0, :cond_8

    .line 372
    add-int v1, v11, v10

    move-object v0, p0

    move v3, v11

    move v4, v8

    move/from16 v6, p3

    move/from16 v7, p4

    .line 375
    invoke-virtual/range {v0 .. v7}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v3

    .line 378
    if-eqz v3, :cond_5

    .line 379
    if-nez v9, :cond_a

    .line 380
    const/4 v0, 0x1

    .line 382
    :goto_5
    new-instance v4, Lcom/amap/api/a/m$a;

    iget v6, p0, Lcom/amap/api/a/ba;->g:I

    const/4 v7, -0x1

    invoke-direct {v4, v1, v2, v6, v7}, Lcom/amap/api/a/m$a;-><init>(IIII)V

    .line 384
    iput-object v3, v4, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 385
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v9, v0

    .line 389
    :cond_5
    sub-int v1, v11, v10

    move-object v0, p0

    move v3, v11

    move v4, v8

    move/from16 v6, p3

    move/from16 v7, p4

    .line 391
    invoke-virtual/range {v0 .. v7}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v0

    .line 394
    if-eqz v0, :cond_7

    .line 395
    if-nez v9, :cond_6

    .line 396
    const/4 v9, 0x1

    .line 398
    :cond_6
    new-instance v3, Lcom/amap/api/a/m$a;

    iget v4, p0, Lcom/amap/api/a/ba;->g:I

    const/4 v6, -0x1

    invoke-direct {v3, v1, v2, v4, v6}, Lcom/amap/api/a/m$a;-><init>(IIII)V

    .line 400
    iput-object v0, v3, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 401
    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    :cond_7
    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    .line 407
    :cond_8
    if-nez v9, :cond_9

    .line 411
    return-object v12

    .line 335
    :cond_9
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto/16 :goto_1

    :cond_a
    move v0, v9

    goto :goto_5

    :cond_b
    move v0, v9

    goto/16 :goto_3

    :cond_c
    move v8, v0

    goto/16 :goto_0
.end method

.method public a()V
    .locals 8

    .prologue
    const v7, 0x4b98dfc2    # 2.0037508E7f

    const v6, -0x3467203e    # -2.0037508E7f

    const-wide/16 v2, 0x0

    .line 72
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    if-eqz v0, :cond_5

    .line 73
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->a:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->a:D

    iput-wide v0, p0, Lcom/amap/api/a/ba;->o:D

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->b:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->b:D

    iput-wide v0, p0, Lcom/amap/api/a/ba;->p:D

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->c:D

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->c:D

    iput-wide v0, p0, Lcom/amap/api/a/ba;->a:D

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget v0, v0, Lcom/amap/api/a/bb;->d:I

    iput v0, p0, Lcom/amap/api/a/ba;->b:I

    .line 85
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->e:D

    iput-wide v0, p0, Lcom/amap/api/a/ba;->c:D

    .line 86
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget-wide v0, v0, Lcom/amap/api/a/bb;->f:D

    iput-wide v0, p0, Lcom/amap/api/a/ba;->d:D

    .line 87
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget v0, v0, Lcom/amap/api/a/bb;->g:I

    if-ltz v0, :cond_3

    .line 88
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget v0, v0, Lcom/amap/api/a/bb;->g:I

    iput v0, p0, Lcom/amap/api/a/ba;->e:I

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget v0, v0, Lcom/amap/api/a/bb;->h:I

    if-ltz v0, :cond_4

    .line 91
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget v0, v0, Lcom/amap/api/a/bb;->h:I

    iput v0, p0, Lcom/amap/api/a/ba;->f:I

    .line 93
    :cond_4
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget v0, v0, Lcom/amap/api/a/bb;->i:I

    if-ltz v0, :cond_5

    .line 94
    iget-object v0, p0, Lcom/amap/api/a/ba;->i:Lcom/amap/api/a/bb;

    iget v0, v0, Lcom/amap/api/a/bb;->i:I

    iput v0, p0, Lcom/amap/api/a/ba;->g:I

    .line 97
    :cond_5
    iget v0, p0, Lcom/amap/api/a/ba;->f:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/amap/api/a/ba;->h:[D

    .line 98
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/amap/api/a/ba;->f:I

    if-gt v0, v1, :cond_6

    .line 99
    iget-wide v1, p0, Lcom/amap/api/a/ba;->a:D

    const/4 v3, 0x2

    invoke-direct {p0, v3, v0}, Lcom/amap/api/a/ba;->a(II)I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v1, v3

    .line 100
    iget-object v3, p0, Lcom/amap/api/a/ba;->h:[D

    aput-wide v1, v3, v0

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_6
    new-instance v0, Lcom/amap/api/a/ac;

    iget-wide v1, p0, Lcom/amap/api/a/ba;->p:D

    iget-wide v3, p0, Lcom/amap/api/a/ba;->o:D

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    .line 105
    invoke-virtual {p0, v0}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 106
    iget-object v0, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->g()Lcom/amap/api/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/ba;->k:Lcom/amap/api/a/ac;

    .line 107
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/amap/api/a/ba;->n:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->c()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/amap/api/a/ba;->n:Lcom/amap/api/a/bf$d;

    invoke-virtual {v2}, Lcom/amap/api/a/bf$d;->d()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    .line 110
    new-instance v0, Lcom/amap/api/a/ba$a;

    invoke-direct {v0}, Lcom/amap/api/a/ba$a;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    .line 111
    iget-object v0, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iput v6, v0, Lcom/amap/api/a/ba$a;->a:F

    .line 112
    iget-object v0, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iput v7, v0, Lcom/amap/api/a/ba$a;->b:F

    .line 113
    iget-object v0, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iput v7, v0, Lcom/amap/api/a/ba$a;->c:F

    .line 114
    iget-object v0, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iput v6, v0, Lcom/amap/api/a/ba$a;->d:F

    .line 115
    return-void
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    .line 119
    return-void
.end method

.method public a(Landroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 8

    .prologue
    .line 442
    iget-object v0, p0, Lcom/amap/api/a/ba;->h:[D

    aget-wide v4, v0, p3

    .line 443
    iget-object v2, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v6, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/amap/api/a/ba;->b(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;

    move-result-object v7

    .line 445
    iget-object v2, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v6, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    move-object v0, p0

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/amap/api/a/ba;->b(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 448
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v1

    invoke-virtual {v7}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v3

    sub-double/2addr v1, v3

    .line 450
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v3

    invoke-virtual {v7}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v5

    sub-double/2addr v3, v5

    .line 453
    iget-object v0, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v5

    add-double v0, v5, v1

    .line 455
    iget-object v2, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    invoke-virtual {v2}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v5

    add-double v2, v5, v3

    .line 457
    :goto_0
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->a:F

    float-to-double v4, v4

    cmpg-double v4, v0, v4

    if-gez v4, :cond_0

    .line 458
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->b:F

    iget-object v5, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v5, v5, Lcom/amap/api/a/ba$a;->a:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    add-double/2addr v0, v4

    goto :goto_0

    .line 461
    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->b:F

    float-to-double v4, v4

    cmpl-double v4, v0, v4

    if-lez v4, :cond_1

    .line 462
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->b:F

    iget-object v5, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v5, v5, Lcom/amap/api/a/ba$a;->a:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v0, v4

    goto :goto_1

    .line 465
    :cond_1
    :goto_2
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->d:F

    float-to-double v4, v4

    cmpg-double v4, v2, v4

    if-gez v4, :cond_2

    .line 466
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->c:F

    iget-object v5, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v5, v5, Lcom/amap/api/a/ba$a;->d:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    add-double/2addr v2, v4

    goto :goto_2

    .line 469
    :cond_2
    :goto_3
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->c:F

    float-to-double v4, v4

    cmpl-double v4, v2, v4

    if-lez v4, :cond_3

    .line 470
    iget-object v4, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v4, v4, Lcom/amap/api/a/ba$a;->c:F

    iget-object v5, p0, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    iget v5, v5, Lcom/amap/api/a/ba$a;->d:F

    sub-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    goto :goto_3

    .line 472
    :cond_3
    iget-object v4, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    invoke-virtual {v4, v2, v3}, Lcom/amap/api/a/ac;->b(D)V

    .line 473
    iget-object v2, p0, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    invoke-virtual {v2, v0, v1}, Lcom/amap/api/a/ac;->a(D)V

    .line 476
    return-void
.end method

.method public b(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;
    .locals 6

    .prologue
    .line 268
    invoke-virtual {p0, p1}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;

    move-result-object v0

    .line 271
    iget-object v1, p0, Lcom/amap/api/a/ba;->n:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amap/api/a/b;->b(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method b(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;
    .locals 7

    .prologue
    .line 231
    iget-object v0, p0, Lcom/amap/api/a/ba;->n:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/a/b;->c(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    .line 232
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iget v2, p3, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 233
    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v2, p3, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    sub-float v2, v0, v2

    .line 235
    invoke-virtual {p2}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v3

    float-to-double v0, v1

    mul-double/2addr v0, p4

    add-double/2addr v0, v3

    .line 236
    invoke-virtual {p2}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v3

    float-to-double v5, v2

    mul-double/2addr v5, p4

    sub-double v5, v3, v5

    .line 238
    :goto_0
    iget v2, p6, Lcom/amap/api/a/ba$a;->a:F

    float-to-double v2, v2

    cmpg-double v2, v0, v2

    if-gez v2, :cond_3

    .line 239
    iget v2, p6, Lcom/amap/api/a/ba$a;->b:F

    iget v3, p6, Lcom/amap/api/a/ba$a;->a:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    add-double/2addr v0, v2

    goto :goto_0

    .line 242
    :goto_1
    iget v0, p6, Lcom/amap/api/a/ba$a;->b:F

    float-to-double v0, v0

    cmpl-double v0, v3, v0

    if-lez v0, :cond_2

    .line 243
    iget v0, p6, Lcom/amap/api/a/ba$a;->b:F

    iget v1, p6, Lcom/amap/api/a/ba$a;->a:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    sub-double/2addr v3, v0

    goto :goto_1

    .line 246
    :goto_2
    iget v2, p6, Lcom/amap/api/a/ba$a;->d:F

    float-to-double v5, v2

    cmpg-double v2, v0, v5

    if-gez v2, :cond_1

    .line 247
    iget v2, p6, Lcom/amap/api/a/ba$a;->c:F

    iget v5, p6, Lcom/amap/api/a/ba$a;->d:F

    sub-float/2addr v2, v5

    float-to-double v5, v2

    add-double/2addr v0, v5

    goto :goto_2

    .line 250
    :goto_3
    iget v0, p6, Lcom/amap/api/a/ba$a;->c:F

    float-to-double v5, v0

    cmpl-double v0, v1, v5

    if-lez v0, :cond_0

    .line 251
    iget v0, p6, Lcom/amap/api/a/ba$a;->c:F

    iget v5, p6, Lcom/amap/api/a/ba$a;->d:F

    sub-float/2addr v0, v5

    float-to-double v5, v0

    sub-double/2addr v1, v5

    goto :goto_3

    .line 253
    :cond_0
    new-instance v0, Lcom/amap/api/a/ac;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    .line 254
    return-object v0

    :cond_1
    move-wide v1, v0

    goto :goto_3

    :cond_2
    move-wide v0, v5

    goto :goto_2

    :cond_3
    move-wide v3, v0

    goto :goto_1
.end method

.method public b(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;
    .locals 14

    .prologue
    const-wide v3, 0x41731bf84570a3d7L    # 2.003750834E7

    const-wide v12, 0x412e848000000000L    # 1000000.0

    const-wide v10, 0x4066800000000000L    # 180.0

    .line 192
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v0

    mul-double/2addr v0, v10

    div-double/2addr v0, v3

    double-to-float v0, v0

    .line 193
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v1

    mul-double/2addr v1, v10

    div-double/2addr v1, v3

    double-to-float v1, v1

    .line 195
    const-wide v2, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    float-to-double v6, v1

    const-wide v8, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v6, v8

    div-double/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    const-wide v6, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v4, v6

    mul-double v1, v2, v4

    double-to-float v1, v1

    .line 198
    new-instance v2, Lcom/amap/api/a/ac;

    float-to-double v3, v1

    mul-double/2addr v3, v12

    double-to-int v1, v3

    float-to-double v3, v0

    mul-double/2addr v3, v12

    double-to-int v0, v3

    invoke-direct {v2, v1, v0}, Lcom/amap/api/a/ac;-><init>(II)V

    return-object v2
.end method
