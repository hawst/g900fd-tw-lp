.class Lcom/amap/api/a/ch;
.super Lcom/amap/api/a/f;
.source "ZoomCtlAnim.java"


# static fields
.field static h:F


# instance fields
.field public e:Lcom/amap/api/a/b;

.field public f:F

.field public g:F

.field public i:I

.field public j:Z

.field private k:Landroid/view/animation/Animation$AnimationListener;

.field private l:F

.field private m:F

.field private n:F

.field private o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/amap/api/a/ch;->h:F

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/a/b;Landroid/view/animation/Animation$AnimationListener;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 35
    const/16 v0, 0xa0

    const/16 v1, 0x28

    invoke-direct {p0, v0, v1}, Lcom/amap/api/a/f;-><init>(II)V

    .line 19
    iput-boolean v2, p0, Lcom/amap/api/a/ch;->p:Z

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/a/ch;->i:I

    .line 32
    iput-boolean v2, p0, Lcom/amap/api/a/ch;->j:Z

    .line 37
    iput-object p1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    .line 38
    iput-object p2, p0, Lcom/amap/api/a/ch;->k:Landroid/view/animation/Animation$AnimationListener;

    .line 39
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 5

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/amap/api/a/ch;->o:Z

    if-eqz v0, :cond_0

    .line 44
    sget v0, Lcom/amap/api/a/ch;->h:F

    iget v1, p0, Lcom/amap/api/a/ch;->n:F

    add-float/2addr v0, v1

    sput v0, Lcom/amap/api/a/ch;->h:F

    .line 47
    :goto_0
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 48
    sget v1, Lcom/amap/api/a/ch;->h:F

    sget v2, Lcom/amap/api/a/ch;->h:F

    iget v3, p0, Lcom/amap/api/a/ch;->f:F

    iget v4, p0, Lcom/amap/api/a/ch;->g:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 49
    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    sget v2, Lcom/amap/api/a/ch;->h:F

    invoke-virtual {v1, v2}, Lcom/amap/api/a/b;->c(F)Z

    .line 50
    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/b;->b(Landroid/graphics/Matrix;)Z

    .line 52
    return-void

    .line 46
    :cond_0
    sget v0, Lcom/amap/api/a/ch;->h:F

    iget v1, p0, Lcom/amap/api/a/ch;->n:F

    sub-float/2addr v0, v1

    sput v0, Lcom/amap/api/a/ch;->h:F

    goto :goto_0
.end method

.method public a(FIZFF)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f000000    # 0.5f

    .line 95
    iput-boolean p3, p0, Lcom/amap/api/a/ch;->o:Z

    .line 96
    iput p4, p0, Lcom/amap/api/a/ch;->f:F

    .line 97
    iput p5, p0, Lcom/amap/api/a/ch;->g:F

    .line 99
    iput p1, p0, Lcom/amap/api/a/ch;->l:F

    .line 100
    iget v0, p0, Lcom/amap/api/a/ch;->l:F

    sput v0, Lcom/amap/api/a/ch;->h:F

    .line 102
    iget-boolean v0, p0, Lcom/amap/api/a/ch;->o:Z

    if-eqz v0, :cond_0

    .line 103
    iget v0, p0, Lcom/amap/api/a/ch;->l:F

    iget v1, p0, Lcom/amap/api/a/ch;->d:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/amap/api/a/ch;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/ch;->n:F

    .line 104
    iget v0, p0, Lcom/amap/api/a/ch;->l:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/ch;->m:F

    .line 109
    :goto_0
    return-void

    .line 106
    :cond_0
    iget v0, p0, Lcom/amap/api/a/ch;->l:F

    mul-float/2addr v0, v2

    iget v1, p0, Lcom/amap/api/a/ch;->d:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/amap/api/a/ch;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/ch;->n:F

    .line 107
    iget v0, p0, Lcom/amap/api/a/ch;->l:F

    mul-float/2addr v0, v2

    iput v0, p0, Lcom/amap/api/a/ch;->m:F

    goto :goto_0
.end method

.method public a(IZFF)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 112
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->b:[I

    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    iget-object v1, v1, Lcom/amap/api/a/b;->b:[I

    aget v1, v1, v6

    aput v1, v0, v7

    .line 113
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->b:[I

    aput p1, v0, v6

    .line 114
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->b:[I

    aget v0, v0, v7

    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    iget-object v1, v1, Lcom/amap/api/a/b;->b:[I

    aget v1, v1, v6

    if-ne v0, v1, :cond_0

    .line 138
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->B()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bf;->a(Z)V

    .line 119
    invoke-virtual {p0}, Lcom/amap/api/a/ch;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    const/16 v0, 0xa0

    iput v0, p0, Lcom/amap/api/a/ch;->c:I

    .line 121
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->H()F

    move-result v1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/ch;->a(FIZFF)V

    .line 123
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0, v6}, Lcom/amap/api/a/bf$a;->a(Z)V

    .line 124
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iput-boolean v6, v0, Lcom/amap/api/a/bf$a;->b:Z

    .line 125
    iget-object v0, p0, Lcom/amap/api/a/ch;->k:Landroid/view/animation/Animation$AnimationListener;

    invoke-interface {v0, v8}, Landroid/view/animation/Animation$AnimationListener;->onAnimationStart(Landroid/view/animation/Animation;)V

    .line 126
    invoke-super {p0}, Lcom/amap/api/a/f;->c()V

    goto :goto_0

    .line 128
    :cond_1
    iput-boolean v6, p0, Lcom/amap/api/a/ch;->p:Z

    .line 129
    invoke-virtual {p0}, Lcom/amap/api/a/ch;->d()V

    .line 130
    iget v1, p0, Lcom/amap/api/a/ch;->m:F

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/ch;->a(FIZFF)V

    .line 132
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0, v6}, Lcom/amap/api/a/bf$a;->a(Z)V

    .line 133
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iput-boolean v6, v0, Lcom/amap/api/a/bf$a;->b:Z

    .line 134
    iget-object v0, p0, Lcom/amap/api/a/ch;->k:Landroid/view/animation/Animation$AnimationListener;

    invoke-interface {v0, v8}, Landroid/view/animation/Animation$AnimationListener;->onAnimationStart(Landroid/view/animation/Animation;)V

    .line 135
    invoke-super {p0}, Lcom/amap/api/a/f;->c()V

    .line 136
    iput-boolean v7, p0, Lcom/amap/api/a/ch;->p:Z

    goto :goto_0
.end method

.method protected b()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 56
    iget-boolean v0, p0, Lcom/amap/api/a/ch;->p:Z

    if-eqz v0, :cond_0

    .line 91
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iput-boolean v4, v0, Lcom/amap/api/a/bf$a;->b:Z

    .line 59
    iget-boolean v0, p0, Lcom/amap/api/a/ch;->j:Z

    if-ne v0, v5, :cond_1

    .line 60
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Lcom/amap/api/a/ch;->f:F

    float-to-int v1, v1

    iget v2, p0, Lcom/amap/api/a/ch;->g:F

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 61
    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v1

    iget v2, p0, Lcom/amap/api/a/ch;->f:F

    float-to-int v2, v2

    iget v3, p0, Lcom/amap/api/a/ch;->g:F

    float-to-int v3, v3

    invoke-interface {v1, v2, v3}, Lcom/amap/api/a/bl;->a(II)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 63
    iget-object v2, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v3}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v3

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v3, v1}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v1

    iput-object v1, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 66
    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ba;->a(Landroid/graphics/Point;)V

    .line 67
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v4, v4}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v0

    iget v1, p0, Lcom/amap/api/a/ch;->i:I

    invoke-virtual {v0, v1}, Lcom/amap/api/a/av;->c(I)I

    .line 71
    iget-object v0, p0, Lcom/amap/api/a/ch;->k:Landroid/view/animation/Animation$AnimationListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/animation/Animation$AnimationListener;->onAnimationEnd(Landroid/view/animation/Animation;)V

    .line 72
    iget-boolean v0, p0, Lcom/amap/api/a/ch;->j:Z

    if-ne v0, v5, :cond_2

    .line 73
    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->c()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v2}, Lcom/amap/api/a/bf$d;->d()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 76
    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v2}, Lcom/amap/api/a/bf$d;->c()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v3}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v3

    iget-object v3, v3, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v3}, Lcom/amap/api/a/bf$d;->d()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-interface {v1, v2, v3}, Lcom/amap/api/a/bl;->a(II)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 79
    iget-object v2, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v3}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v3

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v3, v1}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v1

    iput-object v1, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 82
    iget-object v1, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ba;->a(Landroid/graphics/Point;)V

    .line 83
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v4, v4}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0, v4}, Lcom/amap/api/a/b;->e(I)V

    .line 87
    sput v6, Lcom/amap/api/a/ch;->h:F

    .line 88
    sput v6, Lcom/amap/api/a/bg;->k:F

    .line 89
    iget-object v0, p0, Lcom/amap/api/a/ch;->e:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/amap/api/a/bf;->a(Z)V

    .line 90
    invoke-static {}, Lcom/amap/api/a/s;->a()Lcom/amap/api/a/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/s;->b()V

    goto/16 :goto_0
.end method
