.class Lcom/amap/api/a/a/f$a$a;
.super Ljava/io/FilterOutputStream;
.source "DiskLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/a/f$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/a/f$a;


# direct methods
.method private constructor <init>(Lcom/amap/api/a/a/f$a;Ljava/io/OutputStream;)V
    .locals 0

    .prologue
    .line 878
    iput-object p1, p0, Lcom/amap/api/a/a/f$a$a;->a:Lcom/amap/api/a/a/f$a;

    .line 879
    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 880
    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/a/a/f$a;Ljava/io/OutputStream;Lcom/amap/api/a/a/g;)V
    .locals 0

    .prologue
    .line 877
    invoke-direct {p0, p1, p2}, Lcom/amap/api/a/a/f$a$a;-><init>(Lcom/amap/api/a/a/f$a;Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 903
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 907
    :goto_0
    return-void

    .line 904
    :catch_0
    move-exception v0

    .line 905
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->a:Lcom/amap/api/a/a/f$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/a/a/f$a;->a(Lcom/amap/api/a/a/f$a;Z)Z

    goto :goto_0
.end method

.method public flush()V
    .locals 2

    .prologue
    .line 912
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 916
    :goto_0
    return-void

    .line 913
    :catch_0
    move-exception v0

    .line 914
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->a:Lcom/amap/api/a/a/f$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/a/a/f$a;->a(Lcom/amap/api/a/a/f$a;Z)Z

    goto :goto_0
.end method

.method public write(I)V
    .locals 2

    .prologue
    .line 885
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 889
    :goto_0
    return-void

    .line 886
    :catch_0
    move-exception v0

    .line 887
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->a:Lcom/amap/api/a/a/f$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/a/a/f$a;->a(Lcom/amap/api/a/a/f$a;Z)Z

    goto :goto_0
.end method

.method public write([BII)V
    .locals 2

    .prologue
    .line 894
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 898
    :goto_0
    return-void

    .line 895
    :catch_0
    move-exception v0

    .line 896
    iget-object v0, p0, Lcom/amap/api/a/a/f$a$a;->a:Lcom/amap/api/a/a/f$a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/amap/api/a/a/f$a;->a(Lcom/amap/api/a/a/f$a;Z)Z

    goto :goto_0
.end method
