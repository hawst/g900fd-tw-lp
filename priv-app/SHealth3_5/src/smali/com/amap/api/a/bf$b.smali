.class public Lcom/amap/api/a/bf$b;
.super Ljava/lang/Object;
.source "Mediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/bf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public a:Z

.field b:I

.field final synthetic c:Lcom/amap/api/a/bf;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 214
    iput-object p1, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 212
    iput-boolean v0, p0, Lcom/amap/api/a/bf$b;->a:Z

    .line 238
    iput v0, p0, Lcom/amap/api/a/bf$b;->b:I

    .line 215
    invoke-virtual {p0}, Lcom/amap/api/a/bf$b;->d()V

    .line 216
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 264
    iget-object v1, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    iput-boolean v0, v1, Lcom/amap/api/a/bf$d;->a:Z

    move v1, v0

    .line 266
    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 267
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bc;

    .line 268
    if-nez v0, :cond_0

    .line 266
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 271
    :cond_0
    invoke-virtual {v0}, Lcom/amap/api/a/bc;->a()V

    goto :goto_1

    .line 278
    :cond_1
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 284
    .line 285
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bc;

    .line 287
    if-nez v0, :cond_0

    .line 285
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 290
    :cond_0
    invoke-virtual {v0}, Lcom/amap/api/a/bc;->c()V

    goto :goto_1

    .line 292
    :cond_1
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 298
    .line 299
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bc;

    .line 301
    if-nez v0, :cond_0

    .line 299
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 304
    :cond_0
    invoke-virtual {v0}, Lcom/amap/api/a/bc;->b()V

    goto :goto_1

    .line 306
    :cond_1
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 312
    .line 313
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 314
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bc;

    .line 315
    if-nez v0, :cond_0

    .line 313
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 318
    :cond_0
    invoke-virtual {v0}, Lcom/amap/api/a/bc;->g()V

    goto :goto_1

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bf$b;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-static {v0}, Lcom/amap/api/a/bf$c;->b(Lcom/amap/api/a/bf$c;)V

    .line 322
    return-void
.end method
