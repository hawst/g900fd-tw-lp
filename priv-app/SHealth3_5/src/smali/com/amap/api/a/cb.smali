.class Lcom/amap/api/a/cb;
.super Ljava/lang/Object;
.source "UiSettingsDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/aq;


# instance fields
.field final a:Landroid/os/Handler;

.field private b:Lcom/amap/api/a/af;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I


# direct methods
.method constructor <init>(Lcom/amap/api/a/af;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-boolean v1, p0, Lcom/amap/api/a/cb;->c:Z

    .line 10
    iput-boolean v0, p0, Lcom/amap/api/a/cb;->d:Z

    .line 11
    iput-boolean v1, p0, Lcom/amap/api/a/cb;->e:Z

    .line 12
    iput-boolean v1, p0, Lcom/amap/api/a/cb;->f:Z

    .line 13
    iput-boolean v1, p0, Lcom/amap/api/a/cb;->g:Z

    .line 14
    iput-boolean v0, p0, Lcom/amap/api/a/cb;->h:Z

    .line 15
    iput v0, p0, Lcom/amap/api/a/cb;->i:I

    .line 16
    iput v0, p0, Lcom/amap/api/a/cb;->j:I

    .line 23
    new-instance v0, Lcom/amap/api/a/cc;

    invoke-direct {v0, p0}, Lcom/amap/api/a/cc;-><init>(Lcom/amap/api/a/cb;)V

    iput-object v0, p0, Lcom/amap/api/a/cb;->a:Landroid/os/Handler;

    .line 50
    iput-object p1, p0, Lcom/amap/api/a/cb;->b:Lcom/amap/api/a/af;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/amap/api/a/cb;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->f:Z

    return v0
.end method

.method static synthetic b(Lcom/amap/api/a/cb;)Lcom/amap/api/a/af;
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/amap/api/a/cb;->b:Lcom/amap/api/a/af;

    return-object v0
.end method

.method static synthetic c(Lcom/amap/api/a/cb;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->h:Z

    return v0
.end method

.method static synthetic d(Lcom/amap/api/a/cb;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->g:Z

    return v0
.end method

.method static synthetic e(Lcom/amap/api/a/cb;)Z
    .locals 1

    .prologue
    .line 7
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->d:Z

    return v0
.end method


# virtual methods
.method public a(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 101
    iput p1, p0, Lcom/amap/api/a/cb;->i:I

    .line 102
    iget-object v0, p0, Lcom/amap/api/a/cb;->b:Lcom/amap/api/a/af;

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->c(I)V

    .line 103
    return-void
.end method

.method public a(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/amap/api/a/cb;->h:Z

    .line 56
    iget-object v0, p0, Lcom/amap/api/a/cb;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 58
    return-void
.end method

.method public a()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 113
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->h:Z

    return v0
.end method

.method public b(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    iput p1, p0, Lcom/amap/api/a/cb;->j:I

    .line 108
    iget-object v0, p0, Lcom/amap/api/a/cb;->b:Lcom/amap/api/a/af;

    invoke-interface {v0, p1}, Lcom/amap/api/a/af;->d(I)V

    .line 109
    return-void
.end method

.method public b(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/amap/api/a/cb;->f:Z

    .line 63
    iget-object v0, p0, Lcom/amap/api/a/cb;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 65
    return-void
.end method

.method public b()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->f:Z

    return v0
.end method

.method public c(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/amap/api/a/cb;->g:Z

    .line 70
    iget-object v0, p0, Lcom/amap/api/a/cb;->a:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 72
    return-void
.end method

.method public c()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->g:Z

    return v0
.end method

.method public d(Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 77
    iput-boolean p1, p0, Lcom/amap/api/a/cb;->d:Z

    .line 78
    iget-object v0, p0, Lcom/amap/api/a/cb;->a:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 80
    return-void
.end method

.method public d()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->d:Z

    return v0
.end method

.method public e(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/amap/api/a/cb;->c:Z

    .line 86
    return-void
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->c:Z

    return v0
.end method

.method public f(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/amap/api/a/cb;->e:Z

    .line 91
    return-void
.end method

.method public f()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/amap/api/a/cb;->e:Z

    return v0
.end method

.method public g()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 143
    iget v0, p0, Lcom/amap/api/a/cb;->i:I

    return v0
.end method

.method public g(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/amap/api/a/cb;->f(Z)V

    .line 96
    invoke-virtual {p0, p1}, Lcom/amap/api/a/cb;->e(Z)V

    .line 97
    return-void
.end method
