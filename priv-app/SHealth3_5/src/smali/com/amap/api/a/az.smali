.class Lcom/amap/api/a/az;
.super Landroid/view/ViewGroup;
.source "MapOverlayViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/az$a;
    }
.end annotation


# instance fields
.field private a:Lcom/amap/api/a/af;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/a/af;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p2, p0, Lcom/amap/api/a/az;->a:Lcom/amap/api/a/af;

    .line 21
    return-void
.end method

.method private a(Landroid/view/View;IIIII)V
    .locals 3

    .prologue
    .line 189
    .line 191
    and-int/lit8 v0, p6, 0x7

    .line 192
    and-int/lit8 v1, p6, 0x70

    .line 194
    const/4 v2, 0x5

    if-ne v0, v2, :cond_2

    .line 195
    sub-int/2addr p4, p2

    .line 200
    :cond_0
    :goto_0
    const/16 v0, 0x50

    if-ne v1, v0, :cond_3

    .line 201
    sub-int/2addr p5, p3

    .line 205
    :cond_1
    :goto_1
    add-int v0, p4, p2

    add-int v1, p5, p3

    invoke-virtual {p1, p4, p5, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 206
    return-void

    .line 196
    :cond_2
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 197
    div-int/lit8 v0, p2, 0x2

    sub-int/2addr p4, v0

    goto :goto_0

    .line 202
    :cond_3
    const/16 v0, 0x10

    if-ne v1, v0, :cond_1

    .line 203
    div-int/lit8 v0, p3, 0x2

    sub-int/2addr p5, v0

    goto :goto_1
.end method

.method private a(Landroid/view/View;II[I)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 159
    instance-of v0, p1, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 161
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    aput v1, p4, v2

    .line 163
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    aput v0, p4, v3

    .line 166
    :cond_0
    if-lez p2, :cond_1

    if-gtz p3, :cond_2

    .line 167
    :cond_1
    invoke-virtual {p1, v2, v2}, Landroid/view/View;->measure(II)V

    .line 169
    :cond_2
    if-ne p2, v4, :cond_3

    .line 170
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    aput v0, p4, v2

    .line 177
    :goto_0
    if-ne p3, v4, :cond_5

    .line 178
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    aput v0, p4, v3

    .line 185
    :goto_1
    return-void

    .line 171
    :cond_3
    if-ne p2, v5, :cond_4

    .line 172
    invoke-virtual {p0}, Lcom/amap/api/a/az;->getMeasuredWidth()I

    move-result v0

    aput v0, p4, v2

    goto :goto_0

    .line 174
    :cond_4
    aput p2, p4, v2

    goto :goto_0

    .line 179
    :cond_5
    if-ne p3, v5, :cond_6

    .line 180
    invoke-virtual {p0}, Lcom/amap/api/a/az;->getMeasuredHeight()I

    move-result v0

    aput v0, p4, v3

    goto :goto_1

    .line 182
    :cond_6
    aput p3, p4, v3

    goto :goto_1
.end method

.method private a(Landroid/view/View;Lcom/amap/api/a/az$a;)V
    .locals 7

    .prologue
    .line 107
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 108
    iget v1, p2, Lcom/amap/api/a/az$a;->width:I

    iget v2, p2, Lcom/amap/api/a/az$a;->height:I

    invoke-direct {p0, p1, v1, v2, v0}, Lcom/amap/api/a/az;->a(Landroid/view/View;II[I)V

    .line 109
    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v1, 0x1

    aget v3, v0, v1

    iget v4, p2, Lcom/amap/api/a/az$a;->c:I

    iget v5, p2, Lcom/amap/api/a/az$a;->d:I

    iget v6, p2, Lcom/amap/api/a/az$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az;->a(Landroid/view/View;IIIII)V

    .line 110
    return-void
.end method

.method private a(Lcom/amap/api/a/cg;[II)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 145
    invoke-virtual {p1}, Lcom/amap/api/a/cg;->b()I

    move-result v0

    .line 146
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 147
    aget v2, p2, v4

    aget v3, p2, v5

    invoke-virtual {p0}, Lcom/amap/api/a/az;->getWidth()I

    move-result v0

    aget v1, p2, v4

    sub-int v4, v0, v1

    aget v0, p2, v5

    add-int/lit8 v5, v0, 0x14

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az;->a(Landroid/view/View;IIIII)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    if-ne v0, v5, :cond_2

    .line 150
    aget v2, p2, v4

    aget v3, p2, v5

    invoke-virtual {p0}, Lcom/amap/api/a/az;->getWidth()I

    move-result v0

    aget v1, p2, v4

    sub-int v4, v0, v1

    invoke-virtual {p0}, Lcom/amap/api/a/az;->getHeight()I

    move-result v0

    aget v1, p2, v5

    add-int/2addr v0, v1

    div-int/lit8 v5, v0, 0x2

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 152
    :cond_2
    if-nez v0, :cond_0

    .line 153
    aget v2, p2, v4

    aget v3, p2, v5

    invoke-virtual {p0}, Lcom/amap/api/a/az;->getWidth()I

    move-result v0

    aget v1, p2, v4

    sub-int v4, v0, v1

    invoke-virtual {p0}, Lcom/amap/api/a/az;->getHeight()I

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az;->a(Landroid/view/View;IIIII)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;Lcom/amap/api/a/az$a;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const-wide v7, 0x412e848000000000L    # 1000000.0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 113
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 114
    iget v0, p2, Lcom/amap/api/a/az$a;->width:I

    iget v2, p2, Lcom/amap/api/a/az$a;->height:I

    invoke-direct {p0, p1, v0, v2, v5}, Lcom/amap/api/a/az;->a(Landroid/view/View;II[I)V

    .line 115
    instance-of v0, p1, Lcom/amap/api/a/cg;

    if-eqz v0, :cond_1

    .line 116
    check-cast p1, Lcom/amap/api/a/cg;

    .line 117
    iget v0, p2, Lcom/amap/api/a/az$a;->e:I

    invoke-direct {p0, p1, v5, v0}, Lcom/amap/api/a/az;->a(Lcom/amap/api/a/cg;[II)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    instance-of v0, p1, Lcom/amap/api/a/au;

    if-eqz v0, :cond_2

    .line 119
    aget v2, v5, v4

    aget v3, v5, v6

    invoke-virtual {p0}, Lcom/amap/api/a/az;->getWidth()I

    move-result v0

    aget v1, v5, v4

    sub-int v4, v0, v1

    aget v5, v5, v6

    iget v6, p2, Lcom/amap/api/a/az$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 121
    :cond_2
    instance-of v0, p1, Lcom/amap/api/a/w;

    if-eqz v0, :cond_3

    .line 122
    aget v2, v5, v4

    aget v3, v5, v6

    iget v6, p2, Lcom/amap/api/a/az$a;->e:I

    move-object v0, p0

    move-object v1, p1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 123
    :cond_3
    iget-object v0, p2, Lcom/amap/api/a/az$a;->b:Lcom/amap/api/maps2d/model/LatLng;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p2, Lcom/amap/api/a/az$a;->b:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v2, v7

    double-to-int v0, v2

    .line 125
    iget-object v2, p2, Lcom/amap/api/a/az$a;->b:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v2, v7

    double-to-int v2, v2

    .line 126
    new-instance v3, Lcom/amap/api/a/ac;

    invoke-direct {v3, v0, v2}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/az;->a:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->s()Lcom/amap/api/a/bl;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v3, v2}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 133
    :goto_1
    if-eqz v0, :cond_0

    .line 136
    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v2, p2, Lcom/amap/api/a/az$a;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 137
    iget v1, v0, Landroid/graphics/Point;->y:I

    iget v2, p2, Lcom/amap/api/a/az$a;->d:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 139
    aget v2, v5, v4

    aget v3, v5, v6

    iget v4, v0, Landroid/graphics/Point;->x:I

    iget v5, v0, Landroid/graphics/Point;->y:I

    iget v6, p2, Lcom/amap/api/a/az$a;->e:I

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az;->a(Landroid/view/View;IIIII)V

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 103
    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/az;->onLayout(ZIIII)V

    .line 104
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/amap/api/a/az;->getChildCount()I

    move-result v2

    .line 78
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 79
    invoke-virtual {p0, v1}, Lcom/amap/api/a/az;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 80
    if-nez v3, :cond_0

    .line 78
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Lcom/amap/api/a/az$a;

    if-eqz v0, :cond_2

    .line 84
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/az$a;

    .line 87
    iget v4, v0, Lcom/amap/api/a/az$a;->a:I

    if-nez v4, :cond_1

    .line 88
    invoke-direct {p0, v3, v0}, Lcom/amap/api/a/az;->b(Landroid/view/View;Lcom/amap/api/a/az$a;)V

    goto :goto_1

    .line 90
    :cond_1
    invoke-direct {p0, v3, v0}, Lcom/amap/api/a/az;->a(Landroid/view/View;Lcom/amap/api/a/az$a;)V

    goto :goto_1

    .line 94
    :cond_2
    new-instance v0, Lcom/amap/api/a/az$a;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/amap/api/a/az$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96
    invoke-direct {p0, v3, v0}, Lcom/amap/api/a/az;->a(Landroid/view/View;Lcom/amap/api/a/az$a;)V

    goto :goto_1

    .line 100
    :cond_3
    return-void
.end method
