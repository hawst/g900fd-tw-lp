.class public Lcom/amap/api/a/bu;
.super Ljava/lang/Object;
.source "TileOverlayDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/ap;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/bu$a;,
        Lcom/amap/api/a/bu$b;
    }
.end annotation


# static fields
.field private static f:I


# instance fields
.field private a:Lcom/amap/api/a/bv;

.field private b:Lcom/amap/api/maps2d/model/TileProvider;

.field private c:Ljava/lang/Float;

.field private d:Z

.field private e:Lcom/amap/api/a/af;

.field private g:I

.field private h:I

.field private i:I

.field private j:Lcom/amap/api/a/a/k;

.field private k:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/amap/api/a/bu$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z

.field private m:Lcom/amap/api/a/bu$b;

.field private n:Ljava/lang/String;

.field private o:Ljava/nio/FloatBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/amap/api/a/bu;->f:I

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/maps2d/model/TileOverlayOptions;Lcom/amap/api/a/bv;)V
    .locals 6

    .prologue
    const/16 v0, 0x100

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v0, p0, Lcom/amap/api/a/bu;->g:I

    .line 29
    iput v0, p0, Lcom/amap/api/a/bu;->h:I

    .line 30
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/a/bu;->i:I

    .line 32
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 33
    iput-boolean v3, p0, Lcom/amap/api/a/bu;->l:Z

    .line 34
    iput-object v1, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    .line 41
    iput-object v1, p0, Lcom/amap/api/a/bu;->n:Ljava/lang/String;

    .line 154
    iput-object v1, p0, Lcom/amap/api/a/bu;->o:Ljava/nio/FloatBuffer;

    .line 45
    iput-object p2, p0, Lcom/amap/api/a/bu;->a:Lcom/amap/api/a/bv;

    .line 46
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->getTileProvider()Lcom/amap/api/maps2d/model/TileProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu;->b:Lcom/amap/api/maps2d/model/TileProvider;

    .line 47
    iget-object v0, p0, Lcom/amap/api/a/bu;->b:Lcom/amap/api/maps2d/model/TileProvider;

    invoke-interface {v0}, Lcom/amap/api/maps2d/model/TileProvider;->getTileWidth()I

    move-result v0

    iput v0, p0, Lcom/amap/api/a/bu;->g:I

    .line 48
    iget-object v0, p0, Lcom/amap/api/a/bu;->b:Lcom/amap/api/maps2d/model/TileProvider;

    invoke-interface {v0}, Lcom/amap/api/maps2d/model/TileProvider;->getTileHeight()I

    move-result v0

    iput v0, p0, Lcom/amap/api/a/bu;->h:I

    .line 49
    iget v0, p0, Lcom/amap/api/a/bu;->g:I

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(I)I

    move-result v0

    .line 50
    iget v1, p0, Lcom/amap/api/a/bu;->h:I

    invoke-static {v1}, Lcom/amap/api/a/a/p;->a(I)I

    move-result v1

    .line 51
    iget v2, p0, Lcom/amap/api/a/bu;->g:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 52
    iget v2, p0, Lcom/amap/api/a/bu;->h:I

    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    .line 53
    iget-object v2, p0, Lcom/amap/api/a/bu;->o:Ljava/nio/FloatBuffer;

    if-nez v2, :cond_0

    .line 54
    const/16 v2, 0x8

    new-array v2, v2, [F

    aput v4, v2, v3

    aput v1, v2, v5

    const/4 v3, 0x2

    aput v0, v2, v3

    const/4 v3, 0x3

    aput v1, v2, v3

    const/4 v1, 0x4

    aput v0, v2, v1

    const/4 v0, 0x5

    aput v4, v2, v0

    const/4 v0, 0x6

    aput v4, v2, v0

    const/4 v0, 0x7

    aput v4, v2, v0

    invoke-static {v2}, Lcom/amap/api/a/a/p;->a([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu;->o:Ljava/nio/FloatBuffer;

    .line 57
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->getZIndex()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu;->c:Ljava/lang/Float;

    .line 58
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->isVisible()Z

    move-result v0

    iput-boolean v0, p0, Lcom/amap/api/a/bu;->d:Z

    .line 59
    invoke-virtual {p0}, Lcom/amap/api/a/bu;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu;->n:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/amap/api/a/bu;->a:Lcom/amap/api/a/bv;

    invoke-virtual {v0}, Lcom/amap/api/a/bv;->a()Lcom/amap/api/a/af;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu;->e:Lcom/amap/api/a/af;

    .line 62
    iget-object v0, p0, Lcom/amap/api/a/bu;->n:Ljava/lang/String;

    const-string v1, "TileOverlay"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/amap/api/a/bu;->i:I

    .line 64
    new-instance v0, Lcom/amap/api/a/a/j$a;

    iget-object v1, p0, Lcom/amap/api/a/bu;->a:Lcom/amap/api/a/bv;

    invoke-virtual {v1}, Lcom/amap/api/a/bv;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/a/bu;->n:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/a/j$a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->getMemoryCacheEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/a/j$a;->a(Z)V

    .line 72
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->getDiskCacheEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/a/j$a;->b(Z)V

    .line 73
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->getMemCacheSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/a/j$a;->a(I)V

    .line 74
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->getDiskCacheSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/a/j$a;->b(I)V

    .line 75
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/TileOverlayOptions;->getDiskCacheDir()Ljava/lang/String;

    move-result-object v1

    .line 76
    if-eqz v1, :cond_1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 77
    invoke-virtual {v0, v1}, Lcom/amap/api/a/a/j$a;->a(Ljava/lang/String;)V

    .line 81
    :cond_1
    new-instance v1, Lcom/amap/api/a/a/k;

    iget-object v2, p0, Lcom/amap/api/a/bu;->a:Lcom/amap/api/a/bv;

    invoke-virtual {v2}, Lcom/amap/api/a/bv;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/amap/api/a/bu;->g:I

    iget v4, p0, Lcom/amap/api/a/bu;->h:I

    invoke-direct {v1, v2, v3, v4}, Lcom/amap/api/a/a/k;-><init>(Landroid/content/Context;II)V

    iput-object v1, p0, Lcom/amap/api/a/bu;->j:Lcom/amap/api/a/a/k;

    .line 83
    iget-object v1, p0, Lcom/amap/api/a/bu;->j:Lcom/amap/api/a/a/k;

    iget-object v2, p0, Lcom/amap/api/a/bu;->b:Lcom/amap/api/maps2d/model/TileProvider;

    invoke-virtual {v1, v2}, Lcom/amap/api/a/a/k;->a(Lcom/amap/api/maps2d/model/TileProvider;)V

    .line 84
    iget-object v1, p0, Lcom/amap/api/a/bu;->j:Lcom/amap/api/a/a/k;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/a/k;->a(Lcom/amap/api/a/a/j$a;)V

    .line 86
    invoke-virtual {p0, v5}, Lcom/amap/api/a/bu;->b(Z)V

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/amap/api/a/bu;)Lcom/amap/api/a/a/k;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/amap/api/a/bu;->j:Lcom/amap/api/a/a/k;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 37
    sget v0, Lcom/amap/api/a/bu;->f:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/amap/api/a/bu;->f:I

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/amap/api/a/bu;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(III)Ljava/util/ArrayList;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/bu$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/amap/api/a/bu;->e:Lcom/amap/api/a/af;

    invoke-interface {v1}, Lcom/amap/api/a/af;->b()Lcom/amap/api/a/ba;

    move-result-object v12

    .line 184
    iget-object v7, v12, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 185
    iget-object v1, v12, Lcom/amap/api/a/ba;->h:[D

    iget v2, v12, Lcom/amap/api/a/ba;->g:I

    aget-wide v8, v1, v2

    .line 187
    const/4 v1, 0x0

    .line 190
    invoke-virtual {v7}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v2

    iget-wide v4, v12, Lcom/amap/api/a/ba;->c:D

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4070000000000000L    # 256.0

    mul-double/2addr v4, v8

    div-double/2addr v2, v4

    double-to-int v13, v2

    .line 191
    mul-int/lit16 v2, v13, 0x100

    int-to-double v2, v2

    mul-double/2addr v2, v8

    iget-wide v4, v12, Lcom/amap/api/a/ba;->c:D

    add-double/2addr v4, v2

    .line 194
    const-wide/16 v2, 0x0

    .line 195
    iget v6, v12, Lcom/amap/api/a/ba;->b:I

    if-nez v6, :cond_1

    .line 196
    iget-wide v1, v12, Lcom/amap/api/a/ba;->d:D

    invoke-virtual {v7}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v10

    sub-double/2addr v1, v10

    const-wide/high16 v10, 0x4070000000000000L    # 256.0

    mul-double/2addr v10, v8

    div-double/2addr v1, v10

    double-to-int v1, v1

    .line 198
    iget-wide v2, v12, Lcom/amap/api/a/ba;->d:D

    mul-int/lit16 v6, v1, 0x100

    int-to-double v10, v6

    mul-double/2addr v10, v8

    sub-double/2addr v2, v10

    move v10, v1

    .line 206
    :goto_0
    new-instance v1, Lcom/amap/api/a/ac;

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    .line 209
    iget-object v5, v12, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    move-object v2, v12

    move-object v3, v1

    move-object v4, v7

    move-wide v6, v8

    invoke-virtual/range {v2 .. v7}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;

    move-result-object v14

    .line 212
    new-instance v1, Lcom/amap/api/a/bu$a;

    iget v5, v12, Lcom/amap/api/a/ba;->g:I

    const/4 v6, -0x1

    move-object/from16 v2, p0

    move v3, v13

    move v4, v10

    invoke-direct/range {v1 .. v6}, Lcom/amap/api/a/bu$a;-><init>(Lcom/amap/api/a/bu;IIII)V

    .line 214
    iput-object v14, v1, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    .line 215
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 216
    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    const/4 v1, 0x1

    move v11, v1

    .line 225
    :goto_1
    const/4 v9, 0x0

    .line 226
    sub-int v2, v13, v11

    :goto_2
    add-int v1, v13, v11

    if-gt v2, v1, :cond_2

    .line 228
    add-int v3, v10, v11

    move-object v1, v12

    move v4, v13

    move v5, v10

    move-object v6, v14

    move/from16 v7, p2

    move/from16 v8, p3

    .line 230
    invoke-virtual/range {v1 .. v8}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v16

    .line 233
    if-eqz v16, :cond_0

    .line 234
    if-nez v9, :cond_9

    .line 235
    const/4 v1, 0x1

    .line 237
    :goto_3
    new-instance v4, Lcom/amap/api/a/bu$a;

    iget v8, v12, Lcom/amap/api/a/ba;->g:I

    const/4 v9, -0x1

    move-object/from16 v5, p0

    move v6, v2

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/amap/api/a/bu$a;-><init>(Lcom/amap/api/a/bu;IIII)V

    .line 239
    move-object/from16 v0, v16

    iput-object v0, v4, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    .line 240
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v9, v1

    .line 243
    :cond_0
    sub-int v3, v10, v11

    move-object v1, v12

    move v4, v13

    move v5, v10

    move-object v6, v14

    move/from16 v7, p2

    move/from16 v8, p3

    .line 245
    invoke-virtual/range {v1 .. v8}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v16

    .line 248
    if-eqz v16, :cond_b

    .line 249
    if-nez v9, :cond_a

    .line 250
    const/4 v9, 0x1

    move v1, v9

    .line 252
    :goto_4
    new-instance v4, Lcom/amap/api/a/bu$a;

    iget v8, v12, Lcom/amap/api/a/ba;->g:I

    const/4 v9, -0x1

    move-object/from16 v5, p0

    move v6, v2

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/amap/api/a/bu$a;-><init>(Lcom/amap/api/a/bu;IIII)V

    .line 254
    move-object/from16 v0, v16

    iput-object v0, v4, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    .line 255
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    :goto_5
    add-int/lit8 v2, v2, 0x1

    move v9, v1

    goto :goto_2

    .line 200
    :cond_1
    iget v6, v12, Lcom/amap/api/a/ba;->b:I

    const/4 v10, 0x1

    if-ne v6, v10, :cond_c

    .line 201
    invoke-virtual {v7}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v1

    iget-wide v10, v12, Lcom/amap/api/a/ba;->d:D

    sub-double/2addr v1, v10

    const-wide/high16 v10, 0x4070000000000000L    # 256.0

    mul-double/2addr v10, v8

    div-double/2addr v1, v10

    double-to-int v1, v1

    .line 202
    add-int/lit8 v2, v1, 0x1

    mul-int/lit16 v2, v2, 0x100

    int-to-double v2, v2

    mul-double/2addr v2, v8

    move v10, v1

    goto/16 :goto_0

    .line 260
    :cond_2
    add-int v1, v10, v11

    add-int/lit8 v3, v1, -0x1

    :goto_6
    sub-int v1, v10, v11

    if-le v3, v1, :cond_4

    .line 261
    add-int v2, v13, v11

    move-object v1, v12

    move v4, v13

    move v5, v10

    move-object v6, v14

    move/from16 v7, p2

    move/from16 v8, p3

    .line 264
    invoke-virtual/range {v1 .. v8}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v16

    .line 267
    if-eqz v16, :cond_3

    .line 268
    if-nez v9, :cond_8

    .line 269
    const/4 v1, 0x1

    .line 271
    :goto_7
    new-instance v4, Lcom/amap/api/a/bu$a;

    iget v8, v12, Lcom/amap/api/a/ba;->g:I

    const/4 v9, -0x1

    move-object/from16 v5, p0

    move v6, v2

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/amap/api/a/bu$a;-><init>(Lcom/amap/api/a/bu;IIII)V

    .line 273
    move-object/from16 v0, v16

    iput-object v0, v4, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    .line 274
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v9, v1

    .line 278
    :cond_3
    sub-int v2, v13, v11

    move-object v1, v12

    move v4, v13

    move v5, v10

    move-object v6, v14

    move/from16 v7, p2

    move/from16 v8, p3

    .line 280
    invoke-virtual/range {v1 .. v8}, Lcom/amap/api/a/ba;->a(IIIILandroid/graphics/PointF;II)Landroid/graphics/PointF;

    move-result-object v16

    .line 283
    if-eqz v16, :cond_7

    .line 284
    if-nez v9, :cond_6

    .line 285
    const/4 v9, 0x1

    move v1, v9

    .line 287
    :goto_8
    new-instance v4, Lcom/amap/api/a/bu$a;

    iget v8, v12, Lcom/amap/api/a/ba;->g:I

    const/4 v9, -0x1

    move-object/from16 v5, p0

    move v6, v2

    move v7, v3

    invoke-direct/range {v4 .. v9}, Lcom/amap/api/a/bu$a;-><init>(Lcom/amap/api/a/bu;IIII)V

    .line 289
    move-object/from16 v0, v16

    iput-object v0, v4, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    .line 290
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    :goto_9
    add-int/lit8 v3, v3, -0x1

    move v9, v1

    goto :goto_6

    .line 296
    :cond_4
    if-nez v9, :cond_5

    .line 300
    return-object v15

    .line 224
    :cond_5
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto/16 :goto_1

    :cond_6
    move v1, v9

    goto :goto_8

    :cond_7
    move v1, v9

    goto :goto_9

    :cond_8
    move v1, v9

    goto :goto_7

    :cond_9
    move v1, v9

    goto/16 :goto_3

    :cond_a
    move v1, v9

    goto/16 :goto_4

    :cond_b
    move v1, v9

    goto/16 :goto_5

    :cond_c
    move v10, v1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/amap/api/a/bu;III)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/a/bu;->a(III)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/amap/api/a/bu;Ljava/util/List;IZ)Z
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/amap/api/a/bu;->a(Ljava/util/List;IZ)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;IZ)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/a/bu$a;",
            ">;IZ)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 305
    if-nez p1, :cond_0

    move v0, v2

    .line 343
    :goto_0
    return v0

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_1

    move v0, v2

    .line 309
    goto :goto_0

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bu$a;

    .line 313
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amap/api/a/bu$a;

    .line 314
    invoke-virtual {v0, v1}, Lcom/amap/api/a/bu$a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v3

    .line 319
    :goto_2
    if-nez v1, :cond_2

    .line 320
    invoke-virtual {v0}, Lcom/amap/api/a/bu$a;->b()V

    goto :goto_1

    .line 323
    :cond_4
    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 325
    iget-object v0, p0, Lcom/amap/api/a/bu;->e:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->h()F

    move-result v0

    float-to-int v0, v0

    if-gt p2, v0, :cond_5

    iget-object v0, p0, Lcom/amap/api/a/bu;->e:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->i()F

    move-result v0

    float-to-int v0, v0

    if-ge p2, v0, :cond_6

    :cond_5
    move v0, v2

    .line 327
    goto :goto_0

    .line 329
    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 330
    if-gtz v1, :cond_7

    move v0, v2

    .line 331
    goto :goto_0

    .line 335
    :cond_7
    :goto_3
    if-ge v2, v1, :cond_9

    .line 336
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bu$a;

    .line 337
    if-nez v0, :cond_8

    .line 335
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 340
    :cond_8
    iget-object v4, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    iget-object v4, p0, Lcom/amap/api/a/bu;->j:Lcom/amap/api/a/a/k;

    invoke-virtual {v4, p3, v0}, Lcom/amap/api/a/a/k;->a(ZLcom/amap/api/a/bu$a;)V

    goto :goto_4

    :cond_9
    move v0, v3

    .line 343
    goto :goto_0

    :cond_a
    move v1, v2

    goto :goto_2
.end method

.method static synthetic b(Lcom/amap/api/a/bu;)Lcom/amap/api/a/af;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/amap/api/a/bu;->e:Lcom/amap/api/a/af;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    invoke-virtual {v0}, Lcom/amap/api/a/bu$b;->a()Lcom/amap/api/a/a/b$d;

    move-result-object v0

    sget-object v1, Lcom/amap/api/a/a/b$d;->b:Lcom/amap/api/a/a/b$d;

    if-ne v0, v1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bu$b;->a(Z)Z

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bu$a;

    .line 96
    invoke-virtual {v0}, Lcom/amap/api/a/bu$a;->b()V

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 99
    iget-object v0, p0, Lcom/amap/api/a/bu;->j:Lcom/amap/api/a/a/k;

    invoke-virtual {v0}, Lcom/amap/api/a/a/k;->g()V

    .line 100
    iget-object v0, p0, Lcom/amap/api/a/bu;->a:Lcom/amap/api/a/bv;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/bv;->b(Lcom/amap/api/a/ap;)Z

    .line 101
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 118
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu;->c:Ljava/lang/Float;

    .line 119
    iget-object v0, p0, Lcom/amap/api/a/bu;->a:Lcom/amap/api/a/bv;

    invoke-virtual {v0}, Lcom/amap/api/a/bv;->c()V

    .line 120
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 159
    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 179
    :cond_0
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bu;->k:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/bu$a;

    .line 163
    if-eqz v0, :cond_2

    .line 167
    :try_start_0
    iget-object v2, v0, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    .line 168
    iget-object v3, v0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v2, :cond_2

    .line 170
    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    .line 171
    iget-object v0, v0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    iget v3, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v3, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    const-string v2, "TileOverlayDelegateImp"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x70

    invoke-static {v2, v0, v3}, Lcom/amap/api/a/a/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/amap/api/a/bu;->d:Z

    .line 130
    if-eqz p1, :cond_0

    .line 131
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/a/bu;->b(Z)V

    .line 133
    :cond_0
    return-void
.end method

.method public a(Lcom/amap/api/a/ap;)Z
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/a/ap;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/a/bu;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    :cond_0
    const/4 v0, 0x1

    .line 146
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/amap/api/a/bu;->j:Lcom/amap/api/a/a/k;

    invoke-virtual {v0}, Lcom/amap/api/a/a/k;->f()V

    .line 106
    return-void
.end method

.method public b(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 348
    iget-boolean v0, p0, Lcom/amap/api/a/bu;->l:Z

    if-eqz v0, :cond_0

    .line 357
    :goto_0
    return-void

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    invoke-virtual {v0}, Lcom/amap/api/a/bu$b;->a()Lcom/amap/api/a/a/b$d;

    move-result-object v0

    sget-object v1, Lcom/amap/api/a/a/b$d;->b:Lcom/amap/api/a/a/b$d;

    if-ne v0, v1, :cond_1

    .line 353
    iget-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/bu$b;->a(Z)Z

    .line 355
    :cond_1
    new-instance v0, Lcom/amap/api/a/bu$b;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/a/bu$b;-><init>(Lcom/amap/api/a/bu;Z)V

    iput-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    .line 356
    iget-object v0, p0, Lcom/amap/api/a/bu;->m:Lcom/amap/api/a/bu$b;

    new-array v1, v2, [Lcom/amap/api/a/af;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/amap/api/a/bu;->e:Lcom/amap/api/a/af;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bu$b;->c([Ljava/lang/Object;)Lcom/amap/api/a/a/b;

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/amap/api/a/bu;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 111
    const-string v0, "TileOverlay"

    invoke-static {v0}, Lcom/amap/api/a/bu;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu;->n:Ljava/lang/String;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bu;->n:Ljava/lang/String;

    return-object v0
.end method

.method public d()F
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/amap/api/a/bu;->c:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/amap/api/a/bu;->d:Z

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 151
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
