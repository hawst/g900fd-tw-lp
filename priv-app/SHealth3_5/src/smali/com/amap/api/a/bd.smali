.class public Lcom/amap/api/a/bd;
.super Ljava/lang/Object;
.source "MapServerUrl.java"


# static fields
.field private static g:Lcom/amap/api/a/bd;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v0, "http://wprd01.is.autonavi.com"

    iput-object v0, p0, Lcom/amap/api/a/bd;->a:Ljava/lang/String;

    .line 19
    const-string v0, "http://tm.mapabc.com"

    iput-object v0, p0, Lcom/amap/api/a/bd;->b:Ljava/lang/String;

    .line 20
    const-string v0, "http://restapi.amap.com"

    iput-object v0, p0, Lcom/amap/api/a/bd;->c:Ljava/lang/String;

    .line 21
    const-string v0, "http://ds.mapabc.com:8888"

    iput-object v0, p0, Lcom/amap/api/a/bd;->d:Ljava/lang/String;

    .line 22
    const-string v0, "http://mst01.is.autonavi.com"

    iput-object v0, p0, Lcom/amap/api/a/bd;->e:Ljava/lang/String;

    .line 23
    const-string v0, "http://tmds.mapabc.com"

    iput-object v0, p0, Lcom/amap/api/a/bd;->f:Ljava/lang/String;

    .line 9
    return-void
.end method

.method public static declared-synchronized a()Lcom/amap/api/a/bd;
    .locals 2

    .prologue
    .line 12
    const-class v1, Lcom/amap/api/a/bd;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/amap/api/a/bd;->g:Lcom/amap/api/a/bd;

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Lcom/amap/api/a/bd;

    invoke-direct {v0}, Lcom/amap/api/a/bd;-><init>()V

    sput-object v0, Lcom/amap/api/a/bd;->g:Lcom/amap/api/a/bd;

    .line 15
    :cond_0
    sget-object v0, Lcom/amap/api/a/bd;->g:Lcom/amap/api/a/bd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 12
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public b()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 28
    const-string v0, ""

    .line 29
    new-instance v1, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 30
    const v2, 0x186a0

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 31
    rem-int/lit8 v1, v1, 0x4

    .line 32
    packed-switch v1, :pswitch_data_0

    .line 70
    :goto_0
    iput-object v0, p0, Lcom/amap/api/a/bd;->a:Ljava/lang/String;

    .line 71
    iget-object v0, p0, Lcom/amap/api/a/bd;->a:Ljava/lang/String;

    return-object v0

    .line 34
    :pswitch_0
    sget v0, Lcom/amap/api/a/x;->f:I

    if-ne v0, v4, :cond_0

    .line 35
    const-string v0, "http://wprd01.is.autonavi.com"

    goto :goto_0

    .line 37
    :cond_0
    const-string v0, "http://webrd01.is.autonavi.com"

    goto :goto_0

    .line 43
    :pswitch_1
    sget v0, Lcom/amap/api/a/x;->f:I

    if-ne v0, v4, :cond_1

    .line 44
    const-string v0, "http://wprd02.is.autonavi.com"

    goto :goto_0

    .line 46
    :cond_1
    const-string v0, "http://webrd02.is.autonavi.com"

    goto :goto_0

    .line 53
    :pswitch_2
    sget v0, Lcom/amap/api/a/x;->f:I

    if-ne v0, v4, :cond_2

    .line 54
    const-string v0, "http://wprd03.is.autonavi.com"

    goto :goto_0

    .line 56
    :cond_2
    const-string v0, "http://webrd03.is.autonavi.com"

    goto :goto_0

    .line 62
    :pswitch_3
    sget v0, Lcom/amap/api/a/x;->f:I

    if-ne v0, v4, :cond_3

    .line 63
    const-string v0, "http://wprd04.is.autonavi.com"

    goto :goto_0

    .line 65
    :cond_3
    const-string v0, "http://webrd04.is.autonavi.com"

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/amap/api/a/bd;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 83
    const-string v0, ""

    .line 84
    new-instance v1, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 85
    const v2, 0x186a0

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 86
    rem-int/lit8 v1, v1, 0x4

    .line 87
    packed-switch v1, :pswitch_data_0

    .line 102
    :goto_0
    iput-object v0, p0, Lcom/amap/api/a/bd;->e:Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/amap/api/a/bd;->e:Ljava/lang/String;

    return-object v0

    .line 89
    :pswitch_0
    const-string v0, "http://mst01.is.autonavi.com"

    goto :goto_0

    .line 92
    :pswitch_1
    const-string v0, "http://mst02.is.autonavi.com"

    goto :goto_0

    .line 95
    :pswitch_2
    const-string v0, "http://mst03.is.autonavi.com"

    goto :goto_0

    .line 98
    :pswitch_3
    const-string v0, "http://mst04.is.autonavi.com"

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
