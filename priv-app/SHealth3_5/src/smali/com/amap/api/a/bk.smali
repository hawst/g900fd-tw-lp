.class Lcom/amap/api/a/bk;
.super Ljava/lang/Object;
.source "PolylineDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/am;


# instance fields
.field a:F

.field b:F

.field c:F

.field d:F

.field e:[F

.field f:[F

.field private g:Lcom/amap/api/a/b;

.field private h:F

.field private i:I

.field private j:F

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore2d/IPoint;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation
.end field

.field private q:Lcom/amap/api/maps2d/model/LatLngBounds;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/amap/api/a/bk;->h:F

    .line 24
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/amap/api/a/bk;->i:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/bk;->j:F

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/bk;->k:Z

    .line 27
    iput-boolean v1, p0, Lcom/amap/api/a/bk;->l:Z

    .line 28
    iput-boolean v1, p0, Lcom/amap/api/a/bk;->m:Z

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bk;->p:Ljava/util/List;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/bk;->q:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 259
    const/16 v0, 0x1e0

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/amap/api/a/bk;->e:[F

    .line 260
    const/16 v0, 0x3c

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/amap/api/a/bk;->f:[F

    .line 39
    iput-object p1, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    .line 41
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/bk;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bk;->n:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private l()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 138
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 139
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 140
    if-eqz v0, :cond_0

    .line 141
    new-instance v3, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v3}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 142
    iget-object v4, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    iget v5, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    invoke-virtual {v4, v5, v0, v3}, Lcom/amap/api/a/b;->b(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 143
    new-instance v0, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v3, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v6, v3, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 148
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method a(Lcom/autonavi/amap/mapcore2d/IPoint;Lcom/autonavi/amap/mapcore2d/IPoint;Lcom/autonavi/amap/mapcore2d/IPoint;DI)Lcom/autonavi/amap/mapcore2d/IPoint;
    .locals 9

    .prologue
    .line 309
    new-instance v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 310
    iget v1, p2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v2, p1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    sub-int/2addr v1, v2

    int-to-double v1, v1

    .line 311
    iget v3, p2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v4, p1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    sub-int/2addr v3, v4

    int-to-double v3, v3

    .line 312
    mul-double v5, v3, v3

    mul-double v7, v1, v1

    div-double/2addr v5, v7

    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    add-double/2addr v5, v7

    .line 313
    int-to-double v7, p6

    mul-double/2addr v7, p4

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    div-double v5, v7, v5

    iget v7, p3, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-double v7, v7

    add-double/2addr v5, v7

    double-to-int v5, v5

    iput v5, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 314
    iget v5, p3, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v6, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    sub-int/2addr v5, v6

    int-to-double v5, v5

    mul-double/2addr v3, v5

    div-double v1, v3, v1

    iget v3, p3, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-double v3, v3

    add-double/2addr v1, v3

    double-to-int v1, v1

    iput v1, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 316
    return-object v0
.end method

.method public a(F)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 207
    iput p1, p0, Lcom/amap/api/a/bk;->j:F

    .line 208
    return-void
.end method

.method public a(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 193
    iput p1, p0, Lcom/amap/api/a/bk;->i:I

    .line 194
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/bk;->a:F

    .line 195
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/bk;->b:F

    .line 196
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/bk;->c:F

    .line 197
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Lcom/amap/api/a/bk;->d:F

    .line 198
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 264
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/amap/api/a/bk;->h:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 269
    new-instance v1, Lcom/amap/api/a/ac;

    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v4, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    invoke-direct {v1, v4, v0}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 271
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 272
    iget-object v4, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    invoke-virtual {v4}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v4

    invoke-interface {v4, v1, v0}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    .line 275
    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    move v1, v2

    .line 277
    :goto_1
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 278
    new-instance v4, Lcom/amap/api/a/ac;

    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v5, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    invoke-direct {v4, v5, v0}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 280
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 281
    iget-object v5, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    invoke-virtual {v5}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v5

    invoke-interface {v5, v4, v0}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    .line 283
    iget v4, v0, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 277
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 286
    :cond_2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 287
    invoke-virtual {p0}, Lcom/amap/api/a/bk;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 288
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 289
    invoke-virtual {p0}, Lcom/amap/api/a/bk;->g()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 290
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 291
    iget-boolean v1, p0, Lcom/amap/api/a/bk;->l:Z

    if-eqz v1, :cond_3

    .line 292
    invoke-virtual {p0}, Lcom/amap/api/a/bk;->g()F

    move-result v1

    float-to-int v1, v1

    .line 293
    new-instance v4, Landroid/graphics/DashPathEffect;

    const/4 v5, 0x4

    new-array v5, v5, [F

    mul-int/lit8 v6, v1, 0x3

    int-to-float v6, v6

    aput v6, v5, v7

    int-to-float v6, v1

    aput v6, v5, v2

    const/4 v2, 0x2

    mul-int/lit8 v6, v1, 0x3

    int-to-float v6, v6

    aput v6, v5, v2

    const/4 v2, 0x3

    int-to-float v1, v1

    aput v1, v5, v2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v4, v5, v1}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 295
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 297
    :cond_3
    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method a(Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;Ljava/util/List;Lcom/amap/api/maps2d/model/LatLngBounds$Builder;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/amap/api/maps2d/model/LatLng;",
            "Lcom/amap/api/maps2d/model/LatLng;",
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore2d/IPoint;",
            ">;",
            "Lcom/amap/api/maps2d/model/LatLngBounds$Builder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 367
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v2, v4

    const-wide v4, 0x4066800000000000L    # 180.0

    div-double v20, v2, v4

    .line 369
    new-instance v17, Lcom/amap/api/maps2d/model/LatLng;

    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 373
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 376
    move-object/from16 v0, v17

    iget-wide v2, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    const/16 v22, 0x1

    .line 378
    :goto_0
    new-instance v7, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v7}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    move-object/from16 v0, p1

    iget-wide v3, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual/range {v2 .. v7}, Lcom/amap/api/a/b;->a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 380
    new-instance v13, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v13}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 381
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    move-object/from16 v0, p2

    iget-wide v9, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    move-object/from16 v0, p2

    iget-wide v11, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual/range {v8 .. v13}, Lcom/amap/api/a/b;->a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 382
    new-instance v19, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct/range {v19 .. v19}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 383
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    move-object/from16 v0, v17

    iget-wide v15, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    move-object/from16 v0, v17

    iget-wide v0, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    move-wide/from16 v17, v0

    invoke-virtual/range {v14 .. v19}, Lcom/amap/api/a/b;->a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 387
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double v2, v2, v20

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 389
    iget v4, v7, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v5, v13, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    iget v6, v7, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v8, v13, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    sub-int/2addr v6, v8

    int-to-double v8, v6

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v4, v8

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double v8, v8, v20

    invoke-static {v8, v9}, Ljava/lang/Math;->tan(D)D

    move-result-wide v8

    mul-double v20, v4, v8

    move-object/from16 v16, p0

    move-object/from16 v17, v7

    move-object/from16 v18, v13

    .line 394
    invoke-virtual/range {v16 .. v22}, Lcom/amap/api/a/bk;->a(Lcom/autonavi/amap/mapcore2d/IPoint;Lcom/autonavi/amap/mapcore2d/IPoint;Lcom/autonavi/amap/mapcore2d/IPoint;DI)Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v4

    .line 397
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 398
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    invoke-interface {v5, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/amap/api/a/bk;->a(Ljava/util/List;Ljava/util/List;D)V

    .line 404
    return-void

    .line 376
    :cond_0
    const/16 v22, -0x1

    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/amap/api/a/bk;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/amap/api/a/bk;->l:Z

    if-eqz v0, :cond_1

    .line 122
    :cond_0
    iput-object p1, p0, Lcom/amap/api/a/bk;->p:Ljava/util/List;

    .line 124
    :cond_1
    invoke-virtual {p0, p1}, Lcom/amap/api/a/bk;->b(Ljava/util/List;)V

    .line 125
    return-void
.end method

.method a(Ljava/util/List;Ljava/util/List;D)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore2d/IPoint;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/autonavi/amap/mapcore2d/IPoint;",
            ">;D)V"
        }
    .end annotation

    .prologue
    .line 333
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 359
    :cond_0
    return-void

    .line 338
    :cond_1
    const/high16 v4, 0x3f800000    # 1.0f

    .line 340
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v2, 0xa

    if-gt v3, v2, :cond_0

    .line 341
    int-to-float v2, v3

    const/high16 v5, 0x41200000    # 10.0f

    div-float v5, v2, v5

    .line 342
    new-instance v6, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v6}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 343
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    float-to-double v9, v5

    sub-double/2addr v7, v9

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    float-to-double v11, v5

    sub-double/2addr v9, v11

    mul-double/2addr v7, v9

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-double v9, v2

    mul-double/2addr v7, v9

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v9, v2

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    float-to-double v13, v5

    sub-double/2addr v11, v13

    mul-double/2addr v9, v11

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-double v11, v2

    mul-double/2addr v9, v11

    mul-double v9, v9, p3

    add-double/2addr v7, v9

    mul-float v9, v5, v5

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v9

    float-to-double v9, v2

    add-double/2addr v7, v9

    .line 346
    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    float-to-double v11, v5

    sub-double/2addr v9, v11

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    float-to-double v13, v5

    sub-double/2addr v11, v13

    mul-double/2addr v9, v11

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-double v11, v2

    mul-double/2addr v9, v11

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v11, v2

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    float-to-double v15, v5

    sub-double/2addr v13, v15

    mul-double/2addr v11, v13

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-double v13, v2

    mul-double/2addr v11, v13

    mul-double v11, v11, p3

    add-double/2addr v9, v11

    mul-float v11, v5, v5

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v2, v2, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-float v2, v2

    mul-float/2addr v2, v11

    float-to-double v11, v2

    add-double/2addr v9, v11

    .line 350
    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    float-to-double v13, v5

    sub-double/2addr v11, v13

    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    float-to-double v15, v5

    sub-double/2addr v13, v15

    mul-double/2addr v11, v13

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v13, v2

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v17, v0

    sub-double v15, v15, v17

    mul-double/2addr v13, v15

    mul-double v13, v13, p3

    add-double/2addr v11, v13

    mul-float v2, v5, v5

    float-to-double v13, v2

    add-double/2addr v11, v13

    .line 351
    const-wide/high16 v13, 0x3ff0000000000000L    # 1.0

    float-to-double v15, v5

    sub-double/2addr v13, v15

    const-wide/high16 v15, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v17, v0

    sub-double v15, v15, v17

    mul-double/2addr v13, v15

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, v5

    float-to-double v15, v2

    const-wide/high16 v17, 0x3ff0000000000000L    # 1.0

    float-to-double v0, v5

    move-wide/from16 v19, v0

    sub-double v17, v17, v19

    mul-double v15, v15, v17

    mul-double v15, v15, p3

    add-double/2addr v13, v15

    mul-float v2, v5, v5

    float-to-double v15, v2

    add-double/2addr v13, v15

    .line 353
    div-double/2addr v7, v11

    double-to-int v2, v7

    iput v2, v6, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 354
    div-double v7, v9, v13

    double-to-int v2, v7

    iput v2, v6, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 356
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 340
    int-to-float v2, v3

    add-float/2addr v2, v4

    float-to-int v2, v2

    move v3, v2

    goto/16 :goto_0
.end method

.method public a(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/amap/api/a/bk;->k:Z

    .line 218
    return-void
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 242
    iget-object v2, p0, Lcom/amap/api/a/bk;->q:Lcom/amap/api/maps2d/model/LatLngBounds;

    if-nez v2, :cond_1

    .line 249
    :cond_0
    :goto_0
    return v0

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->x()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v2

    .line 246
    if-nez v2, :cond_2

    move v0, v1

    .line 247
    goto :goto_0

    .line 249
    :cond_2
    iget-object v3, p0, Lcom/amap/api/a/bk;->q:Lcom/amap/api/maps2d/model/LatLngBounds;

    invoke-virtual {v2, v3}, Lcom/amap/api/maps2d/model/LatLngBounds;->contains(Lcom/amap/api/maps2d/model/LatLngBounds;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/a/bk;->q:Lcom/amap/api/maps2d/model/LatLngBounds;

    invoke-virtual {v3, v2}, Lcom/amap/api/maps2d/model/LatLngBounds;->intersects(Lcom/amap/api/maps2d/model/LatLngBounds;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/amap/api/a/ak;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 228
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/a/ak;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/a/bk;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 230
    :cond_0
    const/4 v0, 0x1

    .line 232
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    invoke-virtual {p0}, Lcom/amap/api/a/bk;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/b;->a(Ljava/lang/String;)Z

    .line 109
    return-void
.end method

.method public b(F)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 153
    iput p1, p0, Lcom/amap/api/a/bk;->h:F

    .line 154
    return-void
.end method

.method b(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 48
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    invoke-static {}, Lcom/amap/api/maps2d/model/LatLngBounds;->builder()Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v8

    .line 53
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 54
    if-eqz p1, :cond_6

    .line 55
    const/4 v6, 0x0

    .line 56
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v7, v6

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/amap/api/maps2d/model/LatLng;

    .line 57
    if-eqz v6, :cond_2

    invoke-virtual {v6, v7}, Lcom/amap/api/maps2d/model/LatLng;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 60
    iget-boolean v0, p0, Lcom/amap/api/a/bk;->m:Z

    if-nez v0, :cond_4

    .line 61
    new-instance v5, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 62
    iget-object v0, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    iget-wide v1, v6, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, v6, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/b;->a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 63
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-virtual {v8, v6}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    :cond_3
    :goto_2
    move-object v7, v6

    .line 83
    goto :goto_1

    .line 66
    :cond_4
    if-eqz v7, :cond_3

    .line 67
    iget-wide v0, v6, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    iget-wide v2, v7, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v0, v0, v2

    if-gez v0, :cond_5

    .line 68
    new-instance v5, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 69
    iget-object v0, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    iget-wide v1, v7, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, v7, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/b;->a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 71
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-virtual {v8, v7}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    .line 73
    new-instance v5, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 74
    iget-object v0, p0, Lcom/amap/api/a/bk;->g:Lcom/amap/api/a/b;

    iget-wide v1, v6, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, v6, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/b;->a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 75
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-virtual {v8, v6}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    goto :goto_2

    .line 78
    :cond_5
    iget-object v0, p0, Lcom/amap/api/a/bk;->o:Ljava/util/List;

    invoke-virtual {p0, v7, v6, v0, v8}, Lcom/amap/api/a/bk;->a(Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;Ljava/util/List;Lcom/amap/api/maps2d/model/LatLngBounds$Builder;)V

    goto :goto_2

    .line 86
    :cond_6
    invoke-virtual {v8}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bk;->q:Lcom/amap/api/maps2d/model/LatLngBounds;

    goto/16 :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 163
    iput-boolean p1, p0, Lcom/amap/api/a/bk;->l:Z

    .line 164
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/amap/api/a/bk;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 114
    const-string v0, "Polyline"

    invoke-static {v0}, Lcom/amap/api/a/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bk;->n:Ljava/lang/String;

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bk;->n:Ljava/lang/String;

    return-object v0
.end method

.method public c(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/amap/api/a/bk;->m:Z

    if-eq v0, p1, :cond_0

    .line 178
    iput-boolean p1, p0, Lcom/amap/api/a/bk;->m:Z

    .line 180
    :cond_0
    return-void
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 212
    iget v0, p0, Lcom/amap/api/a/bk;->j:F

    return v0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/amap/api/a/bk;->k:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 237
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public g()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 158
    iget v0, p0, Lcom/amap/api/a/bk;->h:F

    return v0
.end method

.method public h()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 202
    iget v0, p0, Lcom/amap/api/a/bk;->i:I

    return v0
.end method

.method public i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/LatLng;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/amap/api/a/bk;->m:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/amap/api/a/bk;->l:Z

    if-eqz v0, :cond_1

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bk;->p:Ljava/util/List;

    .line 132
    :goto_0
    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/amap/api/a/bk;->l()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/amap/api/a/bk;->l:Z

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/amap/api/a/bk;->m:Z

    return v0
.end method

.method public m()V
    .locals 0

    .prologue
    .line 302
    return-void
.end method
