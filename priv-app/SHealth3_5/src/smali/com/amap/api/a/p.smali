.class Lcom/amap/api/a/p;
.super Ljava/lang/Object;
.source "BitmapManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/p$a;
    }
.end annotation


# instance fields
.field protected final a:[Lcom/amap/api/a/p$a;

.field protected final b:I

.field protected final c:I

.field protected final d:[Lcom/amap/api/a/p$a;

.field e:Landroid/graphics/Paint;

.field f:Landroid/graphics/Path;

.field private g:Z

.field private h:J


# direct methods
.method public constructor <init>(IIZJ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/p;->g:Z

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/a/p;->h:J

    .line 236
    iput-object v2, p0, Lcom/amap/api/a/p;->e:Landroid/graphics/Paint;

    .line 237
    iput-object v2, p0, Lcom/amap/api/a/p;->f:Landroid/graphics/Path;

    .line 27
    iput p1, p0, Lcom/amap/api/a/p;->b:I

    .line 28
    iput p2, p0, Lcom/amap/api/a/p;->c:I

    .line 30
    iput-boolean p3, p0, Lcom/amap/api/a/p;->g:Z

    .line 31
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p4

    iput-wide v0, p0, Lcom/amap/api/a/p;->h:J

    .line 33
    iget v0, p0, Lcom/amap/api/a/p;->b:I

    if-lez v0, :cond_0

    .line 34
    iget v0, p0, Lcom/amap/api/a/p;->b:I

    new-array v0, v0, [Lcom/amap/api/a/p$a;

    iput-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    .line 35
    iget v0, p0, Lcom/amap/api/a/p;->c:I

    new-array v0, v0, [Lcom/amap/api/a/p$a;

    iput-object v0, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    iput-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    .line 38
    iput-object v2, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    goto :goto_0
.end method

.method private a(Landroid/graphics/Bitmap;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/a/by;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 241
    new-instance v0, Lcom/amap/api/a/p$1;

    invoke-direct {v0, p0, p2}, Lcom/amap/api/a/p$1;-><init>(Lcom/amap/api/a/p;Ljava/util/List;)V

    .line 296
    new-instance v1, Lcom/amap/api/a/n;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/amap/api/a/n;-><init>(Landroid/graphics/Bitmap$Config;)V

    .line 297
    invoke-virtual {v1, p1}, Lcom/amap/api/a/n;->a(Landroid/graphics/Bitmap;)V

    .line 298
    invoke-virtual {v1, v0}, Lcom/amap/api/a/n;->a(Lcom/amap/api/a/o;)V

    .line 300
    return-void
.end method

.method private c()J
    .locals 2

    .prologue
    .line 167
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method protected a()I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 171
    move v0, v1

    .line 174
    :goto_0
    iget v2, p0, Lcom/amap/api/a/p;->c:I

    if-ge v0, v2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 174
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v4, v1

    .line 178
    :goto_1
    iget v0, p0, Lcom/amap/api/a/p;->b:I

    if-ge v4, v0, :cond_3

    .line 179
    iget-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v0, v4

    move v3, v1

    .line 181
    :goto_2
    iget v0, p0, Lcom/amap/api/a/p;->c:I

    if-ge v3, v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v3

    if-nez v0, :cond_2

    .line 183
    iget-object v0, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aput-object v2, v0, v3

    .line 178
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 185
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v3

    iget-wide v5, v0, Lcom/amap/api/a/p$a;->d:J

    iget-wide v7, v2, Lcom/amap/api/a/p$a;->d:J

    cmp-long v0, v5, v7

    if-lez v0, :cond_6

    .line 186
    iget-object v0, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v3

    .line 187
    iget-object v5, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aput-object v2, v5, v3

    .line 181
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_2

    .line 193
    :cond_3
    const/4 v0, -0x1

    move v2, v0

    move v0, v1

    .line 194
    :goto_4
    iget v3, p0, Lcom/amap/api/a/p;->c:I

    if-ge v0, v3, :cond_5

    .line 195
    iget-object v3, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aget-object v3, v3, v0

    if-eqz v3, :cond_4

    .line 196
    iget-object v3, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aget-object v3, v3, v0

    iput-boolean v1, v3, Lcom/amap/api/a/p$a;->c:Z

    .line 197
    if-gez v2, :cond_4

    .line 198
    iget-object v2, p0, Lcom/amap/api/a/p;->d:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/amap/api/a/p$a;->e:I

    .line 194
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 202
    :cond_5
    return v2

    :cond_6
    move-object v0, v2

    goto :goto_3
.end method

.method protected a(Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 43
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-ne v0, v4, :cond_0

    move v0, v2

    .line 68
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 47
    :goto_1
    iget v3, p0, Lcom/amap/api/a/p;->b:I

    if-ge v0, v3, :cond_5

    .line 48
    iget-object v3, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v3, v3, v0

    if-nez v3, :cond_2

    .line 47
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 51
    :cond_2
    iget-object v3, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/amap/api/a/p$a;->b:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 54
    iget-object v3, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v3, v3, v0

    iget-boolean v3, v3, Lcom/amap/api/a/p$a;->c:Z

    if-nez v3, :cond_3

    move v0, v2

    .line 55
    goto :goto_0

    .line 57
    :cond_3
    iget-boolean v3, p0, Lcom/amap/api/a/p;->g:Z

    if-ne v3, v4, :cond_4

    .line 58
    invoke-direct {p0}, Lcom/amap/api/a/p;->c()J

    move-result-wide v3

    .line 59
    iget-object v5, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v5, v5, v0

    iget-wide v5, v5, Lcom/amap/api/a/p$a;->f:J

    sub-long/2addr v3, v5

    iget-wide v5, p0, Lcom/amap/api/a/p;->h:J

    cmp-long v3, v3, v5

    if-lez v3, :cond_4

    .line 60
    iget-object v3, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v0, v3, v0

    iput-boolean v1, v0, Lcom/amap/api/a/p$a;->c:Z

    move v0, v2

    .line 61
    goto :goto_0

    .line 64
    :cond_4
    iget-object v1, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v1, v1, v0

    invoke-direct {p0}, Lcom/amap/api/a/p;->c()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/amap/api/a/p$a;->d:J

    goto :goto_0

    :cond_5
    move v0, v2

    .line 68
    goto :goto_0
.end method

.method protected declared-synchronized a([BLjava/io/InputStream;ZLjava/util/List;Ljava/lang/String;)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/io/InputStream;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/a/by;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v0, -0x1

    .line 96
    monitor-enter p0

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    if-nez p4, :cond_1

    .line 159
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 99
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/p;->b()I

    move-result v1

    .line 100
    if-gez v1, :cond_2

    .line 101
    invoke-virtual {p0}, Lcom/amap/api/a/p;->a()I

    move-result v1

    .line 103
    :cond_2
    if-ltz v1, :cond_0

    .line 106
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 112
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 113
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    .line 116
    :cond_3
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->g:Ljava/util/List;

    if-eqz v2, :cond_4

    .line 117
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 118
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/amap/api/a/p$a;->g:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :cond_4
    if-ne p3, v6, :cond_9

    if-eqz p1, :cond_9

    .line 123
    :try_start_1
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    const/4 v3, 0x0

    array-length v4, p1

    invoke-static {p1, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    :cond_5
    :goto_1
    if-eqz p4, :cond_6

    .line 136
    :try_start_2
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    const/16 v3, 0x100

    const/16 v4, 0x100

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    .line 139
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2, p4}, Lcom/amap/api/a/p;->a(Landroid/graphics/Bitmap;Ljava/util/List;)V

    .line 142
    :cond_6
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    if-eqz v2, :cond_0

    .line 145
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/amap/api/a/p$a;->g:Ljava/util/List;

    if-eqz v2, :cond_0

    .line 150
    :cond_7
    iget-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v1

    if-eqz v0, :cond_8

    .line 151
    iget-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v1

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/amap/api/a/p$a;->c:Z

    .line 152
    iget-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v1

    iput-object p5, v0, Lcom/amap/api/a/p$a;->b:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v1

    invoke-direct {p0}, Lcom/amap/api/a/p;->c()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/amap/api/a/p$a;->d:J

    .line 155
    iget-boolean v0, p0, Lcom/amap/api/a/p;->g:Z

    if-ne v0, v6, :cond_8

    .line 156
    iget-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, v1

    invoke-direct {p0}, Lcom/amap/api/a/p;->c()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/amap/api/a/p$a;->f:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    move v0, v1

    .line 159
    goto/16 :goto_0

    .line 127
    :cond_9
    if-eqz p2, :cond_5

    .line 129
    :try_start_3
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v1

    invoke-static {p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 131
    :catch_0
    move-exception v2

    goto :goto_1

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 125
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method protected a(I)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 72
    if-ltz p1, :cond_0

    iget v1, p0, Lcom/amap/api/a/p;->b:I

    if-lt p1, v1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-object v0

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v1, v1, p1

    if-eqz v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/amap/api/a/p$a;->a:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method protected b()I
    .locals 3

    .prologue
    .line 207
    const/4 v1, -0x1

    .line 209
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/amap/api/a/p;->b:I

    if-ge v0, v2, :cond_2

    .line 210
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v0

    if-nez v2, :cond_0

    .line 211
    iget-object v1, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    new-instance v2, Lcom/amap/api/a/p$a;

    invoke-direct {v2, p0}, Lcom/amap/api/a/p$a;-><init>(Lcom/amap/api/a/p;)V

    aput-object v2, v1, v0

    .line 212
    iget-object v1, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v1, v1, v0

    iput v0, v1, Lcom/amap/api/a/p$a;->e:I

    .line 219
    :goto_1
    return v0

    .line 214
    :cond_0
    iget-object v2, p0, Lcom/amap/api/a/p;->a:[Lcom/amap/api/a/p$a;

    aget-object v2, v2, v0

    iget-boolean v2, v2, Lcom/amap/api/a/p$a;->c:Z

    if-nez v2, :cond_1

    if-gez v1, :cond_1

    move v1, v0

    .line 209
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 219
    goto :goto_1
.end method
