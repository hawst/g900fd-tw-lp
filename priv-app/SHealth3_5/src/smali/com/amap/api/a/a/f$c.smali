.class public final Lcom/amap/api/a/a/f$c;
.super Ljava/lang/Object;
.source "DiskLruCache.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/a/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/a/f;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private final d:[Ljava/io/InputStream;


# direct methods
.method private constructor <init>(Lcom/amap/api/a/a/f;Ljava/lang/String;J[Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 753
    iput-object p1, p0, Lcom/amap/api/a/a/f$c;->a:Lcom/amap/api/a/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 754
    iput-object p2, p0, Lcom/amap/api/a/a/f$c;->b:Ljava/lang/String;

    .line 755
    iput-wide p3, p0, Lcom/amap/api/a/a/f$c;->c:J

    .line 756
    iput-object p5, p0, Lcom/amap/api/a/a/f$c;->d:[Ljava/io/InputStream;

    .line 757
    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/a/a/f;Ljava/lang/String;J[Ljava/io/InputStream;Lcom/amap/api/a/a/g;)V
    .locals 0

    .prologue
    .line 748
    invoke-direct/range {p0 .. p5}, Lcom/amap/api/a/a/f$c;-><init>(Lcom/amap/api/a/a/f;Ljava/lang/String;J[Ljava/io/InputStream;)V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/amap/api/a/a/f$c;->d:[Ljava/io/InputStream;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public close()V
    .locals 4

    .prologue
    .line 784
    iget-object v1, p0, Lcom/amap/api/a/a/f$c;->d:[Ljava/io/InputStream;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 785
    invoke-static {v3}, Lcom/amap/api/a/a/f;->a(Ljava/io/Closeable;)V

    .line 784
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 787
    :cond_0
    return-void
.end method
