.class Lcom/amap/api/a/av$b;
.super Ljava/lang/Object;
.source "MapController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/av;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/av;

.field private b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Lcom/amap/api/a/ch;


# direct methods
.method constructor <init>(Lcom/amap/api/a/av;)V
    .locals 1

    .prologue
    .line 495
    iput-object p1, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 496
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/av$b;->b:Ljava/util/LinkedList;

    .line 497
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/av$b;->c:Z

    .line 498
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    return-void
.end method

.method private a(IIIZ)V
    .locals 4

    .prologue
    .line 538
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    if-nez v0, :cond_0

    .line 539
    new-instance v0, Lcom/amap/api/a/ch;

    iget-object v1, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v1}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/amap/api/a/ch;-><init>(Lcom/amap/api/a/b;Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    iput-boolean p4, v0, Lcom/amap/api/a/ch;->j:Z

    .line 544
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    iput p1, v0, Lcom/amap/api/a/ch;->i:I

    .line 545
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    const/4 v1, 0x0

    int-to-float v2, p2

    int-to-float v3, p3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/amap/api/a/ch;->a(IZFF)V

    .line 550
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/av$b;->c:Z

    .line 552
    return-void
.end method

.method private b(IIIZ)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 555
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    if-nez v0, :cond_0

    .line 556
    new-instance v0, Lcom/amap/api/a/ch;

    iget-object v1, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v1}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/amap/api/a/ch;-><init>(Lcom/amap/api/a/b;Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    iput p1, v0, Lcom/amap/api/a/ch;->i:I

    .line 561
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    iput-boolean p4, v0, Lcom/amap/api/a/ch;->j:Z

    .line 563
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    iget-boolean v0, v0, Lcom/amap/api/a/ch;->j:Z

    if-ne v0, v4, :cond_1

    .line 564
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p2, p3}, Landroid/graphics/Point;-><init>(II)V

    .line 565
    iget-object v1, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v1}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Lcom/amap/api/a/bl;->a(II)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 567
    iget-object v2, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v2}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v3}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v3

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v3, v1}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v1

    iput-object v1, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 569
    iget-object v1, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v1}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ba;->a(Landroid/graphics/Point;)V

    .line 572
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/av$b;->d:Lcom/amap/api/a/ch;

    int-to-float v1, p2

    int-to-float v2, p3

    invoke-virtual {v0, p1, v4, v1, v2}, Lcom/amap/api/a/ch;->a(IZFF)V

    .line 577
    iput-boolean v4, p0, Lcom/amap/api/a/av$b;->c:Z

    .line 579
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/amap/api/a/av$b;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 508
    return-void
.end method

.method public a(IIIZZ)V
    .locals 0

    .prologue
    .line 512
    if-nez p4, :cond_0

    .line 514
    invoke-direct {p0, p3, p1, p2, p5}, Lcom/amap/api/a/av$b;->a(IIIZ)V

    .line 535
    :goto_0
    return-void

    .line 517
    :cond_0
    invoke-direct {p0, p3, p1, p2, p5}, Lcom/amap/api/a/av$b;->b(IIIZ)V

    goto :goto_0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v1

    .line 630
    iget-object v0, p0, Lcom/amap/api/a/av$b;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 631
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/av$b;->c:Z

    .line 637
    iget-object v0, p0, Lcom/amap/api/a/av$b;->a:Lcom/amap/api/a/av;

    invoke-static {v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$a;->a()V

    .line 642
    :goto_0
    return-void

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/av$b;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    .line 640
    invoke-virtual {v1, v0}, Lcom/amap/api/a/b;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 623
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 619
    return-void
.end method
