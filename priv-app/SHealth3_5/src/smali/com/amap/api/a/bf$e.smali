.class public Lcom/amap/api/a/bf$e;
.super Ljava/lang/Object;
.source "Mediator.java"

# interfaces
.implements Lcom/amap/api/a/bl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/bf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "e"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/bf;

.field private b:I

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf;)V
    .locals 1

    .prologue
    .line 335
    iput-object p1, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 416
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/bf$e;->b:I

    .line 417
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bf$e;->c:Ljava/util/HashMap;

    return-void
.end method

.method private a(Z)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 473
    iget-object v0, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->c()I

    move-result v0

    .line 474
    iget-object v1, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->d()I

    move-result v1

    .line 475
    invoke-virtual {p0, v2, v1}, Lcom/amap/api/a/bf$e;->a(II)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 476
    invoke-virtual {p0, v0, v2}, Lcom/amap/api/a/bf$e;->a(II)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 478
    if-eqz p1, :cond_0

    invoke-virtual {v1}, Lcom/amap/api/a/ac;->a()I

    move-result v1

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->a()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v1}, Lcom/amap/api/a/ac;->b()I

    move-result v1

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->b()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(F)F
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 420
    .line 421
    iget-object v1, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    .line 422
    iget-object v2, p0, Lcom/amap/api/a/bf$e;->c:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    const/16 v3, 0x1e

    if-gt v2, v3, :cond_0

    iget v2, p0, Lcom/amap/api/a/bf$e;->b:I

    if-eq v1, v2, :cond_1

    .line 423
    :cond_0
    iput v1, p0, Lcom/amap/api/a/bf$e;->b:I

    .line 424
    iget-object v1, p0, Lcom/amap/api/a/bf$e;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 426
    :cond_1
    iget-object v1, p0, Lcom/amap/api/a/bf$e;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 427
    invoke-virtual {p0, v4, v4}, Lcom/amap/api/a/bf$e;->a(II)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 428
    const/16 v2, 0xa

    invoke-virtual {p0, v4, v2}, Lcom/amap/api/a/bf$e;->a(II)Lcom/amap/api/a/ac;

    move-result-object v2

    .line 429
    iget-object v3, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v3, v1, v2}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;)F

    move-result v1

    .line 431
    cmpg-float v2, v1, v0

    if-gtz v2, :cond_2

    .line 438
    :goto_0
    return v0

    .line 434
    :cond_2
    const/high16 v0, 0x41200000    # 10.0f

    div-float v1, p1, v1

    mul-float/2addr v0, v1

    .line 435
    iget-object v1, p0, Lcom/amap/api/a/bf$e;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    :cond_3
    iget-object v0, p0, Lcom/amap/api/a/bf$e;->c:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/amap/api/a/bf$e;->a(Z)I

    move-result v0

    return v0
.end method

.method public a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    .line 346
    iget-object v0, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    .line 347
    iget-object v0, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v2, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v4, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v4, v4, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v4, v4, Lcom/amap/api/a/ba;->h:[D

    aget-wide v4, v4, v1

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;

    move-result-object v0

    .line 352
    iget-object v1, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v1}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/a/b;->G()Lcom/amap/api/a/bg;

    move-result-object v1

    .line 354
    iget-object v2, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-static {v2}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    .line 356
    iget-boolean v3, v1, Lcom/amap/api/a/bg;->n:Z

    if-eqz v3, :cond_4

    .line 357
    iget-boolean v2, v1, Lcom/amap/api/a/bg;->m:Z

    if-eqz v2, :cond_3

    .line 358
    sget v2, Lcom/amap/api/a/bg;->k:F

    iget v3, v0, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    int-to-float v3, v3

    iget-object v4, v1, Lcom/amap/api/a/bg;->g:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget-object v3, v1, Lcom/amap/api/a/bg;->g:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, v3

    iget-object v3, v1, Lcom/amap/api/a/bg;->h:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, v1, Lcom/amap/api/a/bg;->g:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 361
    sget v3, Lcom/amap/api/a/bg;->k:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    int-to-float v0, v0

    iget-object v4, v1, Lcom/amap/api/a/bg;->g:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v4

    mul-float/2addr v0, v3

    iget-object v3, v1, Lcom/amap/api/a/bg;->g:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v3

    iget-object v3, v1, Lcom/amap/api/a/bg;->h:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget-object v1, v1, Lcom/amap/api/a/bg;->g:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float v1, v3, v1

    add-float v3, v0, v1

    .line 364
    float-to-int v1, v2

    .line 365
    float-to-int v0, v3

    .line 366
    float-to-double v4, v2

    int-to-double v6, v1

    add-double/2addr v6, v8

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_0

    .line 367
    add-int/lit8 v1, v1, 0x1

    .line 369
    :cond_0
    float-to-double v2, v3

    int-to-double v4, v0

    add-double/2addr v4, v8

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_1

    .line 370
    add-int/lit8 v0, v0, 0x1

    .line 390
    :cond_1
    :goto_0
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 391
    if-eqz p2, :cond_2

    .line 392
    iget v0, v2, Landroid/graphics/Point;->x:I

    iput v0, p2, Landroid/graphics/Point;->x:I

    .line 393
    iget v0, v2, Landroid/graphics/Point;->y:I

    iput v0, p2, Landroid/graphics/Point;->y:I

    .line 395
    :cond_2
    return-object v2

    .line 373
    :cond_3
    iget v1, v0, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    .line 374
    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    goto :goto_0

    .line 377
    :cond_4
    sget v1, Lcom/amap/api/a/ch;->h:F

    iget v3, v0, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    iget v4, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v1, v3

    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    add-float/2addr v3, v1

    .line 379
    sget v1, Lcom/amap/api/a/ch;->h:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    iget v4, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iget v1, v2, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    add-float v2, v0, v1

    .line 381
    float-to-int v1, v3

    .line 382
    float-to-int v0, v2

    .line 383
    float-to-double v3, v3

    int-to-double v5, v1

    add-double/2addr v5, v8

    cmpl-double v3, v3, v5

    if-ltz v3, :cond_5

    .line 384
    add-int/lit8 v1, v1, 0x1

    .line 386
    :cond_5
    float-to-double v2, v2

    int-to-double v4, v0

    add-double/2addr v4, v8

    cmpl-double v2, v2, v4

    if-ltz v2, :cond_1

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(II)Lcom/amap/api/a/ac;
    .locals 7

    .prologue
    .line 407
    iget-object v0, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v4

    .line 408
    new-instance v1, Landroid/graphics/PointF;

    int-to-float v0, p1

    int-to-float v2, p2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 409
    iget-object v0, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v2, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v5, v5, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v5, v5, Lcom/amap/api/a/ba;->h:[D

    aget-wide v4, v5, v4

    iget-object v6, p0, Lcom/amap/api/a/bf$e;->a:Lcom/amap/api/a/bf;

    iget-object v6, v6, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v6, v6, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    invoke-virtual/range {v0 .. v6}, Lcom/amap/api/a/ba;->a(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 462
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/amap/api/a/bf$e;->a(Z)I

    move-result v0

    return v0
.end method
