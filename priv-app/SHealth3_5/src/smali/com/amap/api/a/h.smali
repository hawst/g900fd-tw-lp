.class abstract Lcom/amap/api/a/h;
.super Lcom/amap/api/a/bc;
.source "AsyncServer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/amap/api/a/bc;"
    }
.end annotation


# instance fields
.field protected volatile a:Z

.field protected b:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field protected c:Lcom/amap/api/a/br;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/amap/api/a/br",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected d:Lcom/amap/api/a/bs;

.field private g:Ljava/lang/Runnable;

.field private h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/amap/api/a/bc;-><init>(Lcom/amap/api/a/bf;Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/h;->a:Z

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    .line 105
    new-instance v0, Lcom/amap/api/a/i;

    invoke-direct {v0, p0}, Lcom/amap/api/a/i;-><init>(Lcom/amap/api/a/h;)V

    iput-object v0, p0, Lcom/amap/api/a/h;->g:Ljava/lang/Runnable;

    .line 194
    new-instance v0, Lcom/amap/api/a/j;

    invoke-direct {v0, p0}, Lcom/amap/api/a/j;-><init>(Lcom/amap/api/a/h;)V

    iput-object v0, p0, Lcom/amap/api/a/h;->h:Ljava/lang/Runnable;

    .line 18
    iget-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    .line 21
    :cond_0
    new-instance v0, Lcom/amap/api/a/bs;

    invoke-virtual {p0}, Lcom/amap/api/a/h;->e()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/h;->h:Ljava/lang/Runnable;

    iget-object v3, p0, Lcom/amap/api/a/h;->g:Ljava/lang/Runnable;

    invoke-direct {v0, v1, v2, v3}, Lcom/amap/api/a/bs;-><init>(ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    .line 24
    iget-object v0, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    invoke-virtual {v0}, Lcom/amap/api/a/bs;->a()V

    .line 30
    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TT;>;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps2d/AMapException;
        }
    .end annotation
.end method

.method protected abstract a(Ljava/util/ArrayList;Ljava/net/Proxy;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<TT;>;",
            "Ljava/net/Proxy;",
            ")",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps2d/AMapException;
        }
    .end annotation
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/amap/api/a/h;->c:Lcom/amap/api/a/br;

    invoke-virtual {v0}, Lcom/amap/api/a/br;->a()V

    .line 39
    invoke-virtual {p0}, Lcom/amap/api/a/h;->d()V

    .line 40
    iget-object v0, p0, Lcom/amap/api/a/h;->c:Lcom/amap/api/a/br;

    invoke-virtual {v0}, Lcom/amap/api/a/br;->c()V

    .line 41
    iput-object v1, p0, Lcom/amap/api/a/h;->c:Lcom/amap/api/a/br;

    .line 42
    iput-object v1, p0, Lcom/amap/api/a/h;->e:Lcom/amap/api/a/bf;

    .line 43
    iput-object v1, p0, Lcom/amap/api/a/h;->f:Landroid/content/Context;

    .line 44
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Lcom/amap/api/a/bc;->b()V

    .line 49
    invoke-virtual {p0}, Lcom/amap/api/a/h;->d()V

    .line 50
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/h;->a:Z

    .line 61
    iget-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    if-nez v0, :cond_1

    .line 64
    new-instance v0, Lcom/amap/api/a/bs;

    invoke-virtual {p0}, Lcom/amap/api/a/h;->e()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/h;->h:Ljava/lang/Runnable;

    iget-object v3, p0, Lcom/amap/api/a/h;->g:Ljava/lang/Runnable;

    invoke-direct {v0, v1, v2, v3}, Lcom/amap/api/a/bs;-><init>(ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    .line 67
    iget-object v0, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    invoke-virtual {v0}, Lcom/amap/api/a/bs;->a()V

    .line 69
    :cond_1
    return-void
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 72
    iput-boolean v2, p0, Lcom/amap/api/a/h;->a:Z

    .line 74
    iget-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v3

    move v1, v2

    .line 76
    :goto_0
    if-ge v1, v3, :cond_1

    .line 77
    iget-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    .line 79
    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 82
    iget-object v0, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    invoke-virtual {v0, v2}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    .line 76
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 85
    :cond_1
    iput-object v4, p0, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    if-eqz v0, :cond_3

    .line 90
    iget-object v0, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    invoke-virtual {v0}, Lcom/amap/api/a/bs;->b()V

    .line 91
    iput-object v4, p0, Lcom/amap/api/a/h;->d:Lcom/amap/api/a/bs;

    .line 94
    :cond_3
    return-void
.end method

.method protected abstract e()I
.end method

.method protected abstract f()I
.end method
