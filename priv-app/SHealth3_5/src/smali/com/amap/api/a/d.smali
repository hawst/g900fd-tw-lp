.class Lcom/amap/api/a/d;
.super Ljava/lang/Thread;
.source "AMapDelegateImpGLSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/b;


# direct methods
.method constructor <init>(Lcom/amap/api/a/b;)V
    .locals 0

    .prologue
    .line 3083
    iput-object p1, p0, Lcom/amap/api/a/d;->a:Lcom/amap/api/a/b;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 3087
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/amap/api/maps2d/MapsInitializer;->getNetworkEnable()Z
    :try_end_0
    .catch Lcom/amap/api/maps2d/AMapException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    if-nez v0, :cond_1

    .line 3089
    const-wide/16 v0, 0x1388

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/amap/api/maps2d/AMapException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 3090
    :catch_0
    move-exception v0

    .line 3091
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catch Lcom/amap/api/maps2d/AMapException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 3103
    :catch_1
    move-exception v0

    .line 3104
    invoke-virtual {p0}, Lcom/amap/api/a/d;->interrupt()V

    .line 3105
    const-string v1, "AuthFailure"

    invoke-virtual {v0}, Lcom/amap/api/maps2d/AMapException;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3106
    invoke-virtual {v0}, Lcom/amap/api/maps2d/AMapException;->printStackTrace()V

    .line 3110
    :cond_0
    :goto_1
    return-void

    .line 3095
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/amap/api/a/d;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->l(Lcom/amap/api/a/b;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/a/a/e;->a(Landroid/content/Context;)Lcom/amap/api/a/a/e;

    .line 3096
    iget-object v0, p0, Lcom/amap/api/a/d;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->l(Lcom/amap/api/a/b;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/a/k;->a(Landroid/content/Context;)Z

    .line 3098
    sget v0, Lcom/amap/api/a/k;->a:I

    if-nez v0, :cond_0

    .line 3099
    iget-object v0, p0, Lcom/amap/api/a/d;->a:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_3
    .catch Lcom/amap/api/maps2d/AMapException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 3107
    :catch_2
    move-exception v0

    .line 3108
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
