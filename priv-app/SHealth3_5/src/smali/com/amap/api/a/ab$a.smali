.class Lcom/amap/api/a/ab$a;
.super Ljava/lang/Object;
.source "GLOverlayLayer.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/ab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/ab;


# direct methods
.method constructor <init>(Lcom/amap/api/a/ab;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/amap/api/a/ab$a;->a:Lcom/amap/api/a/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 33
    check-cast p1, Lcom/amap/api/a/ak;

    .line 34
    check-cast p2, Lcom/amap/api/a/ak;

    .line 36
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 37
    :try_start_0
    invoke-interface {p1}, Lcom/amap/api/a/ak;->d()F

    move-result v0

    invoke-interface {p2}, Lcom/amap/api/a/ak;->d()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 38
    const/4 v0, 0x1

    .line 46
    :goto_0
    return v0

    .line 39
    :cond_0
    invoke-interface {p1}, Lcom/amap/api/a/ak;->d()F

    move-result v0

    invoke-interface {p2}, Lcom/amap/api/a/ak;->d()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 40
    const/4 v0, -0x1

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 46
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
