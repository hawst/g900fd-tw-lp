.class Lcom/amap/api/a/c;
.super Landroid/os/Handler;
.source "AMapDelegateImpGLSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/b;


# direct methods
.method constructor <init>(Lcom/amap/api/a/b;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 165
    if-nez p1, :cond_1

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 234
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/amap/api/maps2d/model/CameraPosition;

    .line 235
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v1}, Lcom/amap/api/a/b;->h(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v1}, Lcom/amap/api/a/b;->h(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;->onCameraChange(Lcom/amap/api/maps2d/model/CameraPosition;)V

    goto :goto_0

    .line 170
    :pswitch_2
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->L()V

    goto :goto_0

    .line 175
    :pswitch_3
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;->onMapLoaded()V

    goto :goto_0

    .line 183
    :pswitch_4
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :goto_1
    if-eqz v0, :cond_5

    .line 188
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 189
    iget-object v3, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v3}, Lcom/amap/api/a/b;->b(Lcom/amap/api/a/b;)Lcom/amap/api/a/cf;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 190
    iget-object v3, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v3}, Lcom/amap/api/a/b;->b(Lcom/amap/api/a/b;)Lcom/amap/api/a/cf;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/amap/api/a/cf;->onDraw(Landroid/graphics/Canvas;)V

    .line 192
    :cond_2
    iget-object v3, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v3}, Lcom/amap/api/a/b;->c(Lcom/amap/api/a/b;)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v3}, Lcom/amap/api/a/b;->d(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/model/Marker;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 193
    iget-object v3, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v3}, Lcom/amap/api/a/b;->c(Lcom/amap/api/a/b;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->getDrawingCache(Z)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 194
    if-eqz v3, :cond_3

    .line 195
    iget-object v4, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v4}, Lcom/amap/api/a/b;->c(Lcom/amap/api/a/b;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 196
    iget-object v5, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v5}, Lcom/amap/api/a/b;->c(Lcom/amap/api/a/b;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    .line 197
    int-to-float v4, v4

    int-to-float v5, v5

    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 200
    :cond_3
    iget-object v2, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v2}, Lcom/amap/api/a/b;->e(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 201
    iget-object v2, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v2}, Lcom/amap/api/a/b;->e(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;->onMapScreenShot(Landroid/graphics/Bitmap;)V

    .line 208
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0, v1}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/b;Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;)Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    goto/16 :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 185
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_1

    .line 204
    :cond_5
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->e(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 205
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->e(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;->onMapScreenShot(Landroid/graphics/Bitmap;)V

    goto :goto_2

    .line 213
    :pswitch_5
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->f(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$CancelableCallback;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->g(Lcom/amap/api/a/b;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 214
    :cond_6
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->h(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 215
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->i(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    .line 216
    iget-object v2, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-virtual {v2, v4, v0}, Lcom/amap/api/a/b;->a(ZLcom/amap/api/maps2d/model/CameraPosition;)V

    .line 220
    :cond_7
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->f(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$CancelableCallback;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 221
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0, v4}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/b;Z)Z

    .line 222
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->f(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$CancelableCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/maps2d/AMap$CancelableCallback;->onFinish()V

    .line 223
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0, v3}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/b;Z)Z

    .line 225
    :cond_8
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->j(Lcom/amap/api/a/b;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 226
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0, v1}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/b;Lcom/amap/api/maps2d/AMap$CancelableCallback;)Lcom/amap/api/maps2d/AMap$CancelableCallback;

    goto/16 :goto_0

    .line 228
    :cond_9
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0, v3}, Lcom/amap/api/a/b;->b(Lcom/amap/api/a/b;Z)Z

    goto/16 :goto_0

    .line 241
    :pswitch_6
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/r;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v0}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/r;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    .line 245
    :pswitch_7
    new-instance v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget-object v1, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v1}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/a/r;->b()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v2}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/a/r;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>(II)V

    iget-object v1, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v1}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/a/r;->d()F

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v2}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/a/r;->e()F

    move-result v2

    iget-object v3, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v3}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v3

    invoke-virtual {v3}, Lcom/amap/api/a/r;->f()F

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/amap/api/a/t;->a(Lcom/autonavi/amap/mapcore2d/IPoint;FFF)Lcom/amap/api/a/t;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    invoke-static {v1}, Lcom/amap/api/a/b;->k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/amap/api/a/r;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 253
    iput-boolean v4, v0, Lcom/amap/api/a/t;->o:Z

    .line 255
    :cond_a
    iget-object v1, p0, Lcom/amap/api/a/c;->a:Lcom/amap/api/a/b;

    iget-object v1, v1, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ax;->a(Lcom/amap/api/a/t;)V

    goto/16 :goto_0

    .line 168
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 243
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_7
    .end packed-switch
.end method
