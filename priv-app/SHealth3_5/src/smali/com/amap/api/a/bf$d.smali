.class public Lcom/amap/api/a/bf$d;
.super Ljava/lang/Object;
.source "Mediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/bf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "d"
.end annotation


# instance fields
.field public a:Z

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/mapcore2d/MapViewListener;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic c:Lcom/amap/api/a/bf;

.field private d:Lcom/amap/api/a/b;

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/ce;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf;Lcom/amap/api/a/b;)V
    .locals 1

    .prologue
    .line 1172
    iput-object p1, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/bf$d;->a:Z

    .line 1173
    iput-object p2, p0, Lcom/amap/api/a/bf$d;->d:Lcom/amap/api/a/b;

    .line 1174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bf$d;->e:Ljava/util/ArrayList;

    .line 1175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bf$d;->b:Ljava/util/ArrayList;

    .line 1176
    return-void
.end method

.method static synthetic a(Lcom/amap/api/a/bf$d;)Lcom/amap/api/a/b;
    .locals 1

    .prologue
    .line 1162
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->d:Lcom/amap/api/a/b;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget v0, v0, Lcom/amap/api/a/ba;->f:I

    return v0
.end method

.method public a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1182
    .line 1183
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget v0, v0, Lcom/amap/api/a/ba;->g:I

    .line 1184
    if-eq p1, v0, :cond_0

    .line 1185
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iput p1, v0, Lcom/amap/api/a/ba;->g:I

    .line 1187
    :cond_0
    invoke-virtual {p0, v1, v1}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    .line 1188
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 1194
    sget v0, Lcom/amap/api/a/x;->h:I

    if-ne p1, v0, :cond_0

    sget v0, Lcom/amap/api/a/x;->i:I

    if-eq p2, v0, :cond_1

    .line 1196
    :cond_0
    sput p1, Lcom/amap/api/a/x;->h:I

    .line 1197
    sput p2, Lcom/amap/api/a/x;->i:I

    .line 1198
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    .line 1200
    :cond_1
    return-void
.end method

.method public a(Lcom/amap/api/a/ac;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1210
    if-nez p1, :cond_0

    .line 1222
    :goto_0
    return-void

    .line 1214
    :cond_0
    sget-boolean v0, Lcom/amap/api/a/x;->k:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1216
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 1218
    iget-object v1, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iput-object v0, v1, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 1220
    :cond_1
    invoke-virtual {p0, v2, v2}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/a/ce;)V
    .locals 1

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1289
    return-void
.end method

.method public a(ZZ)V
    .locals 2

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/ce;

    .line 1316
    invoke-interface {v0, p1, p2}, Lcom/amap/api/a/ce;->a(ZZ)V

    goto :goto_0

    .line 1318
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    if-eqz v0, :cond_1

    .line 1319
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    iget-object v0, v0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bv;->a(Z)V

    .line 1320
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 1322
    :cond_1
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 1249
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget v0, v0, Lcom/amap/api/a/ba;->e:I

    return v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1242
    if-gtz p1, :cond_0

    .line 1246
    :goto_0
    return-void

    .line 1245
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    sput p1, Lcom/amap/api/a/x;->c:I

    iput p1, v0, Lcom/amap/api/a/ba;->f:I

    goto :goto_0
.end method

.method public b(Lcom/amap/api/a/ac;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1225
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->f()Lcom/amap/api/a/ac;

    move-result-object v0

    .line 1226
    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Lcom/amap/api/a/ac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1228
    sget-boolean v0, Lcom/amap/api/a/x;->k:Z

    if-ne v0, v2, :cond_0

    .line 1229
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 1231
    iget-object v1, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iput-object v0, v1, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    .line 1233
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    .line 1235
    :cond_1
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 1261
    sget v0, Lcom/amap/api/a/x;->h:I

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 1253
    if-gtz p1, :cond_0

    .line 1257
    :goto_0
    return-void

    .line 1256
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    sput p1, Lcom/amap/api/a/x;->d:I

    iput p1, v0, Lcom/amap/api/a/ba;->e:I

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1265
    sget v0, Lcom/amap/api/a/x;->i:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1269
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget v0, v0, Lcom/amap/api/a/ba;->g:I

    return v0
.end method

.method public f()Lcom/amap/api/a/ac;
    .locals 2

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v1, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v1, v1, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 1275
    iget-object v1, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    iget-boolean v1, v1, Lcom/amap/api/a/bf$b;->a:Z

    if-eqz v1, :cond_0

    .line 1276
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v0, v0, Lcom/amap/api/a/ba;->k:Lcom/amap/api/a/ac;

    .line 1279
    :cond_0
    return-object v0
.end method

.method public g()Lcom/amap/api/a/b;
    .locals 1

    .prologue
    .line 1325
    iget-object v0, p0, Lcom/amap/api/a/bf$d;->d:Lcom/amap/api/a/b;

    return-object v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 1354
    return-void
.end method
