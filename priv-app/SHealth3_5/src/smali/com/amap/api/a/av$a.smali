.class Lcom/amap/api/a/av$a;
.super Ljava/lang/Object;
.source "MapController.java"

# interfaces
.implements Lcom/amap/api/a/ca;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/av;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/av;

.field private b:Lcom/amap/api/a/bz;

.field private c:Landroid/os/Message;

.field private d:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/amap/api/a/av;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 421
    iput-object p1, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 422
    iput-object v0, p0, Lcom/amap/api/a/av$a;->b:Lcom/amap/api/a/bz;

    .line 423
    iput-object v0, p0, Lcom/amap/api/a/av$a;->c:Landroid/os/Message;

    .line 424
    iput-object v0, p0, Lcom/amap/api/a/av$a;->d:Ljava/lang/Runnable;

    return-void
.end method

.method private a(Lcom/amap/api/a/ac;I)Lcom/amap/api/a/bz;
    .locals 7

    .prologue
    const/16 v1, 0x1f4

    .line 438
    .line 439
    if-ge p2, v1, :cond_0

    .line 444
    :goto_0
    new-instance v0, Lcom/amap/api/a/bz;

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-static {v3}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v3

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    move-object v4, p1

    move v5, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/bz;-><init>(IILcom/amap/api/a/ac;Lcom/amap/api/a/ac;ILcom/amap/api/a/ca;)V

    return-object v0

    :cond_0
    move v1, p2

    .line 442
    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 450
    iput-object v0, p0, Lcom/amap/api/a/av$a;->b:Lcom/amap/api/a/bz;

    .line 451
    iput-object v0, p0, Lcom/amap/api/a/av$a;->c:Landroid/os/Message;

    .line 452
    iput-object v0, p0, Lcom/amap/api/a/av$a;->d:Ljava/lang/Runnable;

    .line 453
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/amap/api/a/av$a;->b:Lcom/amap/api/a/bz;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/amap/api/a/av$a;->b:Lcom/amap/api/a/bz;

    invoke-virtual {v0}, Lcom/amap/api/a/bz;->d()V

    .line 459
    :cond_0
    return-void
.end method

.method public a(Lcom/amap/api/a/ac;)V
    .locals 4

    .prologue
    const-wide/high16 v2, -0x8000000000000000L

    .line 463
    if-nez p1, :cond_0

    .line 474
    :goto_0
    return-void

    .line 466
    :cond_0
    invoke-virtual {p1}, Lcom/amap/api/a/ac;->d()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/amap/api/a/ac;->c()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 468
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-static {v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 470
    iget-object v1, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;)V

    goto :goto_0

    .line 473
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/a/ac;Landroid/os/Message;Ljava/lang/Runnable;I)V
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-static {v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/amap/api/a/bf$b;->a:Z

    .line 429
    iget-object v0, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-static {v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {p1}, Lcom/amap/api/a/ac;->g()Lcom/amap/api/a/ac;

    move-result-object v1

    iput-object v1, v0, Lcom/amap/api/a/ba;->k:Lcom/amap/api/a/ac;

    .line 430
    invoke-virtual {p0}, Lcom/amap/api/a/av$a;->a()V

    .line 431
    invoke-direct {p0, p1, p4}, Lcom/amap/api/a/av$a;->a(Lcom/amap/api/a/ac;I)Lcom/amap/api/a/bz;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/av$a;->b:Lcom/amap/api/a/bz;

    .line 432
    iput-object p2, p0, Lcom/amap/api/a/av$a;->c:Landroid/os/Message;

    .line 433
    iput-object p3, p0, Lcom/amap/api/a/av$a;->d:Ljava/lang/Runnable;

    .line 434
    iget-object v0, p0, Lcom/amap/api/a/av$a;->b:Lcom/amap/api/a/bz;

    invoke-virtual {v0}, Lcom/amap/api/a/bz;->c()V

    .line 435
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 478
    iget-object v0, p0, Lcom/amap/api/a/av$a;->c:Landroid/os/Message;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/amap/api/a/av$a;->c:Landroid/os/Message;

    invoke-virtual {v0}, Landroid/os/Message;->getTarget()Landroid/os/Handler;

    move-result-object v0

    .line 480
    iget-object v1, p0, Lcom/amap/api/a/av$a;->c:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/av$a;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 483
    iget-object v0, p0, Lcom/amap/api/a/av$a;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 485
    :cond_1
    invoke-direct {p0}, Lcom/amap/api/a/av$a;->c()V

    .line 486
    iget-object v0, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-static {v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    if-eqz v0, :cond_2

    .line 487
    iget-object v0, p0, Lcom/amap/api/a/av$a;->a:Lcom/amap/api/a/av;

    invoke-static {v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/amap/api/a/bf$b;->a:Z

    .line 489
    :cond_2
    return-void
.end method
