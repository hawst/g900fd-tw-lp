.class Lcom/amap/api/a/m;
.super Ljava/lang/Object;
.source "BaseMapTile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/m$a;
    }
.end annotation


# static fields
.field private static a:Landroid/graphics/Paint;

.field private static b:Landroid/graphics/Bitmap;

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 13
    sput-object v0, Lcom/amap/api/a/m;->a:Landroid/graphics/Paint;

    .line 14
    sput-object v0, Lcom/amap/api/a/m;->b:Landroid/graphics/Bitmap;

    .line 15
    const/16 v0, 0xde

    const/16 v1, 0xd7

    const/16 v2, 0xd6

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lcom/amap/api/a/m;->c:I

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/amap/api/a/m;->c:I

    return v0
.end method

.method public static declared-synchronized b()Landroid/graphics/Paint;
    .locals 4

    .prologue
    .line 22
    const-class v1, Lcom/amap/api/a/m;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/amap/api/a/m;->a:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/amap/api/a/m;->a:Landroid/graphics/Paint;

    .line 24
    sget-object v0, Lcom/amap/api/a/m;->a:Landroid/graphics/Paint;

    const v2, -0x777778

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 25
    sget-object v0, Lcom/amap/api/a/m;->a:Landroid/graphics/Paint;

    const/16 v2, 0x5a

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 26
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    .line 28
    sget-object v2, Lcom/amap/api/a/m;->a:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 30
    :cond_0
    sget-object v0, Lcom/amap/api/a/m;->a:Landroid/graphics/Paint;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 26
    :array_0
    .array-data 4
        0x40000000    # 2.0f
        0x40200000    # 2.5f
    .end array-data
.end method

.method public static declared-synchronized c()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 34
    const-class v1, Lcom/amap/api/a/m;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/amap/api/a/m;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/amap/api/a/m$1;

    invoke-direct {v0}, Lcom/amap/api/a/m$1;-><init>()V

    .line 50
    new-instance v2, Lcom/amap/api/a/n;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-direct {v2, v3}, Lcom/amap/api/a/n;-><init>(Landroid/graphics/Bitmap$Config;)V

    .line 51
    const/16 v3, 0x100

    const/16 v4, 0x100

    invoke-virtual {v2, v3, v4}, Lcom/amap/api/a/n;->a(II)V

    .line 53
    invoke-virtual {v2, v0}, Lcom/amap/api/a/n;->a(Lcom/amap/api/a/o;)V

    .line 54
    invoke-virtual {v2}, Lcom/amap/api/a/n;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/amap/api/a/m;->b:Landroid/graphics/Bitmap;

    .line 57
    :cond_0
    sget-object v0, Lcom/amap/api/a/m;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
