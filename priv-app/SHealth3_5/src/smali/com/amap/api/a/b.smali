.class public Lcom/amap/api/a/b;
.super Landroid/view/View;
.source "AMapDelegateImpGLSurfaceView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Lcom/amap/api/a/af;
.implements Lcom/amap/api/a/bg$b;
.implements Lcom/amap/api/a/bp$a;
.implements Lcom/amap/api/a/s$a;
.implements Lcom/amap/api/a/u$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/b$a;,
        Lcom/amap/api/a/b$b;
    }
.end annotation


# instance fields
.field private A:Lcom/amap/api/maps2d/LocationSource;

.field private B:Lcom/amap/api/a/w;

.field private C:Lcom/amap/api/a/a;

.field private D:Z

.field private E:Z

.field private F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

.field private G:Lcom/amap/api/a/r;

.field private H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

.field private I:Z

.field private J:Lcom/amap/api/a/ba;

.field private K:Z

.field private L:Z

.field private M:Landroid/view/View;

.field private N:Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;

.field private O:Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;

.field private P:Lcom/amap/api/maps2d/model/Marker;

.field private Q:Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;

.field private R:Landroid/graphics/drawable/Drawable;

.field private S:Lcom/amap/api/a/an;

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Lcom/amap/api/maps2d/model/Marker;

.field private Z:Z

.field public a:Lcom/amap/api/a/av;

.field private aA:I

.field private aB:Z

.field private aC:Lcom/amap/api/a/b$a;

.field private aD:Ljava/lang/Thread;

.field private aa:Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;

.field private ab:Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;

.field private ac:Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;

.field private ad:Lcom/amap/api/maps2d/AMap$OnMapClickListener;

.field private ae:Z

.field private af:Lcom/amap/api/a/z;

.field private ag:Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

.field private ah:Landroid/graphics/Point;

.field private ai:Landroid/view/GestureDetector;

.field private aj:Lcom/amap/api/a/bg;

.field private ak:Z

.field private al:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/GestureDetector$OnGestureListener;",
            ">;"
        }
    .end annotation
.end field

.field private am:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/bg$b;",
            ">;"
        }
    .end annotation
.end field

.field private an:Landroid/widget/Scroller;

.field private ao:I

.field private ap:I

.field private aq:F

.field private ar:Z

.field private as:F

.field private at:F

.field private au:I

.field private av:I

.field private aw:J

.field private ax:I

.field private ay:I

.field private final az:J

.field b:[I

.field c:Z

.field d:Lcom/amap/api/a/ax;

.field e:Lcom/amap/api/a/cg;

.field public f:Lcom/amap/api/a/az;

.field protected g:Lcom/amap/api/a/aq;

.field public h:Lcom/amap/api/a/bv;

.field public i:Lcom/amap/api/a/ay;

.field final j:Landroid/os/Handler;

.field protected k:Landroid/graphics/Matrix;

.field private l:Landroid/content/Context;

.field private m:Lcom/amap/api/a/bf;

.field private n:Z

.field private o:Z

.field private final p:[I

.field private q:Z

.field private r:I

.field private s:Lcom/amap/api/a/au;

.field private t:Landroid/location/Location;

.field private u:Lcom/amap/api/a/e;

.field private v:Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;

.field private w:Z

.field private x:Lcom/amap/api/a/bh;

.field private y:Lcom/amap/api/a/cf;

.field private z:Lcom/amap/api/a/bo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1212
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 87
    iput-boolean v2, p0, Lcom/amap/api/a/b;->n:Z

    .line 89
    iput-boolean v4, p0, Lcom/amap/api/a/b;->o:Z

    .line 91
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/amap/api/a/b;->p:[I

    .line 94
    iput-boolean v4, p0, Lcom/amap/api/a/b;->q:Z

    .line 95
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/amap/api/a/b;->b:[I

    .line 99
    iput-boolean v2, p0, Lcom/amap/api/a/b;->c:Z

    .line 100
    iput v2, p0, Lcom/amap/api/a/b;->r:I

    .line 103
    new-instance v0, Lcom/amap/api/a/ax;

    invoke-direct {v0, p0}, Lcom/amap/api/a/ax;-><init>(Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    .line 110
    iput-boolean v4, p0, Lcom/amap/api/a/b;->w:Z

    .line 119
    iput-object v3, p0, Lcom/amap/api/a/b;->C:Lcom/amap/api/a/a;

    .line 120
    iput-boolean v2, p0, Lcom/amap/api/a/b;->D:Z

    .line 121
    iput-boolean v2, p0, Lcom/amap/api/a/b;->E:Z

    .line 136
    iput-object v3, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 137
    iput-boolean v2, p0, Lcom/amap/api/a/b;->I:Z

    .line 139
    iput-boolean v2, p0, Lcom/amap/api/a/b;->K:Z

    .line 140
    iput-boolean v2, p0, Lcom/amap/api/a/b;->L:Z

    .line 146
    iput-object v3, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;

    .line 149
    iput-boolean v2, p0, Lcom/amap/api/a/b;->T:Z

    .line 153
    iput-boolean v2, p0, Lcom/amap/api/a/b;->U:Z

    .line 155
    iput-boolean v2, p0, Lcom/amap/api/a/b;->V:Z

    .line 156
    iput-object v3, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    .line 157
    iput-boolean v2, p0, Lcom/amap/api/a/b;->Z:Z

    .line 162
    iput-boolean v2, p0, Lcom/amap/api/a/b;->ae:Z

    .line 163
    new-instance v0, Lcom/amap/api/a/c;

    invoke-direct {v0, p0}, Lcom/amap/api/a/c;-><init>(Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    .line 318
    new-instance v0, Lcom/amap/api/a/z;

    invoke-direct {v0}, Lcom/amap/api/a/z;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->af:Lcom/amap/api/a/z;

    .line 1106
    iput-object v3, p0, Lcom/amap/api/a/b;->ag:Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    .line 2126
    iput-boolean v2, p0, Lcom/amap/api/a/b;->ak:Z

    .line 2128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->al:Ljava/util/ArrayList;

    .line 2129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->am:Ljava/util/ArrayList;

    .line 2132
    iput v2, p0, Lcom/amap/api/a/b;->ao:I

    .line 2133
    iput v2, p0, Lcom/amap/api/a/b;->ap:I

    .line 2135
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->k:Landroid/graphics/Matrix;

    .line 2136
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/amap/api/a/b;->aq:F

    .line 2137
    iput-boolean v2, p0, Lcom/amap/api/a/b;->ar:Z

    .line 2142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/a/b;->aw:J

    .line 2144
    iput v2, p0, Lcom/amap/api/a/b;->ax:I

    .line 2145
    iput v2, p0, Lcom/amap/api/a/b;->ay:I

    .line 2150
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/amap/api/a/b;->az:J

    .line 2633
    iput v2, p0, Lcom/amap/api/a/b;->aA:I

    .line 2635
    iput-boolean v2, p0, Lcom/amap/api/a/b;->aB:Z

    .line 2789
    iput-object v3, p0, Lcom/amap/api/a/b;->aC:Lcom/amap/api/a/b$a;

    .line 3083
    new-instance v0, Lcom/amap/api/a/d;

    invoke-direct {v0, p0}, Lcom/amap/api/a/d;-><init>(Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->aD:Ljava/lang/Thread;

    .line 1213
    invoke-direct {p0}, Lcom/amap/api/a/b;->R()V

    .line 1214
    invoke-virtual {p0, v4}, Lcom/amap/api/a/b;->setClickable(Z)V

    .line 1215
    invoke-virtual {p0, p1, v3}, Lcom/amap/api/a/b;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1218
    return-void

    .line 91
    nop

    :array_0
    .array-data 4
        0x989680
        0x4c4b40
        0x1e8480
        0xf4240
        0x7a120
        0x30d40
        0x186a0
        0xc350
        0x7530
        0x4e20
        0x2710
        0x1388
        0x7d0
        0x3e8
        0x1f4
        0xc8
        0x64
        0x32
        0x19
        0xa
        0x5
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1221
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/amap/api/a/b;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1222
    invoke-virtual {p0, p1, p2}, Lcom/amap/api/a/b;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1223
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1234
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    iput-boolean v3, p0, Lcom/amap/api/a/b;->n:Z

    .line 89
    iput-boolean v4, p0, Lcom/amap/api/a/b;->o:Z

    .line 91
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/amap/api/a/b;->p:[I

    .line 94
    iput-boolean v4, p0, Lcom/amap/api/a/b;->q:Z

    .line 95
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/amap/api/a/b;->b:[I

    .line 99
    iput-boolean v3, p0, Lcom/amap/api/a/b;->c:Z

    .line 100
    iput v3, p0, Lcom/amap/api/a/b;->r:I

    .line 103
    new-instance v0, Lcom/amap/api/a/ax;

    invoke-direct {v0, p0}, Lcom/amap/api/a/ax;-><init>(Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    .line 110
    iput-boolean v4, p0, Lcom/amap/api/a/b;->w:Z

    .line 119
    iput-object v2, p0, Lcom/amap/api/a/b;->C:Lcom/amap/api/a/a;

    .line 120
    iput-boolean v3, p0, Lcom/amap/api/a/b;->D:Z

    .line 121
    iput-boolean v3, p0, Lcom/amap/api/a/b;->E:Z

    .line 136
    iput-object v2, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 137
    iput-boolean v3, p0, Lcom/amap/api/a/b;->I:Z

    .line 139
    iput-boolean v3, p0, Lcom/amap/api/a/b;->K:Z

    .line 140
    iput-boolean v3, p0, Lcom/amap/api/a/b;->L:Z

    .line 146
    iput-object v2, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;

    .line 149
    iput-boolean v3, p0, Lcom/amap/api/a/b;->T:Z

    .line 153
    iput-boolean v3, p0, Lcom/amap/api/a/b;->U:Z

    .line 155
    iput-boolean v3, p0, Lcom/amap/api/a/b;->V:Z

    .line 156
    iput-object v2, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    .line 157
    iput-boolean v3, p0, Lcom/amap/api/a/b;->Z:Z

    .line 162
    iput-boolean v3, p0, Lcom/amap/api/a/b;->ae:Z

    .line 163
    new-instance v0, Lcom/amap/api/a/c;

    invoke-direct {v0, p0}, Lcom/amap/api/a/c;-><init>(Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    .line 318
    new-instance v0, Lcom/amap/api/a/z;

    invoke-direct {v0}, Lcom/amap/api/a/z;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->af:Lcom/amap/api/a/z;

    .line 1106
    iput-object v2, p0, Lcom/amap/api/a/b;->ag:Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    .line 2126
    iput-boolean v3, p0, Lcom/amap/api/a/b;->ak:Z

    .line 2128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->al:Ljava/util/ArrayList;

    .line 2129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->am:Ljava/util/ArrayList;

    .line 2132
    iput v3, p0, Lcom/amap/api/a/b;->ao:I

    .line 2133
    iput v3, p0, Lcom/amap/api/a/b;->ap:I

    .line 2135
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/b;->k:Landroid/graphics/Matrix;

    .line 2136
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/amap/api/a/b;->aq:F

    .line 2137
    iput-boolean v3, p0, Lcom/amap/api/a/b;->ar:Z

    .line 2142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/a/b;->aw:J

    .line 2144
    iput v3, p0, Lcom/amap/api/a/b;->ax:I

    .line 2145
    iput v3, p0, Lcom/amap/api/a/b;->ay:I

    .line 2150
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/amap/api/a/b;->az:J

    .line 2633
    iput v3, p0, Lcom/amap/api/a/b;->aA:I

    .line 2635
    iput-boolean v3, p0, Lcom/amap/api/a/b;->aB:Z

    .line 2789
    iput-object v2, p0, Lcom/amap/api/a/b;->aC:Lcom/amap/api/a/b$a;

    .line 3083
    new-instance v0, Lcom/amap/api/a/d;

    invoke-direct {v0, p0}, Lcom/amap/api/a/d;-><init>(Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->aD:Ljava/lang/Thread;

    .line 1235
    invoke-direct {p0}, Lcom/amap/api/a/b;->R()V

    .line 1236
    iput-object p1, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    .line 1238
    new-array v0, v4, [I

    const v1, 0x1010211

    aput v1, v0, v3

    .line 1239
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1240
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xf

    if-ge v1, v2, :cond_1

    .line 1241
    :cond_0
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    .line 1242
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/amap/api/a/b;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1243
    return-void

    .line 91
    :array_0
    .array-data 4
        0x989680
        0x4c4b40
        0x1e8480
        0xf4240
        0x7a120
        0x30d40
        0x186a0
        0xc350
        0x7530
        0x4e20
        0x2710
        0x1388
        0x7d0
        0x3e8
        0x1f4
        0xc8
        0x64
        0x32
        0x19
        0xa
        0x5
    .end array-data
.end method

.method private R()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1182
    const-class v2, Landroid/view/View;

    invoke-virtual {v2}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    .line 1184
    array-length v4, v3

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    .line 1185
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "setLayerType"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1191
    :goto_1
    if-eqz v0, :cond_0

    .line 1193
    :try_start_0
    const-class v1, Landroid/view/View;

    const-string v2, "LAYER_TYPE_SOFTWARE"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 1194
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const/4 v1, 0x1

    const/4 v3, 0x0

    aput-object v3, v2, v1

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1199
    :cond_0
    :goto_2
    return-void

    .line 1184
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1195
    :catch_0
    move-exception v0

    .line 1196
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method private S()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 1477
    iget-object v0, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(Landroid/content/Context;)V

    .line 1478
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1481
    iget-object v1, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1482
    return-void
.end method

.method private T()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1660
    iget-boolean v0, p0, Lcom/amap/api/a/b;->K:Z

    if-eqz v0, :cond_0

    .line 1661
    iput-boolean v2, p0, Lcom/amap/api/a/b;->K:Z

    .line 1662
    :cond_0
    iget-boolean v0, p0, Lcom/amap/api/a/b;->Z:Z

    if-eqz v0, :cond_1

    .line 1663
    iput-boolean v2, p0, Lcom/amap/api/a/b;->Z:Z

    .line 1664
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 1666
    iput-boolean v3, v0, Lcom/amap/api/a/t;->o:Z

    .line 1667
    iget-object v1, p0, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ax;->a(Lcom/amap/api/a/t;)V

    .line 1669
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/a/b;->L:Z

    if-eqz v0, :cond_2

    .line 1670
    iput-boolean v2, p0, Lcom/amap/api/a/b;->L:Z

    .line 1671
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 1673
    iput-boolean v3, v0, Lcom/amap/api/a/t;->o:Z

    .line 1674
    iget-object v1, p0, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ax;->a(Lcom/amap/api/a/t;)V

    .line 1676
    :cond_2
    iput-boolean v2, p0, Lcom/amap/api/a/b;->U:Z

    .line 1677
    iput-boolean v2, p0, Lcom/amap/api/a/b;->V:Z

    .line 1678
    iget-object v0, p0, Lcom/amap/api/a/b;->aa:Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    if-eqz v0, :cond_3

    .line 1679
    iget-object v0, p0, Lcom/amap/api/a/b;->aa:Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;->onMarkerDragEnd(Lcom/amap/api/maps2d/model/Marker;)V

    .line 1680
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    .line 1682
    :cond_3
    return-void
.end method

.method private U()V
    .locals 4

    .prologue
    .line 2269
    iget-object v0, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 2277
    :goto_0
    return-void

    .line 2272
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget v1, p0, Lcom/amap/api/a/b;->ax:I

    sub-int/2addr v0, v1

    .line 2273
    iget-object v1, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget v2, p0, Lcom/amap/api/a/b;->ay:I

    sub-int/2addr v1, v2

    .line 2274
    iget-object v2, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    iget v3, p0, Lcom/amap/api/a/b;->ax:I

    iput v3, v2, Landroid/graphics/Point;->x:I

    .line 2275
    iget-object v2, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    iget v3, p0, Lcom/amap/api/a/b;->ay:I

    iput v3, v2, Landroid/graphics/Point;->y:I

    .line 2276
    iget-object v2, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v2, v0, v1}, Lcom/amap/api/a/av;->c(II)V

    goto :goto_0
.end method

.method private V()Lcom/amap/api/maps2d/model/CameraPosition;
    .locals 6

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 2317
    invoke-virtual {p0}, Lcom/amap/api/a/b;->C()Lcom/amap/api/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->b()I

    move-result v0

    int-to-double v0, v0

    div-double/2addr v0, v4

    .line 2318
    invoke-virtual {p0}, Lcom/amap/api/a/b;->C()Lcom/amap/api/a/ac;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/a/ac;->a()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v2, v4

    .line 2319
    new-instance v4, Lcom/amap/api/maps2d/model/LatLng;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 2320
    invoke-virtual {p0}, Lcom/amap/api/a/b;->f()F

    move-result v0

    .line 2321
    invoke-static {v4, v0}, Lcom/amap/api/maps2d/model/CameraPosition;->fromLatLngZoom(Lcom/amap/api/maps2d/model/LatLng;F)Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    .line 2322
    return-object v0
.end method

.method private W()Lcom/amap/api/maps2d/model/LatLng;
    .locals 5

    .prologue
    .line 3068
    invoke-virtual {p0}, Lcom/amap/api/a/b;->C()Lcom/amap/api/a/ac;

    move-result-object v0

    .line 3069
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->b()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v1

    .line 3070
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->a()I

    move-result v0

    int-to-long v3, v0

    invoke-static {v3, v4}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v3

    .line 3071
    new-instance v0, Lcom/amap/api/maps2d/model/LatLng;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 3072
    return-object v0
.end method

.method private X()Lcom/autonavi/amap/mapcore2d/IPoint;
    .locals 4

    .prologue
    .line 3076
    invoke-virtual {p0}, Lcom/amap/api/a/b;->C()Lcom/amap/api/a/ac;

    move-result-object v0

    .line 3077
    new-instance v1, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 3078
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v2

    double-to-int v2, v2

    iput v2, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 3079
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v2

    double-to-int v0, v2

    iput v0, v1, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 3080
    return-object v1
.end method

.method static synthetic a(Lcom/amap/api/a/b;Lcom/amap/api/maps2d/AMap$CancelableCallback;)Lcom/amap/api/maps2d/AMap$CancelableCallback;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    return-object p1
.end method

.method static synthetic a(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->ac:Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;

    return-object v0
.end method

.method static synthetic a(Lcom/amap/api/a/b;Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;)Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/amap/api/a/b;->ag:Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    return-object p1
.end method

.method private a(FLandroid/graphics/PointF;FF)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2683
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    invoke-interface {v0}, Lcom/amap/api/a/aq;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_1

    .line 2729
    :cond_0
    :goto_0
    return-void

    .line 2686
    :catch_0
    move-exception v0

    .line 2687
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2690
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/amap/api/a/b;->aA:I

    .line 2691
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->c()I

    move-result v0

    div-int/lit8 v4, v0, 0x2

    .line 2692
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->d()I

    move-result v0

    div-int/lit8 v5, v0, 0x2

    .line 2695
    cmpl-float v0, p1, v1

    if-lez v0, :cond_2

    .line 2697
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cg;->a(F)V

    .line 2698
    invoke-virtual {p0}, Lcom/amap/api/a/b;->K()V

    .line 2699
    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    move v1, v2

    .line 2712
    :goto_1
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    add-int/2addr v0, v1

    .line 2716
    :goto_2
    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(I)I

    move-result v0

    .line 2717
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2718
    iget-object v1, p0, Lcom/amap/api/a/b;->b:[I

    iget-object v6, p0, Lcom/amap/api/a/b;->b:[I

    aget v6, v6, v2

    aput v6, v1, v3

    .line 2719
    iget-object v1, p0, Lcom/amap/api/a/b;->b:[I

    aput v0, v1, v2

    .line 2720
    iget-object v1, p0, Lcom/amap/api/a/b;->b:[I

    aget v1, v1, v3

    iget-object v3, p0, Lcom/amap/api/a/b;->b:[I

    aget v2, v3, v2

    if-eq v1, v2, :cond_0

    .line 2724
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    invoke-virtual {v1, v4, v5}, Lcom/amap/api/a/bf$e;->a(II)Lcom/amap/api/a/ac;

    move-result-object v1

    .line 2725
    iget-object v2, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v2, v0}, Lcom/amap/api/a/bf$d;->a(I)V

    .line 2726
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/ac;)V

    goto :goto_0

    .line 2700
    :cond_2
    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    .line 2702
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cg;->a(F)V

    .line 2703
    invoke-virtual {p0}, Lcom/amap/api/a/b;->K()V

    .line 2704
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    move v1, v3

    goto :goto_1

    .line 2712
    :cond_3
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    sub-int v0, v1, v0

    goto :goto_2
.end method

.method private a(II)V
    .locals 1

    .prologue
    .line 2280
    iget-object v0, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    if-nez v0, :cond_0

    .line 2286
    :goto_0
    return-void

    .line 2283
    :cond_0
    iput p1, p0, Lcom/amap/api/a/b;->ax:I

    .line 2284
    iput p2, p0, Lcom/amap/api/a/b;->ay:I

    .line 2285
    invoke-direct {p0}, Lcom/amap/api/a/b;->U()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/amap/api/a/b;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/amap/api/a/b;->D:Z

    return p1
.end method

.method static synthetic b(Lcom/amap/api/a/b;)Lcom/amap/api/a/cf;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    return-object v0
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 1685
    iget-boolean v0, p0, Lcom/amap/api/a/b;->V:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    if-eqz v0, :cond_0

    .line 1686
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 1687
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    const/high16 v2, 0x42700000    # 60.0f

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 1688
    new-instance v2, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 1689
    invoke-virtual {p0, v0, v1, v2}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 1690
    new-instance v0, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v2, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v1, v2, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 1691
    iget-object v1, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    invoke-virtual {v1, v0}, Lcom/amap/api/maps2d/model/Marker;->setPosition(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 1692
    iget-object v0, p0, Lcom/amap/api/a/b;->aa:Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;->onMarkerDrag(Lcom/amap/api/maps2d/model/Marker;)V

    .line 1694
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/amap/api/a/b;Z)Z
    .locals 0

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/amap/api/a/b;->E:Z

    return p1
.end method

.method static synthetic c(Lcom/amap/api/a/b;)Landroid/view/View;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/model/Marker;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->P:Lcom/amap/api/maps2d/model/Marker;

    return-object v0
.end method

.method static synthetic e(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->ag:Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    return-object v0
.end method

.method static synthetic f(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$CancelableCallback;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    return-object v0
.end method

.method static synthetic g(Lcom/amap/api/a/b;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/amap/api/a/b;->I:Z

    return v0
.end method

.method static synthetic h(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    return-object v0
.end method

.method static synthetic i(Lcom/amap/api/a/b;)Lcom/amap/api/maps2d/model/CameraPosition;
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/amap/api/a/b;->V()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/amap/api/a/b;)Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/amap/api/a/b;->E:Z

    return v0
.end method

.method static synthetic k(Lcom/amap/api/a/b;)Lcom/amap/api/a/r;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    return-object v0
.end method

.method static synthetic l(Lcom/amap/api/a/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method A()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    invoke-virtual {v0}, Lcom/amap/api/a/cf;->c()Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public B()Z
    .locals 1

    .prologue
    .line 1400
    iget-boolean v0, p0, Lcom/amap/api/a/b;->o:Z

    return v0
.end method

.method public C()Lcom/amap/api/a/ac;
    .locals 1

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->f()Lcom/amap/api/a/ac;

    move-result-object v0

    return-object v0
.end method

.method public D()Lcom/amap/api/a/av;
    .locals 1

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    return-object v0
.end method

.method public E()Z
    .locals 2

    .prologue
    .line 1549
    .line 1550
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;)Lcom/amap/api/a/as;

    move-result-object v0

    .line 1552
    if-eqz v0, :cond_0

    .line 1553
    iget-boolean v0, v0, Lcom/amap/api/a/as;->f:Z

    .line 1555
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1627
    invoke-virtual {p0}, Lcom/amap/api/a/b;->E()Z

    move-result v1

    .line 1628
    const-string v2, ""

    .line 1629
    if-nez v1, :cond_0

    .line 1630
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->f:Ljava/lang/String;

    .line 1635
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v2, v1}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;)Lcom/amap/api/a/as;

    move-result-object v1

    .line 1636
    if-eqz v1, :cond_0

    .line 1637
    iget-boolean v0, v1, Lcom/amap/api/a/as;->f:Z

    .line 1639
    :cond_0
    return v0
.end method

.method public G()Lcom/amap/api/a/bg;
    .locals 1

    .prologue
    .line 2171
    iget-object v0, p0, Lcom/amap/api/a/b;->aj:Lcom/amap/api/a/bg;

    return-object v0
.end method

.method public H()F
    .locals 1

    .prologue
    .line 2236
    iget v0, p0, Lcom/amap/api/a/b;->aq:F

    return v0
.end method

.method public I()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2240
    iput v0, p0, Lcom/amap/api/a/b;->as:F

    .line 2241
    iput v0, p0, Lcom/amap/api/a/b;->at:F

    .line 2242
    return-void
.end method

.method public J()I
    .locals 1

    .prologue
    .line 2892
    const/4 v0, 0x0

    return v0
.end method

.method K()V
    .locals 2

    .prologue
    .line 2941
    iget-object v0, p0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2942
    return-void
.end method

.method L()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2949
    new-instance v0, Lcom/amap/api/maps2d/model/CameraPosition;

    invoke-direct {p0}, Lcom/amap/api/a/b;->W()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amap/api/a/b;->f()F

    move-result v2

    invoke-direct {v0, v1, v2, v4, v4}, Lcom/amap/api/maps2d/model/CameraPosition;-><init>(Lcom/amap/api/maps2d/model/LatLng;FFF)V

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/maps2d/model/CameraPosition;)V

    .line 2951
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    if-nez v0, :cond_0

    .line 2968
    :goto_0
    return-void

    .line 2954
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v0

    .line 2955
    new-instance v1, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 2956
    new-instance v2, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 2957
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 2958
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 2959
    new-instance v3, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/a/a/p;->a(Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;)D

    move-result-wide v1

    .line 2962
    iget-object v3, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v3}, Lcom/amap/api/a/bf$d;->e()I

    move-result v3

    .line 2963
    iget-object v4, p0, Lcom/amap/api/a/b;->p:[I

    aget v4, v4, v3

    mul-int/2addr v0, v4

    int-to-double v4, v0

    div-double v0, v4, v1

    double-to-int v0, v0

    .line 2964
    iget-object v1, p0, Lcom/amap/api/a/b;->p:[I

    aget v1, v1, v3

    invoke-static {v1}, Lcom/amap/api/a/a/p;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 2965
    iget-object v2, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v2, v0}, Lcom/amap/api/a/bo;->a(I)V

    .line 2966
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bo;->a(Ljava/lang/String;)V

    .line 2967
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v0}, Lcom/amap/api/a/bo;->invalidate()V

    goto :goto_0
.end method

.method public M()V
    .locals 1

    .prologue
    .line 3115
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    if-eqz v0, :cond_0

    .line 3116
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps2d/AMap$CancelableCallback;->onCancel()V

    .line 3118
    :cond_0
    return-void
.end method

.method public N()V
    .locals 1

    .prologue
    .line 3122
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    if-eqz v0, :cond_0

    .line 3123
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps2d/AMap$CancelableCallback;->onFinish()V

    .line 3125
    :cond_0
    return-void
.end method

.method public O()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3129
    new-instance v0, Lcom/amap/api/maps2d/model/CameraPosition;

    invoke-direct {p0}, Lcom/amap/api/a/b;->W()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amap/api/a/b;->f()F

    move-result v2

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/amap/api/maps2d/model/CameraPosition;-><init>(Lcom/amap/api/maps2d/model/LatLng;FFF)V

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/maps2d/model/CameraPosition;)V

    .line 3131
    iget-object v0, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    if-eqz v0, :cond_0

    .line 3132
    invoke-direct {p0}, Lcom/amap/api/a/b;->V()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    .line 3133
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/amap/api/a/b;->a(ZLcom/amap/api/maps2d/model/CameraPosition;)V

    .line 3135
    :cond_0
    return-void
.end method

.method public P()V
    .locals 1

    .prologue
    .line 3142
    invoke-virtual {p0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 3143
    iget-object v0, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    invoke-virtual {v0}, Lcom/amap/api/a/az;->postInvalidate()V

    .line 3144
    return-void
.end method

.method public Q()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3148
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/a/b;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string/jumbo v1, "\u5730\u56fe\u672a\u521d\u59cb\u5316\u5b8c\u6210\uff01"

    invoke-static {v0, v1}, Lcom/amap/api/a/a/a;->a(ZLjava/lang/Object;)V

    .line 3150
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->f()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 3148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)I
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->b()I

    move-result p1

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->a()I

    move-result v0

    if-le p1, v0, :cond_1

    .line 447
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->a()I

    move-result p1

    .line 449
    :cond_1
    return p1
.end method

.method public a(Lcom/amap/api/maps2d/model/CircleOptions;)Lcom/amap/api/a/ag;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 663
    new-instance v0, Lcom/amap/api/a/v;

    invoke-direct {v0, p0}, Lcom/amap/api/a/v;-><init>(Lcom/amap/api/a/b;)V

    .line 664
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/CircleOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/v;->b(I)V

    .line 665
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/CircleOptions;->getCenter()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/v;->a(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 666
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/CircleOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/v;->a(Z)V

    .line 667
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/CircleOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/v;->b(F)V

    .line 668
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/CircleOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/v;->a(F)V

    .line 669
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/CircleOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/v;->a(I)V

    .line 670
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/CircleOptions;->getRadius()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/a/v;->a(D)V

    .line 671
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ab;->a(Lcom/amap/api/a/ak;)V

    .line 672
    return-object v0
.end method

.method public a(Lcom/amap/api/maps2d/model/GroundOverlayOptions;)Lcom/amap/api/a/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 678
    new-instance v0, Lcom/amap/api/a/ae;

    invoke-direct {v0, p0}, Lcom/amap/api/a/ae;-><init>(Lcom/amap/api/a/b;)V

    .line 680
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getAnchorU()F

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getAnchorV()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/a/ae;->b(FF)V

    .line 682
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getBearing()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ae;->c(F)V

    .line 683
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getWidth()F

    move-result v1

    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getHeight()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/a/ae;->a(FF)V

    .line 685
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getImage()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ae;->a(Lcom/amap/api/maps2d/model/BitmapDescriptor;)V

    .line 686
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getLocation()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ae;->a(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 687
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getBounds()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ae;->a(Lcom/amap/api/maps2d/model/LatLngBounds;)V

    .line 688
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getTransparency()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ae;->d(F)V

    .line 689
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ae;->a(Z)V

    .line 690
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/GroundOverlayOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ae;->a(F)V

    .line 691
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ab;->a(Lcom/amap/api/a/ak;)V

    .line 692
    return-object v0
.end method

.method public a(Lcom/amap/api/maps2d/model/PolygonOptions;)Lcom/amap/api/a/al;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 698
    new-instance v0, Lcom/amap/api/a/bj;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bj;-><init>(Lcom/amap/api/a/b;)V

    .line 699
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolygonOptions;->getFillColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bj;->a(I)V

    .line 700
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolygonOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bj;->a(Ljava/util/List;)V

    .line 701
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolygonOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bj;->a(Z)V

    .line 702
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolygonOptions;->getStrokeWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bj;->b(F)V

    .line 703
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolygonOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bj;->a(F)V

    .line 704
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolygonOptions;->getStrokeColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bj;->b(I)V

    .line 705
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ab;->a(Lcom/amap/api/a/ak;)V

    .line 706
    return-object v0
.end method

.method public a(Lcom/amap/api/maps2d/model/PolylineOptions;)Lcom/amap/api/a/am;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 648
    new-instance v0, Lcom/amap/api/a/bk;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bk;-><init>(Lcom/amap/api/a/b;)V

    .line 649
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolylineOptions;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bk;->a(I)V

    .line 650
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolylineOptions;->isDottedLine()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bk;->b(Z)V

    .line 651
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolylineOptions;->isGeodesic()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bk;->c(Z)V

    .line 652
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolylineOptions;->getPoints()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bk;->a(Ljava/util/List;)V

    .line 653
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolylineOptions;->isVisible()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bk;->a(Z)V

    .line 654
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolylineOptions;->getWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bk;->b(F)V

    .line 655
    invoke-virtual {p1}, Lcom/amap/api/maps2d/model/PolylineOptions;->getZIndex()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bk;->a(F)V

    .line 656
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ab;->a(Lcom/amap/api/a/ak;)V

    .line 657
    return-object v0
.end method

.method public a()Lcom/amap/api/a/bf;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    return-object v0
.end method

.method public a(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/maps2d/model/Marker;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 711
    new-instance v0, Lcom/amap/api/a/be;

    iget-object v1, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-direct {v0, p1, v1}, Lcom/amap/api/a/be;-><init>(Lcom/amap/api/maps2d/model/MarkerOptions;Lcom/amap/api/a/ay;)V

    .line 713
    iget-object v1, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ay;->a(Lcom/amap/api/a/aj;)V

    .line 714
    new-instance v1, Lcom/amap/api/maps2d/model/Marker;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/Marker;-><init>(Lcom/amap/api/a/aj;)V

    return-object v1
.end method

.method public a(Lcom/amap/api/maps2d/model/TileOverlayOptions;)Lcom/amap/api/maps2d/model/TileOverlay;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 729
    new-instance v0, Lcom/amap/api/a/bu;

    iget-object v1, p0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    invoke-direct {v0, p1, v1}, Lcom/amap/api/a/bu;-><init>(Lcom/amap/api/maps2d/model/TileOverlayOptions;Lcom/amap/api/a/bv;)V

    .line 731
    iget-object v1, p0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/bv;->a(Lcom/amap/api/a/ap;)V

    .line 732
    new-instance v1, Lcom/amap/api/maps2d/model/TileOverlay;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/TileOverlay;-><init>(Lcom/amap/api/a/ap;)V

    return-object v1
.end method

.method public a(DDLcom/autonavi/amap/mapcore2d/FPoint;)V
    .locals 6

    .prologue
    .line 3005
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v4

    .line 3006
    invoke-static {p1, p2}, Lcom/amap/api/a/y;->a(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 3007
    invoke-static {p3, p4}, Lcom/amap/api/a/y;->a(D)J

    move-result-wide v1

    long-to-int v2, v1

    .line 3008
    new-instance v1, Lcom/amap/api/a/ac;

    invoke-direct {v1, v0, v2}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 3009
    iget-object v0, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v5, v5, Lcom/amap/api/a/ba;->h:[D

    aget-wide v4, v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;

    move-result-object v0

    .line 3011
    if-eqz p5, :cond_0

    .line 3012
    iget v1, v0, Landroid/graphics/PointF;->x:F

    iput v1, p5, Lcom/autonavi/amap/mapcore2d/FPoint;->x:F

    .line 3013
    iget v0, v0, Landroid/graphics/PointF;->y:F

    iput v0, p5, Lcom/autonavi/amap/mapcore2d/FPoint;->y:F

    .line 3015
    :cond_0
    return-void
.end method

.method public a(DDLcom/autonavi/amap/mapcore2d/IPoint;)V
    .locals 6

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 3019
    iget-object v0, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    new-instance v1, Lcom/amap/api/a/ac;

    mul-double v2, p1, v4

    double-to-int v2, v2

    mul-double v3, p3, v4

    double-to-int v3, v3

    invoke-direct {v1, v2, v3}, Lcom/amap/api/a/ac;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 3021
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->a()I

    move-result v1

    iput v1, p5, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 3022
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->b()I

    move-result v0

    iput v0, p5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 3024
    return-void
.end method

.method public a(F)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 829
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    if-eqz v0, :cond_0

    .line 830
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bh;->a(F)V

    .line 832
    :cond_0
    return-void
.end method

.method public a(FLandroid/graphics/Point;Z)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 598
    .line 599
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v0

    int-to-float v0, v0

    .line 600
    add-float v1, v0, p1

    invoke-static {v1}, Lcom/amap/api/a/a/p;->b(F)F

    move-result v1

    sub-float v0, v1, v0

    .line 601
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 605
    :cond_1
    new-instance v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 606
    invoke-direct {p0}, Lcom/amap/api/a/b;->X()Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v3

    .line 609
    if-eqz p2, :cond_0

    .line 610
    new-instance v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 611
    iget v1, p2, Landroid/graphics/Point;->x:I

    iget v2, p2, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v1, v2, v0}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 612
    iget v1, v3, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v2, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    sub-int/2addr v1, v2

    .line 613
    iget v2, v3, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    iget v4, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    sub-int/2addr v2, v4

    .line 614
    int-to-double v4, v1

    float-to-double v6, p1

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    int-to-double v6, v1

    sub-double/2addr v4, v6

    double-to-int v1, v4

    .line 615
    int-to-double v4, v2

    float-to-double v6, p1

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    div-double/2addr v4, v6

    int-to-double v6, v2

    sub-double/2addr v4, v6

    double-to-int v2, v4

    .line 616
    iget v4, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    add-int/2addr v1, v4

    iput v1, v3, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 617
    iget v0, v0, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    add-int/2addr v0, v2

    iput v0, v3, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 618
    new-instance v0, Lcom/amap/api/a/ac;

    iget v1, v3, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    int-to-double v1, v1

    iget v3, v3, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    int-to-double v3, v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    .line 619
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 621
    if-eqz p3, :cond_2

    .line 622
    invoke-virtual {p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;I)V

    goto :goto_0

    .line 624
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;)V

    goto :goto_0
.end method

.method public a(IILcom/autonavi/amap/mapcore2d/DPoint;)V
    .locals 7

    .prologue
    .line 2978
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v4

    .line 2979
    new-instance v1, Landroid/graphics/PointF;

    int-to-float v0, p1

    int-to-float v2, p2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2980
    iget-object v0, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v5, v5, Lcom/amap/api/a/ba;->h:[D

    aget-wide v4, v5, v4

    iget-object v6, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v6, v6, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    invoke-virtual/range {v0 .. v6}, Lcom/amap/api/a/ba;->a(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 2983
    if-eqz p3, :cond_0

    .line 2984
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->b()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v1

    .line 2985
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->a()I

    move-result v0

    int-to-long v3, v0

    invoke-static {v3, v4}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v3

    .line 2986
    iput-wide v1, p3, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    .line 2987
    iput-wide v3, p3, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    .line 2989
    :cond_0
    return-void
.end method

.method public a(IILcom/autonavi/amap/mapcore2d/IPoint;)V
    .locals 7

    .prologue
    .line 2992
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v4

    .line 2993
    new-instance v1, Landroid/graphics/PointF;

    int-to-float v0, p1

    int-to-float v2, p2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2994
    iget-object v0, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v5, v5, Lcom/amap/api/a/ba;->h:[D

    aget-wide v4, v5, v4

    iget-object v6, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v6, v6, Lcom/amap/api/a/ba;->m:Lcom/amap/api/a/ba$a;

    invoke-virtual/range {v0 .. v6}, Lcom/amap/api/a/ba;->a(Landroid/graphics/PointF;Lcom/amap/api/a/ac;Landroid/graphics/Point;DLcom/amap/api/a/ba$a;)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 2997
    if-eqz p3, :cond_0

    .line 2998
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, p3, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 2999
    invoke-virtual {v0}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p3, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 3001
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    .line 2155
    new-instance v0, Landroid/view/GestureDetector;

    invoke-direct {v0, p0}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->ai:Landroid/view/GestureDetector;

    .line 2156
    invoke-static {p1, p0, p0}, Lcom/amap/api/a/bg;->a(Landroid/content/Context;Lcom/amap/api/a/bg$b;Lcom/amap/api/a/b;)Lcom/amap/api/a/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/b;->aj:Lcom/amap/api/a/bg;

    .line 2158
    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    .line 2159
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2160
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 2161
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/amap/api/a/b;->au:I

    .line 2162
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/amap/api/a/b;->av:I

    .line 2163
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/amap/api/a/b;->ao:I

    .line 2165
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/amap/api/a/b;->ap:I

    .line 2168
    return-void
.end method

.method a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v1, -0x2

    const-wide/16 v7, 0x0

    .line 1249
    iput-object p1, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    .line 1250
    new-instance v0, Lcom/amap/api/a/bm;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bm;-><init>(Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->S:Lcom/amap/api/a/an;

    .line 1251
    const/16 v0, 0xde

    const/16 v2, 0xd7

    const/16 v3, 0xd6

    invoke-static {v0, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->setBackgroundColor(I)V

    .line 1252
    invoke-static {}, Lcom/amap/api/a/u;->a()Lcom/amap/api/a/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/a/u;->a(Lcom/amap/api/a/u$a;)V

    .line 1253
    invoke-static {}, Lcom/amap/api/a/bp;->a()Lcom/amap/api/a/bp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/a/bp;->a(Lcom/amap/api/a/bp$a;)V

    .line 1254
    invoke-static {}, Lcom/amap/api/a/s;->a()Lcom/amap/api/a/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/a/s;->a(Lcom/amap/api/a/s$a;)V

    .line 1255
    new-instance v0, Lcom/amap/api/a/a;

    invoke-direct {v0, p0}, Lcom/amap/api/a/a;-><init>(Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->C:Lcom/amap/api/a/a;

    .line 1256
    new-instance v0, Lcom/amap/api/a/e;

    invoke-direct {v0, p0}, Lcom/amap/api/a/e;-><init>(Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->u:Lcom/amap/api/a/e;

    .line 1257
    new-instance v0, Lcom/amap/api/a/r;

    invoke-direct {v0, p1}, Lcom/amap/api/a/r;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    .line 1258
    new-instance v0, Lcom/amap/api/a/bv;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/amap/api/a/bv;-><init>(Landroid/content/Context;Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    .line 1259
    new-instance v0, Lcom/amap/api/a/bf;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/amap/api/a/bf;-><init>(Landroid/content/Context;Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    .line 1260
    iget-object v0, p0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/amap/api/a/bv;->a(Z)V

    .line 1262
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iput-object v0, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    .line 1263
    new-instance v0, Lcom/amap/api/a/av;

    iget-object v2, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    invoke-direct {v0, v2}, Lcom/amap/api/a/av;-><init>(Lcom/amap/api/a/bf;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    .line 1264
    new-instance v0, Lcom/amap/api/a/cb;

    invoke-direct {v0, p0}, Lcom/amap/api/a/cb;-><init>(Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    .line 1266
    new-instance v0, Lcom/amap/api/a/ay;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v2, p2, p0}, Lcom/amap/api/a/ay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    .line 1267
    new-instance v0, Lcom/amap/api/a/cg;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    iget-object v3, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-direct {v0, v2, v3, p0}, Lcom/amap/api/a/cg;-><init>(Landroid/content/Context;Lcom/amap/api/a/av;Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    .line 1268
    new-instance v0, Lcom/amap/api/a/az;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/amap/api/a/az;-><init>(Landroid/content/Context;Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    .line 1269
    new-instance v0, Lcom/amap/api/a/au;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    iget-object v3, p0, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    invoke-direct {v0, v2, v3, p0}, Lcom/amap/api/a/au;-><init>(Landroid/content/Context;Lcom/amap/api/a/ax;Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    .line 1270
    new-instance v0, Lcom/amap/api/a/cf;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/amap/api/a/cf;-><init>(Landroid/content/Context;Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    .line 1271
    new-instance v0, Lcom/amap/api/a/bo;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v2, p0}, Lcom/amap/api/a/bo;-><init>(Landroid/content/Context;Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    .line 1272
    new-instance v0, Lcom/amap/api/a/w;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    iget-object v3, p0, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    invoke-direct {v0, v2, v3, p0}, Lcom/amap/api/a/w;-><init>(Landroid/content/Context;Lcom/amap/api/a/ax;Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->B:Lcom/amap/api/a/w;

    .line 1273
    new-instance v0, Lcom/amap/api/a/ay;

    iget-object v2, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v2, p2, p0}, Lcom/amap/api/a/ay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    .line 1274
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1277
    invoke-direct {p0}, Lcom/amap/api/a/b;->S()V

    .line 1278
    iget-object v2, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v3, p0, Lcom/amap/api/a/b;->h:Lcom/amap/api/a/bv;

    invoke-virtual {v2, v3, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1279
    iget-object v2, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v3, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    invoke-virtual {v2, v3, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1280
    iget-object v2, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v3, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v2, v3, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1281
    new-instance v2, Lcom/amap/api/a/az$a;

    invoke-direct {v2, v0}, Lcom/amap/api/a/az$a;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1283
    iget-object v0, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v3, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, v3, v2}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1284
    new-instance v0, Lcom/amap/api/a/az$a;

    new-instance v3, Lcom/amap/api/maps2d/model/LatLng;

    invoke-direct {v3, v7, v8, v7, v8}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    const/16 v6, 0x53

    move v2, v1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az$a;-><init>(IILcom/amap/api/maps2d/model/LatLng;III)V

    .line 1288
    iget-object v2, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v3, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    invoke-virtual {v2, v3, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1289
    new-instance v0, Lcom/amap/api/a/az$a;

    new-instance v3, Lcom/amap/api/maps2d/model/LatLng;

    invoke-direct {v3, v7, v8, v7, v8}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    const/16 v6, 0x53

    move v2, v1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az$a;-><init>(IILcom/amap/api/maps2d/model/LatLng;III)V

    .line 1294
    iget-object v2, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v3, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    invoke-virtual {v2, v3, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1296
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->q()Lcom/amap/api/a/aq;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/aq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1297
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/amap/api/a/au;->setVisibility(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1303
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/b;->B:Lcom/amap/api/a/w;

    invoke-virtual {v0, v9}, Lcom/amap/api/a/w;->setVisibility(I)V

    .line 1305
    new-instance v0, Lcom/amap/api/a/az$a;

    new-instance v3, Lcom/amap/api/maps2d/model/LatLng;

    invoke-direct {v3, v7, v8, v7, v8}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    const/16 v6, 0x33

    move v2, v1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az$a;-><init>(IILcom/amap/api/maps2d/model/LatLng;III)V

    .line 1310
    iget-object v1, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v2, p0, Lcom/amap/api/a/b;->B:Lcom/amap/api/a/w;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1312
    new-instance v0, Lcom/amap/api/a/bh;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bh;-><init>(Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    .line 1316
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    sget v1, Lcom/amap/api/a/l;->a:I

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cg;->setId(I)V

    .line 1318
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/a/b;->aD:Ljava/lang/Thread;

    const-string v1, "AuthThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 1319
    iget-object v0, p0, Lcom/amap/api/a/b;->aD:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1324
    :goto_1
    return-void

    .line 1299
    :catch_0
    move-exception v0

    .line 1300
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1320
    :catch_1
    move-exception v0

    .line 1321
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public a(Landroid/location/Location;)V
    .locals 6

    .prologue
    .line 383
    if-nez p1, :cond_0

    .line 420
    :goto_0
    return-void

    .line 387
    :cond_0
    new-instance v1, Lcom/amap/api/maps2d/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 390
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/b;->A:Lcom/amap/api/maps2d/LocationSource;

    if-nez v0, :cond_5

    .line 392
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    invoke-virtual {v0}, Lcom/amap/api/a/bh;->a()V

    .line 393
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 405
    :catch_0
    move-exception v0

    .line 406
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 408
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/amap/api/a/bh;->a(Lcom/amap/api/maps2d/model/LatLng;D)V

    .line 410
    iget-object v0, p0, Lcom/amap/api/a/b;->v:Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;

    if-eqz v0, :cond_4

    .line 411
    iget-object v0, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_4

    .line 416
    :cond_3
    iget-object v0, p0, Lcom/amap/api/a/b;->v:Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;

    invoke-interface {v0, p1}, Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;->onMyLocationChange(Landroid/location/Location;)V

    .line 419
    :cond_4
    new-instance v0, Landroid/location/Location;

    invoke-direct {v0, p1}, Landroid/location/Location;-><init>(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    goto :goto_0

    .line 396
    :cond_5
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    if-nez v0, :cond_2

    .line 397
    :cond_6
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    if-nez v0, :cond_7

    .line 398
    new-instance v0, Lcom/amap/api/a/bh;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bh;-><init>(Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    .line 400
    :cond_7
    if-eqz v1, :cond_2

    .line 401
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v1, v0}, Lcom/amap/api/a/t;->a(Lcom/amap/api/maps2d/model/LatLng;F)Lcom/amap/api/a/t;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/t;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public a(Lcom/amap/api/a/aa;)V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/amap/api/a/b;->af:Lcom/amap/api/a/z;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/z;->a(Lcom/amap/api/a/aa;)Z

    .line 335
    return-void
.end method

.method public a(Lcom/amap/api/a/aj;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v2, -0x2

    const/high16 v5, -0x1000000

    .line 929
    .line 930
    if-nez p1, :cond_1

    .line 994
    :cond_0
    :goto_0
    return-void

    .line 933
    :cond_1
    invoke-interface {p1}, Lcom/amap/api/a/aj;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Lcom/amap/api/a/aj;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 937
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/b;->O:Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;

    if-eqz v0, :cond_0

    .line 938
    invoke-virtual {p0}, Lcom/amap/api/a/b;->t()V

    .line 939
    new-instance v7, Lcom/amap/api/maps2d/model/Marker;

    invoke-direct {v7, p1}, Lcom/amap/api/maps2d/model/Marker;-><init>(Lcom/amap/api/a/aj;)V

    .line 940
    iget-object v0, p0, Lcom/amap/api/a/b;->O:Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;

    invoke-interface {v0, v7}, Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;->getInfoWindow(Lcom/amap/api/maps2d/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    .line 943
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_3

    .line 944
    iget-object v0, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    const-string v1, "infowindow_bg.9.png"

    invoke-static {v0, v1}, Lcom/amap/api/a/bi;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 952
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    if-nez v0, :cond_4

    .line 953
    iget-object v0, p0, Lcom/amap/api/a/b;->O:Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;

    invoke-interface {v0, v7}, Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;->getInfoContents(Lcom/amap/api/maps2d/model/Marker;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    .line 955
    :cond_4
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    if-eqz v0, :cond_6

    .line 956
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_5

    .line 957
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    iget-object v1, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 976
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 977
    iget-object v1, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 978
    iget-object v1, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/view/View;->setDrawingCacheQuality(I)V

    .line 979
    invoke-interface {p1}, Lcom/amap/api/a/aj;->f()Lcom/autonavi/amap/mapcore2d/FPoint;

    move-result-object v5

    .line 982
    if-eqz v0, :cond_7

    .line 983
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 984
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 986
    :goto_3
    new-instance v0, Lcom/amap/api/a/az$a;

    invoke-interface {p1}, Lcom/amap/api/a/aj;->c()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v3

    iget v4, v5, Lcom/autonavi/amap/mapcore2d/FPoint;->x:F

    float-to-int v4, v4

    neg-int v4, v4

    invoke-interface {p1}, Lcom/amap/api/a/aj;->q()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    iget v5, v5, Lcom/autonavi/amap/mapcore2d/FPoint;->y:F

    float-to-int v5, v5

    neg-int v5, v5

    add-int/lit8 v5, v5, 0x2

    const/16 v6, 0x51

    invoke-direct/range {v0 .. v6}, Lcom/amap/api/a/az$a;-><init>(IILcom/amap/api/maps2d/model/LatLng;III)V

    .line 991
    iput-object v7, p0, Lcom/amap/api/a/b;->P:Lcom/amap/api/maps2d/model/Marker;

    .line 992
    iget-object v1, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v2, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/amap/api/a/az;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 948
    :catch_0
    move-exception v0

    .line 949
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 961
    :cond_6
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 963
    iget-object v1, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 964
    new-instance v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 965
    invoke-interface {p1}, Lcom/amap/api/a/aj;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 966
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 967
    new-instance v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/amap/api/a/b;->l:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 968
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 969
    invoke-interface {p1}, Lcom/amap/api/a/aj;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 970
    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 971
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 972
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 974
    iput-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    goto :goto_2

    :cond_7
    move v1, v2

    goto :goto_3
.end method

.method public a(Lcom/amap/api/a/t;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 478
    iget-object v0, p0, Lcom/amap/api/a/b;->C:Lcom/amap/api/a/a;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/a;->a(Lcom/amap/api/a/t;)V

    .line 479
    return-void
.end method

.method public a(Lcom/amap/api/a/t;JLcom/amap/api/maps2d/AMap$CancelableCallback;)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 501
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->j:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_0

    .line 502
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v2

    if-lez v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->getHeight()I

    move-result v2

    if-lez v2, :cond_4

    const/4 v2, 0x1

    :goto_0
    const-string/jumbo v3, "the map must have a size"

    invoke-static {v2, v3}, Lcom/amap/api/a/a/a;->a(ZLjava/lang/Object;)V

    .line 505
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    invoke-virtual {v2}, Lcom/amap/api/a/r;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/amap/api/a/r;->a(Z)V

    .line 507
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    if-eqz v2, :cond_1

    .line 508
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    invoke-interface {v2}, Lcom/amap/api/maps2d/AMap$CancelableCallback;->onCancel()V

    .line 510
    :cond_1
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 511
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/amap/api/a/b;->D:Z

    if-eqz v2, :cond_2

    .line 512
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/a/b;->E:Z

    .line 514
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/a/b;->I:Z

    .line 515
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->h:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_6

    .line 516
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-nez v2, :cond_5

    .line 595
    :cond_3
    :goto_1
    return-void

    .line 502
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 519
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/amap/api/a/b;->n:Z

    if-eqz v2, :cond_3

    .line 522
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    move-object/from16 v0, p1

    iget v3, v0, Lcom/amap/api/a/t;->b:F

    float-to-int v3, v3

    move-object/from16 v0, p1

    iget v4, v0, Lcom/amap/api/a/t;->c:F

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/amap/api/a/av;->c(II)V

    .line 524
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->postInvalidate()V

    goto :goto_1

    .line 525
    :cond_6
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->b:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_7

    .line 526
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/a/av;->c()Z

    goto :goto_1

    .line 527
    :cond_7
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->e:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_8

    .line 528
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v2

    invoke-virtual {v2}, Lcom/amap/api/a/av;->d()Z

    goto :goto_1

    .line 529
    :cond_8
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->f:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_9

    .line 530
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/a/t;->d:F

    float-to-int v2, v2

    .line 531
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/amap/api/a/av;->c(I)I

    goto :goto_1

    .line 532
    :cond_9
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->g:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_a

    .line 533
    move-object/from16 v0, p1

    iget v2, v0, Lcom/amap/api/a/t;->e:F

    .line 534
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/amap/api/a/t;->m:Landroid/graphics/Point;

    .line 535
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Lcom/amap/api/a/b;->a(FLandroid/graphics/Point;Z)V

    goto :goto_1

    .line 536
    :cond_a
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->i:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_b

    .line 537
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->h:Lcom/amap/api/maps2d/model/CameraPosition;

    .line 538
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v3

    iget v4, v2, Lcom/amap/api/maps2d/model/CameraPosition;->zoom:F

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/amap/api/a/av;->c(I)I

    .line 539
    iget-object v3, v2, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v3, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    const-wide v5, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v3, v5

    double-to-int v3, v3

    .line 540
    iget-object v2, v2, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v2, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    const-wide v6, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 541
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v4

    new-instance v5, Lcom/amap/api/a/ac;

    invoke-direct {v5, v3, v2}, Lcom/amap/api/a/ac;-><init>(II)V

    move-wide/from16 v0, p2

    long-to-int v2, v0

    invoke-virtual {v4, v5, v2}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;I)V

    goto/16 :goto_1

    .line 542
    :cond_b
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->c:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_c

    .line 543
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->h:Lcom/amap/api/maps2d/model/CameraPosition;

    .line 544
    iget-object v3, v2, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v3, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    const-wide v5, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v3, v5

    double-to-int v3, v3

    .line 545
    iget-object v2, v2, Lcom/amap/api/maps2d/model/CameraPosition;->target:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v2, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    const-wide v6, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v4, v6

    double-to-int v2, v4

    .line 546
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v4

    new-instance v5, Lcom/amap/api/a/ac;

    invoke-direct {v5, v3, v2}, Lcom/amap/api/a/ac;-><init>(II)V

    move-wide/from16 v0, p2

    long-to-int v2, v0

    invoke-virtual {v4, v5, v2}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;I)V

    goto/16 :goto_1

    .line 547
    :cond_c
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->j:Lcom/amap/api/a/t$a;

    if-eq v2, v3, :cond_d

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->k:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_f

    .line 551
    :cond_d
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    sget-object v3, Lcom/amap/api/a/t$a;->j:Lcom/amap/api/a/t$a;

    if-ne v2, v3, :cond_e

    .line 552
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v5

    .line 553
    invoke-virtual/range {p0 .. p0}, Lcom/amap/api/a/b;->getHeight()I

    move-result v6

    .line 558
    :goto_2
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/amap/api/a/t;->i:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 559
    move-object/from16 v0, p1

    iget v7, v0, Lcom/amap/api/a/t;->j:I

    .line 560
    new-instance v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 561
    invoke-direct/range {p0 .. p0}, Lcom/amap/api/a/b;->X()Lcom/autonavi/amap/mapcore2d/IPoint;

    move-result-object v11

    .line 562
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v2}, Lcom/amap/api/a/bf$d;->e()I

    move-result v2

    int-to-float v15, v2

    .line 563
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Lcom/amap/api/a/r;->a(Landroid/view/animation/Interpolator;)V

    .line 565
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 566
    new-instance v2, Lcom/amap/api/a/b$1;

    move-object/from16 v3, p0

    move-wide/from16 v8, p2

    invoke-direct/range {v2 .. v10}, Lcom/amap/api/a/b$1;-><init>(Lcom/amap/api/a/b;Lcom/amap/api/maps2d/model/LatLngBounds;IIIJLcom/amap/api/maps2d/AMap$CancelableCallback;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 587
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/amap/api/a/b;->I:Z

    .line 588
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    iget v3, v11, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v4, v11, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const-wide/16 v13, 0xfa

    move v5, v15

    invoke-virtual/range {v2 .. v14}, Lcom/amap/api/a/r;->a(IIFFFIIFFFJ)V

    goto/16 :goto_1

    .line 555
    :cond_e
    move-object/from16 v0, p1

    iget v5, v0, Lcom/amap/api/a/t;->k:I

    .line 556
    move-object/from16 v0, p1

    iget v6, v0, Lcom/amap/api/a/t;->l:I

    goto :goto_2

    .line 592
    :cond_f
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Lcom/amap/api/a/t;->o:Z

    .line 593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/amap/api/a/b;->d:Lcom/amap/api/a/ax;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/amap/api/a/ax;->a(Lcom/amap/api/a/t;)V

    goto/16 :goto_1
.end method

.method public a(Lcom/amap/api/a/t;Lcom/amap/api/maps2d/AMap$CancelableCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 492
    const-wide/16 v0, 0xfa

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/t;JLcom/amap/api/maps2d/AMap$CancelableCallback;)V

    .line 494
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 923
    iput-object p1, p0, Lcom/amap/api/a/b;->O:Lcom/amap/api/maps2d/AMap$InfoWindowAdapter;

    .line 925
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 2919
    iput-object p1, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    .line 2920
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 916
    iput-object p1, p0, Lcom/amap/api/a/b;->N:Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;

    .line 918
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnMapClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 882
    iput-object p1, p0, Lcom/amap/api/a/b;->ad:Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    .line 883
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 909
    iput-object p1, p0, Lcom/amap/api/a/b;->ac:Lcom/amap/api/maps2d/AMap$OnMapLoadedListener;

    .line 911
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 888
    iput-object p1, p0, Lcom/amap/api/a/b;->ab:Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;

    .line 890
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;)V
    .locals 1

    .prologue
    .line 1110
    iput-object p1, p0, Lcom/amap/api/a/b;->ag:Lcom/amap/api/maps2d/AMap$OnMapScreenShotListener;

    .line 1111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/b;->T:Z

    .line 1112
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 895
    iput-object p1, p0, Lcom/amap/api/a/b;->Q:Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;

    .line 897
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 902
    iput-object p1, p0, Lcom/amap/api/a/b;->aa:Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;

    .line 904
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1035
    iput-object p1, p0, Lcom/amap/api/a/b;->v:Lcom/amap/api/maps2d/AMap$OnMyLocationChangeListener;

    .line 1036
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/LocationSource;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 858
    iput-object p1, p0, Lcom/amap/api/a/b;->A:Lcom/amap/api/maps2d/LocationSource;

    .line 859
    if-eqz p1, :cond_0

    .line 860
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/au;->a(Z)V

    .line 864
    :goto_0
    return-void

    .line 862
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/a/au;->a(Z)V

    goto :goto_0
.end method

.method a(Lcom/amap/api/maps2d/model/CameraPosition;)V
    .locals 2

    .prologue
    .line 2924
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 2925
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 2926
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2927
    iget-object v1, p0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2928
    return-void
.end method

.method public a(Lcom/amap/api/maps2d/model/MyLocationStyle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 837
    invoke-virtual {p0}, Lcom/amap/api/a/b;->o()Lcom/amap/api/a/bh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 838
    invoke-virtual {p0}, Lcom/amap/api/a/b;->o()Lcom/amap/api/a/bh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bh;->a(Lcom/amap/api/maps2d/model/MyLocationStyle;)V

    .line 840
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 372
    return-void
.end method

.method protected a(ZLcom/amap/api/maps2d/model/CameraPosition;)V
    .locals 1

    .prologue
    .line 2897
    iget-object v0, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    if-nez v0, :cond_1

    .line 2914
    :cond_0
    :goto_0
    return-void

    .line 2900
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    invoke-virtual {v0}, Lcom/amap/api/a/r;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2903
    invoke-virtual {p0}, Lcom/amap/api/a/b;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2906
    if-nez p2, :cond_2

    .line 2908
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->g()Lcom/amap/api/maps2d/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 2913
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    invoke-interface {v0, p2}, Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;->onCameraChangeFinish(Lcom/amap/api/maps2d/model/CameraPosition;)V

    goto :goto_0

    .line 2909
    :catch_0
    move-exception v0

    .line 2910
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public a(FF)Z
    .locals 2

    .prologue
    .line 2549
    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/av;->a(Z)V

    .line 2550
    iget-boolean v0, p0, Lcom/amap/api/a/b;->ar:Z

    if-eqz v0, :cond_0

    .line 2551
    iget v0, p0, Lcom/amap/api/a/b;->as:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/amap/api/a/b;->as:F

    .line 2552
    iget v0, p0, Lcom/amap/api/a/b;->at:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/amap/api/a/b;->at:F

    .line 2554
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->invalidate()V

    .line 2555
    iget-boolean v0, p0, Lcom/amap/api/a/b;->ar:Z

    return v0
.end method

.method public a(FLandroid/graphics/PointF;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2595
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iput-boolean v5, v0, Lcom/amap/api/a/bf$a;->c:Z

    .line 2596
    new-instance v0, Lcom/amap/api/maps2d/model/CameraPosition;

    invoke-direct {p0}, Lcom/amap/api/a/b;->W()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amap/api/a/b;->f()F

    move-result v2

    invoke-direct {v0, v1, v2, v4, v4}, Lcom/amap/api/maps2d/model/CameraPosition;-><init>(Lcom/amap/api/maps2d/model/LatLng;FFF)V

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/maps2d/model/CameraPosition;)V

    .line 2598
    iget v0, p0, Lcom/amap/api/a/b;->as:F

    iget v1, p0, Lcom/amap/api/a/b;->at:F

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/amap/api/a/b;->a(FLandroid/graphics/PointF;FF)V

    .line 2599
    iput-boolean v5, p0, Lcom/amap/api/a/b;->ar:Z

    .line 2604
    const-wide/16 v0, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/amap/api/a/b;->postInvalidateDelayed(J)V

    .line 2605
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/bf;->a(Z)V

    .line 2606
    invoke-direct {p0}, Lcom/amap/api/a/b;->V()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    .line 2607
    invoke-virtual {p0, v3, v0}, Lcom/amap/api/a/b;->a(ZLcom/amap/api/maps2d/model/CameraPosition;)V

    .line 2608
    return v3
.end method

.method public a(Landroid/graphics/Matrix;)Z
    .locals 1

    .prologue
    .line 2573
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/graphics/PointF;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2615
    :try_start_0
    iget-object v1, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    invoke-interface {v1}, Lcom/amap/api/a/aq;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    .line 2616
    const/4 v0, 0x0

    .line 2627
    :goto_0
    return v0

    .line 2618
    :catch_0
    move-exception v1

    .line 2619
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2621
    :cond_0
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-boolean v2, p0, Lcom/amap/api/a/b;->o:Z

    invoke-virtual {v1, v2}, Lcom/amap/api/a/bf;->a(Z)V

    .line 2622
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/bf$a;->a(Z)V

    .line 2623
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iput-boolean v0, v1, Lcom/amap/api/a/bf$a;->c:Z

    .line 2624
    iput-boolean v0, p0, Lcom/amap/api/a/b;->ar:Z

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2289
    .line 2291
    iget-object v0, p0, Lcom/amap/api/a/b;->aj:Lcom/amap/api/a/bg;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bg;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2292
    if-nez v0, :cond_0

    .line 2293
    iget-object v0, p0, Lcom/amap/api/a/b;->ai:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2295
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_1

    iget-boolean v1, p0, Lcom/amap/api/a/b;->Z:Z

    if-eqz v1, :cond_1

    .line 2296
    iget-object v1, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    if-eqz v1, :cond_1

    .line 2297
    invoke-direct {p0}, Lcom/amap/api/a/b;->V()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v1

    .line 2298
    invoke-virtual {p0, v3, v1}, Lcom/amap/api/a/b;->a(ZLcom/amap/api/maps2d/model/CameraPosition;)V

    .line 2307
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 2308
    invoke-direct {p0, p1}, Lcom/amap/api/a/b;->b(Landroid/view/MotionEvent;)V

    .line 2310
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v3, :cond_3

    .line 2311
    invoke-direct {p0}, Lcom/amap/api/a/b;->T()V

    .line 2313
    :cond_3
    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 424
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ab;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected b(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 11

    .prologue
    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    .line 2854
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 2855
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v1

    .line 2856
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getHeight()I

    move-result v2

    .line 2857
    iget v3, p1, Landroid/graphics/PointF;->x:F

    shr-int/lit8 v4, v1, 0x1

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 2858
    iget v4, p1, Landroid/graphics/PointF;->y:F

    shr-int/lit8 v5, v2, 0x1

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 2859
    float-to-double v5, v4

    float-to-double v7, v3

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v5

    .line 2860
    float-to-double v7, v3

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    float-to-double v3, v4

    invoke-static {v3, v4, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    add-double/2addr v3, v7

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    .line 2861
    invoke-virtual {p0}, Lcom/amap/api/a/b;->J()I

    move-result v7

    int-to-double v7, v7

    const-wide v9, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v7, v9

    const-wide v9, 0x4066800000000000L    # 180.0

    div-double/2addr v7, v9

    sub-double/2addr v5, v7

    .line 2862
    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    mul-double/2addr v7, v3

    shr-int/lit8 v1, v1, 0x1

    int-to-double v9, v1

    add-double/2addr v7, v9

    double-to-float v1, v7

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 2863
    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    shr-int/lit8 v1, v2, 0x1

    int-to-double v1, v1

    add-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 2865
    return-object v0
.end method

.method public b()Lcom/amap/api/a/ba;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    return-object v0
.end method

.method public b(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/a/be;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 720
    new-instance v0, Lcom/amap/api/a/be;

    iget-object v1, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-direct {v0, p1, v1}, Lcom/amap/api/a/be;-><init>(Lcom/amap/api/maps2d/model/MarkerOptions;Lcom/amap/api/a/ay;)V

    .line 722
    iget-object v1, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ay;->a(Lcom/amap/api/a/aj;)V

    .line 723
    return-object v0
.end method

.method public b(DDLcom/autonavi/amap/mapcore2d/IPoint;)V
    .locals 6

    .prologue
    .line 3049
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v4

    .line 3050
    invoke-static {p1, p2}, Lcom/amap/api/a/y;->a(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 3051
    invoke-static {p3, p4}, Lcom/amap/api/a/y;->a(D)J

    move-result-wide v1

    long-to-int v2, v1

    .line 3052
    new-instance v1, Lcom/amap/api/a/ac;

    invoke-direct {v1, v0, v2}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 3053
    iget-object v0, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget-object v5, p0, Lcom/amap/api/a/b;->J:Lcom/amap/api/a/ba;

    iget-object v5, v5, Lcom/amap/api/a/ba;->h:[D

    aget-wide v4, v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/ba;->b(Lcom/amap/api/a/ac;Lcom/amap/api/a/ac;Landroid/graphics/Point;D)Landroid/graphics/PointF;

    move-result-object v0

    .line 3056
    if-eqz p5, :cond_0

    .line 3057
    iget v1, v0, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iput v1, p5, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    .line 3058
    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    iput v0, p5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 3060
    :cond_0
    return-void
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 2232
    iput p1, p0, Lcom/amap/api/a/b;->aq:F

    .line 2233
    return-void
.end method

.method public b(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 758
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 760
    invoke-virtual {p0, v2}, Lcom/amap/api/a/b;->h(Z)V

    .line 761
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/cf;->a(Z)V

    .line 767
    :goto_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 768
    return-void

    .line 764
    :cond_0
    invoke-virtual {p0, v1}, Lcom/amap/api/a/b;->h(Z)V

    .line 765
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cf;->a(Z)V

    goto :goto_0
.end method

.method public b(IILcom/autonavi/amap/mapcore2d/DPoint;)V
    .locals 2

    .prologue
    .line 3041
    if-eqz p3, :cond_0

    .line 3042
    int-to-long v0, p1

    invoke-static {v0, v1}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v0

    iput-wide v0, p3, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    .line 3043
    int-to-long v0, p2

    invoke-static {v0, v1}, Lcom/amap/api/a/y;->a(J)D

    move-result-wide v0

    iput-wide v0, p3, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    .line 3045
    :cond_0
    return-void
.end method

.method public b(Lcom/amap/api/a/t;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 485
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/t;Lcom/amap/api/maps2d/AMap$CancelableCallback;)V

    .line 486
    return-void
.end method

.method public b(Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 777
    invoke-virtual {p0, p1}, Lcom/amap/api/a/b;->i(Z)V

    .line 778
    invoke-virtual {p0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 779
    return-void
.end method

.method public b(Landroid/graphics/Matrix;)Z
    .locals 1

    .prologue
    .line 2579
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    invoke-interface {v0}, Lcom/amap/api/a/aq;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    .line 2580
    const/4 v0, 0x0

    .line 2588
    :goto_0
    return v0

    .line 2582
    :catch_0
    move-exception v0

    .line 2583
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2585
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 2587
    invoke-virtual {p0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 2588
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(Lcom/amap/api/a/aj;)Z
    .locals 2

    .prologue
    .line 998
    iget-object v0, p0, Lcom/amap/api/a/b;->P:Lcom/amap/api/maps2d/model/Marker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 999
    iget-object v0, p0, Lcom/amap/api/a/b;->P:Lcom/amap/api/maps2d/model/Marker;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/Marker;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/amap/api/a/aj;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1001
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 429
    const/4 v1, 0x0

    .line 431
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ay;->a(Ljava/lang/String;)Lcom/amap/api/a/aj;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 435
    :goto_0
    if-eqz v0, :cond_0

    .line 436
    iget-object v1, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ay;->b(Lcom/amap/api/a/aj;)Z

    move-result v0

    .line 438
    :goto_1
    return v0

    .line 432
    :catch_0
    move-exception v0

    .line 433
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    .line 438
    :cond_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c()I
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->c()I

    move-result v0

    return v0
.end method

.method protected c(Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 11

    .prologue
    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    .line 2869
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 2870
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v1

    .line 2871
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getHeight()I

    move-result v2

    .line 2872
    iget v3, p1, Landroid/graphics/PointF;->x:F

    shr-int/lit8 v4, v1, 0x1

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 2873
    iget v4, p1, Landroid/graphics/PointF;->y:F

    shr-int/lit8 v5, v2, 0x1

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 2874
    float-to-double v5, v4

    float-to-double v7, v3

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v5

    .line 2875
    float-to-double v7, v3

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    float-to-double v3, v4

    invoke-static {v3, v4, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    add-double/2addr v3, v7

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    .line 2876
    invoke-virtual {p0}, Lcom/amap/api/a/b;->J()I

    move-result v7

    int-to-double v7, v7

    const-wide v9, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v7, v9

    const-wide v9, 0x4066800000000000L    # 180.0

    div-double/2addr v7, v9

    add-double/2addr v5, v7

    .line 2877
    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v7

    mul-double/2addr v7, v3

    shr-int/lit8 v1, v1, 0x1

    int-to-double v9, v1

    add-double/2addr v7, v9

    double-to-float v1, v7

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 2878
    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    shr-int/lit8 v1, v2, 0x1

    int-to-double v1, v1

    add-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 2879
    return-object v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    if-eqz v0, :cond_0

    .line 1122
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/cf;->a(I)V

    .line 1123
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    invoke-virtual {v0}, Lcom/amap/api/a/cf;->invalidate()V

    .line 1124
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v0}, Lcom/amap/api/a/bo;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1125
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v0}, Lcom/amap/api/a/bo;->invalidate()V

    .line 1128
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 800
    iget-object v0, p0, Lcom/amap/api/a/b;->A:Lcom/amap/api/maps2d/LocationSource;

    if-eqz v0, :cond_3

    .line 801
    if-eqz p1, :cond_1

    .line 802
    iget-object v0, p0, Lcom/amap/api/a/b;->A:Lcom/amap/api/maps2d/LocationSource;

    iget-object v1, p0, Lcom/amap/api/a/b;->u:Lcom/amap/api/a/e;

    invoke-interface {v0, v1}, Lcom/amap/api/maps2d/LocationSource;->activate(Lcom/amap/api/maps2d/LocationSource$OnLocationChangedListener;)V

    .line 803
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/au;->a(Z)V

    .line 804
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    if-nez v0, :cond_0

    .line 805
    new-instance v0, Lcom/amap/api/a/bh;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bh;-><init>(Lcom/amap/api/a/af;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    .line 819
    :cond_0
    :goto_0
    if-eqz p1, :cond_4

    .line 820
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/au;->setVisibility(I)V

    .line 824
    :goto_1
    iput-boolean p1, p0, Lcom/amap/api/a/b;->w:Z

    .line 825
    return-void

    .line 808
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    if-eqz v0, :cond_2

    .line 809
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    invoke-virtual {v0}, Lcom/amap/api/a/bh;->a()V

    .line 810
    iput-object v1, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    .line 812
    :cond_2
    iput-object v1, p0, Lcom/amap/api/a/b;->t:Landroid/location/Location;

    .line 813
    iget-object v0, p0, Lcom/amap/api/a/b;->A:Lcom/amap/api/maps2d/LocationSource;

    invoke-interface {v0}, Lcom/amap/api/maps2d/LocationSource;->deactivate()V

    .line 814
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/au;->a(Z)V

    goto :goto_0

    .line 817
    :cond_3
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/au;->a(Z)V

    goto :goto_0

    .line 822
    :cond_4
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/a/au;->setVisibility(I)V

    goto :goto_1
.end method

.method public c(F)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2561
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    invoke-interface {v0}, Lcom/amap/api/a/aq;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_0

    .line 2568
    :goto_0
    return v1

    .line 2564
    :catch_0
    move-exception v0

    .line 2565
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2567
    :cond_0
    invoke-virtual {p0, p1}, Lcom/amap/api/a/b;->b(F)V

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1714
    iget-object v0, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1716
    iget-object v0, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget v1, p0, Lcom/amap/api/a/b;->ao:I

    sub-int/2addr v0, v1

    .line 1717
    iget-object v1, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    iget v2, p0, Lcom/amap/api/a/b;->ap:I

    sub-int/2addr v1, v2

    .line 1718
    iget-object v2, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    iput v2, p0, Lcom/amap/api/a/b;->ao:I

    .line 1719
    iget-object v2, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    iput v2, p0, Lcom/amap/api/a/b;->ap:I

    .line 1720
    iget-object v2, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    iget-object v3, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    add-int/2addr v0, v3

    iget-object v3, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, v3, Lcom/amap/api/a/ba;->l:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v0, v1}, Lcom/amap/api/a/bf$e;->a(II)Lcom/amap/api/a/ac;

    move-result-object v0

    .line 1723
    iget-object v1, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1724
    iget-object v0, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    if-eqz v0, :cond_0

    .line 1725
    invoke-direct {p0}, Lcom/amap/api/a/b;->V()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    .line 1726
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/amap/api/a/b;->a(ZLcom/amap/api/maps2d/model/CameraPosition;)V

    .line 1735
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v4, v4}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    .line 1742
    :goto_0
    return-void

    .line 1737
    :cond_1
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/bf$d;->b(Lcom/amap/api/a/ac;)V

    goto :goto_0

    .line 1741
    :cond_2
    invoke-super {p0}, Landroid/view/View;->computeScroll()V

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->d()I

    move-result v0

    return v0
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 1132
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    if-eqz v0, :cond_0

    .line 1133
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/cg;->a(I)V

    .line 1134
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    invoke-virtual {v0}, Lcom/amap/api/a/cg;->invalidate()V

    .line 1136
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 1040
    if-eqz p1, :cond_0

    .line 1041
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cg;->setVisibility(I)V

    .line 1045
    :goto_0
    return-void

    .line 1043
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cg;->setVisibility(I)V

    goto :goto_0
.end method

.method public e()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 361
    iget-object v0, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    return-object v0
.end method

.method e(I)V
    .locals 0

    .prologue
    .line 1443
    iput p1, p0, Lcom/amap/api/a/b;->r:I

    .line 1444
    return-void
.end method

.method public e(Z)V
    .locals 2

    .prologue
    .line 1049
    if-eqz p1, :cond_0

    .line 1050
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/a/au;->setVisibility(I)V

    .line 1054
    :goto_0
    return-void

    .line 1052
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/a/au;->setVisibility(I)V

    goto :goto_0
.end method

.method public f()F
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public f(Z)V
    .locals 2

    .prologue
    .line 1058
    if-eqz p1, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/amap/api/a/b;->B:Lcom/amap/api/a/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/amap/api/a/w;->setVisibility(I)V

    .line 1063
    :goto_0
    return-void

    .line 1061
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->B:Lcom/amap/api/a/w;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/a/w;->setVisibility(I)V

    goto :goto_0
.end method

.method public g()Lcom/amap/api/maps2d/model/CameraPosition;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 459
    invoke-direct {p0}, Lcom/amap/api/a/b;->W()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    .line 460
    invoke-virtual {p0}, Lcom/amap/api/a/b;->f()F

    move-result v1

    .line 461
    invoke-static {}, Lcom/amap/api/maps2d/model/CameraPosition;->builder()Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->target(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->build()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    return-object v0
.end method

.method public g(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1067
    if-eqz p1, :cond_0

    .line 1068
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/bo;->setVisibility(I)V

    .line 1069
    invoke-virtual {p0}, Lcom/amap/api/a/b;->K()V

    .line 1075
    :goto_0
    return-void

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bo;->a(Ljava/lang/String;)V

    .line 1072
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v0, v2}, Lcom/amap/api/a/bo;->a(I)V

    .line 1073
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bo;->setVisibility(I)V

    goto :goto_0
.end method

.method public h()F
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->a()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public h(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1491
    invoke-virtual {p0}, Lcom/amap/api/a/b;->E()Z

    move-result v0

    .line 1492
    if-ne v0, p1, :cond_0

    .line 1545
    :goto_0
    return-void

    .line 1495
    :cond_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->F()Z

    move-result v0

    .line 1496
    invoke-virtual {p0, v5}, Lcom/amap/api/a/b;->i(Z)V

    .line 1497
    if-nez p1, :cond_2

    .line 1498
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v2, v2, Lcom/amap/api/a/bf$a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    .line 1500
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v2, v2, Lcom/amap/api/a/bf$a;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    .line 1502
    if-ne v0, v4, :cond_1

    .line 1503
    invoke-virtual {p0, v4}, Lcom/amap/api/a/b;->i(Z)V

    .line 1505
    :cond_1
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v5, v5}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto :goto_0

    .line 1509
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v2, v2, Lcom/amap/api/a/bf$a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;)Lcom/amap/api/a/as;

    move-result-object v1

    .line 1511
    if-eqz v1, :cond_4

    .line 1512
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v2, v2, Lcom/amap/api/a/bf$a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    .line 1514
    if-ne v0, v4, :cond_3

    .line 1515
    invoke-virtual {p0, v4}, Lcom/amap/api/a/b;->i(Z)V

    .line 1517
    :cond_3
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v5, v5}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto :goto_0

    .line 1520
    :cond_4
    new-instance v1, Lcom/amap/api/a/as;

    invoke-direct {v1}, Lcom/amap/api/a/as;-><init>()V

    .line 1521
    new-instance v2, Lcom/amap/api/a/b$2;

    invoke-direct {v2, p0}, Lcom/amap/api/a/b$2;-><init>(Lcom/amap/api/a/b;)V

    iput-object v2, v1, Lcom/amap/api/a/as;->j:Lcom/amap/api/a/cd;

    .line 1530
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v2, v2, Lcom/amap/api/a/bf$a;->e:Ljava/lang/String;

    iput-object v2, v1, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    .line 1531
    iput-boolean v4, v1, Lcom/amap/api/a/as;->e:Z

    .line 1532
    iput-boolean v4, v1, Lcom/amap/api/a/as;->d:Z

    .line 1533
    iput-boolean v4, v1, Lcom/amap/api/a/as;->f:Z

    .line 1534
    iput-boolean v4, v1, Lcom/amap/api/a/as;->g:Z

    .line 1535
    sget v2, Lcom/amap/api/a/x;->c:I

    iput v2, v1, Lcom/amap/api/a/as;->b:I

    .line 1536
    sget v2, Lcom/amap/api/a/x;->d:I

    iput v2, v1, Lcom/amap/api/a/as;->c:I

    .line 1537
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/amap/api/a/bf$a;->a(Lcom/amap/api/a/as;Landroid/content/Context;)Z

    .line 1538
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v2, v2, Lcom/amap/api/a/bf$a;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    .line 1540
    if-ne v0, v4, :cond_5

    .line 1541
    invoke-virtual {p0, v4}, Lcom/amap/api/a/b;->i(Z)V

    .line 1544
    :cond_5
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v5, v5}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto/16 :goto_0
.end method

.method public i()F
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->b()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public i(Z)V
    .locals 9

    .prologue
    const-wide/32 v7, 0x1d4c0

    const/16 v6, 0x12

    const/16 v3, 0x9

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1559
    invoke-virtual {p0}, Lcom/amap/api/a/b;->F()Z

    move-result v0

    .line 1560
    if-ne p1, v0, :cond_1

    .line 1623
    :cond_0
    :goto_0
    return-void

    .line 1563
    :cond_1
    invoke-virtual {p0}, Lcom/amap/api/a/b;->E()Z

    move-result v0

    .line 1564
    const-string v1, ""

    .line 1565
    if-nez v0, :cond_0

    .line 1566
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v1

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->f:Ljava/lang/String;

    .line 1570
    if-nez p1, :cond_2

    .line 1571
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0, v1, v4}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    .line 1572
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v4, v4}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto :goto_0

    .line 1576
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v2, v1}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;)Lcom/amap/api/a/as;

    move-result-object v2

    .line 1577
    if-eqz v2, :cond_3

    .line 1578
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0, v1, v5}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    .line 1579
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v4, v4}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto :goto_0

    .line 1582
    :cond_3
    if-ne v0, v5, :cond_4

    .line 1584
    new-instance v0, Lcom/amap/api/a/as;

    invoke-direct {v0}, Lcom/amap/api/a/as;-><init>()V

    .line 1585
    iput-boolean v5, v0, Lcom/amap/api/a/as;->h:Z

    .line 1586
    iput-wide v7, v0, Lcom/amap/api/a/as;->i:J

    .line 1588
    iput-object v1, v0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    .line 1589
    iput-boolean v4, v0, Lcom/amap/api/a/as;->e:Z

    .line 1590
    iput-boolean v5, v0, Lcom/amap/api/a/as;->d:Z

    .line 1591
    iput-boolean v5, v0, Lcom/amap/api/a/as;->f:Z

    .line 1592
    iput-boolean v4, v0, Lcom/amap/api/a/as;->g:Z

    .line 1593
    iput v6, v0, Lcom/amap/api/a/as;->b:I

    .line 1594
    iput v3, v0, Lcom/amap/api/a/as;->c:I

    .line 1595
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/amap/api/a/bf$a;->a(Lcom/amap/api/a/as;Landroid/content/Context;)Z

    .line 1621
    :goto_1
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0, v1, v5}, Lcom/amap/api/a/bf$a;->a(Ljava/lang/String;Z)Z

    .line 1622
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v4, v4}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto :goto_0

    .line 1597
    :cond_4
    new-instance v0, Lcom/amap/api/a/as;

    invoke-direct {v0}, Lcom/amap/api/a/as;-><init>()V

    .line 1598
    iput-boolean v5, v0, Lcom/amap/api/a/as;->h:Z

    .line 1599
    iput-wide v7, v0, Lcom/amap/api/a/as;->i:J

    .line 1601
    new-instance v2, Lcom/amap/api/a/b$3;

    invoke-direct {v2, p0}, Lcom/amap/api/a/b$3;-><init>(Lcom/amap/api/a/b;)V

    iput-object v2, v0, Lcom/amap/api/a/as;->j:Lcom/amap/api/a/cd;

    .line 1612
    iput-object v1, v0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    .line 1613
    iput-boolean v4, v0, Lcom/amap/api/a/as;->e:Z

    .line 1614
    iput-boolean v5, v0, Lcom/amap/api/a/as;->d:Z

    .line 1615
    iput-boolean v5, v0, Lcom/amap/api/a/as;->f:Z

    .line 1616
    iput-boolean v4, v0, Lcom/amap/api/a/as;->g:Z

    .line 1617
    iput v6, v0, Lcom/amap/api/a/as;->b:I

    .line 1618
    iput v3, v0, Lcom/amap/api/a/as;->c:I

    .line 1619
    invoke-virtual {p0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v2

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/amap/api/a/bf$a;->a(Lcom/amap/api/a/as;Landroid/content/Context;)Z

    goto :goto_1
.end method

.method public j()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 631
    iget-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    invoke-virtual {v0}, Lcom/amap/api/a/r;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 632
    iget-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/r;->a(Z)V

    .line 633
    iget-object v0, p0, Lcom/amap/api/a/b;->F:Lcom/amap/api/maps2d/AMap$OnCameraChangeListener;

    if-eqz v0, :cond_0

    .line 634
    invoke-direct {p0}, Lcom/amap/api/a/b;->V()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    .line 635
    invoke-virtual {p0, v1, v0}, Lcom/amap/api/a/b;->a(ZLcom/amap/api/maps2d/model/CameraPosition;)V

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    if-eqz v0, :cond_1

    .line 638
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps2d/AMap$CancelableCallback;->onCancel()V

    .line 640
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 642
    :cond_2
    invoke-virtual {p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/amap/api/a/av;->a(Z)V

    .line 643
    return-void
.end method

.method public k()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 738
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/b;->t()V

    .line 739
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v0}, Lcom/amap/api/a/ab;->a()V

    .line 740
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->c()V

    .line 741
    invoke-virtual {p0}, Lcom/amap/api/a/b;->invalidate()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 748
    :goto_0
    return-void

    .line 742
    :catch_0
    move-exception v0

    .line 743
    const-string v1, "amapApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AMapDelegateImpGLSurfaceView clear erro"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public l()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 753
    const/4 v0, 0x0

    return v0
.end method

.method public m()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 772
    invoke-virtual {p0}, Lcom/amap/api/a/b;->F()Z

    move-result v0

    return v0
.end method

.method public n()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 795
    iget-boolean v0, p0, Lcom/amap/api/a/b;->w:Z

    return v0
.end method

.method public o()Lcom/amap/api/a/bh;
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/amap/api/a/b;->x:Lcom/amap/api/a/bh;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 1709
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 1710
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2640
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    invoke-interface {v0}, Lcom/amap/api/a/aq;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_1

    .line 2656
    :cond_0
    :goto_0
    return v3

    .line 2643
    :catch_0
    move-exception v0

    .line 2644
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2646
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/a/b;->q:Z

    if-eqz v0, :cond_2

    .line 2647
    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/a/av;->b(II)Z

    .line 2649
    :cond_2
    iget v0, p0, Lcom/amap/api/a/b;->aA:I

    if-gt v0, v3, :cond_0

    .line 2652
    iput-boolean v3, p0, Lcom/amap/api/a/b;->aB:Z

    .line 2653
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->e()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cg;->a(F)V

    .line 2654
    new-instance v0, Lcom/amap/api/maps2d/model/CameraPosition;

    invoke-direct {p0}, Lcom/amap/api/a/b;->W()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amap/api/a/b;->f()F

    move-result v2

    invoke-direct {v0, v1, v2, v4, v4}, Lcom/amap/api/maps2d/model/CameraPosition;-><init>(Lcom/amap/api/maps2d/model/LatLng;FFF)V

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/maps2d/model/CameraPosition;)V

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2661
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2328
    iput-boolean v1, p0, Lcom/amap/api/a/b;->Z:Z

    .line 2329
    iget-boolean v0, p0, Lcom/amap/api/a/b;->aB:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    invoke-virtual {v0}, Lcom/amap/api/a/r;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2330
    iget-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/r;->a(Z)V

    .line 2332
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    if-eqz v0, :cond_0

    .line 2333
    iget-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    invoke-interface {v0}, Lcom/amap/api/maps2d/AMap$CancelableCallback;->onCancel()V

    .line 2334
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 2336
    :cond_1
    iput-boolean v1, p0, Lcom/amap/api/a/b;->aB:Z

    .line 2337
    iput v1, p0, Lcom/amap/api/a/b;->aA:I

    .line 2338
    iget-object v0, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    if-nez v0, :cond_2

    .line 2339
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    .line 2343
    :goto_0
    return v3

    .line 2341
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/b;->ah:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2191
    iget-boolean v0, p0, Lcom/amap/api/a/b;->T:Z

    if-eqz v0, :cond_0

    .line 2192
    invoke-virtual {p0, v4}, Lcom/amap/api/a/b;->setDrawingCacheEnabled(Z)V

    .line 2193
    invoke-virtual {p0}, Lcom/amap/api/a/b;->buildDrawingCache()V

    .line 2194
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2198
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 2199
    const/16 v2, 0x10

    iput v2, v1, Landroid/os/Message;->what:I

    .line 2200
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2201
    iget-object v0, p0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/b;->T:Z

    .line 2205
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/amap/api/a/b;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/a/bf$d;->a(II)V

    .line 2206
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, p0, Lcom/amap/api/a/b;->k:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/amap/api/a/b;->as:F

    iget v3, p0, Lcom/amap/api/a/b;->at:F

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/amap/api/a/bf$a;->a(Landroid/graphics/Canvas;Landroid/graphics/Matrix;FF)V

    .line 2207
    iget-object v0, p0, Lcom/amap/api/a/b;->G:Lcom/amap/api/a/r;

    invoke-virtual {v0}, Lcom/amap/api/a/r;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2208
    iget-object v0, p0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2210
    :cond_1
    iget-boolean v0, p0, Lcom/amap/api/a/b;->ae:Z

    if-nez v0, :cond_2

    .line 2211
    iget-object v0, p0, Lcom/amap/api/a/b;->j:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2212
    iput-boolean v4, p0, Lcom/amap/api/a/b;->ae:Z

    .line 2225
    :cond_2
    return-void
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 2359
    iget-object v0, p0, Lcom/amap/api/a/b;->aj:Lcom/amap/api/a/bg;

    iget-boolean v0, v0, Lcom/amap/api/a/bg;->l:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/amap/api/a/b;->aj:Lcom/amap/api/a/bg;

    iget-wide v2, v2, Lcom/amap/api/a/bg;->p:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1e

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2384
    :cond_0
    :goto_0
    return v9

    .line 2365
    :cond_1
    invoke-virtual {p0}, Lcom/amap/api/a/b;->invalidate()V

    .line 2366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/b;->Z:Z

    .line 2368
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    invoke-interface {v0}, Lcom/amap/api/a/aq;->e()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2374
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/b;->H:Lcom/amap/api/maps2d/AMap$CancelableCallback;

    .line 2375
    iget-object v0, p0, Lcom/amap/api/a/b;->an:Landroid/widget/Scroller;

    iget v1, p0, Lcom/amap/api/a/b;->ao:I

    iget v2, p0, Lcom/amap/api/a/b;->ap:I

    neg-float v3, p3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x5

    neg-float v4, p4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x5

    iget v5, p0, Lcom/amap/api/a/b;->au:I

    neg-int v5, v5

    iget v6, p0, Lcom/amap/api/a/b;->au:I

    iget v7, p0, Lcom/amap/api/a/b;->av:I

    neg-int v7, v7

    iget v8, p0, Lcom/amap/api/a/b;->av:I

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    goto :goto_0

    .line 2371
    :catch_0
    move-exception v0

    .line 2372
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 1473
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1474
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1759
    iget-object v2, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-nez v2, :cond_1

    move v0, v1

    .line 1765
    :cond_0
    :goto_0
    return v0

    .line 1762
    :cond_1
    iget-boolean v2, p0, Lcom/amap/api/a/b;->n:Z

    if-eqz v2, :cond_0

    .line 1765
    iget-object v2, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v2, p1, p2}, Lcom/amap/api/a/bf$a;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v2, p0, p1, p2}, Lcom/amap/api/a/av;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1771
    iget-object v2, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-nez v2, :cond_1

    move v0, v1

    .line 1777
    :cond_0
    :goto_0
    return v0

    .line 1774
    :cond_1
    iget-boolean v2, p0, Lcom/amap/api/a/b;->n:Z

    if-eqz v2, :cond_0

    .line 1777
    iget-object v2, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v2, p1, p2}, Lcom/amap/api/a/bf$a;->b(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v2, p0, p1, p2}, Lcom/amap/api/a/av;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-eqz v0, :cond_0

    .line 1173
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->h()V

    .line 1175
    :cond_0
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2390
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/b;->Z:Z

    .line 2391
    iget-object v0, p0, Lcom/amap/api/a/b;->ab:Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;

    if-eqz v0, :cond_0

    .line 2392
    new-instance v0, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 2393
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v1, v2, v0}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 2394
    iget-object v1, p0, Lcom/amap/api/a/b;->ab:Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;

    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v5, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v2}, Lcom/amap/api/maps2d/AMap$OnMapLongClickListener;->onMapLongClick(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 2395
    iput-boolean v7, p0, Lcom/amap/api/a/b;->K:Z

    .line 2397
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ay;->a(Landroid/view/MotionEvent;)Lcom/amap/api/a/aj;

    move-result-object v6

    .line 2399
    iget-object v0, p0, Lcom/amap/api/a/b;->aa:Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;

    if-eqz v0, :cond_1

    if-eqz v6, :cond_1

    invoke-interface {v6}, Lcom/amap/api/a/aj;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2401
    new-instance v0, Lcom/amap/api/maps2d/model/Marker;

    invoke-direct {v0, v6}, Lcom/amap/api/maps2d/model/Marker;-><init>(Lcom/amap/api/a/aj;)V

    iput-object v0, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    .line 2402
    iget-object v0, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/Marker;->getPosition()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    .line 2403
    new-instance v5, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 2404
    iget-wide v1, v0, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, v0, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/b;->b(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 2405
    iget v0, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    add-int/lit8 v0, v0, -0x3c

    iput v0, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    .line 2406
    new-instance v0, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 2407
    iget v1, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v2, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    invoke-virtual {p0, v1, v2, v0}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 2408
    new-instance v1, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v4, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    .line 2409
    iget-object v0, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    invoke-virtual {v0, v1}, Lcom/amap/api/maps2d/model/Marker;->setPosition(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 2410
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, v6}, Lcom/amap/api/a/ay;->c(Lcom/amap/api/a/aj;)V

    .line 2411
    iget-object v0, p0, Lcom/amap/api/a/b;->aa:Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;

    iget-object v1, p0, Lcom/amap/api/a/b;->W:Lcom/amap/api/maps2d/model/Marker;

    invoke-interface {v0, v1}, Lcom/amap/api/maps2d/AMap$OnMarkerDragListener;->onMarkerDragStart(Lcom/amap/api/maps2d/model/Marker;)V

    .line 2412
    iput-boolean v7, p0, Lcom/amap/api/a/b;->V:Z

    .line 2414
    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 0

    .prologue
    .line 1802
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 1803
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1698
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1699
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 1704
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2420
    iget-object v0, p0, Lcom/amap/api/a/b;->aj:Lcom/amap/api/a/bg;

    iget-boolean v0, v0, Lcom/amap/api/a/bg;->l:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/amap/api/a/b;->aj:Lcom/amap/api/a/bg;

    iget-wide v2, v2, Lcom/amap/api/a/bg;->p:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1e

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 2443
    :cond_0
    :goto_0
    return v4

    .line 2425
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    invoke-interface {v0}, Lcom/amap/api/a/aq;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2426
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/b;->Z:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2429
    :catch_0
    move-exception v0

    .line 2430
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 2432
    :cond_2
    iget v0, p0, Lcom/amap/api/a/b;->aA:I

    if-le v0, v4, :cond_3

    .line 2433
    iput-boolean v6, p0, Lcom/amap/api/a/b;->Z:Z

    goto :goto_0

    .line 2436
    :cond_3
    iput-boolean v4, p0, Lcom/amap/api/a/b;->Z:Z

    .line 2437
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 2438
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 2439
    invoke-direct {p0, v0, v1}, Lcom/amap/api/a/b;->a(II)V

    .line 2440
    invoke-virtual {p0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 2441
    new-instance v0, Lcom/amap/api/maps2d/model/CameraPosition;

    invoke-direct {p0}, Lcom/amap/api/a/b;->W()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    invoke-virtual {p0}, Lcom/amap/api/a/b;->f()F

    move-result v2

    invoke-direct {v0, v1, v2, v5, v5}, Lcom/amap/api/maps2d/model/CameraPosition;-><init>(Lcom/amap/api/maps2d/model/LatLng;FFF)V

    invoke-virtual {p0, v0}, Lcom/amap/api/a/b;->a(Lcom/amap/api/maps2d/model/CameraPosition;)V

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 2451
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2669
    .line 2670
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 2457
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bf$a;->b(Landroid/view/MotionEvent;)Z

    .line 2458
    iget-object v0, p0, Lcom/amap/api/a/b;->al:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/GestureDetector$OnGestureListener;

    .line 2459
    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 2461
    :cond_0
    iput-boolean v2, p0, Lcom/amap/api/a/b;->Z:Z

    .line 2462
    iget-boolean v0, p0, Lcom/amap/api/a/b;->K:Z

    if-eqz v0, :cond_2

    .line 2463
    iput-boolean v2, p0, Lcom/amap/api/a/b;->K:Z

    .line 2523
    :cond_1
    :goto_1
    return v7

    .line 2469
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 2472
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v3, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2475
    iget-object v1, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/amap/api/a/ay;->a(Landroid/graphics/Rect;II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/amap/api/a/b;->N:Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;

    if-eqz v0, :cond_3

    .line 2478
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->d()Lcom/amap/api/a/aj;

    move-result-object v0

    .line 2479
    invoke-interface {v0}, Lcom/amap/api/a/aj;->m()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2482
    new-instance v1, Lcom/amap/api/maps2d/model/Marker;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/Marker;-><init>(Lcom/amap/api/a/aj;)V

    .line 2483
    iget-object v0, p0, Lcom/amap/api/a/b;->N:Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;

    invoke-interface {v0, v1}, Lcom/amap/api/maps2d/AMap$OnInfoWindowClickListener;->onInfoWindowClick(Lcom/amap/api/maps2d/model/Marker;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2519
    :catch_0
    move-exception v0

    .line 2520
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 2487
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/ay;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 2488
    if-eqz v0, :cond_8

    .line 2489
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->d()Lcom/amap/api/a/aj;

    move-result-object v1

    .line 2490
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/amap/api/a/aj;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2493
    new-instance v0, Lcom/amap/api/maps2d/model/Marker;

    invoke-direct {v0, v1}, Lcom/amap/api/maps2d/model/Marker;-><init>(Lcom/amap/api/a/aj;)V

    .line 2494
    iget-object v2, p0, Lcom/amap/api/a/b;->Q:Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;

    if-eqz v2, :cond_7

    .line 2495
    iget-object v2, p0, Lcom/amap/api/a/b;->Q:Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;

    invoke-interface {v2, v0}, Lcom/amap/api/maps2d/AMap$OnMarkerClickListener;->onMarkerClick(Lcom/amap/api/maps2d/model/Marker;)Z

    move-result v0

    .line 2496
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->b()I

    move-result v0

    if-gtz v0, :cond_5

    .line 2497
    :cond_4
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ay;->c(Lcom/amap/api/a/aj;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 2501
    :cond_5
    :try_start_2
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->d()Lcom/amap/api/a/aj;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 2502
    invoke-virtual {p0, v1}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/aj;)V

    .line 2503
    :cond_6
    invoke-interface {v1}, Lcom/amap/api/a/aj;->c()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    .line 2504
    if-eqz v0, :cond_7

    .line 2505
    invoke-virtual {p0}, Lcom/amap/api/a/b;->D()Lcom/amap/api/a/av;

    move-result-object v2

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/a/ac;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/amap/api/a/av;->a(Lcom/amap/api/a/ac;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2511
    :cond_7
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/ay;->c(Lcom/amap/api/a/aj;)V

    goto/16 :goto_1

    .line 2507
    :catch_1
    move-exception v0

    .line 2508
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 2514
    :cond_8
    iget-object v0, p0, Lcom/amap/api/a/b;->ad:Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    if-eqz v0, :cond_1

    .line 2515
    new-instance v0, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 2516
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v1, v2, v0}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 2517
    iget-object v1, p0, Lcom/amap/api/a/b;->ad:Lcom/amap/api/maps2d/AMap$OnMapClickListener;

    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v5, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-interface {v1, v2}, Lcom/amap/api/maps2d/AMap$OnMapClickListener;->onMapClick(Lcom/amap/api/maps2d/model/LatLng;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1807
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1811
    new-instance v0, Landroid/graphics/Point;

    div-int/lit8 v1, p1, 0x2

    div-int/lit8 v2, p2, 0x2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 1812
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/ba;->a(Landroid/graphics/Point;)V

    .line 1817
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, p1, p2}, Lcom/amap/api/a/bf$d;->a(II)V

    .line 1818
    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v0}, Lcom/amap/api/a/av;->a()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v0}, Lcom/amap/api/a/av;->b()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1819
    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    iget-object v1, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v1}, Lcom/amap/api/a/av;->a()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v2}, Lcom/amap/api/a/av;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/amap/api/a/av;->a(II)V

    .line 1821
    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/av;->a(I)V

    .line 1822
    iget-object v0, p0, Lcom/amap/api/a/b;->a:Lcom/amap/api/a/av;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/av;->b(I)V

    .line 1825
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->aC:Lcom/amap/api/a/b$a;

    if-eqz v0, :cond_1

    .line 1826
    iget-object v0, p0, Lcom/amap/api/a/b;->aC:Lcom/amap/api/a/b$a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/amap/api/a/b$a;->a(IIII)V

    .line 1828
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1783
    sget-boolean v1, Lcom/amap/api/a/x;->j:Z

    if-nez v1, :cond_1

    .line 1797
    :cond_0
    :goto_0
    return v0

    .line 1786
    :cond_1
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-eqz v1, :cond_0

    .line 1789
    iget-boolean v1, p0, Lcom/amap/api/a/b;->n:Z

    if-nez v1, :cond_2

    .line 1790
    const/4 v0, 0x0

    goto :goto_0

    .line 1793
    :cond_2
    iget-object v1, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v1, p1}, Lcom/amap/api/a/bf$a;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1796
    invoke-virtual {p0, p1}, Lcom/amap/api/a/b;->a(Landroid/view/MotionEvent;)Z

    .line 1797
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 1754
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    .line 1755
    return-void
.end method

.method public p()Landroid/location/Location;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 849
    iget-object v0, p0, Lcom/amap/api/a/b;->A:Lcom/amap/api/maps2d/LocationSource;

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/amap/api/a/b;->u:Lcom/amap/api/a/e;

    iget-object v0, v0, Lcom/amap/api/a/e;->a:Landroid/location/Location;

    .line 852
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()Lcom/amap/api/a/aq;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 868
    iget-object v0, p0, Lcom/amap/api/a/b;->g:Lcom/amap/api/a/aq;

    return-object v0
.end method

.method public r()Lcom/amap/api/a/an;
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/amap/api/a/b;->S:Lcom/amap/api/a/an;

    return-object v0
.end method

.method public s()Lcom/amap/api/a/bl;
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    return-object v0
.end method

.method public setClickable(Z)V
    .locals 0

    .prologue
    .line 1748
    iput-boolean p1, p0, Lcom/amap/api/a/b;->n:Z

    .line 1749
    invoke-super {p0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 1750
    return-void
.end method

.method public t()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1006
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1007
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 1008
    iget-object v0, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    iget-object v1, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/az;->removeView(Landroid/view/View;)V

    .line 1009
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1010
    if-eqz v0, :cond_0

    .line 1011
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1014
    :cond_0
    iput-object v2, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    .line 1016
    :cond_1
    iput-object v2, p0, Lcom/amap/api/a/b;->P:Lcom/amap/api/maps2d/model/Marker;

    .line 1018
    return-void
.end method

.method public u()V
    .locals 2

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/amap/api/a/b;->P:Lcom/amap/api/maps2d/model/Marker;

    if-eqz v0, :cond_1

    .line 1023
    iget-object v0, p0, Lcom/amap/api/a/b;->M:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/az$a;

    .line 1025
    if-eqz v0, :cond_0

    .line 1026
    iget-object v1, p0, Lcom/amap/api/a/b;->P:Lcom/amap/api/maps2d/model/Marker;

    invoke-virtual {v1}, Lcom/amap/api/maps2d/model/Marker;->getPosition()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    iput-object v1, v0, Lcom/amap/api/a/az$a;->b:Lcom/amap/api/maps2d/model/LatLng;

    .line 1028
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    invoke-virtual {v0}, Lcom/amap/api/a/az;->a()V

    .line 1030
    :cond_1
    return-void
.end method

.method public v()V
    .locals 2

    .prologue
    .line 1080
    :try_start_0
    invoke-static {}, Lcom/amap/api/a/u;->a()Lcom/amap/api/a/u;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/a/u;->b(Lcom/amap/api/a/u$a;)V

    .line 1081
    invoke-static {}, Lcom/amap/api/a/bp;->a()Lcom/amap/api/a/bp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/a/bp;->b(Lcom/amap/api/a/bp$a;)V

    .line 1082
    invoke-static {}, Lcom/amap/api/a/s;->a()Lcom/amap/api/a/s;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/a/s;->b(Lcom/amap/api/a/s$a;)V

    .line 1083
    iget-object v0, p0, Lcom/amap/api/a/b;->e:Lcom/amap/api/a/cg;

    invoke-virtual {v0}, Lcom/amap/api/a/cg;->a()V

    .line 1084
    iget-object v0, p0, Lcom/amap/api/a/b;->z:Lcom/amap/api/a/bo;

    invoke-virtual {v0}, Lcom/amap/api/a/bo;->a()V

    .line 1085
    iget-object v0, p0, Lcom/amap/api/a/b;->y:Lcom/amap/api/a/cf;

    invoke-virtual {v0}, Lcom/amap/api/a/cf;->a()V

    .line 1086
    iget-object v0, p0, Lcom/amap/api/a/b;->s:Lcom/amap/api/a/au;

    invoke-virtual {v0}, Lcom/amap/api/a/au;->a()V

    .line 1087
    iget-object v0, p0, Lcom/amap/api/a/b;->B:Lcom/amap/api/a/w;

    invoke-virtual {v0}, Lcom/amap/api/a/w;->a()V

    .line 1088
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    invoke-virtual {v0}, Lcom/amap/api/a/ab;->b()V

    .line 1089
    iget-object v0, p0, Lcom/amap/api/a/b;->i:Lcom/amap/api/a/ay;

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->e()V

    .line 1090
    sget-object v0, Lcom/amap/api/a/k;->d:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 1091
    sget-object v0, Lcom/amap/api/a/k;->d:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 1093
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1094
    iget-object v0, p0, Lcom/amap/api/a/b;->R:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1096
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/b;->f:Lcom/amap/api/a/az;

    invoke-virtual {v0}, Lcom/amap/api/a/az;->removeAllViews()V

    .line 1097
    invoke-virtual {p0}, Lcom/amap/api/a/b;->t()V

    .line 1098
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-eqz v0, :cond_2

    .line 1099
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$b;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1104
    :cond_2
    :goto_0
    return-void

    .line 1101
    :catch_0
    move-exception v0

    .line 1102
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public w()F
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1140
    invoke-virtual {p0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v0

    .line 1141
    new-instance v1, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v1}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 1142
    new-instance v2, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v2}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 1143
    invoke-virtual {p0, v3, v3, v1}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 1144
    invoke-virtual {p0, v0, v3, v2}, Lcom/amap/api/a/b;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 1145
    new-instance v3, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v1, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v6, v1, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    new-instance v1, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v4, v2, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v6, v2, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v1, v4, v5, v6, v7}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-static {v3, v1}, Lcom/amap/api/a/a/p;->a(Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;)D

    move-result-wide v1

    .line 1148
    int-to-double v3, v0

    div-double v0, v1, v3

    double-to-float v0, v0

    return v0
.end method

.method public x()Lcom/amap/api/maps2d/model/LatLngBounds;
    .locals 1

    .prologue
    .line 1153
    const/4 v0, 0x0

    return-object v0
.end method

.method public y()V
    .locals 1

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$b;->b()V

    .line 1161
    :cond_0
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    if-eqz v0, :cond_0

    .line 1166
    iget-object v0, p0, Lcom/amap/api/a/b;->m:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$b;->c()V

    .line 1168
    :cond_0
    return-void
.end method
