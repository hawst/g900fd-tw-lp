.class public Lcom/amap/api/a/bf$c;
.super Ljava/lang/Object;
.source "Mediator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/bf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/bf;

.field private final b:Landroid/content/Context;

.field private c:Ljava/net/Proxy;

.field private d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/amap/api/a/bc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf;Lcom/amap/api/a/bf;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 144
    iput-object p1, p0, Lcom/amap/api/a/bf$c;->a:Lcom/amap/api/a/bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bf$c;->d:Landroid/util/SparseArray;

    .line 145
    iput-object p3, p0, Lcom/amap/api/a/bf$c;->b:Landroid/content/Context;

    .line 146
    iget-object v0, p0, Lcom/amap/api/a/bf$c;->d:Landroid/util/SparseArray;

    const/4 v1, 0x0

    new-instance v2, Lcom/amap/api/a/bw;

    invoke-direct {v2, p2, p3}, Lcom/amap/api/a/bw;-><init>(Lcom/amap/api/a/bf;Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 147
    return-void
.end method

.method static synthetic a(Lcom/amap/api/a/bf$c;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/amap/api/a/bf$c;->d:Landroid/util/SparseArray;

    return-object v0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/amap/api/a/bf$c;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/amap/api/a/y;->a(Landroid/content/Context;)Ljava/net/Proxy;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bf$c;->c:Ljava/net/Proxy;

    .line 192
    return-void
.end method

.method static synthetic b(Lcom/amap/api/a/bf$c;)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/amap/api/a/bf$c;->b()V

    return-void
.end method


# virtual methods
.method public a()Ljava/net/Proxy;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/amap/api/a/bf$c;->c:Ljava/net/Proxy;

    return-object v0
.end method
