.class Lcom/amap/api/a/bm;
.super Ljava/lang/Object;
.source "ProjectionDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/an;


# instance fields
.field private a:Lcom/amap/api/a/af;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/af;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/amap/api/a/bm;->a:Lcom/amap/api/a/af;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Lcom/amap/api/maps2d/model/LatLng;)Landroid/graphics/Point;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 31
    new-instance v5, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    .line 32
    iget-object v0, p0, Lcom/amap/api/a/bm;->a:Lcom/amap/api/a/af;

    iget-wide v1, p1, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, p1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-interface/range {v0 .. v5}, Lcom/amap/api/a/af;->b(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 34
    new-instance v0, Landroid/graphics/Point;

    iget v1, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v2, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public a(Landroid/graphics/Point;)Lcom/amap/api/maps2d/model/LatLng;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, Lcom/autonavi/amap/mapcore2d/DPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/DPoint;-><init>()V

    .line 25
    iget-object v1, p0, Lcom/amap/api/a/bm;->a:Lcom/amap/api/a/af;

    iget v2, p1, Landroid/graphics/Point;->x:I

    iget v3, p1, Landroid/graphics/Point;->y:I

    invoke-interface {v1, v2, v3, v0}, Lcom/amap/api/a/af;->a(IILcom/autonavi/amap/mapcore2d/DPoint;)V

    .line 26
    new-instance v1, Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->y:D

    iget-wide v4, v0, Lcom/autonavi/amap/mapcore2d/DPoint;->x:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    return-object v1
.end method

.method public a()Lcom/amap/api/maps2d/model/VisibleRegion;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 39
    iget-object v0, p0, Lcom/amap/api/a/bm;->a:Lcom/amap/api/a/af;

    invoke-interface {v0}, Lcom/amap/api/a/af;->c()I

    move-result v0

    .line 40
    iget-object v1, p0, Lcom/amap/api/a/bm;->a:Lcom/amap/api/a/af;

    invoke-interface {v1}, Lcom/amap/api/a/af;->d()I

    move-result v2

    .line 41
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v5, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/amap/api/a/bm;->a(Landroid/graphics/Point;)Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v3

    .line 42
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v0, v5}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/amap/api/a/bm;->a(Landroid/graphics/Point;)Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v4

    .line 43
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v5, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/amap/api/a/bm;->a(Landroid/graphics/Point;)Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v1

    .line 44
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v5}, Lcom/amap/api/a/bm;->a(Landroid/graphics/Point;)Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v2

    .line 45
    invoke-static {}, Lcom/amap/api/maps2d/model/LatLngBounds;->builder()Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->include(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/LatLngBounds$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/LatLngBounds$Builder;->build()Lcom/amap/api/maps2d/model/LatLngBounds;

    move-result-object v5

    .line 47
    new-instance v0, Lcom/amap/api/maps2d/model/VisibleRegion;

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/maps2d/model/VisibleRegion;-><init>(Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLngBounds;)V

    return-object v0
.end method

.method public b(Lcom/amap/api/maps2d/model/LatLng;)Landroid/graphics/PointF;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 52
    new-instance v5, Lcom/autonavi/amap/mapcore2d/FPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/FPoint;-><init>()V

    .line 53
    iget-object v0, p0, Lcom/amap/api/a/bm;->a:Lcom/amap/api/a/af;

    iget-wide v1, p1, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, p1, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-interface/range {v0 .. v5}, Lcom/amap/api/a/af;->a(DDLcom/autonavi/amap/mapcore2d/FPoint;)V

    .line 55
    new-instance v0, Landroid/graphics/PointF;

    iget v1, v5, Lcom/autonavi/amap/mapcore2d/FPoint;->x:F

    iget v2, v5, Lcom/autonavi/amap/mapcore2d/FPoint;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method
