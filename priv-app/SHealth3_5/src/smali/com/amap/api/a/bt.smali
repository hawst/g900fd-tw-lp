.class Lcom/amap/api/a/bt;
.super Lcom/amap/api/a/br;
.source "TaskPool.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amap/api/a/br",
        "<",
        "Lcom/amap/api/a/m$a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/amap/api/a/br;-><init>()V

    return-void
.end method


# virtual methods
.method protected declared-synchronized b(IZ)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 121
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/bt;->a:Ljava/util/LinkedList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    move-object v0, v4

    .line 169
    :goto_0
    monitor-exit p0

    return-object v0

    .line 124
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/amap/api/a/bt;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    .line 125
    if-le p1, v3, :cond_1

    move p1, v3

    .line 129
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, p1}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    .line 134
    :goto_1
    if-ge v1, v3, :cond_5

    .line 135
    iget-object v0, p0, Lcom/amap/api/a/bt;->a:Ljava/util/LinkedList;

    if-nez v0, :cond_2

    move-object v0, v4

    .line 136
    goto :goto_0

    .line 138
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/bt;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    .line 139
    if-nez v0, :cond_4

    move v0, v1

    move v1, v2

    move v2, v3

    .line 134
    :cond_3
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 142
    :cond_4
    iget v6, v0, Lcom/amap/api/a/m$a;->a:I

    .line 143
    if-eqz p2, :cond_6

    .line 144
    if-nez v6, :cond_7

    .line 145
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v0, p0, Lcom/amap/api/a/bt;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 147
    add-int/lit8 v3, v3, -0x1

    .line 148
    add-int/lit8 v1, v1, -0x1

    .line 149
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    move v2, v3

    .line 160
    :goto_2
    if-lt v1, p1, :cond_3

    .line 168
    :cond_5
    invoke-virtual {p0}, Lcom/amap/api/a/bt;->b()V

    move-object v0, v5

    .line 169
    goto :goto_0

    .line 152
    :cond_6
    if-gez v6, :cond_7

    .line 153
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/amap/api/a/bt;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    add-int/lit8 v3, v3, -0x1

    .line 156
    add-int/lit8 v1, v1, -0x1

    .line 157
    add-int/lit8 v2, v2, 0x1

    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_2

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_2
.end method
