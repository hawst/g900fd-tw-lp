.class public Lcom/amap/api/a/as;
.super Lcom/amap/api/a/at;
.source "LayerPropertys.java"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:J

.field public j:Lcom/amap/api/a/cd;

.field public k:I

.field public l:Ljava/lang/String;

.field m:Lcom/amap/api/a/p;

.field n:Lcom/amap/api/a/q;

.field o:Lcom/amap/api/a/bq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/amap/api/a/bq",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Lcom/amap/api/a/at;-><init>()V

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    .line 31
    const/16 v0, 0x12

    iput v0, p0, Lcom/amap/api/a/as;->b:I

    .line 36
    const/4 v0, 0x3

    iput v0, p0, Lcom/amap/api/a/as;->c:I

    .line 41
    iput-boolean v3, p0, Lcom/amap/api/a/as;->d:Z

    .line 46
    iput-boolean v3, p0, Lcom/amap/api/a/as;->e:Z

    .line 51
    iput-boolean v1, p0, Lcom/amap/api/a/as;->f:Z

    .line 56
    iput-boolean v1, p0, Lcom/amap/api/a/as;->g:Z

    .line 61
    iput-boolean v1, p0, Lcom/amap/api/a/as;->h:Z

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/a/as;->i:J

    .line 71
    iput-object v2, p0, Lcom/amap/api/a/as;->j:Lcom/amap/api/a/cd;

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/amap/api/a/as;->k:I

    .line 81
    const-string v0, ""

    iput-object v0, p0, Lcom/amap/api/a/as;->l:Ljava/lang/String;

    .line 86
    iput-object v2, p0, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    .line 91
    iput-object v2, p0, Lcom/amap/api/a/as;->n:Lcom/amap/api/a/q;

    .line 96
    iput-object v2, p0, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    return-void
.end method


# virtual methods
.method protected a(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 125
    iget-object v0, p0, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    if-nez v0, :cond_1

    .line 148
    :cond_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    .line 131
    iget v1, v0, Lcom/amap/api/a/m$a;->g:I

    if-gez v1, :cond_3

    .line 132
    iget-boolean v1, p0, Lcom/amap/api/a/as;->e:Z

    if-eqz v1, :cond_2

    .line 135
    invoke-static {}, Lcom/amap/api/a/m;->c()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 141
    :goto_1
    iget-object v0, v0, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 142
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 143
    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v3, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 138
    :cond_3
    iget-object v1, p0, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    iget v3, v0, Lcom/amap/api/a/m$a;->g:I

    invoke-virtual {v1, v3}, Lcom/amap/api/a/p;->a(I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 100
    if-ne p0, p1, :cond_0

    .line 101
    const/4 v0, 0x1

    .line 108
    :goto_0
    return v0

    .line 103
    :cond_0
    instance-of v0, p1, Lcom/amap/api/a/as;

    if-nez v0, :cond_1

    .line 104
    const/4 v0, 0x0

    goto :goto_0

    .line 106
    :cond_1
    check-cast p1, Lcom/amap/api/a/as;

    .line 108
    iget-object v0, p0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/amap/api/a/as;->k:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/amap/api/a/as;->a:Ljava/lang/String;

    return-object v0
.end method
