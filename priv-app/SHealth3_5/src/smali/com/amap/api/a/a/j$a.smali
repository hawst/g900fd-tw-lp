.class public Lcom/amap/api/a/a/j$a;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/a/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/io/File;

.field public d:Landroid/graphics/Bitmap$CompressFormat;

.field public e:I

.field public f:Z

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    const/16 v0, 0x1400

    iput v0, p0, Lcom/amap/api/a/a/j$a;->a:I

    .line 491
    const/high16 v0, 0xa00000

    iput v0, p0, Lcom/amap/api/a/a/j$a;->b:I

    .line 493
    invoke-static {}, Lcom/amap/api/a/a/j;->f()Landroid/graphics/Bitmap$CompressFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/a/j$a;->d:Landroid/graphics/Bitmap$CompressFormat;

    .line 494
    const/16 v0, 0x64

    iput v0, p0, Lcom/amap/api/a/a/j$a;->e:I

    .line 495
    iput-boolean v1, p0, Lcom/amap/api/a/a/j$a;->f:Z

    .line 496
    iput-boolean v1, p0, Lcom/amap/api/a/a/j$a;->g:Z

    .line 497
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/a/j$a;->h:Z

    .line 512
    invoke-static {p1, p2}, Lcom/amap/api/a/a/j;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/a/j$a;->c:Ljava/io/File;

    .line 513
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 543
    iput p1, p0, Lcom/amap/api/a/a/j$a;->a:I

    .line 544
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 551
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/amap/api/a/a/j$a;->c:Ljava/io/File;

    .line 552
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 555
    iput-boolean p1, p0, Lcom/amap/api/a/a/j$a;->f:Z

    .line 556
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 547
    iput p1, p0, Lcom/amap/api/a/a/j$a;->b:I

    .line 548
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 559
    iput-boolean p1, p0, Lcom/amap/api/a/a/j$a;->g:Z

    .line 560
    return-void
.end method
