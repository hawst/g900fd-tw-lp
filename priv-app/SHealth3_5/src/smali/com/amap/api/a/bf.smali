.class Lcom/amap/api/a/bf;
.super Ljava/lang/Object;
.source "Mediator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/bf$d;,
        Lcom/amap/api/a/bf$a;,
        Lcom/amap/api/a/bf$e;,
        Lcom/amap/api/a/bf$b;,
        Lcom/amap/api/a/bf$c;
    }
.end annotation


# instance fields
.field public a:Lcom/amap/api/a/bf$e;

.field public b:Lcom/amap/api/a/bf$d;

.field public c:Lcom/amap/api/a/bf$b;

.field public d:Lcom/amap/api/a/bf$a;

.field public e:Lcom/amap/api/a/bf$c;

.field public f:Lcom/amap/api/a/ab;

.field public g:Lcom/amap/api/a/b;

.field public h:Lcom/amap/api/a/ba;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    .line 47
    iput-object p2, p0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    .line 48
    new-instance v0, Lcom/amap/api/a/bf$d;

    invoke-direct {v0, p0, p2}, Lcom/amap/api/a/bf$d;-><init>(Lcom/amap/api/a/bf;Lcom/amap/api/a/b;)V

    iput-object v0, p0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    .line 49
    new-instance v0, Lcom/amap/api/a/ba;

    iget-object v1, p0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-direct {v0, v1}, Lcom/amap/api/a/ba;-><init>(Lcom/amap/api/a/bf$d;)V

    iput-object v0, p0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    .line 51
    iget-object v0, p0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    invoke-virtual {v0}, Lcom/amap/api/a/ba;->a()V

    .line 52
    invoke-virtual {p0, p1}, Lcom/amap/api/a/bf;->a(Landroid/content/Context;)V

    .line 53
    new-instance v0, Lcom/amap/api/a/bf$c;

    invoke-direct {v0, p0, p0, p1}, Lcom/amap/api/a/bf$c;-><init>(Lcom/amap/api/a/bf;Lcom/amap/api/a/bf;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    .line 54
    new-instance v0, Lcom/amap/api/a/bf$a;

    invoke-direct {v0, p0, p1}, Lcom/amap/api/a/bf$a;-><init>(Lcom/amap/api/a/bf;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    .line 55
    new-instance v0, Lcom/amap/api/a/bf$e;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bf$e;-><init>(Lcom/amap/api/a/bf;)V

    iput-object v0, p0, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    .line 56
    new-instance v0, Lcom/amap/api/a/bf$b;

    invoke-direct {v0, p0}, Lcom/amap/api/a/bf$b;-><init>(Lcom/amap/api/a/bf;)V

    iput-object v0, p0, Lcom/amap/api/a/bf;->c:Lcom/amap/api/a/bf$b;

    .line 57
    new-instance v0, Lcom/amap/api/a/ab;

    invoke-direct {v0}, Lcom/amap/api/a/ab;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bf;->f:Lcom/amap/api/a/ab;

    .line 58
    iget-object v0, p0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v2, v2}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    .line 60
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/16 v1, 0xa0

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const-wide/32 v5, 0x25800

    .line 69
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 71
    const/4 v0, 0x0

    .line 73
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "densityDpi"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 81
    :goto_0
    if-eqz v0, :cond_5

    .line 84
    iget v3, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v3, v4

    int-to-long v3, v3

    .line 86
    :try_start_1
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 92
    :goto_1
    const/16 v2, 0x78

    if-gt v0, v2, :cond_0

    .line 93
    sput v7, Lcom/amap/api/a/x;->f:I

    .line 117
    :goto_2
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v0, v1

    .line 91
    goto :goto_1

    .line 89
    :catch_1
    move-exception v0

    .line 90
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    move v0, v1

    goto :goto_1

    .line 94
    :cond_0
    if-gt v0, v1, :cond_1

    .line 95
    sput v9, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 96
    :cond_1
    const/16 v1, 0xf0

    if-gt v0, v1, :cond_2

    .line 97
    sput v8, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 99
    :cond_2
    cmp-long v0, v3, v5

    if-lez v0, :cond_3

    .line 100
    sput v8, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 101
    :cond_3
    cmp-long v0, v3, v5

    if-gez v0, :cond_4

    .line 102
    sput v7, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 104
    :cond_4
    sput v9, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 108
    :cond_5
    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v0, v1

    int-to-long v0, v0

    .line 109
    cmp-long v2, v0, v5

    if-lez v2, :cond_6

    .line 110
    sput v8, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 111
    :cond_6
    cmp-long v0, v0, v5

    if-gez v0, :cond_7

    .line 112
    sput v7, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 114
    :cond_7
    sput v9, Lcom/amap/api/a/x;->f:I

    goto :goto_2

    .line 76
    :catch_2
    move-exception v3

    goto :goto_0

    .line 74
    :catch_3
    move-exception v3

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bf$a;->b(Z)V

    .line 130
    return-void
.end method
