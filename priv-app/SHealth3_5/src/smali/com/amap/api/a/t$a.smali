.class final enum Lcom/amap/api/a/t$a;
.super Ljava/lang/Enum;
.source "CameraUpdateFactoryDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/t;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/amap/api/a/t$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/amap/api/a/t$a;

.field public static final enum b:Lcom/amap/api/a/t$a;

.field public static final enum c:Lcom/amap/api/a/t$a;

.field public static final enum d:Lcom/amap/api/a/t$a;

.field public static final enum e:Lcom/amap/api/a/t$a;

.field public static final enum f:Lcom/amap/api/a/t$a;

.field public static final enum g:Lcom/amap/api/a/t$a;

.field public static final enum h:Lcom/amap/api/a/t$a;

.field public static final enum i:Lcom/amap/api/a/t$a;

.field public static final enum j:Lcom/amap/api/a/t$a;

.field public static final enum k:Lcom/amap/api/a/t$a;

.field public static final enum l:Lcom/amap/api/a/t$a;

.field private static final synthetic m:[Lcom/amap/api/a/t$a;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "none"

    invoke-direct {v0, v1, v3}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->a:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "zoomIn"

    invoke-direct {v0, v1, v4}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->b:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string v1, "changeCenter"

    invoke-direct {v0, v1, v5}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->c:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string v1, "changeGeoCenterZoom"

    invoke-direct {v0, v1, v6}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->d:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "zoomOut"

    invoke-direct {v0, v1, v7}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->e:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "zoomTo"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->f:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "zoomBy"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->g:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "scrollBy"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->h:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "newCameraPosition"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->i:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "newLatLngBounds"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->j:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string/jumbo v1, "newLatLngBoundsWithSize"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->k:Lcom/amap/api/a/t$a;

    new-instance v0, Lcom/amap/api/a/t$a;

    const-string v1, "changeGeoCenterZoomTiltBearing"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/amap/api/a/t$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/amap/api/a/t$a;->l:Lcom/amap/api/a/t$a;

    .line 15
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/amap/api/a/t$a;

    sget-object v1, Lcom/amap/api/a/t$a;->a:Lcom/amap/api/a/t$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/amap/api/a/t$a;->b:Lcom/amap/api/a/t$a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/amap/api/a/t$a;->c:Lcom/amap/api/a/t$a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/amap/api/a/t$a;->d:Lcom/amap/api/a/t$a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/amap/api/a/t$a;->e:Lcom/amap/api/a/t$a;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/amap/api/a/t$a;->f:Lcom/amap/api/a/t$a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/amap/api/a/t$a;->g:Lcom/amap/api/a/t$a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/amap/api/a/t$a;->h:Lcom/amap/api/a/t$a;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/amap/api/a/t$a;->i:Lcom/amap/api/a/t$a;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/amap/api/a/t$a;->j:Lcom/amap/api/a/t$a;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/amap/api/a/t$a;->k:Lcom/amap/api/a/t$a;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/amap/api/a/t$a;->l:Lcom/amap/api/a/t$a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/amap/api/a/t$a;->m:[Lcom/amap/api/a/t$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/amap/api/a/t$a;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/amap/api/a/t$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/t$a;

    return-object v0
.end method

.method public static values()[Lcom/amap/api/a/t$a;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/amap/api/a/t$a;->m:[Lcom/amap/api/a/t$a;

    invoke-virtual {v0}, [Lcom/amap/api/a/t$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/amap/api/a/t$a;

    return-object v0
.end method
