.class Lcom/amap/api/a/i;
.super Ljava/lang/Object;
.source "AsyncServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/h;


# direct methods
.method constructor <init>(Lcom/amap/api/a/h;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 109
    :try_start_0
    iget-object v2, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v2, v2, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    if-eqz v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v2, v2, Lcom/amap/api/a/h;->b:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object v2, v0

    .line 114
    :goto_0
    iget-object v1, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-boolean v1, v1, Lcom/amap/api/a/h;->a:Z

    if-eqz v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v1, v1, Lcom/amap/api/a/h;->e:Lcom/amap/api/a/bf;

    if-nez v1, :cond_2

    .line 116
    iget-object v1, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    const/4 v3, 0x0

    iput-boolean v3, v1, Lcom/amap/api/a/h;->a:Z

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 191
    :cond_1
    return-void

    .line 118
    :cond_2
    invoke-static {}, Lcom/amap/api/maps2d/MapsInitializer;->getNetworkEnable()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_3

    .line 120
    const-wide/16 v3, 0xc8

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 121
    :catch_1
    move-exception v1

    .line 122
    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 126
    :cond_3
    iget-object v1, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v1, v1, Lcom/amap/api/a/h;->c:Lcom/amap/api/a/br;

    if-eqz v1, :cond_9

    .line 127
    iget-object v0, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v0, v0, Lcom/amap/api/a/h;->c:Lcom/amap/api/a/br;

    iget-object v1, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    invoke-virtual {v1}, Lcom/amap/api/a/h;->f()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/amap/api/a/br;->a(IZ)Ljava/util/ArrayList;

    move-result-object v0

    move-object v1, v0

    .line 130
    :goto_1
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    move-object v0, v1

    .line 131
    goto :goto_0

    .line 133
    :cond_4
    iget-object v0, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-boolean v0, v0, Lcom/amap/api/a/h;->a:Z

    if-eqz v0, :cond_1

    .line 142
    if-eqz v1, :cond_8

    .line 145
    iget-object v0, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-boolean v0, v0, Lcom/amap/api/a/h;->a:Z

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v0, v0, Lcom/amap/api/a/h;->e:Lcom/amap/api/a/bf;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v0, v0, Lcom/amap/api/a/h;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v0, :cond_5

    move-object v0, v1

    .line 149
    goto :goto_0

    .line 153
    :cond_5
    :try_start_3
    iget-object v0, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v3, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v3, v3, Lcom/amap/api/a/h;->e:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->e:Lcom/amap/api/a/bf$c;

    invoke-virtual {v3}, Lcom/amap/api/a/bf$c;->a()Ljava/net/Proxy;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/amap/api/a/h;->a(Ljava/util/ArrayList;Ljava/net/Proxy;)Ljava/util/ArrayList;
    :try_end_3
    .catch Lcom/amap/api/maps2d/AMapException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    .line 159
    :goto_2
    if-eqz v0, :cond_6

    :try_start_4
    iget-object v2, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v2, v2, Lcom/amap/api/a/h;->c:Lcom/amap/api/a/br;

    if-eqz v2, :cond_6

    .line 160
    iget-object v2, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-object v2, v2, Lcom/amap/api/a/h;->c:Lcom/amap/api/a/br;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/amap/api/a/br;->a(Ljava/util/List;Z)V

    .line 164
    :cond_6
    :goto_3
    iget-object v2, p0, Lcom/amap/api/a/i;->a:Lcom/amap/api/a/h;

    iget-boolean v2, v2, Lcom/amap/api/a/h;->a:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    .line 166
    const-wide/16 v2, 0x32

    :try_start_5
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    move-object v2, v0

    move-object v0, v1

    .line 169
    goto/16 :goto_0

    .line 155
    :catch_2
    move-exception v0

    .line 156
    :try_start_6
    invoke-virtual {v0}, Lcom/amap/api/maps2d/AMapException;->printStackTrace()V

    move-object v0, v2

    goto :goto_2

    .line 167
    :catch_3
    move-exception v2

    .line 168
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    move-object v2, v0

    move-object v0, v1

    .line 169
    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    :cond_8
    move-object v0, v2

    goto :goto_3

    :cond_9
    move-object v1, v0

    goto :goto_1

    :cond_a
    move-object v2, v0

    move-object v0, v1

    goto/16 :goto_0
.end method
