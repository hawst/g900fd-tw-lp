.class Lcom/amap/api/a/g;
.super Ljava/lang/Object;
.source "AnimBase.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/f;


# direct methods
.method constructor <init>(Lcom/amap/api/a/f;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 63
    iget-object v0, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    invoke-virtual {v0}, Lcom/amap/api/a/f;->e()V

    .line 64
    iget-object v0, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    invoke-virtual {v0}, Lcom/amap/api/a/f;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    invoke-static {v0}, Lcom/amap/api/a/f;->a(Lcom/amap/api/a/f;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 66
    iget-object v0, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/a/f;->a(Lcom/amap/api/a/f;Landroid/os/Handler;)Landroid/os/Handler;

    .line 67
    iget-object v0, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    invoke-virtual {v0}, Lcom/amap/api/a/f;->b()V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 70
    iget-object v2, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    invoke-virtual {v2}, Lcom/amap/api/a/f;->a()V

    .line 71
    iget-object v2, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    invoke-virtual {v2}, Lcom/amap/api/a/f;->g()V

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 73
    sub-long v4, v2, v0

    iget-object v6, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    iget v6, v6, Lcom/amap/api/a/f;->d:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_0

    .line 75
    :try_start_0
    iget-object v4, p0, Lcom/amap/api/a/g;->a:Lcom/amap/api/a/f;

    iget v4, v4, Lcom/amap/api/a/f;->d:I

    int-to-long v4, v4

    sub-long v0, v2, v0

    sub-long v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
