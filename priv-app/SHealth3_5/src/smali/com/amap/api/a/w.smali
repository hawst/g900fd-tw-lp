.class Lcom/amap/api/a/w;
.super Landroid/widget/LinearLayout;
.source "CompassView.java"


# instance fields
.field a:Landroid/graphics/Bitmap;

.field b:Landroid/graphics/Bitmap;

.field c:Landroid/widget/ImageView;

.field d:Lcom/amap/api/a/ax;

.field e:Lcom/amap/api/a/af;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/amap/api/a/ax;Lcom/amap/api/a/af;)V
    .locals 6

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    iput-object p2, p0, Lcom/amap/api/a/w;->d:Lcom/amap/api/a/ax;

    .line 34
    iput-object p3, p0, Lcom/amap/api/a/w;->e:Lcom/amap/api/a/af;

    .line 36
    :try_start_0
    const-string/jumbo v0, "maps_dav_compass_needle_large.png"

    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 37
    sget v1, Lcom/amap/api/a/x;->a:F

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/a/w;->b:Landroid/graphics/Bitmap;

    .line 38
    sget v1, Lcom/amap/api/a/x;->a:F

    const v2, 0x3f333333    # 0.7f

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/amap/api/a/w;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/w;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/amap/api/a/w;->a:Landroid/graphics/Bitmap;

    .line 41
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/amap/api/a/w;->a:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 42
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 43
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 44
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 45
    iget-object v3, p0, Lcom/amap/api/a/w;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget-object v4, p0, Lcom/amap/api/a/w;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v1, v0, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/amap/api/a/w;->c:Landroid/widget/ImageView;

    .line 52
    iget-object v0, p0, Lcom/amap/api/a/w;->c:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 53
    iget-object v0, p0, Lcom/amap/api/a/w;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/amap/api/a/w;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 55
    iget-object v0, p0, Lcom/amap/api/a/w;->c:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/a/w$1;

    invoke-direct {v1, p0}, Lcom/amap/api/a/w$1;-><init>(Lcom/amap/api/a/w;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    iget-object v0, p0, Lcom/amap/api/a/w;->c:Landroid/widget/ImageView;

    new-instance v1, Lcom/amap/api/a/w$2;

    invoke-direct {v1, p0}, Lcom/amap/api/a/w$2;-><init>(Lcom/amap/api/a/w;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 88
    iget-object v0, p0, Lcom/amap/api/a/w;->c:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/amap/api/a/w;->addView(Landroid/view/View;)V

    .line 89
    return-void

    .line 48
    :catch_0
    move-exception v0

    .line 49
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 23
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/w;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 24
    iget-object v0, p0, Lcom/amap/api/a/w;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/w;->a:Landroid/graphics/Bitmap;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/w;->b:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :goto_0
    return-void

    .line 27
    :catch_0
    move-exception v0

    .line 28
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
