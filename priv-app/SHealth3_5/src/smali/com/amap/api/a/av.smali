.class public final Lcom/amap/api/a/av;
.super Ljava/lang/Object;
.source "MapController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/av$b;,
        Lcom/amap/api/a/av$a;
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Lcom/amap/api/a/bf;

.field private d:Z

.field private e:Lcom/amap/api/a/av$b;

.field private f:Lcom/amap/api/a/av$a;


# direct methods
.method constructor <init>(Lcom/amap/api/a/bf;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v0, p0, Lcom/amap/api/a/av;->a:I

    .line 30
    iput v0, p0, Lcom/amap/api/a/av;->b:I

    .line 33
    iput-object p1, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    .line 34
    iput-boolean v0, p0, Lcom/amap/api/a/av;->d:Z

    .line 35
    new-instance v0, Lcom/amap/api/a/av$b;

    invoke-direct {v0, p0}, Lcom/amap/api/a/av$b;-><init>(Lcom/amap/api/a/av;)V

    iput-object v0, p0, Lcom/amap/api/a/av;->e:Lcom/amap/api/a/av$b;

    .line 36
    new-instance v0, Lcom/amap/api/a/av$a;

    invoke-direct {v0, p0}, Lcom/amap/api/a/av$a;-><init>(Lcom/amap/api/a/av;)V

    iput-object v0, p0, Lcom/amap/api/a/av;->f:Lcom/amap/api/a/av$a;

    .line 37
    return-void
.end method

.method private a(F)I
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 246
    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    .line 250
    :goto_0
    int-to-float v3, v1

    cmpl-float v3, v3, p1

    if-lez v3, :cond_0

    .line 251
    return v2

    .line 254
    :cond_0
    mul-int/lit8 v2, v1, 0x2

    .line 255
    add-int/lit8 v1, v0, 0x1

    move v4, v1

    move v1, v2

    move v2, v0

    move v0, v4

    goto :goto_0
.end method

.method static synthetic a(Lcom/amap/api/a/av;)Lcom/amap/api/a/bf;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    return-object v0
.end method

.method private a(IIZZ)Z
    .locals 6

    .prologue
    .line 382
    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/av;->a(IIZZI)Z

    move-result v0

    return v0
.end method

.method private a(IIZZI)Z
    .locals 6

    .prologue
    .line 359
    const/4 v1, 0x0

    .line 360
    if-eqz p3, :cond_2

    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v0

    add-int/2addr v0, p5

    .line 364
    :goto_0
    iget-object v2, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v2}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/amap/api/a/b;->a(I)I

    move-result v3

    .line 366
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v0

    if-eq v3, v0, :cond_0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    .line 367
    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/av;->a(IIIZZ)V

    .line 368
    const/4 v0, 0x1

    move v1, v0

    .line 371
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->q()Lcom/amap/api/a/aq;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->K()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    :cond_1
    :goto_1
    return v1

    .line 360
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->e()I

    move-result v0

    sub-int/2addr v0, p5

    goto :goto_0

    .line 374
    :catch_0
    move-exception v0

    .line 375
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/amap/api/a/av;->a:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/amap/api/a/av;->a:I

    .line 53
    return-void
.end method

.method public a(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    .line 123
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->b()I

    move-result v0

    .line 127
    iget-object v1, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$d;->a()I

    move-result v1

    .line 128
    iget-object v2, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v2}, Lcom/amap/api/a/bf$d;->e()I

    move-result v2

    .line 129
    iget-object v3, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    invoke-virtual {v3}, Lcom/amap/api/a/bf$e;->b()I

    move-result v3

    .line 130
    iget-object v4, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v4, v4, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    invoke-virtual {v4}, Lcom/amap/api/a/bf$e;->a()I

    move-result v4

    .line 132
    if-nez v3, :cond_2

    if-nez v4, :cond_2

    .line 133
    iput p1, p0, Lcom/amap/api/a/av;->a:I

    .line 134
    iput p2, p0, Lcom/amap/api/a/av;->b:I

    goto :goto_0

    .line 139
    :cond_2
    int-to-float v5, p1

    int-to-float v4, v4

    div-float v4, v5, v4

    .line 141
    int-to-float v5, p2

    int-to-float v3, v3

    div-float v3, v5, v3

    .line 143
    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 144
    cmpl-float v4, v3, v8

    if-lez v4, :cond_4

    .line 145
    invoke-direct {p0, v3}, Lcom/amap/api/a/av;->a(F)I

    move-result v1

    sub-int v1, v2, v1

    .line 146
    if-gt v1, v0, :cond_6

    .line 155
    :cond_3
    :goto_1
    invoke-virtual {p0, v0}, Lcom/amap/api/a/av;->c(I)I

    goto :goto_0

    .line 149
    :cond_4
    float-to-double v4, v3

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpg-double v0, v4, v6

    if-gez v0, :cond_5

    .line 150
    div-float v0, v8, v3

    invoke-direct {p0, v0}, Lcom/amap/api/a/av;->a(F)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    .line 151
    if-lt v0, v1, :cond_3

    move v0, v1

    .line 152
    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public a(IIIZZ)V
    .locals 6

    .prologue
    .line 349
    iget-object v0, p0, Lcom/amap/api/a/av;->e:Lcom/amap/api/a/av$b;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/av$b;->a(IIIZZ)V

    .line 355
    return-void
.end method

.method public a(Lcom/amap/api/a/ac;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/ac;)V

    .line 87
    return-void
.end method

.method public a(Lcom/amap/api/a/ac;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 293
    iget-object v0, p0, Lcom/amap/api/a/av;->f:Lcom/amap/api/a/av$a;

    invoke-virtual {v0, p1, v1, v1, p2}, Lcom/amap/api/a/av$a;->a(Lcom/amap/api/a/ac;Landroid/os/Message;Ljava/lang/Runnable;I)V

    .line 294
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/amap/api/a/av;->e:Lcom/amap/api/a/av$b;

    invoke-virtual {v0}, Lcom/amap/api/a/av$b;->a()V

    .line 306
    iget-object v0, p0, Lcom/amap/api/a/av;->f:Lcom/amap/api/a/av$a;

    invoke-virtual {v0}, Lcom/amap/api/a/av$a;->a()V

    .line 307
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/amap/api/a/av;->b:I

    return v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 56
    iput p1, p0, Lcom/amap/api/a/av;->b:I

    .line 57
    return-void
.end method

.method public b(II)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 282
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/amap/api/a/av;->a(IIZZ)Z

    move-result v0

    return v0
.end method

.method public c(I)I
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v0

    .line 94
    invoke-virtual {v0, p1}, Lcom/amap/api/a/b;->a(I)I

    move-result v1

    .line 95
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget v0, v0, Lcom/amap/api/a/ba;->g:I

    .line 96
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bf$d;->a(I)V

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->q()Lcom/amap/api/a/aq;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->K()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    return v1

    .line 116
    :catch_0
    move-exception v0

    .line 117
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public c(II)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 310
    iget-boolean v0, p0, Lcom/amap/api/a/av;->d:Z

    if-eqz v0, :cond_1

    .line 311
    iput-boolean v4, p0, Lcom/amap/api/a/av;->d:Z

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_0

    .line 328
    :cond_2
    sget-boolean v0, Lcom/amap/api/a/x;->k:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 329
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 330
    new-instance v1, Landroid/graphics/PointF;

    int-to-float v2, p1

    int-to-float v3, p2

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    .line 333
    iget-object v2, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v3, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v3}, Lcom/amap/api/a/bf$d;->e()I

    move-result v3

    invoke-virtual {v2, v0, v1, v3}, Lcom/amap/api/a/ba;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;I)V

    .line 338
    :cond_3
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, v4, v4}, Lcom/amap/api/a/bf$d;->a(ZZ)V

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/a/av;->d(I)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/amap/api/a/av;->e(I)Z

    move-result v0

    return v0
.end method

.method d(I)Z
    .locals 6

    .prologue
    .line 268
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->c()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->d()I

    move-result v0

    div-int/lit8 v2, v0, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object v0, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/av;->a(IIZZI)Z

    move-result v0

    return v0
.end method

.method e(I)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 273
    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->c()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    iget-object v0, p0, Lcom/amap/api/a/av;->c:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->d()I

    move-result v0

    div-int/lit8 v2, v0, 0x2

    move-object v0, p0

    move v4, v3

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/av;->a(IIZZI)Z

    move-result v0

    return v0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/16 v3, 0xa

    const/16 v2, -0xa

    const/4 v1, 0x0

    .line 60
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    :goto_0
    return v1

    .line 64
    :cond_0
    const/4 v0, 0x1

    .line 65
    packed-switch p2, :pswitch_data_0

    move v0, v1

    :goto_1
    move v1, v0

    .line 82
    goto :goto_0

    .line 67
    :pswitch_0
    invoke-virtual {p0, v2, v1}, Lcom/amap/api/a/av;->c(II)V

    goto :goto_1

    .line 70
    :pswitch_1
    invoke-virtual {p0, v3, v1}, Lcom/amap/api/a/av;->c(II)V

    goto :goto_1

    .line 73
    :pswitch_2
    invoke-virtual {p0, v1, v2}, Lcom/amap/api/a/av;->c(II)V

    goto :goto_1

    .line 76
    :pswitch_3
    invoke-virtual {p0, v1, v3}, Lcom/amap/api/a/av;->c(II)V

    goto :goto_1

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
