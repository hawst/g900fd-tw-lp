.class Lcom/amap/api/a/bh;
.super Ljava/lang/Object;
.source "MyLocationOverlay.java"


# instance fields
.field private a:Lcom/amap/api/a/af;

.field private b:Lcom/amap/api/a/aj;

.field private c:Lcom/amap/api/a/ag;

.field private d:Lcom/amap/api/maps2d/model/MyLocationStyle;

.field private e:Lcom/amap/api/maps2d/model/LatLng;

.field private f:D


# direct methods
.method constructor <init>(Lcom/amap/api/a/af;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/amap/api/a/bh;->a:Lcom/amap/api/a/af;

    .line 29
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    if-nez v0, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/amap/api/a/bh;->c()V

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    invoke-direct {p0}, Lcom/amap/api/a/bh;->d()V

    goto :goto_0
.end method

.method private c()V
    .locals 7

    .prologue
    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/bh;->a:Lcom/amap/api/a/af;

    new-instance v1, Lcom/amap/api/maps2d/model/CircleOptions;

    invoke-direct {v1}, Lcom/amap/api/maps2d/model/CircleOptions;-><init>()V

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->strokeWidth(F)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    const/16 v2, 0x14

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xb4

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->fillColor(I)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    const/16 v2, 0xff

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xdc

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->strokeColor(I)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->center(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/CircleOptions;)Lcom/amap/api/a/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    .line 110
    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    const-wide/high16 v1, 0x4069000000000000L    # 200.0

    invoke-interface {v0, v1, v2}, Lcom/amap/api/a/ag;->a(D)V

    .line 111
    iget-object v0, p0, Lcom/amap/api/a/bh;->a:Lcom/amap/api/a/af;

    new-instance v1, Lcom/amap/api/maps2d/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps2d/model/MarkerOptions;-><init>()V

    const/high16 v2, 0x3f000000    # 0.5f

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/maps2d/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/amap/api/a/ao$a;->c:Lcom/amap/api/a/ao$a;

    invoke-virtual {v3}, Lcom/amap/api/a/ao$a;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".png"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/amap/api/maps2d/model/BitmapDescriptorFactory;->fromAsset(Ljava/lang/String;)Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/MarkerOptions;->icon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/MarkerOptions;->position(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->b(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private d()V
    .locals 7

    .prologue
    .line 124
    iget-object v0, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    if-nez v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/bh;->a:Lcom/amap/api/a/af;

    new-instance v1, Lcom/amap/api/maps2d/model/CircleOptions;

    invoke-direct {v1}, Lcom/amap/api/maps2d/model/CircleOptions;-><init>()V

    iget-object v2, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->getStrokeWidth()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->strokeWidth(F)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->getRadiusFillColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->fillColor(I)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->getStrokeColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->strokeColor(I)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/CircleOptions;->center(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/CircleOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->a(Lcom/amap/api/maps2d/model/CircleOptions;)Lcom/amap/api/a/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    .line 133
    iget-object v0, p0, Lcom/amap/api/a/bh;->e:Lcom/amap/api/maps2d/model/LatLng;

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    iget-object v1, p0, Lcom/amap/api/a/bh;->e:Lcom/amap/api/maps2d/model/LatLng;

    invoke-interface {v0, v1}, Lcom/amap/api/a/ag;->a(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    iget-wide v1, p0, Lcom/amap/api/a/bh;->f:D

    invoke-interface {v0, v1, v2}, Lcom/amap/api/a/ag;->a(D)V

    .line 137
    iget-object v0, p0, Lcom/amap/api/a/bh;->a:Lcom/amap/api/a/af;

    new-instance v1, Lcom/amap/api/maps2d/model/MarkerOptions;

    invoke-direct {v1}, Lcom/amap/api/maps2d/model/MarkerOptions;-><init>()V

    iget-object v2, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->getAnchorU()F

    move-result v2

    iget-object v3, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    invoke-virtual {v3}, Lcom/amap/api/maps2d/model/MyLocationStyle;->getAnchorV()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/amap/api/maps2d/model/MarkerOptions;->anchor(FF)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v1

    iget-object v2, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    invoke-virtual {v2}, Lcom/amap/api/maps2d/model/MyLocationStyle;->getMyLocationIcon()Lcom/amap/api/maps2d/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/MarkerOptions;->icon(Lcom/amap/api/maps2d/model/BitmapDescriptor;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/amap/api/maps2d/model/LatLng;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/amap/api/maps2d/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v2}, Lcom/amap/api/maps2d/model/MarkerOptions;->position(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/MarkerOptions;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->b(Lcom/amap/api/maps2d/model/MarkerOptions;)Lcom/amap/api/a/be;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    .line 141
    iget-object v0, p0, Lcom/amap/api/a/bh;->e:Lcom/amap/api/maps2d/model/LatLng;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    iget-object v1, p0, Lcom/amap/api/a/bh;->e:Lcom/amap/api/maps2d/model/LatLng;

    invoke-interface {v0, v1}, Lcom/amap/api/a/aj;->a(Lcom/amap/api/maps2d/model/LatLng;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 144
    :catch_0
    move-exception v0

    .line 145
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 74
    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/amap/api/a/bh;->a:Lcom/amap/api/a/af;

    iget-object v1, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    invoke-interface {v1}, Lcom/amap/api/a/ag;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->a(Ljava/lang/String;)Z

    .line 76
    iput-object v2, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/amap/api/a/bh;->a:Lcom/amap/api/a/af;

    iget-object v1, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    invoke-interface {v1}, Lcom/amap/api/a/aj;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/amap/api/a/af;->b(Ljava/lang/String;)Z

    .line 80
    iput-object v2, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    .line 82
    :cond_1
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    if-eqz v0, :cond_0

    .line 152
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    invoke-interface {v0, p1}, Lcom/amap/api/a/aj;->a(F)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 155
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps2d/model/LatLng;D)V
    .locals 2

    .prologue
    .line 46
    iput-object p1, p0, Lcom/amap/api/a/bh;->e:Lcom/amap/api/maps2d/model/LatLng;

    .line 47
    iput-wide p2, p0, Lcom/amap/api/a/bh;->f:D

    .line 48
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    if-nez v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/amap/api/a/bh;->b()V

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    if-nez v0, :cond_2

    .line 63
    :cond_1
    :goto_0
    return-void

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    invoke-interface {v0, p1}, Lcom/amap/api/a/aj;->a(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 56
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    invoke-interface {v0, p1}, Lcom/amap/api/a/ag;->a(Lcom/amap/api/maps2d/model/LatLng;)V

    .line 57
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, p2, v0

    if-eqz v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    invoke-interface {v0, p2, p3}, Lcom/amap/api/a/ag;->a(D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps2d/model/MyLocationStyle;)V
    .locals 1

    .prologue
    .line 32
    iput-object p1, p0, Lcom/amap/api/a/bh;->d:Lcom/amap/api/maps2d/model/MyLocationStyle;

    .line 33
    iget-object v0, p0, Lcom/amap/api/a/bh;->b:Lcom/amap/api/a/aj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bh;->c:Lcom/amap/api/a/ag;

    if-nez v0, :cond_0

    .line 43
    :goto_0
    return-void

    .line 37
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/bh;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_1
    invoke-direct {p0}, Lcom/amap/api/a/bh;->d()V

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
