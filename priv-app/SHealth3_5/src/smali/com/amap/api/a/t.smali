.class public Lcom/amap/api/a/t;
.super Ljava/lang/Object;
.source "CameraUpdateFactoryDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/amap/api/a/t$a;
    }
.end annotation


# instance fields
.field a:Lcom/amap/api/a/t$a;

.field b:F

.field c:F

.field d:F

.field e:F

.field f:F

.field g:F

.field h:Lcom/amap/api/maps2d/model/CameraPosition;

.field i:Lcom/amap/api/maps2d/model/LatLngBounds;

.field j:I

.field k:I

.field l:I

.field m:Landroid/graphics/Point;

.field n:Lcom/autonavi/amap/mapcore2d/IPoint;

.field o:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/amap/api/a/t$a;->a:Lcom/amap/api/a/t$a;

    iput-object v0, p0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/t;->m:Landroid/graphics/Point;

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/amap/api/a/t;->o:Z

    .line 13
    return-void
.end method

.method public static a()Lcom/amap/api/a/t;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/amap/api/a/t;

    invoke-direct {v0}, Lcom/amap/api/a/t;-><init>()V

    .line 32
    return-object v0
.end method

.method public static a(F)Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 62
    sget-object v1, Lcom/amap/api/a/t$a;->f:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 63
    iput p0, v0, Lcom/amap/api/a/t;->d:F

    .line 64
    return-object v0
.end method

.method public static a(FF)Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 51
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 53
    sget-object v1, Lcom/amap/api/a/t$a;->h:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 54
    iput p0, v0, Lcom/amap/api/a/t;->b:F

    .line 55
    iput p1, v0, Lcom/amap/api/a/t;->c:F

    .line 56
    return-object v0
.end method

.method public static a(FLandroid/graphics/Point;)Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 72
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 74
    sget-object v1, Lcom/amap/api/a/t$a;->g:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 75
    iput p0, v0, Lcom/amap/api/a/t;->e:F

    .line 76
    iput-object p1, v0, Lcom/amap/api/a/t;->m:Landroid/graphics/Point;

    .line 77
    return-object v0
.end method

.method public static a(Lcom/amap/api/maps2d/model/CameraPosition;)Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 82
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 84
    sget-object v1, Lcom/amap/api/a/t$a;->i:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 85
    iput-object p0, v0, Lcom/amap/api/a/t;->h:Lcom/amap/api/maps2d/model/CameraPosition;

    .line 86
    return-object v0
.end method

.method public static a(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/a/t;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 90
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 92
    sget-object v1, Lcom/amap/api/a/t$a;->c:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 93
    new-instance v1, Lcom/amap/api/maps2d/model/CameraPosition;

    invoke-direct {v1, p0, v2, v2, v2}, Lcom/amap/api/maps2d/model/CameraPosition;-><init>(Lcom/amap/api/maps2d/model/LatLng;FFF)V

    iput-object v1, v0, Lcom/amap/api/a/t;->h:Lcom/amap/api/maps2d/model/CameraPosition;

    .line 94
    return-object v0
.end method

.method public static a(Lcom/amap/api/maps2d/model/LatLng;F)Lcom/amap/api/a/t;
    .locals 1

    .prologue
    .line 130
    invoke-static {}, Lcom/amap/api/maps2d/model/CameraPosition;->builder()Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->target(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->build()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/a/t;->a(Lcom/amap/api/maps2d/model/CameraPosition;)Lcom/amap/api/a/t;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/amap/api/maps2d/model/LatLng;FFF)Lcom/amap/api/a/t;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/amap/api/maps2d/model/CameraPosition;->builder()Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->target(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->zoom(F)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->bearing(F)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->tilt(F)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->build()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/a/t;->a(Lcom/amap/api/maps2d/model/CameraPosition;)Lcom/amap/api/a/t;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/amap/api/maps2d/model/LatLngBounds;I)Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 154
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 156
    sget-object v1, Lcom/amap/api/a/t$a;->j:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 157
    iput-object p0, v0, Lcom/amap/api/a/t;->i:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 158
    iput p1, v0, Lcom/amap/api/a/t;->j:I

    .line 159
    return-object v0
.end method

.method public static a(Lcom/amap/api/maps2d/model/LatLngBounds;III)Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 164
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 166
    sget-object v1, Lcom/amap/api/a/t$a;->k:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 167
    iput-object p0, v0, Lcom/amap/api/a/t;->i:Lcom/amap/api/maps2d/model/LatLngBounds;

    .line 168
    iput p3, v0, Lcom/amap/api/a/t;->j:I

    .line 169
    iput p1, v0, Lcom/amap/api/a/t;->k:I

    .line 170
    iput p2, v0, Lcom/amap/api/a/t;->l:I

    .line 171
    return-object v0
.end method

.method static a(Lcom/autonavi/amap/mapcore2d/IPoint;FFF)Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 142
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 144
    sget-object v1, Lcom/amap/api/a/t$a;->l:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 145
    iput-object p0, v0, Lcom/amap/api/a/t;->n:Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 146
    iput p1, v0, Lcom/amap/api/a/t;->d:F

    .line 147
    iput p2, v0, Lcom/amap/api/a/t;->g:F

    .line 148
    iput p3, v0, Lcom/amap/api/a/t;->f:F

    .line 149
    return-object v0
.end method

.method public static b()Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 36
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 38
    sget-object v1, Lcom/amap/api/a/t$a;->b:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 39
    return-object v0
.end method

.method public static b(F)Lcom/amap/api/a/t;
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/amap/api/a/t;->a(FLandroid/graphics/Point;)Lcom/amap/api/a/t;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/a/t;
    .locals 1

    .prologue
    .line 124
    invoke-static {}, Lcom/amap/api/maps2d/model/CameraPosition;->builder()Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->target(Lcom/amap/api/maps2d/model/LatLng;)Lcom/amap/api/maps2d/model/CameraPosition$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/maps2d/model/CameraPosition$Builder;->build()Lcom/amap/api/maps2d/model/CameraPosition;

    move-result-object v0

    invoke-static {v0}, Lcom/amap/api/a/t;->a(Lcom/amap/api/maps2d/model/CameraPosition;)Lcom/amap/api/a/t;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/amap/api/a/t;
    .locals 2

    .prologue
    .line 43
    invoke-static {}, Lcom/amap/api/a/t;->a()Lcom/amap/api/a/t;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/amap/api/a/t$a;->e:Lcom/amap/api/a/t$a;

    iput-object v1, v0, Lcom/amap/api/a/t;->a:Lcom/amap/api/a/t$a;

    .line 46
    return-object v0
.end method
