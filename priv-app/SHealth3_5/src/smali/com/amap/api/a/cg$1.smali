.class Lcom/amap/api/a/cg$1;
.super Ljava/lang/Object;
.source "ZoomControllerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/amap/api/a/cg;-><init>(Landroid/content/Context;Lcom/amap/api/a/av;Lcom/amap/api/a/af;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/cg;


# direct methods
.method constructor <init>(Lcom/amap/api/a/cg;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->b(Lcom/amap/api/a/cg;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->a(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    iget-object v0, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->c(Lcom/amap/api/a/cg;)Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->f()F

    move-result v0

    iget-object v1, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->c(Lcom/amap/api/a/cg;)Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/a/af;->h()F

    move-result v1

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->e(Lcom/amap/api/a/cg;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->d(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 98
    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    iget-object v1, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->c(Lcom/amap/api/a/cg;)Lcom/amap/api/a/af;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/a/af;->f()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/amap/api/a/cg;->a(F)V

    .line 99
    iget-object v0, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->g(Lcom/amap/api/a/cg;)Lcom/amap/api/a/av;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/av;->c()Z

    .line 100
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v0}, Lcom/amap/api/a/cg;->e(Lcom/amap/api/a/cg;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/amap/api/a/cg$1;->a:Lcom/amap/api/a/cg;

    invoke-static {v1}, Lcom/amap/api/a/cg;->f(Lcom/amap/api/a/cg;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
