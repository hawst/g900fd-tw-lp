.class public Lcom/amap/api/a/be$a;
.super Ljava/lang/Thread;
.source "MarkerDelegateImp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/be;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/amap/api/a/be;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/be;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 518
    const-string v0, "MarkerThread"

    invoke-virtual {p0, v0}, Lcom/amap/api/a/be$a;->setName(Ljava/lang/String;)V

    .line 521
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v0}, Lcom/amap/api/a/be;->a(Lcom/amap/api/a/be;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v0}, Lcom/amap/api/a/be;->a(Lcom/amap/api/a/be;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 522
    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v0}, Lcom/amap/api/a/be;->b(Lcom/amap/api/a/be;)I

    move-result v0

    iget-object v1, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v1}, Lcom/amap/api/a/be;->a(Lcom/amap/api/a/be;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    .line 523
    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/amap/api/a/be;->a(Lcom/amap/api/a/be;I)I

    .line 527
    :goto_1
    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v0}, Lcom/amap/api/a/be;->d(Lcom/amap/api/a/be;)Lcom/amap/api/a/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/ay;->a()Lcom/amap/api/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 529
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v0}, Lcom/amap/api/a/be;->e(Lcom/amap/api/a/be;)I

    move-result v0

    mul-int/lit16 v0, v0, 0xfa

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 533
    :goto_2
    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v0}, Lcom/amap/api/a/be;->a(Lcom/amap/api/a/be;)Ljava/util/concurrent/CopyOnWriteArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 534
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 525
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/be$a;->a:Lcom/amap/api/a/be;

    invoke-static {v0}, Lcom/amap/api/a/be;->c(Lcom/amap/api/a/be;)I

    goto :goto_1

    .line 530
    :catch_0
    move-exception v0

    .line 531
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 537
    :cond_2
    return-void
.end method
