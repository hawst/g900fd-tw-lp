.class Lcom/amap/api/a/bw;
.super Lcom/amap/api/a/h;
.source "TileServer.java"

# interfaces
.implements Lcom/amap/api/a/ce;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/amap/api/a/h",
        "<",
        "Lcom/amap/api/a/m$a;",
        "Lcom/amap/api/a/m$a;",
        ">;",
        "Lcom/amap/api/a/ce;"
    }
.end annotation


# instance fields
.field private g:Lcom/amap/api/a/ar;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bf;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/amap/api/a/h;-><init>(Lcom/amap/api/a/bf;Landroid/content/Context;)V

    .line 378
    new-instance v0, Lcom/amap/api/a/ar;

    invoke-direct {v0}, Lcom/amap/api/a/ar;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bw;->g:Lcom/amap/api/a/ar;

    .line 33
    new-instance v0, Lcom/amap/api/a/bt;

    invoke-direct {v0}, Lcom/amap/api/a/bt;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/bw;->c:Lcom/amap/api/a/br;

    .line 34
    iget-object v0, p1, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0, p0}, Lcom/amap/api/a/bf$d;->a(Lcom/amap/api/a/ce;)V

    .line 49
    return-void
.end method

.method private a(Ljava/util/ArrayList;Lcom/amap/api/a/as;IZ)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;",
            "Lcom/amap/api/a/as;",
            "IZ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 250
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 307
    :cond_0
    :goto_0
    return-object v0

    .line 253
    :cond_1
    iget-boolean v1, p2, Lcom/amap/api/a/as;->f:Z

    if-eqz v1, :cond_0

    .line 256
    iget-object v1, p2, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    if-eqz v1, :cond_0

    .line 259
    iget-object v1, p2, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    invoke-virtual {v1}, Lcom/amap/api/a/bq;->clear()V

    .line 261
    iget v1, p2, Lcom/amap/api/a/as;->b:I

    if-gt p3, v1, :cond_0

    iget v1, p2, Lcom/amap/api/a/as;->c:I

    if-lt p3, v1, :cond_0

    .line 265
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 266
    if-lez v3, :cond_0

    .line 270
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 276
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_5

    .line 277
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    .line 278
    if-nez v0, :cond_3

    .line 276
    :cond_2
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 281
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 282
    iget v5, v0, Lcom/amap/api/a/m$a;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 283
    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 284
    iget v5, v0, Lcom/amap/api/a/m$a;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 285
    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    iget v5, v0, Lcom/amap/api/a/m$a;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 288
    iget-object v5, p2, Lcom/amap/api/a/as;->m:Lcom/amap/api/a/p;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/amap/api/a/p;->a(Ljava/lang/String;)I

    move-result v4

    .line 290
    new-instance v5, Lcom/amap/api/a/m$a;

    iget v6, v0, Lcom/amap/api/a/m$a;->b:I

    iget v7, v0, Lcom/amap/api/a/m$a;->c:I

    iget v8, v0, Lcom/amap/api/a/m$a;->d:I

    iget v9, p2, Lcom/amap/api/a/as;->k:I

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/amap/api/a/m$a;-><init>(IIII)V

    .line 293
    iput v4, v5, Lcom/amap/api/a/m$a;->g:I

    .line 294
    iget-object v0, v0, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    iput-object v0, v5, Lcom/amap/api/a/m$a;->f:Landroid/graphics/PointF;

    .line 295
    iget-object v0, p2, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v5}, Lcom/amap/api/a/bq;->add(Ljava/lang/Object;)Z

    .line 297
    invoke-direct {p0, v5}, Lcom/amap/api/a/bw;->a(Lcom/amap/api/a/m$a;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p4, :cond_2

    .line 299
    iget-object v0, p0, Lcom/amap/api/a/bw;->g:Lcom/amap/api/a/ar;

    invoke-virtual {v0, v5}, Lcom/amap/api/a/ar;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 300
    iget-boolean v0, p2, Lcom/amap/api/a/as;->g:Z

    if-nez v0, :cond_4

    .line 301
    const/4 v0, -0x1

    iput v0, v5, Lcom/amap/api/a/m$a;->a:I

    .line 303
    :cond_4
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 307
    goto/16 :goto_0
.end method

.method private a(Ljava/util/ArrayList;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lcom/amap/api/a/bw;->c:Lcom/amap/api/a/br;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/amap/api/a/bw;->c:Lcom/amap/api/a/br;

    invoke-virtual {v0, p1, p2}, Lcom/amap/api/a/br;->a(Ljava/util/List;Z)V

    goto :goto_0
.end method

.method private a(Lcom/amap/api/a/m$a;)Z
    .locals 1

    .prologue
    .line 367
    if-eqz p1, :cond_0

    iget v0, p1, Lcom/amap/api/a/m$a;->g:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bw;->g:Lcom/amap/api/a/ar;

    if-nez v0, :cond_1

    .line 166
    :cond_0
    return-void

    .line 159
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 160
    if-eqz v2, :cond_0

    .line 163
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 164
    iget-object v3, p0, Lcom/amap/api/a/bw;->g:Lcom/amap/api/a/ar;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    invoke-virtual {v3, v0}, Lcom/amap/api/a/ar;->a(Lcom/amap/api/a/m$a;)V

    .line 163
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private h()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 244
    :goto_0
    return v0

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    if-nez v0, :cond_2

    move v0, v1

    .line 228
    goto :goto_0

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v4

    .line 231
    if-gtz v4, :cond_3

    move v0, v1

    .line 232
    goto :goto_0

    :cond_3
    move v3, v1

    .line 235
    :goto_1
    if-ge v3, v4, :cond_6

    .line 236
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 237
    if-nez v0, :cond_5

    .line 235
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 240
    :cond_5
    iget-boolean v0, v0, Lcom/amap/api/a/as;->f:Z

    if-ne v0, v2, :cond_4

    move v0, v2

    .line 241
    goto :goto_0

    :cond_6
    move v0, v1

    .line 244
    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 400
    if-nez p1, :cond_1

    move-object v3, v5

    .line 487
    :cond_0
    :goto_0
    return-object v3

    .line 403
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 404
    if-nez v4, :cond_2

    move-object v3, v5

    .line 405
    goto :goto_0

    :cond_2
    move v2, v7

    move-object v3, v5

    .line 414
    :goto_1
    if-ge v2, v4, :cond_0

    .line 415
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    .line 416
    if-nez v0, :cond_3

    move v0, v2

    move-object v1, v3

    move v2, v4

    .line 414
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move-object v3, v1

    move v4, v2

    move v2, v0

    goto :goto_1

    .line 419
    :cond_3
    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    if-nez v1, :cond_5

    :cond_4
    move-object v3, v5

    .line 421
    goto :goto_0

    .line 423
    :cond_5
    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v1}, Lcom/amap/api/a/bq;->size()I

    move-result v1

    .line 424
    iget v6, v0, Lcom/amap/api/a/m$a;->e:I

    if-lt v6, v1, :cond_6

    move v0, v2

    move-object v1, v3

    move v2, v4

    .line 425
    goto :goto_2

    .line 427
    :cond_6
    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    iget v6, v0, Lcom/amap/api/a/m$a;->e:I

    invoke-virtual {v1, v6}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amap/api/a/as;

    iget-boolean v1, v1, Lcom/amap/api/a/as;->g:Z

    if-nez v1, :cond_7

    move v0, v2

    move-object v1, v3

    move v2, v4

    .line 429
    goto :goto_2

    .line 437
    :cond_7
    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    iget v6, v0, Lcom/amap/api/a/m$a;->e:I

    invoke-virtual {v1, v6}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amap/api/a/as;

    iget-object v1, v1, Lcom/amap/api/a/as;->n:Lcom/amap/api/a/q;

    invoke-virtual {v1, v0}, Lcom/amap/api/a/q;->a(Lcom/amap/api/a/m$a;)I

    move-result v8

    .line 440
    if-ltz v8, :cond_c

    .line 444
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 445
    add-int/lit8 v4, v4, -0x1

    .line 446
    add-int/lit8 v2, v2, -0x1

    .line 453
    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v1, v1, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    iget v6, v0, Lcom/amap/api/a/m$a;->e:I

    invoke-virtual {v1, v6}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amap/api/a/as;

    iget-object v9, v1, Lcom/amap/api/a/as;->o:Lcom/amap/api/a/bq;

    .line 455
    if-nez v9, :cond_8

    move v0, v2

    move-object v1, v3

    move v2, v4

    .line 456
    goto :goto_2

    .line 461
    :cond_8
    invoke-virtual {v9}, Lcom/amap/api/a/bq;->size()I

    move-result v10

    move v6, v7

    .line 464
    :goto_3
    if-ge v6, v10, :cond_b

    .line 465
    invoke-virtual {v9, v6}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amap/api/a/m$a;

    .line 466
    if-nez v1, :cond_a

    .line 464
    :cond_9
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_3

    .line 469
    :cond_a
    invoke-virtual {v1, v0}, Lcom/amap/api/a/m$a;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 470
    iput v8, v1, Lcom/amap/api/a/m$a;->g:I

    .line 473
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$a;->a()V

    :cond_b
    move v0, v2

    move-object v1, v3

    move v2, v4

    .line 477
    goto/16 :goto_2

    .line 478
    :cond_c
    if-nez v3, :cond_d

    .line 479
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 481
    :goto_4
    new-instance v3, Lcom/amap/api/a/m$a;

    invoke-direct {v3, v0}, Lcom/amap/api/a/m$a;-><init>(Lcom/amap/api/a/m$a;)V

    .line 482
    const/4 v0, -0x1

    iput v0, v3, Lcom/amap/api/a/m$a;->a:I

    .line 483
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    move v2, v4

    goto/16 :goto_2

    :cond_d
    move-object v1, v3

    goto :goto_4
.end method

.method protected a(Ljava/util/ArrayList;Ljava/net/Proxy;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;",
            "Ljava/net/Proxy;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/amap/api/maps2d/AMapException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 55
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-object v1

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v2

    .line 63
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    iget v0, v0, Lcom/amap/api/a/m$a;->e:I

    if-ge v0, v2, :cond_0

    .line 66
    invoke-virtual {p0, p1}, Lcom/amap/api/a/bw;->a(Ljava/util/List;)V

    .line 67
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    iget v0, v0, Lcom/amap/api/a/m$a;->e:I

    .line 75
    iget-object v2, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v2, v2, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v2, v0}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 77
    iget-object v0, v0, Lcom/amap/api/a/as;->j:Lcom/amap/api/a/cd;

    if-eqz v0, :cond_4

    .line 78
    new-instance v2, Lcom/amap/api/a/bx;

    invoke-direct {v2, p1, p2, v1, v1}, Lcom/amap/api/a/bx;-><init>(Ljava/util/ArrayList;Ljava/net/Proxy;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v3, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    iget v0, v0, Lcom/amap/api/a/m$a;->e:I

    invoke-virtual {v3, v0}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    invoke-virtual {v2, v0}, Lcom/amap/api/a/bx;->a(Lcom/amap/api/a/as;)V

    .line 82
    invoke-virtual {v2}, Lcom/amap/api/a/bx;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 83
    invoke-virtual {v2, v1}, Lcom/amap/api/a/bx;->a(Lcom/amap/api/a/as;)V

    .line 93
    :goto_1
    invoke-direct {p0, p1}, Lcom/amap/api/a/bw;->b(Ljava/util/ArrayList;)V

    .line 94
    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    if-nez v1, :cond_3

    :cond_2
    move-object v1, v0

    .line 95
    goto/16 :goto_0

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v1, v1, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    invoke-virtual {v1}, Lcom/amap/api/a/bf$a;->a()V

    move-object v1, v0

    .line 117
    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 23
    invoke-super {p0}, Lcom/amap/api/a/h;->a()V

    .line 27
    iget-object v0, p0, Lcom/amap/api/a/bw;->g:Lcom/amap/api/a/ar;

    invoke-virtual {v0}, Lcom/amap/api/a/ar;->clear()V

    .line 29
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/a/m$a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    if-nez p1, :cond_1

    .line 145
    :cond_0
    return-void

    .line 133
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 134
    if-eqz v2, :cond_0

    .line 137
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 138
    iget-object v3, p0, Lcom/amap/api/a/bw;->g:Lcom/amap/api/a/ar;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/m$a;

    invoke-virtual {v3, v0}, Lcom/amap/api/a/ar;->b(Lcom/amap/api/a/m$a;)Z

    move-result v0

    .line 139
    if-nez v0, :cond_2

    .line 140
    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 141
    add-int/lit8 v0, v1, -0x1

    .line 142
    add-int/lit8 v1, v2, -0x1

    .line 137
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public a(ZZ)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 312
    invoke-direct {p0}, Lcom/amap/api/a/bw;->h()Z

    move-result v0

    .line 313
    if-nez v0, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v2, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v2, v2, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget-object v2, v2, Lcom/amap/api/a/ba;->j:Lcom/amap/api/a/ac;

    iget-object v3, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v3, v3, Lcom/amap/api/a/bf;->h:Lcom/amap/api/a/ba;

    iget v3, v3, Lcom/amap/api/a/ba;->g:I

    iget-object v5, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v5, v5, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v5}, Lcom/amap/api/a/bf$d;->c()I

    move-result v5

    iget-object v6, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v6, v6, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v6}, Lcom/amap/api/a/bf$d;->d()I

    move-result v6

    invoke-virtual {v0, v2, v3, v5, v6}, Lcom/amap/api/a/ba;->a(Lcom/amap/api/a/ac;III)Ljava/util/ArrayList;

    move-result-object v5

    .line 334
    if-eqz v5, :cond_0

    .line 337
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 338
    if-lez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0}, Lcom/amap/api/a/bq;->size()I

    move-result v6

    move v3, v1

    move v2, v4

    .line 345
    :goto_1
    if-ge v3, v6, :cond_2

    .line 346
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->d:Lcom/amap/api/a/bf$a;

    iget-object v0, v0, Lcom/amap/api/a/bf$a;->a:Lcom/amap/api/a/bq;

    invoke-virtual {v0, v3}, Lcom/amap/api/a/bq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/as;

    .line 347
    iget-object v7, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v7, v7, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v7}, Lcom/amap/api/a/bf$d;->e()I

    move-result v7

    invoke-direct {p0, v5, v0, v7, p2}, Lcom/amap/api/a/bw;->a(Ljava/util/ArrayList;Lcom/amap/api/a/as;IZ)Ljava/util/ArrayList;

    move-result-object v7

    .line 349
    if-eqz v7, :cond_3

    .line 350
    invoke-direct {p0, v7, v2}, Lcom/amap/api/a/bw;->a(Ljava/util/ArrayList;Z)V

    .line 351
    if-ne v2, v4, :cond_4

    move v0, v1

    .line 354
    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 345
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 359
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 361
    iget-object v0, p0, Lcom/amap/api/a/bw;->e:Lcom/amap/api/a/bf;

    iget-object v0, v0, Lcom/amap/api/a/bf;->b:Lcom/amap/api/a/bf$d;

    invoke-virtual {v0}, Lcom/amap/api/a/bf$d;->g()Lcom/amap/api/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/b;->invalidate()V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 390
    const/4 v0, 0x4

    return v0
.end method

.method protected f()I
    .locals 1

    .prologue
    .line 385
    const/4 v0, 0x1

    return v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 17
    return-void
.end method
