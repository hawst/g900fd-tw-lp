.class Lcom/amap/api/a/bz;
.super Lcom/amap/api/a/f;
.source "TransAnim.java"


# instance fields
.field private e:Lcom/amap/api/a/ac;

.field private f:Lcom/amap/api/a/ac;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:Lcom/amap/api/a/ca;


# direct methods
.method public constructor <init>(IILcom/amap/api/a/ac;Lcom/amap/api/a/ac;ILcom/amap/api/a/ca;)V
    .locals 4

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/amap/api/a/f;-><init>(II)V

    .line 29
    iput-object p3, p0, Lcom/amap/api/a/bz;->e:Lcom/amap/api/a/ac;

    .line 30
    iput-object p4, p0, Lcom/amap/api/a/bz;->f:Lcom/amap/api/a/ac;

    .line 32
    iget-object v0, p0, Lcom/amap/api/a/bz;->e:Lcom/amap/api/a/ac;

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/amap/api/a/bz;->g:I

    .line 33
    iget-object v0, p0, Lcom/amap/api/a/bz;->e:Lcom/amap/api/a/ac;

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/amap/api/a/bz;->h:I

    .line 34
    iput-object p6, p0, Lcom/amap/api/a/bz;->n:Lcom/amap/api/a/ca;

    .line 35
    invoke-virtual {p4}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v0

    iget-object v2, p0, Lcom/amap/api/a/bz;->e:Lcom/amap/api/a/ac;

    invoke-virtual {v2}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/amap/api/a/bz;->k:I

    .line 37
    invoke-virtual {p4}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v0

    iget-object v2, p0, Lcom/amap/api/a/bz;->e:Lcom/amap/api/a/ac;

    invoke-virtual {v2}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v2

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/amap/api/a/bz;->l:I

    .line 39
    invoke-direct {p0, p5}, Lcom/amap/api/a/bz;->a(I)V

    .line 40
    return-void
.end method

.method private a(III)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    if-le p2, p1, :cond_0

    .line 89
    add-int v0, p1, p3

    .line 90
    if-lt v0, p2, :cond_1

    .line 92
    iput v1, p0, Lcom/amap/api/a/bz;->m:I

    .line 101
    :goto_0
    return p2

    .line 95
    :cond_0
    sub-int v0, p1, p3

    .line 96
    if-gt v0, p2, :cond_1

    .line 98
    iput v1, p0, Lcom/amap/api/a/bz;->m:I

    goto :goto_0

    :cond_1
    move p2, v0

    goto :goto_0
.end method

.method private a(I)V
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 43
    div-int/lit8 v1, p1, 0xa

    div-int/lit8 v1, v1, 0xa

    .line 44
    if-ge v1, v0, :cond_0

    .line 47
    :goto_0
    iget v1, p0, Lcom/amap/api/a/bz;->k:I

    div-int/2addr v1, v0

    iput v1, p0, Lcom/amap/api/a/bz;->i:I

    .line 48
    iget v1, p0, Lcom/amap/api/a/bz;->l:I

    div-int v0, v1, v0

    iput v0, p0, Lcom/amap/api/a/bz;->j:I

    .line 50
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 65
    iget-object v0, p0, Lcom/amap/api/a/bz;->f:Lcom/amap/api/a/ac;

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->e()D

    move-result-wide v0

    double-to-int v6, v0

    .line 66
    iget-object v0, p0, Lcom/amap/api/a/bz;->f:Lcom/amap/api/a/ac;

    invoke-virtual {v0}, Lcom/amap/api/a/ac;->f()D

    move-result-wide v0

    double-to-int v7, v0

    .line 68
    invoke-virtual {p0}, Lcom/amap/api/a/bz;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 69
    iput v6, p0, Lcom/amap/api/a/bz;->g:I

    .line 70
    iput v7, p0, Lcom/amap/api/a/bz;->h:I

    .line 72
    iget-object v6, p0, Lcom/amap/api/a/bz;->n:Lcom/amap/api/a/ca;

    new-instance v0, Lcom/amap/api/a/ac;

    iget v1, p0, Lcom/amap/api/a/bz;->h:I

    int-to-double v1, v1

    iget v3, p0, Lcom/amap/api/a/bz;->g:I

    int-to-double v3, v3

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    invoke-interface {v6, v0}, Lcom/amap/api/a/ca;->a(Lcom/amap/api/a/ac;)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget v0, p0, Lcom/amap/api/a/bz;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/a/bz;->m:I

    .line 75
    iget v0, p0, Lcom/amap/api/a/bz;->g:I

    iget v1, p0, Lcom/amap/api/a/bz;->i:I

    invoke-direct {p0, v0, v6, v1}, Lcom/amap/api/a/bz;->a(III)I

    move-result v0

    iput v0, p0, Lcom/amap/api/a/bz;->g:I

    .line 76
    iget v0, p0, Lcom/amap/api/a/bz;->h:I

    iget v1, p0, Lcom/amap/api/a/bz;->j:I

    invoke-direct {p0, v0, v7, v1}, Lcom/amap/api/a/bz;->a(III)I

    move-result v0

    iput v0, p0, Lcom/amap/api/a/bz;->h:I

    .line 78
    iget-object v8, p0, Lcom/amap/api/a/bz;->n:Lcom/amap/api/a/ca;

    new-instance v0, Lcom/amap/api/a/ac;

    iget v1, p0, Lcom/amap/api/a/bz;->h:I

    int-to-double v1, v1

    iget v3, p0, Lcom/amap/api/a/bz;->g:I

    int-to-double v3, v3

    invoke-direct/range {v0 .. v5}, Lcom/amap/api/a/ac;-><init>(DDZ)V

    invoke-interface {v8, v0}, Lcom/amap/api/a/ca;->a(Lcom/amap/api/a/ac;)V

    .line 79
    iget v0, p0, Lcom/amap/api/a/bz;->g:I

    if-ne v0, v6, :cond_0

    iget v0, p0, Lcom/amap/api/a/bz;->h:I

    if-ne v0, v7, :cond_0

    .line 80
    invoke-virtual {p0, v5}, Lcom/amap/api/a/bz;->a(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/amap/api/a/bz;->h()V

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/amap/api/a/bz;->n:Lcom/amap/api/a/ca;

    invoke-interface {v0}, Lcom/amap/api/a/ca;->b()V

    .line 54
    invoke-static {}, Lcom/amap/api/a/s;->a()Lcom/amap/api/a/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/s;->b()V

    .line 55
    return-void
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/amap/api/a/bp;->a()Lcom/amap/api/a/bp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/amap/api/a/bp;->b()V

    .line 106
    return-void
.end method
