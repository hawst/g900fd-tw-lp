.class public Lcom/amap/api/a/bu$a;
.super Ljava/lang/Object;
.source "TileOverlayDelegateImp.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/bu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public e:Landroid/graphics/PointF;

.field public f:Landroid/graphics/Bitmap;

.field public g:Lcom/amap/api/a/a/m$a;

.field public h:I

.field final synthetic i:Lcom/amap/api/a/bu;


# direct methods
.method public constructor <init>(Lcom/amap/api/a/bu;IIII)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 422
    iput-object p1, p0, Lcom/amap/api/a/bu$a;->i:Lcom/amap/api/a/bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    iput-object v0, p0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    .line 522
    iput-object v0, p0, Lcom/amap/api/a/bu$a;->g:Lcom/amap/api/a/a/m$a;

    .line 523
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/bu$a;->h:I

    .line 423
    iput p2, p0, Lcom/amap/api/a/bu$a;->a:I

    .line 424
    iput p3, p0, Lcom/amap/api/a/bu$a;->b:I

    .line 425
    iput p4, p0, Lcom/amap/api/a/bu$a;->c:I

    .line 426
    iput p5, p0, Lcom/amap/api/a/bu$a;->d:I

    .line 427
    return-void
.end method

.method public constructor <init>(Lcom/amap/api/a/bu;Lcom/amap/api/a/bu$a;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 429
    iput-object p1, p0, Lcom/amap/api/a/bu$a;->i:Lcom/amap/api/a/bu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    iput-object v0, p0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    .line 522
    iput-object v0, p0, Lcom/amap/api/a/bu$a;->g:Lcom/amap/api/a/a/m$a;

    .line 523
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/bu$a;->h:I

    .line 430
    iget v0, p2, Lcom/amap/api/a/bu$a;->a:I

    iput v0, p0, Lcom/amap/api/a/bu$a;->a:I

    .line 431
    iget v0, p2, Lcom/amap/api/a/bu$a;->b:I

    iput v0, p0, Lcom/amap/api/a/bu$a;->b:I

    .line 432
    iget v0, p2, Lcom/amap/api/a/bu$a;->c:I

    iput v0, p0, Lcom/amap/api/a/bu$a;->c:I

    .line 433
    iget v0, p2, Lcom/amap/api/a/bu$a;->d:I

    iput v0, p0, Lcom/amap/api/a/bu$a;->d:I

    .line 434
    iget-object v0, p2, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/amap/api/a/bu$a;->e:Landroid/graphics/PointF;

    .line 435
    return-void
.end method


# virtual methods
.method public a()Lcom/amap/api/a/bu$a;
    .locals 2

    .prologue
    .line 439
    new-instance v0, Lcom/amap/api/a/bu$a;

    iget-object v1, p0, Lcom/amap/api/a/bu$a;->i:Lcom/amap/api/a/bu;

    invoke-direct {v0, v1, p0}, Lcom/amap/api/a/bu$a;-><init>(Lcom/amap/api/a/bu;Lcom/amap/api/a/bu$a;)V

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/16 v4, 0x6f

    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 476
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 478
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/amap/api/a/bu$a;->g:Lcom/amap/api/a/a/m$a;

    .line 479
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 480
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 481
    invoke-static {v0}, Lcom/amap/api/a/a/p;->a(I)I

    move-result v0

    invoke-static {v1}, Lcom/amap/api/a/a/p;->a(I)I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/amap/api/a/a/p;->a(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 503
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/bu$a;->i:Lcom/amap/api/a/bu;

    invoke-static {v0}, Lcom/amap/api/a/bu;->b(Lcom/amap/api/a/bu;)Lcom/amap/api/a/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/amap/api/a/af;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->g:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 505
    return-void

    .line 484
    :catch_0
    move-exception v0

    .line 485
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 486
    iget v0, p0, Lcom/amap/api/a/bu$a;->h:I

    if-ge v0, v3, :cond_0

    .line 487
    iget-object v0, p0, Lcom/amap/api/a/bu$a;->i:Lcom/amap/api/a/bu;

    invoke-static {v0}, Lcom/amap/api/a/bu;->a(Lcom/amap/api/a/bu;)Lcom/amap/api/a/a/k;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, Lcom/amap/api/a/a/k;->a(ZLcom/amap/api/a/bu$a;)V

    .line 488
    iget v0, p0, Lcom/amap/api/a/bu$a;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/a/bu$a;->h:I

    .line 489
    const-string v0, "TileOverlayDelegateImp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setBitmap Exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "retry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/amap/api/a/bu$a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/amap/api/a/a/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 495
    :cond_1
    iget v0, p0, Lcom/amap/api/a/bu$a;->h:I

    if-ge v0, v3, :cond_0

    .line 496
    iget-object v0, p0, Lcom/amap/api/a/bu$a;->i:Lcom/amap/api/a/bu;

    invoke-static {v0}, Lcom/amap/api/a/bu;->a(Lcom/amap/api/a/bu;)Lcom/amap/api/a/a/k;

    move-result-object v0

    invoke-virtual {v0, v2, p0}, Lcom/amap/api/a/a/k;->a(ZLcom/amap/api/a/bu$a;)V

    .line 497
    iget v0, p0, Lcom/amap/api/a/bu$a;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/amap/api/a/bu$a;->h:I

    .line 498
    const-string v0, "TileOverlayDelegateImp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setBitmap failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "retry: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/amap/api/a/bu$a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/amap/api/a/a/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 508
    invoke-static {p0}, Lcom/amap/api/a/a/m;->a(Lcom/amap/api/a/bu$a;)V

    .line 509
    iget-object v0, p0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 512
    :cond_0
    iput-object v1, p0, Lcom/amap/api/a/bu$a;->f:Landroid/graphics/Bitmap;

    .line 513
    iput-object v1, p0, Lcom/amap/api/a/bu$a;->g:Lcom/amap/api/a/a/m$a;

    .line 514
    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 421
    invoke-virtual {p0}, Lcom/amap/api/a/bu$a;->a()Lcom/amap/api/a/bu$a;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 444
    if-ne p0, p1, :cond_1

    .line 452
    :cond_0
    :goto_0
    return v0

    .line 447
    :cond_1
    instance-of v2, p1, Lcom/amap/api/a/bu$a;

    if-nez v2, :cond_2

    move v0, v1

    .line 448
    goto :goto_0

    .line 451
    :cond_2
    check-cast p1, Lcom/amap/api/a/bu$a;

    .line 452
    iget v2, p0, Lcom/amap/api/a/bu$a;->a:I

    iget v3, p1, Lcom/amap/api/a/bu$a;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/a/bu$a;->b:I

    iget v3, p1, Lcom/amap/api/a/bu$a;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/a/bu$a;->c:I

    iget v3, p1, Lcom/amap/api/a/bu$a;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/amap/api/a/bu$a;->d:I

    iget v3, p1, Lcom/amap/api/a/bu$a;->d:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 459
    iget v0, p0, Lcom/amap/api/a/bu$a;->a:I

    mul-int/lit8 v0, v0, 0x7

    iget v1, p0, Lcom/amap/api/a/bu$a;->b:I

    mul-int/lit8 v1, v1, 0xb

    add-int/2addr v0, v1

    iget v1, p0, Lcom/amap/api/a/bu$a;->c:I

    mul-int/lit8 v1, v1, 0xd

    add-int/2addr v0, v1

    iget v1, p0, Lcom/amap/api/a/bu$a;->d:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 464
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 465
    iget v1, p0, Lcom/amap/api/a/bu$a;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 466
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 467
    iget v1, p0, Lcom/amap/api/a/bu$a;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 468
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    iget v1, p0, Lcom/amap/api/a/bu$a;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 470
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    iget v1, p0, Lcom/amap/api/a/bu$a;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 472
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
