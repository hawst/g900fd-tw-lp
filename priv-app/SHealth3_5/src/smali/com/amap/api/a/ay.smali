.class Lcom/amap/api/a/ay;
.super Landroid/view/View;
.source "MapOverlayImageView.java"


# instance fields
.field a:Lcom/amap/api/a/b;

.field private b:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/amap/api/a/aj;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/autonavi/amap/mapcore2d/IPoint;

.field private d:Lcom/amap/api/a/aj;

.field private final e:Landroid/os/Handler;

.field private f:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/amap/api/a/b;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 120
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/ay;->e:Landroid/os/Handler;

    .line 133
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/ay;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 42
    iput-object p3, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    .line 43
    return-void
.end method

.method static synthetic a(Lcom/amap/api/a/ay;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/amap/api/a/ay;->g()V

    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 190
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    .line 191
    iget-object v2, p0, Lcom/amap/api/a/ay;->d:Lcom/amap/api/a/aj;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/amap/api/a/ay;->d:Lcom/amap/api/a/aj;

    invoke-interface {v2}, Lcom/amap/api/a/aj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/amap/api/a/aj;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    invoke-interface {v0}, Lcom/amap/api/a/aj;->b()Landroid/graphics/Rect;

    move-result-object v2

    .line 193
    new-instance v3, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v4, v2, Landroid/graphics/Rect;->left:I

    invoke-interface {v0}, Lcom/amap/api/a/aj;->q()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-direct {v3, v0, v2}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>(II)V

    iput-object v3, p0, Lcom/amap/api/a/ay;->c:Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 195
    iget-object v0, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->u()V

    goto :goto_0

    .line 198
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)Lcom/amap/api/a/aj;
    .locals 6

    .prologue
    .line 209
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 210
    const/4 v1, 0x0

    move v2, v0

    .line 211
    :goto_0
    if-ltz v2, :cond_1

    .line 212
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    .line 213
    invoke-interface {v0}, Lcom/amap/api/a/aj;->b()Landroid/graphics/Rect;

    move-result-object v3

    .line 214
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/amap/api/a/ay;->a(Landroid/graphics/Rect;II)Z

    move-result v3

    .line 215
    if-eqz v3, :cond_0

    .line 220
    :goto_1
    return-object v0

    .line 211
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)Lcom/amap/api/a/aj;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    .line 47
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/amap/api/a/aj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Lcom/amap/api/a/b;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/amap/api/a/ay;->e:Landroid/os/Handler;

    new-instance v1, Lcom/amap/api/a/ay$1;

    invoke-direct {v1, p0}, Lcom/amap/api/a/ay$1;-><init>(Lcom/amap/api/a/ay;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 152
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    .line 153
    iget-object v2, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    invoke-interface {v0, p1, v2}, Lcom/amap/api/a/aj;->a(Landroid/graphics/Canvas;Lcom/amap/api/a/af;)V

    goto :goto_0

    .line 155
    :cond_0
    return-void
.end method

.method public a(Lcom/amap/api/a/aj;)V
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 74
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 75
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public a(Landroid/graphics/Rect;II)Z
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    return v0
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 245
    const/4 v2, 0x0

    .line 246
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    .line 247
    :goto_0
    if-ltz v3, :cond_1

    .line 248
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    .line 249
    invoke-interface {v0}, Lcom/amap/api/a/aj;->b()Landroid/graphics/Rect;

    move-result-object v4

    .line 250
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v4, v1, v5}, Lcom/amap/api/a/ay;->a(Landroid/graphics/Rect;II)Z

    move-result v1

    .line 251
    if-eqz v1, :cond_0

    .line 253
    new-instance v2, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v3, v4, Landroid/graphics/Rect;->left:I

    invoke-interface {v0}, Lcom/amap/api/a/aj;->q()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-direct {v2, v3, v4}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>(II)V

    iput-object v2, p0, Lcom/amap/api/a/ay;->c:Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 255
    iput-object v0, p0, Lcom/amap/api/a/ay;->d:Lcom/amap/api/a/aj;

    move v0, v1

    .line 277
    :goto_1
    return v0

    .line 247
    :cond_0
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public b(Lcom/amap/api/a/aj;)Z
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0, p1}, Lcom/amap/api/a/ay;->e(Lcom/amap/api/a/aj;)V

    .line 80
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    if-nez v0, :cond_0

    .line 70
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    .line 67
    invoke-interface {v0}, Lcom/amap/api/a/aj;->n()V

    goto :goto_1

    .line 69
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    goto :goto_0
.end method

.method public c(Lcom/amap/api/a/aj;)V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 85
    iget-object v1, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 87
    iget-object v2, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    iget-object v3, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 88
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 92
    return-void
.end method

.method public d()Lcom/amap/api/a/aj;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/amap/api/a/ay;->d:Lcom/amap/api/a/aj;

    return-object v0
.end method

.method public d(Lcom/amap/api/a/aj;)V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lcom/amap/api/a/ay;->c:Lcom/autonavi/amap/mapcore2d/IPoint;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v0}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    iput-object v0, p0, Lcom/amap/api/a/ay;->c:Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 99
    :cond_0
    invoke-interface {p1}, Lcom/amap/api/a/aj;->b()Landroid/graphics/Rect;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/autonavi/amap/mapcore2d/IPoint;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    invoke-interface {p1}, Lcom/amap/api/a/aj;->q()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-direct {v1, v2, v0}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>(II)V

    iput-object v1, p0, Lcom/amap/api/a/ay;->c:Lcom/autonavi/amap/mapcore2d/IPoint;

    .line 101
    iput-object p1, p0, Lcom/amap/api/a/ay;->d:Lcom/amap/api/a/aj;

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    invoke-virtual {p0}, Lcom/amap/api/a/ay;->d()Lcom/amap/api/a/aj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/b;->a(Lcom/amap/api/a/aj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 229
    :try_start_0
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    .line 230
    if-eqz v0, :cond_0

    .line 231
    invoke-interface {v0}, Lcom/amap/api/a/aj;->n()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 238
    const-string v1, "amapApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MapOverlayImageView clear erro"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :goto_1
    return-void

    .line 235
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/amap/api/a/ay;->c()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public e(Lcom/amap/api/a/aj;)V
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/amap/api/a/ay;->f(Lcom/amap/api/a/aj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->t()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/ay;->d:Lcom/amap/api/a/aj;

    .line 114
    :cond_0
    return-void
.end method

.method public f()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/amap/api/maps2d/model/Marker;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 281
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 282
    new-instance v8, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->c()I

    move-result v1

    iget-object v2, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    invoke-virtual {v2}, Lcom/amap/api/a/b;->d()I

    move-result v2

    invoke-direct {v8, v0, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 285
    new-instance v5, Lcom/autonavi/amap/mapcore2d/IPoint;

    invoke-direct {v5}, Lcom/autonavi/amap/mapcore2d/IPoint;-><init>()V

    move v6, v0

    .line 286
    :goto_0
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    .line 287
    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    invoke-interface {v0}, Lcom/amap/api/a/aj;->d()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v3

    .line 288
    iget-object v0, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    iget-wide v1, v3, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    iget-wide v3, v3, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    invoke-virtual/range {v0 .. v5}, Lcom/amap/api/a/b;->b(DDLcom/autonavi/amap/mapcore2d/IPoint;)V

    .line 289
    iget v0, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->x:I

    iget v1, v5, Lcom/autonavi/amap/mapcore2d/IPoint;->y:I

    invoke-virtual {p0, v8, v0, v1}, Lcom/amap/api/a/ay;->a(Landroid/graphics/Rect;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    new-instance v1, Lcom/amap/api/maps2d/model/Marker;

    iget-object v0, p0, Lcom/amap/api/a/ay;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/amap/api/a/aj;

    invoke-direct {v1, v0}, Lcom/amap/api/maps2d/model/Marker;-><init>(Lcom/amap/api/a/aj;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 293
    :cond_1
    return-object v7
.end method

.method public f(Lcom/amap/api/a/aj;)Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/amap/api/a/ay;->a:Lcom/amap/api/a/b;

    invoke-virtual {v0, p1}, Lcom/amap/api/a/b;->b(Lcom/amap/api/a/aj;)Z

    move-result v0

    return v0
.end method
