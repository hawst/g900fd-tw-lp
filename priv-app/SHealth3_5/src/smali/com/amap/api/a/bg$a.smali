.class Lcom/amap/api/a/bg$a;
.super Lcom/amap/api/a/bg;
.source "MultiTouchGestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/amap/api/a/bg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field q:F

.field r:F

.field s:F

.field t:F

.field u:J


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/amap/api/a/bg;-><init>()V

    .line 106
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/a/bg$a;->u:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/amap/api/a/bg$1;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/amap/api/a/bg$a;-><init>()V

    return-void
.end method

.method private a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 283
    .line 285
    :try_start_0
    invoke-static {}, Lcom/amap/api/a/bg;->b()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {}, Lcom/amap/api/a/bg;->b()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, p2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    add-float/2addr v0, v1

    .line 303
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/amap/api/a/bg;->c()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {}, Lcom/amap/api/a/bg;->c()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, p2, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_5

    move-result v0

    add-float/2addr v0, v1

    .line 318
    :goto_1
    sget-object v0, Lcom/amap/api/a/bg$a;->b:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 319
    sget-object v1, Lcom/amap/api/a/bg$a;->b:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 320
    div-float/2addr v0, v5

    div-float/2addr v1, v5

    invoke-virtual {p1, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 321
    return-void

    .line 290
    :catch_0
    move-exception v0

    .line 292
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 293
    :catch_1
    move-exception v0

    .line 295
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 296
    :catch_2
    move-exception v0

    .line 298
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0

    .line 308
    :catch_3
    move-exception v0

    .line 310
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 311
    :catch_4
    move-exception v0

    .line 313
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 314
    :catch_5
    move-exception v0

    .line 316
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method private c(Landroid/view/MotionEvent;)F
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 241
    .line 243
    :try_start_0
    invoke-static {}, Lcom/amap/api/a/bg;->b()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-virtual {v0, p1, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {}, Lcom/amap/api/a/bg;->b()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    sub-float v0, v1, v0

    move v1, v0

    .line 261
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/amap/api/a/bg;->c()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-static {}, Lcom/amap/api/a/bg;->c()Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, p1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_5

    move-result v0

    sub-float v2, v3, v0

    .line 278
    :goto_1
    mul-float v0, v1, v1

    mul-float v1, v2, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0

    .line 248
    :catch_0
    move-exception v0

    .line 250
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v1, v2

    .line 257
    goto :goto_0

    .line 251
    :catch_1
    move-exception v0

    .line 253
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    move v1, v2

    .line 257
    goto :goto_0

    .line 254
    :catch_2
    move-exception v0

    .line 256
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    move v1, v2

    goto :goto_0

    .line 266
    :catch_3
    move-exception v0

    .line 268
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 269
    :catch_4
    move-exception v0

    .line 271
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 272
    :catch_5
    move-exception v0

    .line 274
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/high16 v5, 0x41200000    # 10.0f

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 110
    invoke-static {p1}, Lcom/amap/api/a/bg;->b(Landroid/view/MotionEvent;)V

    .line 112
    invoke-static {}, Lcom/amap/api/a/bg;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v2

    .line 115
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    :cond_2
    :pswitch_0
    move v0, v2

    :cond_3
    :goto_1
    move v2, v0

    .line 237
    goto :goto_0

    .line 117
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/amap/api/a/bg$a;->u:J

    .line 118
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iput v1, p0, Lcom/amap/api/a/bg$a;->q:F

    .line 119
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iput v1, p0, Lcom/amap/api/a/bg$a;->r:F

    .line 120
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->e:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 121
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->f:Landroid/graphics/PointF;

    iget v3, p0, Lcom/amap/api/a/bg$a;->q:F

    iget v4, p0, Lcom/amap/api/a/bg$a;->r:F

    invoke-virtual {v1, v3, v4}, Landroid/graphics/PointF;->set(FF)V

    .line 122
    iput v0, p0, Lcom/amap/api/a/bg$a;->c:I

    move v0, v2

    .line 123
    goto :goto_1

    .line 126
    :pswitch_2
    iget v1, p0, Lcom/amap/api/a/bg$a;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/amap/api/a/bg$a;->o:I

    .line 127
    iget v1, p0, Lcom/amap/api/a/bg$a;->o:I

    if-ne v1, v0, :cond_2

    .line 128
    iput-boolean v0, p0, Lcom/amap/api/a/bg$a;->n:Z

    .line 129
    const/high16 v1, 0x3f800000    # 1.0f

    sput v1, Lcom/amap/api/a/bg$a;->k:F

    .line 130
    invoke-direct {p0, p1}, Lcom/amap/api/a/bg$a;->c(Landroid/view/MotionEvent;)F

    move-result v1

    iput v1, p0, Lcom/amap/api/a/bg$a;->i:F

    .line 131
    iget v1, p0, Lcom/amap/api/a/bg$a;->i:F

    cmpl-float v1, v1, v5

    if-lez v1, :cond_2

    .line 132
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 133
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->e:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 134
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->e:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 135
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->g:Landroid/graphics/PointF;

    invoke-direct {p0, v1, p1}, Lcom/amap/api/a/bg$a;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 136
    iput v4, p0, Lcom/amap/api/a/bg$a;->c:I

    .line 137
    iput-boolean v0, p0, Lcom/amap/api/a/bg$a;->l:Z

    .line 138
    iget-object v0, p0, Lcom/amap/api/a/bg$a;->a:Lcom/amap/api/a/bg$b;

    iget-object v1, p0, Lcom/amap/api/a/bg$a;->f:Landroid/graphics/PointF;

    invoke-interface {v0, v1}, Lcom/amap/api/a/bg$b;->a(Landroid/graphics/PointF;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 139
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->g:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iput v1, p0, Lcom/amap/api/a/bg$a;->s:F

    .line 140
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->g:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, p0, Lcom/amap/api/a/bg$a;->t:F

    goto :goto_1

    .line 145
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/amap/api/a/bg$a;->p:J

    .line 146
    iput-boolean v2, p0, Lcom/amap/api/a/bg$a;->l:Z

    .line 147
    iput v2, p0, Lcom/amap/api/a/bg$a;->c:I

    move v0, v2

    .line 148
    goto :goto_1

    .line 150
    :pswitch_4
    iget v1, p0, Lcom/amap/api/a/bg$a;->o:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/amap/api/a/bg$a;->o:I

    .line 152
    iget v1, p0, Lcom/amap/api/a/bg$a;->o:I

    if-ne v1, v0, :cond_4

    .line 153
    iput-boolean v0, p0, Lcom/amap/api/a/bg$a;->n:Z

    .line 154
    iput v4, p0, Lcom/amap/api/a/bg$a;->c:I

    .line 159
    :cond_4
    iget v0, p0, Lcom/amap/api/a/bg$a;->o:I

    if-nez v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/amap/api/a/bg$a;->g:Landroid/graphics/PointF;

    invoke-direct {p0, v0, p1}, Lcom/amap/api/a/bg$a;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 161
    iput-boolean v2, p0, Lcom/amap/api/a/bg$a;->m:Z

    .line 162
    iput-boolean v2, p0, Lcom/amap/api/a/bg$a;->n:Z

    .line 163
    iget-boolean v0, p0, Lcom/amap/api/a/bg$a;->l:Z

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/amap/api/a/bg$a;->a:Lcom/amap/api/a/bg$b;

    iget v1, p0, Lcom/amap/api/a/bg$a;->j:F

    iget-object v3, p0, Lcom/amap/api/a/bg$a;->g:Landroid/graphics/PointF;

    invoke-interface {v0, v1, v3}, Lcom/amap/api/a/bg$b;->a(FLandroid/graphics/PointF;)Z

    move-result v0

    or-int/2addr v0, v2

    .line 170
    iput v2, p0, Lcom/amap/api/a/bg$a;->c:I

    goto/16 :goto_1

    .line 175
    :pswitch_5
    iget v1, p0, Lcom/amap/api/a/bg$a;->c:I

    if-ne v1, v0, :cond_5

    .line 177
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 178
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 179
    iget-object v4, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    iget-object v5, p0, Lcom/amap/api/a/bg$a;->e:Landroid/graphics/Matrix;

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 180
    iget-object v4, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/amap/api/a/bg$a;->f:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v7, p0, Lcom/amap/api/a/bg$a;->f:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 182
    iget-object v4, p0, Lcom/amap/api/a/bg$a;->a:Lcom/amap/api/a/bg$b;

    iget v5, p0, Lcom/amap/api/a/bg$a;->q:F

    sub-float v5, v1, v5

    iget v6, p0, Lcom/amap/api/a/bg$a;->r:F

    sub-float v6, v3, v6

    invoke-interface {v4, v5, v6}, Lcom/amap/api/a/bg$b;->a(FF)Z

    move-result v4

    or-int/2addr v2, v4

    .line 184
    iput v1, p0, Lcom/amap/api/a/bg$a;->q:F

    .line 185
    iput v3, p0, Lcom/amap/api/a/bg$a;->r:F

    .line 186
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->a:Lcom/amap/api/a/bg$b;

    iget-object v3, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    invoke-interface {v1, v3}, Lcom/amap/api/a/bg$b;->a(Landroid/graphics/Matrix;)Z

    move-result v1

    or-int/2addr v1, v2

    .line 187
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/amap/api/a/bg$a;->u:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1e

    cmp-long v2, v2, v4

    if-ltz v2, :cond_3

    :goto_2
    move v0, v1

    goto/16 :goto_1

    .line 190
    :cond_5
    iget v1, p0, Lcom/amap/api/a/bg$a;->c:I

    if-ne v1, v4, :cond_2

    .line 192
    :try_start_0
    sget-object v1, Lcom/amap/api/a/bg$a;->b:Lcom/amap/api/a/b;

    invoke-virtual {v1}, Lcom/amap/api/a/b;->q()Lcom/amap/api/a/aq;

    move-result-object v1

    invoke-interface {v1}, Lcom/amap/api/a/aq;->f()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    :goto_3
    invoke-direct {p0, p1}, Lcom/amap/api/a/bg$a;->c(Landroid/view/MotionEvent;)F

    move-result v3

    .line 199
    const/4 v1, 0x0

    iput v1, p0, Lcom/amap/api/a/bg$a;->j:F

    .line 200
    cmpl-float v1, v3, v5

    if-lez v1, :cond_2

    iget v1, p0, Lcom/amap/api/a/bg$a;->i:F

    sub-float v1, v3, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v4, 0x40a00000    # 5.0f

    cmpl-float v1, v1, v4

    if-lez v1, :cond_2

    .line 201
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/amap/api/a/bg$a;->e:Landroid/graphics/Matrix;

    invoke-virtual {v1, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 202
    iget v1, p0, Lcom/amap/api/a/bg$a;->i:F

    cmpl-float v1, v3, v1

    if-lez v1, :cond_7

    iget v1, p0, Lcom/amap/api/a/bg$a;->i:F

    div-float v1, v3, v1

    :goto_4
    iput v1, p0, Lcom/amap/api/a/bg$a;->j:F

    .line 204
    iget v1, p0, Lcom/amap/api/a/bg$a;->i:F

    div-float v1, v3, v1

    sput v1, Lcom/amap/api/a/bg$a;->k:F

    .line 206
    iget v1, p0, Lcom/amap/api/a/bg$a;->i:F

    cmpg-float v1, v3, v1

    if-gez v1, :cond_6

    .line 207
    iget v1, p0, Lcom/amap/api/a/bg$a;->j:F

    neg-float v1, v1

    iput v1, p0, Lcom/amap/api/a/bg$a;->j:F

    .line 217
    :cond_6
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->h:Landroid/graphics/PointF;

    invoke-direct {p0, v1, p1}, Lcom/amap/api/a/bg$a;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 218
    iget-object v1, p0, Lcom/amap/api/a/bg$a;->a:Lcom/amap/api/a/bg$b;

    iget-object v4, p0, Lcom/amap/api/a/bg$a;->h:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget v5, p0, Lcom/amap/api/a/bg$a;->s:F

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/amap/api/a/bg$a;->h:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget v6, p0, Lcom/amap/api/a/bg$a;->t:F

    sub-float/2addr v5, v6

    invoke-interface {v1, v4, v5}, Lcom/amap/api/a/bg$b;->a(FF)Z

    move-result v1

    or-int/2addr v1, v2

    .line 220
    iget-object v2, p0, Lcom/amap/api/a/bg$a;->h:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iput v2, p0, Lcom/amap/api/a/bg$a;->s:F

    .line 221
    iget-object v2, p0, Lcom/amap/api/a/bg$a;->h:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iput v2, p0, Lcom/amap/api/a/bg$a;->t:F

    .line 225
    iget-object v2, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    iget v4, p0, Lcom/amap/api/a/bg$a;->i:F

    div-float v4, v3, v4

    iget v5, p0, Lcom/amap/api/a/bg$a;->i:F

    div-float/2addr v3, v5

    iget-object v5, p0, Lcom/amap/api/a/bg$a;->g:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lcom/amap/api/a/bg$a;->g:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 227
    iget-object v2, p0, Lcom/amap/api/a/bg$a;->a:Lcom/amap/api/a/bg$b;

    iget v3, p0, Lcom/amap/api/a/bg$a;->j:F

    invoke-interface {v2, v3}, Lcom/amap/api/a/bg$b;->c(F)Z

    move-result v2

    or-int/2addr v1, v2

    .line 228
    iget-object v2, p0, Lcom/amap/api/a/bg$a;->a:Lcom/amap/api/a/bg$b;

    iget-object v3, p0, Lcom/amap/api/a/bg$a;->d:Landroid/graphics/Matrix;

    invoke-interface {v2, v3}, Lcom/amap/api/a/bg$b;->b(Landroid/graphics/Matrix;)Z

    move-result v2

    or-int/2addr v1, v2

    .line 229
    iput-boolean v0, p0, Lcom/amap/api/a/bg$a;->m:Z

    goto/16 :goto_2

    .line 195
    :catch_0
    move-exception v1

    .line 196
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_3

    .line 202
    :cond_7
    iget v1, p0, Lcom/amap/api/a/bg$a;->i:F

    div-float/2addr v1, v3

    goto :goto_4

    .line 115
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
