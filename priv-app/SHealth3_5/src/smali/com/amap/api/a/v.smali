.class Lcom/amap/api/a/v;
.super Ljava/lang/Object;
.source "CircleDelegateImp.java"

# interfaces
.implements Lcom/amap/api/a/ag;


# static fields
.field private static l:F

.field private static m:I

.field private static n:I


# instance fields
.field private a:Lcom/amap/api/maps2d/model/LatLng;

.field private b:D

.field private c:F

.field private d:I

.field private e:I

.field private f:F

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Lcom/amap/api/a/b;

.field private j:I

.field private k:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    const v0, 0x4c18dfc2    # 4.0075016E7f

    sput v0, Lcom/amap/api/a/v;->l:F

    .line 171
    const/16 v0, 0x100

    sput v0, Lcom/amap/api/a/v;->m:I

    .line 172
    const/16 v0, 0x14

    sput v0, Lcom/amap/api/a/v;->n:I

    return-void
.end method

.method public constructor <init>(Lcom/amap/api/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/v;->a:Lcom/amap/api/maps2d/model/LatLng;

    .line 14
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/amap/api/a/v;->b:D

    .line 15
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/amap/api/a/v;->c:F

    .line 16
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/amap/api/a/v;->d:I

    .line 17
    iput v2, p0, Lcom/amap/api/a/v;->e:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/amap/api/a/v;->f:F

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/amap/api/a/v;->g:Z

    .line 22
    iput v2, p0, Lcom/amap/api/a/v;->j:I

    iput v2, p0, Lcom/amap/api/a/v;->k:I

    .line 25
    iput-object p1, p0, Lcom/amap/api/a/v;->i:Lcom/amap/api/a/b;

    .line 27
    :try_start_0
    invoke-virtual {p0}, Lcom/amap/api/a/v;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/v;->h:Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :goto_0
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 29
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public a(D)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/amap/api/a/v;->b:D

    .line 132
    invoke-virtual {p0}, Lcom/amap/api/a/v;->g()V

    .line 133
    return-void
.end method

.method public a(F)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 54
    iput p1, p0, Lcom/amap/api/a/v;->f:F

    .line 55
    iget-object v0, p0, Lcom/amap/api/a/v;->i:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 56
    return-void
.end method

.method public a(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 152
    iput p1, p0, Lcom/amap/api/a/v;->d:I

    .line 153
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const-wide v5, 0x412e848000000000L    # 1000000.0

    .line 91
    invoke-virtual {p0}, Lcom/amap/api/a/v;->h()Lcom/amap/api/maps2d/model/LatLng;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/amap/api/a/v;->b:D

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/amap/api/a/v;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/amap/api/a/v;->i:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->a()Lcom/amap/api/a/bf;

    move-result-object v0

    iget-object v0, v0, Lcom/amap/api/a/bf;->a:Lcom/amap/api/a/bf$e;

    invoke-virtual {p0}, Lcom/amap/api/a/v;->i()D

    move-result-wide v1

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/bf$e;->a(F)F

    move-result v0

    .line 96
    new-instance v1, Lcom/amap/api/a/ac;

    iget-object v2, p0, Lcom/amap/api/a/v;->a:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v2, v2, Lcom/amap/api/maps2d/model/LatLng;->latitude:D

    mul-double/2addr v2, v5

    double-to-int v2, v2

    iget-object v3, p0, Lcom/amap/api/a/v;->a:Lcom/amap/api/maps2d/model/LatLng;

    iget-wide v3, v3, Lcom/amap/api/maps2d/model/LatLng;->longitude:D

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-direct {v1, v2, v3}, Lcom/amap/api/a/ac;-><init>(II)V

    .line 98
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 99
    iget-object v3, p0, Lcom/amap/api/a/v;->i:Lcom/amap/api/a/b;

    invoke-virtual {v3}, Lcom/amap/api/a/b;->s()Lcom/amap/api/a/bl;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Lcom/amap/api/a/bl;->a(Lcom/amap/api/a/ac;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 100
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 101
    invoke-virtual {p0}, Lcom/amap/api/a/v;->l()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 104
    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v4, v2, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 106
    invoke-virtual {p0}, Lcom/amap/api/a/v;->k()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 108
    invoke-virtual {p0}, Lcom/amap/api/a/v;->j()F

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 109
    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {p1, v3, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public a(Lcom/amap/api/maps2d/model/LatLng;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 120
    iput-object p1, p0, Lcom/amap/api/a/v;->a:Lcom/amap/api/maps2d/model/LatLng;

    .line 121
    invoke-virtual {p0}, Lcom/amap/api/a/v;->g()V

    .line 122
    return-void
.end method

.method public a(Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/amap/api/a/v;->g:Z

    .line 66
    iget-object v0, p0, Lcom/amap/api/a/v;->i:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 67
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/amap/api/a/ak;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/amap/api/a/ak;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/amap/api/a/v;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    :cond_0
    const/4 v0, 0x1

    .line 81
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lcom/amap/api/a/v;->i:Lcom/amap/api/a/b;

    invoke-virtual {p0}, Lcom/amap/api/a/v;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/amap/api/a/b;->a(Ljava/lang/String;)Z

    .line 41
    iget-object v0, p0, Lcom/amap/api/a/v;->i:Lcom/amap/api/a/b;

    invoke-virtual {v0}, Lcom/amap/api/a/b;->postInvalidate()V

    .line 42
    return-void
.end method

.method public b(F)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 142
    iput p1, p0, Lcom/amap/api/a/v;->c:F

    .line 143
    return-void
.end method

.method public b(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 162
    iput p1, p0, Lcom/amap/api/a/v;->e:I

    .line 163
    return-void
.end method

.method public b(Lcom/amap/api/maps2d/model/LatLng;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/amap/api/a/v;->b:D

    iget-object v2, p0, Lcom/amap/api/a/v;->a:Lcom/amap/api/maps2d/model/LatLng;

    invoke-static {v2, p1}, Lcom/amap/api/maps2d/AMapUtils;->calculateLineDistance(Lcom/amap/api/maps2d/model/LatLng;Lcom/amap/api/maps2d/model/LatLng;)F

    move-result v2

    float-to-double v2, v2

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 190
    const/4 v0, 0x1

    .line 192
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lcom/amap/api/a/v;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 47
    const-string v0, "Circle"

    invoke-static {v0}, Lcom/amap/api/a/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/amap/api/a/v;->h:Ljava/lang/String;

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/amap/api/a/v;->h:Ljava/lang/String;

    return-object v0
.end method

.method public d()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 60
    iget v0, p0, Lcom/amap/api/a/v;->f:F

    return v0
.end method

.method public e()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/amap/api/a/v;->g:Z

    return v0
.end method

.method public f()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method g()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 114
    iput v0, p0, Lcom/amap/api/a/v;->j:I

    .line 115
    iput v0, p0, Lcom/amap/api/a/v;->k:I

    .line 116
    return-void
.end method

.method public h()Lcom/amap/api/maps2d/model/LatLng;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/amap/api/a/v;->a:Lcom/amap/api/maps2d/model/LatLng;

    return-object v0
.end method

.method public i()D
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/amap/api/a/v;->b:D

    return-wide v0
.end method

.method public j()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 147
    iget v0, p0, Lcom/amap/api/a/v;->c:F

    return v0
.end method

.method public k()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 157
    iget v0, p0, Lcom/amap/api/a/v;->d:I

    return v0
.end method

.method public l()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 167
    iget v0, p0, Lcom/amap/api/a/v;->e:I

    return v0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/amap/api/a/v;->a:Lcom/amap/api/maps2d/model/LatLng;

    .line 185
    return-void
.end method
